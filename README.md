SkyWeb
==============================


Install:
--------

For php dependencies management composer is used.

#### Installing composer

```sh
$ curl -sS https://getcomposer.org/installer | php
$ mv composer.phar /usr/local/bin/composer
```

#### Installing dependencies

```sh
$ composer install
```

For frontend building ad dependency management bower and gulp are used.

#### Installing nodejs
```sh
$ apt-get install nodejs
$ ln -s /usr/bin/nodejs /usr/bin/node
```
#### Installing npm
```sh
$ apt-get install npm
$ npm install npm -g
```
#### Installing gulp
```sh
$ npm install --global gulp
```
#### Installing bower
```sh
$ npm install -g bower
```
#### Installing git (is required for bower)
```sh
$ apt-get install git
```
#### Go to project directory and execute
```sh
$ npm install --save gulp
$ npm install --save gulp-sass
$ npm install --save gulp-copy
$ npm install --save gulp-minify-css
$ npm install --save gulp-concat
$ npm install --save gulp-uglify
$ npm install --save gulp-rename
$ npm install --save gulp-concat-css
$ npm install --save gulp-clean
$ npm install --save gulp-obfuscate
```
#### To install libraries in the project directory execute
```sh
$ bower install --allow-root
```
#### To install collect all styles, scripts, fonts and images run
```sh
$ gulp install
```
#### Gulp watch also is supported. To watch styles and scripts changes run
```sh
$ gulp watch
```

PHP Interpreter Preparation
-----------------------------

For correct working of admin area is needed to increase default max_input_vars value in php.ini (www.conf for fpm) to at least 2000.
max_input_vars=2000


MuOnline Database Preparation
-----------------------------

For correct database ORM mapping purposes MuOnline database collation for primary keys must be the same. If following SQL scripts will not be executed, website functionality proper working is not
guaranteed.

#### Execute following SQL scripts on your MuOnline databases.
```sh
ALTER TABLE dbo.MEMB_INFO ALTER COLUMN memb___id VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CS_AS;

ALTER TABLE dbo.MEMB_STAT DROP CONSTRAINT PK_MEMB_STAT;
ALTER TABLE dbo.MEMB_STAT ALTER COLUMN memb___id VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CS_AS NOT NULL;
ALTER TABLE dbo.MEMB_STAT   
ADD CONSTRAINT [PK_MEMB_STAT] PRIMARY KEY CLUSTERED 
(
	[memb___id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

ALTER TABLE dbo.Character ALTER COLUMN AccountID VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CS_AS;


ALTER TABLE dbo.AccountCharacter DROP CONSTRAINT PK_AccountCharacter;
ALTER TABLE dbo.AccountCharacter ALTER COLUMN Id VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CS_AS NOT NULL;
ALTER TABLE dbo.AccountCharacter   
ADD CONSTRAINT [PK_AccountCharacter] PRIMARY KEY CLUSTERED
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];


ALTER TABLE dbo.warehouse DROP CONSTRAINT PK_warehouse;
ALTER TABLE dbo.warehouse ALTER COLUMN AccountID VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CS_AS NOT NULL;
ALTER TABLE dbo.warehouse
ADD CONSTRAINT [PK_warehouse] PRIMARY KEY CLUSTERED 
(
	[AccountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];



ALTER TABLE dbo.MEMB_STAT ADD TotalSpentTime INT NOT NULL DEFAULT 0
ALTER TABLE dbo.Character ADD GRAND_RESETS INT NOT NULL DEFAULT 0
ALTER TABLE dbo.Character ADD PermanentStats INT NOT NULL DEFAULT 0

ALTER TABLE C_PlayerKiller_Info ADD Id INTEGER NOT NULL IDENTITY;
ALTER TABLE C_PlayerKiller_Info ADD PRIMARY KEY (Id);

```
#### Need to update WZ_DISCONNECT_MEMB Procedure
```sh
IF OBJECT_ID ( 'WZ_DISCONNECT_MEMB', 'P' ) IS NOT NULL
USE [IGC_low]
GO
/****** Object:  StoredProcedure [dbo].[WZ_DISCONNECT_MEMB]    Script Date: 16.12.2017 16:56:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER Procedure [dbo].[WZ_DISCONNECT_MEMB]

	@memb___id 	varchar(10)
AS
BEGIN

SET NOCOUNT ON
	DECLARE @find_id		 varchar(10)	
	DECLARE @ConnectStat	 tinyint
	SET @ConnectStat = 0
	SET @find_id = 'NOT'
	SELECT @find_id = S.memb___id FROM MEMB_STAT S INNER JOIN MEMB_INFO I ON S.memb___id = I.memb___id 
	     WHERE I.memb___id = @memb___id

	IF( @find_id <> 'NOT' )
	BEGIN
		UPDATE MEMB_STAT set ConnectStat = @ConnectStat, DisConnectTM = getdate(), TotalSpentTime = TotalSpentTime+(DATEDIFF(mi,ConnectTM,getdate()))
		 WHERE memb___id = @memb___id
	END
END
End
```

#### For correct working (and security reasons) it is recommended to specify separate SQL Users for each database. And for each user it is required
#### to specify default database. So for user created for database must be executes following query:
```sh
EXEC sp_defaultdb @loginame='USER_NAME', @defdb='DATABASE_NAME'
```

####MMOTOP Vote Reward
There is 2 commands which add WCoins to users. 
Both designed to be ran by CRON.
WCoins can be added only to users which has accounts on server.
Command which add WCoins each time user make vote can be safely ran multiple times. It will not add WCoins multiple times.
```
php bin/console app:vote:mmotop c
```
Command which add WCoins each month and choose Top Month Voters will regenerate voters vop each time when it is executed. So WCoins will be added each time too. So to avoid bugs it is recommended to execute it only one time per month.
```
php bin/console app:vote:mmotop f
```