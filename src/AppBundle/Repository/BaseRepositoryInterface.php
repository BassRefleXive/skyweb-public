<?php

declare(strict_types=1);

namespace AppBundle\Repository;


use AppBundle\Filter\Interfaces\FilterInterface;
use AppBundle\Filter\Interfaces\PaginatedFilterInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;

interface BaseRepositoryInterface
{
    public function findByQueryFilerPaginated(PaginatedFilterInterface $filter): Paginator;

    public function findByQueryFilter(FilterInterface $filter);

    public function save($entity);

    public function remove($entity);

    public function persist($entity);

    public function flush($entity = null);
}