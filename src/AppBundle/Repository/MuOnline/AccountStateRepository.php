<?php

namespace AppBundle\Repository\MuOnline;


use AppBundle\Repository\BaseDoctrineRepository;
use AppBundle\Repository\Traits\Cachable;

class AccountStateRepository extends BaseDoctrineRepository
{
    use Cachable;

    const STATE_OFFLINE = 0;
    const STATE_ONLINE = 1;

    public function findOnlineCount(): int
    {
        $qb = $this->createQueryBuilder('e')
                   ->select('COUNT(e.account) as ONLINE_COUNT')
                   ->where('e.connectionState = :STATE_ONLINE')
                   ->setParameter('STATE_ONLINE', AccountStateRepository::STATE_ONLINE);

        $result = $this->getSingleResult($this->getCachedQueryFromQueryBuilder($qb));

        return $result !== null
            ? $result['ONLINE_COUNT'] ?? 0
            : 0;
    }

    public function findOnline(): array
    {
        $qb = $this->createQueryBuilder('e')
                   ->where('e.connectionState = :STATE_ONLINE')
                   ->setParameter('STATE_ONLINE', AccountStateRepository::STATE_ONLINE);

        return $qb->getQuery()->getResult();
    }
}