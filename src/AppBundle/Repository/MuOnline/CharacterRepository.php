<?php

namespace AppBundle\Repository\MuOnline;


use AppBundle\Entity\MuOnline\Character;
use AppBundle\Repository\BaseDoctrineRepository;
use AppBundle\Repository\Traits\Cachable;

class CharacterRepository extends BaseDoctrineRepository
{
    use Cachable;

    const CTL_CODE_REGULAR = 0;
    const CTL_CODE_BANNED = 1;
    const CTL_CODE_GM = 8;
    const CTL_CODE_ADMIN = 32;

    public function changeName(Character $character, string $name)
    {
        $connection = $this->_em->getConnection();
        $sqls = [
            'UPDATE dbo.Character			SET Name		= ? 		WHERE Name			= ?;',
            'UPDATE dbo.AccountCharacter	SET GameID1 	= ? 		WHERE GameID1 		= ?;',
            'UPDATE dbo.AccountCharacter 	SET GameID2 	= ? 		WHERE GameID2 		= ?;',
            'UPDATE dbo.AccountCharacter 	SET GameID3 	= ? 		WHERE GameID3 		= ?;',
            'UPDATE dbo.AccountCharacter 	SET GameID4 	= ? 		WHERE GameID4 		= ?;',
            'UPDATE dbo.AccountCharacter 	SET GameID5 	= ? 		WHERE GameID5 		= ?;',
            'UPDATE dbo.AccountCharacter 	SET GameIDC 	= ? 		WHERE GameIDC 		= ?;',
            'UPDATE dbo.Guild 				SET G_Master 	= ? 		WHERE G_Master 		= ?;',
            'UPDATE dbo.GuildMember 		SET Name 		= ? 		WHERE Name 			= ?;',
            'UPDATE dbo.T_WaitFriend 		SET FriendName	= ? 		WHERE FriendName	= ?;',
            'UPDATE dbo.T_FriendMail 		SET FriendName 	= ? 		WHERE FriendName	= ?;',
            'UPDATE dbo.T_FriendMain		SET Name 		= ? 		WHERE Name 			= ?;',
            'UPDATE dbo.T_CGuid 			SET Name 		= ? 		WHERE Name 			= ?;',
            'UPDATE dbo.OptionData 			SET Name 		= ? 		WHERE Name 			= ?;',
        ];

        foreach ($sqls as $sql) {
            $stmt = $connection->prepare($sql);
            $stmt->execute([
                $name,
                $character->getName(),
            ]);
        }

    }

}