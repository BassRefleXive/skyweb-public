<?php

declare(strict_types=1);

namespace AppBundle\Repository\MuOnline\CastleSiege;

use AppBundle\Repository\BaseDoctrineRepository;
use AppBundle\Repository\Traits\Cachable;

class SiegeRegisteredGuildRepository extends BaseDoctrineRepository
{
    use Cachable;

    public function registered(): array
    {
        $qb = $this->createQueryBuilder('srg');

        $qb->select('srg')
            ->orderBy('srg.signCount', 'DESC');

        return $this->getCachedQuery($qb->getQuery())->getResult();
    }
}