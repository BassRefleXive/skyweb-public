<?php

declare(strict_types=1);

namespace AppBundle\Repository\MuOnline\CastleSiege;

use AppBundle\Entity\MuOnline\CastleSiege\CastleData;
use AppBundle\Exception\MuOnline\CastleSiege\DataNotFoundException;
use AppBundle\Repository\BaseDoctrineRepository;
use AppBundle\Repository\Traits\Cachable;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class CastleDataRepository extends BaseDoctrineRepository
{
    use Cachable;

    public function get(): CastleData
    {
        $qb = $this->createQueryBuilder('cd');

        $qb->select('cd')
            ->setMaxResults(1);

        try {
            $castleData = $this->getCachedQuery($qb->getQuery())->getSingleResult();
        } catch (NonUniqueResultException|NoResultException $e) {
            throw DataNotFoundException::missingData();
        }

        return $castleData;
    }
}