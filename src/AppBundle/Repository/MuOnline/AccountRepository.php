<?php

namespace AppBundle\Repository\MuOnline;


use AppBundle\Repository\BaseDoctrineRepository;

class AccountRepository extends BaseDoctrineRepository
{
    const CTL_CODE_REGULAR = 1;
    const CTL_CODE_BANNED = 0;
}