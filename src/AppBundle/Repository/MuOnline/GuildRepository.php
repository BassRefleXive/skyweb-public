<?php

namespace AppBundle\Repository\MuOnline;

use AppBundle\Repository\BaseDoctrineRepository;
use AppBundle\Repository\Traits\Cachable;

class GuildRepository extends BaseDoctrineRepository
{
    use Cachable;

    public function findByName(string $name)
    {
        $query = $this->_em->createQuery('SELECT g, gm, gmc FROM AppBundle\Entity\MuOnline\Guild g JOIN g.members gm JOIN gm.character gmc WHERE g.name = :NAME');
        $query->setParameter('NAME', $name);

        return $this->getSingleResult($this->getCachedQuery($query));
    }

}