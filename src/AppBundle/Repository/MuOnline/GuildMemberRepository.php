<?php

namespace AppBundle\Repository\MuOnline;

use AppBundle\Repository\BaseDoctrineRepository;
use AppBundle\Repository\Traits\Cachable;

class GuildMemberRepository extends BaseDoctrineRepository
{
    use Cachable;
}