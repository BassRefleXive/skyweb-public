<?php

namespace AppBundle\Repository\MuOnline;


use AppBundle\Entity\MuOnline\Character;
use AppBundle\Enum\MuOnline\PeriodItemType;
use AppBundle\Repository\BaseDoctrineRepository;
use Collection\Sequence;

class PeriodItemInfoRepository extends BaseDoctrineRepository
{
    public function findActiveByCharacter(Character $character): Sequence
    {
        $qb = $this->createQueryBuilder('pi');

        $qb->select('pi')
            ->where('pi.type = :type_seal')
            ->andWhere('pi.active = :is_active')
            ->andWhere('pi.expireAt > :now')
            ->andWhere('pi.character = :owner')
            ->setParameter('type_seal', PeriodItemType::SEAL)
            ->setParameter('is_active', true)
            ->setParameter('now', (new \DateTime())->getTimestamp())
            ->setParameter('owner', $character);

        return new Sequence($qb->getQuery()->getResult());
    }
}