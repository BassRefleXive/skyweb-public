<?php

namespace AppBundle\Repository\Traits;


use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;

trait Cachable
{

    protected $ttl;

    public final function setTtl(int $ttl)
    {
        $this->ttl = $ttl;
    }

    public final function getCachedQuery(Query $query): AbstractQuery
    {
        $this->validateTtl();

        if ($this->ttl > 0) {
            $query->useResultCache(true)->setResultCacheLifetime($this->ttl);
        }

        return $query;
    }

    public final function getCachedQueryFromQueryBuilder(QueryBuilder $qb): AbstractQuery
    {
        return $this->getCachedQuery($qb->getQuery());
    }

    private function validateTtl()
    {
        if ($this->ttl === null) {
            throw new \RuntimeException('This repository method require to be cachable.');
        }
    }

}