<?php


namespace AppBundle\Repository\Application;


use AppBundle\Entity\Application\Server;
use AppBundle\Entity\Application\TopVoter;
use AppBundle\Repository\BaseDoctrineRepository;
use Collection\Sequence;

class TopVoterRepository extends BaseDoctrineRepository
{
    public function findByServer(Server $server)
    {
        $qb = $this->createQueryBuilder('tv')
            ->select('tv')
            ->leftJoin('tv.userServerAccount', 'usa')
            ->leftJoin('usa.server', 's')
            ->where('s = :SERVER')
            ->setParameters([
                'SERVER' => $server,
            ]);

        return $qb->getQuery()->getResult();
    }

    public function bulkDelete(Sequence $voters)
    {
        $qb = $this->createQueryBuilder('tv')
            ->delete(TopVoter::class, 'tv')
            ->where('tv IN (:TOP_VOTERS)')
            ->setParameters([
                'TOP_VOTERS' => $voters->all(),
            ]);

        $qb->getQuery()->execute();
    }
}