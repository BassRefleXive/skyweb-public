<?php

namespace AppBundle\Repository\Application;

use AppBundle\Repository\BaseDoctrineRepository;
use AppBundle\Repository\Traits\Cachable;

class CartDiscountCouponRepository extends BaseDoctrineRepository
{
    const STATUS_DISABLED = 0;
    const STATUS_ENABLED = 1;

    use Cachable;

    public function findByText(string $text)
    {
        $result = null;

        $qb = $this
            ->createQueryBuilder('c')
            ->where('c.coupon = :COUPON')
            ->andWhere('c.expireAt > :TODAY')
            ->setParameters([
                'COUPON' => $text,
                'TODAY'  => new \DateTime(),
            ])
            ->setMaxResults(1);

        return $this->getSingleResult($qb->getQuery());
    }

}