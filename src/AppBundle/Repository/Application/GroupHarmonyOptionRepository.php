<?php

namespace AppBundle\Repository\Application;


use AppBundle\Entity\Application\Items\HarmonyOption;
use AppBundle\Repository\BaseDoctrineRepository;
use AppBundle\Repository\Traits\Cachable;

class GroupHarmonyOptionRepository extends BaseDoctrineRepository
{
    use Cachable;

    public function findByItemGroupAndHarmonyType(int $groupId, int $harmonyTypeId)
    {
        $qb = $this->createQueryBuilder('gho');

        $qb
            ->select('gho')
            ->leftJoin('gho.group', 'g')
            ->where('g INSTANCE OF :group')
            ->andWhere('gho.type = :type')
            ->setParameters([
                'group' => $groupId - 1,
                'type'  => $harmonyTypeId,
            ]);

        $query = $this->getCachedQueryFromQueryBuilder($qb);

        return $this->getSingleResult($query);
    }

    public function findByItemGroupAndHarmonyOption(int $groupId, HarmonyOption $option)
    {
        $qb = $this->createQueryBuilder('gho');

        $qb
            ->select('gho')
            ->leftJoin('gho.group', 'g')
            ->where('g INSTANCE OF :group')
            ->andWhere('gho.harmonyOption = :option')
            ->setParameters([
                'group'  => $groupId -1,
                'option' => $option,
            ]);

        $query = $this->getCachedQueryFromQueryBuilder($qb);

        return $this->getSingleResult($query);
    }
}