<?php

namespace AppBundle\Repository\Application\MagicList;


use AppBundle\Entity\Application\MagicList\Skill;
use AppBundle\Enum\Application\MagicList\SkillUseType;
use AppBundle\Enum\MuOnline\CharacterClass;
use AppBundle\Repository\BaseDoctrineRepository;
use AppBundle\Repository\Traits\Cachable;
use Collection\Map;

class SkillRepository extends BaseDoctrineRepository
{
    use Cachable;

    public function removeAll(): void
    {
        $this->createQueryBuilder('s')
            ->delete()
            ->getQuery()
            ->execute();
    }

    public function findByIds(array $ids): Map
    {
        $qb = $this->createQueryBuilder('s');

        $qb->select('s')->where($qb->expr()->in('s.id', $ids));

        $skills = $this->getCachedQuery($qb->getQuery())->getResult();;

        $result = new Map();

        /** @var Skill $skill */
        foreach ($skills as $skill) {
            $result->set($skill->id(), $skill);
        }

        return $result;
    }

    public function findMasterSkills(CharacterClass $characterClass): Map
    {
        $qb = $this->createQueryBuilder('s');

        $qb->select('s')
            ->leftJoin('s.characterClasses', 'cc')
            ->where('cc.type = :character_type')
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->eq('s.useType', ':master_passive'),
                    $qb->expr()->eq('s.useType', ':master_active')
                )
            )
            ->setParameter('character_type', $characterClass->getValue())
            ->setParameter('master_passive', SkillUseType::ML_PASSIVE)
            ->setParameter('master_active', SkillUseType::ML_ACTIVE);

        $skills = $this->getCachedQuery($qb->getQuery())->getResult();;

        $result = new Map();

        /** @var Skill $skill */
        foreach ($skills as $skill) {
            $result->set($skill->id(), $skill);
        }

        return $result;
    }
}