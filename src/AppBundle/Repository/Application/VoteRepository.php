<?php


namespace AppBundle\Repository\Application;


use AppBundle\Collection\Votes\VoteCollection;
use AppBundle\Entity\Application\Vote;
use AppBundle\Enum\Application\VoteProvider;
use AppBundle\Exception\Application\Vote\VoteNotFoundException;
use AppBundle\Model\Application\Votes\VoteModel;
use AppBundle\Repository\BaseDoctrineRepository;
use Collection\Sequence;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;

class VoteRepository extends BaseDoctrineRepository
{
    public function findByCollection(VoteCollection $collection): Sequence
    {
        return new Sequence(
            $collection->length() > 0
                ? $this->findBy(
                [
                    'providerId' => $collection
                        ->map(function ($idx, VoteModel $model) {
                            return $model->id();
                        })
                        ->all(),
                ]
            )
                : []
        );
    }

    public function getProviderVoteByIpInPeriod(string $ip, VoteProvider $provider, \DateTimeImmutable $start, \DateTimeImmutable $end): Vote
    {
        $qb = $this->createProviderVoteQueryBuilder($provider);

        try {
            /** @var Vote|null $vote */
            $qb->select('v')
                ->andWhere('v.ip = :ip')
                ->andWhere('v.dateTime >= :vote_day_start')
                ->andWhere('v.dateTime < :vote_day_end')
                ->setParameter('ip', $ip)
                ->setParameter('vote_day_start', $start->format('Y-m-d H:i:s'))
                ->setParameter('vote_day_end', $end->format('Y-m-d H:i:s'))
                ->setMaxResults(1);

            $vote = $qb->getQuery()->getOneOrNullResult();
        } catch (NoResultException $e) {
            throw VoteNotFoundException::missingByIpAtDate($ip, $provider, $end);
        }

        if (null === $vote) {
            throw VoteNotFoundException::missingByIpAtDate($ip, $provider, $end);
        }

        return $vote;
    }

    public function getProviderVoteById(int $id, VoteProvider $provider): Vote
    {
        $qb = $this->createProviderVoteQueryBuilder($provider);

        try {
            /** @var Vote|null $vote */
            $qb->select('v')
                ->andWhere('v.id = :id')
                ->setParameter('id', $id)
                ->setMaxResults(1);

            $vote = $qb->getQuery()->getOneOrNullResult();
        } catch (NoResultException $e) {
            throw VoteNotFoundException::missingById($id, $provider);
        }

        if (null === $vote) {
            throw VoteNotFoundException::missingById($id, $provider);
        }

        return $vote;
    }

    private function createProviderVoteQueryBuilder(VoteProvider $provider): QueryBuilder
    {
        $qb = $this->createQueryBuilder('v');

        /** @var Vote|null $vote */
        $qb->select('v')
            ->andWhere('v.provider = :provider')
            ->setParameter('provider', $provider->getValue());

        return $qb;
    }

    public function getProviderVoteByAccountAtDate(int $account, VoteProvider $provider, \DateTimeInterface $date): Vote
    {
        /** @var Vote $vote */
        $vote = $this->findOneBy([
            'voter'    => $account,
            'dateTime' => $date->format('Y-m-d'),
            'provider' => $provider,
        ]);

        if (null === $vote) {
            throw VoteNotFoundException::missingByAccountDate($account, $provider, $date);
        }

        return $vote;
    }

    /**
     * @param \DateTimeInterface $start
     * @param \DateTimeInterface $end
     *
     * @return Vote[]
     */
    public function findVotesInPeriod(\DateTimeInterface $start, \DateTimeInterface $end): array
    {
        $qb = $this->createQueryBuilder('v');

        $qb->where('v.dateTime > :start_date')
            ->andWhere('v.dateTime <= :end_date')
            ->setParameter('start_date', $start->format('Y-m-d H:i:s'))
            ->setParameter('end_date', $end->format('Y-m-d H:i:s'));

        return $qb->getQuery()->getResult();
    }
}