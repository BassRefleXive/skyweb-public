<?php

namespace AppBundle\Repository\Application;

use AppBundle\Entity\Application\Server;
use AppBundle\Entity\Application\WebShopCategory;
use AppBundle\Repository\BaseDoctrineRepository;
use AppBundle\Repository\Traits\Cachable;

class WebShopCategoryRepository extends BaseDoctrineRepository
{
    use Cachable;

    const STATUS_DISABLED = 0;
    const STATUS_ENABLED = 1;

    public function findPossibleParentCategories(Server $server, WebShopCategory $category = null): array
    {
        $qb = $this->createQueryBuilder('wsc')
            ->select('wsc')
            ->innerJoin('wsc.config', 'cfg')
            ->where('wsc.status = :status_enabled')
            ->andWhere('cfg.server = :server')
            ->setParameter('status_enabled', self::STATUS_ENABLED)
            ->setParameter('server', $server);

        if ($category instanceof WebShopCategory) {
            $qb->andWhere('wsc != :current_category')
                ->setParameter('current_category', $category);
        }

        return $this->getCachedQueryFromQueryBuilder($qb)->getResult();
    }

    public function findRootCategories(Server $server): array
    {
        return $this->getCachedQueryFromQueryBuilder(
            $this->createQueryBuilder('wsc')
                ->select('wsc')
                ->leftJoin('wsc.config', 'cfg')
                ->where('wsc.status = :status_enabled')
                ->andWhere('wsc.parentCategory IS NULL')
                ->andWhere('cfg.server = :server')
                ->setParameter('status_enabled', self::STATUS_ENABLED)
                ->setParameter('server', $server)
                ->orderBy('wsc.position', 'ASC')
        )->getResult();
    }
}