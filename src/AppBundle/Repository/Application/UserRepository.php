<?php


namespace AppBundle\Repository\Application;


use AppBundle\Collection\Votes\VoteCollection;
use AppBundle\Entity\Application\User\User;
use AppBundle\Entity\Application\UserServerAccount;
use AppBundle\Exception\Application\User\UserNotFoundException;
use AppBundle\Model\Application\Votes\VoteModel;
use AppBundle\Repository\BaseDoctrineRepository;
use AppBundle\Repository\Traits\LikeQueryHelpers;
use Collection\Sequence;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query;

class UserRepository extends BaseDoctrineRepository
{
    use LikeQueryHelpers;

    /**
     * @return null|User
     */
    public function findByLoginAndEmail(string $login, string $email)
    {
        /** @var null|User $result */
        $result = $this->findOneBy([
            'username' => $login,
            'email' => $email,
        ]);

        return $result;
    }

    public function getByLogin(string $login): User
    {
        /** @var User|null $user */
        $user = $this->findOneBy(['username' => $login]);

        if (null === $user) {
            throw UserNotFoundException::missingByLogin($login);
        }

        return $user;
    }

    public function getByDisplayName(string $displayName): User
    {
        /** @var User|null $user */
        $user = $this->findOneBy(['displayName' => $displayName]);

        if (null === $user) {
            throw UserNotFoundException::missingByDisplayName($displayName);
        }

        return $user;
    }

    /**
     * @return null|User
     */
    public function findByPasswordResetToken(string $token)
    {
        /** @var null|User $result */
        $result = $this->findOneBy([
            'resetPasswordToken' => $token,
        ]);

        return $result;
    }

    public function findByDisplayName(array $params): array
    {
        return $this->createQueryBuilder('u')
            ->select('u')
            ->where('UPPER(u.displayName) = UPPER(:display_name)')
            ->setParameter('display_name', $params['displayName'])
            ->getQuery()
            ->getResult();
    }

    public function findByLogin(array $params): array
    {
        return $this->createQueryBuilder('u')
            ->select('u')
            ->where('UPPER(u.username) = UPPER(:username)')
            ->setParameter('username', $params['username'])
            ->getQuery()
            ->getResult();
    }

    public function findByUserNames(array $userNames): array
    {
        return $this->createQueryBuilder('u')
            ->select('u, usa, s, vl, c')
            ->innerJoin('u.userServerAccounts', 'usa')
            ->leftJoin('u.country', 'c')
            ->innerJoin('usa.server', 's')
            ->leftJoin('usa.vipLevel', 'vl')
            ->where('u.username IN (:userNames)')
            ->setParameter('userNames', $userNames)
            ->getQuery()
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->getResult();
    }

    public function findByEmail(array $params): array
    {
        return $this->createQueryBuilder('u')
            ->select('u')
            ->where('UPPER(u.email) = UPPER(:email)')
            ->setParameter('email', $params['email'])
            ->getQuery()
            ->getResult();
    }

    public function findByKeyword(string $keyword): array
    {
        $qb = $this->createQueryBuilder('u');

        $qb->select('u')
            ->where("u.id LIKE :keyword ESCAPE '!'")
            ->orWhere("u.username LIKE :keyword ESCAPE '!'")
            ->orWhere("u.displayName LIKE :keyword ESCAPE '!'")
            ->orWhere("u.email LIKE :keyword ESCAPE '!'")
            ->setParameter('keyword', $this->makeLikeParam($keyword));

        if (filter_var($keyword, FILTER_VALIDATE_IP)) {
            $qb
                ->orWhere("u.ipAddress LIKE :ip_address ESCAPE '!'")
                ->setParameter('ip_address', ip2long($keyword));
        }

        return $qb->getQuery()->getResult();
    }

    public function saveUser(User $user)
    {
        $this->_em->persist($user);
        $this->_em->flush($user);
    }

    /**
     * @param \DateTimeInterface $date
     * @return User[]
     */
    public function findUsersWithExpiredBans(\DateTimeInterface $date): array
    {
        $qb = $this->createQueryBuilder('u');

        $qb->select('u, b')
            ->innerJoin('u.bans', 'b')
            ->where('b.unBanProcessed = :not_processed')
            ->groupBy('u')
            ->having('MAX(b.expireAt) < :now')
            ->orderBy('b.expireAt', 'DESC')
            ->setParameter('not_processed', false)
            ->setParameter('now', $date);

        return $qb->getQuery()->getResult();
    }

    public function findByVoteCollection(VoteCollection $collection): Sequence
    {
        return new Sequence(
            $collection->length() > 0
                ? $this->findBy(
                [
                    'username' => $collection
                        ->map(function ($idx, VoteModel $model) {
                            return $model->name();
                        })
                        ->all(),
                ]
            )
                : []
        );
    }
}