<?php

namespace AppBundle\Repository\Application\Item;


use AppBundle\Entity\Application\Items\SocketOption;
use AppBundle\Repository\BaseDoctrineRepository;
use AppBundle\Repository\Traits\Cachable;
use Collection\Map;

class SocketOptionsRepository extends BaseDoctrineRepository
{
    use Cachable;

    public function all(): Map
    {
        $qb = $this->createQueryBuilder('so');

        $qb->select('so');

        $options = $this->getCachedQueryFromQueryBuilder($qb)->getResult();

        $result = new Map();

        /** @var SocketOption $option */
        foreach ($options as $option) {
            $result->set($option->option(), $option);
        }

        return $result;
    }
}