<?php

namespace AppBundle\Repository\Application;

use AppBundle\Repository\BaseDoctrineRepository;
use AppBundle\Repository\Traits\Cachable;

class CartPurchaseLogRepository extends BaseDoctrineRepository
{
    use Cachable;
}