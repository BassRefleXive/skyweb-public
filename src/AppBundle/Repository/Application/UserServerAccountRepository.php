<?php


namespace AppBundle\Repository\Application;


use AppBundle\Doctrine\Type\VoteTypeEnum;
use AppBundle\Entity\Application\Server;
use AppBundle\Entity\Application\User\Admin;
use AppBundle\Enum\Application\VoteType;
use AppBundle\Repository\BaseDoctrineRepository;
use AppBundle\Repository\Traits\Cachable;

class UserServerAccountRepository extends BaseDoctrineRepository
{
    use Cachable;

    public function findLastMonthTopVotersByServer(Server $server): array
    {
        $qb = $this->createQueryBuilder('usa')
            ->select('usa, tv')
            ->leftJoin('usa.server', 's')
            ->innerJoin('usa.monthlyVotersTopRewards', 'tv')
            ->where('s = :server')
            ->andWhere('tv.periodStart >= :from')
            ->andWhere('tv.periodEnd <= :till')
            ->setParameter('server', $server)
            ->setParameter('from', new \DateTime(date('Y-m-d 00:00:00', strtotime('first day of last month'))))
            ->setParameter('till', new \DateTime(date('Y-m-d 23:59:59', strtotime('last day of last month'))))
            ->orderBy('tv.place', 'ASC');

        return $this->getCachedQueryFromQueryBuilder($qb)->getResult();
    }

    public function unHideHiddenExpiredAccounts(): int
    {
        $qb = $this->createQueryBuilder('usa');

        $qb->update()
            ->set('usa.hiddenTill', ':null')
            ->where('usa.hiddenTill IS NOT NULL')
            ->andWhere('usa.hiddenTill <= :now')
            ->setParameter('null', null)
            ->setParameter('now', new \DateTimeImmutable());

        return $qb->getQuery()->execute();
    }

    public function deactivateExpiredVIP(): int
    {
        $qb = $this->createQueryBuilder('usa');

        $qb->update()
            ->set('usa.vipDays', ':null')
            ->set('usa.vipLevel', ':null')
            ->set('usa.vipTill', ':null')
            ->where('usa.vipLevel IS NOT NULL')
            ->andWhere('usa.vipTill <= :now')
            ->setParameter('null', null)
            ->setParameter('now', new \DateTimeImmutable());

        return $qb->getQuery()->execute();
    }
}