<?php

declare(strict_types = 1);

namespace AppBundle\Repository\Application\Payments;


use AppBundle\Entity\Application\Payments\Order;
use AppBundle\Repository\BaseDoctrineRepository;

class OrderRepository extends BaseDoctrineRepository
{
}