<?php

namespace AppBundle\Repository\Application;

use AppBundle\Repository\BaseDoctrineRepository;
use AppBundle\Repository\Traits\Cachable;

class CharacterClassRepository extends BaseDoctrineRepository
{
    use Cachable;

    public function findByTypes(array $types): array
    {
        $qb = $this->createQueryBuilder('cc');

        $qb->select('cc')
            ->where($qb->expr()->in('cc.type', $types));

        return $this->getCachedQuery($qb->getQuery())->getResult();
    }
}