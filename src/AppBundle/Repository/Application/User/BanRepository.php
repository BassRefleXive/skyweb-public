<?php

declare(strict_types = 1);

namespace AppBundle\Repository\Application\User;

use AppBundle\Entity\Application\User\User;
use AppBundle\Repository\BaseDoctrineRepository;

class BanRepository extends BaseDoctrineRepository
{
    public function findByUser(User $user): array
    {
        return $this->findBy(['user' => $user]);
    }
}