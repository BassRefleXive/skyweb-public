<?php

namespace AppBundle\Repository\Application;

use AppBundle\Entity\Application\CartPersistentDiscount;
use AppBundle\Entity\Application\Server;
use AppBundle\Repository\BaseDoctrineRepository;
use AppBundle\Repository\Traits\Cachable;

class CartPersistentDiscountRepository extends BaseDoctrineRepository
{
    use Cachable;

    public function findInRange(Server $server, float $spentAmount, string $type)
    {
        $result = null;

        $qb = $this
            ->createQueryBuilder('d')
            ->where('d.spentFrom <= :TOTAL_SPENT')
            ->andWhere('d.spentTo > :TOTAL_SPENT')
            ->andWhere('d.server = :SERVER')
            ->andWhere('d.type = :TYPE')
            ->setParameters([
                'TOTAL_SPENT' => $spentAmount,
                'SERVER'      => $server,
                'TYPE'        => $type,
            ])
            ->setMaxResults(1);

        return $this->getSingleResult($this->getCachedQueryFromQueryBuilder($qb));
    }

    public function findInRangeExceptLast(Server $server, float $spentAmount, string $type)
    {
        $result = null;

        $qb = $this->createQueryBuilder('d');

        $qb->where('d.spentFrom <= :TOTAL_SPENT')
            ->andWhere('d.spentTo > :TOTAL_SPENT')
            ->andWhere('d.server = :SERVER')
            ->andWhere('d.type = :TYPE')
            ->andWhere(
                $qb->expr()->lt(
                    'd.spentTo',
                    $this->createGreatestLimitException($server, $type)
                )
            )
            ->setParameter('SERVER', $server)
            ->setParameter('TOTAL_SPENT', $spentAmount)
            ->setParameter('TYPE', $type)
            ->setMaxResults(1);

        return $this->getSingleResult($this->getCachedQueryFromQueryBuilder($qb));
    }

    public function findNext(CartPersistentDiscount $cartPersistentDiscount)
    {
        $qb = $this->createQueryBuilder('d');

        $qb->where('d.spentFrom >= :PREVIOUS_TO')
            ->andWhere('d.server = :SERVER')
            ->andWhere('d.type = :TYPE')
            ->orderBy('d.spentFrom', 'ASC')
            ->setParameter('SERVER', $cartPersistentDiscount->getServer())
            ->setParameter('PREVIOUS_TO', $cartPersistentDiscount->getSpentTo())
            ->setParameter('TYPE', $cartPersistentDiscount->getType())
            ->setMaxResults(1);

        return $this->getSingleResult($this->getCachedQueryFromQueryBuilder($qb));
    }

    private function createGreatestLimitException(Server $server, string $type): string
    {
        $qb = $this->createQueryBuilder('d2');

        return '(' . $qb->select('MAX(d2.spentTo)')
                ->where('d2.server = :SERVER')
                ->andWhere('d2.type = :TYPE')
                ->setParameter('SERVER', $server)
                ->setParameter('TYPE', $type)
                ->getDQL() . ')';
    }
}