<?php

namespace AppBundle\Repository\Application;


use AppBundle\Doctrine\Type\Mappings\Items\Item;
use AppBundle\Entity\Application\Items\ItemInfo;
use AppBundle\Repository\BaseDoctrineRepository;
use AppBundle\Repository\Traits\Cachable;
use Collection\Sequence;

class ItemInfoRepository extends BaseDoctrineRepository
{
    use Cachable;

    public function findByItem(Item $item)
    {
        $qb = $this->createQueryBuilder('ii')
            ->select('ii')
            ->leftJoin('ii.group', 'g')
            ->where('g INSTANCE OF :group AND ii.itemId = :itemId AND ii.level = 0')
            ->orWhere('g INSTANCE OF :group AND ii.itemId = :itemId AND ii.level = :level')
            ->orderBy('ii.level', 'ASC')
            ->setParameters([
                'group' => $item->getGeneralOptions()->getGroup(),
                'itemId' => $item->getGeneralOptions()->getId(),
                'level' => $item->getGeneralOptions()->getLvl(),
            ]);

        $result = $this->getCachedQueryFromQueryBuilder($qb)->getResult();

        $res = end($result);

        return $item->getGeneralOptions()->getLvl() > 0
            ? ($res instanceof ItemInfo && $res->getLevel() === $item->getGeneralOptions()->getLvl() ? $res : array_shift($result))
            : array_shift($result);
    }

    public function findByCombinedItemCodes(array $codes): Sequence
    {
        $q = $this->createNativeNamedQuery('findByCombinedCodes')
            ->setParameter('codes', $codes);

        return new Sequence($q->getResult());
    }
}