<?php


namespace AppBundle\Repository\Application;


use AppBundle\Entity\Application\User\AuthLog;
use AppBundle\Exception\Application\AuthLog\AuthLogNotFoundException;
use AppBundle\Repository\BaseDoctrineRepository;
use Doctrine\ORM\NoResultException;

class AuthLogRepository extends BaseDoctrineRepository
{
    public function getLastSuccessfulForIp(int $ip): AuthLog
    {
        $qb = $this->createQueryBuilder('ual');

        try {
            return $qb->select('ual')
                ->where('ual.ip = :ip')
                ->andWhere('ual.success = :success')
                ->orderBy('ual.createdAt', 'DESC')
                ->setParameter('ip', $ip)
                ->setParameter('success', true)
                ->setMaxResults(1)
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $e) {
            throw AuthLogNotFoundException::missingSuccessfulByIp($ip);
        }
    }

    public function getUserLastSuccessful(string $username): AuthLog
    {
        $qb = $this->createQueryBuilder('ual');

        try {
            return $qb->select('ual')
                ->where('ual.login = :username')
                ->andWhere('ual.success = :success')
                ->orderBy('ual.createdAt', 'DESC')
                ->setParameter('username', $username)
                ->setParameter('success', true)
                ->setMaxResults(1)
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $e) {
            throw AuthLogNotFoundException::missingSuccessfulByUsername($username);
        }
    }

    /**
     * @return AuthLog[]
     */
    public function findUserSuccessfulInPeriod(string $username): array
    {
        $qb = $this->createQueryBuilder('ual');

        return $qb->select('ual')
            ->where('ual.login = :login')
            ->andWhere('ual.success = :success')
            ->orderBy('ual.createdAt', 'ASC')
            ->groupBy('ual.ip')
            ->setParameter('login', $username)
            ->setParameter('success', true)
            ->getQuery()
            ->getResult();
    }
}