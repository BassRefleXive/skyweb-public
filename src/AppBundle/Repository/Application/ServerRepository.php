<?php

namespace AppBundle\Repository\Application;


use AppBundle\Repository\BaseDoctrineRepository;
use AppBundle\Repository\Traits\Cachable;


class ServerRepository extends BaseDoctrineRepository
{
    use Cachable;

    public function findFirst()
    {
        $result = null;

        $qb = $this
            ->createQueryBuilder('s')
            ->setMaxResults(1)
            ->orderBy('s.id');

        $query = $this->getCachedQueryFromQueryBuilder($qb);

        return $this->getSingleResult($query);
    }
    
    public function findLast()
    {
        $result = null;

        $qb = $this
            ->createQueryBuilder('s')
            ->setMaxResults(1)
            ->orderBy('s.id', 'DESC');

        $query = $this->getCachedQueryFromQueryBuilder($qb);

        return $this->getSingleResult($query);
    }
    
    public function findAll()
    {
        $qb = $this
            ->createQueryBuilder('s')
            ->select('s');

        return $this->getCachedQueryFromQueryBuilder($qb)->getResult();
    }

}