<?php

namespace AppBundle\Repository\Application;


use AppBundle\Entity\Application\Server;
use AppBundle\Repository\BaseDoctrineRepository;
use AppBundle\Repository\Traits\Cachable;

class NewsRepository extends BaseDoctrineRepository
{
    use Cachable;

    const STATUS_DISABLED = 0;
    const STATUS_ENABLED = 1;
}