<?php

namespace AppBundle\Repository\Application;


use AppBundle\Entity\Application\Server;
use AppBundle\Entity\Application\Timer;
use AppBundle\Enum\Application\TimerStatus;
use AppBundle\Repository\BaseDoctrineRepository;
use AppBundle\Repository\Traits\Cachable;


class TimerRepository extends BaseDoctrineRepository
{
    use Cachable;

    public function getClosestActive(Server $server, \DateTimeInterface $from = null): ?Timer
    {
        $from = $from ?? new \DateTimeImmutable();

        $qb = $this->createQueryBuilder('t');

        $qb->select('t')
            ->where('t.status = :status_active')
            ->andWhere('t.time > :from_date')
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->orX('t.server = :server'),
                    $qb->expr()->orX('t.server IS NULL')
                )
            )
            ->orderBy('t.time', 'ASC')
            ->setMaxResults(1)
            ->setParameter('status_active', TimerStatus::ACTIVE)
            ->setParameter('from_date', $from)
            ->setParameter('server', $server);

        return $this->getCachedQueryFromQueryBuilder($qb)->getOneOrNullResult();
    }
}