<?php

namespace AppBundle\Repository;


use AppBundle\Filter\Interfaces\FilterInterface;
use AppBundle\Filter\Interfaces\PaginatedFilterInterface;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;

class BaseDoctrineRepository extends EntityRepository
{
    protected function getSingleResult(AbstractQuery $query)
    {
        $result = null;

        try {
            $result = $query->getSingleResult();
        } catch (NoResultException $e) {
        } catch (NonUniqueResultException $e) {
        }

        return $result;
    }

    public function findByQueryFilerPaginated(PaginatedFilterInterface $filter): Paginator
    {
        /** @var QueryBuilder $qb */
        $qb = $this->createQueryBuilder('e');

        $qb->select('e');

        $paginator = $filter->buildPaginator($qb);

        $query = $paginator->getQuery();

        if (property_exists($this, 'ttl') && $this->ttl > 0) {
            $query->useResultCache(true)->setResultCacheLifetime($this->ttl);
        }

        return $paginator;
    }

    public function findByQueryFilter(FilterInterface $filter)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->createQueryBuilder('e');

        $qb->select('e');

        $query = $filter->buildQueryFilter($qb);

        if (property_exists($this, 'ttl') && $this->ttl > 0) {
            $query->useResultCache(true)->setResultCacheLifetime($this->ttl);
        }

        return $query->getResult();
    }

    public function save($entity)
    {
        $this->_em->persist($entity);
        $this->_em->flush($entity);
    }

    public function remove($entity)
    {
        $this->_em->remove($entity);
        $this->_em->flush($entity);
    }

    public function persist($entity)
    {
        $this->_em->persist($entity);
    }

    public function flush($entity = null)
    {
        $this->_em->flush($entity);
    }
}