<?php

declare(strict_types=1);

namespace AppBundle\Http\GuzzleHttp;

use AppBundle\Http\Exception\HttpClientException;
use AppBundle\Http\HttpClientInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class Client implements HttpClientInterface
{
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function send(RequestInterface $request, array $options = []): ResponseInterface
    {
        try {
            return $this->client->send($request, $options);
        } catch (RequestException $e) {
            throw new HttpClientException($e->getMessage(), $e->getCode(), $e->getPrevious());
        }
    }
}
