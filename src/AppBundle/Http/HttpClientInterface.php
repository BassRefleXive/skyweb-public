<?php

declare(strict_types=1);

namespace AppBundle\Http;

use AppBundle\Http\Exception\HttpClientException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

interface HttpClientInterface
{
    /**
     * @throws HttpClientException
     */
    public function send(RequestInterface $request, array $options = []): ResponseInterface;
}
