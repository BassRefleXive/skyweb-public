<?php

declare(strict_types=1);

namespace AppBundle\Http\Factory;

use AppBundle\Http\Criteria\UriCriteriaInterface;

interface UriFactoryInterface
{
    public function create(UriCriteriaInterface $uriCriteria): string;
}
