<?php

declare(strict_types=1);

namespace AppBundle\Http\Exception;

class HttpClientException extends \RuntimeException
{
}
