<?php

declare(strict_types=1);

namespace AppBundle\Http\Exception;


class ResponseException extends \RuntimeException
{
    public function __construct($message = '', $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function invalidFormat(string $format): self
    {
        return new self(sprintf('Invalid response format. Expected "%s".', $format));
    }
}
