<?php

declare(strict_types=1);

namespace AppBundle\Http\RequestBody;

interface BodyParamsInterface
{
    public function params(): array;
}
