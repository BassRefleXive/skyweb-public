<?php

declare(strict_types=1);

namespace AppBundle\Http\RequestBody\Factory;

use AppBundle\Http\RequestBody\BodyParamsInterface;
use GuzzleHttp\Psr7\MultipartStream;

abstract class RequestBodyFactory
{
    /**
     * @return MultipartStream|string
     */
    abstract public function create(BodyParamsInterface $params);
}
