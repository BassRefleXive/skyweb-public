<?php

declare(strict_types=1);

namespace AppBundle\Http\RequestBody\Factory;

use AppBundle\Http\RequestBody\BodyParamsInterface;

class JsonBodyFactory extends RequestBodyFactory
{
    public function create(BodyParamsInterface $params): string
    {
        return json_encode($params->params());
    }
}
