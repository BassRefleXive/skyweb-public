<?php

namespace AppBundle\Slack\Services;

use AppBundle\Entity\Application\Payments\Order;
use AppBundle\Enum\Application\PaymentSystem;
use AppBundle\Slack\Model\PaymentNotification;

class PaymentNotificationService extends NotificationService
{
    public function paymentStarted(PaymentSystem $paymentSystem, Order $order)
    {
        $this->send(PaymentNotification::started($paymentSystem, $order));
    }

    public function paymentSuccess(PaymentSystem $paymentSystem, Order $order)
    {
        $this->send(PaymentNotification::success($paymentSystem, $order));
    }

    public function paymentFailure(PaymentSystem $paymentSystem, Order $order)
    {
        $this->send(PaymentNotification::failure($paymentSystem, $order));
    }

    public function paymentPending(PaymentSystem $paymentSystem, Order $order)
    {
        $this->send(PaymentNotification::pending($paymentSystem, $order));
    }

    public function paymentCredited(PaymentSystem $paymentSystem, Order $order)
    {
        $this->send(PaymentNotification::credited($paymentSystem, $order));
    }

    public function interactionError(PaymentSystem $paymentSystem, \Throwable $e)
    {
        $this->send(PaymentNotification::interactionError($paymentSystem, $e));
    }

    public function initializationError(PaymentSystem $paymentSystem, \Throwable $e)
    {
        $this->send(PaymentNotification::paymentInitializationError($paymentSystem, $e));
    }

    public function finalizationError(PaymentSystem $paymentSystem, \Throwable $e)
    {
        $this->send(PaymentNotification::paymentFinalizationError($paymentSystem, $e));
    }
}