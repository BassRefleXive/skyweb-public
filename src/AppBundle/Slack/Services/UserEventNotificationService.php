<?php


namespace AppBundle\Slack\Services;


use AppBundle\Entity\Application\Server;
use AppBundle\Entity\Application\User\User;
use AppBundle\Model\Application\ContactRequestModel;
use AppBundle\Slack\Model\UserEventNotification;

class UserEventNotificationService extends NotificationService
{
    public function registered(User $user)
    {
        $this->send(UserEventNotification::registered($user));
    }

    public function contactRequestSucceeded(ContactRequestModel $model)
    {
        $this->send(UserEventNotification::contactRequestSucceeded($model));
    }

    public function contactRequestFailed(ContactRequestModel $model)
    {
        $this->send(UserEventNotification::contactRequestFailed($model));
    }

    public function serverAccountCreationFailed(User $user, Server $server)
    {
        $this->send(UserEventNotification::serverAccountCreationFailed($user, $server));
    }
}