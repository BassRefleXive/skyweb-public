<?php


namespace AppBundle\Slack\Services;


use AppBundle\Slack\Exception\SlackException;
use AppBundle\Slack\Model\NotificationInterface;
use CL\Slack\Model\Attachment;
use CL\Slack\Payload\ChatPostMessagePayload;
use CL\Slack\Transport\ApiClient;
use Psr\Log\LoggerInterface;
use CL\Slack\Exception\SlackException as NativeSlackException;

abstract class NotificationService
{
    private $client;
    private $channel;
    private $botNickname;
    private $logger;

    public function __construct(ApiClient $client, string $channel, string $botNickname, LoggerInterface $logger)
    {
        $this->client = $client;
        $this->channel = $channel;
        $this->botNickname = $botNickname;
        $this->logger = $logger;
    }

    protected function send(NotificationInterface $notification)
    {
        try {
            $payload = new ChatPostMessagePayload();

            $payload->setChannel($this->channel);
            $payload->setText($notification->title());
            $payload->setUsername($this->botNickname);

            $attachment = new Attachment();
            $attachment->setColor($notification->color());
            $attachment->setText($notification->text());

            $payload->addAttachment($attachment);

            $response = $this->client->send($payload);

            if (!$response->isOk()) {
                throw SlackException::failedPostMessage($notification->type(), $this->channel, $response->getErrorExplanation());
            }
        } catch (NativeSlackException $e) {
            $this->logger->critical(
                sprintf('Failed to execute Slack API call. Exception: "%s".', $e->getMessage()),
                $e->getTrace()
            );
        }
    }
}