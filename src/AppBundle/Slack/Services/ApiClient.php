<?php


namespace AppBundle\Slack\Services;


use GuzzleHttp\Client;

class ApiClient extends \CL\Slack\Transport\ApiClient
{
    public function __construct($token = null, $client = null, $eventDispatcher = null)
    {
        if (null === $client) {
            $client = new Client([
                'connect_timeout' => 2,
            ]);
        }

        parent::__construct($token, $client, $eventDispatcher);
    }
}