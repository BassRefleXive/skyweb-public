<?php

namespace AppBundle\Slack\Services;

use AppBundle\Slack\Model\DupeNotification;

class DupeNotificationService extends NotificationService
{
    public function checkFinishedWithoutIssues()
    {
        $this->send(DupeNotification::clean());
    }

    public function duplicatedItemsFound(array $items)
    {
        $this->send(DupeNotification::detected($items));
    }
}