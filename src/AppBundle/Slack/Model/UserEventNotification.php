<?php


namespace AppBundle\Slack\Model;


use AppBundle\Entity\Application\Server;
use AppBundle\Entity\Application\User\User;
use AppBundle\Model\Application\ContactRequestModel;

final class UserEventNotification extends Notification
{
    protected function __construct(string $text, string $color)
    {
        $this->title = 'User Event!';

        parent::__construct($text, $color);
    }

    public static function registered(User $user): self
    {
        return new self(
            sprintf(
                "User Registered!\n\nUser ID: #%d;",
                $user->getId()
            ),
            'good'
        );
    }

    public static function contactRequestSucceeded(ContactRequestModel $model): self
    {
        return new self(
            sprintf(
                "Contact request succeeded!\n\nSubject: %s\nEmail: %s\nText: %s",
                $model->getSubject(),
                $model->getEmail(),
                $model->getText()
            ),
            'good'
        );
    }

    public static function contactRequestFailed(ContactRequestModel $model): self
    {
        return new self(
            sprintf(
                "Contact request failed!\n\nSubject: %s\nEmail: %s\nText: %s",
                $model->getSubject(),
                $model->getEmail(),
                $model->getText()
            ),
            'danger'
        );
    }

    public static function serverAccountCreationFailed(User $user, Server $server): self
    {
        return new self(
            sprintf(
                "Server Account Creation Failed!\n\nUser ID: #%d;\nEmail: %s;\nServer: %s;",
                $user->getId(),
                $user->email(),
                $server->getName()
            ),
            'danger'
        );
    }
}