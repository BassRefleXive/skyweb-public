<?php


namespace AppBundle\Slack\Model;


abstract class Notification implements NotificationInterface
{
    protected $title;
    private $text;
    private $color;

    protected function __construct(string $text, string $color)
    {
        $this->text = $text;
        $this->color = $color;
    }

    public function text(): string
    {
        return $this->text;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function color(): string
    {
        return $this->color;
    }

    public function type(): string
    {
        return static::class;
    }
}