<?php

namespace AppBundle\Slack\Model;

use AppBundle\Entity\Application\Payments\Order;
use AppBundle\Enum\Application\PaymentSystem;

final class PaymentNotification extends Notification
{
    protected function __construct(string $text, string $color)
    {
        $this->title = 'New Payment Event!';

        parent::__construct($text, $color);
    }

    public static function started(PaymentSystem $paymentSystem, Order $order): self
    {
        return new self(
            sprintf(
                'PaymentSystem: %s. New payment has been started. Order #%d, amount: %.2f %s.',
                $paymentSystem->getName(),
                $order->id(),
                $order->amount(),
                $order->currency()
            ),
            '#439FE0'
        );
    }

    public static function success(PaymentSystem $paymentSystem, Order $order): self
    {
        return new self(
            sprintf(
                'PaymentSystem: %s. Payment of order with id #%d and amount %.2f %s succeeded.',
                $paymentSystem->getName(),
                $order->id(),
                $order->amount(),
                $order->currency()
            ),
            'good'
        );
    }

    public static function failure(PaymentSystem $paymentSystem, Order $order): self
    {
        return new self(
            sprintf(
                'PaymentSystem: %s. Payment of order with id #%d and amount %.2f %s failed. User email: %s.',
                $paymentSystem->getName(),
                $order->id(),
                $order->amount(),
                $order->currency(),
                $order->getUserServerAccount()->getUser()->email()
            ),
            'danger'
        );
    }

    public static function pending(PaymentSystem $paymentSystem, Order $order): self
    {
        return new self(
            sprintf(
                'PaymentSystem: %s. Payment of order with id #%d and amount %.2f %s pending.',
                $paymentSystem->getName(),
                $order->id(),
                $order->amount(),
                $order->currency()
            ),
            'warning'
        );
    }

    public static function credited(PaymentSystem $paymentSystem, Order $order): self
    {
        return new self(
            sprintf(
                'PaymentSystem: %s. Payment of order with id #%d and amount %.2f %s credited. Payment system: %s, amount %.2f. Amount added to cart: %.2f.',
                $paymentSystem->getName(),
                $order->id(),
                $order->amount(),
                $order->currency(),
                $order->method(),
                $order->psAmount(),
                $order->cartAmount()
            ),
            '#5f42f4'
        );
    }

    public static function interactionError(PaymentSystem $paymentSystem, \Throwable $e): self
    {
        return new self(
            sprintf(
                'PaymentSystem: %s. Payment interaction error: %s; Trace: %s',
                $paymentSystem->getName(),
                $e->getMessage(),
                $e->getTraceAsString()
            ),
            'danger'
        );
    }

    public static function paymentInitializationError(PaymentSystem $paymentSystem, \Throwable $e): self
    {
        return new self(
            sprintf(
                'PaymentSystem: %s. Payment initialization error: %s; Trace: %s',
                $paymentSystem->getName(),
                $e->getMessage(),
                $e->getTraceAsString()
            ),
            'danger'
        );
    }

    public static function paymentFinalizationError(PaymentSystem $paymentSystem, \Throwable $e): self
    {
        return new self(
            sprintf(
                'PaymentSystem: %s. Payment finalization error: %s; Trace: %s',
                $paymentSystem->getName(),
                $e->getMessage(),
                $e->getTraceAsString()
            ),
            'danger'
        );
    }
}