<?php


namespace AppBundle\Slack\Model;


interface NotificationInterface
{
    public function text(): string;

    public function title(): string;

    public function color(): string;

    public function type(): string;
}