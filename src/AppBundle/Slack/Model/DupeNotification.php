<?php


namespace AppBundle\Slack\Model;


class DupeNotification extends Notification
{
    protected function __construct(string $text, string $color)
    {
        $this->title = 'Dupe Check Finished!';

        parent::__construct($text, $color);
    }

    public static function clean(): self
    {
        return new self('No suspicious items found!', 'good');
    }

    public static function detected(array $items): self
    {
        return new self(implode(PHP_EOL, $items), 'danger');
    }
}