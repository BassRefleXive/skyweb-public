<?php

namespace AppBundle\Slack\Exception;

class SlackException extends \RuntimeException
{
    public static function failedPostMessage(string $type, string $channel, string $reason): self
    {
        return new self(
            sprintf(
                'Failed to post message of type "%s" in channel "%s" with reason: "%s".',
                $type,
                $channel,
                $reason
            )
        );
    }
}