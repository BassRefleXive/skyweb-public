<?php

declare(strict_types = 1);

namespace AppBundle\EventListener;


use AppBundle\Entity\Application\User\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class LoggerContextListener
{
    private $client;
    private $tokenStorage;

    public function __construct(\Raven_Client $client, TokenStorageInterface $tokenStorage)
    {
        $this->client = $client;
        $this->tokenStorage = $tokenStorage;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $this->populateLoggerContext($event->getRequest());
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $this->populateLoggerContext($event->getRequest());
    }

    private function populateLoggerContext(Request $request)
    {
        $this->client->extra_context([
            'Request' => [
                'IPs'        => $request->getClientIps(),
                'Content'    => $request->getContent(),
                'Method'     => $request->getMethod(),
                'RealMethod' => $request->getRealMethod(),
            ],
        ]);

        if (!$user = $this->getUser()) {
            return null;
        }

        $userServerAccount = $user->getLastUserServerAccount();

        $this->client->user_context(
            array_merge(
                [
                    'ID'       => $user->getId(),
                    'Username' => $user->getUsername(),
                ],
                null !== $userServerAccount
                    ? [
                    'User Server Account ID' => $userServerAccount->getId(),
                    'Selected Server ID'     => $userServerAccount->getServer()->getId(),
                    'Selected Server Name'   => $userServerAccount->getServer()->getName(),
                ]
                    : []
            )
        );
    }

    /**
     * @return User|null
     */
    private function getUser()
    {
        if (null === $token = $this->tokenStorage->getToken()) {
            return null;
        }

        if (!is_object($user = $token->getUser())) {
            return null;
        }

        return $user;
    }
}