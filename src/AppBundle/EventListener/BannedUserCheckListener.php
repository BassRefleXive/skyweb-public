<?php

namespace AppBundle\EventListener;


use AppBundle\Entity\Application\User\User;
use AppBundle\Exception\Application\DisplayableException;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class BannedUserCheckListener
{
    protected $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if (null !== $token = $this->tokenStorage->getToken()) {
            $user = $token->getUser();

            if ($user instanceof User && $user->isBanned()) {
                $this->tokenStorage->setToken(null);
                $event->getRequest()->getSession()->invalidate();

                throw new DisplayableException('errors.51');
            }
        }
    }

}