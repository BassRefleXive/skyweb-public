<?php

namespace AppBundle\EventListener;


use AppBundle\Doctrine\Filters\ActiveEntityFilter;
use AppBundle\Doctrine\Filters\ActiveServerFilter;
use AppBundle\Doctrine\Filters\SoftDeletedEntityFilter;
use AppBundle\Entity\Application\User\Admin;
use AppBundle\ServerManager\Connection\Configuration;
use AppBundle\ServerManager\Connection\Manager;
use AppBundle\Service\Application\Cache\Provider\DoctrineCacheProvider;
use AppBundle\Entity\Application\Server;
use AppBundle\Service\Application\ServerService;
use Doctrine\Bundle\DoctrineBundle\ConnectionFactory;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Doctrine\Common\EventManager;
use Doctrine\DBAL\Configuration as DBALConfiguration;
use Doctrine\ORM\Configuration as ORMConfiguration;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class KernelRequestListener
{
    const COMMANDS_TO_SKIP_DATABASE_INITIALIZATION = [
        'doctrine:migrations:migrate',
    ];

    private $container;
    private $connectionFactory;
    private $em;
    private $serverService;
    private $cacheProvider;
    private $tokenStorage;

    public function __construct(
        ContainerInterface $container,
        EntityManager $em,
        ConnectionFactory $connectionFactory,
        ServerService $serverService,
        DoctrineCacheProvider $cacheProvider,
        TokenStorageInterface $tokenStorage
    )
    {
        $this->container = $container;
        $this->connectionFactory = $connectionFactory;
        $this->em = $em;
        $this->serverService = $serverService;
        $this->cacheProvider = $cacheProvider;
        $this->tokenStorage = $tokenStorage;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $this->setResultCacheDriver($this->em, 'default');
        $this->setFilters($this->em->getConfiguration());
        $this->activateFilters($this->em);

        $this->createDatabaseConnections();
        $this->fillServerManagerRegistry();
    }

    public function onConsoleCommand(ConsoleCommandEvent $event)
    {
        if (in_array($event->getCommand()->getName(), self::COMMANDS_TO_SKIP_DATABASE_INITIALIZATION, true)) {
            return;
        }

        $this->setResultCacheDriver($this->em, 'default');
        $this->setFilters($this->em->getConfiguration());
        $this->activateFilters($this->em);

        $this->createDatabaseConnections();
        $this->fillServerManagerRegistry();
    }

    private function fillServerManagerRegistry(): void
    {
        /**
         * Temporally disabled because server manager was used only for coins management. Coins management
         * moved to database.
         * Potentially functionality could be used to send requests to GS to disconnect accounts as JS do it.
         * @ToDo Needs to be investigated how to do that.
         */
//        $registry = $this->container->get('sky_app.app.server_manager.registry.manager');
//        $logger = $this->container->get('monolog.logger.server.manager.core');
//
//        /** @var Server $server */
//        foreach ($this->serverService->getServerForConnection() as $server) {
//            $configuration = new Configuration(
//                $server->getDataServer()->getIp(),
//                $server->getDataServer()->getPort(),
//                $server->getDataServer()->getTimeout()
//            );
//
//            $connection = new \AppBundle\ServerManager\Connection\Connection($configuration, $logger);
//
//            $registry->add($server, new Manager($connection, $logger));
//        }
    }

    private function createDatabaseConnections()
    {
        /** @var Server $server */
        foreach ($this->serverService->getServerForConnection() as $server) {
            $DBALConfiguration = $this->em->getConnection()->getConfiguration();
            $ORMConfiguration = clone $this->em->getConfiguration();
            $eventManager = $this->em->getEventManager();

            $connection = $this->createDatabaseConnection($server, $DBALConfiguration, $eventManager);
            $entityManager = $this->createEntityManager($connection, $ORMConfiguration, $eventManager);
            $this->setResultCacheDriver($entityManager, $server->getHash());
            $this->setManagerToContainer($server, $entityManager);
        }
    }

    private function setManagerToContainer(Server $server, EntityManager $entityManager)
    {
        $this->container->set($server->getHash(), $entityManager);
    }

    private function createEntityManager(Connection $connection, ORMConfiguration $configuration, EventManager $eventManager): EntityManager
    {
        return EntityManager::create($connection, $configuration, $eventManager);
    }

    private function createDatabaseConnection(Server $server, DBALConfiguration $configuration, EventManager $eventManager): Connection
    {
        $credential = $server->getDatabase();

        return $this->connectionFactory->createConnection(
            [
                'driver'              => $credential->getDriver(),
                'charset'             => $credential->getCharset(),
                'host'                => $credential->getHost(),
                'port'                => $credential->getPort(),
                'user'                => $credential->getUser(),
                'password'            => $credential->getPassword(),
                'defaultTableOptions' => [],
            ],
            $configuration,
            $eventManager
        );
    }

    private function setFilters(ORMConfiguration $configuration)
    {
        $configuration->addFilter(ActiveEntityFilter::NAME, ActiveEntityFilter::class);
        $configuration->addFilter(SoftDeletedEntityFilter::NAME, SoftDeletedEntityFilter::class);
        $configuration->addFilter(ActiveServerFilter::NAME, ActiveServerFilter::class);
    }

    private function activateFilters(EntityManager $em): void
    {
        $this->em->getFilters()->enable(SoftDeletedEntityFilter::NAME);

        $hideInactiveServer = true;
        if (null !== $token = $this->tokenStorage->getToken()) {
            if ($token->getUser() instanceof Admin) {
                $hideInactiveServer = false;
            }
        }

        $hideInactiveServer && $this->em->getFilters()->enable(ActiveServerFilter::NAME);
    }

    private function setResultCacheDriver(EntityManager $entityManager, string $namespacePrefix)
    {
        $entityManager
            ->getConfiguration()
            ->setResultCacheImpl($this->cacheProvider->getProvider($namespacePrefix));
    }
}