<?php

namespace AppBundle\EventListener;


use AppBundle\Entity\Application\User\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class ReferralSystemListener
{

    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        $request = $event->getRequest();
        $response = $event->getResponse();

        if (!$request->cookies->has('referredBy') && $request->query->has('referrer')) {

            $user = $this->em->getRepository(User::class)->findOneBy(['displayName' => $request->query->get('referrer')]);

            if ($user) {
                $response->headers->setCookie(new Cookie('referredBy', $user->getId()));
            }

        }

    }

}