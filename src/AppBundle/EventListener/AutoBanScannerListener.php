<?php

declare(strict_types=1);

namespace AppBundle\EventListener;


use AppBundle\Cloudflare\Enum\AccessRule\Mode;
use AppBundle\Cloudflare\Enum\AccessRule\TargetType;
use AppBundle\Cloudflare\Service\AccessRuleManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class AutoBanScannerListener
{
    private $accessRuleManager;
    private $file;
    private $paths;

    public function __construct(AccessRuleManager $accessRuleManager, string $file, array $paths)
    {
        $this->accessRuleManager = $accessRuleManager;
        $this->file = $file;
        $this->paths = $paths;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if (!$exception instanceof HttpExceptionInterface) {
            return;
        }

        if (!is_file($this->file)) {
            throw new \RuntimeException(sprintf('%s file must exists and should be writable.', $this->file));
        }

        $request = $event->getRequest();

        $url = $request->getPathInfo();

        if ($exception instanceof MethodNotAllowedHttpException) {
            $this->recordBan(
                $request->getClientIp(),
                sprintf('MethodNotAllowed: %s %s', $request->getMethod(), $request->getPathInfo())
            );
        }

        if ($exception->getStatusCode() !== Response::HTTP_NOT_FOUND) {
            return;
        }

        $bannedScanners = file_get_contents($this->file);

        foreach ($this->paths as $path) {
            $path = strtolower(trim($path));
            if ($path && false !== stripos($url, $path) && false === stripos($bannedScanners, $request->getClientIp())) {
                $this->recordBan($request->getClientIp(), $path);
                break;
            }
        }
    }

    private function recordBan(string $ip, string $reason)
    {
        file_put_contents(
            $this->file,
            sprintf(
                "[%s] %s %s\n",
                (new \DateTimeImmutable())->format('Y-m-d H:i:s'),
                $ip,
                $reason
            ),
            FILE_APPEND
        );

        $this->accessRuleManager->create(
            Mode::byValue(Mode::BLOCK),
            TargetType::byValue(TargetType::IP),
            $ip,
            sprintf('%s (%s)', $reason, (new \DateTimeImmutable())->format('Y-m-d H:i:s'))
        );
    }
}