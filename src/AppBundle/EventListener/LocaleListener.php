<?php

namespace AppBundle\EventListener;


use AppBundle\Location\Discoverer\Exception\DiscovererExceptionInterface;
use AppBundle\Location\Discoverer\LocationDiscoverer;
use AppBundle\Service\Application\SessionService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class LocaleListener implements EventSubscriberInterface
{
    private $locationDiscoverer;
    private $sessionService;
    private $defaultLocale;
    private $env;

    public function __construct(LocationDiscoverer $locationDiscoverer, SessionService $sessionService, string $defaultLocale, string $env)
    {
        $this->locationDiscoverer = $locationDiscoverer;
        $this->sessionService = $sessionService;
        $this->defaultLocale = $defaultLocale;
        $this->env = $env;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        if (!$request->hasPreviousSession()) {
            return;
        }

        if (null === $locale = $this->sessionService->getLocale()) {
            $locale = $this->defaultLocale;

            $ip = $request->getClientIp();

            if ('prod' === $this->env && null !== $ip) {
                try {
                    $locale = $this->locationDiscoverer->country($ip)->locale();
                } catch (DiscovererExceptionInterface $e) {}
            }
        }

        $this->sessionService->setLocale($locale);
        $request->setLocale($request->getSession()->get('_locale', $locale));
    }

    public static function getSubscribedEvents()
    {
        return [
            // must be registered after the default Locale listener
            KernelEvents::REQUEST => [['onKernelRequest', 15]],
        ];
    }
}