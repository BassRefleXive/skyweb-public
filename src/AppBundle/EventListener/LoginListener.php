<?php

namespace AppBundle\EventListener;


use AppBundle\Entity\Application\User\User;
use AppBundle\Location\Discoverer\Exception\DiscovererExceptionInterface;
use AppBundle\Location\Discoverer\LocationDiscoverer;
use AppBundle\Repository\Application\UserRepository;
use AppBundle\Service\Application\SessionService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\Event\SwitchUserEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class LoginListener implements EventSubscriberInterface
{
    private $session;
    private $sessionService;
    private $locationDiscoverer;
    private $userRepository;

    public function __construct(
        Session $session,
        SessionService $sessionService,
        LocationDiscoverer $locationDiscoverer,
        UserRepository $userRepository
    )
    {
        $this->session = $session;
        $this->sessionService = $sessionService;
        $this->locationDiscoverer = $locationDiscoverer;
        $this->userRepository = $userRepository;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            SecurityEvents::INTERACTIVE_LOGIN => 'onInteractiveLogin',
            SecurityEvents::SWITCH_USER => 'onSwitchUser',
        ];
    }

    public function onInteractiveLogin(InteractiveLoginEvent $event): void
    {
        /** @var User $user */
        $user = $event->getAuthenticationToken()->getUser();

        $this->sessionService->setLocale($user->getLocale());

        if ($lastUserServerAccount = $user->getLastUserServerAccount()) {
            $this->sessionService->setServerHash($lastUserServerAccount->getServer());
        }

        if (null === $user->country()) {
            if (null !== $ip = $event->getRequest()->getClientIp()) {
                try {
                    $user->fromCountry($this->locationDiscoverer->country($ip));
                    $this->userRepository->save($user);
                } catch (DiscovererExceptionInterface $e) {
                }
            }
        }
    }

    public function onSwitchUser(SwitchUserEvent $event): void
    {
        $this->sessionService->purgeLocale();
    }
}