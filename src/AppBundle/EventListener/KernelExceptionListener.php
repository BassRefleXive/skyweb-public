<?php

namespace AppBundle\EventListener;


use AppBundle\Controller\Helper\RedirectBackTrait;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Exception\Application\NeedAccountException;
use AppBundle\Exception\Application\RedirectBackException;
use AppBundle\Exception\Application\RequireLoginException;
use AppBundle\ServerManager\Connection\Exception\ServerManagerExceptionInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class KernelExceptionListener
{
    use RedirectBackTrait;

    private $logger;
    private $urlGenerator;
    private $session;
    private $kernel;
    private $twig;
    private $scannerPaths;

    public function __construct(
        LoggerInterface $logger,
        UrlGeneratorInterface $urlGenerator,
        Session $session,
        KernelInterface $kernel,
        TwigEngine $twig,
        array $scannerPaths
    )
    {
        $this->logger = $logger;
        $this->urlGenerator = $urlGenerator;
        $this->session = $session;
        $this->kernel = $kernel;
        $this->twig = $twig;
        $this->scannerPaths = $scannerPaths;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        $response = $this->processException($exception, $event->getRequest());

        if ($response) {
            $event->setResponse($response);
        }
    }

    private function processException(\Throwable $exception, Request $request)
    {
        $result = null;

        try {
            throw $exception;
        } catch (AccessDeniedException $e) {
            $result = new RedirectResponse($this->urlGenerator->generate('sky_users_login'));
        } catch (RedirectBackException $e) {
            $result = $this->buildRedirectBackResponse($request, $this->urlGenerator->generate('sky_accounts_index'));
            $this->addFlash('error', $e->getMessage());
        } catch (RequireLoginException $e) {
            $result = new RedirectResponse($this->urlGenerator->generate('sky_users_login'));
            $this->addFlash('error', $e->getMessage());
        } catch (NeedAccountException $e) {
            $result = new RedirectResponse($this->urlGenerator->generate('sky_accounts_index'));
            $this->addFlash('error', $e->getMessage());
        } catch (ServerManagerExceptionInterface $e) {
            $result = new RedirectResponse($this->urlGenerator->generate('sky_accounts_index'));
            $this->addFlash('error', 'errors.48');
        } catch (DisplayableException $e) {
            $result = new RedirectResponse($this->urlGenerator->generate('sky_index_index'));
            $this->addFlash('error', $e->getMessage());
        } catch (\Throwable $e) {
            $result = $this->processUnhandledException($e, $request);
        }

        return $result;
    }

    private function processUnhandledException(\Throwable $e, Request $request): Response
    {
        if ($this->kernel->getEnvironment() !== 'prod') {
            $this->logUnhandledException($e);

            throw $e;
        }

        $response = new Response();

        if ($e instanceof HttpExceptionInterface) {
            $response->setStatusCode($e->getStatusCode());
            $response->headers->replace($e->getHeaders());
        } else {
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        switch ($response->getStatusCode()) {
            case 404: {
                if (preg_match('/-GRAN/', $request->getPathInfo())) {
                    return new RedirectResponse($this->urlGenerator->generate('sky_index_index'));
                }

                $this->logPageNotFoundException($e);
                $response->setContent($this->twig->render('@App/error/404.html.twig', ['isError' => true]));
                break;
            }
            default: {
                $this->logUnhandledException($e);
                $response->setContent($this->twig->render('@App/error/500.html.twig', ['isError' => true]));
            }
        }

        return $response;
    }

    private function logPageNotFoundException(\Throwable $e)
    {
        $loweredError = strtolower($e->getMessage());

        foreach ($this->scannerPaths as $path) {
            $path = strtolower(trim($path));
            if ($path && false !== stripos($loweredError, $path)) {
                return;
            }
        }

        $this->logger->error(
            'Page hot found exception: {error}',
            [
                'exception' => get_class($e),
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]
        );
    }

    private function logUnhandledException(\Throwable $e)
    {
        $this->logger->error(
            'Unhandled general exception {exception}: {error}',
            [
                'exception' => get_class($e),
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]
        );
    }

    protected function addFlash($type, $message)
    {
        $this->session->getFlashBag()->add($type, $message);
    }

}