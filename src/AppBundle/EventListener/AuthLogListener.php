<?php


namespace AppBundle\EventListener;

use AppBundle\Entity\Application\User\Builder\AuthLogBuilder;
use AppBundle\Repository\Application\AuthLogRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class AuthLogListener implements EventSubscriberInterface
{
    private $authLogRepository;
    private $requestStack;

    public function __construct(AuthLogRepository $authLogRepository, RequestStack $requestStack)
    {
        $this->authLogRepository = $authLogRepository;
        $this->requestStack = $requestStack;
    }

    public static function getSubscribedEvents()
    {
        return [
            SecurityEvents::INTERACTIVE_LOGIN => 'onAuthenticationSuccess',
            AuthenticationEvents::AUTHENTICATION_FAILURE => 'onAuthenticationFailure',
        ];
    }

    public function onAuthenticationSuccess(InteractiveLoginEvent $event): void
    {
        $token = $event->getAuthenticationToken();

        $this->authLogRepository->save(
            $this->preparedAuthLogBuilder($token->getUser()->getUsername())
                ->withPassword($token->getUser()->getPassword())
                ->markSuccessful()
                ->build()
        );
    }

    public function onAuthenticationFailure(AuthenticationFailureEvent $event): void
    {
        $token = $event->getAuthenticationToken();

        $this->authLogRepository->save(
            $this->preparedAuthLogBuilder($token->getUsername())
                ->withPassword($token->getCredentials())
                ->markFailed()
                ->build()
        );
    }

    private function preparedAuthLogBuilder(string $login): AuthLogBuilder
    {
        $currentRequest = $this->currentRequest();

        return (new AuthLogBuilder())
            ->withUserAgent($currentRequest->headers->get('User-Agent'))
            ->withIp($currentRequest->getClientIp())
            ->withLogin($login);
    }

    private function currentRequest(): Request
    {
        return $this->requestStack->getCurrentRequest();
    }
}