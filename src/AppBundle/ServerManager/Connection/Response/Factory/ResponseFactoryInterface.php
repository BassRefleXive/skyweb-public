<?php

namespace AppBundle\ServerManager\Connection\Response\Factory;

use AppBundle\ServerManager\Connection\ResponseInterface;

interface ResponseFactoryInterface
{
    public function build(string $payload): ResponseInterface;
}