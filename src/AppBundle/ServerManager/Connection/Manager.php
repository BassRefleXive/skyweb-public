<?php


namespace AppBundle\ServerManager\Connection;


use Psr\Log\LoggerInterface;

class Manager
{
    private $connection;
    private $fundsManager;
    private $logger;

    public function __construct(Connection $connection, LoggerInterface $logger)
    {
        $this->connection = $connection;
        $this->logger = $logger;
    }

    public function connection(): Connection
    {
        return $this->connection;
    }
}