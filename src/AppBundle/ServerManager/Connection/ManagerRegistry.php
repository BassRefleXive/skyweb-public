<?php


namespace AppBundle\ServerManager\Connection;


use AppBundle\Entity\Application\Server;
use AppBundle\ServerManager\Connection\Exception\RegistryException;

class ManagerRegistry
{
    private $managers;

    public function __construct(array $managers = [])
    {
        $this->managers = $managers;
    }

    public function add(Server $server, Manager $manager): void
    {
        if (isset($this->managers[$server->getHash()])) {
            throw RegistryException::createDuplicateEntryError($server);
        }

        $this->managers[$server->getHash()] = $manager;
    }

    public function get(Server $server): Manager
    {
        if (!isset($this->managers[$server->getHash()])) {
            throw RegistryException::createNotFoundEntryException($server);
        }

        return $this->managers[$server->getHash()];
    }
}