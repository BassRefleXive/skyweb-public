<?php

namespace AppBundle\ServerManager\Connection\Request\Factory;

use AppBundle\ServerManager\Connection\Command\CommandInterface;
use AppBundle\ServerManager\Connection\RequestInterface;

interface RequestFactoryInterface
{
    public function build(CommandInterface $command): RequestInterface;
}