<?php

namespace AppBundle\ServerManager\Connection;

interface RequestInterface
{
    public function payload(): string;

    public function responseLength(): int;
}