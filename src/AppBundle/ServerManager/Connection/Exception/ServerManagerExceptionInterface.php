<?php


namespace AppBundle\ServerManager\Connection\Exception;


interface ServerManagerExceptionInterface extends \Throwable
{
}