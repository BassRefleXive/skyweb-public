<?php

namespace AppBundle\ServerManager\Connection\Exception;

class ConnectionException extends \RuntimeException implements ServerManagerExceptionInterface
{
    public static function createSocketCreateError(): self
    {
        return new self('Failed to create new socket.');
    }

    public static function createSocketSetTimeoutError(): self
    {
        return new self('Failed to set connection timeout to socket.');
    }

    public static function createSocketConnectionError(string $ip, int $port): self
    {
        return new self(
            sprintf(
                'Failed to create socket connection with %s:%d.',
                $ip,
                $port
            )
        );
    }
}