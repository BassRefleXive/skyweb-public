<?php

namespace AppBundle\ServerManager\Connection\Exception;

use Exception;

class InteractionException extends \RuntimeException implements ServerManagerExceptionInterface
{
    private $socket;

    public function __construct($socket, $message = "", $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->socket = $socket;
    }

    public static function createSocketWriteError(&$socket, int $code, string $message): self
    {
        return new self(
            $socket,
            sprintf(
                'Failed to write to socket. Code: #%d, message: %s.',
                $code,
                $message
            )
        );
    }

    public static function createSocketReadError(&$socket, int $code, string $message): self
    {
        return new self(
            $socket,
            sprintf(
                'Failed to read from socket. Code: #%d, message: %s.',
                $code,
                $message
            )
        );
    }

    public function socket()
    {
        return $this->socket;
    }
}