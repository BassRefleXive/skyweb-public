<?php


namespace AppBundle\ServerManager\Connection\Exception;


use AppBundle\Entity\Application\Server;

class RegistryException extends \LogicException implements ServerManagerExceptionInterface
{
    public static function createDuplicateEntryError(Server $server): self
    {
        return new self(
            sprintf(
                'Failed to add manager to registry. Manager for server #%d with hash "%s" already exists in registry.',
                $server->getId(),
                $server->getHash()
            )
        );
    }

    public static function createNotFoundEntryException(Server $server): self
    {
        return new self(
            sprintf(
                'Failed to retrieve manager from registry. Manager for server #%d with hash "%s" no found in registry.',
                $server->getId(),
                $server->getHash()
            )
        );
    }
}