<?php


namespace AppBundle\ServerManager\Connection;


use AppBundle\ServerManager\Connection\Driver\DriverInterface;
use AppBundle\ServerManager\Connection\Driver\Socket;
use AppBundle\ServerManager\Connection\Response\Factory\ResponseFactoryInterface;
use Psr\Log\LoggerInterface;

class Connection
{
    private $driver;

    public function __construct(Configuration $configuration, LoggerInterface $logger)
    {
        $this->driver = new Socket($configuration, $logger);
    }

    public function driver(): DriverInterface
    {
        return $this->driver;
    }

    public function execute(RequestInterface $request, ResponseFactoryInterface $responseFactory): ResponseInterface
    {
        return $this->driver->execute($request, $responseFactory);
    }
}