<?php


namespace AppBundle\ServerManager\Connection;

class Configuration
{
    private $ip;
    private $port;
    private $timeout;

    public function __construct(string $ip, int $port, int $timeout)
    {
        $this->ip = $ip;
        $this->port = $port;
        $this->timeout = $timeout;
    }

    public function ip(): string
    {
        return $this->ip;
    }

    public function port(): int
    {
        return $this->port;
    }

    public function timeout(): int
    {
        return $this->timeout;
    }
}