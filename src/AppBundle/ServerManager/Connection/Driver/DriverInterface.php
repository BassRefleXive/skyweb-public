<?php


namespace AppBundle\ServerManager\Connection\Driver;


use AppBundle\ServerManager\Connection\RequestInterface;
use AppBundle\ServerManager\Connection\Response\Factory\ResponseFactoryInterface;
use AppBundle\ServerManager\Connection\ResponseInterface;

interface DriverInterface
{
    public function execute(RequestInterface $request, ResponseFactoryInterface $responseFactory): ResponseInterface;
}