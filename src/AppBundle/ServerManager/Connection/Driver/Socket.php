<?php

namespace AppBundle\ServerManager\Connection\Driver;


use AppBundle\ServerManager\Connection\Configuration;
use AppBundle\ServerManager\Connection\Exception\ConnectionException;
use AppBundle\ServerManager\Connection\Exception\InteractionException;
use AppBundle\ServerManager\Connection\RequestInterface;
use AppBundle\ServerManager\Connection\Response\Factory\ResponseFactoryInterface;
use AppBundle\ServerManager\Connection\ResponseInterface;
use Psr\Log\LoggerInterface;

class Socket implements DriverInterface
{
    private $configuration;
    private $logger;

    public function __construct(Configuration $configuration, LoggerInterface $logger)
    {
        $this->configuration = $configuration;
        $this->logger = $logger;
    }

    public function execute(RequestInterface $request, ResponseFactoryInterface $responseFactory): ResponseInterface
    {
        try {
            $socket = $this->createConnection();

            $this->write($socket, $request->payload());

            $payload = $this->read($socket, $request->responseLength());

            $this->close($socket);

            return $responseFactory->build($payload);
        } catch (InteractionException $e) {
            $this->logger->error(sprintf('Driver::Socket::InteractionException: Message: "%s"; Code: #%d.', $e->getMessage(), $e->getCode()));
            $this->close($e->socket());

            throw $e;
        } catch (ConnectionException $e) {
            $this->logger->error(sprintf('Driver::Socket::ConnectionException: Message: "%s"; Code: #%d.', $e->getMessage(), $e->getCode()));

            throw $e;
        }
    }

    /**
     * @return resource
     */
    private function createConnection()
    {
        $socket = @socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

        if (false === $socket) {
            throw ConnectionException::createSocketCreateError();
        }

        if (false === @socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, ['sec' => $this->configuration->timeout(), 'usec' => 0])) {
            throw ConnectionException::createSocketSetTimeoutError();
        }

        if (false === @socket_connect($socket, $this->configuration->ip(), $this->configuration->port())) {
            throw ConnectionException::createSocketConnectionError($this->configuration->ip(), $this->configuration->port());
        }

        return $socket;
    }

    private function write($socket, string $buffer): void
    {
        $length = strlen($buffer);

        while (true) {
            if (false === $sent = @socket_write($socket, $buffer, strlen($buffer))) {
                $code = socket_last_error($socket);
                $message = socket_strerror($code);

                throw InteractionException::createSocketWriteError($socket, $code, $message);
            }

            if ($sent < $length) {
                $sent = substr($buffer, $sent);
                $length -= $sent;
            } else {
                break;
            }
        }
    }

    private function read($socket, int $length): string
    {
        if (false === $received = @socket_read($socket, $length)) {
            $code = socket_last_error($socket);
            $message = socket_strerror($code);

            throw InteractionException::createSocketReadError($socket, $code, $message);
        }

        return $received;
    }

    private function close($socket): void
    {
        socket_close($socket);
    }
}