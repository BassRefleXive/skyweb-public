<?php

declare(strict_types = 1);

namespace AppBundle\BonusCode\Service;


use AppBundle\BonusCode\Model\BonusCode;
use AppBundle\BonusCode\Repository\BonusCodeRepository;

class StatusToggler
{

    private $bonusCodeRepository;

    public function __construct(BonusCodeRepository $bonusCodeRepository)
    {
        $this->bonusCodeRepository = $bonusCodeRepository;
    }

    public function toggle(BonusCode $bonusCode)
    {
        $bonusCode->toggleStatus();

        $this->bonusCodeRepository->save($bonusCode);
    }
}