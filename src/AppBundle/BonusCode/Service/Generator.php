<?php

declare(strict_types = 1);

namespace AppBundle\BonusCode\Service;

use AppBundle\BonusCode\Dto\BonusCodeGenerationCriteriaDto;
use AppBundle\BonusCode\Model\BonusCode;
use AppBundle\BonusCode\Repository\BonusCodeRepository;
use AppBundle\Exception\Application\BonusCode\BonusCodeNotFoundException;

class Generator
{
    const LENGTH = 10;

    private $bonusCodeRepository;

    public function __construct(BonusCodeRepository $bonusCodeRepository)
    {
        $this->bonusCodeRepository = $bonusCodeRepository;
    }

    public function createBonusCodes(BonusCodeGenerationCriteriaDto $criteria): array
    {
        $result = [];

        do {
            $bonusCode = new BonusCode(
                $criteria->server,
                $this->generateCode(),
                $criteria->amount,
                $criteria->expireAt
            );

            $this->bonusCodeRepository->save($bonusCode);

            $result[] = $bonusCode;
        } while (count($result) < $criteria->count);

        return $result;
    }

    private function generateCode(): string
    {
        foreach (self::generate() as $code) {
            try {
                $this->bonusCodeRepository->findByCode($code);
            } catch (BonusCodeNotFoundException $bonusCodeException) {
                return $code;
            }
        }
    }

    private function generate(): \Generator
    {
        while (true) {
            $code = '';
            $allowedSymbols = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $max = strlen($allowedSymbols) - 1;
            for ($i = 0; $i < self::LENGTH; ++$i) {
                $code .= $allowedSymbols[mt_rand(0, $max)];
            }

            yield $code;
        }
    }
}