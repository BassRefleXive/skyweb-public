<?php

declare(strict_types = 1);

namespace AppBundle\BonusCode\Service;


use AppBundle\BonusCode\Model\BonusCode;
use AppBundle\BonusCode\Repository\BonusCodeRepository;
use AppBundle\Entity\Application\UserServerAccount;
use AppBundle\Service\MuOnline\Funds\CoinsService;

class Activator
{
    private $bonusCodeRepository;
    private $coinsService;

    public function __construct(
        BonusCodeRepository $bonusCodeRepository,
        CoinsService $coinsService
    ) {
        $this->bonusCodeRepository = $bonusCodeRepository;
        $this->coinsService = $coinsService;
    }

    public function activate(BonusCode $bonusCode, UserServerAccount $userServerAccount)
    {
        $userServerAccount->activateBonusCode($bonusCode);

        $this->addBonusWCoin($userServerAccount, $bonusCode->amount());

        $this->bonusCodeRepository->save($bonusCode);
    }

    private function addBonusWCoin(UserServerAccount $userServerAccount, int $amount)
    {
        $this->coinsService->add($userServerAccount->getAccountId(), $amount);
    }
}