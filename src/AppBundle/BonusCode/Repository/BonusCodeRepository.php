<?php

declare(strict_types = 1);

namespace AppBundle\BonusCode\Repository;


use AppBundle\BonusCode\Model\BonusCode;
use AppBundle\Exception\Application\BonusCode\BonusCodeNotFoundException;
use AppBundle\Repository\BaseDoctrineRepository;

class BonusCodeRepository extends BaseDoctrineRepository
{
    public function findByCode(string $code): BonusCode
    {
        /** @var BonusCode|null $result */
        $result = $this->findOneBy([
            'code' => $code,
        ]);

        if ($result === null) {
            throw BonusCodeNotFoundException::missingCode($code);
        }

        return $result;
    }
}