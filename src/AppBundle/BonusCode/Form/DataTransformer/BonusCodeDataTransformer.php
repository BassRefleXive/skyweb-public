<?php

declare(strict_types = 1);

namespace AppBundle\BonusCode\Form\DataTransformer;

use AppBundle\BonusCode\Model\BonusCode;
use AppBundle\BonusCode\Repository\BonusCodeRepository;
use AppBundle\Exception\Application\BonusCode\BonusCodeNotFoundException;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class BonusCodeDataTransformer implements DataTransformerInterface
{
    private $repository;

    public function __construct(BonusCodeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function transform($entity)
    {
        if (null === $entity || !$entity instanceof BonusCode) {
            return null;
        }

        return $entity->code();
    }

    public function reverseTransform($code)
    {
        if (!$code) {
            return null;
        }

        try {
            $entity = $this->repository->findByCode($code);
        } catch (BonusCodeNotFoundException $e) {
            throw new TransformationFailedException($e->getMessage());
        }

        return $entity;
    }
}