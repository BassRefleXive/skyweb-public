<?php

declare(strict_types = 1);

namespace AppBundle\BonusCode\Form;


use AppBundle\BonusCode\Dto\BonusCodeActivationCriteriaDto;
use AppBundle\BonusCode\Form\DataTransformer\BonusCodeDataTransformer;
use AppBundle\BonusCode\Repository\BonusCodeRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class BonusCodeActivationType extends AbstractType
{
    private $bonusCodeRepository;

    public function __construct(BonusCodeRepository $bonusCodeRepository)
    {
        $this->bonusCodeRepository = $bonusCodeRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', TextType::class, [
                'required' => true,
                'label' => 'bonus_code.activate.1',
            ]);

        $builder->get('code')->addModelTransformer(new BonusCodeDataTransformer($this->bonusCodeRepository));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BonusCodeActivationCriteriaDto::class,
        ]);
    }
}