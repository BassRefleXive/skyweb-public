<?php

declare(strict_types = 1);

namespace AppBundle\BonusCode\Form;

use AppBundle\BonusCode\Dto\BonusCodeGenerationCriteriaDto;
use AppBundle\Entity\Application\Server;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class BonusCodeGenerationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('server', EntityType::class, [
                'required'     => true,
                'class'        => Server::class,
                'choice_label' => 'name',
                'placeholder'  => 'Select Server',
            ])
            ->add('count', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Codes Count',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 1,
                    ]),
                ],
                'label'       => 'Codes Count',
            ])
            ->add('amount', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Amount',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 1,
                    ]),
                ],
                'label'       => 'Amount',
            ])
            ->add('expireAt', DateTimeType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => '+ 24 hours',
                    ]),
                ],
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BonusCodeGenerationCriteriaDto::class,
        ]);
    }
}