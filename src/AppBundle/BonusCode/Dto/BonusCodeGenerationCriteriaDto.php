<?php

declare(strict_types = 1);

namespace AppBundle\BonusCode\Dto;

class BonusCodeGenerationCriteriaDto
{
    public $server;
    public $amount;
    public $expireAt;
    public $count;
}