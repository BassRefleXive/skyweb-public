<?php

declare(strict_types = 1);

namespace AppBundle\BonusCode\Dto;


use AppBundle\BonusCode\Model\BonusCode;

class BonusCodeActivationCriteriaDto
{
    /**
     * @var BonusCode
     */
    public $code;
}