<?php

declare(strict_types = 1);

namespace AppBundle\BonusCode\Model;

use AppBundle\Entity\Application\Server;
use AppBundle\Entity\Application\UserServerAccount;
use Doctrine\ORM\Mapping as ORM;

use AppBundle\Entity\Traits\Timestampable;

/**
 * @ORM\Entity(repositoryClass="AppBundle\BonusCode\Repository\BonusCodeRepository")
 * @ORM\Table(name="BonusCode")
 */
class BonusCode
{
    use Timestampable;

    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=10)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $code;

    /**
     * @ORM\Column(type="integer")
     */
    private $amount;

    /**
     * @ORM\Column(type="smallint")
     */
    private $status;

    /**
     * @ORM\Column(name="expire_at", type="datetime")
     */
    private $expireAt;

    /**
     * @ORM\Column(name="activated_at", type="datetime", nullable=true)
     */
    private $activatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\UserServerAccount", inversedBy="bonusCodes")
     * @ORM\JoinColumn(name="activated_by", referencedColumnName="id")
     */
    private $activatedBy;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\Server", inversedBy="bonusCodes")
     * @ORM\JoinColumn(name="server_id", referencedColumnName="id")
     */
    protected $server;

    public function __construct(Server $server, string $code, int $amount, \DateTime $expireAt)
    {
        $this->server = $server;
        $this->code = $code;
        $this->amount = $amount;
        $this->status = self::STATUS_ENABLED;
        $this->expireAt = $expireAt;
    }

    public function activate(UserServerAccount $userServerAccount)
    {
        $this->activatedAt = new \DateTime();
        $this->activatedBy = $userServerAccount;
    }

    public function activated(): bool
    {
        return $this->activatedBy !== null && $this->activatedAt !== null;
    }

    public function enabled(): bool
    {
        return $this->status === self::STATUS_ENABLED;
    }

    public function server(): Server
    {
        return $this->server;
    }

    public function code(): string
    {
        return $this->code;
    }

    public function amount(): int
    {
        return $this->amount;
    }

    public function expireAt(): \DateTime
    {
        return $this->expireAt;
    }

    /**
     * @return \DateTime|null
     */
    public function activatedAt()
    {
        return $this->activatedAt;
    }

    /**
     * @return UserServerAccount|null
     */
    public function activatedBy()
    {
        return $this->activatedBy;
    }

    public function toggleStatus()
    {
        $this->status = $this->status === self::STATUS_ENABLED
            ? self::STATUS_DISABLED
            : self::STATUS_DISABLED;
    }
}