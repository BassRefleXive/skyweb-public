<?php

declare(strict_types = 1);

namespace AppBundle\Exception\Application\BonusCode;


use AppBundle\Exception\ApplicationException;

class BonusCodeNotFoundException extends ApplicationException
{
    public static function missingCode(string $code): self
    {
        return new self(sprintf('Bonus code with code "%s" not found.', $code));
    }
}