<?php

namespace AppBundle\Exception\Application;


use AppBundle\Exception\ApplicationException;

class DisplayableException extends ApplicationException
{
    private $data = [];

    public function __construct(string $message = '', array $data = [])
    {
        parent::__construct($message);

        $this->data = $data;
    }

    public static function create(string $message = '', array $data = []): self
    {
        return new self($message, $data);
    }

    public function data(): array
    {
        return $this->data;
    }
}