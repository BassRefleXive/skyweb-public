<?php

declare(strict_types=1);

namespace AppBundle\Exception\Application\Vote;


use AppBundle\Enum\Application\VoteProvider;

class AlreadyVotedException extends \LogicException
{
    public static final function ip(string $ip, VoteProvider $provider, \DateTimeInterface $date): self
    {
        return new self(
            sprintf(
                'IP "%s" already voted "%s" on "%s".',
                $ip,
                $provider->getName(),
                $date->format('Y-m-d')
            )
        );
    }

    public static final function account(int $account, VoteProvider $provider, \DateTimeInterface $date): self
    {
        return new self(
            sprintf(
                'Account "%d" already voted "%s" on "%s".',
                $account,
                $provider->getName(),
                $date->format('Y-m-d')
            )
        );
    }
}