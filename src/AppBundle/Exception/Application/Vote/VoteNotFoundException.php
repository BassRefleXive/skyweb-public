<?php

declare(strict_types=1);

namespace AppBundle\Exception\Application\Vote;

use AppBundle\Enum\Application\VoteProvider;

class VoteNotFoundException extends \RuntimeException
{
    public static final function missingByIpAtDate(string $ip, VoteProvider $provider, \DateTimeInterface $date): self
    {
        return new self(
            sprintf(
                'Missing vote by IP "%s" at "%s" for provider "%s".',
                $ip,
                $date->format('Y-m-d'),
                $provider->getName()
            )
        );
    }

    public static final function missingByAccountDate(int $account, VoteProvider $provider, \DateTimeInterface $date): self
    {
        return new self(
            sprintf(
                'Missing vote by account "%d" at "%s" for provider "%s".',
                $account,
                $date->format('Y-m-d'),
                $provider->getName()
            )
        );
    }
    public static final function missingById(int $id, VoteProvider $provider): self
    {
        return new self(
            sprintf(
                'Missing vote by ID "%d" for provider "%s".',
                $id,
                $provider->getName()
            )
        );
    }
}