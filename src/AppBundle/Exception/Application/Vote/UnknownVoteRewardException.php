<?php

declare(strict_types=1);

namespace AppBundle\Exception\Application\Vote;


use AppBundle\Enum\Application\VoteProvider;
use AppBundle\Enum\Application\VoteType;

class UnknownVoteRewardException extends \LogicException
{
    public static final function create(VoteProvider $provider, VoteType $type): self
    {
        return new self(
            sprintf(
                'Cannot determine vote reward for provider "%s" and vote type "%s".',
                $provider->getName(),
                $type->getName()
            )
        );
    }
}