<?php

declare(strict_types=1);

namespace AppBundle\Exception\Application\AuthLog;

class AuthLogNotFoundException extends \RuntimeException
{
    public static final function missingSuccessfulByIp(string $ip): self
    {
        return new self(
            sprintf(
                'Successful AuthLog for IP "%s" not found.',
                $ip
            )
        );
    }

    public static final function missingSuccessfulByUsername(string $username): self
    {
        return new self(
            sprintf(
                'Successful AuthLog for username "%s" not found.',
                $username
            )
        );
    }
}