<?php

declare(strict_types = 1);

namespace AppBundle\Exception\Application\Payments\PaymentSystem;


class PaymentAlreadyProcessedException extends \LogicException
{
    public static function create(int $orderId): self
    {
        return new self(sprintf('Payment for order #%d already processed.', $orderId));
    }
}