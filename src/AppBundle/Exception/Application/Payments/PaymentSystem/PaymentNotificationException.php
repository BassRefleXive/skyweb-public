<?php

namespace AppBundle\Exception\Application\Payments\PaymentSystem;

use AppBundle\Exception\ApplicationException;

class PaymentNotificationException extends ApplicationException
{
    public static function missingParameter(string $name): self
    {
        return new self(sprintf('Missing parameter "%s" in request.', $name));
    }

    public static function signMismatch(): self
    {
        return new self('Sign mismatch.');
    }

    public static function checkoutMismatch(): self
    {
        return new self('Checkout mismatch.');
    }

    public static function invalidState(): self
    {
        return new self('Invalid state.');
    }

    public static function missingOrder(int $orderId): self
    {
        return new self(sprintf('Order with given id #%d missing.', $orderId));
    }

    public static function amountMismatch(float $orderAmount, float $notificationAmount): self
    {
        return new self(
            sprintf(
                'Payment notification amount mismatch. Order amount is %.2f, notification amount is %.2f',
                $orderAmount,
                $notificationAmount
            )
        );
    }

    public static function unexpectedResponse(string $message, string $data): self
    {
        return new self(
            sprintf(
                'API Call failed with unexpected response. Message: "%s"; Data: "%s";',
                $message,
                $data
            )
        );
    }

    public static function invalidPaymentSystem(string $expected, string $actual): self
    {
        return new self(sprintf('Invalid payment provider. Expected: "%s" actual: "%s".', $expected, $actual));
    }

    public static function unknownPaymentSystem(): self
    {
        return new self('Payment system unknown.');
    }
}