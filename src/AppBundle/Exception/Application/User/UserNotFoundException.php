<?php

declare(strict_types=1);

namespace AppBundle\Exception\Application\User;

class UserNotFoundException extends \RuntimeException
{
    public static final function missingByLogin(string $login): self
    {
        return new self(
            sprintf(
                'User with login "%s" not found.',
                $login
            )
        );
    }

    public static final function missingByDisplayName(string $displayName): self
    {
        return new self(
            sprintf(
                'User with display name "%s" not found.',
                $displayName
            )
        );
    }
}