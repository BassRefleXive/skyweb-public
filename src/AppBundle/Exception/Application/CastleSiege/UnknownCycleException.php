<?php

declare(strict_types=1);

namespace AppBundle\Exception\Application\CastleSiege;

class UnknownCycleException extends \LogicException
{
    public static final function missingByStage(int $stage): self
    {
        return new self(sprintf('Failed to determine cycle with stage #%d.', $stage));
    }
}