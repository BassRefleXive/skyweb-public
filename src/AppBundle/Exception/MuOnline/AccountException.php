<?php

namespace AppBundle\Exception\MuOnline;


use AppBundle\Exception\ApplicationException;

class AccountException extends ApplicationException
{
    public static function missingAccount(): self
    {
        return new self('Account not found.');
    }
}