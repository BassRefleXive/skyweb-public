<?php

declare(strict_types=1);

namespace AppBundle\Exception\MuOnline\CastleSiege;

use AppBundle\Exception\ApplicationException;

class DataNotFoundException extends ApplicationException
{
    public static final function missingData(): self
    {
        return new self('Missing CastleSiege data.');
    }
}