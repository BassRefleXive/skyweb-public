<?php

declare(strict_types = 1);

namespace AppBundle\Controller\Manager;

use AppBundle\Controller\BaseController;
use AppBundle\Entity\Application\User\Admin;
use AppBundle\Entity\Application\User\Ban;
use AppBundle\Entity\Application\User\Moderator;
use AppBundle\Entity\Application\User\User;
use AppBundle\Exception\Application\RedirectBackException;
use AppBundle\Form\Application\User\BanType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BanController extends BaseController
{
    public function createAction(User $user, Request $request): Response
    {
        if ($user instanceof Admin) {
            throw new RedirectBackException('You cannot ban Admin');
        }

        /** @var Moderator|Admin $moderator */
        $moderator = $this->getUser();

        if ($user instanceof Moderator and !$moderator instanceof Admin) {
            throw new RedirectBackException('Only Admin is allowed to ban managers.');
        }

        $ban = (new Ban())
            ->setBannedBy($moderator)
            ->setUser($user);

        $form = $this->createForm(BanType::class, $ban);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->get('sky_app.app.manager.user_manager.service.ban_manager')->banUser($form->getData());

                $this->addFlash('success', 'User has been banned!');

                return $this->redirectToRoute('sky_manager_user.manager_view', ['id' => $user->getId()]);
            } catch (\Throwable $e) {
                throw new RedirectBackException($e->getMessage());
            }
        }

        return $this->render(
            '@App/manager/user_manager/ban/index.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }
}