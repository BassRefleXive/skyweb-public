<?php

declare(strict_types = 1);

namespace AppBundle\Controller\Manager;

use AppBundle\Controller\BaseController;
use AppBundle\Entity\Application\User\User;
use AppBundle\Exception\Application\RedirectBackException;
use AppBundle\Form\Application\Admin\UserManager\Funds\ChangeCoinsType;
use AppBundle\Form\Application\Admin\UserManager\Funds\ChangeZenBankType;
use AppBundle\Form\Application\Admin\UserManager\SearchType;
use AppBundle\Model\Application\Admin\UserManager\Funds\ChangeCoinsModel;
use AppBundle\Model\Application\Admin\UserManager\Funds\ChangeZenBankModel;
use AppBundle\Model\Application\Admin\UserManager\SearchModel;
use AppBundle\Repository\MuOnline\AccountStateRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserManagerController extends BaseController
{
    public function indexAction(Request $request): Response
    {
        $form = $this->createForm(SearchType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var SearchModel $searchModel */
            $searchModel = $form->getData();

            $searchResults = $this->get('sky_app.app.manager.user_manager.service.finder')->find($searchModel);

            if (!$searchResults->length()) {
                $this->addFlash('warning', 'No results.');

                return $this->getRedirectBackResponse($request);
            }

            return $this->render(
                '@App/manager/user_manager/search_results.html.twig',
                [
                    'searchResults' => $searchResults,
                ]
            );
        }

        return $this->render(
            '@App/manager/user_manager/index.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    public function viewAction(User $user, Request $request): Response
    {
        return $this->render(
            '@App/manager/user_manager/view.html.twig',
            [
                'result' => $user,
                'zenBank' => $user->getUserServerAccountByServer($this->getCurrentServer())->zen(),
                'resultCoins' => $this->get('sky_app.muo.service.funds.coins')->get($user->getUsername(), null, false),
                'resultAccount' => $this->get('sky_app.muo.service.account')->getAccountByUser($user),
            ]
        );
    }

    public function changeUserCoinsAction(User $user, Request $request): Response
    {
        $coins = $this->get('sky_app.muo.service.funds.coins')->get($user->getUsername(), null, false);

        $form = $this->createForm(ChangeCoinsType::class, (new ChangeCoinsModel())
            ->setWCoinC($coins->wCoinC())
            ->setWCoinP($coins->wCoinP())
            ->setWCoinG($coins->wCoinG())
            ->setAccount($user->getUsername())
        );

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            /** @var ChangeCoinsModel $model */
            $model = $form->getData();

            $this->get('sky_app.muo.service.funds.coins')->set($user->getUsername(), $model->getWCoinC());

            return $this->redirectToRoute('sky_manager_user.manager_view', ['id' => $user->getId()]);
        }

        return $this->render(
            '@App/manager/user_manager/funds/change_coins.html.twig',
            [
                'form' => $form->createView(),
                'resultCoins' => $coins,
            ]
        );
    }

    public function changeUserZenBankAction(User $user, Request $request): Response
    {
        $userServerAccount = $user->getUserServerAccountByServer($this->getCurrentServer());

        $form = $this->createForm(ChangeZenBankType::class, (new ChangeZenBankModel())
            ->setAmount($userServerAccount->zen())
            ->setAccount($userServerAccount->getId())
        );

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            /** @var ChangeZenBankModel $model */
            $model = $form->getData();

            $userServerAccount->withdrawZen($userServerAccount->zen());
            $userServerAccount->depositZen($model->getAmount());

            $this->get('sky_app.app.repository.voter')->save($userServerAccount);

            return $this->redirectToRoute('sky_manager_user.manager_view', ['id' => $user->getId()]);
        }

        return $this->render(
            '@App/manager/user_manager/funds/change_zen_bank.html.twig',
            [
                'form' => $form->createView(),
                'zenBank' => $user->getUserServerAccountByServer($this->getCurrentServer())->zen(),
            ]
        );
    }
}