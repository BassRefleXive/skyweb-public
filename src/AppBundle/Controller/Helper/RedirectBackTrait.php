<?php

namespace AppBundle\Controller\Helper;


use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

trait RedirectBackTrait
{
    protected function buildRedirectBackResponse(Request $request, string $defaultRoute)
    {
        $referrer = $request->headers->get('referer');
        $redirectTo = $referrer === null
            ? $defaultRoute
            : $referrer;

        return new RedirectResponse($redirectTo, 302);
    }
}