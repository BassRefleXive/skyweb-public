<?php

namespace AppBundle\Controller\Helper;

use AppBundle\Entity\Application\ItemToBuy;
use AppBundle\Entity\Application\MarketItem;
use AppBundle\Entity\Application\Payments\Order;
use AppBundle\Entity\Application\Server;
use AppBundle\Entity\Application\User\Admin;
use AppBundle\Entity\Application\User\Moderator;
use AppBundle\Entity\Application\User\User;
use AppBundle\Entity\Application\UserServerAccount;
use AppBundle\Entity\Application\CartDiscountCoupon;
use AppBundle\Entity\Application\WebShopCategoryItem;
use AppBundle\Entity\Application\WebStorageItem;
use AppBundle\Entity\MuOnline\Account;
use AppBundle\Entity\MuOnline\Character;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Exception\Application\NeedAccountException;
use AppBundle\Exception\Application\RedirectBackException;
use AppBundle\Exception\Application\RequireLoginException;
use AppBundle\Repository\Application\UserRepository;
use AppBundle\Repository\Application\WebShopCategoryRepository;
use AppBundle\Repository\MuOnline\AccountStateRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait PermissionTrait
{
    /**
     * @return User|null
     */
    abstract function getUser();

    /**
     * @return UserServerAccount|null
     */
    abstract function getCurrentUserServerAccount();

    /**
     * @return Account
     */
    abstract protected function getCurrentAccount();

    /**
     * @return Server
     */
    abstract protected function getCurrentServer(): Server;

    protected function loggedInAccess()
    {
        if (!$this->getUser()) {
            throw new RequireLoginException('errors.2');
        }

        return $this;
    }

    protected function loggedOutAccess()
    {
        if ($this->getUser()) {
            throw new RedirectBackException('errors.25');
        }

        return $this;
    }

    protected function hasRequestedPasswordChange(string $token)
    {
        /**
         * @var UserRepository $userRepository
         */
        $userRepository = $this->get('sky_app.app.repository.user');

        if ($userRepository->findByPasswordResetToken($token) === null) {
            throw new RedirectBackException('errors.43');
        }

        return $this;
    }

    protected function hasAccountAccess()
    {
        if (!$this->getCurrentUserServerAccount()) {
            throw new NeedAccountException('errors.3');
        }

        return $this;
    }

    protected function offlineAccess()
    {
        $connectionState = $this->getCurrentAccount()->getState()->getConnectionState();

        if ($connectionState !== null && $connectionState !== AccountStateRepository::STATE_OFFLINE) {
            throw new RedirectBackException('errors.20');
        }

        return $this;
    }

    protected function warehouseClosedAccess()
    {
        $warehouseOpened = $this->getCurrentAccount()->getWarehouse()->opened();

        if ($warehouseOpened) {
            throw new RedirectBackException('errors.61');
        }

        return $this;
    }

    protected function cartItemAccess(ItemToBuy $cartItem)
    {
        $this->loggedInAccess()
             ->hasAccountAccess();

        if ($cartItem->getCart()->getUserServerAccount()->getUser()->getId() !== $this->getUser()->getId()) {
            throw new DisplayableException('errors.4');
        }

        if ($cartItem->getCart()->getUserServerAccount()->getServer()->getId() !== $this->getCurrentUserServerAccount()->getServer()->getId()) {
            throw new DisplayableException('errors.5');
        }

        return $this;
    }

    protected function orderAccess(Order $order)
    {
        $this->loggedInAccess()
            ->hasAccountAccess();

        if ($order->getUserServerAccount()->getUser()->getId() !== $this->getUser()->getId()) {
            throw new DisplayableException('errors.39');
        }

        if ($order->getUserServerAccount()->getServer()->getId() !== $this->getCurrentUserServerAccount()->getServer()->getId()) {
            throw new DisplayableException('errors.10');
        }

        return $this;
    }

    protected function webStorageItemAccess(WebStorageItem $item)
    {
        $this->loggedInAccess()
             ->hasAccountAccess();

        if ($item->getWebStorage()->getUserServerAccount()->getUser()->getId() !== $this->getUser()->getId()) {
            throw new DisplayableException('errors.6');
        }

        if ($item->getWebStorage()->getUserServerAccount()->getServer()->getId() !== $this->getCurrentUserServerAccount()->getServer()->getId()) {
            throw new DisplayableException('errors.7');
        }

        return $this;
    }

    protected function webShopDiscountCouponAccess(CartDiscountCoupon $coupon)
    {
        $this->loggedInAccess()
             ->hasAccountAccess();

        if ($coupon->getServer()->getId() !== $this->getCurrentUserServerAccount()->getServer()->getId()) {
            throw new DisplayableException('errors.8');
        }

        return $this;
    }

    protected function marketItemAccess(MarketItem $item)
    {
        $this->loggedInAccess()
             ->hasAccountAccess();

        $userServerAccount = $this->getCurrentUserServerAccount();

        if ($item->getServer()->getId() !== $userServerAccount->getServer()->getId()) {
            throw new DisplayableException('errors.9');
        }

        return $this;
    }

    protected function marketItemOwnerAccess(MarketItem $item)
    {
        $this->marketItemAccess($item);

        $userServerAccount = $this->getCurrentUserServerAccount();

        if ($item->getUserServerAccount()->getId() !== $userServerAccount->getId()) {
            throw new DisplayableException('errors.10');
        }

        return $this;
    }

    protected function activeMarketItemAccess(MarketItem $item)
    {
        $this->marketItemAccess($item);

        if (null !== $item->boughtBy()) {
            throw new DisplayableException('errors.53');
        }

        return $this;
    }

    protected function accountAccess(Account $account)
    {
        $this->loggedInAccess()
             ->hasAccountAccess();

        $userServerAccount = $this->getCurrentUserServerAccount();

        if ($userServerAccount->getAccountId() !== $account->getMemberId()) {
            throw new DisplayableException('errors.11');
        }

        return $this;
    }

    protected function characterAccess(Character $character)
    {
        $this->loggedInAccess()
             ->hasAccountAccess();

        $userServerAccount = $this->getCurrentUserServerAccount();

        if ($userServerAccount->getAccountId() !== $character->getAccount()->getMemberId()) {
            throw new DisplayableException('errors.12');
        }

        return $this;
    }

    protected function userHasBeenReferred(User $user)
    {
        if (!$user->getReferredBy()) {
            throw new RedirectBackException('errors.26');
        }

        return $this;
    }

    protected function referredUserAccess(User $user)
    {
        $this
            ->loggedInAccess()
            ->hasAccountAccess()
            ->userHasBeenReferred($user);

        $currentUser = $this->getUser();

        if ($user->getReferredBy()->getId() !== $currentUser->getId()) {
            throw new RedirectBackException('errors.27');
        }

        return $this;
    }

    protected function webShopCategoryItemAccess(WebShopCategoryItem $item) {
        $this->loggedInAccess()
            ->hasAccountAccess();

        if ($item->getWebShopCategory()->getStatus() !== WebShopCategoryRepository::STATUS_ENABLED) {
            throw new NotFoundHttpException();
        }

        return $this;
    }
}