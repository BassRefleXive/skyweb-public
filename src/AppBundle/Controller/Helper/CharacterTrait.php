<?php

namespace AppBundle\Controller\Helper;


use AppBundle\Entity\MuOnline\Character;
use AppBundle\Exception\Application\DisplayableException;
use Doctrine\ORM\EntityManager;

trait CharacterTrait
{
    private function findCharacterByName(string $name): Character
    {
        /** @var EntityManager $manager */
        $manager = $this->get('sky_app.app.service.server')->getCurrentServerDataBaseManager();

        $character = $manager->find(Character::class, $name);

        if (!$character) {
            throw new DisplayableException('errors.1');
        }

        return $character;
    }
}