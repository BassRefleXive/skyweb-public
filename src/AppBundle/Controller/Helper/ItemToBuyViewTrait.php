<?php


namespace AppBundle\Controller\Helper;


use AppBundle\Doctrine\Type\ItemToBuyTypeEnum;
use AppBundle\Doctrine\Type\Mappings\Items\Item;
use AppBundle\Entity\Application\ItemToBuy;
use AppBundle\Service\MuOnline\Item\OptionsParserChain;
use Collection\Sequence;

trait ItemToBuyViewTrait
{
    private function itemsToView(array $items): Sequence
    {
        /** @var OptionsParserChain $itemParser */
        $itemParser = $this->get('sky_app.muo.service.item.parser.chain');

        $result = new Sequence();

        /** @var ItemToBuy $itemToBuy */
        foreach ($items as $itemToBuy) {
            $result->add(
                array_merge(
                    [
                        'itemToBuy' => $itemToBuy,
                    ],
                    $itemToBuy->getType() === ItemToBuyTypeEnum::TYPE_ITEM
                        ? ['parsedItem' => $itemParser->parse(Item::createExisting()->setHex($itemToBuy->getItem())),]
                        : []
                ));
        }

        return $result;
    }
}