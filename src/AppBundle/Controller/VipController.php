<?php

declare(strict_types = 1);

namespace AppBundle\Controller;


use AppBundle\Controller\Helper\PermissionTrait;
use AppBundle\Entity\Application\Config\VipLevelConfig;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Form\Application\Account\PurchaseVipType;
use AppBundle\Model\Application\Account\PurchaseVipModel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class VipController extends BaseController
{
    use PermissionTrait;

    public function indexAction(Request $request): Response
    {
        $this->loggedInAccess()
            ->hasAccountAccess();

        return $this->render(
            '@App/vip/index.html.twig',
            [
                'levels' => $this->getCurrentServer()->vipLevels(),
            ]
        );
    }

    public function buyAction(VipLevelConfig $vipLevel, Request $request): Response
    {
        $this->loggedInAccess()
            ->hasAccountAccess()
            ->offlineAccess();

        $form = $this->createForm(
            PurchaseVipType::class,
            (new PurchaseVipModel())->setDailyRate($vipLevel->getDailyCoinsPrice())
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->get('sky_app.app.vip.service.vip')->buy(
                    $vipLevel,
                    $form->getData(),
                    $this->getCurrentUserServerAccount()
                );

                $this->addFlash('success', 'vip.buy.4');

                return $this->redirectToRoute('sky_vip_index');
            } catch (DisplayableException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
            }
        }

        return $this->render(
            '@App/vip/buy.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    public function renewalAction(VipLevelConfig $vipLevel, Request $request): Response
    {
        $this->loggedInAccess()
            ->hasAccountAccess()
            ->offlineAccess();

        $userServerAccount = $this->getCurrentUserServerAccount();

        $form = $this->createForm(
            PurchaseVipType::class,
            (new PurchaseVipModel())->setDailyRate(
                (int) ceil($vipLevel->getDailyCoinsPrice() * (100 - ($userServerAccount->vipDays() >= 30 ? $vipLevel->getRenewalDiscount() : 0))) / 100
            )
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->get('sky_app.app.vip.service.vip')->extend(
                    $vipLevel,
                    $form->getData(),
                    $this->getCurrentUserServerAccount()
                );

                $this->addFlash('success', 'vip.renew.2');

                return $this->redirectToRoute('sky_vip_index');
            } catch (DisplayableException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
            }
        }

        return $this->render(
            '@App/vip/renew.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }
}