<?php

namespace AppBundle\Controller;


use AppBundle\Controller\Helper\RedirectBackTrait;
use AppBundle\Doctrine\Filters\ActiveEntityFilter;
use AppBundle\Doctrine\Type\CartPersistentDiscountTypeEnum;
use AppBundle\Doctrine\Type\Mappings\Items\Item;
use AppBundle\Entity\Application\MarketItem;
use AppBundle\Entity\Application\Server;
use AppBundle\Entity\Application\User\User;
use AppBundle\Filter\CharacterFilter;
use AppBundle\Filter\GuildFilter;
use AppBundle\Filter\MarketFilter;
use AppBundle\Form\Application\User\LoginType;
use AppBundle\Forum\Model\Builder\PostBuilder;
use AppBundle\Service\Common\Funds\GetCoinsResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends Controller
{
    use RedirectBackTrait;

    protected function getRedirectBackResponse(Request $request, string $fallbackRoute = null)
    {
        return $this->buildRedirectBackResponse(
            $request,
            $this->get('router')
                ->generate(
                    null === $fallbackRoute
                        ? 'sky_index_index'
                        : $fallbackRoute
                )
        );
    }

    protected function render($view, array $parameters = [], Response $response = null)
    {
        $response = parent::render(
            $view,
            array_merge(
                $parameters,
                $this->getCommonParameters(),
                null === $this->getUser()
                    ? ['login_form_obj' => $this->createLoginForm()]
                    : []
            ),
            $response
        );

        if ('prod' === $this->getParameter('kernel.environment')) {
            $response->headers->set('Content-Security-Policy', "default-src * 'unsafe-inline'");
//            $response->headers->set('Referrer-Policy', 'same-origin');
            $response->headers->set('Strict-Transport-Security', 'max-age=3600');
            $response->headers->set('X-XSS-Protection', '1; mode=block');
            $response->headers->set('X-Content-Type-Options', 'nosniff');
            $response->headers->set('Timing-Allow-Origin', '*');
            $response->headers->set('X-Frame-Options', 'SAMEORIGIN');
        }

        return $response;
    }

    private function getCommonParameters()
    {
        $userServerAccount = $this->getCurrentUserServerAccount();
        $currentServer = $this->getCurrentServer();

        return [
            'serverTime'        => (new \DateTimeImmutable())->format(\DateTime::ATOM),
            'serverTimeZone'    => (new \DateTimeImmutable())->getTimezone()->getName(),
            'servers'           => [
                'list'    => $this->getServersList(),
                'current' => $currentServer,
            ],
            'account'           => $this->getCurrentAccount(),
            'userServerAccount' => $userServerAccount,
            'user'              => $this->getUser(),
            'webShop'           => $this->createCartPersistentDiscountData(),
            'chat'              => $this->get('sky_app.app.service.chat.chat_bro.parameters_builder')->build()->all(),
            'topChars'          => $this->getCurrentServerTopCharacters(),
            'topGuilds'         => $this->getCurrentServerTopGuilds(),
            'lastMarketItems'   => $this->getCurrentServerLastMarketItems(),
            'latestTopics'      => $this->getCurrentServerLastForumTopics(),
            'coins'             => $this->currentCoins(),
            'forumLink'         => $this->getParameter('sky_app.app.forum.host'),
            'timer'             => $this->getClosestTimer($currentServer),
            'voteBanners'       => $this->getParameter('sky_app.app.vote_banners'),
        ];
    }

    private function currentCoins(): ?GetCoinsResponse
    {
        return null !== $this->getUser()
            ? $this->get('sky_app.muo.service.funds.coins')->get(
                $this->getUser()->getUsername(),
                null,
                false
            )
            : null;
    }

    private function createLoginForm(): FormInterface
    {
        return $this->createForm(
            LoginType::class,
            null,
            [
                'action' => $this->generateUrl('sky_users_login'),
            ]
        );
    }

    private function createCartPersistentDiscountData(): array
    {
        $userServerAccount = $this->getCurrentUserServerAccount();

        $data = [
            'spent'         => [
                'money'   => null,
                'credits' => null,
            ],
            'discount'      => [
                'money'   => null,
                'credits' => null,
            ],
            'next_discount' => [
                'money'   => null,
                'credits' => null,
            ],
        ];

        if (!$userServerAccount) {
            return $data;
        }

        $currentServer = $this->getCurrentServer();
        $webShopPersistentDiscountRepo = $this->get('sky_app.app.repository.cart_persistent_discount');

        $data['spent']['money'] = $userServerAccount->getSpentMoney();
        $data['spent']['credits'] = $userServerAccount->getSpentCredits();

        $data['discount']['money'] = $webShopPersistentDiscountRepo->findInRange(
            $currentServer,
            $userServerAccount->getSpentMoney(),
            CartPersistentDiscountTypeEnum::PERSISTENT_DISCOUNT_TYPE_MONEY
        );
        $data['discount']['credits'] = $webShopPersistentDiscountRepo->findInRange(
            $currentServer,
            $userServerAccount->getSpentCredits(),
            CartPersistentDiscountTypeEnum::PERSISTENT_DISCOUNT_TYPE_CREDITS
        );

        if (null !== $data['discount']['money']) {
            $data['next_discount']['money'] = $webShopPersistentDiscountRepo->findNext($data['discount']['money']);
        }

        if (null !== $data['discount']['credits']) {
            $data['next_discount']['credits'] = $webShopPersistentDiscountRepo->findNext($data['discount']['credits']);
        }

        return $data;
    }

    protected function getCurrentServerTopCharacters()
    {
        return $this
            ->get('sky_app.muo.service.character')
            ->getByFilter((new CharacterFilter(null, 1))->setCount(15));
    }

    protected function getCurrentServerTopGuilds()
    {
        $service = $this->get('sky_app.muo.service.guild');

        return $service->extractEntities($service->getByFilter((new GuildFilter(1))->setCount(15)));
    }

    protected function getCurrentServerLastMarketItems()
    {
        return
            $this->getMarketItemsForView(
                $this
                    ->get('sky_app.app.service.market')
                    ->getByFilter(
                        (new MarketFilter(1))
                            ->setServer($this->getCurrentServer()->getId())
                            ->setCount(15)
                    )
            );
    }

    protected function getCurrentServerLastForumTopics()
    {
        return $this->get('sky_app.app.forum.finder.posts')
            ->lastFive($this->getParameter('sky_app.app.forum.last_five_forum_ids'));
    }

    protected function getClosestTimer(Server $server): array
    {
        $timer = $this->get('sky_app.app.repository.timer')->getClosestActive($server);

        if (null === $timer) {
            return [];
        }

        return [
            'time'  => $timer->getTime()->format(\DateTime::ATOM),
            'title' => [
                'en' => $timer->getTitleEng(),
                'ru' => $timer->getTitleRus(),
            ]
        ];
    }

    protected function getCurrentAccount()
    {
        $userServerAccount = $this->getCurrentUserServerAccount();

        return $userServerAccount
            ? $this->get('sky_app.muo.service.account')->getAccountByUserServerAccount($userServerAccount)
            : null;
    }

    protected function getCurrentUserServerAccount()
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user) {
            return null;
        }

        $server = $this->getCurrentServer();

        return $user->getUserServerAccountByServer($server);
    }

    protected function getServersList(): array
    {
        $serversList = $this->get('sky_app.app.service.server')->getList();
        $accountInfoService = $this->get('sky_app.muo.service.account_state');

        $result = [];

        /** @var Server $server */
        foreach ($serversList as $server) {
            $onlineCount = $accountInfoService->getOnlineCountByServer($server);
            $result[$server->getId()] = [
                'server' => $server,
                'online' => $server->increasedOnline($onlineCount),
            ];
        }

        return $result;
    }

    protected function getCurrentServer(): Server
    {
        return $this->get('sky_app.app.service.server')->getCurrentServer();
    }

    protected function getAndSetEntityFilter(Request $request, $default = null)
    {
        switch (strtolower($request->get('filter'))) {
            case 'a':
            case 'active':
                $filter = ActiveEntityFilter::NAME;
                break;
            default:
                $filter = $default;
        }

        if ($filter) {
            $this->getDoctrine()->getManager()->getFilters()->enable($filter);
        }

        return $filter;
    }

    protected function getMarketItemsForView(array $items): array
    {
        $itemParser = $this->get('sky_app.muo.service.item.parser.chain');

        $result = [];

        /** @var MarketItem $item */
        foreach ($items as $item) {
            $result[] = [
                'parsedItem' => $itemParser->parse(Item::createExisting()->setHex($item->getHex())),
                'marketItem' => $item,
            ];
        }

        return $result;
    }

    /**
     * @return User|null
     */
    protected function getUser()
    {
        return parent::getUser();
    }
}