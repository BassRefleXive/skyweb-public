<?php

namespace AppBundle\Controller;

use AppBundle\Controller\Helper\PermissionTrait;
use AppBundle\Entity\Application\User\Regular as RegularUser;
use AppBundle\Entity\Application\User\User;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Exception\Application\RedirectBackException;
use AppBundle\Form\Application\User\ChangePasswordType;
use AppBundle\Form\Application\User\RequestResetPasswordType;
use AppBundle\Form\Application\User\ResetPasswordType;
use AppBundle\Form\Application\User\UserType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class UsersController extends BaseController
{
    use PermissionTrait;

    public function viewAction(User $user, Request $request): Response
    {
        $accountService = $this->get('sky_app.muo.service.account');

        $server = $this->getCurrentServer();

        $userServerAccount = $user->getUserServerAccountByServer($server);

        $account = $userServerAccount
            ? $accountService->getAccountByUserServerAccount($userServerAccount)
            : null;

        return $this->render(
            '@App/users/index.html.twig',
            [
                'userToView'              => $user,
                'accountToView'           => $account,
                'charactersToView'        => $account
                    ? $account->getCharacters()
                    : null,
                'userServerAccountToView' => $userServerAccount,
            ]
        );
    }

    public function loginAction(Request $request)
    {
        $this->loggedOutAccess();

        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();

        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('@App/users/login.html.twig', [
            'last_username' => $lastUsername,
            'error'         => $error,
        ]);
    }

    public function registerAction(Request $request): Response
    {
        $this->loggedOutAccess();

        $user = (new RegularUser())->specifyIpAddress(ip2long($request->getClientIp()));

        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $user = $this->get('AppBundle\Service\Application\UserService')->register($user, $request->getLocale());
            } finally {
                if (null !== $user && null !== $user->getId()) {
                    $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
                    $this->get('security.token_storage')->setToken($token);
                    $this->get('session')->set('_security_main', serialize($token));
                }
            }

            return $this->redirectToRoute('sky_users_registered');
        }

        return $this->render(
            '@App/users/register.html.twig',
            [
                'form'      => $form->createView(),
                'isEnabled' => $this->getParameter('sky_app.app.modules.registration.enabled'),
            ]
        );
    }

    public function requestResetPasswordAction(Request $request): Response
    {
        $this->loggedOutAccess();

        $form = $this->createForm(RequestResetPasswordType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->get('AppBundle\Service\Application\UserService')->requestResetPassword($form->getData());

                return $this->render(
                    '@App/users/reset_password_request.html.twig',
                    [
                        'done' => true,
                    ]
                );
            } catch (DisplayableException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
            }
        }

        return $this->render(
            '@App/users/reset_password_request.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    public function resetPasswordAction(string $token, Request $request): Response
    {
        $this->loggedOutAccess()
            ->hasRequestedPasswordChange($token);

        $form = $this->createForm(ResetPasswordType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $user = $this->get('AppBundle\Service\Application\UserService')->resetPassword($token, $form->getData());

                $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
                $this->get('security.token_storage')->setToken($token);
                $this->get('session')->set('_security_main', serialize($token));

                $this->addFlash(
                    'success',
                    $this->getUser()->isRegisteredInForum()
                        ? 'users.change_password.5'
                        : 'users.change_password.3'
                );

                return $this->redirectToRoute('sky_index_index');
            } catch (DisplayableException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
            }
        }

        return $this->render(
            '@App/users/reset_password.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    public function registeredAction(Request $request): Response
    {
        $this->loggedInAccess();

        return $this->render(
            '@App/users/registered.html.twig',
            [
                'user' => $this->getUser(),
                'registrationConfig' => $this->get('AppBundle\Repository\Application\Config\RegistrationConfigRepository')->findAll()[0],
            ]
        );
    }

    public function changePasswordAction(Request $request): Response
    {
        $this->loggedInAccess();

        $form = $this->createForm(ChangePasswordType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->get('AppBundle\Service\Application\UserService')->changePassword($this->getUser(), $form->getData());

                $this->addFlash(
                    'success',
                    $this->getUser()->isRegisteredInForum()
                        ? 'users.change_password.5'
                        : 'users.change_password.3'
                );
                return $this->redirectToRoute('sky_accounts_index');
            } catch (DisplayableException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
            }
        }

        return $this->render(
            '@App/users/change_password.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }
}
