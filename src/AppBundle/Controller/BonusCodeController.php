<?php

declare(strict_types = 1);

namespace AppBundle\Controller;


use AppBundle\BonusCode\Dto\BonusCodeActivationCriteriaDto;
use AppBundle\BonusCode\Form\BonusCodeActivationType;
use AppBundle\Controller\Helper\PermissionTrait;
use AppBundle\Exception\Application\DisplayableException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BonusCodeController extends BaseController
{
    use PermissionTrait;

    public function indexAction(Request $request): Response
    {
        $this->loggedInAccess()
            ->hasAccountAccess();

        return $this->render(
            '@App/bonus_code/index.html.twig',
            [
                'bonusCodes' => $this->getCurrentUserServerAccount()->bonusCodes()->toArray(),
            ]
        );
    }

    public function activateAction(Request $request): Response
    {
        $this->loggedInAccess()
            ->hasAccountAccess();

        $form = $this->createForm(BonusCodeActivationType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var BonusCodeActivationCriteriaDto $activationCriteria */
            $activationCriteria = $form->getData();

            try {
                $this->get('sky_app.bonus_code.service.activator')->activate(
                    $activationCriteria->code,
                    $this->getCurrentUserServerAccount()
                );

                $this->addFlash('success', 'bonus_code.activate.3');
                return $this->redirectToRoute('sky_bonus.code_index');
            } catch (DisplayableException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
            }

            return $this->redirectToRoute('sky_bonus.code_activate');
        }

        return $this->render(
            '@App/bonus_code/activate.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }
}