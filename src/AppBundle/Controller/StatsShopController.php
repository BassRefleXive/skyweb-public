<?php

namespace AppBundle\Controller;


use AppBundle\Controller\Helper\PermissionTrait;
use AppBundle\Form\Application\StatsShopType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class StatsShopController extends BaseController
{
    use PermissionTrait;

    public function indexAction(Request $request): Response
    {
        $this
            ->loggedInAccess()
            ->hasAccountAccess();

        $form = $this->createForm(StatsShopType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('sky_app.app.service.stats_shop')->buy($this->getUser(), $form->get('step')->getData());

            return $this->render('@App/stats_shop/bought.html.twig');
        }

        return $this->render(
            '@App/stats_shop/index.html.twig',
            [
                'form' => $form->createView(),
                'config' => $this->getCurrentServer()->getStatsShopConfig(),
            ]
        );
    }

}