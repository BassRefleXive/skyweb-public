<?php

namespace AppBundle\Controller;


use AppBundle\Controller\Helper\PermissionTrait;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Form\Application\MarketSellType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class WarehouseController extends BaseController
{
    use PermissionTrait;

    public function moveAction(int $serial, Request $request): Response
    {
        $this->loggedInAccess()
            ->hasAccountAccess()
            ->warehouseClosedAccess()
        ;

        try {
            if (!$serial) {
                throw new DisplayableException('errors.59');
            }

            $userServerAccount = $this->getCurrentUserServerAccount();

            $warehouseService = $this->get('sky_app.muo.service.warehouse');
            $webStorageService = $this->get('sky_app.app.service.web_storage');

            $item = $warehouseService->findItemBySerial($userServerAccount, $serial);

            if (!$item) {
                throw new DisplayableException('errors.0');
            }

            $webStorageService->add($userServerAccount, $item);

            $warehouseService->remove($userServerAccount, $item);

            $this->addFlash('success', 'web_storage.6');
        } catch (DisplayableException $e) {
            $this->addFlash(
                'error',
                $e->getMessage()
            );
        }

        return $this->redirectToRoute('sky_accounts_index');
    }

    public function removeAction(int $serial, Request $request): Response
    {
        $this->loggedInAccess()
            ->hasAccountAccess()
            ->warehouseClosedAccess()
        ;

        try {
            if (!$serial) {
                throw new DisplayableException('errors.59');
            }

            $userServerAccount = $this->getCurrentUserServerAccount();

            $warehouseService = $this->get('sky_app.muo.service.warehouse');

            $item = $warehouseService->findItemBySerial($userServerAccount, $serial);

            if (!$item) {
                throw new DisplayableException('errors.0');
            }

            $warehouseService->remove($userServerAccount, $item);

            $this->addFlash('success', 'web_storage.7');
        } catch (DisplayableException $e) {
            $this->addFlash(
                'error',
                $e->getMessage()
            );
        }

        return $this->redirectToRoute('sky_accounts_index');
    }

    public function sellAction(int $serial, Request $request): Response
    {
        $this->loggedInAccess()
            ->hasAccountAccess()
            ->warehouseClosedAccess();

        try {
            if (!$serial) {
                throw new DisplayableException('errors.59');
            }

            $userServerAccount = $this->getCurrentUserServerAccount();

            $warehouseService = $this->get('sky_app.muo.service.warehouse');

            $item = $warehouseService->findItemBySerial($userServerAccount, $serial);

            if (!$item) {
                throw new DisplayableException('errors.0');
            }

            $form = $this->createForm(MarketSellType::class);

            $form->handleRequest($request);


            if ($form->isSubmitted() && $form->isValid()) {

                $this->get('sky_app.app.service.market')->sell(
                    $userServerAccount,
                    $item,
                    $form->get('price')->getData(),
                    $form->get('priceType')->getData()
                );

                $warehouseService->remove($userServerAccount, $item);

                $this->addFlash('success', 'market.sell.5');

                return $this->redirectToRoute('sky_accounts_index');
            }

            return $this->render(
                '@App/market/sell.html.twig',
                [
                    'item' => $item,
                    'form' => $form->createView(),
                ]
            );


        } catch (DisplayableException $e) {
            $this->addFlash(
                'error',
                $e->getMessage()
            );

            return $this->redirectToRoute('sky_accounts_index');
        }
    }
}