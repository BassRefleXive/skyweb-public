<?php

namespace AppBundle\Controller;

use AppBundle\Doctrine\Filters\ActiveEntityFilter;
use AppBundle\Entity\Application\News;
use AppBundle\Filter\NewsFilter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class IndexController extends BaseController
{
    public function indexAction(Request $request): Response
    {
        $this->getAndSetEntityFilter($request, ActiveEntityFilter::NAME);

//        $news = $this->get('sky_app.app.service.news')->get();

//        $newsFilter = NewsFilter::buildFromRequest($request)->forServer($this->getCurrentServer());
//        $news = $this->get('sky_app.app.repository.news')->findByQueryFilerPaginated($newsFilter);

        $newsFilter = NewsFilter::buildFromRequest($request)->forServer($this->getCurrentServer());
        $newsPaginator = $this->getDoctrine()->getRepository(News::class)->findByQueryFilerPaginated($newsFilter);

        return $this->render(
            '@App/index/index.html.twig',
            [
                'news' => $newsPaginator->getIterator()->getArrayCopy(),
                'pagination' => [
                    'maxPages' => ceil($newsPaginator->count() / NewsFilter::NEWS_FILTER_LIMIT),
                    'thisPage' => $newsFilter->getPage(),
                ],
            ]
        );
    }
}
