<?php

namespace AppBundle\Controller;


use AppBundle\Controller\Helper\PermissionTrait;
use AppBundle\Doctrine\Type\Mappings\Items\Item;
use AppBundle\Entity\Application\MarketCategory;
use AppBundle\Entity\Application\MarketItem;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Filter\MarketFilter;
use AppBundle\Form\Application\MarketBuyType;
use AppBundle\Form\Application\MarketReturnType;
use AppBundle\Form\Application\MarketSearchType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MarketController extends BaseController
{
    use PermissionTrait;

    public function indexAction(Request $request): Response
    {
        $sessionService = $this->get('sky_app.app.service.session');

        /** @var MarketFilter $marketFilter */
        if ($marketFilter = $sessionService->getMarketFilter()) {
            $marketFilter = $marketFilter->updateByRequest($request);
            if (($category = $marketFilter->getCategory()) instanceof MarketCategory) {
                $this->getDoctrine()->getManager()->persist($category);
            }
        } else {
            $marketFilter = MarketFilter::buildFromRequest($request);
        }

        $marketFilter
            ->setServer($this->getCurrentServer()->getId())
            ->setUserServerAccount($this->getCurrentUserServerAccount())
            ->setBought(false);

        $form = $this->createForm(MarketSearchType::class, $marketFilter);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $sessionService->setMarketFilter($form->getData());
        }

        $itemsPaginator = $this->getDoctrine()->getRepository(MarketItem::class)->findByQueryFilerPaginated($form->getData());

        return $this->render(
            '@App/market/index.html.twig',
            [
                'form'       => $form->createView(),
                'items'      => $this->getMarketItemsForView($itemsPaginator->getIterator()->getArrayCopy()),
                'pagination' => [
                    'maxPages' => ceil($itemsPaginator->count() / MarketFilter::MARKET_FILTER_LIMIT),
                    'thisPage' => $form->getData()->getPage(),
                ],
            ]
        );
    }

    public function sellHistoryAction(Request $request): Response
    {
        $marketFilter = MarketFilter::buildFromRequest($request);
        $marketFilter
            ->setServer($this->getCurrentServer()->getId())
            ->setUserServerAccount($this->getCurrentUserServerAccount())
            ->setOwned(true)
            ->setBought(null)
            ->setCount(15);

        $itemsPaginator = $this->getDoctrine()->getRepository(MarketItem::class)->findByQueryFilerPaginated($marketFilter);

        return $this->render(
            '@App/market/sale_history.html.twig',
            [
                'items' => $this->getMarketItemsForView($itemsPaginator->getIterator()->getArrayCopy()),
                'pagination' => [
                    'maxPages' => ceil($itemsPaginator->count() / 15),
                    'thisPage' => $marketFilter->getPage(),
                ],
            ]
        );
    }

    public function buyHistoryAction(Request $request): Response
    {
        $marketFilter = MarketFilter::buildFromRequest($request);
        $marketFilter
            ->setServer($this->getCurrentServer()->getId())
            ->setUserServerAccount($this->getCurrentUserServerAccount())
            ->setOwned(false)
            ->setBought(true)
            ->setCount(15);

        $itemsPaginator = $this->getDoctrine()->getRepository(MarketItem::class)->findByQueryFilerPaginated($marketFilter);

        return $this->render(
            '@App/market/purchase_history.html.twig',
            [
                'items' => $this->getMarketItemsForView($itemsPaginator->getIterator()->getArrayCopy()),
                'pagination' => [
                    'maxPages' => ceil($itemsPaginator->count() / 15),
                    'thisPage' => $marketFilter->getPage(),
                ],
            ]
        );
    }

    public function returnAction(MarketItem $item, Request $request): Response
    {
        $this
            ->marketItemOwnerAccess($item)
            ->activeMarketItemAccess($item);

        $parsedItem = $this->get('sky_app.muo.service.item.parser.chain')->parse(Item::createExisting()->setHex($item->getHex()));

        $form = $this->createForm(MarketReturnType::class, [
            'item' => $item,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {
                $userServerAccount = $this->getCurrentUserServerAccount();

                $marketService = $this->get('sky_app.app.service.market');
                $webStorageService = $this->get('sky_app.app.service.web_storage');

                $marketService->returnItem($userServerAccount, $item, $form->get('commission')->getData());
                $webStorageService->add($userServerAccount, $parsedItem);

                $this->addFlash('success', 'market.return.8');

                return $this->redirectToRoute('sky_accounts_index');
            } catch (DisplayableException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
            }

        }

        return $this->render(
            '@App/market/return.html.twig',
            [
                'form'       => $form->createView(),
                'item'       => $parsedItem,
                'marketItem' => $item,
            ]
        );
    }

    public function buyAction(MarketItem $item, Request $request): Response
    {
        $this
            ->marketItemAccess($item)
            ->activeMarketItemAccess($item);

        $parsedItem = $this->get('sky_app.muo.service.item.parser.chain')->parse(Item::createExisting()->setHex($item->getHex()));

        $form = $this->createForm(MarketBuyType::class, [
            'item' => $item,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {
                $userServerAccount = $this->getCurrentUserServerAccount();

                $marketService = $this->get('sky_app.app.service.market');
                $webStorageService = $this->get('sky_app.app.service.web_storage');

                $marketService->buyItem($userServerAccount, $item, $form->get('price')->getData());
                $webStorageService->add($userServerAccount, $parsedItem);

                $this->addFlash('success', 'market.buy.8');

                return $this->redirectToRoute('sky_accounts_index');
            } catch (DisplayableException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
            }

        }

        return $this->render(
            '@App/market/buy.html.twig',
            [
                'form'       => $form->createView(),
                'item'       => $parsedItem,
                'marketItem' => $item,
            ]
        );
    }
}