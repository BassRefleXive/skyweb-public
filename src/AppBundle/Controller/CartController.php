<?php

namespace AppBundle\Controller;


use AppBundle\Controller\Helper\ItemToBuyViewTrait;
use AppBundle\Controller\Helper\PermissionTrait;
use AppBundle\Doctrine\Type\ItemToBuyTypeEnum;
use AppBundle\Doctrine\Type\Mappings\Items\Item;
use AppBundle\Entity\Application\ItemToBuy;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Form\Application\BuyCartType;
use Collection\Sequence;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CartController extends BaseController
{
    use PermissionTrait, ItemToBuyViewTrait;

    public function indexAction(Request $request): Response
    {
        $this->loggedInAccess()
            ->hasAccountAccess();

        $cart = $this->getCurrentUserServerAccount()->getCart();

        $formData = [
            'priceCredits' => $cart->calculateRequiredCreditsCount(),
            'priceMoney' => $cart->calculateRequiredMoney(),
        ];

        $form = $this->createForm(BuyCartType::class, $formData);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $data = $form->getData();

                $coupon = $data['coupon'] ?? null;

                $result = $this->get('sky_app.app.service.cart')->buy($cart, $data['priceCredits'], $data['priceMoney'], $data['type'], $coupon);

                if ($result === null) {
                    $this->addFlash('success', 'cart.13');
                }

                return $result === null
                    ? $this->redirectToRoute('sky_accounts_index')
                    : $this->redirectToRoute('sky_order_view', ['id' => $result]);

            } catch (DisplayableException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
            }
        }

        return $this->render(
            '@App/cart/index.html.twig',
            [
                'form' => $form->createView(),
                'items' => $this->itemsToView($this->getCartItems()),
            ]
        );
    }

    public function removeAction(): Response
    {
        $this->loggedInAccess()
            ->hasAccountAccess();

        $cart = $this->getCurrentUserServerAccount()
            ->getCart()
            ->removeItems();

        $em = $this->getDoctrine()->getManager();
        $em->persist($cart);
        $em->flush();

        $this->addFlash('success', 'cart.15');

        return $this->redirectToRoute('sky_cart_index');
    }

    public function removeItemAction(ItemToBuy $item): Response
    {
        $this->cartItemAccess($item);

        $em = $this->getDoctrine()->getManager();
        $em->remove($item);
        $em->flush();

        $this->addFlash('success', 'cart.14');

        return $this->redirectToRoute('sky_cart_index');
    }


    private function getCartItems(): array
    {
        $userServerAccount = $this->getCurrentUserServerAccount();

        return $userServerAccount->getCart()->getItems()->toArray();
    }
}