<?php


namespace AppBundle\Controller;


use AppBundle\Filter\VoterFilter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class VotersController extends BaseController
{
    public function indexAction(Request $request): Response
    {
        $currentServer = $this->getCurrentServer();

        $filter = VoterFilter::buildFromRequest($request)->withServer($this->getCurrentServer());

        $paginator = $this->get('sky_app.app.repository.voter')->findByQueryFilerPaginated($filter);

        $voterService = $this->get('sky_app.app.votes.service.voter');

        return $this->render(
            '@App/voters/index.html.twig',
            [
                'lastMonthTop' => $voterService->getLastMonthTopVotersByServer($currentServer),
                'votersUserServerAccounts' => $paginator->getIterator()->getArrayCopy(),
                'pagination' => [
                    'maxPages' => ceil(($paginator->count() - 1) / VoterFilter::FILTER_LIMIT),
                    'thisPage' => $filter->getPage(),
                    'perPage' => VoterFilter::FILTER_LIMIT,
                ],
            ]
        );
    }
}