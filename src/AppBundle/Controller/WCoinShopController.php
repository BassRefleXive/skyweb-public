<?php


namespace AppBundle\Controller;


use AppBundle\Controller\Helper\PermissionTrait;
use AppBundle\Form\Application\Account\WCoinShopPurchaseType;
use AppBundle\Model\Application\Account\WCoinShopPurchaseModel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class WCoinShopController extends BaseController
{
    use PermissionTrait;

    public function indexAction(Request $request): Response
    {
        $this->loggedInAccess()
            ->hasAccountAccess();

        $config = $this->getCurrentServer()->getWCoinShopConfig();

        $form = $this->createForm(
            WCoinShopPurchaseType::class,
            (new WCoinShopPurchaseModel())
                ->setCoinsPerUsd($config->getCoinsPerUsd())
                ->setMinAmount($config->getMinAmount())
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('sky_app.app.service.wcoin_shop')->buy($this->getCurrentUserServerAccount(), $form->getData());

            return $this->redirectToRoute('sky_wcoin.shop_bought');
        }

        return $this->render(
            '@App/wcoin_shop/index.html.twig',
            [
                'config' => $config,
                'form' => $form->createView(),
            ]
        );
    }

    public function boughtAction(Request $request): Response
    {
        $this->loggedInAccess()
            ->hasAccountAccess();

        return $this->render('@App/wcoin_shop/bought.html.twig');
    }
}