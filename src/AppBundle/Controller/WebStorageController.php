<?php

namespace AppBundle\Controller;


use AppBundle\Controller\Helper\PermissionTrait;
use AppBundle\Doctrine\Type\Mappings\Items\Item;
use AppBundle\Entity\Application\WebStorageItem;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Exception\MuOnline\WarehouseFullException;
use AppBundle\Form\Application\MarketSellType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class WebStorageController extends BaseController
{
    use PermissionTrait;

    public function moveAction(WebStorageItem $item, Request $request): Response
    {
        $this
            ->webStorageItemAccess($item)
            ->warehouseClosedAccess()
        ;

        try {
            $userServerAccount = $this->getCurrentUserServerAccount();

            $warehouseService = $this->get('sky_app.muo.service.warehouse');
            $webStorageService = $this->get('sky_app.app.service.web_storage');

            try {
                $webStorageItem = $webStorageService->remove($userServerAccount, $item);
                $warehouseService->add($userServerAccount, $webStorageItem);
            } catch (WarehouseFullException $e) {
                $webStorageService->add($userServerAccount, $webStorageItem);

                throw new DisplayableException('errors.18');
            }

            $this->addFlash('success', 'web_storage.5');

        } catch (DisplayableException $e) {
            $this->addFlash(
                'error',
                $e->getMessage()
            );
        }

        return $this->redirectToRoute('sky_accounts_index');
    }

    public function removeAction(WebStorageItem $item, Request $request): Response
    {
        $this
            ->webStorageItemAccess($item)
            ->warehouseClosedAccess()
        ;

        try {
            $userServerAccount = $this->getCurrentUserServerAccount();

            $this->get('sky_app.app.service.web_storage')->remove($userServerAccount, $item);

            $this->addFlash('success', 'web_storage.7');

        } catch (DisplayableException $e) {
            $this->addFlash(
                'error',
                $e->getMessage()
            );
        }

        return $this->redirectToRoute('sky_accounts_index');
    }

    public function sellAction(WebStorageItem $item, Request $request): Response
    {
        $this->webStorageItemAccess($item);

        try {
            $parsedItem = $this->get('sky_app.muo.service.item.parser.chain')->parse(
                Item::createExisting()->setHex($item->getItem())
            );

            if (!$parsedItem->getGeneralOptions()->getSerialNumber()->compound()) {
                throw new DisplayableException('errors.59');
            }

            $userServerAccount = $this->getCurrentUserServerAccount();

            $form = $this->createForm(MarketSellType::class);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $this->get('sky_app.app.service.market')->sell(
                    $userServerAccount,
                    $parsedItem,
                    $form->get('price')->getData(),
                    $form->get('priceType')->getData()
                );

                $this->get('sky_app.app.service.web_storage')->remove($userServerAccount, $item);

                $this->addFlash('success', 'market.sell.5');

                return $this->redirectToRoute('sky_accounts_index');

            }

            return $this->render(
                '@App/market/sell.html.twig',
                [
                    'item' => $parsedItem,
                    'form' => $form->createView(),
                ]
            );

        } catch (DisplayableException $e) {
            $this->addFlash(
                'error',
                $e->getMessage()
            );

            return $this->redirectToRoute('sky_accounts_index');
        }

    }
}