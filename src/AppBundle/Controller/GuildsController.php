<?php

namespace AppBundle\Controller;


use AppBundle\Entity\MuOnline\Character;
use AppBundle\Entity\MuOnline\GuildMember;
use AppBundle\Filter\GuildFilter;
use AppBundle\Filter\GuildMemberFilter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GuildsController extends BaseController
{
    public function indexAction(Request $request): Response
    {
        $filter = GuildFilter::buildFromRequest($request);

        $service = $this->get('sky_app.muo.service.guild');

        $paginator = $service->findByQueryFilerPaginated($filter);

        return $this->render(
            '@App/guild/index.html.twig',
            [
                'guilds' => $service->extractEntities($paginator->getIterator()->getArrayCopy()),
                'pagination' => [
                    'maxPages' => ceil($paginator->count() / GuildFilter::FILTER_LIMIT),
                    'thisPage' => $filter->getPage(),
                    'perPage' => GuildFilter::FILTER_LIMIT,
                ],
            ]);
    }

    public function viewAction(string $name, Request $request): Response
    {
        $filter = GuildMemberFilter::buildFromRequest($request)->withName($name);

        $service = $this->get('sky_app.muo.service.guild');

        $paginator = $service->findMembersByQueryFilerPaginated($filter);

        $result = $paginator->getIterator()->getArrayCopy();

        if (!$paginator->count()) {
            throw new NotFoundHttpException('Guild not found.');
        }

        $members = $service->extractEntities($result);

        $users = $this->get('AppBundle\Service\Application\UserService')
            ->getUsersByCharacters(
                array_map(function (GuildMember $member): Character {
                    return $member->getCharacter();
                }, $members)
            );

        return $this->render(
            '@App/guild/view.html.twig',
            [
                'members' => $members,
                'guild' => $members[0]->getGuild(),
                'users' => $users,
                'pagination' => [
                    'maxPages' => ceil($paginator->count() / GuildFilter::FILTER_LIMIT),
                    'thisPage' => $filter->getPage(),
                    'perPage' => GuildFilter::FILTER_LIMIT,
                ],
            ]
        );
    }
}