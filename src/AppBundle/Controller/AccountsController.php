<?php

namespace AppBundle\Controller;


use AppBundle\Controller\Helper\CharacterTrait;
use AppBundle\Controller\Helper\PermissionTrait;
use AppBundle\Doctrine\Type\Mappings\Items\Item;
use AppBundle\Entity\Application\WebStorageItem;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Form\Application\Account\FundsTransferType;
use AppBundle\Form\Application\Account\HideInfoType;
use AppBundle\Form\Application\Account\WebCoinOperationType;
use AppBundle\Form\Application\AddShopStatsType;
use AppBundle\Form\Application\Account\ZenBankOperationType;
use AppBundle\Form\Application\ExchangeOnlineTimeType;
use AppBundle\Model\Application\Account\FundsTransferModel;
use AppBundle\Model\Application\Account\HideInfoModel;
use AppBundle\Model\Application\Account\WebCoinOperationModel;
use AppBundle\Model\Application\AddShopStatsModel;
use AppBundle\Model\Application\Account\ZenBankOperationModel;
use AppBundle\Service\Application\FundsTransferService;
use AppBundle\Service\Application\ZenBankService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class AccountsController extends BaseController
{
    use PermissionTrait, CharacterTrait;

    public function indexAction(Request $request): Response
    {
        $this->loggedInAccess()
            ->hasAccountAccess();

        $accountService = $this->get('sky_app.muo.service.account');
        $warehouseService = $this->get('sky_app.muo.service.warehouse');
        $account = $accountService->getAccountByUserServerAccount($this->getCurrentUserServerAccount());

        return $this->render(
            '@App/accounts/index.html.twig',
            [
                'items' => $warehouseService->getParsedItems($account),
                'webStorageItems' => $this->getWebStorageItemsForView(),
            ]
        );
    }

    public function createAction(Request $request): Response
    {
        if (!$user = $this->getUser()) {
            throw new AccessDeniedHttpException();
        }

        try {
            $this->get('sky_app.muo.service.account')->createAccount($user);
        } catch (DisplayableException $e) {
            $this->addFlash(
                'error',
                $e->getMessage()
            );
        }

        return $this->getRedirectBackResponse($request);
    }

    public function addShopStatsAction(Request $request): Response
    {
        $this
            ->loggedInAccess()
            ->hasAccountAccess()
            ->offlineAccess();

        $form = $this->createForm(AddShopStatsType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var AddShopStatsModel $model */
            $model = $form->getData();

            $character = $model->getCharacter();

            $this->characterAccess($character);

            $this->get('sky_app.muo.service.character')->addPermanentStats($character, $model->getStats());
            $userServerAccount = $this->getCurrentUserServerAccount();

            $userServerAccount->setPurchasedStats($userServerAccount->getPurchasedStats() - $model->getStats());

            $manager = $this->getDoctrine()->getManager();

            $manager->persist($userServerAccount);
            $manager->flush();

            return $this->redirectToRoute('sky_accounts_index');
        }

        return $this->render(
            '@App/accounts/add_shop_stats.html.twig',
            [
                'form' => $form->createView(),
            ]
        );

    }

    public function hideInfoAction(Request $request): Response
    {
        $this->loggedInAccess();

        $form = $this->createForm(
            HideInfoType::class,
            new HideInfoModel(
                $this->getCurrentServer()->getAccountMenuConfig()->hideInfoDailyRate()
            )
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->get('sky_app.app.service.account')->hideInfo(
                    $this->getCurrentUserServerAccount(),
                    $form->getData()
                );

                $this->addFlash('success', 'hide_info.2');

                return $this->redirectToRoute('sky_accounts_index');
            } catch (DisplayableException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
            }
        }

        return $this->render(
            '@App/accounts/hide_info.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    public function zenBankAction(Request $request): Response
    {
        $this
            ->loggedInAccess()
            ->hasAccountAccess()
            ->warehouseClosedAccess();

        $form = $this->createForm(ZenBankOperationType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                /** @var ZenBankOperationModel $model */
                $model = $form->getData();

                $this->get(ZenBankService::class)->processOperation($model, $this->getCurrentUserServerAccount());

                $this->addFlash('success', 'zen_bank.8');

                return $this->redirectToRoute('sky_accounts_index');
            } catch (DisplayableException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
            }
        }

        $account = $this->get('sky_app.muo.service.account')->getAccountByUserServerAccount($this->getCurrentUserServerAccount());

        return $this->render(
            '@App/accounts/zen_bank.html.twig',
            [
                'accountZen' => $account->getWarehouse()->getMoney(),
                'form'       => $form->createView(),
            ]
        );
    }

    public function fundsTransferAction(Request $request): Response {
        $this
            ->loggedInAccess()
            ->hasAccountAccess();

        $form = $this->createForm(FundsTransferType::class, new FundsTransferModel($this->getCurrentUserServerAccount()));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                /** @var FundsTransferModel $model */
                $model = $form->getData();

                $this->get(FundsTransferService::class)->transfer($model);

                $this->addFlash('success', 'funds_transfer.12');

                return $this->redirectToRoute('sky_accounts_index');
            } catch (DisplayableException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
            }
        }

        return $this->render(
            '@App/accounts/funds_transfer.html.twig',
            [
                'form'       => $form->createView(),
            ]
        );
    }

    private function getWebStorageItems(): array
    {
        $userServerAccount = $this->getCurrentUserServerAccount();

        return $userServerAccount->getWebStorage()->getItems()->toArray();
    }

    private function getWebStorageItemsForView(): array
    {
        $itemParser = $this->get('sky_app.muo.service.item.parser.chain');

        $items = $this->getWebStorageItems();

        $result = [];

        /** @var WebStorageItem $item */
        foreach ($items as $item) {
            $result[] = [
                'parsedItem'     => $itemParser->parse(Item::createExisting()->setHex($item->getItem())),
                'webStorageItem' => $item,
            ];
        }

        return $result;
    }
}