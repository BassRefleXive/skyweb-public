<?php

namespace AppBundle\Controller;


use AppBundle\Controller\Helper\PermissionTrait;
use AppBundle\Doctrine\Filters\ActiveEntityFilter;
use AppBundle\Entity\Application\WebShopCategory;
use AppBundle\Entity\Application\WebShopCategoryItem;
use AppBundle\Form\Application\WebShopBuyItemType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class WebShopController extends BaseController
{
    use PermissionTrait;

    public function indexAction(Request $request): Response
    {
        $this->loggedInAccess()
            ->hasAccountAccess();

        $this->getAndSetEntityFilter($request, ActiveEntityFilter::NAME);

        return $this->render(
            '@App/webshop/index.html.twig',
            [
                'categoriesByLevels' => $this->get('sky_app.app.service.web_shop')
                    ->flattenCategories(
                        $this->get('sky_app.app.repository.webshop_category')
                            ->findRootCategories($this->getCurrentServer())
                    ),
                'config' => $this->getCurrentServer()->getWebShopConfig(),
            ]
        );
    }

    public function buyAction(WebShopCategoryItem $item, Request $request): Response
    {
        $this->webShopCategoryItemAccess($item);

        $currentServer = $this->getCurrentServer();

        if ($currentServer->getId() !== $item->getWebShopCategory()->getConfig()->getServer()->getId()) {
            throw new NotFoundHttpException();
        }

        $formData = [
            'item' => $item,
        ];

        $form = $this->createForm(WebShopBuyItemType::class, $formData);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $this->get('sky_app.app.service.web_shop')->buy(
                $this->getUser(),
                $item,
                $form->get('priceCredits')->getData(),
                $form->get('priceMoney')->getData(),
                $data['level'] ?? null,
                $data['option'] ?? null,
                $data['skill'] ?? false,
                $data['luck'] ?? false,
                $data['excellent'] ?? [],
                $data['ancient'] ?? null,
                $data['harmony'] ?? null,
                $data['pvp'] ?? false
            );

            return $this->render('@App/webshop/bought.html.twig');

        }

        /** @var WebShopCategory $category */
        $category = $item->getWebShopCategory();

        return $this->render(
            '@App/webshop/buy.html.twig',
            [
                'form' => $form->createView(),
                'item' => $item,
                'categoryConfig' => $category->getConfig(),
            ]
        );
    }

}