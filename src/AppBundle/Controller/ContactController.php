<?php


namespace AppBundle\Controller;


use AppBundle\Entity\Application\User\User;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Form\Application\ContactRequestType;
use AppBundle\Model\Application\ContactRequestModel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ContactController extends BaseController
{
    public function indexAction(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $model = new ContactRequestModel();

        if ($user instanceof User) {
            $model->setEmail($user->email());
        }

        $form = $this->createForm(ContactRequestType::class, $model);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->get('sky_app.app.service.contact')
                    ->sendContactRequest($form->getData());

                $this->addFlash('success', 'contact.sent.0');

                return $this->redirectToRoute('sky_contacts_index');
            } catch (DisplayableException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
            }
        }

        return $this->render(
            '@App/contact/index.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }
}