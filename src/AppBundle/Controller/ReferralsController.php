<?php

namespace AppBundle\Controller;


use AppBundle\Controller\Helper\PermissionTrait;
use AppBundle\Entity\Application\User\User;
use AppBundle\Exception\Application\RedirectBackException;
use AppBundle\Filter\ReferralFilter;
use AppBundle\Form\Application\ReferralRewardType;
use AppBundle\Model\Application\ReferralRewardModel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ReferralsController extends BaseController
{
    use PermissionTrait;

    public function indexAction(Request $request): Response
    {
        $this->loggedInAccess();

        $referralFilter = ReferralFilter::buildFromRequest($request)->setUser($this->getUser());
        $referralPaginator = $this->getDoctrine()->getRepository(User::class)->findByQueryFilerPaginated($referralFilter);

        return $this->render(
            '@App/referrals/index.html.twig',
            [
                'referredBy' => $this->getUser()->getReferredBy(),
                'referrals'  => $referralPaginator->getIterator()->getArrayCopy(),
                'pagination' => [
                    'maxPages' => ceil($referralPaginator->count() / ReferralFilter::REFERRAL_FILTER_LIMIT),
                    'thisPage' => $referralFilter->getPage(),
                ],
            ]
        );
    }

    public function rewardAction(User $user, Request $request): Response
    {
        $this
            ->referredUserAccess($user)
            ->hasAccountAccess();

        $referredUserServerAccount = $user->getUserServerAccountByServer($this->getCurrentServer());

        if (!$referredUserServerAccount) {
            throw new RedirectBackException('errors.28');
        }

        if ($referredUserServerAccount->getReferredReward()) {
            throw new RedirectBackException('errors.29');
        }

        $form = $this->createForm(ReferralRewardType::class, new ReferralRewardModel());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->get('sky_app.app.service.referral')->getReward(
                $this->getCurrentUserServerAccount(),
                $referredUserServerAccount,
                $form->getData()->getReward()
            );

            $this->addFlash('success', 'referrals.reward.3');

            return $this->redirectToRoute('sky_referrals_index');
        }

        return $this->render(
            '@App/referrals/reward.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

}