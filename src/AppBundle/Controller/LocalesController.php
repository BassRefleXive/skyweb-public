<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Application\User\User;
use AppBundle\Form\Application\SetLocaleType;
use AppBundle\Model\Application\LocaleModel;
use AppBundle\Service\Application\SessionService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class LocalesController extends BaseController
{
    public function setAction(Request $request, SessionService $sessionService): Response
    {
        $form = $this->createForm(SetLocaleType::class, null, [
            'method' => Request::METHOD_GET,
        ]);

        $form->submit($request->query->all());

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var LocaleModel $localeModel */
            $localeModel = $form->getData();

            $sessionService->setLocale($localeModel->getLocale());

            /** @var User $user */
            if (($user = $this->getUser()) instanceof User) {
                $user->setLocale($localeModel->getLocale());
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($user);
                $em->flush($user);
            }

        }

        return $this->getRedirectBackResponse($request);
    }
}