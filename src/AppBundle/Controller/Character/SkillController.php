<?php

declare(strict_types=1);

namespace AppBundle\Controller\Character;

use AppBundle\Controller\BaseController;
use AppBundle\Controller\Helper\CharacterTrait;
use AppBundle\Controller\Helper\PermissionTrait;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SkillController extends BaseController
{
    use PermissionTrait, CharacterTrait;

    public function indexAction(string $name, Request $request): Response
    {
        $character = $this->findCharacterByName($name);

        $this->characterAccess($character);

        return $this->render(
            '@App/character/skill/index.html.twig',
            [
                'character' => $character,
                'magicList' => $this->get('sky_app.app.magic_list.processor')->process($character->magicList()),
            ]
        );
    }

    public function removeAction(string $name, Request $request): Response
    {
        $character = $this->findCharacterByName($name);

        $this->characterAccess($character);

        $this->get('sky_app.muo.service.character')->resetSkills(
            $character,
            $this->getCurrentUserServerAccount(),
            array_map(
                function ($param): int {
                    return (int) $param;
                },
                json_decode($request->getContent(), true)['skills'] ?? []
            )
        );

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}