<?php

declare(strict_types=1);

namespace AppBundle\Controller\Rating;

use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Response;

class CastleSiegeController extends BaseController
{
    public function indexAction(): Response
    {
        return $this->render(
            '@App/rating/castle_siege/index.html.twig',
            [
                'data' => $this->get('AppBundle\Service\Application\CastleSiege\CastleDataProvider')->provide($this->getCurrentServer()),
                'registeredGuilds' => $this->get('AppBundle\Service\Application\CastleSiege\CastleDataProvider')->registeredGuilds($this->getCurrentServer()),
            ]
        );
    }
}