<?php

declare(strict_types = 1);

namespace AppBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PolicyController extends BaseController
{
    public function indexAction(Request $request): Response
    {
        return $this->render('@App/policy/index.html.twig');
    }
}