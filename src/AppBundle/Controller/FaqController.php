<?php


namespace AppBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FaqController extends BaseController
{
    public function indexAction(Request $request): Response
    {
        return $this->render('@App/faq/faq.html.twig');
    }

    public function voteRewardAction(Request $request): Response
    {
        return $this->render('@App/faq/vote_reward.html.twig');
    }

    public function offtradeAction(Request $request): Response
    {
        return $this->render('@App/faq/offtrade.html.twig');
    }

    public function offafkAction(Request $request): Response
    {
        return $this->render('@App/faq/offafk.html.twig');
    }

    public function muHelperAction(Request $request): Response
    {
        return $this->render('@App/faq/mu-helper.html.twig');
    }

    public function reconnectSystemAction(Request $request): Response
    {
        return $this->render('@App/faq/reconnect_system.html.twig');
    }

    public function ancientSetsAction(Request $request): Response
    {
        return $this->render('@App/faq/ancient_sets.html.twig');
    }

    public function eventWhiteRabbitInvasionAction(Request $request): Response
    {
        return $this->render('@App/faq/event/white_rabbit.html.twig');
    }

    public function eventRewardsAction(Request $request): Response
    {
        return $this->render('@App/faq/event/rewards.html.twig');
    }

    public function dropFromBoxesAction(Request $request): Response
    {
        return $this->render('@App/faq/drop_from_boxes.html.twig');
    }

    public function resAndGRAction(Request $request): Response
    {
        return $this->render('@App/faq/res_and_gr.html.twig');
    }

    public function referralSystemAction(Request $request): Response
    {
        return $this->render('@App/faq/referral_system.html.twig');
    }

    public function webBankAction(Request $request): Response
    {
        return $this->render('@App/faq/web_bank.html.twig');
    }

    public function changeNickAction(Request $request): Response
    {
        return $this->render('@App/faq/change_nick.html.twig');
    }

    public function webShopAction(Request $request): Response
    {
        return $this->render('@App/faq/web_shop.html.twig');
    }

    public function discountCouponsAction(Request $request): Response
    {
        return $this->render('@App/faq/discount_coupons.html.twig');
    }

    public function persistentDiscountsAction(Request $request): Response
    {
        return $this->render('@App/faq/persistent_discounts.html.twig');
    }

    public function cartAction(Request $request): Response
    {
        return $this->render('@App/faq/cart.html.twig');
    }

    public function kalimaAction(Request $request): Response
    {
        return $this->render('@App/faq/kalima.html.twig');
    }

    public function arenaRemovedAction(Request $request): Response
    {
        return $this->render('@App/faq/arena_removed.html.twig');
    }

    public function wingsHuntAction(Request $request): Response
    {
        return $this->render('@App/faq/wings_hunt.html.twig');
    }

    public function eventCherryBlossomAction(Request $request): Response
    {
        return $this->render('@App/faq/event/cherry_blossom.html.twig');
    }

    public function eventHappyHourAction(Request $request): Response
    {
        return $this->render('@App/faq/event/happy_hour.html.twig');
    }

    public function mapsWithAdditionalExpAction(Request $request): Response
    {
        return $this->render('@App/faq/maps_additional_exp.html.twig');
    }

    public function availableCommandsAction(Request $request): Response
    {
        return $this->render('@App/faq/available_commands.html.twig');
    }

    public function bonusCodesAction(Request $request): Response
    {
        return $this->render('@App/faq/bonus_codes.html.twig');
    }

    public function resetStatsAction(Request $request): Response
    {
        return $this->render('@App/faq/reset_stats.html.twig');
    }

    public function eventJewelsHuntAction(Request $request): Response
    {
        return $this->render('@App/faq/event/jewels_hunt.html.twig');
    }

    public function luckyItemsAction(Request $request): Response
    {
        return $this->render('@App/faq/lucky_items.html.twig');
    }

    public function darkJewelsAction(Request $request): Response
    {
        return $this->render('@App/faq/dark_jewels.html.twig');
    }

    public function buffersAction(Request $request): Response
    {
        return $this->render('@App/faq/buffers.html.twig');
    }

    public function eventBloodCastleAction(Request $request): Response
    {
        return $this->render('@App/faq/event/blood_castle.html.twig');
    }

    public function eventMedusaAction(Request $request): Response
    {
        return $this->render('@App/faq/event/medusa.html.twig');
    }

    public function eventGladiatorBuffAction(Request $request): Response
    {
        return $this->render('@App/faq/event/gladiator_buff.html.twig');
    }

    public function gensAction(Request $request): Response
    {
        return $this->render('@App/faq/gens.html.twig');
    }

    public function eventLastManStandingAction(Request $request): Response
    {
        return $this->render('@App/faq/event/last_man_standing.html.twig');
    }

    public function eventWeekendBonusExpAction(Request $request): Response
    {
        return $this->render('@App/faq/event/weekend_bonus_exp.html.twig');
    }
}