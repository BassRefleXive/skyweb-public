<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Controller\BaseController;
use AppBundle\Entity\Application\News;
use AppBundle\Form\Application\NewsType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class NewsController extends BaseController
{
    public function indexAction(): Response
    {
        $news = $this->get('sky_app.app.repository.news')->findAll();

        return $this->render(
            '@App/admin/news/index.html.twig',
            [
                'news' => $news,
            ]
        );
    }

    public function createAction(Request $request): Response
    {
        $news = new News();

        $form = $this->createForm(NewsType::class, $news);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $news->setAuthor($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($news);
            $em->flush();

            return $this->redirectToRoute('sky_admin_news_index');
        }

        return $this->render(
            '@App/admin/news/create.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    public function removeAction(News $news, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($news);
        $em->flush();

        return $this->getRedirectBackResponse($request);
    }

    public function editAction(News $news, Request $request): Response
    {
        $form = $this->createForm(NewsType::class, $news, [
            'method' => Request::METHOD_PUT,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $news->setAuthor($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($news);
            $em->flush();

            return $this->redirectToRoute('sky_admin_news_index');
        }

        return $this->render(
            '@App/admin/news/edit.html.twig',
            [
                'form' => $form->createView(),
            ]
        );

    }

}