<?php


namespace AppBundle\Controller\Admin;


use AppBundle\Controller\BaseController;
use AppBundle\Entity\Application\Timer;
use AppBundle\Form\Application\TimerType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TimerController extends BaseController
{
    public function indexAction(): Response
    {
        $timers = $this->get('sky_app.app.repository.timer')->findAll();

        return $this->render(
            '@App/admin/timer/index.html.twig',
            [
                'timers' => $timers,
            ]
        );
    }

    public function createAction(Request $request): Response
    {
        $form = $this->createForm(TimerType::class, new Timer());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('sky_app.app.repository.timer')->save($form->getData());

            return $this->redirectToRoute('sky_admin_timers_index');
        }

        return $this->render(
            '@App/admin/timer/create.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    public function removeAction(Timer $timer, Request $request): Response
    {
        $this->get('sky_app.app.repository.timer')->remove($timer);

        return $this->getRedirectBackResponse($request);
    }

    public function editAction(Timer $timer, Request $request): Response
    {
        $form = $this->createForm(TimerType::class, $timer, [
            'method' => Request::METHOD_PUT,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('sky_app.app.repository.timer')->save($form->getData());

            return $this->redirectToRoute('sky_admin_timers_index');
        }

        return $this->render(
            '@App/admin/timer/edit.html.twig',
            [
                'form' => $form->createView(),
            ]
        );

    }
}