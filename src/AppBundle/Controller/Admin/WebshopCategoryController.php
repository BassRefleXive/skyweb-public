<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Controller\BaseController;
use AppBundle\Entity\Application\WebShopCategory;
use AppBundle\Entity\Application\WebShopConfig;
use AppBundle\Form\Application\WebShopCategoryType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class WebshopCategoryController extends BaseController
{

    public function createAction(WebShopConfig $webShopConfig, Request $request): Response
    {
        $category = new WebShopCategory($webShopConfig);

        $form = $this->createForm(WebShopCategoryType::class, $category);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($items = $form->get('items')) {
                foreach ($items->all() as $itemForm) {
                    $categoryItem = $itemForm->getData();
                    $options = $itemForm->get('options')->getData();

                    foreach ($categoryItem->getPossibleOptions() as $option) {
                        in_array($option, $options, true)
                            ? $categoryItem->addAllowedOption($option)
                            : $categoryItem->removeAllowedOption($option);
                    }
                }
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('sky_admin_servers_edit', ['id' => $category->getConfig()->getServer()->getId()]);
        }

        return $this->render(
            '@App/admin/webshop_category/create.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    public function removeAction(WebShopCategory $category, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($category);
        $em->flush();

        return $this->getRedirectBackResponse($request);
    }

    public function editAction(WebShopCategory $category, Request $request): Response
    {
        $originalItems = clone $category->getItems();

        $form = $this->createForm(WebShopCategoryType::class, $category, [
            'method' => Request::METHOD_PUT,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if ($items = $form->get('items')) {
                foreach ($originalItems as $item) {
                    if (!$category->getItems()->contains($item)) {
                        $em->remove($item);
                    }
                }

                foreach ($items->all() as $itemForm) {
                    $categoryItem = $itemForm->getData();
                    $options = $itemForm->get('options')->getData();

                    foreach ($categoryItem->getPossibleOptions() as $option) {
                        in_array($option, $options, true)
                            ? $categoryItem->addAllowedOption($option)
                            : $categoryItem->removeAllowedOption($option);
                    }
                }
            }

            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('sky_admin_webshop.category_edit', ['id' => $category->getId()]);
        }

        return $this->render(
            '@App/admin/webshop_category/edit.html.twig',
            [
                'form' => $form->createView(),
            ]
        );

    }

}