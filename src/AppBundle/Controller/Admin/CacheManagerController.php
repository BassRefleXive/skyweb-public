<?php

declare(strict_types = 1);

namespace AppBundle\Controller\Admin;


use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Response;

class CacheManagerController extends BaseController
{
    public function indexAction(): Response
    {
        return $this->render('@App/admin/cache_manager/index.html.twig');
    }

    public function invalidateAction(): Response
    {
        $service = $this->get('sky_app.app.service.cache.manager');

        return $this->render(
            '@App/admin/cache_manager/invalidated.html.twig',
            [
                'output' => implode(PHP_EOL, [
                    $service->clear(),
                    $service->warmUp(),
                ])
            ]
        );
    }
}