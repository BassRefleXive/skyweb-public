<?php

namespace AppBundle\Controller\Admin;


use AppBundle\Controller\BaseController;
use AppBundle\Entity\Application\Server;
use AppBundle\Entity\Application\CartDiscountCoupon;
use AppBundle\Form\Application\CartDiscountCouponType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CartDiscountCouponController extends BaseController
{

    public function createAction(Server $server, Request $request): Response
    {
        $coupon = new CartDiscountCoupon();

        $form = $this->createForm(CartDiscountCouponType::class, $coupon);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $couponText = $this->get('sky_app.app.service.cart_discount_coupon')->getUniqueCoupon();

            $coupon
                ->setServer($server)
                ->setCoupon($couponText);

            $em = $this->getDoctrine()->getManager();
            $em->persist($coupon);
            $em->flush();

            return $this->redirectToRoute('sky_admin_servers_edit', ['id' => $server->getId()]);
        }

        return $this->render(
            '@App/admin/cart_discount_coupons/create.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    public function removeAction(CartDiscountCoupon $coupon, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($coupon);
        $em->flush();

        return $this->getRedirectBackResponse($request);
    }

    public function editAction(CartDiscountCoupon $coupon, Request $request): Response
    {
        $form = $this->createForm(CartDiscountCouponType::class, $coupon, [
            'method' => Request::METHOD_PUT,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($coupon);
            $em->flush();

            return $this->redirectToRoute('sky_admin_servers_edit', ['id' => $coupon->getServer()->getId()]);
        }

        return $this->render(
            '@App/admin/cart_discount_coupons/edit.html.twig',
            [
                'form' => $form->createView(),
            ]
        );

    }
}