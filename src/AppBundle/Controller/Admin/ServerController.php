<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Controller\BaseController;
use AppBundle\Entity\Application\Server;
use AppBundle\Form\Application\ServerType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ServerController extends BaseController
{
    public function indexAction(): Response
    {
        return $this->render('@App/admin/servers/index.html.twig');
    }

    public function createAction(Request $request): Response
    {
        $server = new Server();

        $form = $this->createForm(ServerType::class, $server);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($server);
            $em->flush();

            return $this->redirectToRoute('sky_admin_servers_index');
        }

        return $this->render(
            '@App/admin/servers/create.html.twig',
            [
                'server' => $server,
                'form' => $form->createView(),
                'webShopCategories' => [],
                'webShopConfig' => null,
                'webShopDiscountCoupons' => [],
                'webShopPersistentDiscounts' => [],
                'bonusCodes' => [],
                'vipLevels' => [],
                'castleSiegeCycles' => [],
            ]
        );
    }

    public function removeAction(Server $server, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($server);
        $em->flush();

        return $this->getRedirectBackResponse($request);
    }

    public function editAction(Server $server, Request $request): Response
    {
        $form = $this->createForm(ServerType::class, $server, [
            'method' => Request::METHOD_PUT,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($server);
            $em->flush();

            return $this->redirectToRoute('sky_admin_servers_index');
        }

        return $this->render(
            '@App/admin/servers/edit.html.twig',
            [
                'server' => $server,
                'form' => $form->createView(),
                'webShopCategories' => $server->getWebShopConfig()
                    ? $server->getWebShopConfig()->rootCategories()->toArray()
                    : [],
                'webShopConfig' => $server->getWebShopConfig(),
                'webShopDiscountCoupons' => $server->getCartDiscountCoupons()->toArray(),
                'webShopPersistentDiscounts' => $server->getCartPersistentDiscounts()->toArray(),
                'bonusCodes' => $server->bonusCodes()->toArray(),
                'vipLevels' => $server->vipLevels()->toArray(),
                'castleSiegeCycles' => $server->castleSiegeCycles()->toArray(),
            ]
        );

    }

}