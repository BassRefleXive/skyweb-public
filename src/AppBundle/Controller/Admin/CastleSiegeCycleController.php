<?php


namespace AppBundle\Controller\Admin;


use AppBundle\Controller\BaseController;
use AppBundle\Entity\Application\Config\CastleSiegeCycle;
use AppBundle\Entity\Application\Server;
use AppBundle\Form\Application\Config\CastleSiegeCycleType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CastleSiegeCycleController extends BaseController
{
    public function createAction(Server $server, Request $request): Response
    {
        $cycle = new CastleSiegeCycle();

        $form = $this->createForm(CastleSiegeCycleType::class, $cycle->setServer($server));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($form->getData());
            $em->flush();

            return $this->redirectToRoute('sky_admin_servers_edit', ['id' => $cycle->getServer()->getId()]);
        }

        return $this->render(
            '@App/admin/castle_siege_cycle/create.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    public function removeAction(CastleSiegeCycle $cycle, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($cycle);
        $em->flush();

        return $this->getRedirectBackResponse($request);
    }

    public function editAction(CastleSiegeCycle $cycle, Request $request): Response
    {
        $form = $this->createForm(CastleSiegeCycleType::class, $cycle, [
            'method' => Request::METHOD_PUT,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($form->getData());
            $em->flush();

            return $this->redirectToRoute('sky_admin_servers_edit', ['id' => $cycle->getServer()->getId()]);
        }

        return $this->render(
            '@App/admin/castle_siege_cycle/edit.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }
}