<?php


namespace AppBundle\Controller\Admin;


use AppBundle\Controller\BaseController;
use AppBundle\Entity\Application\Config\VipLevelConfig;
use AppBundle\Entity\Application\Server;
use AppBundle\Form\Application\Config\VipLevelConfigType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class VipLevelController extends BaseController
{
    public function createAction(Server $server, Request $request): Response
    {
        $vipLevel = new VipLevelConfig();

        $form = $this->createForm(VipLevelConfigType::class, $vipLevel->setServer($server));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($form->getData());
            $em->flush();

            return $this->redirectToRoute('sky_admin_servers_edit', ['id' => $vipLevel->getServer()->getId()]);
        }

        return $this->render(
            '@App/admin/vip_level/create.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    public function removeAction(VipLevelConfig $vipLevel, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($vipLevel);
        $em->flush();

        return $this->getRedirectBackResponse($request);
    }

    public function editAction(VipLevelConfig $vipLevel, Request $request): Response
    {
        $form = $this->createForm(VipLevelConfigType::class, $vipLevel, [
            'method' => Request::METHOD_PUT,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($form->getData());
            $em->flush();

            return $this->redirectToRoute('sky_admin_servers_edit', ['id' => $vipLevel->getServer()->getId()]);
        }

        return $this->render(
            '@App/admin/vip_level/edit.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }
}