<?php

namespace AppBundle\Controller\Admin;


use AppBundle\Controller\BaseController;
use AppBundle\Entity\Application\Server;
use AppBundle\Entity\Application\CartPersistentDiscount;
use AppBundle\Form\Application\CartPersistentDiscountType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CartPersistentDiscountController extends BaseController
{
    public function createAction(Server $server, Request $request): Response
    {
        $discount = new CartPersistentDiscount();

        $form = $this->createForm(CartPersistentDiscountType::class, $discount);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $discount->setServer($server);

            $em = $this->getDoctrine()->getManager();
            $em->persist($discount);
            $em->flush();

            return $this->redirectToRoute('sky_admin_servers_edit', ['id' => $server->getId()]);
        }

        return $this->render(
            '@App/admin/cart_persistent_discounts/create.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    public function removeAction(CartPersistentDiscount $discount, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($discount);
        $em->flush();

        return $this->getRedirectBackResponse($request);
    }

    public function editAction(CartPersistentDiscount $discount, Request $request): Response
    {
        $form = $this->createForm(CartPersistentDiscountType::class, $discount, [
            'method' => Request::METHOD_PUT,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($discount);
            $em->flush();

            return $this->redirectToRoute('sky_admin_servers_edit', ['id' => $discount->getServer()->getId()]);
        }

        return $this->render(
            '@App/admin/cart_persistent_discounts/edit.html.twig',
            [
                'form' => $form->createView(),
            ]
        );

    }

}