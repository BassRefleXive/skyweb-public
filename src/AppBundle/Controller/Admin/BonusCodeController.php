<?php

declare(strict_types = 1);

namespace AppBundle\Controller\Admin;


use AppBundle\BonusCode\Dto\BonusCodeGenerationCriteriaDto;
use AppBundle\BonusCode\Form\BonusCodeGenerationType;
use AppBundle\BonusCode\Model\BonusCode;
use AppBundle\Controller\BaseController;
use AppBundle\Entity\Application\Server;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BonusCodeController extends BaseController
{
    public function createAction(Server $server, Request $request): Response
    {
        $criteria = new BonusCodeGenerationCriteriaDto();
        $criteria->server = $server;

        $form = $this->createForm(BonusCodeGenerationType::class, $criteria);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var BonusCodeGenerationCriteriaDto $criteria */
            $criteria = $form->getData();
            $criteria->server = $server;

            $this->get('sky_app.bonus_code.service.generator')->createBonusCodes($criteria);

            return $this->redirectToRoute('sky_admin_servers_edit', ['id' => $server->getId()]);
        }

        return $this->render(
            '@App/admin/bonus_code/create.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    public function toggleStatusAction(BonusCode $bonusCode, Request $request): Response
    {
        $this->get('sky_app.bonus_code.service.status_toggler')->toggle($bonusCode);

        return $this->redirectToRoute('sky_admin_servers_edit', ['id' => $bonusCode->server()->getId()]);
    }
}