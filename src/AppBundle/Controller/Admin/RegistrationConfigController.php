<?php

declare(strict_types = 1);


namespace AppBundle\Controller\Admin;


use AppBundle\Controller\BaseController;
use AppBundle\Form\Application\Config\RegistrationConfigType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RegistrationConfigController extends BaseController
{
    public function editAction(Request $request): Response
    {
        $config = $this->get('AppBundle\Repository\Application\Config\RegistrationConfigRepository')->findAll()[0];

        $form = $this->createForm(RegistrationConfigType::class, $config, [
            'method' => Request::METHOD_PUT,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('AppBundle\Repository\Application\Config\RegistrationConfigRepository')->save($config);

            return $this->redirectToRoute('sky_admin_servers_index');
        }

        return $this->render(
            '@App/admin/registration_config/edit.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }
}