<?php

namespace AppBundle\Controller\Admin;


use AppBundle\Controller\BaseController;
use AppBundle\Entity\Application\Items\ItemInfo;
use AppBundle\Entity\Application\MarketCategory;
use AppBundle\Form\Application\MarketCategoryType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MarketCategoryController extends BaseController
{

    public function indexAction(Request $request): Response
    {
        return $this->render(
            '@App/admin/market_category/index.html.twig',
            [
                'categories' => $this->getDoctrine()->getManager()->getRepository(MarketCategory::class)->findAll(),
            ]
        );
    }

    public function createAction(Request $request): Response
    {
        $category = new MarketCategory();

        $form = $this->createForm(MarketCategoryType::class, $category);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var ItemInfo $item */
            foreach ($category->getItems() as $item) {
                $item->setMarketCategory($category);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('sky_admin_market.category_edit', ['id' => $category->getId()]);
        }

        return $this->render(
            '@App/admin/market_category/create.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    public function removeAction(MarketCategory $category, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($category);
        $em->flush();

        return $this->getRedirectBackResponse($request);
    }

    public function editAction(MarketCategory $category, Request $request): Response
    {
        $originalItems = clone $category->getItems();

        $form = $this->createForm(MarketCategoryType::class, $category, [
            'method' => Request::METHOD_PUT,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if ($items = $form->get('items')) {

                /** @var ItemInfo $item */
                foreach ($category->getItems() as $item) {
                    $item->setMarketCategory($category);
                }

                foreach ($originalItems as $item) {
                    if (!$category->getItems()->contains($item)) {
                        $item->setMarketCategory(null);
//                        $em->remove($item);
                    }
                }

            }

            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('sky_admin_market.category_edit', ['id' => $category->getId()]);
        }

        return $this->render(
            '@App/admin/market_category/edit.html.twig',
            [
                'form' => $form->createView(),
            ]
        );

    }

}