<?php

namespace AppBundle\Controller\Admin;


use AppBundle\Controller\BaseController;
use AppBundle\Entity\Application\DownloadLink\Group;
use AppBundle\Form\Application\DownloadLink\GroupType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DownloadLinkController extends BaseController
{
    public function indexAction(Request $request): Response
    {
        return $this->render(
            '@App/admin/download_link/index.html.twig',
            [
                'groups' => $this->getDoctrine()->getManager()->getRepository(Group::class)->findAll(),
            ]
        );
    }

    public function createAction(Request $request): Response
    {
        $group = new Group();

        $form = $this->createForm(GroupType::class, $group);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($group);
            $em->flush();

            return $this->redirectToRoute('sky_admin_download.link_index');
        }

        return $this->render(
            '@App/admin/download_link/create.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    public function removeAction(Group $group, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($group);
        $em->flush();

        return $this->getRedirectBackResponse($request);
    }

    public function editAction(Group $group, Request $request): Response
    {
        $originalLinks = clone $group->getLinks();

        $form = $this->createForm(GroupType::class, $group, [
            'method' => Request::METHOD_PUT,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if ($links = $form->get('links')) {
                foreach ($originalLinks as $link) {
                    if (!$group->getLinks()->contains($link)) {
                        $em->remove($link);
                    }
                }

            }

            $em->persist($group);
            $em->flush();

            return $this->redirectToRoute('sky_admin_download.link_index');
        }

        return $this->render(
            '@App/admin/download_link/edit.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }
}