<?php

declare(strict_types = 1);

namespace AppBundle\Controller\Admin;


use AppBundle\BonusCode\Model\BonusCode;
use AppBundle\Controller\BaseController;
use AppBundle\Mailer\Factory\MailerFactory;
use AppBundle\Mailer\Mailer\Mailer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MailerController extends BaseController
{
    public function indexAction(): Response
    {
        return $this->render(
            '@App/admin/mailer/index.html.twig',
            [
                'mails' => [
                    $this->createOBTInvitationPreviewMailNode(),
                    $this->createOpeningSoonPreviewMailNode(),
                ]
            ]
        );
    }

#   OBT Invitation
    public function obtInvitationRealSendAction(Request $request): Response
    {
        $this->get('sky_app.app.promotion.obt_invitation.service.sender')->send();

        return $this->getRedirectBackResponse($request);
    }

    public function obtInvitationTestSendAction(Request $request): Response
    {
        $this->createOBTInvitationTestMails()->send();

        return $this->getRedirectBackResponse($request);
    }


    private function createOBTInvitationPreviewMailNode(): array
    {
        return [
            'id' => md5('OBT Invitation'),
            'name' => 'OBT Invitation',
            'previews' => $this
                ->createOBTInvitationTestMails()
                ->messages()
                ->map(function (\Swift_Message $message): string {
                    return $message->getBody();
                }),
            'send_real' => 'sky_admin_mailer_obt.invitation_send_real',
            'send_test' => 'sky_admin_mailer_obt.invitation_send_test',
        ];
    }

    private function createOBTInvitationTestMails(): Mailer
    {
        return $this->mailerFactory()
            ->createDynamicMailerFactory(
                $this->getUser()->email(),
                'email.obt_invitation.20',
                '@App/email/other/obt_invitation.html.twig',
                [
                    'bonus' => new BonusCode($this->getCurrentServer(), 'ABCDEFG', 1, new \DateTime()),
                ]
            );
    }

#   Opening Soon
    public function openingSoonRealSendAction(Request $request): Response
    {
        $this->get('sky_app.app.promotion.opening_soon.service.sender')->send();

        return $this->getRedirectBackResponse($request);
    }

    public function openingSoonTestSendAction(Request $request): Response
    {
        $this->createOpeningSoonTestMails()->send();

        return $this->getRedirectBackResponse($request);
    }


    private function createOpeningSoonPreviewMailNode(): array
    {
        return [
            'id' => md5('Opening Soon'),
            'name' => 'Opening Soon',
            'previews' => $this
                ->createOpeningSoonTestMails()
                ->messages()
                ->map(function (\Swift_Message $message): string {
                    return $message->getBody();
                }),
            'send_real' => 'sky_admin_mailer_opening.soon_send_real',
            'send_test' => 'sky_admin_mailer_opening.soon_send_test',
        ];
    }

    private function createOpeningSoonTestMails(): Mailer
    {
        return $this->mailerFactory()
            ->createDynamicMailerFactory(
                $this->getUser()->email(),
                'email.opening_soon.0',
                '@App/email/other/opening_soon.html.twig'
            );
    }

    private function mailerFactory(): MailerFactory
    {
        return $this->container->get('sky_app.app.mailer.services.factory');
    }
}