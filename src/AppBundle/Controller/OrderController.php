<?php


namespace AppBundle\Controller;


use AppBundle\Controller\Helper\ItemToBuyViewTrait;
use AppBundle\Controller\Helper\PermissionTrait;
use AppBundle\Entity\Application\Payments\Order;
use AppBundle\Enum\Application\PaymentSystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class OrderController extends BaseController
{
    use PermissionTrait, ItemToBuyViewTrait;

    public function indexAction(Request $request): Response
    {
        $this->loggedInAccess()
            ->hasAccountAccess();

        $orders = $this->getCurrentUserServerAccount()->orders();

        return $this->render(
            '@App/order/index.html.twig',
            [
                'orders' => $orders,
            ]
        );
    }

    public function viewAction(Order $order, Request $request): Response
    {
        $this->orderAccess($order);

        return $this->render(
            '@App/order/view.html.twig',
            [
                'order' => $order,
                'items' => $this->itemsToView($order->items()->toArray()),
                'pay_pal' => [
                    'enabled' => $this->getParameter('sky_app.app.payments.pay_pal.enabled'),
                    'environment' => $this->getParameter('sky_app.app.payments.pay_pal.environment'),
                ]
            ]
        );
    }

    public function payAction(Order $order): Response
    {
        $this->orderAccess($order);

        $paymentUrl = $this->get('sky_app.app.service.payments.payment_processor')->createPaymentUrl($order);

        $this
            ->get('sky_app.app.slack.services.payment_notification')
            ->paymentStarted(PaymentSystem::get(PaymentSystem::INTERKASSA), $order);

        return $this->redirect($paymentUrl);
    }
}