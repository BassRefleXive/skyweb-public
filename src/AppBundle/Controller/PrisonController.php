<?php

declare(strict_types = 1);

namespace AppBundle\Controller;


use AppBundle\Entity\Application\User\Ban;
use AppBundle\Filter\PrisonFilter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PrisonController extends BaseController
{
    public function indexAction(Request $request): Response
    {
        $filter = PrisonFilter::buildFromRequest($request);
        $paginator = $this->getDoctrine()->getRepository(Ban::class)->findByQueryFilerPaginated($filter);

        return $this->render(
            '@App/prison/index.html.twig',
            [
                'bans' => $paginator->getIterator()->getArrayCopy(),
                'pagination' => [
                    'maxPages' => ceil($paginator->count() / PrisonFilter::PRISON_FILTER_LIMIT),
                    'thisPage' => $filter->getPage(),
                ],
            ]
        );
    }
}