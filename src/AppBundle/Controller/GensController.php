<?php


namespace AppBundle\Controller;


use AppBundle\Entity\MuOnline\Character;
use AppBundle\Entity\MuOnline\Gens;
use AppBundle\Filter\GensFilter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GensController extends BaseController
{

    public function indexAction(Request $request, int $family): Response
    {
        $gensFilter = GensFilter::buildFromRequest($family, $request);

        $gensPaginator = $this->get('sky_app.muo.service.gens')->getByFilterPaginated($gensFilter);

        $gensMembers = $gensPaginator->getIterator()->getArrayCopy();

        $users = $this->get('AppBundle\Service\Application\UserService')->getUsersByCharacters(
            array_map(
                function (Gens $gens): Character {
                    return $gens->character();
                },
                $gensMembers
            )
        );

        return $this->render(
            '@App/gens/index.html.twig',
            [
                'gens'        => $gensMembers,
                'users' => $users,
                'currentUser' => $this->getUser(),
                'family'      => $family,
                'pagination' => [
                    'maxPages' => ceil($gensPaginator->count() / GensFilter::FILTER_LIMIT),
                    'thisPage' => $gensFilter->getPage(),
                    'perPage' => GensFilter::FILTER_LIMIT,
                ],
            ]
        );
    }
}