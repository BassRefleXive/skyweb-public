<?php

namespace AppBundle\Controller;


use AppBundle\Controller\Helper\PermissionTrait;
use AppBundle\Doctrine\Filters\ActiveEntityFilter;
use AppBundle\Entity\Application\CartDiscountCoupon;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DiscountCouponController extends BaseController
{
    use PermissionTrait;

    public function getAction(string $coupon, Request $request): Response
    {
        $this->getAndSetEntityFilter($request, ActiveEntityFilter::NAME);

        /** @var CartDiscountCoupon $coupon */
        $coupon = $this->get('sky_app.app.repository.cart_discount_coupon')->findByText($coupon);

        if (!$coupon) {
            throw new NotFoundHttpException('Coupon not found');
        }

        $this->webShopDiscountCouponAccess($coupon);

        return new JsonResponse([
            'discount' => $coupon->getDiscount(),
            'type'     => $coupon->getType(),
        ]);
    }

}