<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Application\Server;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ServersController extends BaseController
{
    public function switchAction(Server $server, Request $request): Response
    {
        $this->get('sky_app.app.service.server')->setCurrentServer($server, $this->getUser());

        return $this->redirectToRoute('sky_index_index');
    }
}