<?php


namespace AppBundle\Controller;


use AppBundle\Entity\Application\DownloadLink\Group;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DownloadLinkController extends BaseController
{
    public function indexAction(Request $request): Response
    {
        return $this->render('@App/download_link/index.html.twig');
    }
}