<?php

declare(strict_types=1);

namespace AppBundle\Controller\Integrations\Top;

use AppBundle\Controller\BaseController;
use AppBundle\Service\Application\Votes\NotificationHandler\Provider\GTop100\Factory\RequestFactory;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GTop100Controller extends BaseController
{
    public function notificationAction(Request $request): Response
    {
        $this->get('AppBundle\Service\Application\Votes\NotificationHandler\Provider\GTop100\RequestHandler')
            ->handle(
                RequestFactory::build($request)
            );

        return new JsonResponse([
            'status' => Response::HTTP_OK,
            'message' => 'ok'
        ]);
    }
}