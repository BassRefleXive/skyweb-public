<?php

declare(strict_types = 1);

namespace AppBundle\Controller\Integrations\Payments;


use AppBundle\Controller\BaseController;
use AppBundle\Entity\Application\Payments\Order;
use AppBundle\Enum\Application\PaymentSystem;
use AppBundle\Slack\Services\NotificationService;
use AppBundle\Slack\Services\PaymentNotificationService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PayPalController extends BaseController
{
    public function initializeAction(Order $order, Request $request): Response
    {
        try {
            $transactionId = $this->get('sky_app.app.service.payments.paypal.payment_processor')->initialize($order);

            $this->slackNotificationService()->paymentStarted(PaymentSystem::byValue(PaymentSystem::PAYPAL), $order);

            return new JsonResponse(['id' => $transactionId]);
        } catch (\Throwable $e) {
            $this->slackNotificationService()->initializationError(PaymentSystem::byValue(PaymentSystem::PAYPAL), $e);

            return new Response('', Response::HTTP_BAD_REQUEST);
        }
    }

    public function finalizeAction(Order $order, Request $request): Response
    {
        try {
            $paymentId = $request->request->get('paymentID');
            $payerId = $request->request->get('payerID');

            $payment = $this->get('sky_app.app.service.payments.paypal.payment_processor')->finalize($order, $paymentId, $payerId);

            $this->slackNotificationService()->paymentSuccess(PaymentSystem::byValue(PaymentSystem::PAYPAL), $order);
            return new Response();
        } catch (\Throwable $e) {
            $this->slackNotificationService()->finalizationError(PaymentSystem::byValue(PaymentSystem::PAYPAL), $e);

            return new Response('', Response::HTTP_BAD_REQUEST);
        }
    }

    public function successAction(Request $request): Response
    {
        return $this->render('@App/integrations/payments/success.html.twig');
    }

    public function errorAction(Order $order, Request $request): Response
    {
        return $this->render(
            '@App/integrations/payments/error.html.twig',
            [
                'orderId' => $order->id(),
            ]
        );
    }

    private function slackNotificationService(): PaymentNotificationService
    {
        return $this->get('sky_app.app.slack.services.payment_notification');
    }
}