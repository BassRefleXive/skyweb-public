<?php

namespace AppBundle\Controller\Integrations\Payments;


use AppBundle\Controller\BaseController;
use AppBundle\Enum\Application\PaymentSystem;
use AppBundle\Exception\Application\Payments\PaymentSystem\PaymentAlreadyProcessedException;
use AppBundle\Service\Application\Payments\PaymentSystem\Interkassa\PaymentNotification;
use AppBundle\Slack\Services\PaymentNotificationService;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class InterkassaController extends BaseController
{
    public function successAction(Request $request): Response
    {
        $this->slackNotificationService()
            ->paymentSuccess(
                PaymentSystem::byValue(PaymentSystem::INTERKASSA),
                $this->get('sky_app.app.repository.payments.order')->find($this->paymentNumber($request))
            );

        return $this->render('@App/integrations/payments/success.html.twig');
    }

    public function errorAction(Request $request): Response
    {
        $this->slackNotificationService()
            ->paymentFailure(
                PaymentSystem::byValue(PaymentSystem::INTERKASSA),
                $this->get('sky_app.app.repository.payments.order')->find($this->paymentNumber($request))
            );

        return $this->render(
            '@App/integrations/payments/error.html.twig',
            [
                'orderId' => $request->request->get('ik_pm_no'),
            ]
        );
    }

    public function pendingAction(Request $request): Response
    {
        $this->slackNotificationService()
            ->paymentPending(
                PaymentSystem::byValue(PaymentSystem::INTERKASSA),
                $this->get('sky_app.app.repository.payments.order')->find($this->paymentNumber($request))
            );

        return $this->render('@App/integrations/payments/pending.html.twig');
    }

    public function interactionAction(Request $request): Response
    {
        try {
            $vars = $request->request->all();

            $this->getLogger()->info('Request', $vars);

            $notification = PaymentNotification::fromRequest($request);

            $this->get('sky_app.app.service.payments.payment_processor')->processPaymentNotification($notification);

            $code = Response::HTTP_OK;
        } catch (PaymentAlreadyProcessedException $e) {
            $this->getLogger()->error($e->getMessage(), $e->getTrace());

            $this->slackNotificationService()->interactionError(PaymentSystem::byValue(PaymentSystem::INTERKASSA), $e);

            $code = Response::HTTP_OK;
        } catch (\Throwable $e) {
            $this->getLogger()->error($e->getMessage(), $e->getTrace());

            $this->slackNotificationService()->interactionError(PaymentSystem::byValue(PaymentSystem::INTERKASSA), $e);

            $code = Response::HTTP_BAD_REQUEST;
        }

        return Response::create('', $code);
    }

    private function getLogger(): LoggerInterface
    {
        return $this->get('monolog.logger.payments.interkassa');
    }

    private function slackNotificationService(): PaymentNotificationService
    {
        return $this->get('sky_app.app.slack.services.payment_notification');
    }

    private function paymentNumber(Request $request): int
    {
        if ($request->query->has('ik_pm_no')) {
            return (int) $request->query->has('ik_pm_no');
        }

        return (int) $request->request->get('ik_pm_no');
    }
}