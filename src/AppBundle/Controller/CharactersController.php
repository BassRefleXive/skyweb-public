<?php

namespace AppBundle\Controller;


use AppBundle\Controller\Helper\CharacterTrait;
use AppBundle\Controller\Helper\PermissionTrait;
use AppBundle\Doctrine\Type\Mappings\Items\Item;
use AppBundle\Doctrine\Type\Mappings\MagicList\Skill;
use AppBundle\Entity\Application\User\User;
use AppBundle\Entity\MuOnline\Character;
use AppBundle\Entity\MuOnline\CharacterKillLog;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Exception\Application\RedirectBackException;
use AppBundle\Filter\CharacterFilter;
use AppBundle\Form\Application\Character\GrandResetType;
use AppBundle\Form\Application\Character\ResetMasterSkillTreeType;
use AppBundle\Form\Application\Character\ResetType;
use AppBundle\Form\Application\Character\SearchCharacterType;
use AppBundle\Form\Application\Character\ClassChangeType;
use AppBundle\Form\Application\Character\NickChangeType;
use AppBundle\Form\Application\Character\ResetStatsType;
use AppBundle\Model\Application\Character\ClassChangeModel;
use AppBundle\Model\Application\Character\GrandResetModel;
use AppBundle\Model\Application\Character\NickChangeModel;
use AppBundle\Model\Application\Character\ResetMasterSkillTreeModel;
use AppBundle\Model\Application\Character\ResetModel;
use AppBundle\Model\Application\Character\ResetStatsModel;
use AppBundle\Repository\MuOnline\AccountStateRepository;
use Collection\Map;
use Collection\Sequence;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CharactersController extends BaseController
{
    use PermissionTrait, CharacterTrait;

    public function indexAction(Request $request): Response
    {
        $form = $this->createForm(SearchCharacterType::class);

        $form->handleRequest($request);

        $characterFilter = CharacterFilter::buildFromRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $characterFilter->setNick($form->getData()->getNick());
        }

        $charactersPaginator = $this->get('sky_app.muo.service.character')->getByFilterPaginated($characterFilter);
        $characters = $charactersPaginator->getIterator()->getArrayCopy();

        $users = $this->get('AppBundle\Service\Application\UserService')->getUsersByCharacters($characters);

        return $this->render(
            '@App/character/index.html.twig',
            [
                'characters' => $characters,
                'currentUser' => $this->getUser(),
                'users' => $users,
                'searchForm' => $form->createView(),
                'pagination' => [
                    'maxPages' => ceil($charactersPaginator->count() / CharacterFilter::CHARACTER_FILTER_LIMIT),
                    'thisPage' => $characterFilter->getPage(),
                    'perPage' => CharacterFilter::CHARACTER_FILTER_LIMIT,
                ],
            ]
        );
    }

    public function viewAction(string $name, Request $request): Response
    {
        $character = $this->findCharacterByName($name);

        $account = null;

        $user = $this->getUser();

        if ($user) {
            $account = $this->get('sky_app.muo.service.account')->getAccountByUser($user);
        }

        $itemsParser = $this->get('sky_app.muo.service.item.parser.chain');

        $items = $character->getInventory()->map(function (Item $item) use ($itemsParser) {
            return $itemsParser->parse($item);
        });

        /** @var User $accountUser */
        $accountUser = $this->get('sky_app.app.repository.user')->findByLogin(['username' => $character->getAccount()->getMemberId()])[0];

        return $this->render('@App/character/view.html.twig', [
            'character' => $character,
            'currentAccount' => $account,
            'inventoryItems' => $items,
            'accountUser' => $accountUser,
            'accountUserServerAccount' => $accountUser->getUserServerAccountByServer($this->getCurrentServer()),
            'currentUser' => $this->getUser(),
            'buffItems' => $this->get('sky_app.app.service.item_buff_effect')->characterUsedBuffItems($character),
            'skills' => $this->get('sky_app.app.magic_list.processor')->process($character->magicList()),
            'availableMasterSkills' => $this->get('sky_app.app.magic_list.repository.skill')->findMasterSkills($character->classInfo()),
        ]);
    }

    public function changeNickAction(string $name, Request $request): Response
    {
        $character = $this->findCharacterByName($name);

        $this
            ->characterAccess($character)
            ->offlineAccess();

        $form = $this->createForm(
            NickChangeType::class,
            (new NickChangeModel())->setCharacter($character),
            [
                'method' => Request::METHOD_PUT,
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {
                /** @var NickChangeModel $model */
                $model = $form->getData();

                $this->get('sky_app.muo.service.character')->changeNick($model->getCharacter(), $model->getNick(), $model->getPrice());

                $this->addFlash('success', 'change_nick.4');

                return $this->redirectToRoute('sky_accounts_index');
            } catch (DisplayableException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
            }
        }

        return $this->render(
            '@App/character/change_nick.html.twig',
            [
                'form' => $form->createView(),
                'nick' => $name,
            ]
        );
    }

    public function changeClassAction(string $name, Request $request): Response
    {
        $character = $this->findCharacterByName($name);

        $this
            ->characterAccess($character)
            ->offlineAccess();

        $form = $this->createForm(
            ClassChangeType::class,
            (new ClassChangeModel())->setCharacter($character),
            [
                'method' => Request::METHOD_PUT,
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {
                /** @var ClassChangeModel $model */
                $model = $form->getData();

                $this->get('sky_app.muo.service.character')->changeClass($model->getCharacter(), $model->getClass(), $model->getPrice());

                $this->addFlash('success', 'change_class.4');

                return $this->redirectToRoute('sky_accounts_index');
            } catch (DisplayableException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
            }
        }

        return $this->render(
            '@App/character/change_class.html.twig',
            [
                'form' => $form->createView(),
                'nick' => $name,
            ]
        );
    }

    public function resetStatsAction(string $name, Request $request): Response
    {
        $character = $this->findCharacterByName($name);

        $this
            ->characterAccess($character)
            ->offlineAccess();

        $dayOfWeek = (int) (new \DateTime())->format('N');

        if (!in_array($dayOfWeek, range(1, 4), true)) {
            throw new RedirectBackException('reset_stats.4');
        }

        $form = $this->createForm(
            ResetStatsType::class,
            (new ResetStatsModel())->setCharacter($character),
            [
                'method' => Request::METHOD_PUT,
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {
                /** @var ResetStatsModel $model */
                $model = $form->getData();

                $this->get('sky_app.muo.service.character')->resetStats($model->getCharacter(), $model->getPrice());

                $this->addFlash('success', 'reset_stats.3');

                return $this->redirectToRoute('sky_accounts_index');
            } catch (DisplayableException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
            }
        }

        return $this->render(
            '@App/character/reset_stats.html.twig',
            [
                'form' => $form->createView(),
                'nick' => $name,
            ]
        );
    }

    public function resetAction(string $name, Request $request): Response
    {
        $character = $this->findCharacterByName($name);

        $this->characterAccess($character);

        $form = $this->createForm(ResetType::class, (new ResetModel())->setCharacter($character));

        $form->handleRequest($request);

        $userServerAccount = $this->getCurrentUserServerAccount();

        if ($form->isSubmitted() && $form->isValid()) {
            $this->offlineAccess();

            try {
                /** @var ResetModel $model */
                $model = $form->getData();

                $this->get('sky_app.muo.service.character')->reset($model->getCharacter(), $userServerAccount);

                $this->addFlash('success', 'reset.22');

                return $this->redirectToRoute('sky_accounts_index');
            } catch (DisplayableException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
            }
        }

        $ravenEquipped = false;

        if ($character->classInfo()->isDarkLord()) {
            if (null !== $item = $character->getInventory()->get(1)) {
                $shieldSlotItem = $this->get('sky_app.muo.service.item.parser.chain')->parse($item);

                if (!$shieldSlotItem->isEmpty() && $shieldSlotItem->getItemInfo()->getGroup()->isGroup13() && 5 === $shieldSlotItem->getItemInfo()->getItemId()) {
                    $ravenEquipped = true;
                }
            }
        }

        return $this->render(
            '@App/character/reset.html.twig',
            [
                'form' => $form->createView(),
                'nick' => $name,
                'config' => $this->getCurrentServer()->getCharacterEvolutionConfig(),
                'character' => $character,
                'isOffline' => AccountStateRepository::STATE_OFFLINE === $this->getCurrentAccount()->getState()->getConnectionState(),
                'storageZen' => $userServerAccount->zen(),
                'ravenEquipped' => $ravenEquipped,
                'isDarkLord' => $character->classInfo()->isDarkLord(),
            ]
        );
    }

    public function killLogAction(string $name, Request $request): Response
    {
        $character = $this->findCharacterByName($name);

        $this->characterAccess($character);

        $kills = $character->kills()->slice(0, 15);
        $killedBy = $character->killedBy()->slice(0, 15);

        $users = new Map();

        if (!$this->getCurrentUserServerAccount()->isVip()) {
            $users = $this->get('AppBundle\Service\Application\UserService')
                ->getUsersByCharacters(
                    array_map(function (CharacterKillLog $log): Character {
                        return $log->killer();
                    }, $killedBy)
                );
        }

        return $this->render(
            '@App/character/kill_log.html.twig',
            [
                'character' => $character,
                'kills' => $kills,
                'killedBy' => $killedBy,
                'users' => $users,
            ]
        );
    }

    public function grandResetAction(string $name, Request $request): Response
    {
        $character = $this->findCharacterByName($name);

        $this->characterAccess($character);

        $form = $this->createForm(GrandResetType::class, (new GrandResetModel())->setCharacter($character));

        $form->handleRequest($request);

        $userServerAccount = $this->getCurrentUserServerAccount();

        if ($form->isSubmitted() && $form->isValid()) {
            $this->offlineAccess();

            try {
                /** @var GrandResetModel $model */
                $model = $form->getData();

                $this->get('sky_app.muo.service.character')->grandReset($model->getCharacter(), $userServerAccount);

                $this->addFlash('success', 'grand_reset.17');

                return $this->redirectToRoute('sky_accounts_index');
            } catch (DisplayableException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
            }
        }

        return $this->render(
            '@App/character/grand_reset.html.twig',
            [
                'form' => $form->createView(),
                'nick' => $name,
                'config' => $this->getCurrentServer()->getCharacterEvolutionConfig(),
                'character' => $character,
                'isOffline' => AccountStateRepository::STATE_OFFLINE === $this->getCurrentAccount()->getState()->getConnectionState(),
                'storageZen' => $userServerAccount->zen(),
            ]
        );
    }

    public function resetMasterSkillAction(string $name, Request $request): Response
    {
        $character = $this->findCharacterByName($name);

        $this->characterAccess($character);

        $form = $this->createForm(
            ResetMasterSkillTreeType::class,
            (new ResetMasterSkillTreeModel())->setCharacter($character)
        );

        $form->handleRequest($request);

        $userServerAccount = $this->getCurrentUserServerAccount();

        if ($form->isSubmitted() && $form->isValid()) {
            $this->offlineAccess();

            try {
                /** @var ResetMasterSkillTreeModel $model */
                $model = $form->getData();

                $this->get('sky_app.muo.service.character')->resetMasterSkillTree($model->getCharacter(), $userServerAccount);

                $this->addFlash('success', 'reset_master_skill_tree.1');

                return $this->redirectToRoute('sky_accounts_index');
            } catch (DisplayableException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
            }
        }

        return $this->render(
            '@App/character/reset_master_skill_tree.html.twig',
            [
                'form' => $form->createView(),
                'nick' => $name,
            ]
        );
    }
}