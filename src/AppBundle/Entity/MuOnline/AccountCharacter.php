<?php


namespace AppBundle\Entity\MuOnline;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="AccountCharacter")
 */
class AccountCharacter
{
    /**
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\MuOnline\Account", inversedBy="currentAccountCharacter", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="Id", referencedColumnName="memb___id")
     */
    private $account;

    /**
     * @ORM\Column(type="string", name="GameIDC", length=10, nullable=true)
     */
    private $currentCharacter;

    public function account(): Account
    {
        return $this->account;
    }

    /**
     * @return string|null
     */
    public function currentCharacter()
    {
        return $this->currentCharacter;
    }
}