<?php

namespace AppBundle\Entity\MuOnline;

use AppBundle\Entity\Application\Config\VipLevelConfig;
use AppBundle\Exception\Application\DisplayableException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MuOnline\AccountRepository")
 * @ORM\EntityListeners({"AppBundle\Doctrine\EntityListeners\AccountListener"})
 * @ORM\Table(name="MEMB_INFO")
 */
class Account
{
    public const STATUS_ACTIVE = 0;
    public const STATUS_BLOCKED = 1;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="string", name="memb___id", length=10, nullable=false)
     */
    private $memberId;

    /**
     * @ORM\Column(type="string", name="memb__pwd", length=10, nullable=false)
     */
    private $password;

    /**
     * @ORM\Column(type="string", name="memb_name", length=10, nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(type="string", name="sno__numb", length=13, nullable=false)
     */
    private $serial;

    /**
     * @ORM\Column(type="string", name="post_code", length=6, nullable=true)
     */
    private $postalCode;

    /**
     * @ORM\Column(type="string", name="addr_info", length=50, nullable=true)
     */
    private $addressInfo;

    /**
     * @ORM\Column(type="string", name="addr_deta", length=50, nullable=true)
     */
    private $addressDeta;

    /**
     * @ORM\Column(type="string", name="tel__numb", length=20, nullable=true)
     */
    private $telNumber;

    /**
     * @ORM\Column(type="string", name="phon_numb", length=15, nullable=true)
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="string", name="mail_addr", length=50, nullable=false)
     */
    private $email;

    /**
     * @ORM\Column(type="string", name="fpas_ques", length=50, nullable=true)
     */
    private $secretQuestion;

    /**
     * @ORM\Column(type="string", name="fpas_answ", length=50, nullable=true)
     */
    private $secretAnswer;

    /**
     * @ORM\Column(type="string", name="job__code", length=2, nullable=true)
     */
    private $jobCode;

    /**
     * @ORM\Column(type="datetime", name="appl_days", length=23, nullable=true)
     */
    private $applDays;

    /**
     * @ORM\Column(type="datetime", name="modi_days", length=23, nullable=true)
     */
    private $modiDays;

    /**
     * @ORM\Column(type="datetime", name="out__days", length=23, nullable=true)
     */
    private $outDays;

    /**
     * @ORM\Column(type="datetime", name="true_days", length=23, nullable=true)
     */
    private $trueDays;

    /**
     * @ORM\Column(type="string", name="mail_chek", length=2, nullable=true)
     */
    private $mailCheck;

    /**
     * @ORM\Column(type="string", name="bloc_code", length=2, nullable=false)
     */
    private $blocCode;

    /**
     * @ORM\Column(type="string", name="ctl1_code", length=2, nullable=false)
     */
    private $ctl1Code;

    /**
     * @ORM\Column(type="integer", name="cspoints", nullable=true)
     */
    private $csPoints;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\MuOnline\Warehouse", mappedBy="account", cascade={"persist"})
     */
    private $warehouse;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\MuOnline\Character", mappedBy="account")
     */
    private $characters;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\MuOnline\AccountState", mappedBy="account", cascade={"persist"})
     */
    private $state;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\MuOnline\AccountCharacter", mappedBy="account")
     */
    private $currentAccountCharacter;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\MuOnline\AccountVipState", mappedBy="account", cascade={"persist"})
     */
    private $vipState;


    public function __construct()
    {
        $this->serial = substr(implode('', range(0, 11)), 0, 13);
        $this->characters = new ArrayCollection();
        $this->state = (new AccountState())->setAccount($this);
        $this->csPoints = 0;
    }

    public function getMemberId(): string
    {
        return $this->memberId;
    }

    public function setMemberId(string $memberId): self
    {
        $this->memberId = $memberId;

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSerial(): string
    {
        return $this->serial;
    }

    public function setSerial(string $serial): self
    {
        $this->serial = $serial;

        return $this;
    }

    public function getPostalCode()
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getAddressInfo()
    {
        return $this->addressInfo;
    }

    public function setAddressInfo(string $addressInfo): self
    {
        $this->addressInfo = $addressInfo;

        return $this;
    }

    public function getAddressDeta()
    {
        return $this->addressDeta;
    }

    public function setAddressDeta(string $addressDeta): self
    {
        $this->addressDeta = $addressDeta;

        return $this;
    }

    public function getTelNumber()
    {
        return $this->telNumber;
    }

    public function setTelNumber(string $telNumber): self
    {
        $this->telNumber = $telNumber;

        return $this;
    }

    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getSecretQuestion()
    {
        return $this->secretQuestion;
    }

    public function setSecretQuestion(string $secretQuestion): self
    {
        $this->secretQuestion = $secretQuestion;

        return $this;
    }

    public function getSecretAnswer()
    {
        return $this->secretAnswer;
    }

    public function setSecretAnswer(string $secretAnswer): self
    {
        $this->secretAnswer = $secretAnswer;

        return $this;
    }

    public function getJobCode()
    {
        return $this->jobCode;
    }

    public function setJobCode(string $jobCode): self
    {
        $this->jobCode = $jobCode;

        return $this;
    }

    public function getApplDays()
    {
        return $this->applDays;
    }

    public function setApplDays(\DateTime $applDays): self
    {
        $this->applDays = $applDays;

        return $this;
    }

    public function getModiDays()
    {
        return $this->modiDays;
    }

    public function setModiDays(\DateTime $modiDays): self
    {
        $this->modiDays = $modiDays;

        return $this;
    }

    public function getOutDays()
    {
        return $this->outDays;
    }

    public function setOutDays(\DateTime $outDays): self
    {
        $this->outDays = $outDays;

        return $this;
    }

    public function getTrueDays()
    {
        return $this->trueDays;
    }

    public function setTrueDays(\DateTime $trueDays): self
    {
        $this->trueDays = $trueDays;

        return $this;
    }

    public function getMailCheck()
    {
        return $this->mailCheck;
    }

    public function setMailCheck(string $mailCheck): self
    {
        $this->mailCheck = $mailCheck;

        return $this;
    }

    public function getBlocCode(): string
    {
        return $this->blocCode;
    }

    public function setBlocCode(int $blocCode): self
    {
        $this->blocCode = $blocCode;

        return $this;
    }

    public function getCtl1Code(): string
    {
        return $this->ctl1Code;
    }

    public function setCtl1Code(string $ctl1Code): self
    {
        $this->ctl1Code = $ctl1Code;

        return $this;
    }

    public function getCsPoints()
    {
        return $this->csPoints;
    }

    public function setCsPoints(int $csPoints): self
    {
        $this->csPoints = $csPoints;

        return $this;
    }

    public function setWarehouse(Warehouse $warehouse): self
    {
        $this->warehouse = $warehouse;

        return $this;
    }

    /**
     * @return Warehouse
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }

    public function getCharacters(): Collection
    {
        $this->characters === null && $this->characters = new ArrayCollection();

        return $this->characters;
    }

    public function getState(): AccountState
    {
        $this->state === null && $this->state = (new AccountState())->setAccount($this);

        return $this->state;
    }

    public function setState(AccountState $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function currentAccountCharacter(): AccountCharacter
    {
        return $this->currentAccountCharacter;
    }

    public function getVipState(): AccountVipState
    {
        if (null === $this->vipState) {
            $this->vipState = new AccountVipState();
            $this->vipState->setAccount($this);
        }

        return $this->vipState;
    }

    public function setVipState(AccountVipState $vipState): self
    {
        $this->vipState = $vipState;

        return $this;
    }

    public function isVipActive(): bool
    {
        return $this->getVipState()->isActive();
    }

    public function activateVip(VipLevelConfig $vipLevel, int $days): self
    {
        if ($this->isVipActive() && $this->getVipState()->getType() !== $vipLevel->getType()) {
            throw new DisplayableException('vip.purchase.1');
        }

        if ($this->isVipActive()) {
            $this->getVipState()->addDays($days);
        } else {
            $this->getVipState()->activate($vipLevel, $days);
        }

        return $this;
    }
}