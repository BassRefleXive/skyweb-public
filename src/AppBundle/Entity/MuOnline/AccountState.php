<?php

namespace AppBundle\Entity\MuOnline;

use AppBundle\Enum\MuOnline\AccountConnectionType;
use AppBundle\Repository\MuOnline\AccountStateRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MuOnline\AccountStateRepository")
 * @ORM\Table(name="MEMB_STAT")
 */
class AccountState
{
    /**
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\MuOnline\Account", inversedBy="state", fetch="EAGER")
     * @ORM\JoinColumn(name="memb___id", referencedColumnName="memb___id")
     */
    private $account;

    /**
     * @ORM\Column(type="integer", name="ConnectStat", nullable=true)
     */
    private $connectionState;

    /**
     * @ORM\Column(type="string", name="ServerName")
     */
    private $serverName;

    /**
     * @ORM\Column(type="string", name="IP")
     */
    private $ip;

    /**
     * @ORM\Column(type="datetime", name="ConnectTM")
     */
    private $connectedAt;

    /**
     * @ORM\Column(type="datetime", name="DisConnectTM")
     */
    private $disconnectedAt;

    /**
     * @ORM\Column(type="integer", name="TotalSpentTime")
     */
    private $totalSpentTime;

    public function __construct()
    {
        $this->totalSpentTime = 0;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    public function setAccount(Account $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getConnectionState()
    {
        return $this->connectionState;
    }

    public function setConnectionState(int $connectionState): self
    {
        if (!in_array($connectionState, [AccountStateRepository::STATE_OFFLINE, AccountStateRepository::STATE_ONLINE], true)) {
            throw new \InvalidArgumentException('Invalid Account State.');
        }

        $this->connectionState = $connectionState;

        return $this;
    }

    public function getServerName()
    {
        return $this->serverName;
    }

    public function setServerName(string $serverName): self
    {
        $this->serverName = $serverName;

        return $this;
    }

    public function getIp()
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getConnectedAt(): ?\DateTime
    {
        return $this->connectedAt;
    }

    public function setConnectedAt(\DateTime $connectedAt): self
    {
        $this->connectedAt = $connectedAt;

        return $this;
    }

    public function getDisconnectedAt()
    {
        return $this->disconnectedAt;
    }

    public function setDisconnectedAt(\DateTime $disconnectedAt): self
    {
        $this->disconnectedAt = $disconnectedAt;

        return $this;
    }

    public function getTotalSpentTime(): int
    {
        return $this->totalSpentTime;
    }

    public function setTotalSpentTime(int $totalSpentTime): self
    {
        $this->totalSpentTime = $totalSpentTime;

        return $this;
    }
}