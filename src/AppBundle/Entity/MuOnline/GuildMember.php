<?php

namespace AppBundle\Entity\MuOnline;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MuOnline\GuildMemberRepository")
 * @ORM\Table(name="GuildMember")
 */
class GuildMember
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="Name")
     */
    protected $name;

    /**
     * @ORM\Column(type="enum_guild_member_status", name="G_Status")
     */
    protected $status;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\MuOnline\Guild", inversedBy="members")
     * @ORM\JoinColumn(name="G_Name", referencedColumnName="G_Name")
     */
    protected $guild;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\MuOnline\Character", inversedBy="guildMember")
     * @ORM\JoinColumn(name="Name", referencedColumnName="Name")
     */
    protected $character;

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name): GuildMember
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Guild
     */
    public function getGuild()
    {
        return $this->guild;
    }

    public function setGuild(Guild $guild): GuildMember
    {
        $this->guild = $guild;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus(int $status): GuildMember
    {
        $this->status = $status;

        return $this;
    }

    public function getCharacter()
    {
        return $this->character;
    }

    public function setCharacter(Character $character): GuildMember
    {
        $this->character = $character;

        return $this;
    }

}