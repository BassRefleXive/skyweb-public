<?php

declare(strict_types = 1);

namespace AppBundle\Entity\MuOnline;


use AppBundle\Enum\MuOnline\PeriodItemType;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MuOnline\PeriodItemInfoRepository")
 * @ORM\Table(name="IGC_PeriodItemInfo")
 */
class PeriodItemInfo
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="ItemType", type="period_item_type")
     */
    private $type;

    /**
     * @ORM\Column(name="ItemCode", type="integer")
     */
    private $code;

    /**
     * @ORM\Column(name="Duration", type="integer")
     */
    private $duration;

    /**
     * @ORM\Column(name="BuyDate", type="integer_timestamp")
     */
    private $boughtAt;

    /**
     * @ORM\Column(name="ExpireDate", type="integer_timestamp")
     */
    private $expireAt;

    /**
     * @ORM\Column(name="UsedInfo", type="boolean")
     */
    private $active;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\MuOnline\Character", inversedBy="periodItems", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="CharacterName", referencedColumnName="Name")
     */
    private $character;

    public function id()
    {
        return $this->id;
    }

    public function type(): PeriodItemType
    {
        return $this->type;
    }

    public function code(): int
    {
        return $this->code;
    }

    public function duration(): int
    {
        return $this->duration;
    }

    public function boughtAt(): \DateTimeInterface
    {
        return $this->boughtAt;
    }

    public function expireAt(): \DateTimeInterface
    {
        return $this->expireAt;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function character(): Character
    {
        return $this->character;
    }

    public function secondsLeft(): int
    {
        return $this->expireAt()->getTimestamp() - (new \DateTime())->getTimestamp();
    }
}