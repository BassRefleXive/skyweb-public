<?php

declare(strict_types = 1);

namespace AppBundle\Entity\MuOnline;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="C_PlayerKiller_Info")
 */
class CharacterKillLog
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="Id")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\MuOnline\Character", inversedBy="killedBy", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="Victim", referencedColumnName="Name")
     */
    private $victim;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\MuOnline\Character", inversedBy="kills", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="Killer", referencedColumnName="Name")
     */
    private $killer;

    /**
     * @ORM\Column(name="KillDate", type="datetime", nullable=false)
     */
    private $date;

    public function victim(): Character
    {
        return $this->victim;
    }

    public function killer(): Character
    {
        return $this->killer;
    }

    public function date(): \DateTime
    {
        return $this->date;
    }
}