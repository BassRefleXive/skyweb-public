<?php

namespace AppBundle\Entity\MuOnline;

use AppBundle\Doctrine\Type\Mappings\Items\Items;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MuOnline\AccountRepository")
 * @ORM\Table(name="warehouse")
 */
class Warehouse
{
    /**
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\MuOnline\Account", inversedBy="warehouse", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="AccountID", referencedColumnName="memb___id")
     */
    private $account;

    /**
     * @ORM\Column(type="muo_items", name="Items", nullable=true)
     */
    private $items;

    /**
     * @ORM\Column(type="integer", name="Money", nullable=true)
     */
    private $money;

    /**
     * @ORM\Column(type="datetime", name="EndUseDate", length=16, nullable=true)
     */
    private $endUseDate;

    /**
     * @ORM\Column(type="integer", name="pw", length=6, nullable=true)
     */
    private $password;

    /**
     * @ORM\Column(type="boolean", name="WHOpen")
     */
    private $opened;

    public function getItems(): Items
    {
        return clone $this->items;
    }

    public function setItems(Items $items): self
    {
        $this->items = $items;

        return $this;
    }

    public function getMoney(): int
    {
        return $this->money ?? 0;
    }

    public function setMoney(int $money): self
    {
        $this->money = $money;

        return $this;
    }

    public function getEndUseDate()
    {
        return $this->endUseDate;
    }

    public function setEndUseDate(\DateTime $endUseDate): self
    {
        $this->endUseDate = $endUseDate;

        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword(int $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getAccount(): Account
    {
        return $this->account;
    }

    public function setAccount(Account $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function opened(): bool
    {
        return $this->opened;
    }
}