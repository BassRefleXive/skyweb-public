<?php

namespace AppBundle\Entity\MuOnline;

use AppBundle\Doctrine\Type\GuildMemberStatusEnum;
use AppBundle\Doctrine\Type\Mappings\GuildMark;
use Collection\Sequence;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MuOnline\GuildRepository")
 * @ORM\Table(name="Guild")
 */
class Guild
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="G_Name")
     */
    private $name;

    /**
     * @ORM\Column(type="muo_guild_mark", name="G_Mark")
     */
    private $mark;

    /**
     * @ORM\Column(type="smallint", name="G_Score")
     */
    private $score;

    /**
     * @ORM\Column(type="string", name="G_Notice")
     */
    private $notice;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\MuOnline\Character", inversedBy="ownedGuild")
     * @ORM\JoinColumn(name="G_Master", referencedColumnName="Name")
     */
    private $master;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\MuOnline\GuildMember", mappedBy="guild")
     */
    private $members;

    public function __construct()
    {
        $this->members = new ArrayCollection();
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMark()
    {
        return clone $this->mark;
    }

    public function setMark(GuildMark $mark): self
    {
        $this->mark = $mark;

        return $this;
    }

    public function getScore()
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getNotice()
    {
        return $this->notice;
    }

    public function setNotice(string $notice): self
    {
        $this->notice = $notice;

        return $this;
    }

    public function getMembers()
    {
        $this->members === null && $this->members = new ArrayCollection();

        return $this->members;
    }

    public function getMaster(): Character
    {
        return $this->master;
    }

}