<?php

declare(strict_types=1);

namespace AppBundle\Entity\MuOnline\CastleSiege;

use AppBundle\Entity\MuOnline\Guild;
use AppBundle\Enum\MuOnline\CastleSiege\RegisteredGuildState;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MuOnline\CastleSiege\SiegeRegisteredGuildRepository")
 * @ORM\Table(name="MuCastle_REG_SIEGE")
 */
class SiegeRegisteredGuild
{
    /**
     * @ORM\Id()
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\MuOnline\Guild", fetch="EAGER")
     * @ORM\JoinColumn(name="REG_SIEGE_GUILD", referencedColumnName="G_Name")
     */
    private $guild;

    /**
     * @ORM\Column(type="integer", name="REG_MARKS", nullable=false)
     */
    private $signCount;

    /**
     * @ORM\Column(type="registered_guild_state", name="IS_GIVEUP")
     */
    private $state;

    public function guild(): Guild
    {
        return $this->guild;
    }

    public function signCount(): int
    {
        return $this->signCount;
    }

    public function state(): RegisteredGuildState
    {
        return $this->state;
    }
}