<?php

declare(strict_types=1);

namespace AppBundle\Entity\MuOnline\CastleSiege;

use AppBundle\Entity\MuOnline\Guild;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MuOnline\CastleSiege\CastleDataRepository")
 * @ORM\Table(name="MuCastle_DATA")
 */
class CastleData
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="integer", name="MAP_SVR_GROUP")
     */
    private $mapSvrGroup;

    /**
     * @ORM\Column(type="datetime", name="SIEGE_START_DATE", nullable=false)
     */
    private $startDate;

    /**
     * @ORM\Column(type="datetime", name="SIEGE_END_DATE", nullable=false)
     */
    private $endDate;

    /**
     * @var Guild
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\MuOnline\Guild")
     * @ORM\JoinColumn(name="OWNER_GUILD", referencedColumnName="G_Name")
     */
    private $owner;

    /**
     * @ORM\Column(type="integer", name="MONEY", nullable=false)
     */
    private $money;

    /**
     * @ORM\Column(type="integer", name="TAX_RATE_CHAOS", nullable=false)
     */
    private $chaosTaxRate;

    /**
     * @ORM\Column(type="integer", name="TAX_RATE_STORE", nullable=false)
     */
    private $storeTaxRate;

    /**
     * @ORM\Column(type="integer", name="TAX_HUNT_ZONE", nullable=false)
     */
    private $huntZoneTax;

    public function startDate(): \DateTime
    {
        return $this->startDate;
    }

    public function endDate(): \DateTime
    {
        return $this->endDate;
    }

    public function owner(): ?Guild
    {
        return '' !== $this->owner->getName()
            ? $this->owner
            : null;
    }

    public function money(): int
    {
        return $this->money;
    }

    public function chaosTaxRate(): int
    {
        return $this->chaosTaxRate;
    }

    public function storeTaxRate(): int
    {
        return $this->storeTaxRate;
    }

    public function huntZoneTax(): int
    {
        return $this->huntZoneTax;
    }
}