<?php

namespace AppBundle\Entity\MuOnline;

use AppBundle\Doctrine\Type\Mappings\Items\Items;
use AppBundle\Doctrine\Type\Mappings\MagicList\MagicList;
use AppBundle\Enum\MuOnline\CharacterClass;
use AppBundle\Service\Application\ItemService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MuOnline\CharacterRepository")
 * @ORM\Table(name="Character")
 */
class Character
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="Name")
     */
    private $name;

    /**
     * @ORM\Column(type="smallint", name="cLevel", nullable=true)
     */
    private $level;


    /**
     * @ORM\Column(type="smallint", name="mLevel", nullable=true)
     */
    private $masterLevel;

    /**
     * @ORM\Column(type="smallint", name="Resets", nullable=true)
     */
    private $reset;

    /**
     * @ORM\Column(type="smallint", name="GRAND_RESETS")
     */
    private $grandReset;

    /**
     * @ORM\Column(type="integer", name="LevelUpPoint", nullable=true)
     */
    private $points;

    /**
     * @ORM\Column(type="integer", name="mlPoint", nullable=true)
     */
    private $masterPoints;

    /**
     * @ORM\Column(type="smallint", name="Class", nullable=true)
     */
    private $class;

    /**
     * @ORM\Column(type="integer", name="Experience", nullable=true)
     */
    private $exp;

    /**
     * @ORM\Column(type="integer", name="Strength", nullable=true)
     */
    private $strength;

    /**
     * @ORM\Column(type="integer", name="Dexterity", nullable=true)
     */
    private $agility;

    /**
     * @ORM\Column(type="integer", name="Vitality", nullable=true)
     */
    private $vitality;

    /**
     * @ORM\Column(type="integer", name="Energy", nullable=true)
     */
    private $energy;

    /**
     * @ORM\Column(type="integer", name="Leadership", nullable=true)
     */
    private $stamina;

    /**
     * @ORM\Column(type="muo_items", name="Inventory", nullable=true)
     */
    private $inventory;

    /**
     * @ORM\Column(type="integer", name="Money", nullable=true)
     */
    private $zen;

    /**
     * @ORM\Column(type="smallint", name="MapNumber", nullable=true)
     */
    private $mapNumber;

    /**
     * @ORM\Column(type="smallint", name="MapPosX", nullable=true)
     */
    private $mapX;

    /**
     * @ORM\Column(type="smallint", name="MapPosY", nullable=true)
     */
    private $mapY;

    /**
     * @ORM\Column(type="integer", name="PkCount", nullable=true)
     */
    private $pkCount;

    /**
     * @ORM\Column(type="integer", name="PkLevel", nullable=true)
     */
    private $pkLevel;

    /**
     * @ORM\Column(type="integer", name="PkTime", nullable=true)
     */
    private $pkTime;

    /**
     * @ORM\Column(type="smallint", name="CtlCode", nullable=true)
     */
    private $ctlCode;

    /**
     * @ORM\Column(type="integer", name="FruitPoint", nullable=true)
     */
    private $fruitPoints;

    /**
     * @ORM\Column(type="integer", name="PermanentStats")
     */
    private $permanentStats;

    /**
     * @ORM\Column(type="integer", name="WinDuels")
     */
    private $wonDuels;

    /**
     * @ORM\Column(type="integer", name="LoseDuels")
     */
    private $loosedDuels;

    /**
     * @ORM\Column(type="muo_magic_list", name="MagicList", nullable=false)
     */
    private $magicList;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\MuOnline\Account", inversedBy="characters", fetch="EAGER")
     * @ORM\JoinColumn(name="AccountID", referencedColumnName="memb___id")
     */
    private $account;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\MuOnline\GuildMember", mappedBy="character")
     */
    private $guildMember;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\MuOnline\Guild", mappedBy="master")
     */
    private $ownedGuild;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\MuOnline\Gens", mappedBy="character", fetch="EAGER")
     */
    private $gens;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\MuOnline\CharacterKillLog", mappedBy="victim")
     * @ORM\OrderBy({"date" = "DESC"})
     */
    private $killedBy;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\MuOnline\CharacterKillLog", mappedBy="killer")
     * @ORM\OrderBy({"date" = "DESC"})
     */
    private $kills;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\MuOnline\PeriodItemInfo", mappedBy="character", fetch="EXTRA_LAZY")
     */
    private $periodItems;

    public function __construct()
    {
        $this->level = 1;
        $this->reset = 0;
        $this->grandReset = 0;
        $this->zen = 0;

        $this->permanentStats = 0;

        $this->killedBy = new ArrayCollection();
        $this->kills = new ArrayCollection();
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLevel()
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function masterLevel(): ?int
    {
        return $this->masterLevel;
    }

    public function getReset()
    {
        return $this->reset;
    }

    public function setReset(int $reset): self
    {
        $this->reset = $reset;

        return $this;
    }

    public function getGrandReset()
    {
        return $this->grandReset;
    }

    public function setGrandReset(int $grandReset): self
    {
        $this->grandReset = $grandReset;

        return $this;
    }

    public function getPoints()
    {
        return $this->points;
    }

    public function setPoints(int $points): self
    {
        $this->points = $points;

        return $this;
    }

    public function masterPoints(): ?int
    {
        return $this->masterPoints;
    }

    public function updateMasterPoints(int $points): self
    {
        $this->masterPoints = $points;

        return $this;
    }

    public function getClass()
    {
        return $this->class;
    }

    public function setClass(int $class): self
    {
        $this->class = $class;

        return $this;
    }

    public function classInfo(): CharacterClass
    {
        return CharacterClass::byValue($this->class);
    }

    public function getExp()
    {
        return $this->exp;
    }

    public function setExp(int $exp): self
    {
        $this->exp = $exp;

        return $this;
    }

    public function getStrength()
    {
        return $this->strength;
    }

    public function setStrength(int $strength): self
    {
        $this->strength = $strength;

        return $this;
    }

    public function getAgility()
    {
        return $this->agility;
    }

    public function setAgility(int $agility): self
    {
        $this->agility = $agility;

        return $this;
    }

    public function getVitality()
    {
        return $this->vitality;
    }

    public function setVitality(int $vitality): self
    {
        $this->vitality = $vitality;

        return $this;
    }

    public function getEnergy()
    {
        return $this->energy;
    }

    public function setEnergy(int $energy): self
    {
        $this->energy = $energy;

        return $this;
    }

    public function getStamina()
    {
        return $this->stamina;
    }

    public function setStamina(int $stamina): self
    {
        $this->stamina = $stamina;

        return $this;
    }

    public function getInventory()
    {
        return $this->inventory instanceof Items
            ? clone $this->inventory->take(12)
            : new ArrayCollection();
    }

    public function completeInventory()
    {
        return $this->inventory instanceof Items
            ? clone $this->inventory
            : new ArrayCollection();
    }

    public function equippedItemsCellsEmpty(ItemService $itemService): bool
    {
        foreach ($this->getInventory() as $item) {
            if (!$itemService->parse($item)->isEmpty()) {
                return false;
            }
        }

        return true;
    }

    public function magicList(): MagicList
    {
        return $this->magicList;
    }

    public function updateMagicList(MagicList $magicList): self
    {
        $this->magicList = $magicList;

        return $this;
    }

    public function getZen()
    {
        return $this->zen;
    }

    public function setZen(int $zen): self
    {
        $this->zen = $zen;

        return $this;
    }

    public function getMapNumber()
    {
        return $this->mapNumber;
    }

    public function setMapNumber(int $mapNumber): self
    {
        $this->mapNumber = $mapNumber;

        return $this;
    }

    public function getMapX()
    {
        return $this->mapX;
    }

    public function setMapX(int $mapX): self
    {
        $this->mapX = $mapX;

        return $this;
    }

    public function getMapY()
    {
        return $this->mapY;
    }

    public function setMapY(int $mapY): self
    {
        $this->mapY = $mapY;

        return $this;
    }

    public function getPkCount()
    {
        return $this->pkCount;
    }

    public function setPkCount(int $pkCount): self
    {
        $this->pkCount = $pkCount;

        return $this;
    }

    public function getPkLevel()
    {
        return $this->pkLevel;
    }

    public function setPkLevel(int $pkLevel): self
    {
        $this->pkLevel = $pkLevel;

        return $this;
    }

    public function getPkTime()
    {
        return $this->pkTime;
    }

    public function setPkTime(int $pkTime): self
    {
        $this->pkTime = $pkTime;

        return $this;
    }

    public function getCtlCode()
    {
        return $this->ctlCode;
    }

    public function setCtlCode(int $ctlCode): self
    {
        $this->ctlCode = $ctlCode;

        return $this;
    }

    public function getFruitPoints()
    {
        return $this->fruitPoints;
    }

    public function setFruitPoints(int $fruitPoints): self
    {
        $this->fruitPoints = $fruitPoints;

        return $this;
    }

    public function getPermanentStats()
    {
        return $this->permanentStats;
    }

    public function setPermanentStats(int $permanentStats): self
    {
        $this->permanentStats = $permanentStats;

        return $this;
    }

    public function wonDuels(): int
    {
        return $this->wonDuels;
    }

    public function loosedDuels(): int
    {
        return $this->loosedDuels;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    public function setAccount(Account $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function ownedGuild(): ?Guild
    {
        return $this->guildMember;
    }

    public function getGuildMember(): ?GuildMember
    {
        return $this->guildMember;
    }

    public function setGuildMember(GuildMember $guildMember): self
    {
        $this->guildMember = $guildMember;

        return $this;
    }

    public function isOnline(): bool
    {
        return (
            $this->getName() === $this->getAccount()->currentAccountCharacter()->currentCharacter() &&
            $this->getAccount()->getState()->getConnectionState()
        );
    }

    public function isInGensFamily(): bool
    {
        return null !== $this->gens() && null !== $this->gens()->family();
    }

    public function gens(): ?Gens
    {
        return $this->gens;
    }

    public function killedBy(): Collection
    {
        null === $this->killedBy && $this->killedBy = new ArrayCollection();

        return $this->killedBy;
    }

    public function kills(): Collection
    {
        null === $this->kills && $this->kills = new ArrayCollection();

        return $this->kills;
    }

    public function periodItems(): Collection
    {
        null === $this->periodItems && $this->periodItems = new ArrayCollection();

        return $this->periodItems;
    }
}