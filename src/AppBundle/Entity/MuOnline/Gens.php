<?php


namespace AppBundle\Entity\MuOnline;


use Doctrine\ORM\Mapping as ORM;
use AppBundle\Enum\MuOnline\Gens\Family;
use AppBundle\Enum\MuOnline\Gens\Rank;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MuOnline\GensRepository")
 * @ORM\Table(name="IGC_Gens")
 */
class Gens
{
    /**
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\MuOnline\Character", inversedBy="gens")
     * @ORM\JoinColumn(name="Name", referencedColumnName="Name")
     */
    private $character;

    /**
     * @ORM\Column(type="gens_family", name="Influence", nullable=false)
     */
    private $family;

    /**
     * @ORM\Column(type="smallint", name="Rank", nullable=false)
     */
    private $rank;

    /**
     * @ORM\Column(type="smallint", name="Points", nullable=false)
     */
    private $points;

    public function character(): Character
    {
        return $this->character;
    }

    public function family(): ?Family
    {
        return $this->family;
    }

    public function rank(): int
    {
        return $this->rank;
    }

    public function points(): int
    {
        return $this->points;
    }

    public function rankInfo(): Rank
    {
        return Rank::byGens($this);
    }
}