<?php


namespace AppBundle\Entity\MuOnline;

use AppBundle\Entity\Application\Config\VipLevelConfig;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="T_VIPList")
 */
class AccountVipState
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\MuOnline\Account", inversedBy="vipState")
     * @ORM\JoinColumn(name="AccountID", referencedColumnName="memb___id")
     */
    private $account;

    /**
     * @ORM\Column(type="smalldatetime", name="Date", nullable=false)
     */
    private $date;

    /**
     * @ORM\Column(type="integer", name="Type", nullable=false)
     */
    private $type;

    public function __construct()
    {
        $this->date = new \DateTime();
    }

    public function getAccount(): Account
    {
        return $this->account;
    }

    public function setAccount(Account $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getDate(): ?\DateTime
    {
        return clone $this->date;
    }

    public function setDate(\DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function isActive(): bool
    {
        return null !== $this->getDate() && $this->getDate() > new \DateTime();
    }

    public function addDays(int $days): self
    {
        $this->setDate($this->getDate()->add(new \DateInterval(sprintf('P%dD', $days))));

        return $this;
    }

    public function activate(VipLevelConfig $vipLevel, int $days): self
    {
        $this->setType($vipLevel->getType())
            ->setDate((new \DateTime())->add(new \DateInterval(sprintf('P%dD', $days))));

        return $this;
    }
}