<?php

namespace AppBundle\Entity\Application\PurchaseLog;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="StatsPurchaseLogItem")
 */
class StatsPurchaseLogItem extends PurchaseLogItem
{

    /**
     * @ORM\Column(type="integer")
     */
    protected $stats;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\PurchaseLog\CartPurchaseLog", inversedBy="stats")
     * @ORM\JoinColumn(name="cart_purchase_item_log_id", referencedColumnName="id")
     */
    protected $cartPurchaseLog;

    public function getStats()
    {
        return $this->stats;
    }

    public function setStats(int $stats): StatsPurchaseLogItem
    {
        $this->stats = $stats;

        return $this;
    }

}