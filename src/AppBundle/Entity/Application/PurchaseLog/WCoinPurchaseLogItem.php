<?php

namespace AppBundle\Entity\Application\PurchaseLog;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="WCoinPurchaseLogItem")
 */
class WCoinPurchaseLogItem extends PurchaseLogItem
{
    /**
     * @ORM\Column(type="integer")
     */
    private $coins;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\PurchaseLog\CartPurchaseLog", inversedBy="coins")
     * @ORM\JoinColumn(name="cart_purchase_item_log_id", referencedColumnName="id")
     */
    protected $cartPurchaseLog;

    public function getCoins()
    {
        return $this->coins;
    }

    public function setCoins(int $coins): self
    {
        $this->coins = $coins;

        return $this;
    }
}