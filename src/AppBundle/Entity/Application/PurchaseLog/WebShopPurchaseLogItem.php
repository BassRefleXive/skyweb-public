<?php

namespace AppBundle\Entity\Application\PurchaseLog;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(
 *      name="WebShopPurchaseLogItem",
 *      uniqueConstraints={
 *          @ORM\UniqueConstraint(name="WebShopPurchaseLogItem_serial_UNIQUE", columns={"first_serial", "second_serial"})
 *      }
 * )
 */
class WebShopPurchaseLogItem extends PurchaseLogItem
{

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $item;

    /**
     * @ORM\Column(type="integer")
     */
    protected $firstSerial;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $secondSerial;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\PurchaseLog\CartPurchaseLog", inversedBy="items")
     * @ORM\JoinColumn(name="cart_purchase_item_log_id", referencedColumnName="id")
     */
    protected $cartPurchaseLog;

    public function getItem()
    {
        return $this->item;
    }

    public function setItem(string $item): WebShopPurchaseLogItem
    {
        $this->item = $item;

        return $this;
    }

    public function getFirstSerial()
    {
        return $this->firstSerial;
    }

    public function setFirstSerial(int $firstSerial): WebShopPurchaseLogItem
    {
        $this->firstSerial = $firstSerial;

        return $this;
    }

    public function getSecondSerial()
    {
        return $this->secondSerial;
    }

    public function setSecondSerial(int $secondSerial): WebShopPurchaseLogItem
    {
        $this->secondSerial = $secondSerial;

        return $this;
    }

}