<?php

namespace AppBundle\Entity\Application\PurchaseLog;

use Doctrine\ORM\Mapping as ORM;

abstract class PurchaseLogItem
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="smallint")
     */
    protected $priceCredits;

    /**
     * @ORM\Column(type="float")
     */
    protected $priceMoney;

    protected $cartPurchaseLog;

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getPriceCredits()
    {
        return $this->priceCredits;
    }

    public function setPriceCredits(int $priceCredits): self
    {
        $this->priceCredits = $priceCredits;

        return $this;
    }

    public function getPriceMoney()
    {
        return $this->priceMoney;
    }

    public function setPriceMoney(float $priceMoney): self
    {
        $this->priceMoney = $priceMoney;

        return $this;
    }

    public function getCartPurchaseLog()
    {
        return $this->cartPurchaseLog;
    }

    public function setCartPurchaseLog(CartPurchaseLog $cartPurchaseLog): self
    {
        $this->cartPurchaseLog = $cartPurchaseLog;

        return $this;
    }
}