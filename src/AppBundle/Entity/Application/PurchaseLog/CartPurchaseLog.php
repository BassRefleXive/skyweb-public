<?php

namespace AppBundle\Entity\Application\PurchaseLog;


use AppBundle\Doctrine\Type\CartPurchaseTypeEnum;
use AppBundle\Entity\Application\CartPurchaseDiscount;
use AppBundle\Entity\Application\UserServerAccount;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Traits\Timestampable;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Application\CartPurchaseLogRepository")
 * @ORM\Table(name="CartPurchaseLog")
 */
class CartPurchaseLog
{
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     */
    private $priceCredits;

    /**
     * @ORM\Column(type="float")
     */
    private $priceMoney;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\UserServerAccount", inversedBy="cartPurchaseLog")
     * @ORM\JoinColumn(name="user_server_account_id", referencedColumnName="id")
     */
    private $userServerAccount;

    /**
     * @ORM\Column(type="enum_cart_purchase_type")
     */
    private $type;

    /**
     * @ORM\Column(type="float")
     */
    private $finalPrice;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\PurchaseLog\WebShopPurchaseLogItem", mappedBy="cartPurchaseLog", cascade={"persist"}, orphanRemoval=true)
     */
    private $items;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\PurchaseLog\StatsPurchaseLogItem", mappedBy="cartPurchaseLog", cascade={"persist"}, orphanRemoval=true)
     */
    private $stats;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\PurchaseLog\WCoinPurchaseLogItem", mappedBy="cartPurchaseLog", cascade={"persist"}, orphanRemoval=true)
     */
    private $coins;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\CartPurchaseDiscount", mappedBy="purchaseLog", cascade={"persist"})
     */
    private $discounts;

    public function __construct()
    {
        $this->items = new ArrayCollection();
        $this->discounts = new ArrayCollection();
        $this->stats = new ArrayCollection();
        $this->coins = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getPriceCredits()
    {
        return $this->priceCredits;
    }

    public function setPriceCredits(int $priceCredits): self
    {
        $this->priceCredits = $priceCredits;

        return $this;
    }

    public function getPriceMoney()
    {
        return $this->priceMoney;
    }

    public function setPriceMoney(float $priceMoney): self
    {
        $this->priceMoney = $priceMoney;

        return $this;
    }

    public function getUserServerAccount()
    {
        return $this->userServerAccount;
    }

    public function setUserServerAccount(UserServerAccount $userServerAccount): self
    {
        $this->userServerAccount = $userServerAccount;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        if (!array_key_exists($type, CartPurchaseTypeEnum::getTypes())) {
            throw new \InvalidArgumentException('Invalid WebShop purchase type.');
        }

        $this->type = $type;

        return $this;
    }

    public function getFinalPrice()
    {
        return $this->finalPrice;
    }

    public function setFinalPrice(float $finalPrice): self
    {
        $this->finalPrice = $finalPrice;

        return $this;
    }

    public function getItems(): Collection
    {
        $this->items === null && $this->items = new ArrayCollection();

        return $this->items;
    }

    public function addItem(WebShopPurchaseLogItem $item): self
    {
        $item->setCartPurchaseLog($this);
        $this->getItems()->add($item);

        return $this;
    }

    public function getDiscounts(): Collection
    {
        $this->discounts === null && $this->discounts = new ArrayCollection();

        return $this->discounts;
    }

    public function addDiscount(CartPurchaseDiscount $discount): self
    {
        $discount->setPurchaseLog($this);
        $this->getDiscounts()->add($discount);

        return $this;
    }

    public function getStats(): Collection
    {
        $this->stats === null && $this->stats = new ArrayCollection();

        return $this->stats;
    }

    public function addStats(StatsPurchaseLogItem $stats): self
    {
        $stats->setCartPurchaseLog($this);
        $this->getStats()->add($stats);

        return $this;
    }

    public function getCoins(): Collection
    {
        $this->coins === null && $this->coins = new ArrayCollection();

        return $this->coins;
    }

    public function addCoins(WCoinPurchaseLogItem $coins): self
    {
        $coins->setCartPurchaseLog($this);
        $this->getCoins()->add($coins);

        return $this;
    }
}