<?php

namespace AppBundle\Entity\Application;

use AppBundle\Doctrine\Type\CartPersistentDiscountTypeEnum;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Application\CartPersistentDiscountRepository")
 * @ORM\Table(
 *      name="CartPersistentDiscount",
 *      uniqueConstraints={
 *          @ORM\UniqueConstraint(name="CartPersistentDiscount_range_UNIQUE", columns={"server_id", "spent_from", "spent_to"})
 *      }
 * )
 */
class CartPersistentDiscount
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="enum_cart_persistent_discount_type")
     */
    protected $type;

    /**
     * @ORM\Column(type="integer")
     */
    protected $discount;

    /**
     * @ORM\Column(type="integer")
     */
    protected $spentFrom;

    /**
     * @ORM\Column(type="integer")
     */
    protected $spentTo;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\Server", inversedBy="cartPersistentDiscounts")
     * @ORM\JoinColumn(name="server_id", referencedColumnName="id")
     */
    protected $server;

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): CartPersistentDiscount
    {
        $this->id = $id;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType(string $type): CartPersistentDiscount
    {
        if (!array_key_exists($type, CartPersistentDiscountTypeEnum::getTypes())) {
            throw new \InvalidArgumentException('Invalid CartPersistentDiscount type.');
        }

        $this->type = $type;

        return $this;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function setDiscount(int $discount): CartPersistentDiscount
    {
        $this->discount = $discount;

        return $this;
    }

    public function getSpentFrom()
    {
        return $this->spentFrom;
    }

    public function setSpentFrom(int $spentFrom): CartPersistentDiscount
    {
        $this->spentFrom = $spentFrom;

        return $this;
    }

    public function getSpentTo()
    {
        return $this->spentTo;
    }

    public function setSpentTo(int $spentTo): CartPersistentDiscount
    {
        $this->spentTo = $spentTo;

        return $this;
    }

    /**
     * @return Server
     */
    public function getServer()
    {
        return $this->server;
    }

    public function setServer(Server $server): CartPersistentDiscount
    {
        $this->server = $server;

        return $this;
    }
}