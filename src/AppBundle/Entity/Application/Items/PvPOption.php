<?php

namespace AppBundle\Entity\Application\Items;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="PvPOption")
 */
class PvPOption
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="json_array")
     */
    private $options;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\Items\ItemInfo", mappedBy="pvpOption")
     */
    private $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function getItems(): Collection
    {
        return $this->items;
    }

}