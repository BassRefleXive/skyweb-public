<?php

namespace AppBundle\Entity\Application\Items;

use AppBundle\Entity\Application\Items\Groups\Group;
use Collection\Sequence;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Application\GroupHarmonyOptionRepository")
 * @ORM\Table(name="GroupHarmonyOption")
 */
class GroupHarmonyOption
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\Items\Groups\Group", inversedBy="harmonyOptions", fetch="EAGER")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    protected $group;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\Items\HarmonyOption", inversedBy="groups", fetch="EAGER")
     * @ORM\JoinColumn(name="harmony_option_id", referencedColumnName="id")
     */
    protected $harmonyOption;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\Items\GroupHarmonyOptionLevel", mappedBy="groupHarmonyOption")
     */
    protected $levels;

    /**
     * @ORM\Column(type="integer")
     */
    protected $type;

    public function __construct()
    {
        $this->levels = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getGroup(): Group
    {
        return $this->group;
    }

    public function getHarmonyOption(): HarmonyOption
    {
        return $this->harmonyOption;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function getLevels(): Collection
    {
        $this->levels === null && $this->levels = new ArrayCollection();

        return $this->levels;
    }

    public function getLevelByValue(int $value)
    {
        return (new Sequence($this->getLevels()->toArray()))->find(function (GroupHarmonyOptionLevel $level) use ($value) {
            return $level->getValue() === $value;
        })->getOrElse(null);
    }

    public function getValueByLevel(int $level)
    {
        return (new Sequence($this->getLevels()->toArray()))->find(function (GroupHarmonyOptionLevel $levelToCmp) use ($level) {
            return $levelToCmp->getLevel() === $level;
        })->getOrElse(null);
    }

    public function getLowestLevel(): GroupHarmonyOptionLevel
    {
        return (new Sequence($this->getLevels()->toArray()))
            ->sortWith(function (GroupHarmonyOptionLevel $a, GroupHarmonyOptionLevel $b) {
                return $a->getLevel() <=> $b->getLevel();
            })
            ->head();
    }

}