<?php

namespace AppBundle\Entity\Application\Items;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="GroupHarmonyOptionLevel")
 */
class GroupHarmonyOptionLevel
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $value;

    /**
     * @ORM\Column(type="integer")
     */
    protected $level;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\Items\GroupHarmonyOption", inversedBy="levels")
     * @ORM\JoinColumn(name="group_harmony_option_id", referencedColumnName="id")
     */
    protected $groupHarmonyOption;

    public function getLevel(): int
    {
        return $this->level;
    }

    public function getValue(): int
    {
        return $this->value;
    }

}