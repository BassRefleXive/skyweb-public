<?php

namespace AppBundle\Entity\Application\Items;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="AncientSet")
 */
class AncientSet
{

    const ANCIENT_BONUS_5 = 5;
    const ANCIENT_BONUS_10 = 10;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enabled;

    /**
     * @ORM\Column(type="json_array")
     */
    protected $options;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\Items\AncientSetItem", mappedBy="ancientSet")
     */
    protected $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function getItems(): Collection
    {
        return $this->items;
    }

    public function isEnabled(): bool
    {
        return !!$this->enabled;
    }
}