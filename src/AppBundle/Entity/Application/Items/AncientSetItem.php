<?php

namespace AppBundle\Entity\Application\Items;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="AncientSetItem")
 */
class AncientSetItem
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $bonus;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\Items\AncientSet", inversedBy="items")
     * @ORM\JoinColumn(name="ancient_set_id", referencedColumnName="id")
     */
    protected $ancientSet;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\Items\ItemInfo", inversedBy="ancientSets")
     * @ORM\JoinColumn(name="item_info_id", referencedColumnName="id")
     */
    protected $item;

    public function getId()
    {
        return $this->id;
    }

    public function getBonus(): int
    {
        return $this->bonus;
    }

    public function getAncientSet(): AncientSet
    {
        return $this->ancientSet;
    }

    public function getItem(): ItemInfo
    {
        return $this->item;
    }

}