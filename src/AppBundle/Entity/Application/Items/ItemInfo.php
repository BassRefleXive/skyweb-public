<?php

namespace AppBundle\Entity\Application\Items;


use AppBundle\Doctrine\Type\ExcellentOptionsEnum;
use AppBundle\Entity\Application\CharacterClass;
use AppBundle\Entity\Application\Items\Groups\Group;
use AppBundle\Entity\Application\Items\Groups\Group12;
use AppBundle\Entity\Application\Items\Groups\Group13;
use AppBundle\Entity\Application\MarketCategory;
use AppBundle\Entity\Application\MarketItem;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Application\ItemInfoRepository")
 * @ORM\Table(
 *     name="ItemInfo",
 *     indexes={
 *          @ORM\Index(name="idx_item_id", columns={"item_id"})
 *      }
 * )
 *
 * @ORM\NamedNativeQueries({
 *      @ORM\NamedNativeQuery(
 *          name="findByCombinedCodes",
 *          resultClass="AppBundle\Entity\Application\Items\ItemInfo",
 *          query="SELECT * FROM ItemInfo ii WHERE (ii.group_id - 1) * 512 + ii.item_id IN (:codes)"
 *      )
 * })
 */
class ItemInfo
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $itemId;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $level;

    /**
     * @ORM\Column(type="integer")
     */
    private $x;

    /**
     * @ORM\Column(type="integer")
     */
    private $y;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $dropLevel;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $durability;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $attackSpeed;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $defense;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $minDmg;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $maxDmg;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wizardryDmg;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wizardryExcellentDmg;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $reqStr;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $reqAgi;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $reqEne;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $reqLvl;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $skill;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $buffEffect;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\Items\Groups\Group", inversedBy="items", fetch="EAGER")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    private $group;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\Items\PvPOption", inversedBy="items", fetch="EAGER")
     * @ORM\JoinColumn(name="pvp_option_id", referencedColumnName="id")
     */
    private $pvpOption;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\Items\AncientSetItem", mappedBy="item")
     */
    private $ancientSets;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Application\CharacterClass", inversedBy="equippedItems")
     * @ORM\JoinTable(name="character_class_items")
     */
    private $equippedBy;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\WebShopCategoryItem", mappedBy="item", cascade={"persist"})
     */
    private $webShopCategoryItems;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\MarketCategory", inversedBy="items")
     * @ORM\JoinColumn(name="market_category_id", referencedColumnName="id")
     */
    private $marketCategory;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\MarketItem", mappedBy="itemInfo")
     */
    private $marketItems;

    public function __construct()
    {
        $this->ancientSets = new ArrayCollection();
        $this->equippedBy = new ArrayCollection();
        $this->webShopCategoryItems = new ArrayCollection();
        $this->marketItems = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getItemId()
    {
        return $this->itemId;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getLevel()
    {
        return $this->level;
    }

    public function getX()
    {
        return $this->x;
    }

    public function getY()
    {
        return $this->y;
    }

    public function getDropLevel()
    {
        return $this->dropLevel;
    }

    public function getDurability()
    {
        return $this->durability;
    }

    public function getAttackSpeed()
    {
        return $this->attackSpeed;
    }

    public function getDefense()
    {
        return $this->defense;
    }

    public function getMinDmg()
    {
        return $this->minDmg;
    }

    public function getMaxDmg()
    {
        return $this->maxDmg;
    }

    public function getWizardryDmg()
    {
        return $this->wizardryDmg;
    }

    public function getWizardryExcellentDmg()
    {
        return $this->wizardryExcellentDmg;
    }

    public function getReqStr()
    {
        return $this->reqStr;
    }

    public function getReqAgi()
    {
        return $this->reqAgi;
    }

    public function getReqEne()
    {
        return $this->reqEne;
    }

    public function getReqLvl()
    {
        return $this->reqLvl;
    }

    public function getSkill()
    {
        return $this->skill;
    }

    public function getGroup(): Group
    {
        return $this->group;
    }

    public function getPvpOption()
    {
        return $this->pvpOption;
    }

    public function getAncientSets(): Collection
    {
        $this->ancientSets === null && $this->ancientSets = new ArrayCollection();

        return $this->ancientSets->filter(function (AncientSetItem $ancientSetItem): bool {
            return $ancientSetItem->getAncientSet()->isEnabled();
        });
    }

    public function getEquippedBy(): Collection
    {
        $this->equippedBy === null && $this->equippedBy = new ArrayCollection();

        return $this->equippedBy;
    }

    public function addEquippedBy(CharacterClass $characterClass): self
    {
        $this->equippedBy->add($characterClass);

        return $this;
    }

    public function getMarketCategory()
    {
        return $this->marketCategory;
    }

    public function setMarketCategory(MarketCategory $marketCategory = null): self
    {
        $this->marketCategory = $marketCategory;

        return $this;
    }

    public function getMarketItems(): Collection
    {
        $this->marketItems === null && $this->marketItems = new ArrayCollection();

        return $this->marketItems;
    }

    public function setMarketItems(Collection $items): self
    {
        $this->marketItems = $items;

        return $this;
    }

    public function addMarketItem(MarketItem $item): self
    {
        $item->setItemInfo($this);

        $this->getMarketItems()->add($item);

        return $this;
    }

    public function removeMarketItem(MarketItem $item): self
    {
        $items = $this->getMarketItems();

        if (($index = $items->indexOf($item)) !== false) {
            $items->remove($index);
        }

        return $this;
    }

    public function buffEffect(): string
    {
        return $this->buffEffect;
    }

    public function getAvailableExcellentOptions(): array
    {
        $result = [];

        if ($this->getGroup()->isWeapons() || ($this->getGroup()->isGroup13() && $this->itemId !== Group13::getCapeOfLordId())) {
            $result = ExcellentOptionsEnum::getWeaponTypes();
        }

        if ($this->getGroup()->isArmors() || $this->getGroup()->isShields()) {
            $result = ExcellentOptionsEnum::getArmorTypes();
        }

        if ($this->getGroup()->isGroup12() && in_array($this->itemId, Group12::getWingsIds())) {
            if (in_array($this->getItemId(), Group12::getThirdWingsIds())) {
                $result = ExcellentOptionsEnum::getThirdWingsTypes();
            } else {
                $result = ExcellentOptionsEnum::getSecondWingsTypes();
            }
        }

        if ($this->getGroup()->isGroup13() && $this->getItemId() === Group13::getCapeOfLordId()) {
            $result = ExcellentOptionsEnum::getCapeOfLordTypes();
        }

        if ($this->getGroup()->isGroup12() && $this->getItemId() === Group12::getCapeOfFighterId()) {
            $result = ExcellentOptionsEnum::getCapeOfFighterTypes();
        }

        if ($this->getGroup()->isGroup13() && $this->getItemId() === Group13::getFenrirId()) {
            $result = ExcellentOptionsEnum::getFenrirTypes();
        }

        switch (true) {
            case $this->getGroup()->isGroup13():
                {
                    $res = [
                            Group13::rubyRingId() => ExcellentOptionsEnum::rubyRingTypes(),
                            Group13::sapphireRingId() => ExcellentOptionsEnum::sapphireRingTypes(),
                            Group13::topazRingId() => ExcellentOptionsEnum::topazRingTypes(),
                            Group13::amethystRingId() => ExcellentOptionsEnum::amethystRingTypes(),
                            Group13::rubyNecklaceId() => ExcellentOptionsEnum::rubyNecklaceTypes(),
                            Group13::sapphireNecklaceId() => ExcellentOptionsEnum::sapphireNecklaceTypes(),
                            Group13::emeraldNecklaceId() => ExcellentOptionsEnum::emeraldNecklaceTypes(),
                        ][$this->getItemId()] ?? null;

                    $res !== null && ($result = $res);
                    break;
                }
        }

        return $result;
    }
}