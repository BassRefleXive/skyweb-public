<?php


namespace AppBundle\Entity\Application\Items;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Application\Item\SocketOptionsRepository")
 * @ORM\Table(name="SocketOption")
 */
class SocketOption
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $option;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $title;

    public function option(): int
    {
        return $this->option;
    }

    public function title(): string
    {
        return $this->title;
    }
}