<?php

namespace AppBundle\Entity\Application\Items\Groups;

use AppBundle\Doctrine\Type\ItemGroupEnum;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class StaffsGroup extends Group
{
    public function __construct()
    {
        parent::__construct();
        $this->name = 'Staffs';
    }

    public function getCountableItemIds(): array
    {
        return [];
    }

    public function getTypeId(): int
    {
        return ItemGroupEnum::ITEM_GROUP_STAFF;
    }
}