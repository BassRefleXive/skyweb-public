<?php

namespace AppBundle\Entity\Application\Items\Groups;

use AppBundle\Doctrine\Type\ItemGroupEnum;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class AxesGroup extends Group
{
    public function __construct()
    {
        parent::__construct();
        $this->name = 'Axes';
    }

    public function getCountableItemIds(): array
    {
        return [];
    }

    public function getTypeId(): int
    {
        return ItemGroupEnum::ITEM_GROUP_AXE;
    }
}