<?php

namespace AppBundle\Entity\Application\Items\Groups;

use AppBundle\Doctrine\Type\ItemGroupEnum;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class PantsGroup extends Group
{
    public function __construct()
    {
        parent::__construct();
        $this->name = 'Pants';
    }

    public function getCountableItemIds(): array
    {
        return [];
    }

    public function getTypeId(): int
    {
        return ItemGroupEnum::ITEM_GROUP_PANTS;
    }
}