<?php

namespace AppBundle\Entity\Application\Items\Groups;

use AppBundle\Doctrine\Type\ItemGroupEnum;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Group12 extends Group
{
    public function __construct()
    {
        parent::__construct();
        $this->name = '12';
    }

    public static function getWingsIds(): array
    {
        return [0, 1, 2, 3, 4, 5, 6, 6, 36, 37, 38, 39, 40, 41, 42, 43, 49, 50, 130, 131, 132, 133, 134, 135, 262, 263, 264, 265];
    }

    public static function getFirstWingsIds(): array
    {
        return [0, 1, 2];
    }

    public static function getThirdWingsIds(): array
    {
        return [36, 37, 38, 39, 40, 41, 43, 50];
    }

    public static function getCapeOfFighterId(): int
    {
        return 49;
    }

    public function getCountableItemIds(): array
    {
        return [];
    }

    public function getTypeId(): int
    {
        return ItemGroupEnum::ITEM_GROUP_12;
    }

}