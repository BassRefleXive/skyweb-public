<?php

namespace AppBundle\Entity\Application\Items\Groups;

use AppBundle\Doctrine\Type\ItemGroupEnum;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Group14 extends Group
{
    public function __construct()
    {
        parent::__construct();
        $this->name = '14';
    }

    public function getCountableItemIds(): array
    {
        return [
            21,
            29,
            88,
            89,
            90
        ];
    }

    public function getTypeId(): int
    {
        return ItemGroupEnum::ITEM_GROUP_14;
    }
}