<?php

namespace AppBundle\Entity\Application\Items\Groups;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Doctrine\Type\ItemGroupEnum;


/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="ItemGroup")
 *
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type_id", type="enum_item_group")
 * @ORM\DiscriminatorMap({
 *     ItemGroupEnum::ITEM_GROUP_SWORD = "AppBundle\Entity\Application\Items\Groups\SwordsGroup",
 *     ItemGroupEnum::ITEM_GROUP_AXE = "AppBundle\Entity\Application\Items\Groups\AxesGroup",
 *     ItemGroupEnum::ITEM_GROUP_SCEPTER = "AppBundle\Entity\Application\Items\Groups\SceptersGroup",
 *     ItemGroupEnum::ITEM_GROUP_SPEAR = "AppBundle\Entity\Application\Items\Groups\SpearsGroup",
 *     ItemGroupEnum::ITEM_GROUP_BOW = "AppBundle\Entity\Application\Items\Groups\BowsGroup",
 *     ItemGroupEnum::ITEM_GROUP_STAFF = "AppBundle\Entity\Application\Items\Groups\StaffsGroup",
 *     ItemGroupEnum::ITEM_GROUP_SHIELD = "AppBundle\Entity\Application\Items\Groups\ShieldsGroup",
 *     ItemGroupEnum::ITEM_GROUP_HELM = "AppBundle\Entity\Application\Items\Groups\HelmsGroup",
 *     ItemGroupEnum::ITEM_GROUP_ARMOR = "AppBundle\Entity\Application\Items\Groups\ArmorsGroup",
 *     ItemGroupEnum::ITEM_GROUP_PANTS = "AppBundle\Entity\Application\Items\Groups\PantsGroup",
 *     ItemGroupEnum::ITEM_GROUP_GLOVES = "AppBundle\Entity\Application\Items\Groups\GlovesGroup",
 *     ItemGroupEnum::ITEM_GROUP_BOOTS = "AppBundle\Entity\Application\Items\Groups\BootsGroup",
 *     ItemGroupEnum::ITEM_GROUP_12 = "AppBundle\Entity\Application\Items\Groups\Group12",
 *     ItemGroupEnum::ITEM_GROUP_13 = "AppBundle\Entity\Application\Items\Groups\Group13",
 *     ItemGroupEnum::ITEM_GROUP_14 = "AppBundle\Entity\Application\Items\Groups\Group14",
 *     ItemGroupEnum::ITEM_GROUP_SCROLLS = "AppBundle\Entity\Application\Items\Groups\ScrollsGroup"
 * })
 *
 */
abstract class Group
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=20, unique=true)
     */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\Items\GroupHarmonyOption", mappedBy="group", fetch="EAGER")
     */
    protected $harmonyOptions;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\Items\ItemInfo", mappedBy="group", fetch="EAGER")
     */
    protected $items;

    public function __construct()
    {
        $this->harmonyOptions = new ArrayCollection();
        $this->items = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): Group
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getHarmonyOptions(): Collection
    {
        $this->harmonyOptions === null && $this->harmonyOptions = new ArrayCollection();

        return $this->harmonyOptions;
    }

    public function getItems(): Collection
    {
        $this->items === null && $this->items = new ArrayCollection();

        return $this->items;
    }

    public function isWeapons()
    {
        return (
            $this instanceof SwordsGroup ||
            $this instanceof AxesGroup ||
            $this instanceof SceptersGroup ||
            $this instanceof SpearsGroup ||
            $this instanceof BowsGroup ||
            $this instanceof StaffsGroup
        );
    }

    public function isStaffs()
    {
        return $this instanceof StaffsGroup;
    }

    public function isArmors()
    {
        return (
            $this instanceof HelmsGroup ||
            $this instanceof ArmorsGroup ||
            $this instanceof PantsGroup ||
            $this instanceof GlovesGroup ||
            $this instanceof BootsGroup
        );
    }

    public function isShields()
    {
        return $this instanceof ShieldsGroup;
    }

    public function isScrolls()
    {
        return $this instanceof ScrollsGroup;
    }

    public function isGroup12()
    {
        return $this instanceof Group12;
    }

    public function isGroup13()
    {
        return $this instanceof Group13;
    }

    abstract public function getCountableItemIds(): array;

    abstract public function getTypeId(): int;
}