<?php

namespace AppBundle\Entity\Application\Items\Groups;

use AppBundle\Doctrine\Type\ItemGroupEnum;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class BowsGroup extends Group
{
    public function __construct()
    {
        parent::__construct();
        $this->name = 'Bows / Crossbows';
    }

    public function getCountableItemIds(): array
    {
        return [7, 15,];
    }

    public function getTypeId(): int
    {
        return ItemGroupEnum::ITEM_GROUP_BOW;
    }
}