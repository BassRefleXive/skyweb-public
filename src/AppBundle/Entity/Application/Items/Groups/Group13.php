<?php

namespace AppBundle\Entity\Application\Items\Groups;

use AppBundle\Doctrine\Type\ItemGroupEnum;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Group13 extends Group
{
    public function __construct()
    {
        parent::__construct();
        $this->name = '13';
    }

    public static function getCapeOfLordId(): int
    {
        return 30;
    }

    public static function getFenrirId(): int
    {
        return 37;
    }

    public static function rubyRingId(): int {
        return 110;
    }

    public static function sapphireRingId(): int {
        return 109;
    }

    public static function topazRingId(): int {
        return 111;
    }

    public static function amethystRingId(): int {
        return 112;
    }

    public static function rubyNecklaceId(): int {
        return 113;
    }

    public static function sapphireNecklaceId(): int {
        return 115;
    }

    public static function emeraldNecklaceId(): int {
        return 114;
    }

    public static function cashShopAccessoryIds(): array
    {
        return range(109, 115);
    }

    public function getCountableItemIds(): array
    {
        return [
            32, 33, 34, 46, 47,
        ];
    }

    public function getTypeId(): int
    {
        return ItemGroupEnum::ITEM_GROUP_13;
    }
}