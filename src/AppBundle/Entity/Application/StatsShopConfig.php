<?php

namespace AppBundle\Entity\Application;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="StatsShopConfig")
 */
class StatsShopConfig
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     */
    private $step;

    /**
     * @ORM\Column(type="integer")
     */
    private $stepPriceCredits;

    /**
     * @ORM\Column(type="integer")
     */
    private $stepPriceMoney;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enabled;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\Server", inversedBy="statsShopConfig")
     * @ORM\JoinColumn(name="server_id", referencedColumnName="id")
     */
    private $server;

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getStep()
    {
        return $this->step;
    }

    public function setStep(int $step): self
    {
        $this->step = $step;

        return $this;
    }

    public function getStepPriceCredits()
    {
        return $this->stepPriceCredits;
    }

    public function setStepPriceCredits(int $stepPriceCredits): self
    {
        $this->stepPriceCredits = $stepPriceCredits;

        return $this;
    }

    public function getStepPriceMoney()
    {
        return $this->stepPriceMoney;
    }

    public function setStepPriceMoney(int $stepPriceMoney): self
    {
        $this->stepPriceMoney = $stepPriceMoney;

        return $this;
    }

    public function getServer()
    {
        return $this->server;
    }

    public function setServer(Server $server): self
    {
        $this->server = $server;

        return $this;
    }

    public function isEnabled(): bool
    {
        return true === $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }
}