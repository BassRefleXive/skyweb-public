<?php

namespace AppBundle\Entity\Application\User;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Regular extends User
{
    protected $roles = [
        self::ROLE_REGULAR,
    ];
}