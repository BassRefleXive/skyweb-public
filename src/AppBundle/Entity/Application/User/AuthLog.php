<?php


namespace AppBundle\Entity\Application\User;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Application\User\Builder\AuthLogBuilder;
use AppBundle\Entity\Traits\Timestampable;


/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Application\AuthLogRepository")
 * @ORM\Table(
 *     name="UserAuthLog",
 *     indexes={
 *          @ORM\Index(name="login_idx", columns={"login"}),
 *          @ORM\Index(name="last_login_ip_INDEX", columns={"login", "ip_address", "success", "created_at"})
 *      }
 * )
 */
class AuthLog
{
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="user_agent", type="string", length=256, nullable=false)
     */
    private $userAgent;

    /**
     * @ORM\Column(name="ip_address", type="integer", nullable=false)
     */
    private $ip;

    /**
     * @ORM\Column(name="login", type="string", length=256, nullable=false)
     */
    private $login;

    /**
     * @ORM\Column(name="password", type="string", length=256, nullable=false)
     */
    private $password;

    /**
     * @ORM\Column(name="success", type="boolean", nullable=false)
     */
    private $success;

    public function __construct(AuthLogBuilder $builder)
    {
        $this->userAgent = $builder->userAgent();
        $this->ip = $builder->ip();
        $this->login = $builder->login();
        $this->password = $builder->password();
        $this->success = $builder->success();
    }

    public function id(): int
    {
        return $this->id;
    }

    public function userAgent(): string
    {
        return $this->userAgent;
    }

    public function ip(): int
    {
        return $this->ip;
    }

    public function stringIp(): string
    {
        return long2ip($this->ip);
    }

    public function login(): string
    {
        return $this->login;
    }

    public function password(): string
    {
        return $this->password;
    }

    public function success(): bool
    {
        return $this->success;
    }
}