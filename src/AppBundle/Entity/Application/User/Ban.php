<?php

declare(strict_types = 1);

namespace AppBundle\Entity\Application\User;


use AppBundle\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Application\User\BanRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="Ban")
 */
class Ban
{
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="reason", length=255, unique=false, nullable=false)
     */
    private $reason;

    /**
     * @ORM\Column(type="string", name="link", length=255, unique=false, nullable=true)
     */
    private $link;

    /**
     * @ORM\Column(name="expire_at", type="datetime")
     */
    private $expireAt;

    /**
     * @ORM\Column(name="un_ban_processed", type="boolean", nullable=false)
     */
    private $unBanProcessed;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\User\User", inversedBy="bans")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\User\Moderator", inversedBy="issuedBans")
     * @ORM\JoinColumn(name="banned_by", referencedColumnName="id")
     */
    private $bannedBy;

    public function __construct()
    {
        $this->unBanProcessed = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getReason(): ?string
    {
        return $this->reason;
    }

    public function setReason(string $reason): self
    {
        $this->reason = $reason;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getExpireAt(): ?\DateTimeInterface
    {
        return $this->expireAt;
    }

    public function setExpireAt(\DateTimeInterface $expireAt): self
    {
        $this->expireAt = $expireAt;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getBannedBy(): ?Moderator
    {
        return $this->bannedBy;
    }

    public function setBannedBy(Moderator $bannedBy): self
    {
        $this->bannedBy = $bannedBy;

        return $this;
    }

    public function isActive(): bool
    {
        return $this->expireAt >= new \DateTime();
    }

    public function unBanProcessed(): bool
    {
        return $this->unBanProcessed;
    }

    public function unBan(): void
    {
        $this->unBanProcessed = true;
    }
}