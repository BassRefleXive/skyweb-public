<?php

namespace AppBundle\Entity\Application\User;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Admin extends Moderator
{
    protected $roles = [
        self::ROLE_ADMIN,
    ];
}