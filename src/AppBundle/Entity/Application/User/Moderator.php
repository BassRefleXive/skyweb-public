<?php

namespace AppBundle\Entity\Application\User;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Moderator extends User
{
    protected $roles = [
        self::ROLE_MODERATOR,
    ];

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\User\Ban", mappedBy="bannedBy", cascade={"persist"})
     */
    protected $issuedBans;

    public function addIssuedBan(Ban $ban): self
    {
        $this->getIssuedBans()->add($ban);

        return $this;
    }

    public function getIssuedBans(): Collection
    {
        $this->issuedBans === null && $this->issuedBans = new ArrayCollection();

        return $this->issuedBans;
    }
}