<?php

namespace AppBundle\Entity\Application\User;

use AppBundle\Entity\Application\News;
use AppBundle\Entity\Application\Server;
use AppBundle\Entity\Application\UserServerAccount;
use AppBundle\Enum\Application\EmailState;
use AppBundle\Location\Model\Country;
use AppBundle\Model\Application\ResetPasswordModel;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Traits\Timestampable;
use Collection\Sequence;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use AppBundle\Doctrine\Type\UserTypeEnum;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\User\UserInterface;
use AppBundle\Forum\Model\User as ForumUser;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Application\UserRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="User")
 *
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type_id", type="enum_user_type")
 * @ORM\DiscriminatorMap({
 *     UserTypeEnum::USER_TYPE_ADMIN = "AppBundle\Entity\Application\User\Admin",
 *     UserTypeEnum::USER_TYPE_MOD = "AppBundle\Entity\Application\User\Moderator",
 *     UserTypeEnum::USER_TYPE_REGULAR = "AppBundle\Entity\Application\User\Regular"
 * })
 *
 */
abstract class User implements UserInterface
{
    use Timestampable;

    const ROLE_ADMIN = 'ROLE_ADMINISTRATOR';
    const ROLE_MODERATOR = 'ROLE_MODERATOR';
    const ROLE_REGULAR = 'ROLE_REGULAR';

    protected $roles = [];

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", name="username", length=10, unique=true)
     */
    protected $username;

    /**
     * @ORM\Column(type="string", name="display_name", length=10, unique=true)
     */
    protected $displayName;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    protected $email;

    /**
     * @ORM\Column(type="email_state_type")
     */
    private $emailState;

    /**
     * @ORM\Column(type="string", length=10)
     */
    protected $password;

    /**
     * @ORM\Column(name="logged_at", type="datetime")
     */
    protected $loggedAt;

    /**
     * @ORM\Column(type="string", name="locale", length=6, unique=false, nullable=false)
     */
    protected $locale;

    /**
     * @ORM\Column(type="string", name="reset_password_token", length=36, unique=true, nullable=true)
     */
    protected $resetPasswordToken;

    /**
     * @ORM\Column(name="reset_password_date", type="datetime", nullable=true)
     */
    protected $resetPasswordDate;

    /**
     * @ORM\Column(name="ip_address", type="integer", nullable=false)
     */
    private $ipAddress;

    /**
     * @ORM\Column(name="forum_id", type="integer", nullable=true)
     */
    private $forumId;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\UserServerAccount", mappedBy="user", cascade={"persist"})
     */
    protected $userServerAccounts;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\News", mappedBy="author", cascade={"persist"})
     */
    protected $news;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\User\User", mappedBy="referredBy", cascade={"persist"})
     */
    protected $referrals;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\User\Ban", mappedBy="user", cascade={"persist"})
     */
    private $bans;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\User\User", inversedBy="referrals")
     * @ORM\JoinColumn(name="referred_by", referencedColumnName="id")
     */
    protected $referredBy;

    /**
     * @ORM\OneToOne(targetEntity="\AppBundle\Location\Model\Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="code_iso3")
     */
    private $country;

    private $plainPassword;

    public function __construct()
    {
        $this->loggedAt = new \DateTime();
        $this->userServerAccounts = new ArrayCollection();
        $this->referrals = new ArrayCollection();
        $this->emailState = EmailState::byValue(EmailState::ACTIVE);
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): User
    {
        $this->id = $id;

        return $this;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername(string $username): User
    {
        $this->username = $username;

        return $this;
    }

    public function getDisplayName()
    {
        return $this->displayName;
    }

    public function setDisplayName(string $displayName): User
    {
        $this->displayName = $displayName;

        return $this;
    }

    public function email()
    {
        return $this->email;
    }

    public function setEmail(string $email): User
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword(string $password): User
    {
        $this->password = $password;

        return $this;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword): User
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    public function getLoggedAt()
    {
        return $this->loggedAt;
    }

    public function setLoggedAt(\DateTime $loggedAt): User
    {
        $this->loggedAt = $loggedAt;

        return $this;
    }

    public function getLocale()
    {
        return $this->locale;
    }

    public function setLocale(string $locale): User
    {
        $this->locale = $locale;

        return $this;
    }

    public function addUserServerAccount(UserServerAccount $userServerAccount): User
    {
        $this->getUserServerAccounts()->add($userServerAccount);

        return $this;
    }

    /**
     * @return UserServerAccount[]
     */
    public function getUserServerAccounts(): Collection
    {
        $this->userServerAccounts === null && $this->userServerAccounts = new ArrayCollection();

        return $this->userServerAccounts;
    }

    public function addNews(News $news): User
    {
        $this->getNews()->add($news);

        return $this;
    }

    public function getNews(): Collection
    {
        $this->news === null && $this->news = new ArrayCollection();

        return $this->news;
    }

    public function getReferrals(): Collection
    {
        $this->referrals === null && $this->referrals = new ArrayCollection();

        return $this->referrals;
    }

    public function addReferral(User $user): User
    {
        $user->setReferredBy($this);

        $this->getReferrals()->add($user);

        return $this;
    }

    public function removeReferral(User $user): User
    {
        $items = $this->getReferrals();

        if (($index = $items->indexOf($user)) !== false) {
            $items->remove($index);
        }

        return $this;
    }

    /**
     * @return User
     */
    public function getReferredBy()
    {
        return $this->referredBy;
    }

    public function setReferredBy(User $referredBy): User
    {
        $this->referredBy = $referredBy;

        return $this;
    }


    public function getRoles(): array
    {
        return $this->roles;
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    public function resetPasswordToken()
    {
        return $this->resetPasswordToken;
    }

    /**
     * @return null|\DateTime
     */
    public function resetPasswordDate()
    {
        return $this->resetPasswordDate;
    }

    public function resetPassword()
    {
        $this->resetPasswordToken = (string) Uuid::uuid4();
        $this->resetPasswordDate = new \DateTime();
    }

    public function finalizePasswordReset(ResetPasswordModel $model)
    {
        $this->password = $model->getPassword();
        $this->resetPasswordToken = null;
        $this->resetPasswordDate = null;
    }

    public function ipAddress(): int
    {
        return $this->ipAddress;
    }

    public function specifyIpAddress(int $ipAddress): self
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    /**
     * @return UserServerAccount
     */
    public function getLastUserServerAccount()
    {
        return (new Sequence($this->getUserServerAccounts()->toArray()))
            ->sortWith(function (UserServerAccount $a, UserServerAccount $b) {
                return $a->getSelectedAt() <=> $b->getSelectedAt();
            })
            ->last();
    }

    /**
     * @param Server $server
     * @return UserServerAccount|null
     */
    public function getUserServerAccountByServer(Server $server)
    {
        return (new Sequence($this->getUserServerAccounts()->toArray()))
            ->find(function (UserServerAccount $userServerAccount) use ($server) {
                return $userServerAccount->getServer()->getId() === $server->getId();
            })
            ->getOrElse(null);
    }

    public function addBan(Ban $ban): self
    {
        $this->getBans()->add($ban);

        return $this;
    }

    public function getBans(): Collection
    {
        $this->bans === null && $this->bans = new ArrayCollection();

        return $this->bans;
    }

    public function isBanned(): bool
    {
        /** @var Ban $ban $ban */
        foreach ($this->getBans() as $ban) {
            if ($ban->isActive()) {
                return true;
            }
        }

        return false;
    }

    public function country(): ?Country
    {
        return $this->country;
    }

    public function fromCountry(Country $country): void
    {
        $this->country = $country;
    }

    public function forumId(): ?int
    {
        return $this->forumId;
    }

    public function registeredOnForum(ForumUser $user): void
    {
        $this->forumId = $user->id();
    }

    public function isRegisteredInForum(): bool
    {
        return null !== $this->forumId;
    }

    public function emailState(): EmailState
    {
        return $this->emailState;
    }

    final public function __sleep()
    {
        return [
            'roles',
            'id',
            'username',
            'displayName',
            'email',
            'password',
            'loggedAt',
            'locale',
            'resetPasswordToken',
            'resetPasswordDate',
        ];
    }
}