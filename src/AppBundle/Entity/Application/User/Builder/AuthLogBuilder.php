<?php

namespace AppBundle\Entity\Application\User\Builder;

use AppBundle\Entity\Application\User\AuthLog;

class AuthLogBuilder
{
    private $userAgent;

    private $ip;

    private $login;

    private $password;

    private $success;

    public function userAgent(): string
    {
        return $this->userAgent;
    }

    public function withUserAgent(string $userAgent): self
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    public function ip(): int
    {
        return $this->ip;
    }

    public function withIp(string $ip): self
    {
        $this->ip = ip2long($ip);

        return $this;
    }

    public function login(): string
    {
        return $this->login;
    }

    public function withLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }

    public function password(): string
    {
        return $this->password;
    }

    public function withPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function success(): bool
    {
        return $this->success;
    }

    public function markSuccessful(): self
    {
        $this->success = true;

        return $this;
    }

    public function markFailed(): self
    {
        $this->success = false;

        return $this;
    }

    public function build(): AuthLog
    {
        $this->validate();

        return new AuthLog($this);
    }

    private function validate(): void
    {
        foreach ($this as $property => $value) {
            if (null === $value) {
                throw new \InvalidArgumentException(
                    sprintf(
                        'Missing property "%s" value when building instance of "%s".',
                        $property,
                        AuthLog::class
                    )
                );
            }
        }
    }
}