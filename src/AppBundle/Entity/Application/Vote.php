<?php


namespace AppBundle\Entity\Application;


use AppBundle\Entity\Traits\Timestampable;
use AppBundle\Enum\Application\VoteProvider;
use AppBundle\Enum\Application\VoteType;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Application\VoteRepository")
 * @ORM\Table(name="Vote")
 */
class Vote
{
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="datetime")
     */
    private $dateTime;

    /**
     * @ORM\Column(type="string")
     */
    private $ip;

    /**
     * @ORM\Column(type="bigint")
     */
    private $providerId;

    /**
     * @ORM\Column(type="vote_provider")
     */
    private $provider;

    /**
     * @ORM\Column(type="vote_type")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\UserServerAccount", inversedBy="votes")
     * @ORM\JoinColumn(name="user_server_account_id", referencedColumnName="id")
     */
    protected $voter;

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getDateTime(): ?\DateTime
    {
        return $this->dateTime;
    }

    public function setDateTime(\DateTimeInterface $dateTime): self
    {
        $this->dateTime = $dateTime;

        return $this;
    }

    public function getIp()
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function providerId()
    {
        return $this->providerId;
    }

    public function setProviderId(int $providerId): self
    {
        $this->providerId = $providerId;

        return $this;
    }

    public function provider(): VoteProvider
    {
        return $this->provider;
    }

    public function setProvider(VoteProvider $provider): self
    {
        $this->provider = $provider;

        return $this;
    }

    public function getType(): ?VoteType
    {
        return $this->type;
    }

    public function setType(VoteType $type): self
    {
        $this->type = $type;

        return $this;
    }

    /** @return UserServerAccount */
    public function getVoter()
    {
        return $this->voter;
    }

    public function setVoter(UserServerAccount $voter): self
    {
        $this->voter = $voter;

        return $this;
    }

}