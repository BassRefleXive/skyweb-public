<?php

namespace AppBundle\Entity\Application;

use AppBundle\Doctrine\Type\ItemToBuyTypeEnum;
use AppBundle\Entity\Application\Payments\Order;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="ItemToBuy")
 */
class ItemToBuy
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $item;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $stats;

    /**
     * @ORM\Column(type="enum_item_to_buy_type")
     */
    protected $type;

    /**
     * @ORM\Column(type="smallint")
     */
    protected $priceCredits;

    /**
     * @ORM\Column(type="float")
     */
    protected $priceMoney;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\Cart", inversedBy="items")
     * @ORM\JoinColumn(name="cart_id", referencedColumnName="id")
     */
    protected $cart;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\Payments\Order", inversedBy="items")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    protected $order;

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): ItemToBuy
    {
        $this->id = $id;

        return $this;
    }

    public function getItem()
    {
        return $this->item;
    }

    public function setItem(string $item): ItemToBuy
    {
        $this->item = $item;

        return $this;
    }

    public function getPriceCredits()
    {
        return $this->priceCredits;
    }

    public function setPriceCredits(int $priceCredits): ItemToBuy
    {
        $this->priceCredits = $priceCredits;

        return $this;
    }

    public function getPriceMoney()
    {
        return $this->priceMoney;
    }

    public function withAppliedDiscount(float $discount): float
    {
        return ceil($this->priceMoney * (100 - $discount)) / 100;
    }

    public function setPriceMoney(float $priceMoney): ItemToBuy
    {
        $this->priceMoney = $priceMoney;

        return $this;
    }

    public function getCart(): Cart
    {
        return $this->cart;
    }

    public function setCart(Cart $cart = null): ItemToBuy
    {
        $cart !== null && $this->order = null;

        $this->cart = $cart;

        return $this;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    public function setOrder(Order $order = null): ItemToBuy
    {
        $order !== null && $this->cart = null;

        $this->order = $order;

        return $this;
    }

    public function getStats()
    {
        return $this->stats;
    }

    public function setStats(int $stats): ItemToBuy
    {
        $this->stats = $stats;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType(string $type): ItemToBuy
    {
        if (!array_key_exists($type, ItemToBuyTypeEnum::getTypes())) {
            throw new \InvalidArgumentException('Invalid ItemToBuyTypeEnum type.');
        }

        $this->type = $type;

        return $this;
    }

}