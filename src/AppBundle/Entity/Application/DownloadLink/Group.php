<?php

namespace AppBundle\Entity\Application\DownloadLink;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="DownloadLinkGroup")
 */
class Group
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $size;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\DownloadLink\Link", mappedBy="group", cascade={"persist", "remove"})
     */
    private $links;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setSize(int $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getLinks(): Collection
    {
        null === $this->links && $this->links = new ArrayCollection();

        return $this->links;
    }

    public function setLinks(Collection $links): self
    {
        /** @var Link $link */
        foreach ($links as $link) {
            $link->setGroup($this);
        }
        $this->links = $links;

        return $this;
    }

    public function addLink(Link $link): self
    {
        $link->setGroup($this);

        $this->getLinks()->add($link);

        return $this;
    }

    public function removeLink(Link $link): self
    {
        if (false !== $index = $this->getLinks()->indexOf($link)) {
            $this->getLinks()->remove($index);
        }

        return $this;
    }
}