<?php

namespace AppBundle\Entity\Application;

use AppBundle\Doctrine\Type\CartDiscountCouponTypeEnum;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Traits\Timestampable;
use AppBundle\Repository\Application\CartDiscountCouponRepository;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Application\CartDiscountCouponRepository")
 * @ORM\Table(
 *      name="CartDiscountCoupon",
 *      uniqueConstraints={
 *          @ORM\UniqueConstraint(name="CartDiscountCoupon_coupon_UNIQUE", columns={"coupon"})
 *      }
 * )
 */
class CartDiscountCoupon
{
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=5)
     */
    protected $coupon;

    /**
     * @ORM\Column(type="integer")
     */
    protected $discount;

    /**
     * @ORM\Column(type="enum_cart_discount_coupon_type")
     */
    protected $type;

    /**
     * @ORM\Column(type="integer")
     */
    protected $status;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $expireAt;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\CartPurchaseDiscount", mappedBy="discountCoupon")
     */
    protected $purchaseDiscount;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\Server", inversedBy="cartDiscountCoupons")
     * @ORM\JoinColumn(name="server_id", referencedColumnName="id")
     */
    protected $server;

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): CartDiscountCoupon
    {
        $this->id = $id;

        return $this;
    }

    public function getCoupon()
    {
        return $this->coupon;
    }

    public function setCoupon(string $coupon): CartDiscountCoupon
    {
        $this->coupon = $coupon;

        return $this;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function setDiscount(int $discount): CartDiscountCoupon
    {
        $this->discount = $discount;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType(string $type): CartDiscountCoupon
    {
        if (!array_key_exists($type, CartDiscountCouponTypeEnum::getTypes())) {
            throw new \InvalidArgumentException('Invalid CartDiscountCoupon type.');
        }

        $this->type = $type;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus(int $status): CartDiscountCoupon
    {
        if (!in_array($status, [CartDiscountCouponRepository::STATUS_DISABLED, CartDiscountCouponRepository::STATUS_ENABLED], true)) {
            throw new \InvalidArgumentException('Invalid CartDiscountCoupon status');
        }

        $this->status = $status;

        return $this;
    }

    public function getExpireAt()
    {
        return $this->expireAt;
    }

    public function setExpireAt(\DateTime $expireAt): CartDiscountCoupon
    {
        $this->expireAt = $expireAt;

        return $this;
    }

    public function getPurchaseDiscount()
    {
        return $this->purchaseDiscount;
    }

    public function setPurchaseDiscount(CartPurchaseDiscount $purchaseDiscount): CartDiscountCoupon
    {
        $this->purchaseDiscount = $purchaseDiscount;

        return $this;
    }

    /**
     * @return Server
     */
    public function getServer()
    {
        return $this->server;
    }

    public function setServer(Server $server): CartDiscountCoupon
    {
        $this->server = $server;

        return $this;
    }

}