<?php

namespace AppBundle\Entity\Application;

use AppBundle\Doctrine\Type\CartPurchaseDiscountTypeEnum;
use AppBundle\Entity\Application\PurchaseLog\CartPurchaseLog;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="CartPurchaseDiscount")
 */
class CartPurchaseDiscount
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="enum_cart_purchase_discount_type")
     */
    protected $type;

    /**
     * @ORM\Column(type="integer")
     */
    protected $discountPercent;

    /**
     * @ORM\Column(type="float")
     */
    protected $discountAmount;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\PurchaseLog\CartPurchaseLog", inversedBy="discounts")
     * @ORM\JoinColumn(name="cart_purchase_item_log_id", referencedColumnName="id")
     */
    protected $purchaseLog;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\CartDiscountCoupon", inversedBy="purchaseDiscount")
     * @ORM\JoinColumn(name="cart_discount_coupon_id", referencedColumnName="id")
     */
    protected $discountCoupon;

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): CartPurchaseDiscount
    {
        $this->id = $id;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType(string $type): CartPurchaseDiscount
    {
        if (!array_key_exists($type, CartPurchaseDiscountTypeEnum::getTypes())) {
            throw new \InvalidArgumentException('Invalid WebShop purchase discount type.');
        }

        $this->type = $type;

        return $this;
    }

    public function getDiscountPercent()
    {
        return $this->discountPercent;
    }

    public function setDiscountPercent(int $discountPercent): CartPurchaseDiscount
    {
        $this->discountPercent = $discountPercent;

        return $this;
    }

    public function getDiscountAmount()
    {
        return $this->discountAmount;
    }

    public function setDiscountAmount(float $discountAmount): CartPurchaseDiscount
    {
        $this->discountAmount = $discountAmount;

        return $this;
    }

    public function getPurchaseLog()
    {
        return $this->purchaseLog;
    }

    public function setPurchaseLog(CartPurchaseLog $purchaseLog): CartPurchaseDiscount
    {
        $this->purchaseLog = $purchaseLog;

        return $this;
    }

    public function getDiscountCoupon()
    {
        return $this->discountCoupon;
    }

    public function setDiscountCoupon(CartDiscountCoupon $discountCoupon): CartPurchaseDiscount
    {
        $this->discountCoupon = $discountCoupon;

        return $this;
    }

}