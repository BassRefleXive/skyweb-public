<?php

namespace AppBundle\Entity\Application;

use AppBundle\Doctrine\Type\MarketPriceTypeEnum;
use AppBundle\Entity\Application\Items\ItemInfo;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Traits\Timestampable;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Application\MarketItemRepository")
 * @ORM\Table(
 *     name="MarketItem",
 *     indexes={
 *          @ORM\Index(name="MarketItem_index_item_specs", columns={"level","option","skill","luck","excellent","ancient","harmony","pvp","price","price_type"})
 *      }
 * )
 */
class MarketItem
{
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $hex;

    /**
     * @ORM\Column(type="smallint")
     */
    private $level;

    /**
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @ORM\Column(type="smallint")
     */
    private $option;

    /**
     * @ORM\Column(type="boolean")
     */
    private $skill;

    /**
     * @ORM\Column(type="boolean")
     */
    private $luck;

    /**
     * @ORM\Column(type="boolean")
     */
    private $excellent;

    /**
     * @ORM\Column(type="boolean")
     */
    private $ancient;

    /**
     * @ORM\Column(type="boolean")
     */
    private $harmony;

    /**
     * @ORM\Column(type="boolean")
     */
    private $pvp;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="enum_market_price_type")
     */
    private $priceType;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\UserServerAccount", inversedBy="boughtMarketItems")
     * @ORM\JoinColumn(name="bought_by", referencedColumnName="id")
     */
    private $boughtBy;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\Items\ItemInfo", inversedBy="marketItems")
     * @ORM\JoinColumn(name="item_info_id", referencedColumnName="id")
     */
    private $itemInfo;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\UserServerAccount", inversedBy="marketItems")
     * @ORM\JoinColumn(name="user_server_account_id", referencedColumnName="id")
     */
    private $userServerAccount;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\Server", inversedBy="marketItems")
     * @ORM\JoinColumn(name="server_id", referencedColumnName="id")
     */
    protected $server;

    public function __construct()
    {
        $this->level = 0;
        $this->option = 0;
        $this->skill = false;
        $this->luck = false;
        $this->excellent = false;
        $this->ancient = false;
        $this->harmony = false;
        $this->pvp = false;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getHex()
    {
        return $this->hex;
    }

    public function setHex(string $hex): self
    {
        $this->hex = $hex;

        return $this;
    }

    public function getLevel()
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getOption()
    {
        return $this->option;
    }

    public function setOption(int $option): self
    {
        $this->option = $option;

        return $this;
    }

    public function getSkill()
    {
        return $this->skill;
    }

    public function setSkill(bool $skill): self
    {
        $this->skill = $skill;

        return $this;
    }

    public function getLuck()
    {
        return $this->luck;
    }

    public function setLuck(bool $luck): self
    {
        $this->luck = $luck;

        return $this;
    }

    public function getExcellent()
    {
        return $this->excellent;
    }

    public function setExcellent(bool $excellent): self
    {
        $this->excellent = $excellent;

        return $this;
    }

    public function getAncient()
    {
        return $this->ancient;
    }

    public function setAncient(bool $ancient): self
    {
        $this->ancient = $ancient;

        return $this;
    }

    public function getHarmony()
    {
        return $this->harmony;
    }

    public function setHarmony(bool $harmony): self
    {
        $this->harmony = $harmony;

        return $this;
    }

    public function getPvp()
    {
        return $this->pvp;
    }

    public function setPvp(bool $pvp): self
    {
        $this->pvp = $pvp;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPriceType()
    {
        return $this->priceType;
    }

    public function setPriceType(string $priceType): self
    {
        if (!array_key_exists($priceType, MarketPriceTypeEnum::getTypes())) {
            throw new \InvalidArgumentException('Invalid Market price type.');
        }

        $this->priceType = $priceType;

        return $this;
    }

    public function getItemInfo()
    {
        return $this->itemInfo;
    }

    public function setItemInfo(ItemInfo $itemInfo): self
    {
        $this->itemInfo = $itemInfo;

        return $this;
    }

    /**
     * @return UserServerAccount
     */
    public function getUserServerAccount()
    {
        return $this->userServerAccount;
    }

    public function setUserServerAccount(UserServerAccount $userServerAccount): self
    {
        $this->userServerAccount = $userServerAccount;

        return $this;
    }

    /**
     * @return Server
     */
    public function getServer()
    {
        return $this->server;
    }

    public function setServer(Server $server): self
    {
        $this->server = $server;

        return $this;
    }

    public function setBoughtBy(UserServerAccount $userServerAccount): self
    {
        $this->boughtBy = $userServerAccount;

        return $this;
    }

    public function boughtBy(): ?UserServerAccount
    {
        return $this->boughtBy;
    }
}