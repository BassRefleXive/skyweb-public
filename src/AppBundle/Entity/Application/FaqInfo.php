<?php


namespace AppBundle\Entity\Application;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="FaqInfo")
 */
class FaqInfo
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="json_array", name="`values`")
     */
    protected $values;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\Server", inversedBy="faqInfo")
     * @ORM\JoinColumn(name="server_id", referencedColumnName="id")
     */
    protected $server;

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): FaqInfo
    {
        $this->id = $id;

        return $this;
    }

    public function getValues(): array
    {
        return $this->values;
    }

    public function setValues(array $values)
    {
        $this->values = $values;
    }

    public function getServer()
    {
        return $this->server;
    }

    public function setServer(Server $server): FaqInfo
    {
        $this->server = $server;

        return $this;
    }

    public function getEventTimings(): array
    {
        $result = [];

        if (isset($this->getValues()['events'])) {
            foreach ($this->getValues()['events'] as $event) {
                isset($event['name'], $event['timing']) && $result[] = array_merge(
                    [
                        'name'   => $event['name'],
                        'timing' => $event['timing'],
                    ],
                    isset($event['url'])
                        ? ['url' => $event['url']]
                        : []
                );
            }
        }

        return $result;
    }
}