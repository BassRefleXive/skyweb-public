<?php

declare(strict_types=1);

namespace AppBundle\Entity\Application\MagicList;

use AppBundle\Entity\Application\CharacterClass;
use AppBundle\Entity\Application\Items\ItemInfo;
use AppBundle\Enum\Application\MagicList\SkillAttribute;
use AppBundle\Enum\Application\MagicList\SkillType;
use AppBundle\Enum\Application\MagicList\SkillUseType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Application\MagicList\SkillRepository")
 * @ORM\Table(name="Skill")
 */
class Skill
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $damage;

    /**
     * @ORM\Column(type="integer")
     */
    private $manaUsage;

    /**
     * @ORM\Column(type="integer")
     */
    private $staminaUsage;

    /**
     * @ORM\Column(type="integer")
     */
    private $distance;

    /**
     * @ORM\Column(type="integer")
     */
    private $delay;

    /**
     * @ORM\Column(type="integer")
     */
    private $requiredLevel;

    /**
     * @ORM\Column(type="integer")
     */
    private $requiredStrength;

    /**
     * @ORM\Column(type="integer")
     */
    private $requiredAgility;

    /**
     * @ORM\Column(type="integer")
     */
    private $requiredEnergy;

    /**
     * @ORM\Column(type="integer")
     */
    private $requiredCommand;

    /**
     * @ORM\Column(type="integer")
     */
    private $requiredMLPoints;

    /**
     * @ORM\Column(type="integer")
     */
    private $iconNumber;

    /**
     * @ORM\Column(type="magic_list_skill_attribute")
     */
    private $attribute;

    /**
     * @ORM\Column(type="magic_list_skill_type")
     */
    private $type;

    /**
     * @ORM\Column(type="magic_list_skill_use_type")
     */
    private $useType;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDeletable;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\Items\ItemInfo")
     * @ORM\JoinColumn(name="item_info_id", referencedColumnName="id")
     */
    private $learnItem;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Application\CharacterClass", cascade={"remove", "persist"})
     * @ORM\JoinTable(name="skill_character_classes",
     *      joinColumns={@ORM\JoinColumn(name="skill_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="character_class_id", referencedColumnName="id")}
     *      )
     */
    private $characterClasses;

    public function __construct(SkillBuilder $builder)
    {
        $this->id = $builder->id();
        $this->name = $builder->name();
        $this->damage = $builder->damage();
        $this->manaUsage = $builder->manaUsage();
        $this->staminaUsage = $builder->staminaUsage();
        $this->distance = $builder->distance();
        $this->delay = $builder->delay();
        $this->requiredLevel = $builder->requiredLevel();
        $this->requiredStrength = $builder->requiredStrength();
        $this->requiredAgility = $builder->requiredAgility();
        $this->requiredEnergy = $builder->requiredEnergy();
        $this->requiredCommand = $builder->requiredCommand();
        $this->requiredMLPoints = $builder->requiredMLPoints();
        $this->iconNumber = $builder->iconNumber();
        $this->attribute = $builder->attribute();
        $this->type = $builder->type();
        $this->useType = $builder->useType();

        foreach ($builder->characterClasses() as $characterClass) {
            $this->addCharacterClass($characterClass);
        }
    }

    public function id(): int
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function damage(): int
    {
        return $this->damage;
    }

    public function manaUsage(): int
    {
        return $this->manaUsage;
    }

    public function staminaUsage(): int
    {
        return $this->staminaUsage;
    }

    public function distance(): int
    {
        return $this->distance;
    }

    public function delay(): int
    {
        return $this->delay;
    }

    public function requiredLevel(): int
    {
        return $this->requiredLevel;
    }

    public function requiredStrength(): int
    {
        return $this->requiredStrength;
    }

    public function requiredAgility(): int
    {
        return $this->requiredAgility;
    }

    public function requiredEnergy(): int
    {
        return $this->requiredEnergy;
    }

    public function requiredCommand(): int
    {
        return $this->requiredCommand;
    }

    public function requiredMLPoints(): int
    {
        return $this->requiredMLPoints;
    }

    public function iconNumber(): int
    {
        return $this->iconNumber;
    }

    public function attribute(): SkillAttribute
    {
        return $this->attribute;
    }

    public function type(): SkillType
    {
        return $this->type;
    }

    public function useType(): SkillUseType
    {
        return $this->useType;
    }

    public function characterClasses(): Collection
    {
        null === $this->characterClasses && $this->characterClasses = new ArrayCollection();

        return $this->characterClasses;
    }

    public function addCharacterClass(CharacterClass $characterClass): self
    {
        $this->characterClasses()->add($characterClass);

        return $this;
    }

    public function imageId(): int
    {
        return $this->id() > 300 && null !== $this->iconNumber()
            ? $this->iconNumber() + 300
            : $this->id();
    }

    public function isDeletable(): bool
    {
        return $this->isDeletable;
    }

    public function learnItem(): ?ItemInfo
    {
        return $this->learnItem;
    }
}