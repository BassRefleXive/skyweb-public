<?php

declare(strict_types=1);

namespace AppBundle\Entity\Application\MagicList;


use AppBundle\Enum\Application\MagicList\SkillAttribute;
use AppBundle\Enum\Application\MagicList\SkillType;
use AppBundle\Enum\Application\MagicList\SkillUseType;

class SkillBuilder
{
    private $id;
    private $name;
    private $damage;
    private $manaUsage;
    private $staminaUsage;
    private $distance;
    private $delay;
    private $requiredLevel;
    private $requiredStrength;
    private $requiredAgility;
    private $requiredEnergy;
    private $requiredCommand;
    private $requiredMLPoints;
    private $iconNumber;
    private $attribute;
    private $type;
    private $useType;
    private $characterClasses;


    public function id(): int
    {
        return $this->id;
    }

    public function withId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function withName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function damage(): int
    {
        return $this->damage;
    }

    public function withDamage(int $damage): self
    {
        $this->damage = $damage;

        return $this;
    }

    public function manaUsage(): int
    {
        return $this->manaUsage;
    }

    public function withManaUsage(int $manaUsage): self
    {
        $this->manaUsage = $manaUsage;

        return $this;
    }

    public function staminaUsage(): int
    {
        return $this->staminaUsage;
    }

    public function withStaminaUsage(int $staminaUsage): self
    {
        $this->staminaUsage = $staminaUsage;

        return $this;
    }

    public function distance(): int
    {
        return $this->distance;
    }

    public function withDistance(int $distance): self
    {
        $this->distance = $distance;

        return $this;
    }

    public function delay(): int
    {
        return $this->delay;
    }

    public function withDelay(int $delay): self
    {
        $this->delay = $delay;

        return $this;
    }

    public function requiredLevel(): int
    {
        return $this->requiredLevel;
    }

    public function withRequiredLevel(int $requiredLevel): self
    {
        $this->requiredLevel = $requiredLevel;

        return $this;
    }

    public function requiredStrength(): int
    {
        return $this->requiredStrength;
    }

    public function withRequiredStrength(int $requiredStrength): self
    {
        $this->requiredStrength = $requiredStrength;

        return $this;
    }

    public function requiredAgility(): int
    {
        return $this->requiredAgility;
    }

    public function withRequiredAgility(int $requiredAgility): self
    {
        $this->requiredAgility = $requiredAgility;

        return $this;
    }

    public function requiredEnergy(): int
    {
        return $this->requiredEnergy;
    }

    public function withRequiredEnergy(int $requiredEnergy): self
    {
        $this->requiredEnergy = $requiredEnergy;

        return $this;
    }

    public function requiredCommand(): int
    {
        return $this->requiredCommand;
    }

    public function withRequiredCommand(int $requiredCommand): self
    {
        $this->requiredCommand = $requiredCommand;

        return $this;
    }

    public function requiredMLPoints(): int
    {
        return $this->requiredMLPoints;
    }

    public function withRequiredMLPoints(int $requiredMLPoints): self
    {
        $this->requiredMLPoints = $requiredMLPoints;

        return $this;
    }

    public function iconNumber(): int
    {
        return $this->iconNumber;
    }

    public function withIconNumber(int $iconNumber): self
    {
        $this->iconNumber = $iconNumber;

        return $this;
    }

    public function attribute(): SkillAttribute
    {
        return $this->attribute;
    }

    public function withAttribute(SkillAttribute $attribute): self
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function type(): SkillType
    {
        return $this->type;
    }

    public function withType(SkillType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function useType(): SkillUseType
    {
        return $this->useType;
    }

    public function withUseType(SkillUseType $useType): self
    {
        $this->useType = $useType;

        return $this;
    }

    public function characterClasses(): array
    {
        return $this->characterClasses ?? [];
    }

    public function withCharacterClasses(array $characterClasses): self
    {
        $this->characterClasses = $characterClasses;

        return $this;
    }

    public function build(): Skill
    {
        return new Skill($this);
    }
}