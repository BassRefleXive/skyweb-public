<?php

declare(strict_types = 1);

namespace AppBundle\Entity\Application\Payments;

use AppBundle\Doctrine\Type\OrderStateEnum;
use AppBundle\Entity\Application\CartDiscountCoupon;
use AppBundle\Entity\Application\ItemToBuy;
use AppBundle\Entity\Application\UserServerAccount;
use AppBundle\Enum\Application\PaymentSystem;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Traits\Timestampable;
use AppBundle\Service\Application\Payments\PaymentSystem\PaymentNotificationInterface;
use Collection\Sequence;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use PayPal\Api\Payment;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Application\Payments\OrderRepository")
 * @ORM\Table(name="`Order`")
 */
class Order
{
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $transactionId;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    private $amount;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $paymentSystemAmount;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $cartAmount;

    /**
     * @ORM\Column(type="string", length=3, nullable=false)
     */
    private $currency;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $paymentMethod;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @ORM\Column(type="enum_order_state", nullable=false)
     */
    private $state;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $processedAt;

    /**
     * @ORM\Column(type="payment_system", nullable=true)
     */
    private $paymentSystem;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\ItemToBuy", mappedBy="order", cascade={"persist"}, orphanRemoval=true)
     */
    private $items;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\CartDiscountCoupon")
     * @ORM\JoinColumn(name="discount_coupon_id", referencedColumnName="id", nullable=true)
     */
    private $discountCoupon;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\UserServerAccount", inversedBy="orders")
     * @ORM\JoinColumn(name="user_server_account_id", referencedColumnName="id")
     */
    private $userServerAccount;

    public function __construct(UserServerAccount $userServerAccount, CartDiscountCoupon $discountCoupon = null)
    {
        $this->userServerAccount = $userServerAccount;
        $this->items = new ArrayCollection();
        $this->state = OrderStateEnum::ORDER_STATE_INIT;
        $this->createdAt = new \DateTime();
        $this->discountCoupon = $discountCoupon;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function transactionId(): string
    {
        return $this->transactionId;
    }

    public function amount(): float
    {
        return $this->amount;
    }

    public function changeAmount(float $amount)
    {
        $this->amount = $amount;
    }

    public function description(): string
    {
        return $this->description;
    }

    public function method(): string
    {
        return $this->paymentMethod;
    }

    public function psAmount(): float
    {
        return $this->paymentSystemAmount;
    }

    public function cartAmount(): float
    {
        return $this->cartAmount;
    }

    public function changeDescription(string $description)
    {
        $this->description = $description;
    }

    public function currency(): string
    {
        return $this->currency;
    }

    public function changeCurrency(string $currency)
    {
        $this->currency = $currency;
    }

    public function state(): string
    {
        return $this->state;
    }

    public function processedAt(): ?\DateTime
    {
        return $this->processedAt;
    }

    public function paymentSystem(): ?PaymentSystem
    {
        return $this->paymentSystem;
    }

    public function items(): Collection
    {
        $this->items === null && $this->items = new ArrayCollection();

        return $this->items;
    }

    public function addItem(ItemToBuy $item): self
    {
        $item->setOrder($this);
        $this->items()->add($item);

        return $this;
    }

    public function removeItem(ItemToBuy $item): self
    {
        $items = $this->items();

        if (($index = $items->indexOf($item)) !== false) {
            $items->remove($index);
        }

        return $this;
    }

    public function removeItems(): self
    {
        foreach ($this->items() as $item) {
            $this->removeItem($item);
        }

        return $this;
    }

    public function getUserServerAccount(): UserServerAccount
    {
        return $this->userServerAccount;
    }

    public function setUserServerAccount(UserServerAccount $userServerAccount): self
    {
        $this->userServerAccount = $userServerAccount;

        return $this;
    }

    /**
     * @return CartDiscountCoupon|null
     */
    public function discountCoupon()
    {
        return $this->discountCoupon;
    }

    public function finalizeInterKassaPayment(PaymentNotificationInterface $paymentNotification)
    {
        $this->transactionId = $paymentNotification->transactionId();
        $this->paymentSystemAmount = $paymentNotification->paymentSystemAmount();
        $this->cartAmount = $paymentNotification->cartAmount();
        $this->paymentMethod = $paymentNotification->paymentMethod();
        $this->processedAt = $paymentNotification->processedAt();
        $this->state = OrderStateEnum::ORDER_STATE_SUCCESS;
        $this->paymentSystem = PaymentSystem::byValue(PaymentSystem::INTERKASSA);
    }

    public function startPayPalPayment(Payment $payment): void
    {
        $this->state = OrderStateEnum::ORDER_STATE_STARTED;
        $this->transactionId = $payment->getId();
        $this->paymentSystem = PaymentSystem::byValue(PaymentSystem::PAYPAL);
    }

    public function finalizePayPalPayment(): void
    {
        $this->state = OrderStateEnum::ORDER_STATE_SUCCESS;
        $this->paymentSystemAmount = $this->amount();
        $this->cartAmount = $this->amount();
        $this->paymentMethod = $this->paymentSystem()->getValue();
        $this->processedAt = new \DateTime();
    }

    public function markFailed(PaymentSystem $paymentSystem): void
    {
        $this->state = OrderStateEnum::ORDER_STATE_FAIL;
        $this->paymentSystem = $paymentSystem;
    }

    public function calculateRequiredCreditsCount(): int
    {
        return (new Sequence($this->items()->toArray()))
            ->foldLeft(0, function ($memo, ItemToBuy $item) {
                return $memo + $item->getPriceCredits();
            });
    }

    public function calculateRequiredMoney(): float
    {
        return (new Sequence($this->items()->toArray()))
            ->foldLeft(0, function ($memo, ItemToBuy $item) {
                return $memo + $item->getPriceMoney();
            });
    }

    public function discountedAmount(): float
    {
        return null !== $this->discountCoupon
            ? ceil($this->amount * (100 - $this->discountCoupon()->getDiscount())) / 100
            : $this->amount();
    }
}