<?php

namespace AppBundle\Entity\Application;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="ReferralConfig")
 */
class ReferralConfig
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="smallint")
     */
    protected $requiredReset;

    /**
     * @ORM\Column(type="smallint")
     */
    protected $requiredGrandReset;

    /**
     * @ORM\Column(type="smallint")
     */
    protected $requiredOnlineTime;

    /**
     * @ORM\Column(type="smallint")
     */
    protected $requiredVotes;

    /**
     * @ORM\Column(type="smallint")
     */
    protected $reward;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\Server", inversedBy="referralConfig")
     * @ORM\JoinColumn(name="server_id", referencedColumnName="id")
     */
    protected $server;

    public function __construct()
    {
        $this->requiredReset = 0;
        $this->requiredGrandReset = 0;
        $this->requiredOnlineTime = 0;
        $this->requiredVotes = 0;
        $this->reward = 0;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): ReferralConfig
    {
        $this->id = $id;

        return $this;
    }

    public function getRequiredReset()
    {
        return $this->requiredReset;
    }

    public function setRequiredReset(int $requiredReset): ReferralConfig
    {
        $this->requiredReset = $requiredReset;

        return $this;
    }

    public function getRequiredGrandReset()
    {
        return $this->requiredGrandReset;
    }

    public function setRequiredGrandReset(int $requiredGrandReset): ReferralConfig
    {
        $this->requiredGrandReset = $requiredGrandReset;

        return $this;
    }

    public function getRequiredOnlineTime()
    {
        return $this->requiredOnlineTime;
    }

    public function setRequiredOnlineTime(int $requiredOnlineTime): ReferralConfig
    {
        $this->requiredOnlineTime = $requiredOnlineTime;

        return $this;
    }

    public function getRequiredVotes()
    {
        return $this->requiredVotes;
    }

    public function setRequiredVotes(int $requiredVotes): ReferralConfig
    {
        $this->requiredVotes = $requiredVotes;

        return $this;
    }

    public function getReward()
    {
        return $this->reward;
    }

    public function setReward(int $reward): ReferralConfig
    {
        $this->reward = $reward;

        return $this;
    }

    public function getServer()
    {
        return $this->server;
    }

    public function setServer(Server $server): ReferralConfig
    {
        $this->server = $server;

        return $this;
    }

}