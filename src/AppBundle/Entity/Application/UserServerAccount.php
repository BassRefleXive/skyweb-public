<?php

namespace AppBundle\Entity\Application;

use AppBundle\BonusCode\Model\BonusCode;
use AppBundle\Doctrine\Type\CartPersistentDiscountTypeEnum;
use AppBundle\Entity\Application\Config\VipLevelConfig;
use AppBundle\Entity\Application\Payments\Order;
use AppBundle\Entity\Application\PurchaseLog\CartPurchaseLog;
use AppBundle\Entity\Application\User\User;
use AppBundle\Exception\Application\DisplayableException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Application\UserServerAccountRepository")
 * @ORM\Table(name="UserServerAccount")
 */
class UserServerAccount
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\User\User", inversedBy="userServerAccounts")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Server", inversedBy="serverUserAccounts")
     * @ORM\JoinColumn(name="server_id", referencedColumnName="id")
     */
    private $server;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\Cart", mappedBy="userServerAccount", cascade={"persist"})
     */
    private $cart;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\WebStorage", mappedBy="userServerAccount", cascade={"persist"})
     */
    private $webStorage;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\PurchaseLog\CartPurchaseLog", mappedBy="userServerAccount", cascade={"persist"})
     */
    private $cartPurchaseLog;

    /**
     * @ORM\Column(type="string", length=10, name="account_id")
     */
    private $accountId;

    /**
     * @ORM\Column(name="selected_at", type="datetime")
     */
    private $selectedAt;

    /**
     * @ORM\Column(type="float")
     */
    private $spentMoney;

    /**
     * @ORM\Column(type="integer")
     */
    private $spentCredits;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\MarketItem", mappedBy="userServerAccount", orphanRemoval=true)
     */
    private $marketItems;

    /**
     * @ORM\Column(type="integer")
     */
    private $purchasedStats;

    /**
     * @ORM\Column(name="hidden_till", type="datetime", nullable=true)
     */
    private $hiddenTill;

    /**
     * @ORM\Column(type="integer")
     */
    private $zen;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\Vote", mappedBy="voter", cascade={"persist"})
     */
    private $votes;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\ReferralReward", mappedBy="referral", cascade={"persist"})
     */
    private $referralReward;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\ReferralReward", mappedBy="referred", cascade={"persist"})
     */
    private $referredReward;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\TopVoter", mappedBy="userServerAccount", cascade={"persist"})
     */
    private $monthlyVotersTopRewards;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\Payments\Order", mappedBy="userServerAccount", cascade={"persist"})
     */
    private $orders;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\BonusCode\Model\BonusCode", mappedBy="activatedBy")
     */
    private $bonusCodes;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\MarketItem", mappedBy="boughtBy", cascade={"persist"}, orphanRemoval=true)
     */
    private $boughtMarketItems;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\Config\VipLevelConfig")
     * @ORM\JoinColumn(name="vip_level_id", referencedColumnName="id")
     */
    private $vipLevel;

    /**
     * @ORM\Column(name="vip_expire_date", type="datetime", nullable=true)
     */
    private $vipTill;

    /**
     * @ORM\Column(name="vip_days", type="smallint", nullable=true)
     */
    private $vipDays;

    public function __construct()
    {
        $this->selectedAt = new \DateTime();
        $this->cart = (new Cart())->setUserServerAccount($this);
        $this->webStorage = (new WebStorage())->setUserServerAccount($this);
        $this->cartPurchaseLog = new ArrayCollection();
        $this->spentCredits = 0;
        $this->spentMoney = 0;
        $this->marketItems = new ArrayCollection();
        $this->purchasedStats = 0;
        $this->zen = 0;
        $this->referralReward = new ArrayCollection();
        $this->votes = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->bonusCodes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getServer(): Server
    {
        return $this->server;
    }

    public function setServer(Server $server): self
    {
        $this->server = $server;

        return $this;
    }

    public function getAccountId(): string
    {
        return $this->accountId;
    }

    public function setAccountId(string $accountId): self
    {
        $this->accountId = $accountId;

        return $this;
    }

    public function getSelectedAt(): \DateTime
    {
        return $this->selectedAt;
    }

    public function setSelectedAt(\DateTime $selectedAt): self
    {
        $this->selectedAt = $selectedAt;

        return $this;
    }

    public function getCart(): Cart
    {
        $this->cart === null && $this->cart = (new Cart())->setUserServerAccount($this);

        return $this->cart;
    }

    public function setCart(Cart $cart): self
    {
        $this->cart = $cart;

        return $this;
    }

    public function getWebStorage(): WebStorage
    {
        $this->webStorage === null && $this->webStorage = (new WebStorage())->setUserServerAccount($this);

        return $this->webStorage;
    }

    public function setWebStorage(WebStorage $webStorage): self
    {
        $this->webStorage = $webStorage;

        return $this;
    }

    public function getCartPurchaseLog(): Collection
    {
        $this->cartPurchaseLog === null && $this->cartPurchaseLog = new ArrayCollection();

        return $this->cartPurchaseLog;
    }

    public function addCartPurchaseLog(CartPurchaseLog $item): self
    {
        $item->setUserServerAccount($this);
        $this->getCartPurchaseLog()->add($item);

        return $this;
    }

    public function getSpentMoney()
    {
        return $this->spentMoney;
    }

    public function setSpentMoney(float $spentMoney): self
    {
        $this->spentMoney = $spentMoney;

        return $this;
    }

    public function addSpentMoney(float $money): self
    {
        $this->spentMoney += $money;

        return $this;
    }

    public function getSpentCredits()
    {
        return $this->spentCredits;
    }

    public function setSpentCredits(int $spentCredits): self
    {
        $this->spentCredits = $spentCredits;

        return $this;
    }

    public function addSpentCredits(int $credits): self
    {
        $this->spentCredits += $credits;

        return $this;
    }

    public function getMarketItems(): Collection
    {
        $this->marketItems === null && $this->marketItems = new ArrayCollection();

        return $this->marketItems;
    }

    public function activeMarketItems(): Collection
    {
        return $this->getMarketItems()->filter(function (MarketItem $item): bool {
            return null === $item->boughtBy();
        });
    }

    public function setMarketItems(Collection $items): self
    {
        $this->marketItems = $items;

        return $this;
    }

    public function addMarketItem(MarketItem $item): self
    {
        $item->setUserServerAccount($this);

        $this->getMarketItems()->add($item);

        return $this;
    }

    public function removeMarketItem(MarketItem $item): self
    {
        $items = $this->getMarketItems();

        if (($index = $items->indexOf($item)) !== false) {
            $items->remove($index);
        }

        return $this;
    }

    public function getPurchasedStats()
    {
        return $this->purchasedStats;
    }

    public function setPurchasedStats(int $purchasedStats): self
    {
        $this->purchasedStats = $purchasedStats;

        return $this;
    }

    public function addPurchasedStats(int $purchasedStats): self
    {
        $this->purchasedStats += $purchasedStats;

        return $this;
    }

    public function hiddenTill(): ?\DateTimeInterface
    {
        return $this->hiddenTill;
    }

    public function setHiddenTill(\DateTimeInterface $hiddenTill): self
    {
        $this->hiddenTill = $hiddenTill;

        return $this;
    }

    public function hideInfo(int $days): self
    {
        $this->hiddenTill = (new \DateTime())->add(new \DateInterval(sprintf('P%dD', $days)));

        return $this;
    }

    public function unHideInfo(): self
    {
        $this->hiddenTill = null;

        return $this;
    }

    public function zen(): int
    {
        return $this->zen;
    }

    public function depositZen(int $amount): self
    {
        $this->zen += $amount;

        return $this;
    }

    public function withdrawZen(int $amount): self
    {
        $this->zen -= $amount;

        return $this;
    }

    public function getReferralReward()
    {
        return $this->referralReward;
    }

    public function setReferralReward(ReferralReward $referralReward): self
    {
        $this->referralReward = $referralReward;

        return $this;
    }

    public function getReferredReward()
    {
        return $this->referredReward;
    }

    public function setReferredReward(ReferralReward $referredReward): self
    {
        $this->referredReward = $referredReward;

        return $this;
    }

    public function getMonthlyVotersTopRewards()
    {
        return $this->monthlyVotersTopRewards;
    }

    public function getVotes(): Collection
    {
        $this->votes === null && $this->votes = new ArrayCollection();

        return $this->votes;
    }

    public function getRegularVotes(bool $thisMonthOnly = false): Collection
    {
        $thisMonthStart = new \DateTime(date('Y-m-1 00:00:00', time()));

        return $this->getVotes()->filter(function (Vote $vote) use ($thisMonthOnly, $thisMonthStart) {
            return (
                $vote->getType()->isRegular() &&
                (
                $thisMonthOnly
                    ? $vote->getDateTime() >= $thisMonthStart
                    : true
                )
            );
        });
    }

    public function getSmsVotes(bool $thisMonthOnly = false): Collection
    {
        $thisMonthStart = new \DateTime(date('Y-m-1 00:00:00', time()));

        return $this->getVotes()->filter(function (Vote $vote) use ($thisMonthOnly, $thisMonthStart) {
            return (
                $vote->getType()->isPaid() &&
                (
                $thisMonthOnly
                    ? $vote->getDateTime() >= $thisMonthStart
                    : true
                )
            );
        });
    }

    public function setVotes(Collection $votes): self
    {
        $this->votes = $votes;

        return $this;
    }

    public function addVote(Vote $vote): self
    {
        $vote->setVoter($this);

        $this->getVotes()->add($vote);

        return $this;
    }

    public function removeVote(Vote $vote): self
    {
        $items = $this->getVotes();

        if (($index = $items->indexOf($vote)) !== false) {
            $items->remove($index);
        }

        return $this;
    }

    public function getSpentAmount(string $type): float
    {
        if (!array_key_exists($type, CartPersistentDiscountTypeEnum::getTypes())) {
            throw new \InvalidArgumentException('Invalid CartPersistentDiscount type.');
        }

        switch ($type) {
            case CartPersistentDiscountTypeEnum::PERSISTENT_DISCOUNT_TYPE_CREDITS: {
                $result = $this->getSpentCredits();
                break;
            }
            case CartPersistentDiscountTypeEnum::PERSISTENT_DISCOUNT_TYPE_MONEY: {
                $result = $this->getSpentMoney();
                break;
            }
            default: {
                throw new \RuntimeException('Not implemented.');
            }
        }

        return (float)$result;
    }

    public function orders(): Collection
    {
        $this->orders === null && $this->orders = new ArrayCollection();

        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        $order->setUserServerAccount($this);

        $this->orders()->add($order);

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        $items = $this->orders();

        if (($index = $items->indexOf($order)) !== false) {
            $items->remove($index);
        }

        return $this;
    }

    public function bonusCodes(): Collection
    {
        $this->bonusCodes === null && $this->bonusCodes = new ArrayCollection();

        return $this->bonusCodes;
    }

    public function activateBonusCode(BonusCode $bonusCode)
    {
        if ($bonusCode->activated()) {
            throw new DisplayableException('errors.44');
        }

        if (!$bonusCode->enabled()) {
            throw new DisplayableException('errors.45');
        }

        if ($bonusCode->server()->getId() !== $this->getServer()->getId()) {
            throw new DisplayableException('errors.46');
        }

        $bonusCode->activate($this);
    }

    public function getBoughtMarketItems(): Collection
    {
        $this->boughtMarketItems === null && $this->boughtMarketItems = new ArrayCollection();

        return $this->boughtMarketItems;
    }

    public function addBoughtMarketItem(MarketItem $item): self
    {
        $item->setBoughtBy($this);

        $this->getBoughtMarketItems()->add($item);

        return $this;
    }

    public function activateVip(VipLevelConfig $vipLevelConfig, int $days): self
    {
        $this->vipLevel = $vipLevelConfig;
        $this->extendVip($days);

        return $this;
    }

    public function extendVip(int $days): self
    {
        null === $this->vipTill && $this->vipTill = new \DateTime();
        $this->vipTill = clone $this->vipTill;
        $this->vipTill->add(new \DateInterval(sprintf('P%dD', $days)));
        $this->vipDays = $days;

        return $this;
    }

    public function deactivateVip(): self
    {
        $this->vipLevel = null;
        $this->vipTill = null;
        $this->vipDays = null;

        return $this;
    }

    public function isVip(): bool
    {
        return null !== $this->vipLevel;
    }

    public function vipLevel(): ?VipLevelConfig
    {
        return $this->vipLevel;
    }

    public function vipActiveTill(): ?\DateTime
    {
        return $this->vipTill;
    }

    public function vipDays(): ?int
    {
        return $this->vipDays;
    }
}