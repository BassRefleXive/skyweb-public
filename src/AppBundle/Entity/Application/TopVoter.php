<?php


namespace AppBundle\Entity\Application;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Application\TopVoterRepository")
 * @ORM\Table(name="TopVoter")
 */
class TopVoter
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     */
    private $place;

    /**
     * @ORM\Column(type="integer")
     */
    private $regularVotes;

    /**
     * @ORM\Column(type="integer")
     */
    private $smsVotes;

    /**
     * @ORM\Column(type="smallint")
     */
    private $reward;

    /**
     * @ORM\Column(name="period_start", type="datetime")
     */
    private $periodStart;

    /**
     * @ORM\Column(name="period_end", type="datetime")
     */
    private $periodEnd;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\UserServerAccount", inversedBy="monthlyVotersTopRewards")
     * @ORM\JoinColumn(name="voter_user_server_account_id", referencedColumnName="id")
     */
    private $userServerAccount;

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): TopVoter
    {
        $this->id = $id;

        return $this;
    }

    public function getPlace()
    {
        return $this->place;
    }

    public function setPlace(int $place): TopVoter
    {
        $this->place = $place;

        return $this;
    }

    public function getRegularVotes()
    {
        return $this->regularVotes;
    }

    public function setRegularVotes(int $regularVotes): TopVoter
    {
        $this->regularVotes = $regularVotes;

        return $this;
    }

    public function getSmsVotes()
    {
        return $this->smsVotes;
    }

    public function setSmsVotes(int $smsVotes): TopVoter
    {
        $this->smsVotes = $smsVotes;

        return $this;
    }

    public function getReward()
    {
        return $this->reward;
    }

    public function setReward(int $reward): TopVoter
    {
        $this->reward = $reward;

        return $this;
    }

    public function setPeriod(\DateTimeInterface $start, \DateTimeInterface $end): self
    {
        $this->periodStart = $start;
        $this->periodEnd = $end;

        return $this;
    }

    public function getUserServerAccount()
    {
        return $this->userServerAccount;
    }

    public function setUserServerAccount(UserServerAccount $userServerAccount): TopVoter
    {
        $this->userServerAccount = $userServerAccount;

        return $this;
    }
}