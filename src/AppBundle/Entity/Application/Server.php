<?php

namespace AppBundle\Entity\Application;

use AppBundle\Doctrine\Type\CartPersistentDiscountTypeEnum;
use AppBundle\Entity\Application\Config\CharacterEvolutionConfig;
use AppBundle\Entity\Application\Config\WCoinShopConfig;
use Collection\Sequence;
use Collection\SequenceInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Application\ServerRepository")
 * @ORM\Table(name="Server")
 */
class Server
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $exp;

    /**
     * @ORM\Column(type="integer", name="`drop`")
     */
    private $drop;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $version;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $maxOnline;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $addOnline;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $enabled;

    /**
     * @ORM\OneToOne(targetEntity="DatabaseCredential", fetch="EAGER", cascade={"persist"})
     * @ORM\JoinColumn(name="database_credential_id", referencedColumnName="id", nullable=false)
     */
    private $database;

    /**
     * @ORM\OneToMany(targetEntity="UserServerAccount", mappedBy="server")
     */
    private $serverUserAccounts;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\News", mappedBy="server", cascade={"persist"})
     */
    private $news;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\WebShopConfig", mappedBy="server", cascade={"persist"})
     */
    private $webShopConfig;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\CartDiscountCoupon", mappedBy="server", cascade={"persist"})
     */
    private $cartDiscountCoupons;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\CartPersistentDiscount", mappedBy="server", cascade={"persist"})
     */
    private $cartPersistentDiscounts;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\MarketConfig", mappedBy="server", cascade={"persist"})
     */
    private $marketConfig;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\MarketItem", mappedBy="server", cascade={"persist"})
     */
    private $marketItems;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\StatsShopConfig", mappedBy="server", cascade={"persist"})
     */
    private $statsShopConfig;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\AccountMenuConfig", mappedBy="server", cascade={"persist"})
     */
    private $accountMenuConfig;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\ReferralConfig", mappedBy="server", cascade={"persist"})
     */
    private $referralConfig;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\VoteRewardConfig", mappedBy="server", cascade={"persist"})
     */
    private $voteRewardConfig;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\FaqInfo", mappedBy="server", cascade={"persist"})
     */
    private $faqInfo;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\BonusCode\Model\BonusCode", mappedBy="server")
     */
    private $bonusCodes;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\DataServerCredentials", fetch="EAGER", cascade={"persist"})
     * @ORM\JoinColumn(name="data_server_credentials_id", referencedColumnName="id", nullable=false)
     */
    private $dataServer;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Statistics\OnlinePlayers\Model\OnlinePlayers", mappedBy="server")
     */
    private $onlinePlayersStatistics;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\Config\CharacterEvolutionConfig", mappedBy="server", cascade={"persist"})
     */
    private $characterEvolutionConfig;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\Config\VipLevelConfig", mappedBy="server")
     */
    private $vipLevels;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\Config\WCoinShopConfig", mappedBy="server", cascade={"persist"})
     */
    private $wCoinShopConfig;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\Timer", mappedBy="server", cascade={"persist"})
     */
    private $timers;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\Config\CastleSiegeCycle", mappedBy="server", cascade={"persist"})
     */
    private $castleSiegeCycles;

    public function __construct()
    {
        $this->serverUserAccounts = new ArrayCollection();
        $this->news = new ArrayCollection();
        $this->cartDiscountCoupons = new ArrayCollection();
        $this->cartPersistentDiscounts = new ArrayCollection();
        $this->marketItems = new ArrayCollection();
        $this->bonusCodes = new ArrayCollection();
        $this->onlinePlayersStatistics = new ArrayCollection();
        $this->vipLevels = new ArrayCollection();
        $this->castleSiegeCycles = new ArrayCollection();
        $this->timers = new ArrayCollection();
        $this->maxOnline = 0;
        $this->addOnline = 0;
        $this->enabled = true;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getExp()
    {
        return $this->exp;
    }

    public function setExp(int $exp): self
    {
        $this->exp = $exp;

        return $this;
    }

    public function getDrop()
    {
        return $this->drop;
    }

    public function setDrop(int $drop): self
    {
        $this->drop = $drop;

        return $this;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion(string $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription(string $description)
    {
        $this->description = $description;

        return $this;
    }

    public function getMaxOnline(): int
    {
        null === $this->maxOnline && $this->maxOnline = 0;

        return $this->maxOnline;
    }

    public function setMaxOnline(int $maxOnline): self
    {
        $this->maxOnline = $maxOnline;

        return $this;
    }

    public function getAddOnline(): int
    {
        null === $this->addOnline && $this->addOnline = 0;

        return $this->addOnline;
    }

    public function setAddOnline(int $addOnline): self
    {
        $this->addOnline = $addOnline;

        return $this;
    }

    public function increasedOnline(int $online): int
    {
        if ($online <= 0) {
            return $online;
        }

        return $this->addOnline + $online;
    }

    public function getEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getDatabase()
    {
        return $this->database;
    }

    public function setDatabase(DatabaseCredential $database): self
    {
        $this->database = $database;

        return $this;
    }

    /**
     * @return DataServerCredentials
     */
    public function getDataServer()
    {
        return $this->dataServer;
    }

    public function setDataServer(DataServerCredentials $dataServer): self
    {
        $this->dataServer = $dataServer;

        return $this;
    }

    public function addNews(News $news): Server
    {
        $this->getNews()->add($news);

        return $this;
    }

    public function getNews(): Collection
    {
        $this->news === null && $this->news = new ArrayCollection();

        return $this->news;
    }

    /**
     * @return WebShopConfig
     */
    public function getWebShopConfig()
    {
        return $this->webShopConfig;
    }

    public function setWebShopConfig(WebShopConfig $webShopConfig): self
    {
        $webShopConfig->setServer($this);

        $this->webShopConfig = $webShopConfig;

        return $this;
    }

    public function getCharacterEvolutionConfig(): ?CharacterEvolutionConfig
    {
        return $this->characterEvolutionConfig;
    }

    public function setCharacterEvolutionConfig(CharacterEvolutionConfig $characterEvolutionConfig): self
    {
        $characterEvolutionConfig->setServer($this);

        $this->characterEvolutionConfig = $characterEvolutionConfig;

        return $this;
    }

    public function getCartDiscountCoupons(): Collection
    {
        $this->cartDiscountCoupons === null && $this->cartDiscountCoupons = new ArrayCollection();

        return $this->cartDiscountCoupons;
    }

    public function addCartDiscountCoupon(CartDiscountCoupon $coupon): self
    {
        $coupon->setServer($this);

        $this->getCartDiscountCoupons()->add($coupon);

        return $this;
    }

    public function removeCartDiscountCoupon(CartDiscountCoupon $coupon): self
    {
        $this->getCartDiscountCoupons()->remove($coupon);

        return $this;
    }


    public function getCartPersistentDiscounts(): Collection
    {
        $this->cartPersistentDiscounts === null && $this->cartPersistentDiscounts = new ArrayCollection();

        return $this->cartPersistentDiscounts;
    }

    public function addCartPersistentDiscount(CartPersistentDiscount $discount): self
    {
        $discount->setServer($this);

        $this->getCartPersistentDiscounts()->add($discount);

        return $this;
    }

    public function removeCartPersistentDiscount(CartPersistentDiscount $discount): self
    {
        $this->getCartPersistentDiscounts()->remove($discount);

        return $this;
    }

    public function getCartPersistentDiscountsSortedByDiscount(): SequenceInterface
    {
        return (new Sequence($this->getCartPersistentDiscounts()->toArray()))
            ->sortWith(function (CartPersistentDiscount $a, CartPersistentDiscount $b) {
                return $a->getDiscount() <=> $b->getDiscount();
            });
    }

    public function getMoneyCartPersistentDiscountsSortedByDiscount(): SequenceInterface
    {
        return $this
            ->getCartPersistentDiscountsSortedByDiscount()
            ->filter(function (CartPersistentDiscount $discount) {
                return $discount->getType() === CartPersistentDiscountTypeEnum::PERSISTENT_DISCOUNT_TYPE_MONEY;
            });
    }

    public function getCreditsCartPersistentDiscountsSortedByDiscount(): SequenceInterface
    {
        return $this
            ->getCartPersistentDiscountsSortedByDiscount()
            ->filter(function (CartPersistentDiscount $discount) {
                return $discount->getType() === CartPersistentDiscountTypeEnum::PERSISTENT_DISCOUNT_TYPE_CREDITS;
            });
    }

    /**
     * @return MarketConfig
     */
    public function getMarketConfig()
    {
        return $this->marketConfig;
    }

    public function setMarketConfig(MarketConfig $marketConfig): self
    {
        $marketConfig->setServer($this);

        $this->marketConfig = $marketConfig;

        return $this;
    }

    public function getMarketItems(): Collection
    {
        $this->marketItems === null && $this->marketItems = new ArrayCollection();

        return $this->marketItems;
    }

    public function setMarketItems(Collection $items): self
    {
        $this->marketItems = $items;

        return $this;
    }

    public function addMarketItem(MarketItem $item): self
    {
        $item->setServer($this);

        $this->getMarketItems()->add($item);

        return $this;
    }

    public function removeMarketItem(MarketItem $item): self
    {
        $this->getMarketItems()->remove($item);

        return $this;
    }

    public function getStatsShopConfig(): ?StatsShopConfig
    {
        return $this->statsShopConfig;
    }

    public function setStatsShopConfig(StatsShopConfig $statsShopConfig): self
    {
        $statsShopConfig->setServer($this);

        $this->statsShopConfig = $statsShopConfig;

        return $this;
    }

    /**
     * @return AccountMenuConfig
     */
    public function getAccountMenuConfig()
    {
        return $this->accountMenuConfig;
    }

    public function setAccountMenuConfig(AccountMenuConfig $accountMenuConfig): self
    {
        $accountMenuConfig->setServer($this);

        $this->accountMenuConfig = $accountMenuConfig;

        return $this;
    }

    /**
     * @return ReferralConfig
     */
    public function getReferralConfig()
    {
        return $this->referralConfig;
    }

    public function setReferralConfig(ReferralConfig $referralConfig): self
    {
        $referralConfig->setServer($this);

        $this->referralConfig = $referralConfig;

        return $this;
    }

    /** @return VoteRewardConfig */
    public function getVoteRewardConfig()
    {
        return $this->voteRewardConfig;
    }

    public function setVoteRewardConfig(VoteRewardConfig $voteRewardConfig): self
    {
        $voteRewardConfig->setServer($this);

        $this->voteRewardConfig = $voteRewardConfig;

        return $this;
    }

    /** @return FaqInfo */
    public function getFaqInfo()
    {
        return $this->faqInfo;
    }

    public function setFaqInfo(FaqInfo $faqInfo): self
    {
        $faqInfo->setServer($this);

        $this->faqInfo = $faqInfo;

        return $this;
    }

    public function bonusCodes(): Collection
    {
        $this->bonusCodes === null && $this->bonusCodes = new ArrayCollection();

        return $this->bonusCodes;
    }

    public function onlinePlayersStatistics(): Collection
    {
        $this->onlinePlayersStatistics === null && $this->onlinePlayersStatistics = new ArrayCollection();

        return $this->onlinePlayersStatistics;
    }

    public function vipLevels(): Collection
    {
        $this->vipLevels === null && $this->vipLevels = new ArrayCollection();

        return $this->vipLevels;
    }

    public function getWCoinShopConfig(): ?WCoinShopConfig
    {
        return $this->wCoinShopConfig;
    }

    public function setWCoinShopConfig(WCoinShopConfig $wCoinShopConfig): self
    {
        $wCoinShopConfig->setServer($this);

        $this->wCoinShopConfig = $wCoinShopConfig;

        return $this;
    }

    public function getHash()
    {
        return md5(implode(
            ':',
            [
                $this->getId(),
                $this->getExp(),
                $this->getDrop(),
                $this->getVersion(),
                $this->getName(),
                $this->getDescription(),
            ]
        ));
    }

    public function addTimer(Timer $timer): self
    {
        $this->timers()->add($timer);

        return $this;
    }

    public function timers(): Collection
    {
        $this->timers === null && $this->timers = new ArrayCollection();

        return $this->timers;
    }

    public function castleSiegeCycles(): Collection
    {
        $this->castleSiegeCycles === null && $this->castleSiegeCycles = new ArrayCollection();

        return $this->castleSiegeCycles;
    }
}