<?php

namespace AppBundle\Entity\Application;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="WebStorage")
 */
class WebStorage
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\UserServerAccount", inversedBy="webStorage")
     * @ORM\JoinColumn(name="user_server_account_id", referencedColumnName="id")
     */
    protected $userServerAccount;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\WebStorageItem", mappedBy="webStorage", cascade={"persist"}, orphanRemoval=true)
     */
    protected $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): WebStorage
    {
        $this->id = $id;

        return $this;
    }

    public function getUserServerAccount(): UserServerAccount
    {
        return $this->userServerAccount;
    }

    public function setUserServerAccount(UserServerAccount $userServerAccount): WebStorage
    {
        $this->userServerAccount = $userServerAccount;

        return $this;
    }

    public function getItems(): Collection
    {
        $this->items === null && $this->items = new ArrayCollection();

        return $this->items;
    }

    public function addItem(WebStorageItem $item): WebStorage
    {
        $item->setWebStorage($this);
        $this->getItems()->add($item);

        return $this;
    }

    public function removeItem(WebStorageItem $item): WebStorage
    {
        $items = $this->getItems();

        if (($index = $items->indexOf($item)) !== false) {
            $items->remove($index);
        }

        return $this;
    }

    public function removeItems(): WebStorage
    {
        foreach ($this->getItems() as $item) {
            $this->removeItem($item);
        }

        return $this;
    }
}