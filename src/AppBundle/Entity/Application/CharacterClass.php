<?php

namespace AppBundle\Entity\Application;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Application\CharacterClassRepository")
 * @ORM\Table(name="CharacterClass")
 */
class CharacterClass
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=5, unique=true)
     */
    protected $abbreviation;

    /**
     * @ORM\Column(type="enum_character_class")
     */
    protected $type;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Application\Items\ItemInfo", mappedBy="equippedBy")
     */
    protected $equippedItems;

    public function __construct()
    {
        $this->equippedItems = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAbbreviation(): string
    {
        return $this->abbreviation;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function getEquippedItems(): Collection
    {
        return $this->equippedItems;
    }

}