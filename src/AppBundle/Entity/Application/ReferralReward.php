<?php

namespace AppBundle\Entity\Application;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="ReferralReward")
 */
class ReferralReward
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\UserServerAccount", inversedBy="referralReward")
     * @ORM\JoinColumn(name="referral_user_server_account_id", referencedColumnName="id")
     */
    protected $referral;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\UserServerAccount", inversedBy="referredReward")
     * @ORM\JoinColumn(name="referred_user_server_account_id", referencedColumnName="id")
     */
    protected $referred;

    /**
     * @ORM\Column(type="integer")
     */
    protected $reward;

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): ReferralReward
    {
        $this->id = $id;

        return $this;
    }

    public function getReferral()
    {
        return $this->referral;
    }

    public function setReferral(UserServerAccount $referral): ReferralReward
    {
        $this->referral = $referral;

        return $this;
    }

    public function getReferred()
    {
        return $this->referred;
    }

    public function setReferred(UserServerAccount $referred): ReferralReward
    {
        $this->referred = $referred;

        return $this;
    }

    public function getReward()
    {
        return $this->reward;
    }

    public function setReward(int $reward): ReferralReward
    {
        $this->reward = $reward;

        return $this;
    }

}