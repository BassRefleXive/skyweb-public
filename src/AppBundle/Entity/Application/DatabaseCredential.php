<?php

namespace AppBundle\Entity\Application;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="DatabaseCredentials")
 */
class DatabaseCredential
{
    const DRIVER_MSSQL = 'sqlsrv';

    const CHARSET_UTF_8 = 'UTF-8';
    const CHARSET_SQLSRV_ENC_CHAR = 'SQLSRV_ENC_CHAR';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $host;

    /**
     * @ORM\Column(type="integer")
     */
    protected $port;

    /**
     * @ORM\Column(type="string", length=255, name="dbname")
     */
    protected $databaseName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $password;

    /**
     * @ORM\Column(type="string", length=10)
     */
    protected $charset;

    /**
     * @ORM\Column(type="string", length=10)
     */
    protected $driver;

    public function __construct()
    {
        $this->driver = DatabaseCredential::DRIVER_MSSQL;
        $this->charset = DatabaseCredential::CHARSET_UTF_8;
    }


    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): DatabaseCredential
    {
        $this->id = $id;

        return $this;
    }

    public function getHost()
    {
        return $this->host;
    }

    public function setHost(string $host): DatabaseCredential
    {
        $this->host = $host;

        return $this;
    }

    public function getPort()
    {
        return $this->port;
    }

    public function setPort(int $port): DatabaseCredential
    {
        $this->port = $port;

        return $this;
    }

    public function getDatabaseName()
    {
        return $this->databaseName;
    }

    public function setDatabaseName(string $databaseName): DatabaseCredential
    {
        $this->databaseName = $databaseName;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(string $user): DatabaseCredential
    {
        $this->user = $user;

        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword(string $password): DatabaseCredential
    {
        $this->password = $password;

        return $this;
    }

    public function getCharset()
    {
        return $this->charset;
    }

    public function setCharset(string $charset): DatabaseCredential
    {
        $this->charset = $charset;

        return $this;
    }

    public function getDriver()
    {
        return $this->driver;
    }

    public function setDriver(string $driver): DatabaseCredential
    {
        $this->driver = $driver;

        return $this;
    }

    public function getHash()
    {
        return md5(implode(
            ':',
            [
                $this->getId(),
                $this->getHost(),
                $this->getPort(),
                $this->getDatabaseName(),
                $this->getUser(),
                $this->getPassword(),
                $this->getCharset(),
                $this->getDriver(),
            ]
        ));
    }

}