<?php

namespace AppBundle\Entity\Application;

use AppBundle\Repository\Application\WebShopCategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Application\WebShopCategoryRepository")
 * @ORM\Table(name="WebShopCategory")
 */
class WebShopCategory
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="name", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="smallint")
     */
    private $status;

    /**
     * @ORM\Column(type="smallint")
     */
    private $position;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\WebShopConfig", inversedBy="categories")
     * @ORM\JoinColumn(name="web_shop_config_id", referencedColumnName="id")
     */
    private $config;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\WebShopCategoryItem", mappedBy="webShopCategory", cascade={"persist", "remove"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $items;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\WebShopCategory", inversedBy="childCategories")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parentCategory;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\WebShopCategory", mappedBy="parentCategory")
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $childCategories;

    public function __construct(WebShopConfig $config)
    {
        $this->status = WebShopCategoryRepository::STATUS_DISABLED;
        $this->items = new ArrayCollection();
        $this->childCategories = new ArrayCollection();
        $this->position = 0;

        $this->config = $config;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): WebShopCategory
    {
        $this->id = $id;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name): WebShopCategory
    {
        $this->name = $name;

        return $this;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): WebShopCategory
    {
        if (!in_array($status, [WebShopCategoryRepository::STATUS_DISABLED, WebShopCategoryRepository::STATUS_ENABLED], true)) {
            throw new \InvalidArgumentException('Invalid WebshopCategory status');
        }

        $this->status = $status;

        return $this;
    }

    /**
     * @return WebShopConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    public function getItems(): Collection
    {
        $this->items === null && $this->items = new ArrayCollection();

        return $this->items;
    }

    public function setItems(Collection $items): WebShopCategory
    {
        /** @var WebShopCategoryItem $item */
        foreach ($items as $item) {
            $item->setWebShopCategory($this);
        }
        $this->items = $items;

        return $this;
    }

    public function addItem(WebShopCategoryItem $item): WebShopCategory
    {
        $item->setWebShopCategory($this);

        $this->getItems()->add($item);

        return $this;
    }

    public function removeItem(WebShopCategoryItem $item): WebShopCategory
    {
        if (($index = $this->getItems()->indexOf($item)) !== false) {
            $this->getItems()->remove($index);
        }

        return $this;
    }

    public function childCategories(): Collection
    {
        $this->childCategories === null && $this->childCategories = new ArrayCollection();

        return $this->childCategories;
    }

    public function setParentCategory(WebShopCategory $category)
    {
        $this->parentCategory = $category;
    }

    /**
     * @return WebShopCategory|null
     */
    public function parentCategory()
    {
        return $this->parentCategory;
    }

    public function isNew(): bool
    {
        return $this->id === null;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function position(): int
    {
        return $this->position;
    }
}