<?php

namespace AppBundle\Entity\Application;

use AppBundle\Entity\Application\User\User;
use AppBundle\Repository\Application\NewsRepository;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Traits\Timestampable;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Application\NewsRepository")
 * @ORM\Table(name="News")
 */
class News
{
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $titleRus;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $titleEng;

    /**
     * @ORM\Column(type="text")
     */
    private $shortTextEng;

    /**
     * @ORM\Column(type="text")
     */
    private $shortTextRus;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $fullTextEng;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $fullTextRus;

    /**
     * @ORM\Column(type="smallint")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\User\User", inversedBy="news")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $author;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\Server", inversedBy="news")
     * @ORM\JoinColumn(name="server_id", referencedColumnName="id")
     */
    private $server;

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getShortTextEng()
    {
        return $this->shortTextEng;
    }

    public function setShortTextEng(string $text): self
    {
        $this->shortTextEng = $text;

        return $this;
    }

    public function getShortTextRus()
    {
        return $this->shortTextRus;
    }

    public function setShortTextRus(string $text): self
    {
        $this->shortTextRus = $text;

        return $this;
    }

    public function getShortText(string $locale): string
    {
        $res = $locale === 'en' ? $this->getShortTextEng() : $this->getShortTextRus();

        return $res ?? '';
    }

    public function getTitleEng(): string
    {
        null === $this->titleEng && $this->titleEng = '';

        return $this->titleEng;
    }

    public function setTitleEng(string $title): self
    {
        $this->titleEng = $title;

        return $this;
    }

    public function getTitleRus(): string
    {
        null === $this->titleRus && $this->titleRus = '';

        return $this->titleRus;
    }

    public function setTitleRus(string $title): self
    {
        $this->titleRus = $title;

        return $this;
    }

    public function getTitle(string $locale): string
    {
        $res = $locale === 'en' ? $this->getTitleEng() : $this->getTitleRus();

        return $res ?? '';
    }

    public function getFullTextEng(): ?string
    {
        return $this->fullTextEng;
    }

    public function setFullTextEng(?string $text): self
    {
        $this->fullTextEng = $text;

        return $this;
    }

    public function getFullTextRus(): ?string
    {
        return $this->fullTextRus;
    }

    public function setFullTextRus(?string $text): self
    {
        $this->fullTextRus = $text;

        return $this;
    }

    public function getFullText(string $locale): ?string
    {
        $res = $locale === 'en' ? $this->getFullTextEng() : $this->getFullTextRus();

        return $res ?? '';
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        if (!in_array($status, [NewsRepository::STATUS_DISABLED, NewsRepository::STATUS_ENABLED], true)) {
            throw new \InvalidArgumentException('Invalid News status');
        }
        $this->status = $status;

        return $this;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function setAuthor(User $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getServer()
    {
        return $this->server;
    }

    public function setServer(Server $server): self
    {
        $this->server = $server;

        return $this;
    }

}