<?php

namespace AppBundle\Entity\Application;

use AppBundle\Entity\Application\Items\ItemInfo;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="MarketCategory")
 */
class MarketCategory
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", name="name", length=50)
     */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\Items\ItemInfo", mappedBy="marketCategory", cascade={"persist"})
     */
    protected $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): MarketCategory
    {
        $this->id = $id;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name): MarketCategory
    {
        $this->name = $name;

        return $this;
    }

    public function getItems(): Collection
    {
        $this->items === null && $this->items = new ArrayCollection();

        return $this->items;
    }

    public function setItems(Collection $items): MarketCategory
    {
        $this->items = $items;

        return $this;
    }

    public function addItem(ItemInfo $item): MarketCategory
    {
        $item->setMarketCategory($this);

        $this->getItems()->add($item);

        return $this;
    }

    public function removeItem(ItemInfo $item): MarketCategory
    {
        $this->getItems()->remove($item);

        return $this;
    }

}