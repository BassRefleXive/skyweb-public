<?php

namespace AppBundle\Entity\Application;


use AppBundle\Entity\Traits\Timestampable;
use AppBundle\Enum\Application\TimerStatus;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Application\TimerRepository")
 * @ORM\Table(name="Timer")
 */
class Timer
{
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $titleEng;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $titleRus;

    /**
     * @ORM\Column(type="datetime")
     */
    private $time;

    /**
     * @ORM\Column(type="timer_status_type")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\Server", inversedBy="timers")
     * @ORM\JoinColumn(name="server_id", referencedColumnName="id")
     */
    private $server;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getTitleEng(): ?string
    {
        return $this->titleEng;
    }

    public function setTitleEng(string $titleEng): self
    {
        $this->titleEng = $titleEng;

        return $this;
    }

    public function getTitleRus(): ?string
    {
        return $this->titleRus;
    }

    public function setTitleRus(string $titleRus): self
    {
        $this->titleRus = $titleRus;

        return $this;
    }

    public function getTime(): ?\DateTimeInterface
    {
        return $this->time;
    }

    public function setTime(\DateTimeInterface $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getStatus(): ?TimerStatus
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = TimerStatus::byValue($status);

        return $this;
    }

    public function getServer(): ?Server
    {
        return $this->server;
    }

    public function setServer(Server $server): self
    {
        $this->server = $server;

        return $this;
    }
}