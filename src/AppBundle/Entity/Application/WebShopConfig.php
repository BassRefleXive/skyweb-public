<?php

namespace AppBundle\Entity\Application;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity()
 * @ORM\Table(name="WebShopConfig")
 */
class WebShopConfig
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="smallint")
     */
    protected $maxItemLevel;

    /**
     * @ORM\Column(type="smallint")
     */
    protected $maxItemOption;

    /**
     * @ORM\Column(type="smallint")
     */
    protected $maxExcCount;

    /**
     * @ORM\Column(type="float")
     */
    protected $levelPriceAddMoney;

    /**
     * @ORM\Column(type="float")
     */
    protected $optionPriceAddMoney;

    /**
     * @ORM\Column(type="smallint")
     */
    protected $levelPriceAddCredits;

    /**
     * @ORM\Column(type="smallint")
     */
    protected $optionPriceAddCredits;

    /**
     * @ORM\Column(type="float")
     */
    protected $skillPriceMultiplier;

    /**
     * @ORM\Column(type="float")
     */
    protected $luckPriceMultiplier;

    /**
     * @ORM\Column(type="float")
     */
    protected $excellentPriceMultiplier;

    /**
     * @ORM\Column(type="float")
     */
    protected $ancientPriceMultiplier;

    /**
     * @ORM\Column(type="float")
     */
    protected $harmonyPriceMultiplier;

    /**
     * @ORM\Column(type="float")
     */
    protected $pvpPriceMultiplier;

    /**
     * @ORM\Column(type="smallint")
     */
    private $globalCreditsDiscountPercent;

    /**
     * @ORM\Column(type="smallint")
     */
    private $globalMoneyDiscountPercent;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enabled;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\Server", inversedBy="webShopConfig")
     * @ORM\JoinColumn(name="server_id", referencedColumnName="id")
     */
    protected $server;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\WebShopCategory", mappedBy="config", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $categories;

    public function __construct()
    {
        $this->levelPriceAddMoney = 1;
        $this->optionPriceAddMoney = 1;
        $this->levelPriceAddCredits = 1;
        $this->optionPriceAddCredits = 1;
        $this->skillPriceMultiplier = 1;
        $this->luckPriceMultiplier = 1;
        $this->excellentPriceMultiplier = 1;
        $this->ancientPriceMultiplier = 1;
        $this->harmonyPriceMultiplier = 1;
        $this->pvpPriceMultiplier = 1;
        $this->categories = new ArrayCollection();
        $this->enabled = true;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): WebShopConfig
    {
        $this->id = $id;

        return $this;
    }

    public function getMaxItemLevel()
    {
        return $this->maxItemLevel;
    }

    public function setMaxItemLevel($maxItemLevel): WebShopConfig
    {
        $this->maxItemLevel = $maxItemLevel;

        return $this;
    }

    public function getMaxItemOption()
    {
        return $this->maxItemOption;
    }

    public function setMaxItemOption($maxItemOption): WebShopConfig
    {
        $this->maxItemOption = $maxItemOption;

        return $this;
    }

    public function getMaxExcCount()
    {
        return $this->maxExcCount;
    }

    public function setMaxExcCount($maxExcCount): WebShopConfig
    {
        $this->maxExcCount = $maxExcCount;

        return $this;
    }

    /**
     * @return Server
     */
    public function getServer()
    {
        return $this->server;
    }

    public function setServer($server): WebShopConfig
    {
        $this->server = $server;

        return $this;
    }

    public function getCategories()
    {
        $this->categories === null && $this->categories = new ArrayCollection();

        return $this->categories;
    }

    public function rootCategories(): Collection
    {
        return $this->getCategories()->filter(function (WebShopCategory $category): bool {
            return !$category->parentCategory() instanceof WebShopCategory;
        });
    }

    public function setCategories($categories): WebShopConfig
    {
        $this->categories = $categories;

        return $this;
    }

    public function addCategory(WebShopCategory $category): WebShopConfig
    {
        $this->getCategories()->add($category);

        return $this;
    }

    public function removeCategory(WebShopCategory $category): WebShopConfig
    {
        $this->getCategories()->removeElement($category);

        return $this;
    }

    public function getLevelPriceAddMoney(): float
    {
        return $this->levelPriceAddMoney;
    }

    public function setLevelPriceAddMoney(float $levelPriceAddMoney): WebShopConfig
    {
        $this->levelPriceAddMoney = $levelPriceAddMoney;

        return $this;
    }

    public function getOptionPriceAddMoney(): float
    {
        return $this->optionPriceAddMoney;
    }

    public function setOptionPriceAddMoney(float $optionPriceAddMoney): WebShopConfig
    {
        $this->optionPriceAddMoney = $optionPriceAddMoney;

        return $this;
    }

    public function getLevelPriceAddCredits(): float
    {
        return $this->levelPriceAddCredits;
    }

    public function setLevelPriceAddCredits(float $levelPriceAddCredits): WebShopConfig
    {
        $this->levelPriceAddCredits = $levelPriceAddCredits;

        return $this;
    }

    public function getOptionPriceAddCredits(): float
    {
        return $this->optionPriceAddCredits;
    }

    public function setOptionPriceAddCredits(float $optionPriceAddCredits): WebShopConfig
    {
        $this->optionPriceAddCredits = $optionPriceAddCredits;

        return $this;
    }

    public function getSkillPriceMultiplier(): float
    {
        return $this->skillPriceMultiplier;
    }

    public function setSkillPriceMultiplier(float $skillPriceMultiplier): WebShopConfig
    {
        $this->skillPriceMultiplier = $skillPriceMultiplier;

        return $this;
    }

    public function getLuckPriceMultiplier(): float
    {
        return $this->luckPriceMultiplier;
    }

    public function setLuckPriceMultiplier(float $luckPriceMultiplier): WebShopConfig
    {
        $this->luckPriceMultiplier = $luckPriceMultiplier;

        return $this;
    }

    public function getExcellentPriceMultiplier(): float
    {
        return $this->excellentPriceMultiplier;
    }

    public function setExcellentPriceMultiplier(float $excellentPriceMultiplier): WebShopConfig
    {
        $this->excellentPriceMultiplier = $excellentPriceMultiplier;

        return $this;
    }

    public function getAncientPriceMultiplier(): float
    {
        return $this->ancientPriceMultiplier;
    }

    public function setAncientPriceMultiplier(float $ancientPriceMultiplier): WebShopConfig
    {
        $this->ancientPriceMultiplier = $ancientPriceMultiplier;

        return $this;
    }

    public function getHarmonyPriceMultiplier(): float
    {
        return $this->harmonyPriceMultiplier;
    }

    public function setHarmonyPriceMultiplier(float $harmonyPriceMultiplier): WebShopConfig
    {
        $this->harmonyPriceMultiplier = $harmonyPriceMultiplier;

        return $this;
    }

    public function getPvpPriceMultiplier(): float
    {
        return $this->pvpPriceMultiplier;
    }

    public function setPvpPriceMultiplier(float $pvpPriceMultiplier): WebShopConfig
    {
        $this->pvpPriceMultiplier = $pvpPriceMultiplier;

        return $this;
    }

    public function globalCreditsDiscountPercent(): int
    {
        null === $this->globalCreditsDiscountPercent && $this->globalCreditsDiscountPercent = 0;

        return $this->globalCreditsDiscountPercent;
    }

    public function setGlobalCreditsDiscountPercent(int $globalCreditsDiscountPercent)
    {
        $this->globalCreditsDiscountPercent = $globalCreditsDiscountPercent;
    }

    public function globalMoneyDiscountPercent(): int
    {
        null === $this->globalMoneyDiscountPercent && $this->globalMoneyDiscountPercent = 0;

        return $this->globalMoneyDiscountPercent;
    }

    public function setGlobalMoneyDiscountPercent(int $globalMoneyDiscountPercent)
    {
        $this->globalMoneyDiscountPercent = $globalMoneyDiscountPercent;
    }

    public function isEnabled(): bool
    {
        return true === $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }
}