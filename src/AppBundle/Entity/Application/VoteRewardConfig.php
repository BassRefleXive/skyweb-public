<?php


namespace AppBundle\Entity\Application;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="VoteRewardConfig")
 */
class VoteRewardConfig
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     */
    private $rewardMonthTop1;

    /**
     * @ORM\Column(type="smallint")
     */
    private $rewardMonthTop2;

    /**
     * @ORM\Column(type="smallint")
     */
    private $rewardMonthTop3;

    /**
     * @ORM\Column(type="smallint")
     */
    private $rewardMmotopRegularVote;

    /**
     * @ORM\Column(type="smallint")
     */
    private $rewardMmotopSmsVote;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $votesMmotopFileUrl;

    /**
     * @ORM\Column(type="smallint")
     */
    private $rewardQtopRegularVote;

    /**
     * @ORM\Column(type="smallint")
     */
    private $rewardQtopSmsVote;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $votesQtopFileUrl;

    /**
     * @ORM\Column(type="smallint")
     */
    private $rewardXtremeTop100Vote;

    /**
     * @ORM\Column(type="smallint")
     */
    private $rewardGTop100Vote;

    /**
     * @ORM\Column(type="smallint")
     */
    private $rewardTopServers200Vote;

    /**
     * @ORM\Column(type="integer")
     */
    private $topServers200SiteId;

    /**
     * @ORM\Column(type="smallint")
     */
    private $rewardTopGVote;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\Server", inversedBy="voteRewardConfig")
     * @ORM\JoinColumn(name="server_id", referencedColumnName="id")
     */
    private $server;

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getRewardMonthTop1()
    {
        return $this->rewardMonthTop1;
    }

    public function setRewardMonthTop1(int $rewardMonthTop1): self
    {
        $this->rewardMonthTop1 = $rewardMonthTop1;

        return $this;
    }

    public function getRewardMonthTop2()
    {
        return $this->rewardMonthTop2;
    }

    public function setRewardMonthTop2(int $rewardMonthTop2): self
    {
        $this->rewardMonthTop2 = $rewardMonthTop2;

        return $this;
    }

    public function getRewardMonthTop3()
    {
        return $this->rewardMonthTop3;
    }

    public function setRewardMonthTop3(int $rewardMonthTop3): self
    {
        $this->rewardMonthTop3 = $rewardMonthTop3;

        return $this;
    }

    public function getMmotopRewardRegularVote()
    {
        return $this->rewardMmotopRegularVote;
    }

    public function setMmotopRewardRegularVote(int $rewardMmotopRegularVote): self
    {
        $this->rewardMmotopRegularVote = $rewardMmotopRegularVote;

        return $this;
    }

    public function getMmotopRewardSmsVote()
    {
        return $this->rewardMmotopSmsVote;
    }

    public function setMmotopRewardSmsVote(int $rewardMmotopSmsVote): self
    {
        $this->rewardMmotopSmsVote = $rewardMmotopSmsVote;

        return $this;
    }

    public function getMmotopVotesFileUrl()
    {
        return $this->votesMmotopFileUrl;
    }

    public function setMmotopVotesFileUrl(string $votesMmotopFileUrl): self
    {
        $this->votesMmotopFileUrl = $votesMmotopFileUrl;

        return $this;
    }

    public function getQtopRewardRegularVote()
    {
        return $this->rewardQtopRegularVote;
    }

    public function setQtopRewardRegularVote(int $rewardQtopRegularVote): self
    {
        $this->rewardQtopRegularVote = $rewardQtopRegularVote;

        return $this;
    }

    public function getQtopRewardSmsVote()
    {
        return $this->rewardQtopSmsVote;
    }

    public function setQtopRewardSmsVote(int $rewardQtopSmsVote): self
    {
        $this->rewardQtopSmsVote = $rewardQtopSmsVote;

        return $this;
    }

    public function getQtopVotesFileUrl()
    {
        return $this->votesQtopFileUrl;
    }

    public function setQtopVotesFileUrl(string $votesQtopFileUrl): self
    {
        $this->votesQtopFileUrl = $votesQtopFileUrl;

        return $this;
    }

    public function getRewardXtremeTop100Vote()
    {
        return $this->rewardXtremeTop100Vote;
    }

    public function setRewardXtremeTop100Vote(int $rewardXtremeTop100Vote): self
    {
        $this->rewardXtremeTop100Vote = $rewardXtremeTop100Vote;

        return $this;
    }

    public function setRewardGTop100Vote(int $rewardGTop100Vote): self
    {
        $this->rewardGTop100Vote = $rewardGTop100Vote;

        return $this;
    }

    public function getRewardGTop100Vote()
    {
        return $this->rewardGTop100Vote;
    }

    public function setRewardTopServers200Vote(int $rewardTopServers200Vote): self
    {
        $this->rewardTopServers200Vote = $rewardTopServers200Vote;

        return $this;
    }

    public function getRewardTopServers200Vote()
    {
        return $this->rewardTopServers200Vote;
    }

    public function setTopServers200SiteId(int $topServers200SiteId): self
    {
        $this->topServers200SiteId = $topServers200SiteId;

        return $this;
    }

    public function getTopServers200SiteId()
    {
        return $this->topServers200SiteId;
    }

    public function setRewardTopGVote(int $rewardTopGVote): self
    {
        $this->rewardTopGVote = $rewardTopGVote;

        return $this;
    }

    public function getRewardTopGVote()
    {
        return $this->rewardTopGVote;
    }

    public function getServer()
    {
        return $this->server;
    }

    public function setServer(Server $server): self
    {
        $this->server = $server;

        return $this;
    }

}