<?php

declare(strict_types = 1);


namespace AppBundle\Entity\Application\Config;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Application\Config\RegistrationConfigRepository")
 * @ORM\Table(name="RegistrationConfig")
 */
class RegistrationConfig
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $addVipDays;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\Config\VipLevelConfig")
     * @ORM\JoinColumn(name="add_vip_level_id", referencedColumnName="id")
     */
    private $addVipType;


    public function getAddVipDays(): ?int
    {
        return $this->addVipDays;
    }

    public function setAddVipDays(int $addVipDays): self
    {
        $this->addVipDays = $addVipDays;

        return $this;
    }

    public function getAddVipType(): ?VipLevelConfig
    {
        return $this->addVipType;
    }

    public function setAddVipType(VipLevelConfig $addVipType): self
    {
        $this->addVipType = $addVipType;

        return $this;
    }
}