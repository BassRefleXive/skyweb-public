<?php

namespace AppBundle\Entity\Application\Config;

use AppBundle\Entity\Application\Server;
use AppBundle\Entity\MuOnline\Character;
use AppBundle\Enum\Application\Config\CharacterEvolution\ResetForm;
use AppBundle\Enum\Application\Config\CharacterEvolution\ResetPaymentForm;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="CharacterEvolutionConfig")
 */
class CharacterEvolutionConfig
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $resetLevel;

    /**
     * @ORM\Column(type="reset_form")
     */
    private $resetType;

    /**
     * @ORM\Column(type="integer")
     */
    private $resetPointsDK;

    /**
     * @ORM\Column(type="integer")
     */
    private $resetPointsDW;

    /**
     * @ORM\Column(type="integer")
     */
    private $resetPointsFE;

    /**
     * @ORM\Column(type="integer")
     */
    private $resetPointsMG;

    /**
     * @ORM\Column(type="integer")
     */
    private $resetPointsDL;

    /**
     * @ORM\Column(type="integer")
     */
    private $resetPointsSUM;

    /**
     * @ORM\Column(type="integer")
     */
    private $resetPointsRF;

    /**
     * @ORM\Column(type="reset_payment_form")
     */
    private $resetPaymentType;

    /**
     * @ORM\Column(type="integer")
     */
    private $resetPrice;

    /**
     * @ORM\Column(type="integer")
     */
    private $resetReward;

    /**
     * @ORM\Column(type="integer")
     */
    private $resetLimit;

    /**
     * @ORM\Column(type="integer")
     */
    private $grandResetReset;

    /**
     * @ORM\Column(type="integer")
     */
    private $grandResetPrice;

    /**
     * @ORM\Column(type="integer")
     */
    private $grandResetPoints;

    /**
     * @ORM\Column(type="integer")
     */
    private $grandResetReward;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\Server", inversedBy="characterEvolutionConfig")
     * @ORM\JoinColumn(name="server_id", referencedColumnName="id")
     */
    private $server;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getResetLevel(): ?int
    {
        return $this->resetLevel;
    }

    public function setResetLevel(int $resetLevel): self
    {
        $this->resetLevel = $resetLevel;

        return $this;
    }

    public function getResetType(): ?ResetForm
    {
        null === $this->resetType && ($this->resetType = ResetForm::byValue(ResetForm::KEEP_POINTS));

        return $this->resetType;
    }

    public function setResetType(int $resetType): self
    {
        $this->resetType = ResetForm::byValue($resetType);

        return $this;
    }

    public function getResetPointsDK(): ?int
    {
        return $this->resetPointsDK;
    }

    public function setResetPointsDK(int $resetPointsDK): self
    {
        $this->resetPointsDK = $resetPointsDK;

        return $this;
    }

    public function getResetPointsDW(): ?int
    {
        return $this->resetPointsDW;
    }

    public function setResetPointsDW(int $resetPointsDW): self
    {
        $this->resetPointsDW = $resetPointsDW;

        return $this;
    }

    public function getResetPointsFE(): ?int
    {
        return $this->resetPointsFE;
    }

    public function setResetPointsFE(int $resetPointsFE): self
    {
        $this->resetPointsFE = $resetPointsFE;

        return $this;
    }

    public function getResetPointsMG(): ?int
    {
        return $this->resetPointsMG;
    }

    public function setResetPointsMG(int $resetPointsMG): self
    {
        $this->resetPointsMG = $resetPointsMG;

        return $this;
    }

    public function getResetPointsDL(): ?int
    {
        return $this->resetPointsDL;
    }

    public function setResetPointsDL(int $resetPointsDL): self
    {
        $this->resetPointsDL = $resetPointsDL;

        return $this;
    }

    public function getResetPointsSUM(): ?int
    {
        return $this->resetPointsSUM;
    }

    public function setResetPointsSUM(int $resetPointsSUM): self
    {
        $this->resetPointsSUM = $resetPointsSUM;

        return $this;
    }

    public function getResetPointsRF(): ?int
    {
        return $this->resetPointsRF;
    }

    public function setResetPointsRF(int $resetPointsRF): self
    {
        $this->resetPointsRF = $resetPointsRF;

        return $this;
    }

    public function getResetPaymentType(): ResetPaymentForm
    {
        null === $this->resetPaymentType && ($this->resetPaymentType = ResetPaymentForm::byValue(ResetPaymentForm::FREE));

        return $this->resetPaymentType;
    }

    public function setResetPaymentType(int $resetPaymentType): self
    {
        $this->resetPaymentType = ResetPaymentForm::byValue($resetPaymentType);

        return $this;
    }

    public function getResetPrice(): ?int
    {
        return $this->resetPrice;
    }

    public function setResetPrice(int $resetPrice): self
    {
        $this->resetPrice = $resetPrice;

        return $this;
    }

    public function getResetReward(): ?int
    {
        return $this->resetReward;
    }

    public function setResetReward(int $resetReward): self
    {
        $this->resetReward = $resetReward;

        return $this;
    }

    public function getResetLimit(): ?int
    {
        return $this->resetLimit;
    }

    public function setResetLimit(int $resetLimit): self
    {
        $this->resetLimit = $resetLimit;

        return $this;
    }

    public function getGrandResetReset(): ?int
    {
        return $this->grandResetReset;
    }

    public function setGrandResetReset(int $grandResetReset): self
    {
        $this->grandResetReset = $grandResetReset;

        return $this;
    }

    public function getGrandResetPrice(): ?int
    {
        return $this->grandResetPrice;
    }

    public function setGrandResetPrice(int $grandResetPrice): self
    {
        $this->grandResetPrice = $grandResetPrice;

        return $this;
    }

    public function getGrandResetPoints(): ?int
    {
        return $this->grandResetPoints;
    }

    public function setGrandResetPoints(int $grandResetPoints): self
    {
        $this->grandResetPoints = $grandResetPoints;

        return $this;
    }

    public function getGrandResetReward(): ?int
    {
        return $this->grandResetReward;
    }

    public function setGrandResetReward(int $grandResetReward): self
    {
        $this->grandResetReward = $grandResetReward;

        return $this;
    }

    public function getServer(): ?Server
    {
        return $this->server;
    }

    public function setServer(Server $server): self
    {
        $this->server = $server;

        return $this;
    }

    public function resetLevel(Character $character): int
    {
        switch ($character->getReset()) {
            case 0: {
                return $this->resetLevel - 25;
            }
            case 1: {
                return $this->resetLevel - 20;
            }
            case 2: {
                return $this->resetLevel - 15;
            }
            case 3: {
                return $this->resetLevel - 10;
            }
            case 4: {
                return $this->resetLevel - 5;
            }
        }

        return $this->resetLevel;
    }
}