<?php


namespace AppBundle\Entity\Application\Config;

use AppBundle\Entity\Application\Server;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="VipLevelConfig")
 */
class VipLevelConfig
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private $title;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $resetPriceDiscount;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $resetPointsBonus;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $grandResetPriceDiscount;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $grandResetPointsBonus;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $nightStartHour;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $nightStartMinute;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $dayStartHour;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $dayStartMinute;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $nightAddExp;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $nightAddMasterExp;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $nightAddDrop;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $nightAddExcDrop;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $dayAddExp;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $dayAddMasterExp;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $dayAddDrop;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $dayAddExcDrop;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $offAfkMaxHours;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $dailyCoinsPrice;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $renewalDiscount;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $enabled;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\Server", inversedBy="vipLevels")
     * @ORM\JoinColumn(name="server_id", referencedColumnName="id")
     */
    private $server;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getResetPriceDiscount(): ?int
    {
        return $this->resetPriceDiscount;
    }

    public function setResetPriceDiscount(int $resetPriceDiscount): self
    {
        $this->resetPriceDiscount = $resetPriceDiscount;

        return $this;
    }

    public function getResetPointsBonus(): ?int
    {
        return $this->resetPointsBonus;
    }

    public function setResetPointsBonus(int $resetPointsBonus): self
    {
        $this->resetPointsBonus = $resetPointsBonus;

        return $this;
    }

    public function getGrandResetPriceDiscount(): ?int
    {
        return $this->grandResetPriceDiscount;
    }

    public function setGrandResetPriceDiscount(int $grandResetPriceDiscount): self
    {
        $this->grandResetPriceDiscount = $grandResetPriceDiscount;

        return $this;
    }

    public function getGrandResetPointsBonus(): ?int
    {
        return $this->grandResetPointsBonus;
    }

    public function setGrandResetPointsBonus(int $grandResetPointsBonus): self
    {
        $this->grandResetPointsBonus = $grandResetPointsBonus;

        return $this;
    }

    public function getNightStartHour(): ?int
    {
        return $this->nightStartHour;
    }

    public function setNightStartHour(int $nightStartHour): self
    {
        $this->nightStartHour = $nightStartHour;

        return $this;
    }

    public function getNightStartMinute(): ?int
    {
        return $this->nightStartMinute;
    }

    public function setNightStartMinute(int $nightStartMinute): self
    {
        $this->nightStartMinute = $nightStartMinute;

        return $this;
    }

    public function getDayStartHour(): ?int
    {
        return $this->dayStartHour;
    }

    public function setDayStartHour(int $dayStartHour): self
    {
        $this->dayStartHour = $dayStartHour;

        return $this;
    }

    public function getDayStartMinute(): ?int
    {
        return $this->dayStartMinute;
    }

    public function setDayStartMinute(int $dayStartMinute): self
    {
        $this->dayStartMinute = $dayStartMinute;

        return $this;
    }

    public function getNightAddExp(): ?int
    {
        return $this->nightAddExp;
    }

    public function setNightAddExp(int $nightAddExp): self
    {
        $this->nightAddExp = $nightAddExp;

        return $this;
    }

    public function getNightAddMasterExp(): ?int
    {
        return $this->nightAddMasterExp;
    }

    public function setNightAddMasterExp(int $nightAddMasterExp): self
    {
        $this->nightAddMasterExp = $nightAddMasterExp;

        return $this;
    }

    public function getNightAddDrop(): ?int
    {
        return $this->nightAddDrop;
    }

    public function setNightAddDrop(int $nightAddDrop): self
    {
        $this->nightAddDrop = $nightAddDrop;

        return $this;
    }

    public function getNightAddExcDrop(): ?int
    {
        return $this->nightAddExcDrop;
    }

    public function setNightAddExcDrop(int $nightAddExcDrop): self
    {
        $this->nightAddExcDrop = $nightAddExcDrop;

        return $this;
    }

    public function getDayAddExp(): ?int
    {
        return $this->dayAddExp;
    }

    public function setDayAddExp(int $dayAddExp): self
    {
        $this->dayAddExp = $dayAddExp;

        return $this;
    }

    public function getDayAddMasterExp(): ?int
    {
        return $this->dayAddMasterExp;
    }

    public function setDayAddMasterExp(int $dayAddMasterExp): self
    {
        $this->dayAddMasterExp = $dayAddMasterExp;

        return $this;
    }

    public function getDayAddDrop(): ?int
    {
        return $this->dayAddDrop;
    }

    public function setDayAddDrop(int $dayAddDrop): self
    {
        $this->dayAddDrop = $dayAddDrop;

        return $this;
    }

    public function getDayAddExcDrop(): ?int
    {
        return $this->dayAddExcDrop;
    }

    public function setDayAddExcDrop(int $dayAddExcDrop): self
    {
        $this->dayAddExcDrop = $dayAddExcDrop;

        return $this;
    }

    public function getOffAfkMaxHours(): ?int
    {
        return $this->offAfkMaxHours;
    }

    public function setOffAfkMaxHours(int $offAfkMaxHours): self
    {
        $this->offAfkMaxHours = $offAfkMaxHours;

        return $this;
    }

    public function getDailyCoinsPrice(): ?int
    {
        return $this->dailyCoinsPrice;
    }

    public function setDailyCoinsPrice(int $dailyCoinsPrice): self
    {
        $this->dailyCoinsPrice = $dailyCoinsPrice;

        return $this;
    }

    public function getRenewalDiscount(): ?int
    {
        return $this->renewalDiscount;
    }

    public function setRenewalDiscount(int $renewalDiscount): self
    {
        $this->renewalDiscount = $renewalDiscount;

        return $this;
    }

    public function isEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getServer(): ?Server
    {
        return $this->server;
    }

    public function setServer(Server $server): self
    {
        $this->server = $server;

        return $this;
    }
}