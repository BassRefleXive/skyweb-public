<?php


namespace AppBundle\Entity\Application\Config;


use AppBundle\Entity\Application\Server;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="WCoinShopShopConfig")
 */
class WCoinShopConfig
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $coinsPerUsd;

    /**
     * @ORM\Column(type="float")
     */
    private $minAmount;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enabled;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\Server", inversedBy="wCoinShopConfig")
     * @ORM\JoinColumn(name="server_id", referencedColumnName="id")
     */
    private $server;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getCoinsPerUsd(): ?int
    {
        return $this->coinsPerUsd;
    }

    public function setCoinsPerUsd(int $coinsPerUsd): self
    {
        $this->coinsPerUsd = $coinsPerUsd;

        return $this;
    }

    public function getMinAmount(): ?float
    {
        return $this->minAmount;
    }

    public function setMinAmount(float $minAmount): self
    {
        $this->minAmount = $minAmount;

        return $this;
    }

    public function isEnabled(): bool
    {
        return true === $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getServer(): ?Server
    {
        return $this->server;
    }

    public function setServer(Server $server): self
    {
        $this->server = $server;

        return $this;
    }
}