<?php

declare(strict_types=1);

namespace AppBundle\Entity\Application\Config;


use AppBundle\Entity\Application\Server;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="CastleSiegeCycle")
 */
class CastleSiegeCycle
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $stage;

    /**
     * @ORM\Column(type="smallint")
     */
    private $day;

    /**
     * @ORM\Column(type="smallint")
     */
    private $hour;

    /**
     * @ORM\Column(type="smallint")
     */
    private $minute;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\Server", inversedBy="castleSiegeCycles")
     * @ORM\JoinColumn(name="server_id", referencedColumnName="id")
     */
    private $server;

    public function getStage(): ?int
    {
        return $this->stage;
    }

    public function setStage(int $stage): void
    {
        $this->stage = $stage;
    }

    public function getDay(): ?int
    {
        return $this->day;
    }

    public function setDay(int $day): void
    {
        $this->day = $day;
    }

    public function getHour(): ?int
    {
        return $this->hour;
    }

    public function setHour(int $hour): void
    {
        $this->hour = $hour;
    }

    public function getMinute(): ?int
    {
        return $this->minute;
    }

    public function setMinute(int $minute): void
    {
        $this->minute = $minute;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getServer(): ?Server
    {
        return $this->server;
    }

    public function setServer(Server $server): self
    {
        $this->server = $server;

        return $this;
    }
}