<?php

namespace AppBundle\Entity\Application;

use Collection\Sequence;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="Cart")
 */
class Cart
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\UserServerAccount", inversedBy="cart")
     * @ORM\JoinColumn(name="user_server_account_id", referencedColumnName="id")
     */
    protected $userServerAccount;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\ItemToBuy", mappedBy="cart", cascade={"persist"}, orphanRemoval=true)
     */
    protected $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): Cart
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return UserServerAccount
     */
    public function getUserServerAccount(): UserServerAccount
    {
        return $this->userServerAccount;
    }

    public function setUserServerAccount(UserServerAccount $userServerAccount): Cart
    {
        $this->userServerAccount = $userServerAccount;

        return $this;
    }

    public function getItems(): Collection
    {
        $this->items === null && $this->items = new ArrayCollection();

        return $this->items;
    }

    public function addItem(ItemToBuy $item): Cart
    {
        $item->setCart($this);
        $this->getItems()->add($item);

        return $this;
    }

    public function removeItem(ItemToBuy $item): Cart
    {
        $items = $this->getItems();

        if (($index = $items->indexOf($item)) !== false) {
            $items->remove($index);
        }

        return $this;
    }

    public function removeItems(): Cart
    {
        foreach ($this->getItems() as $item) {
            $this->removeItem($item);
        }

        return $this;
    }

    public function calculateRequiredCreditsCount(): int
    {
        return (new Sequence($this->getItems()->toArray()))
            ->foldLeft(0, function ($memo, ItemToBuy $item) {
                return $memo + $item->getPriceCredits();
            });
    }

    public function calculateRequiredMoney(): float
    {
        return (new Sequence($this->getItems()->toArray()))
            ->foldLeft(0, function ($memo, ItemToBuy $item) {
                return $memo + $item->getPriceMoney();
            });
    }
}