<?php

namespace AppBundle\Entity\Application;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="WebStorageItem")
 */
class WebStorageItem
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $item;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\WebStorage", inversedBy="items")
     * @ORM\JoinColumn(name="web_storage_id", referencedColumnName="id")
     */
    protected $webStorage;

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): WebStorageItem
    {
        $this->id = $id;

        return $this;
    }

    public function getItem()
    {
        return $this->item;
    }

    public function setItem(string $item): WebStorageItem
    {
        $this->item = $item;

        return $this;
    }

    public function getWebStorage(): WebStorage
    {
        return $this->webStorage;
    }

    public function setWebStorage(WebStorage $webStorage): WebStorageItem
    {
        $this->webStorage = $webStorage;

        return $this;
    }

}