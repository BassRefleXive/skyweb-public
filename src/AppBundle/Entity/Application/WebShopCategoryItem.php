<?php

namespace AppBundle\Entity\Application;


use AppBundle\Entity\Application\Items\ItemInfo;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity()
 * @ORM\Table(name="WebShopCategoryItem")
 */
class WebShopCategoryItem
{
    const ALLOWED_LEVEL = 1;
    const ALLOWED_OPTION = 2;
    const ALLOWED_SKILL = 4;
    const ALLOWED_LUCK = 8;
    const ALLOWED_EXCELLENT = 16;
    const ALLOWED_ANCIENT = 32;
    const ALLOWED_HARMONY = 64;
    const ALLOWED_PVP = 128;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $allowedOptions;

    /**
     * @ORM\Column(type="integer")
     */
    private $priceCredits;

    /**
     * @ORM\Column(type="decimal", precision=2, scale=2)
     */
    private $priceMoney;

    /**
     * @ORM\Column(type="smallint")
     */
    private $position;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\WebShopCategory", inversedBy="items")
     * @ORM\JoinColumn(name="webshop_category_id", referencedColumnName="id")
     */
    private $webShopCategory;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\Items\ItemInfo", inversedBy="webShopCategoryItems")
     * @ORM\JoinColumn(name="item_info_id", referencedColumnName="id")
     */
    private $item;

    public function __construct()
    {
        $this->allowedOptions = 0;
        $this->position = 0;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): WebShopCategoryItem
    {
        $this->id = $id;

        return $this;
    }

    public function getPriceCredits()
    {
        return $this->priceCredits;
    }

    public function setPriceCredits(int $priceCredits): WebShopCategoryItem
    {
        $this->priceCredits = $priceCredits;

        return $this;
    }

    public function getPriceMoney()
    {
        return $this->priceMoney;
    }

    public function setPriceMoney(float $priceMoney): WebShopCategoryItem
    {
        $this->priceMoney = $priceMoney;

        return $this;
    }

    /**
     * @return WebShopCategory
     */
    public function getWebShopCategory()
    {
        return $this->webShopCategory;
    }

    public function setWebShopCategory(WebShopCategory $webShopCategory): WebShopCategoryItem
    {
        $this->webShopCategory = $webShopCategory;

        return $this;
    }

    /**
     * @return ItemInfo
     */
    public function getItem()
    {
        return $this->item;
    }

    public function setItem(ItemInfo $item): WebShopCategoryItem
    {
        $this->item = $item;

        return $this;
    }

    public function addAllowedOption(int $option): WebShopCategoryItem
    {
        $this->allowedOptions = ($this->allowedOptions | $option);

        return $this;
    }

    public function setAllowedOptions(int $options): WebShopCategoryItem
    {
        $this->allowedOptions = $options;

        return $this;
    }

    public function removeAllowedOption(int $option): WebShopCategoryItem
    {
        $this->allowedOptions = ($this->allowedOptions & ~$option);

        return $this;
    }

    public function checkAllowedOption(int $bit): bool
    {
        return (bool)((int)$this->allowedOptions & (int)$bit);
    }

    public function isAddLevelAllowed(): bool
    {
        return $this->checkAllowedOption(WebShopCategoryItem::ALLOWED_LEVEL);
    }

    public function isAddOptionAllowed(): bool
    {
        return $this->checkAllowedOption(WebShopCategoryItem::ALLOWED_OPTION);
    }

    public function isAddSkillAllowed(): bool
    {
        return $this->checkAllowedOption(WebShopCategoryItem::ALLOWED_SKILL);
    }

    public function isAddLuckAllowed(): bool
    {
        return $this->checkAllowedOption(WebShopCategoryItem::ALLOWED_LUCK);
    }

    public function isAddExcellentAllowed(): bool
    {
        return $this->checkAllowedOption(WebShopCategoryItem::ALLOWED_EXCELLENT);
    }

    public function isAddAncientAllowed(): bool
    {
        return $this->checkAllowedOption(WebShopCategoryItem::ALLOWED_ANCIENT);
    }

    public function isAddHarmonyAllowed(): bool
    {
        return $this->checkAllowedOption(WebShopCategoryItem::ALLOWED_HARMONY);
    }

    public function isAddPvPAllowed(): bool
    {
        return $this->checkAllowedOption(WebShopCategoryItem::ALLOWED_PVP);
    }

    public function getPossibleOptions(): array
    {
        return [
            WebShopCategoryItem::ALLOWED_LEVEL,
            WebShopCategoryItem::ALLOWED_OPTION,
            WebShopCategoryItem::ALLOWED_SKILL,
            WebShopCategoryItem::ALLOWED_LUCK,
            WebShopCategoryItem::ALLOWED_EXCELLENT,
            WebShopCategoryItem::ALLOWED_ANCIENT,
            WebShopCategoryItem::ALLOWED_HARMONY,
            WebShopCategoryItem::ALLOWED_PVP,
        ];
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function position(): int
    {
        return $this->position;
    }
}