<?php

namespace AppBundle\Entity\Application;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="AccountMenuConfig")
 */
class AccountMenuConfig
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $nickChangePrice;

    /**
     * @ORM\Column(type="integer")
     */
    private $classChangePrice;

    /**
     * @ORM\Column(type="integer")
     */
    private $resetStatsPrice;

    /**
     * @ORM\Column(type="integer")
     */
    private $hideInfoDailyRate;

    /**
     * @ORM\Column(type="integer")
     */
    private $onlineHoursRate;

    /**
     * @ORM\Column(type="integer")
     */
    private $resetMasterSkillTreePrice;

    /**
     * @ORM\Column(type="integer")
     */
    private $registrationAddWCoin;

    /**
     * @ORM\Column(type="integer")
     */
    private $registrationAddZen;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Application\Server", inversedBy="accountMenuConfig")
     * @ORM\JoinColumn(name="server_id", referencedColumnName="id")
     */
    protected $server;

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getNickChangePrice()
    {
        return $this->nickChangePrice;
    }

    public function setNickChangePrice(int $nickChangePrice): self
    {
        $this->nickChangePrice = $nickChangePrice;

        return $this;
    }

    public function getClassChangePrice()
    {
        return $this->classChangePrice;
    }

    public function setClassChangePrice(int $classChangePrice): self
    {
        $this->classChangePrice = $classChangePrice;

        return $this;
    }

    public function getResetStatsPrice()
    {
        return $this->resetStatsPrice;
    }

    public function setResetStatsPrice(int $resetStatsPrice): self
    {
        $this->resetStatsPrice = $resetStatsPrice;

        return $this;
    }

    public function getOnlineHoursRate()
    {
        return $this->onlineHoursRate;
    }

    public function setOnlineHoursRate(int $onlineHoursRate): self
    {
        $this->onlineHoursRate = $onlineHoursRate;

        return $this;
    }

    public function hideInfoDailyRate()
    {
        return $this->hideInfoDailyRate;
    }

    public function setHideInfoDailyRate(int $hideInfoDailyRate): self
    {
        $this->hideInfoDailyRate = $hideInfoDailyRate;

        return $this;
    }

    public function getResetMasterSkillTreePrice(): ?int
    {
        return $this->resetMasterSkillTreePrice;
    }

    public function setResetMasterSkillTreePrice(int $resetMasterSkillTreePrice): self
    {
        $this->resetMasterSkillTreePrice = $resetMasterSkillTreePrice;

        return $this;
    }

    public function getRegistrationAddWCoin(): ?int
    {
        return $this->registrationAddWCoin;
    }

    public function setRegistrationAddWCoin(int $registrationAddWCoin): self
    {
        $this->registrationAddWCoin = $registrationAddWCoin;

        return $this;
    }

    public function getRegistrationAddZen(): ?int
    {
        return $this->registrationAddZen;
    }

    public function setRegistrationAddZen(int $registrationAddZen): self
    {
        $this->registrationAddZen = $registrationAddZen;

        return $this;
    }

    public function getServer()
    {
        return $this->server;
    }

    public function setServer(Server $server): self
    {
        $this->server = $server;

        return $this;
    }
}