<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class CartPrice extends Constraint
{
    const PRICE_CREDITS = 'credits';
    const PRICE_MONEY = 'money';

    public $message = 'Price is invalid.';
    public $type;
    public $base_price;
    public $items;

    public function validatedBy()
    {
        return get_class($this) . 'Validator';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequiredOptions()
    {
        return ['type', 'base_price', 'items'];
    }

}