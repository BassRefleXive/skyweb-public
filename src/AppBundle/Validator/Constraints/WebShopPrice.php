<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;


class WebShopPrice extends Constraint
{
    const PRICE_CREDITS = 'credits';
    const PRICE_MONEY = 'money';

    public $message = 'Price is invalid.';
    public $config;
    public $type;
    public $base_price;

    public function validatedBy()
    {
        return get_class($this) . 'Validator';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequiredOptions()
    {
        return ['config', 'type', 'base_price'];
    }

}