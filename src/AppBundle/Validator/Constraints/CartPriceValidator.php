<?php

namespace AppBundle\Validator\Constraints;


use AppBundle\Doctrine\Type\CartDiscountCouponTypeEnum;
use AppBundle\Doctrine\Type\CartPersistentDiscountTypeEnum;
use AppBundle\Doctrine\Type\ItemToBuyTypeEnum;
use AppBundle\Entity\Application\ItemToBuy;
use AppBundle\Entity\Application\User\User;
use AppBundle\Entity\Application\CartDiscountCoupon;
use AppBundle\Entity\Application\CartPersistentDiscount;
use AppBundle\Repository\Application\CartDiscountCouponRepository;
use AppBundle\Repository\Application\CartPersistentDiscountRepository;
use AppBundle\Service\Application\ServerService;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Form\Form;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\ValidatorException;

class CartPriceValidator extends ConstraintValidator
{

    protected $serverService;
    protected $tokenStorage;
    protected $persistentDiscountRepository;

    public function __construct(
        ServerService $serverService,
        TokenStorageInterface $tokenStorage,
        CartPersistentDiscountRepository $persistentDiscountRepository
    )
    {
        $this->serverService = $serverService;
        $this->tokenStorage = $tokenStorage;
        $this->persistentDiscountRepository = $persistentDiscountRepository;
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof CartPrice) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\Type');
        }

        if (null === $value) {
            return;
        }

        /** @var ExecutionContextInterface $context */
        $context = $this->context;

        if (!$context instanceof ExecutionContextInterface) {
            throw new ValidatorException('Context expected to be ExecutionContextInterface. Future validation impossible.');
        }

        $basePrice = $this->getBasePriceWithPersistentDiscount($constraint);

        if ($constraint->type === CartPrice::PRICE_CREDITS) {
            if (!(is_numeric($value) ? intval($value) == $value : false)) {
                $context->buildViolation('Credits price must be integer.')->addViolation();
            }
        }

        /** @var Form $form */
        $form = $context->getRoot();

        /** @var CartDiscountCoupon $coupon */
        $coupon = $form->get('coupon')->getData();

        if ($coupon === null) {
            if (abs(($basePrice - $value) / $value) > 0.00001) {
                $context->buildViolation($constraint->message)->addViolation();
            }
        } else {
            if (
                $constraint->type === CartPrice::PRICE_CREDITS &&
                $basePrice !== (float) $value &&
                $coupon->getType() === CartDiscountCouponTypeEnum::DISCOUNT_COUPON_TYPE_MONEY
            ) {
                $context->buildViolation('This discount cannot be assigned to credits.')->addViolation();
            }

            if (
                $constraint->type === CartPrice::PRICE_MONEY &&
                $basePrice !== (float) $value &&
                $coupon->getType() === CartDiscountCouponTypeEnum::DISCOUNT_COUPON_TYPE_CREDITS
            ) {
                $context->buildViolation('This discount cannot be assigned to money.')->addViolation();
            }

            if (
                $basePrice !== (float) $value &&
                (
                    ($constraint->type === CartPrice::PRICE_MONEY && $coupon->getType() === CartDiscountCouponTypeEnum::DISCOUNT_COUPON_TYPE_MONEY) ||
                    ($constraint->type === CartPrice::PRICE_CREDITS && $coupon->getType() === CartDiscountCouponTypeEnum::DISCOUNT_COUPON_TYPE_CREDITS)
                )
            ) {
                $priceWithCouponDiscount = $this->getBasePriceWithPersistentDiscount($constraint, $coupon->getDiscount());


                if (abs(($priceWithCouponDiscount - $value)/$value) > 0.00001) {
                    $context->buildViolation($constraint->message)->addViolation();
                }
            }

        }

    }

    private function getDiscountedPrice(float $price, int $discount): float
    {
        return ceil($price * (100 - $discount)) / 100;
    }

    private function getBasePriceWithPersistentDiscount(Constraint $constraint, int $couponDiscount = 0): float
    {
        $items = $this->cartItems($constraint);

        $basePricePersistentDiscount = $this->getBasePricePersistentDiscount($constraint);
        $basePrice = 0;

        foreach ($items as $item) {
            $basePrice += 0 !== $basePricePersistentDiscount && $item->getType() !== ItemToBuyTypeEnum::TYPE_WCOIN
                ? $this->getDiscountedPrice($this->getItemPrice($item, $constraint), $basePricePersistentDiscount)
                : $this->getItemPrice($item, $constraint);
        }

        if ($couponDiscount) {
            $basePrice = $this->getDiscountedPrice($basePrice, $couponDiscount);
        }

        if ($constraint->type === CartPrice::PRICE_CREDITS) {
            $basePrice = ceil($basePrice);
        }

        return $basePrice;
    }

    private function getItemPrice(ItemToBuy $item, Constraint $constraint): float
    {
        switch ($constraint->type) {
            case CartPrice::PRICE_CREDITS: {
                return $item->getPriceCredits();
            }
            case CartPrice::PRICE_MONEY: {
                return $item->getPriceMoney();
            }
            default: {
                throw new ValidatorException('Unknown type. Cannot determine CartPersistentDiscountType');
            }
        }
    }

    private function getWebShopPersistentDiscountType(Constraint $constraint): string
    {
        switch ($constraint->type) {
            case CartPrice::PRICE_CREDITS: {
                return CartPersistentDiscountTypeEnum::PERSISTENT_DISCOUNT_TYPE_CREDITS;
            }
            case CartPrice::PRICE_MONEY: {
                return CartPersistentDiscountTypeEnum::PERSISTENT_DISCOUNT_TYPE_MONEY;
            }
            default: {
                throw new ValidatorException('Unknown type. Cannot determine CartPersistentDiscountType');
            }
        }
    }

    private function getBasePricePersistentDiscount(Constraint $constraint): int
    {
        $server = $this->serverService->getCurrentServer();
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();
        $discountType = $this->getWebShopPersistentDiscountType($constraint);

        /** @var CartPersistentDiscount $persistentDiscount */
        $persistentDiscount = $this->persistentDiscountRepository->findInRange(
            $server,
            $user->getUserServerAccountByServer($server)->getSpentAmount($discountType),
            $discountType
        );

        return $persistentDiscount
            ? $persistentDiscount->getDiscount()
            : 0;
    }

    /**
     * @param Constraint $constraint
     * @return ItemToBuy[]
     */
    private function cartItems(Constraint $constraint): array
    {
        return $constraint->items->toArray();
    }
}