<?php


namespace AppBundle\Validator\Constraints;

use AppBundle\Entity\Application\WebShopConfig;
use Symfony\Component\Form\Form;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\ValidatorException;


class WebShopPriceValidator extends ConstraintValidator
{

    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof WebShopPrice) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\Type');
        }

        if (null === $value) {
            return;
        }

        if (!in_array($constraint->type, [WebShopPrice::PRICE_MONEY, WebShopPrice::PRICE_CREDITS], true)) {
            throw new ValidatorException('Unknown WebShop price validation type.');
        }

        if (!$constraint->config instanceof WebShopConfig) {
            throw new ValidatorException('Config must be instance of WebShopConfig.');
        }

        if (!is_float($constraint->base_price)) {
            throw new ValidatorException('base_price must be float.');
        }

        /** @var ExecutionContextInterface $context */
        $context = $this->context;

        if (!$context instanceof ExecutionContextInterface) {
            throw new ValidatorException('Context expected to be ExecutionContextInterface. Future validation impossible.');
        }

        /** @var Form $form */
        $form = $context->getRoot();

        $expectedPrice = $this->getExpectedPrice($form, $constraint);

        if ($constraint->type === 'credits') {
            $expectedPrice = (float) ceil($expectedPrice);
        }

        if ($expectedPrice !== (float) $value) {
            $context->buildViolation($constraint->message)
                ->addViolation();
        }
    }

    private function getExpectedPrice(Form $form, Constraint $constraint): float
    {
        $currentValues = $this->getPriceAffectingValues($form);
        $config = $this->getPriceValidationConfig($constraint->config);

        $result = $constraint->base_price;

        foreach ($currentValues as $key => $value) {
            if (isset($config['mult'][$key]) && $value > 0) {
                $result += ceil(($value * $config['mult'][$key] * $constraint->base_price) * 100) / 100;
            } elseif (isset($config['add'][$key]) && $value > 0) {
                $result += ceil(($value * $config['add'][$key][$constraint->type]) * 100) / 100;
            }
        }

        return ceil($this->withAppliedGlobalDiscount($result, $constraint->type, $constraint->config) * 100) / 100;
    }

    private function withAppliedGlobalDiscount(float $price, string $type, WebShopConfig $config): float
    {
        $discount = [
                'money'   => $config->globalMoneyDiscountPercent(),
                'credits' => $config->globalCreditsDiscountPercent(),
            ][$type] ?? 0;

        return ceil($price * (100 - $discount)) / 100;
    }

    private function getPriceAffectingValues(Form $form): array
    {
        $data = $form->getData();

        return [
            'level'     => isset($data['level']) && $data['level'] ? $data['level'] : 0,
            'option'    => isset($data['option']) && $data['option'] ? ($data['option'] / 4) : 0,
            'skill'     => isset($data['skill']) && $data['skill'] ? 1 : 0,
            'luck'      => isset($data['luck']) && $data['luck'] ? 1 : 0,
            'excellent' => isset($data['excellent']) && $data['excellent'] ? count($data['excellent']) : 0,
            'ancient'   => isset($data['ancient']) && $data['ancient'] ? 1 : 0,
            'harmony'   => isset($data['harmony']) && $data['harmony'] ? 1 : 0,
            'pvp'       => isset($data['pvp']) && $data['pvp'] ? 1 : 0,
        ];
    }

    private function getPriceValidationConfig(WebShopConfig $config): array
    {
        return [
            'mult' => [
                'skill'     => $config->getSkillPriceMultiplier(),
                'luck'      => $config->getLuckPriceMultiplier(),
                'excellent' => $config->getExcellentPriceMultiplier(),
                'ancient'   => $config->getAncientPriceMultiplier(),
                'harmony'   => $config->getHarmonyPriceMultiplier(),
                'pvp'       => $config->getPvpPriceMultiplier(),
            ],
            'add'  => [
                'level'  => [
                    'money'   => $config->getLevelPriceAddMoney(),
                    'credits' => $config->getLevelPriceAddCredits(),
                ],
                'option' => [
                    'money'   => $config->getOptionPriceAddMoney(),
                    'credits' => $config->getOptionPriceAddCredits(),
                ],
            ],
        ];
    }
}