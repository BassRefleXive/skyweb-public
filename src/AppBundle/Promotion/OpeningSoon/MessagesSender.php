<?php

declare(strict_types = 1);

namespace AppBundle\Promotion\OpeningSoon;


use AppBundle\Entity\Application\User\User;
use AppBundle\Mailer\Factory\MailerFactory;
use AppBundle\Repository\Application\UserRepository;
use Symfony\Component\Translation\TranslatorInterface;

class MessagesSender
{
    private $mailerFactory;
    private $userRepository;
    private $translator;

    public function __construct(MailerFactory $mailerFactory, UserRepository $userRepository, TranslatorInterface $translator)
    {
        $this->mailerFactory = $mailerFactory;
        $this->userRepository = $userRepository;
        $this->translator = $translator;
    }

    public function send()
    {
        $users = json_decode(file_get_contents('app/data/promotion/user_list.json'), true);

        $users = [
            ['email' => 'mihails.bagrovs@gmail.com'],
        ];

        $this->sendPreviousProject($users);
//        $this->sendRegistered();
    }

    private function sendPreviousProject(array $users)
    {
        foreach ($users as $user) {
            $this->translator->setLocale('ru');

            try {
                $this->sendMail($user['email']);
            } catch (\Throwable $e) {
                echo sprintf(
                    'Failed to send message to recipient "%s". Error: "%s".',
                    $user['email'],
                    $e->getMessage()
                );

                continue;
            }
        }
    }

    private function sendRegistered()
    {
        /** @var User[] $users */
        $users = $this->userRepository->findAll();

        foreach ($users as $user) {
            if (!$user->emailState()->isActive()) {
                continue;
            }

            $this->translator->setLocale($user->getLocale());

            try {
                $this->sendMail($user->email());
            } catch (\Throwable $e) {
                echo sprintf(
                    'Failed to send message to recipient "%s". Error: "%s".',
                    $user->email(),
                    $e->getMessage()
                );

                continue;
            }
        }
    }

    private function sendMail(string $email): void
    {
        $this->mailerFactory
            ->createDynamicMailerFactory(
                $email,
                'email.opening_soon.0',
                '@App/email/other/opening_soon.html.twig'
            )
            ->send();
    }
}