<?php

declare(strict_types = 1);

namespace AppBundle\Promotion\OBTInvitation;


use AppBundle\BonusCode\Dto\BonusCodeGenerationCriteriaDto;
use AppBundle\BonusCode\Model\BonusCode;
use AppBundle\BonusCode\Service\Generator;
use AppBundle\Entity\Application\User\User;
use AppBundle\Entity\Application\UserServerAccount;
use AppBundle\Repository\Application\ServerRepository;

class MessageCreator
{
    private $generator;
    private $serverRepository;

    public function __construct(Generator $generator, ServerRepository $serverRepository)
    {
        $this->generator = $generator;
        $this->serverRepository = $serverRepository;
    }

    public function create(): BonusCode
    {
        $bonusCodeCriteria = new BonusCodeGenerationCriteriaDto();
        $bonusCodeCriteria->server = $this->serverRepository->find(1);
        $bonusCodeCriteria->amount = 30;
        $bonusCodeCriteria->count = 1;
        $bonusCodeCriteria->expireAt = (new \DateTime('+1 year'));

        return $this->generator->createBonusCodes($bonusCodeCriteria)[0];
    }
}