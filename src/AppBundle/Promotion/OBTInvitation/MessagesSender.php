<?php

declare(strict_types = 1);

namespace AppBundle\Promotion\OBTInvitation;


use AppBundle\Mailer\Factory\MailerFactory;

class MessagesSender
{
    private $creator;
    private $mailerFactory;

    public function __construct(MessageCreator $creator, MailerFactory $mailerFactory)
    {
        $this->creator = $creator;
        $this->mailerFactory = $mailerFactory;
    }

    public function send()
    {
        $users = json_decode(file_get_contents('app/data/promotion/user_list.json'), true);

        foreach ($users as $user) {
            try {
                $bonus = $this->creator->create();

                $this->mailerFactory
                    ->createDynamicMailerFactory(
                        $user['email'],
                        'email.obt_invitation.20',
                        '@App/email/other/obt_invitation.html.twig',
                        [
                            'bonus' => $bonus
                        ]
                    )
                    ->send();
            } catch (\Throwable $e) {
                echo sprintf(
                    'Failed to send message to recipient "%s". Error: "%s".',
                    $user['email'],
                    $e->getMessage()
                );

                continue;
            }
        }
    }
}