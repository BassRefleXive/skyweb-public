<?php

declare(strict_types=1);

namespace AppBundle\Promotion\ReturnLeavedPlayers;


use AppBundle\Entity\Application\Config\VipLevelConfig;
use AppBundle\Entity\Application\User\User;
use AppBundle\Entity\MuOnline\Account;
use AppBundle\Entity\MuOnline\Character;
use AppBundle\Mailer\Factory\MailerFactory;
use AppBundle\Repository\Application\UserRepository;
use AppBundle\Service\Application\VipService;
use AppBundle\Service\MuOnline\AccountService;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Translation\TranslatorInterface;

class SenderService
{
    private $userRepository;
    private $accountService;
    private $mailerFactory;
    private $translator;
    private $vipService;
    private $em;
    private $logger;

    public function __construct(
        UserRepository $userRepository,
        AccountService $accountService,
        MailerFactory $mailerFactory,
        TranslatorInterface $translator,
        VipService $vipService,
        EntityManager $em,
        LoggerInterface $logger
    )
    {
        $this->userRepository = $userRepository;
        $this->accountService = $accountService;
        $this->mailerFactory = $mailerFactory;
        $this->translator = $translator;
        $this->vipService = $vipService;
        $this->em = $em;
        $this->logger = $logger;
    }

    public function execute()
    {
        $users = $this->userRepository->findAll();

        $results = [
            0 => 0,
            1 => 0,
            2 => 0,
            3 => 0,
            4 => 0,
            5 => 0,
            6 => 0,
            7 => 0,
        ];
        $i = 0;

        /** @var User $user $user */
        foreach ($users as $user) {
            try {
                $code = $this->processUser($user);

                $this->logger->info('User #{id} ({login}) notification status code: {status}.', [
                    'id' => $user->getId(),
                    'login' => $user->getUsername(),
                    'status' => $code,
                ]);

                if (!isset($results[$code])) {
                    $results[$code] = 0;
                }

                $results[$code]++;

                $i++;

                if ($i >= 1) {
//                    break;
                }
            } catch (\Throwable $e) {
                $this->logger->critical($e->getMessage());
                $i++;

                continue;
            }
        }

        $this->logger->info('Promotion finished.', [
            'skipped' => $results[0],
            'not_logged_in' => $results[1],
            'less_than_50' => $results[2],
            'less_than_150' => $results[3],
            'less_than_250' => $results[4],
            'longer_than_5_days' => $results[5],
            'longer_than_3_days' => $results[6],
            'already_vip' => $results[7],
        ]);
    }

    private function processUser(User $user): int
    {
        $userServerAccount = $user->getLastUserServerAccount();

        if (null === $userServerAccount || !$user->emailState()->isActive()) {
            return 0;
        }

        if ($userServerAccount->isVip()) {
            return 7;
        }

        $account = $this->accountService->getAccountByUserServerAccount($userServerAccount);
        $characters = $account->getCharacters()->toArray();

        if (null === $account->getState()->getConnectedAt() || !count($characters)) {
            $this->processNotLoggedIn($user, $account, 1);

            return 1;
        }

        if ($account->getState()->getConnectedAt() > (new \DateTimeImmutable())->modify('-3 days')) {
            return 0;
        }

        usort($characters, function (Character $a, Character $b): int {
            return $a->getLevel() <=> $b->getLevel();
        });

        /** @var Character $character */
        $character = array_pop($characters);

        if ($character->getLevel() < 50) {
            $this->processLessThan50($user, $account, $character->getLevel());

            return 2;
        }

        if ($character->getLevel() < 150) {
            $this->processLessThan150($user, $account, $character->getLevel());

            return 3;
        }

        if ($character->getLevel() < 250) {
            $this->processLessThan250($user, $account, $character->getLevel());

            return 4;
        }

        if ($account->getState()->getConnectedAt() < (new \DateTimeImmutable())->modify('-5 days')) {
            $this->processLongerThan5Days($user, $account, $character->getLevel());

            return 5;
        }

        $this->processLongerThan3Days($user, $account, $character->getLevel());

        return 6;
    }

    private function processNotLoggedIn(User $user, Account $account, int $maxLevel): void
    {
        $vipLevel = $this->vipLevel(4);

        $this->vipService->activate($vipLevel, $user->getLastUserServerAccount(), 5);

        $this->sendMail(
            $user,
            $vipLevel,
            '@App/email/other/return_leaved_players/not_logged_in.html.twig',
            5,
            0,
            $maxLevel
        );
    }

    private function processLessThan50(User $user, Account $account, int $maxLevel): void
    {
        $vipLevel = $this->vipLevel(4);

        $this->vipService->activate($vipLevel, $user->getLastUserServerAccount(), 5);

        $this->sendMail(
            $user,
            $vipLevel,
            '@App/email/other/return_leaved_players/low_level.html.twig',
            5,
            $account->getState()->getConnectedAt()->diff(new \DateTimeImmutable())->d,
            $maxLevel
        );
    }

    private function processLessThan150(User $user, Account $account, int $maxLevel): void
    {
        $vipLevel = $this->vipLevel(4);

        $this->vipService->activate($vipLevel, $user->getLastUserServerAccount(), 4);

        $this->sendMail(
            $user,
            $vipLevel,
            '@App/email/other/return_leaved_players/low_level.html.twig',
            4,
            $account->getState()->getConnectedAt()->diff(new \DateTimeImmutable())->d,
            $maxLevel
        );
    }

    private function processLessThan250(User $user, Account $account, int $maxLevel): void
    {
        $vipLevel = $this->vipLevel(4);

        $this->vipService->activate($vipLevel, $user->getLastUserServerAccount(), 3);

        $this->sendMail(
            $user,
            $vipLevel,
            '@App/email/other/return_leaved_players/low_level.html.twig',
            3,
            $account->getState()->getConnectedAt()->diff(new \DateTimeImmutable())->d,
            $maxLevel
        );
    }

    private function processLongerThan5Days(User $user, Account $account, int $maxLevel): void
    {
        $vipLevel = $this->vipLevel(3);

        $this->vipService->activate($vipLevel, $user->getLastUserServerAccount(), 4);

        $this->sendMail(
            $user,
            $vipLevel,
            '@App/email/other/return_leaved_players/stopped_playing.html.twig',
            4,
            $account->getState()->getConnectedAt()->diff(new \DateTimeImmutable())->d,
            $maxLevel
        );
    }

    private function processLongerThan3Days(User $user, Account $account, int $maxLevel): void
    {
        $vipLevel = $this->vipLevel(4);

        $this->vipService->activate($vipLevel, $user->getLastUserServerAccount(), 3);

        $this->sendMail(
            $user,
            $vipLevel,
            '@App/email/other/return_leaved_players/stopped_playing.html.twig',
            3,
            $account->getState()->getConnectedAt()->diff(new \DateTimeImmutable())->d,
            $maxLevel
        );
    }

    private function sendMail(User $user, VipLevelConfig $vipLevelConfig, string $templateName, int $vipDays, int $lastSeenDaysAgo, int $maxLevel): void
    {
        $this->translator->setLocale($user->getLocale());

        $this->mailerFactory
            ->createDynamicMailerFactory(
                $user->email(),
                'email.return_leaved_players.subject',
                $templateName,
                [
                    'user' => $user,
                    'userServerAccount' => $user->getLastUserServerAccount(),
                    'vipLevel' => $vipLevelConfig,
                    'vipDays' => $vipDays,
                    'lastSeenDaysAgo' => $lastSeenDaysAgo,
                    'maxLevel' => $maxLevel,
                ]
            )
            ->send();
    }

    private function vipLevel(int $levelType): VipLevelConfig
    {
        return $this->em->createQueryBuilder()
            ->select('vl')
            ->from(VipLevelConfig::class, 'vl')
            ->where('vl.type = :level_type')
            ->setParameter('level_type', $levelType)
            ->getQuery()
            ->getSingleResult();
    }
}