<?php

declare(strict_types=1);

namespace AppBundle\Promotion;

use AppBundle\Entity\Application\User\User;
use AppBundle\Mailer\Factory\MailerFactory;
use AppBundle\Repository\Application\UserRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Translation\TranslatorInterface;

class UserPlainEmailSender
{
    private $userRepository;
    private $mailerFactory;
    private $translator;
    private $logger;

    public function __construct(UserRepository $userRepository, MailerFactory $mailerFactory, TranslatorInterface $translator, LoggerInterface $logger)
    {
        $this->userRepository = $userRepository;
        $this->mailerFactory = $mailerFactory;
        $this->translator = $translator;
        $this->logger = $logger;
    }

    public function send(string $title, string $template, bool $debug)
    {
        if ($debug) {
            $users = $this->userRepository->findByLogin(['username' => 'admin']);
        } else {
            $users = $this->userRepository->findAll();
        }

        /** @var User $user */
        foreach ($users as $user) {
            $this->translator->setLocale($user->getLocale());

            try {
                $this->mailerFactory
                    ->createDynamicMailerFactory(
                        $user->email(),
                        $title,
                        $template,
                        [
                            'user' => $user
                        ]
                    )
                    ->send();
            } catch (\Throwable $e) {
                $this->logger->error(
                    'Failed to send message to recipient "{recipient}". Error: "{error}".',
                    [
                        'recipient' => $user->email(),
                        'error' => $e->getMessage(),
                    ]
                );

                continue;
            }

        }
    }
}