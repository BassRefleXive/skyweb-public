<?php

namespace AppBundle\Mailer\Transport;


use Mailgun\HttpClientConfigurator;
use Psr\Log\LoggerInterface;
use Swift_Events_EventListener;
use Swift_Mime_Message;
use Swift_Transport;
use Mailgun\Mailgun as MailgunApi;

class Mailgun implements Swift_Transport
{
    private $logger;

    private $domain;

    private $mailgun;

    private $senders;

    public function __construct(LoggerInterface $logger, string $apiKey, string $domain, array $senders)
    {
        $this->logger = $logger;
        $this->domain = $domain;
        $this->mailgun = MailgunApi::create($apiKey);
        $this->senders = $senders;
    }

    public function send(Swift_Mime_Message $message, &$failedRecipients = null)
    {
        if (null === $message->getHeaders()->get('To')) {
            throw new \Swift_TransportException(
                'Cannot send message without a recipient'
            );
        }
        if (!count($message->getFrom())) {
            $message->setFrom($this->senders);
        }

        $postData = $this->prepareRecipients($message);
        $sendResult = $this->mailgun->sendMessage($this->domain, $postData, $message->toString());

        $result = true;
        if ($sendResult->http_response_code !== 200) {
            $result = false;
            $responseBody = '';
            foreach ($sendResult->http_response_body->items as $errorItem) {
                $responseBody .= sprintf('Message id: %s, message: %s', $errorItem->message_id, $errorItem->message) . PHP_EOL;
            }
            $this->logger->error(
                'Mailer transport error. Mail send failed. Status code: {statusCode}; Response body: {responseBody}',
                [
                    'statusCode' => $sendResult->http_response_code,
                    'responseBody' => $responseBody,
                ]
            );
        }
        return $result;
    }

    protected function prepareRecipients(Swift_Mime_Message $message)
    {
        $headerNames = ['from', 'to', 'bcc', 'cc'];
        $messageHeaders = $message->getHeaders();
        $postData = [];
        foreach ($headerNames as $name) {
            /** @var \Swift_Mime_Headers_MailboxHeader $h */
            $h = $messageHeaders->get($name);
            $postData[$name] = $h === null ? [] : $h->getAddresses();
        }

        $postData['to'] = array_merge($postData['to'], $postData['bcc'], $postData['cc']);
        unset($postData['bcc']);
        unset($postData['cc']);

        $messageHeaders->removeAll('bcc');

        return $postData;
    }

    public function registerPlugin(Swift_Events_EventListener $plugin)
    {
    }

    public function isStarted()
    {
        return true;
    }

    public function start()
    {
    }

    public function stop()
    {
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setDomain($domain): self
    {
        $this->domain = $domain;

        return $this;
    }
}