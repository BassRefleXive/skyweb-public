<?php

declare(strict_types=1);

namespace AppBundle\Mailer\Exception;

use AppBundle\Exception\Application\DisplayableException;

class InvalidEmailException extends DisplayableException
{
    public static final function markedAsInvalid(): self
    {
        return new self('errors.60');
    }
}