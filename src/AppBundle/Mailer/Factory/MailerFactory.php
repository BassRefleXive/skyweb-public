<?php

namespace AppBundle\Mailer\Factory;


use AppBundle\Mailer\Mailer\Other\ContactRequestMailer;
use AppBundle\Mailer\Mailer\Other\DynamicMailer;
use AppBundle\Mailer\Mailer\User\ResetPasswordMailer;
use Swift_Mailer;
use Symfony\Component\Translation\TranslatorInterface;

class MailerFactory
{
    private $mailersConfig;
    private $mailer;
    private $renderer;
    private $translator;

    public function __construct(Swift_Mailer $mailer, \Twig_Environment $renderer, TranslatorInterface $translator, array $mailersConfig)
    {
        $this->mailersConfig = $mailersConfig;
        $this->mailer = $mailer;
        $this->renderer = $renderer;
        $this->translator = $translator;
    }

    public function createResetPasswordMailer(): ResetPasswordMailer
    {
        return new ResetPasswordMailer(
            $this->mailer,
            $this->renderer,
            $this->translator,
            $this->mailersConfig['reset_password']
        );
    }

    public function createContactRequestMailer(): ContactRequestMailer
    {
        return new ContactRequestMailer(
            $this->mailer,
            $this->renderer,
            $this->translator,
            $this->mailersConfig['contact_request']
        );
    }

    public function createDynamicMailerFactory(string $recipient, string $subject, string $template, array $parameters = []): DynamicMailer
    {
        return (new DynamicMailer($this->mailer, $this->renderer, $this->translator))
            ->toRecipient($recipient)
            ->withSubject($subject)
            ->withTemplate($template)
            ->withParameters($parameters);
    }
}