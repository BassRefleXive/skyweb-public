<?php


namespace AppBundle\Mailer\Mailer\User;


use AppBundle\Entity\Application\User\User;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Mailer\Exception\InvalidEmailException;
use AppBundle\Mailer\Mailer\Mailer;

abstract class UserMailer extends Mailer
{
    private $user;

    public function setUser(User $user): UserMailer
    {
        $this->user = $user;

        return $this;
    }

    protected function user(): User
    {
        if (!$this->user instanceof User) {
            throw new \RuntimeException('User must be set.');
        }

        if (!$this->user->emailState()->isActive()) {
            throw InvalidEmailException::markedAsInvalid();
        }

        return $this->user;
    }
}