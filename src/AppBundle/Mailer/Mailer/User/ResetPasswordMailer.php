<?php

namespace AppBundle\Mailer\Mailer\User;


use Collection\Sequence;

class ResetPasswordMailer extends UserMailer
{
    protected function buildMessages(): Sequence
    {
        $message = \Swift_Message::newInstance();
        $message->setSubject($this->translator->trans($this->config['subject']))
            ->setTo($this->user()->email())
            ->setBody($this->renderer->render('@App/email/user/reset_password.html.twig', [
                'message' => $message,
                'toEmbed' => $this->commonAssets(),
                'user'    => $this->user(),
            ]), 'text/html');

        return new Sequence([$message]);
    }
}