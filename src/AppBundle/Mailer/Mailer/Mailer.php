<?php

namespace AppBundle\Mailer\Mailer;

use Collection\Sequence;
use Swift_Mailer;
use Symfony\Component\Translation\TranslatorInterface;

abstract class Mailer
{
    private $mailer;
    protected $renderer;
    protected $translator;
    protected $config;
//    private $assetsDir;

    public function __construct(Swift_Mailer $mailer, \Twig_Environment $renderer, TranslatorInterface $translator, array $config = [])
    {
        $this->mailer = $mailer;
        $this->renderer = $renderer;
        $this->config = $config;
        $this->translator = $translator;

//        $this->assetsDir = __DIR__ . '/../Resources/assets/images/';
    }

    protected function commonAssets(): array
    {
        return [
//            'headerLogo' => Swift_Image::fromPath($this->assetsDir . 'logo-small.png'),
        ];
    }

    public final function send()
    {
        return $this->sendMessages($this->messages());
    }

    private function sendMessages(Sequence $messages)
    {
        return !$messages->map(function (\Swift_Message $message) {
//            echo $message->getBody();
//            $message->setTo('mihails.bagrovs@gmail.com');
            return $this->mailer->send($message) > 0;
        })->contains(false);
    }

    public function messages(): Sequence
    {
        return $this->buildMessages();
    }

    abstract protected function buildMessages(): Sequence;
}