<?php

declare(strict_types=1);

namespace AppBundle\Mailer\Mailer\Other;

use AppBundle\Mailer\Mailer\Mailer;
use Collection\Sequence;

class DynamicMailer extends Mailer
{
    private $subject;
    private $template;
    private $parameters;
    private $recipient;

    public function withSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function withTemplate(string $template): self
    {
        $this->template = $template;

        return $this;
    }

    public function withParameters(array $parameters = []): self
    {
        $this->parameters = $parameters;

        return $this;
    }

    public function toRecipient(string $recipient): self
    {
        $this->recipient = $recipient;

        return $this;
    }

    protected function buildMessages(): Sequence
    {
        $message = \Swift_Message::newInstance();
        $message->setSubject($this->translator->trans($this->subject))
            ->setTo($this->recipient)
            ->setBody($this->renderer->render(
                $this->template,
                array_merge(
                    [
                        'message' => $message,
                        'toEmbed' => $this->commonAssets(),
                    ],
                    $this->parameters
                )
            ), 'text/html');

        return new Sequence([$message]);
    }
}