<?php

namespace AppBundle\Mailer\Mailer\Other;

use AppBundle\Mailer\Mailer\Mailer;
use AppBundle\Model\Application\ContactRequestModel;
use Collection\Sequence;

class ContactRequestMailer extends Mailer
{
    /**
     * @var ContactRequestModel
     */
    private $model;

    public function setModel(ContactRequestModel $model): self
    {
        $this->model = $model;

        return $this;
    }

    protected function buildMessages(): Sequence
    {
        $message = \Swift_Message::newInstance();
        $message->setSubject($this->translator->trans($this->config['subject']))
            ->setTo($this->config['receiver_email'])
            ->setFrom($this->model->getEmail())
            ->setBody($this->renderer->render('@App/email/other/contact_request.html.twig', [
                'message' => $message,
                'toEmbed' => $this->commonAssets(),
                'request' => [
                    'email'   => $this->model->getEmail(),
                    'subject' => $this->model->getSubject(),
                    'text'    => $this->model->getText(),
                ],
            ]), 'text/html');

        return new Sequence([$message]);
    }
}