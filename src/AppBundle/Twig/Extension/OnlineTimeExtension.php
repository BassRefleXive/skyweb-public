<?php

namespace AppBundle\Twig\Extension;


use Symfony\Component\Translation\Translator;

class OnlineTimeExtension extends \Twig_Extension
{
    private $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    /**
     * Return the functions registered as twig extensions
     *
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('online_time', [$this, 'formatOnlineTime']),
        ];
    }

    public function formatOnlineTime(int $seconds): string
    {
        if ($seconds === 0) {
            return '0 ' . $this->translator->trans('online_time.2');
        }

        $dtF = new \DateTime('@0');
        $dtT = new \DateTime('@' . $seconds);

        $diff = $dtF->diff($dtT);

        return implode(', ', array_merge(
            $diff->m > 0
                ? [$diff->m . ' ' . $this->translator->trans('online_time.0')]
                : [],
            $diff->d > 0
                ? [$diff->d . ' ' . $this->translator->trans('online_time.1')]
                : [],
            $diff->h > 0
                ? [$diff->h . ' ' . $this->translator->trans('online_time.2')]
                : [],
            $diff->i > 0
                ? [$diff->i . ' ' . $this->translator->trans('online_time.3')]
                : [],
            $diff->s > 0
                ? [$diff->s . ' ' . $this->translator->trans('online_time.4')]
                : []
        ));
    }

    public
    function getName()
    {
        return 'app.online_time';
    }

}