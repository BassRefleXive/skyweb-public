<?php


namespace AppBundle\Twig\Extension;


class ContactsExtension extends \Twig_Extension
{
    private $contacts;

    public function __construct(array $contacts)
    {
        $this->contacts = $contacts;
    }

    public function getFunctions(): array
    {
        return [
            new \Twig_SimpleFunction('contacts', [$this, 'contactsFunction']),
        ];
    }

    public function contactsFunction(string $type): string
    {
        return $this->contacts[$type] ?? '';
    }
}