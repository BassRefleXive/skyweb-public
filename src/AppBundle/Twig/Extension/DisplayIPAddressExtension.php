<?php


namespace AppBundle\Twig\Extension;


class DisplayIPAddressExtension extends \Twig_Extension
{
    public function getFilters(): array
    {
        return [
            new \Twig_SimpleFilter('long2ip', 'long2ip'),
        ];
    }
}