<?php

declare(strict_types = 1);

namespace AppBundle\Twig\Extension;


use AppBundle\Service\Application\PartialTranslatorService;

class PartialTranslator extends \Twig_Extension
{
    private $partialTranslatorService;

    public function __construct(PartialTranslatorService $partialTranslatorService)
    {
        $this->partialTranslatorService = $partialTranslatorService;
    }

    public function getFilters(): array
    {
        return [
            new \Twig_SimpleFilter('translate_webshop_category', [$this->partialTranslatorService, 'translateWebShopCategory']),
            new \Twig_SimpleFilter('translate_market_category', [$this->partialTranslatorService, 'translateMarketCategory']),
        ];
    }
}