<?php

namespace AppBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class AppExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        $this->configureDoctrineCacheProvider($container);
        $this->configureApplicationCacheStorage($container);
        $this->configureSlack($config['slack'], $container);
        $this->configureUser($config['user'], $container);
        $this->configureMailer($config['mailer'], $container);
        $this->configureContacts($config['contacts'], $container);
        $this->configureSentry($config['sentry'], $container);
        $this->configureChatBro($config['chat_bro'], $container);
        $this->configureScannersAutoBan($config['scanners_auto_ban'], $container);
        $this->configureTrackingScripts($config['tracking_script'], $container);
        $this->configureModules($config['modules'], $container);
        $this->configureForum($config['forum'], $container);
        $this->configurePayments($config['payments'], $container);
        $this->configureVoteBanners($config['vote_banners'], $container);
        $this->configureCloudflare($config['cloudflare'], $container);
    }

    private function configureCloudflare(array $config, ContainerBuilder $container)
    {
        $container->setParameter('sky_app.app.cloudflare.credentials.email', $config['credentials']['email']);
        $container->setParameter('sky_app.app.cloudflare.credentials.key', $config['credentials']['key']);
    }

    private function configureVoteBanners(array $config, ContainerBuilder $container)
    {
        $container->setParameter('sky_app.app.vote_banners', $config);
    }

    private function configurePayments(array $config, ContainerBuilder $container)
    {
        $container->setParameter('sky_app.app.payments.pay_pal.environment', $config['pay_pal']['environment']);
        $container->setParameter('sky_app.app.payments.pay_pal.enabled', $config['pay_pal']['enabled']);
        $container->setParameter('sky_app.app.payments.pay_pal.account_id', $config['pay_pal']['account_id']);
        $container->setParameter('sky_app.app.payments.pay_pal.secret', $config['pay_pal']['secret']);
        $container->setParameter('sky_app.app.payments.pay_pal.currency', $config['pay_pal']['currency']);
    }

    private function configureForum(array $config, ContainerBuilder $container)
    {
        $container->setParameter('sky_app.app.forum.host', $config['host']);
        $container->setParameter('sky_app.app.forum.api_host', $config['api_host']);
        $container->setParameter('sky_app.app.forum.api_key', $config['api_key']);
        $container->setParameter('sky_app.app.forum.last_five_forum_ids', $config['last_five_forum_ids']);
    }

    private function configureModules(array $config, ContainerBuilder $container)
    {
        $container->setParameter('sky_app.app.modules.registration.enabled', (bool) $config['registration']['enabled']);
    }

    private function configureTrackingScripts(array $config, ContainerBuilder $container)
    {
        $container->setParameter('sky_app.app.tracking_script.google.code', $config['google']['code']);
        $container->setParameter('sky_app.app.tracking_script.yandex.code', $config['yandex']['code']);
        $container->setParameter('sky_app.app.tracking_script.mmotop.code', $config['mmotop']['code']);
        $container->setParameter('sky_app.app.tracking_script.qtop.code', $config['qtop']['code']);
        $container->setParameter('sky_app.app.tracking_script.chat.tawk', $config['chat']['tawk']);

        if ($container->getParameter('kernel.environment') === 'prod') {
            $container->getDefinition('sky_app.tracking_script.twig.extension.real')->addTag('twig.extension');
        } else {
            $container->getDefinition('sky_app.tracking_script.twig.extension.empty')->addTag('twig.extension');
        }
    }

    private function configureScannersAutoBan(array $config, ContainerBuilder $container)
    {
        $container->setParameter('sky_app.app.scanners_auto_ban.file', $config['file']);
        $container->setParameter('sky_app.app.scanners_auto_ban.paths', $config['paths']);
    }

    private function configureChatBro(array $config, ContainerBuilder $container)
    {
        $container->setParameter('sky_app.app.chat_bro.secret_key', $config['secret_key']);
        $container->setParameter('sky_app.app.chat_bro.chat_id', $config['chat_id']);
        $container->setParameter('sky_app.app.chat_bro.users', $config['users']);
    }

    private function configureSentry(array $config, ContainerBuilder $container)
    {
        $container->setParameter('sky_app.app.sentry.dsn', $config['dsn']);
    }

    private function configureContacts(array $config, ContainerBuilder $container)
    {
        $container->setParameter('sky_app.app.contacts.email', $config['email']);
        $container->setParameter('sky_app.app.contacts.skype', $config['skype']);
        $container->setParameter('sky_app.app.contacts.facebook', $config['facebook']);
        $container->setParameter('sky_app.app.contacts.vkontakte', $config['vkontakte']);
    }

    private function configureMailer(array $config, ContainerBuilder $container)
    {
        $container
            ->getDefinition('sky_app.app.mailer.services.factory')
            ->replaceArgument(3, $config['mailers']);

        $container
            ->getDefinition('sky_app.app.mailer.transport.mailgun')
            ->replaceArgument(1, $config['mailgun']['api_key'])
            ->replaceArgument(2, $config['mailgun']['domain'])
            ->replaceArgument(3, $config['mailgun']['senders']);

        $container->setAlias('mailgun_transport', 'sky_app.app.mailer.transport.mailgun');
        $container->setAlias('swiftmailer.mailer.transport.mailgun_transport', 'sky_app.app.mailer.transport.mailgun');
        $container->setAlias('swiftmailer.mailer.default.transport', 'swiftmailer.mailer.transport.mailgun_transport');
    }


    private function configureUser(array $config, ContainerBuilder $container)
    {
        $container->setParameter('sky.sky_app.user.reset_password_interval', $config['reset_password_interval']);
    }

    private function configureSlack(array $config, ContainerBuilder $container)
    {
        $container
            ->getDefinition('sky_app.app.slack.services.payment_notification')
            ->replaceArgument(1, $config['payment']['channel'])
            ->replaceArgument(2, $config['bot_nickname']);

        $container
            ->getDefinition('sky_app.app.slack.services.user_event_notification')
            ->replaceArgument(1, $config['user_event']['channel'])
            ->replaceArgument(2, $config['bot_nickname']);

        $container
            ->getDefinition('sky_app.app.slack.services.dupe_check_notification')
            ->replaceArgument(1, $config['dupe_check']['channel'])
            ->replaceArgument(2, $config['bot_nickname']);
    }

    private function configureDoctrineCacheProvider(ContainerBuilder $container)
    {
        $doctrineCacheProviderServiceId = 'sky_app.app.service.cache.provider.doctrine';

        if (!$container->hasDefinition($doctrineCacheProviderServiceId)) {
            throw new \RuntimeException('Doctrine cache provider service definition not found.');
        }

        $doctrineCacheServiceId = "doctrine_cache.providers." . $container->getParameter('doctrine_cache_driver');

        $container
            ->getDefinition($doctrineCacheProviderServiceId)
            ->replaceArgument(0, new ChildDefinition($doctrineCacheServiceId));
    }

    private function configureApplicationCacheStorage(ContainerBuilder $container)
    {
        $appCacheStorageService = 'sky_app.app.service.cache.storage';

        $container->setAlias(
            $appCacheStorageService,
            $appCacheStorageService . '.' . $container->getParameter('app_cache_storage')
        );
    }
}
