<?php

namespace AppBundle\Filter;


use AppBundle\Filter\Interfaces\FilterInterface;
use AppBundle\Filter\Interfaces\PaginatedFilterInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\Tools\Pagination\Paginator;

class GuildFilter extends AbstractFilter implements FilterInterface, PaginatedFilterInterface
{
    const DEFAULT_COUNT = 18;
    const FILTER_LIMIT = 18;

    private $count;

    public function __construct(int $page)
    {
        $this->setPage($page);
    }

    static public function buildFromRequest(Request $request): GuildFilter
    {
        return new static(
            (int)$request->get('page', PaginatedFilterInterface::DEFAULT_PAGE)
        );
    }

    public function getCount()
    {
        return $this->count ?? GuildFilter::DEFAULT_COUNT;
    }

    public function setCount(int $count): GuildFilter
    {
        $this->count = $count;

        return $this;
    }

    public function buildPaginator(QueryBuilder $qb, int $itemsPerPage = self::DEFAULT_COUNT): Paginator
    {
        return parent::buildPaginator(
            $this->buildQueryBuilder($qb),
            self::FILTER_LIMIT
        );
    }

    public function buildQueryFilter(QueryBuilder $qb): Query
    {
        $qb = $this->buildQueryBuilder($qb);

        return parent::buildQueryFilter($qb);
    }


    private function buildQueryBuilder(QueryBuilder $qb): QueryBuilder
    {
        $qb->select('e AS entity')
            ->orderBy('e.score', 'DESC')
            ->setMaxResults($this->getCount());

        return $qb;
    }

    protected function validate()
    {
        (new OptionsResolver())
            ->setDefined(
                [
                    'count',
                ]
            )
            ->setAllowedTypes('count', ['int', 'null'])
            ->resolve(
                [
                    'count' => $this->count,
                ]
            );
    }
}