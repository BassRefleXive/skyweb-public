<?php

namespace AppBundle\Filter\Interfaces;

use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;

interface FilterInterface
{
    function buildQueryFilter(QueryBuilder $qb): Query;
}