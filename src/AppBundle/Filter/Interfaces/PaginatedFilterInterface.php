<?php

namespace AppBundle\Filter\Interfaces;


use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;

interface PaginatedFilterInterface
{
    const ITEMS_PER_PAGE = 10;
    const DEFAULT_PAGE = 1;

    function buildPaginator(QueryBuilder $qb): Paginator;

}