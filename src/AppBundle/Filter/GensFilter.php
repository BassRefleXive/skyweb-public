<?php


namespace AppBundle\Filter;


use AppBundle\Enum\MuOnline\Gens\Family;
use AppBundle\Filter\Interfaces\FilterInterface;
use AppBundle\Filter\Interfaces\PaginatedFilterInterface;
use AppBundle\Repository\MuOnline\CharacterRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\Tools\Pagination\Paginator;

class GensFilter extends AbstractFilter implements FilterInterface, PaginatedFilterInterface
{
    const DEFAULT_COUNT = 18;
    const FILTER_LIMIT = 18;

    private $family;
    private $count;

    public function __construct(Family $family, int $page)
    {
        $this->family = $family;
        $this->setPage($page);
    }

    static public function buildFromRequest(int $family, Request $request): self
    {
        return new static(
            Family::byValue($family),
            (int) $request->get('page', PaginatedFilterInterface::DEFAULT_PAGE)
        );
    }

    public function getCount()
    {
        return $this->count ?? self::DEFAULT_COUNT;
    }

    public function setCount(int $count): self
    {
        $this->count = $count;

        return $this;
    }

    public function buildPaginator(QueryBuilder $qb, int $itemsPerPage = self::DEFAULT_COUNT): Paginator
    {
        return parent::buildPaginator(
            $this->buildQueryBuilder($qb),
            self::FILTER_LIMIT
        );
    }

    public function buildQueryFilter(QueryBuilder $qb): Query
    {
        $qb = $this->buildQueryBuilder($qb);

        return parent::buildQueryFilter($qb)->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, 1);
    }

    private function buildQueryBuilder(QueryBuilder $qb): QueryBuilder
    {
        $qb
            ->select('e, c, acc, accChar')
            ->innerJoin('e.character', 'c')
            ->innerJoin('c.account', 'acc')
            ->innerJoin('acc.currentAccountCharacter', 'accChar')
            ->where('e.family = :family')
            ->andWhere('c.ctlCode NOT IN (:HIDDEN_CHARACTER_CTL_CODES)')
            ->setParameter('family', $this->family->getValue())
            ->setParameter('HIDDEN_CHARACTER_CTL_CODES', [
                CharacterRepository::CTL_CODE_ADMIN,
                CharacterRepository::CTL_CODE_GM,
                CharacterRepository::CTL_CODE_BANNED,
            ])
            ->orderBy('e.rank', 'DESC')
            ->addOrderBy('e.points', 'DESC')
            ->setMaxResults($this->getCount());

        return $qb;
    }

    public function paginate(QueryBuilder $qb, int $page, int $limit): Paginator
    {
        $paginator = parent::paginate($qb, $page, $limit);

        $paginator->getQuery()
            ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, 1);

        return $paginator;
    }

    protected function validate()
    {
        (new OptionsResolver())
            ->setDefined(
                [
                    'count',
                    'family',
                ]
            )
            ->setAllowedTypes('count', ['int', 'null'])
            ->setAllowedTypes('family', Family::class)
            ->resolve(
                [
                    'count'  => $this->count,
                    'family' => $this->family,
                ]
            );
    }
}