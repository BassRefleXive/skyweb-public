<?php

declare(strict_types = 1);

namespace AppBundle\Filter;

use AppBundle\Entity\Application\Server;
use AppBundle\Entity\Application\User\Admin;
use AppBundle\Enum\Application\VoteType;
use AppBundle\Filter\Interfaces\FilterInterface;
use AppBundle\Filter\Interfaces\PaginatedFilterInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\Tools\Pagination\Paginator;

class VoterFilter extends AbstractFilter implements FilterInterface, PaginatedFilterInterface
{
    const DEFAULT_COUNT = 12;
    const FILTER_LIMIT = 12;

    private $count;
    private $from;
    private $till;
    private $server;

    public function __construct(int $page)
    {
        $this->setPage($page);
    }

    static public function buildFromRequest(Request $request): self
    {
        return new static(
            (int) $request->get('page', PaginatedFilterInterface::DEFAULT_PAGE)
        );
    }

    public function getCount()
    {
        return $this->count ?? self::DEFAULT_COUNT;
    }

    public function setCount(int $count): self
    {
        $this->count = $count;

        return $this;
    }

    public function server(): Server
    {
        return $this->server;
    }

    public function withServer(Server $server): self
    {
        $this->server = $server;

        return $this;
    }

    public function withStartDate(\DateTimeInterface $from): self
    {
        $this->from = $from;

        return $this;
    }

    public function withEndDate(\DateTimeInterface $till): self
    {
        $this->till = $till;

        return $this;
    }

    public function buildPaginator(QueryBuilder $qb, int $itemsPerPage = self::DEFAULT_COUNT): Paginator
    {
        return parent::buildPaginator(
            $this->buildQueryBuilder($qb),
            self::FILTER_LIMIT
        );
    }

    public function buildQueryFilter(QueryBuilder $qb): Query
    {
        $qb = $this->buildQueryBuilder($qb);

        return parent::buildQueryFilter($qb);
    }

    private function buildQueryBuilder(QueryBuilder $qb): QueryBuilder
    {
        $qb
            ->select('e as entity, s, tv, rr1, rr2, u')
            ->addSelect('(
                SELECT COUNT(v1.id) FROM AppBundle\Entity\Application\Vote v1 
                WHERE v1.voter = e AND v1.type = :VOTE_TYPE_REGULAR AND v1.dateTime >= :FROM AND v1.dateTime < :TILL
            ) AS regularVotes')
            ->addSelect('(
                SELECT COUNT(v2.id) FROM AppBundle\Entity\Application\Vote v2 
                WHERE v2.voter = e AND v2.type = :VOTE_TYPE_SMS AND v2.dateTime >= :FROM AND v2.dateTime < :TILL
            ) AS smsVotes')
            ->addSelect('(
                SELECT COUNT(v3.id) FROM AppBundle\Entity\Application\Vote v3 
                WHERE v3.voter = e AND v3.dateTime >= :FROM AND v3.dateTime < :TILL
            ) AS totalVotes')
            ->innerJoin('e.server', 's')
            ->leftJoin('e.monthlyVotersTopRewards', 'tv')
            ->leftJoin('e.referredReward', 'rr1')
            ->leftJoin('e.referralReward', 'rr2')
            ->innerJoin('e.user', 'u')
            ->where('u NOT INSTANCE OF :admin')
            ->andWhere('e.server = :SERVER')
            ->andWhere('regularVotes > 0 OR smsVotes > 0')
            ->setParameter('admin', $qb->getEntityManager()->getClassMetadata(Admin::class))
            ->setParameter('SERVER', $this->server)
            ->setParameter('VOTE_TYPE_REGULAR', VoteType::REGULAR)
            ->setParameter('VOTE_TYPE_SMS', VoteType::PAID)
            ->setParameter('FROM', $this->from ?? new \DateTime(date('Y-m-01 00:00:00', time())))
            ->setParameter('TILL', $this->till ?? new \DateTime(date('Y-m-t 23:59:59', time())))
            ->orderBy('totalVotes', 'DESC');

        return $qb;
    }

    protected function validate()
    {
        (new OptionsResolver())
            ->setDefined(
                [
                    'count',
                    'server',
                    'from',
                    'till',
                ]
            )
            ->setAllowedTypes('count', ['int', 'null'])
            ->setAllowedTypes('server', [Server::class])
            ->setAllowedTypes('from', [\DateTimeInterface::class, 'null'])
            ->setAllowedTypes('till', [\DateTimeInterface::class, 'null'])
            ->resolve(
                [
                    'count'  => $this->count,
                    'server' => $this->server,
                    'from'   => $this->from,
                    'till'   => $this->till,
                ]
            );
    }
}