<?php

namespace AppBundle\Filter;


use AppBundle\Doctrine\Type\CharacterClassEnum;
use AppBundle\Filter\Interfaces\FilterInterface;
use AppBundle\Filter\Interfaces\PaginatedFilterInterface;
use AppBundle\Repository\MuOnline\CharacterRepository;
use AppBundle\Repository\Traits\LikeQueryHelpers;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\Tools\Pagination\Paginator;

class CharacterFilter extends AbstractFilter implements FilterInterface, PaginatedFilterInterface
{
    use LikeQueryHelpers;

    const DEFAULT_COUNT = 15;
    const CHARACTER_FILTER_LIMIT = 15;
    const LAST_CONNECT_DATE_DAYS_LIMIT = 30;

    private $class;
    private $nicks;
    private $count;

    public function __construct(int $class = null, int $page)
    {
        $this->setClass($class);
        $this->setPage($page);
    }

    static public function buildFromRequest(Request $request): CharacterFilter
    {
        $filter = new static(
            $request->get('class'),
            (int) $request->get('page', PaginatedFilterInterface::DEFAULT_PAGE)
        );

        return $filter;
    }

    public function updateByRequest(Request $request): MarketFilter
    {
        return $this->setPage((int) $request->get('page', PaginatedFilterInterface::DEFAULT_PAGE));
    }

    public function getClass()
    {
        return $this->class;
    }

    protected function setClass(int $class = null): CharacterFilter
    {
        $this->class = $class;

        return $this;
    }

    public function setNick(string $nick): CharacterFilter
    {
        $this->nicks = explode(',', $nick);

        return $this;
    }

    public function getCount()
    {
        return $this->count ?? CharacterFilter::DEFAULT_COUNT;
    }

    public function setCount(int $count): CharacterFilter
    {
        $this->count = $count;

        return $this;
    }

    public function buildPaginator(QueryBuilder $qb, int $itemsPerPage = self::DEFAULT_COUNT): Paginator
    {
        return parent::buildPaginator(
            $this->buildQueryBuilder($qb),
            self::CHARACTER_FILTER_LIMIT
        );
    }

    public function buildQueryFilter(QueryBuilder $qb): Query
    {
        $qb = $this->buildQueryBuilder($qb);
        $qb->setMaxResults($this->getCount());

        return parent::buildQueryFilter($qb)->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, 1);
    }

    private function buildQueryBuilder(QueryBuilder $qb): QueryBuilder
    {
        $qb
            ->select('e, gm, gens, acc, accChar, state, g')
            ->leftJoin('e.guildMember', 'gm')
            ->leftJoin('gm.guild', 'g')
            ->leftJoin('e.account', 'acc')
            ->leftJoin('e.gens', 'gens')
            ->leftJoin('acc.state', 'state')
            ->innerJoin('acc.currentAccountCharacter', 'accChar')
            ->where('e.ctlCode NOT IN (:HIDDEN_CHARACTER_CTL_CODES)')
            ->andWhere('acc.blocCode != 1')
            ->andWhere('state.connectedAt > :LAST_CONNECT_DATE_DAYS_LIMIT')
            ->orderBy('e.grandReset', 'DESC')
            ->addOrderBy('e.reset', 'DESC')
            ->addOrderBy('e.level', 'DESC')
            ->setMaxResults($this->getCount());

        $qb
            ->setParameters([
                'HIDDEN_CHARACTER_CTL_CODES'   => [
                    CharacterRepository::CTL_CODE_ADMIN,
                    CharacterRepository::CTL_CODE_GM,
                    CharacterRepository::CTL_CODE_BANNED,
                ],
                'LAST_CONNECT_DATE_DAYS_LIMIT' => (new \DateTime())->sub(new \DateInterval(sprintf('P%dD', self::LAST_CONNECT_DATE_DAYS_LIMIT)))->format('Y-m-d'),
            ]);

        if ($this->class !== null) {

            switch ($this->class) {
                case CharacterClassEnum::CHARACTER_CLASS_DARK_WIZARD: {
                    $classesToSelect = [
                        CharacterClassEnum::CHARACTER_CLASS_DARK_WIZARD,
                        CharacterClassEnum::CHARACTER_CLASS_SOUL_MASTER,
                        CharacterClassEnum::CHARACTER_CLASS_GRAND_MASTER,
                    ];
                    break;
                }
                case CharacterClassEnum::CHARACTER_CLASS_DARK_KNIGHT: {
                    $classesToSelect = [
                        CharacterClassEnum::CHARACTER_CLASS_DARK_KNIGHT,
                        CharacterClassEnum::CHARACTER_CLASS_BLADE_KNIGHT,
                        CharacterClassEnum::CHARACTER_CLASS_BLADE_MASTER,
                    ];
                    break;
                }
                case CharacterClassEnum::CHARACTER_CLASS_ELF: {
                    $classesToSelect = [
                        CharacterClassEnum::CHARACTER_CLASS_ELF,
                        CharacterClassEnum::CHARACTER_CLASS_MUSE_ELF,
                        CharacterClassEnum::CHARACTER_CLASS_HIGH_ELF,
                    ];
                    break;
                }
                case CharacterClassEnum::CHARACTER_CLASS_MAGIC_GLADIATOR: {
                    $classesToSelect = [
                        CharacterClassEnum::CHARACTER_CLASS_MAGIC_GLADIATOR,
                        CharacterClassEnum::CHARACTER_CLASS_DUEL_MASTER,
                    ];
                    break;
                }
                case CharacterClassEnum::CHARACTER_CLASS_DARK_LORD: {
                    $classesToSelect = [
                        CharacterClassEnum::CHARACTER_CLASS_DARK_LORD,
                        CharacterClassEnum::CHARACTER_CLASS_LORD_EMPEROR,
                    ];
                    break;
                }
                case CharacterClassEnum::CHARACTER_CLASS_SUMMONER: {
                    $classesToSelect = [
                        CharacterClassEnum::CHARACTER_CLASS_SUMMONER,
                        CharacterClassEnum::CHARACTER_CLASS_BLOODY_SUMMONER,
                        CharacterClassEnum::CHARACTER_CLASS_DIMENSION_MASTER,
                    ];
                    break;
                }
                case CharacterClassEnum::CHARACTER_CLASS_RAGE_FIGHTER: {
                    $classesToSelect = [
                        CharacterClassEnum::CHARACTER_CLASS_RAGE_FIGHTER,
                        CharacterClassEnum::CHARACTER_CLASS_FIST_MASTER,
                    ];
                    break;
                }

                default: {
                    throw new \InvalidArgumentException('Unknown character class provided');
                }
            }

            if (count($classesToSelect) > 0) {
                $qb->andWhere('e.class IN (:CLASS)')->setParameter('CLASS', $classesToSelect);
            }

        }

        if (null !== $this->nicks && count($this->nicks) > 0) {
            $statement = $qb->expr()->orX();

            foreach ($this->nicks as $nick) {
                $statement->add($qb->expr()->orX(sprintf("e.name LIKE '%%%s%%' ESCAPE '!'", $nick)));
            }

            $qb->andWhere($statement);
        }

        return $qb;
    }

    public function paginate(QueryBuilder $qb, int $page, int $limit): Paginator
    {
        $paginator = parent::paginate($qb, $page, $limit);

        $paginator->getQuery()
            ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, 1);

        return $paginator;
    }

    protected function validate()
    {
        (new OptionsResolver())
            ->setDefined(
                [
                    'class',
                    'nicks',
                    'count',
                ]
            )
            ->setAllowedTypes('class', ['int', 'null'])
            ->setAllowedTypes('nicks', ['array', 'null'])
            ->setAllowedTypes('count', ['int', 'null'])
            ->resolve(
                [
                    'class' => $this->class,
                    'nicks'  => $this->nicks,
                    'count' => $this->count,
                ]
            );
    }
}