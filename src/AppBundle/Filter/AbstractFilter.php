<?php

namespace AppBundle\Filter;

use AppBundle\Filter\Interfaces\FilterInterface;
use AppBundle\Filter\Interfaces\PaginatedFilterInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractFilter implements FilterInterface, PaginatedFilterInterface
{
    protected $page;


    public function getPage(): int
    {
        return $this->page ?? 0;
    }

    public function setPage(int $page): AbstractFilter
    {
        if ($page < 0) {
            throw new \InvalidArgumentException('Page cannot be less than zero.');
        }

        $this->page = $page;

        return $this;
    }

    public function buildQueryFilter(QueryBuilder $qb): Query
    {
        $this->validate();

        return $qb->getQuery();
    }

    public function buildPaginator(QueryBuilder $qb, int $itemsPerPage = PaginatedFilterInterface::ITEMS_PER_PAGE): Paginator
    {
        $this->validate();

        return $this->paginate($qb, $this->page, $itemsPerPage);
    }

    public function paginate(QueryBuilder $qb, int $page, int $limit): Paginator
    {
        $paginator = new Paginator($qb);

        $paginator->getQuery()
                  ->setFirstResult($limit * ($page - 1))
                  ->setMaxResults($limit);

        return $paginator;
    }

    abstract protected function validate();
}