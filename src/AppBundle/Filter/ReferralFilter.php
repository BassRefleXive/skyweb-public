<?php

namespace AppBundle\Filter;

use AppBundle\Entity\Application\User\User;
use AppBundle\Filter\Interfaces\PaginatedFilterInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReferralFilter extends AbstractFilter implements PaginatedFilterInterface
{
    const DEFAULT_COUNT = 10;
    const REFERRAL_FILTER_LIMIT = 10;

    private $user;
    private $count;


    public function __construct(int $page)
    {
        $this->setPage($page);
    }

    static public function buildFromRequest(Request $request): ReferralFilter
    {
        $filter = new static(
            (int) $request->get('page', PaginatedFilterInterface::DEFAULT_PAGE)
        );

        return $filter;
    }

    public function updateByRequest(Request $request): ReferralFilter
    {
        return $this->setPage((int) $request->get('page', PaginatedFilterInterface::DEFAULT_PAGE));
    }

    public function getCount()
    {
        return $this->count ?? ReferralFilter::DEFAULT_COUNT;
    }

    public function setCount(int $count): ReferralFilter
    {
        $this->count = $count;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user): ReferralFilter
    {
        $this->user = $user;

        return $this;
    }

    public function buildQueryFilter(QueryBuilder $qb): Query
    {
        $qb = $this->buildQueryBuilder($qb);
        $qb->setMaxResults($this->getCount());

        return parent::buildQueryFilter($qb);
    }

    public function buildPaginator(QueryBuilder $qb, int $itemsPerPage = ReferralFilter::REFERRAL_FILTER_LIMIT): Paginator
    {
        return parent::buildPaginator(
            $this->buildQueryBuilder($qb),
            $itemsPerPage
        );
    }

    private function buildQueryBuilder(QueryBuilder $qb): QueryBuilder
    {
        $this->user && $qb->andWhere('e.referredBy = :USER')->setParameter('USER', $this->user);

        $qb->orderBy('e.createdAt', 'DESC');

        return $qb;
    }

    protected function validate()
    {
        (new OptionsResolver())
            ->setDefined(
                [
                    'count',
                    'user',
                ]
            )
            ->setAllowedTypes('count', ['int', 'null'])
            ->setAllowedTypes('user', [User::class, 'null'])
            ->resolve(
                [
                    'count'    => $this->count,
                    'user' => $this->user,
                ]
            );
    }
}