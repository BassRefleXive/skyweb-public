<?php


namespace AppBundle\Filter;

use AppBundle\Entity\Application\Server;
use AppBundle\Filter\Interfaces\PaginatedFilterInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\Tools\Pagination\Paginator;

class NewsFilter extends AbstractFilter implements PaginatedFilterInterface
{
    const DEFAULT_COUNT = 4;
    const NEWS_FILTER_LIMIT = 4;

    private $count;
    private $server;

    public function __construct(int $page)
    {
        $this->setPage($page);
    }

    static public function buildFromRequest(Request $request): self
    {
        $filter = new static(
            (int) $request->get('page', PaginatedFilterInterface::DEFAULT_PAGE)
        );

        return $filter;
    }

    public function count()
    {
        return $this->count ?? self::DEFAULT_COUNT;
    }

    public function witCount(int $count): self
    {
        $this->count = $count;

        return $this;
    }

    public function server(): Server
    {
        return $this->server;
    }

    public function forServer(Server $server): self
    {
        $this->server = $server;

        return $this;
    }

    public function buildQueryFilter(QueryBuilder $qb): Query
    {
        $qb = $this->buildQueryBuilder($qb);
        $qb->setMaxResults($this->count());

        return parent::buildQueryFilter($qb);
    }

    public function buildPaginator(QueryBuilder $qb, int $itemsPerPage = self::DEFAULT_COUNT): Paginator
    {
        return parent::buildPaginator(
            $this->buildQueryBuilder($qb),
            self::NEWS_FILTER_LIMIT
        );
    }

    private function buildQueryBuilder(QueryBuilder $qb): QueryBuilder
    {
        $qb
            ->select('e as entity')
            ->where('e.server IS NULL')
            ->orWhere('e.server = :SERVER')
            ->orderBy('e.updatedAt', 'DESC')
            ->setParameter('SERVER', $this->server());
        return $qb;
    }
    protected function validate()
    {
        (new OptionsResolver())
            ->setDefined(
                [
                    'count',
                    'server',
                ]
            )
            ->setAllowedTypes('count', ['int', 'null'])
            ->setAllowedTypes('server', [Server::class])
            ->resolve(
                [
                    'count' => $this->count,
                    'server' => $this->server,
                ]
            );
    }
}