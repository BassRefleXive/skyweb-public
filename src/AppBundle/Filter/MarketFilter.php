<?php

namespace AppBundle\Filter;


use AppBundle\Doctrine\Type\MarketPriceTypeEnum;
use AppBundle\Entity\Application\MarketCategory;
use AppBundle\Entity\Application\Server;
use AppBundle\Entity\Application\UserServerAccount;
use AppBundle\Filter\Interfaces\FilterInterface;
use AppBundle\Filter\Interfaces\PaginatedFilterInterface;
use AppBundle\Repository\Traits\LikeQueryHelpers;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MarketFilter extends AbstractFilter implements FilterInterface, PaginatedFilterInterface
{
    use LikeQueryHelpers;

    const DEFAULT_COUNT = 17;
    const MARKET_FILTER_LIMIT = 17;

    private $level;
    private $option;
    private $skill;
    private $luck;
    private $excellent;
    private $ancient;
    private $harmony;
    private $pvp;
    private $price;
    private $priceType;
    private $category;
    private $server;
    private $count;
    private $title;
    private $owned;
    private $bought;
    private $userServerAccount;


    public function __construct(int $page)
    {
        $this->setPage($page);
    }

    static public function buildFromRequest(Request $request): MarketFilter
    {
        $filter = new static(
            (int) $request->get('page', PaginatedFilterInterface::DEFAULT_PAGE)
        );

        return $filter;
    }

    public function updateByRequest(Request $request): MarketFilter
    {
        return $this->setPage((int) $request->get('page', PaginatedFilterInterface::DEFAULT_PAGE));
    }

    public function getCount(): int
    {
        return $this->count ?? MarketFilter::DEFAULT_COUNT;
    }

    public function setCount(int $count): MarketFilter
    {
        $this->count = $count;

        return $this;
    }

    public function getLevel()
    {
        return $this->level;
    }

    public function setLevel(int $level = null): MarketFilter
    {
        $this->level = $level;

        return $this;
    }

    public function getOption()
    {
        return $this->option;
    }

    public function setOption(int $option = null): MarketFilter
    {
        $this->option = $option;

        return $this;
    }

    public function getSkill()
    {
        return $this->skill;
    }

    public function setSkill(bool $skill = null): MarketFilter
    {
        $this->skill = $skill;

        return $this;
    }

    public function getLuck()
    {
        return $this->luck;
    }

    public function setLuck(bool $luck = null): MarketFilter
    {
        $this->luck = $luck;

        return $this;
    }

    public function getOwned()
    {
        return $this->owned;
    }

    public function setOwned(bool $owned = null): self
    {
        $this->owned = $owned;

        return $this;
    }

    public function getBought()
    {
        return $this->bought;
    }

    public function setBought(bool $bought = null): self
    {
        $this->bought = $bought;

        return $this;
    }

    public function getExcellent()
    {
        return $this->excellent;
    }

    public function setExcellent(bool $excellent = null): MarketFilter
    {
        $this->excellent = $excellent;

        return $this;
    }

    public function getAncient()
    {
        return $this->ancient;
    }

    public function setAncient(bool $ancient = null): MarketFilter
    {
        $this->ancient = $ancient;

        return $this;
    }

    public function getHarmony()
    {
        return $this->harmony;
    }

    public function setHarmony(bool $harmony = null): MarketFilter
    {
        $this->harmony = $harmony;

        return $this;
    }

    public function getPvp()
    {
        return $this->pvp;
    }

    public function setPvp(bool $pvp = null): MarketFilter
    {
        $this->pvp = $pvp;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice(int $price = null): MarketFilter
    {
        $this->price = $price;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title = null): self
    {
        $this->title = $title;

        return $this;
    }

    public function getPriceType()
    {
        return $this->priceType;
    }

    public function setPriceType(string $priceType = null): MarketFilter
    {
        if ($priceType && !array_key_exists($priceType, MarketPriceTypeEnum::getTypes())) {
            throw new \InvalidArgumentException('Invalid Market price type.');
        }

        $this->priceType = $priceType;

        return $this;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory($category = null): MarketFilter
    {
        if ($category instanceof MarketCategory) {
            $this->category = $category->getId();
        } else {
            $this->category = $category;
        }

        return $this;
    }

    public function getServer()
    {
        return $this->server;
    }

    public function setServer(int $server): MarketFilter
    {
        $this->server = $server;

        return $this;
    }

    public function getUserServerAccount()
    {
        return $this->userServerAccount;
    }

    public function setUserServerAccount(UserServerAccount $userServerAccount = null): self
    {
        $this->userServerAccount = $userServerAccount;

        return $this;
    }

    public function buildPaginator(QueryBuilder $qb, int $itemsPerPage = self::DEFAULT_COUNT): Paginator
    {
        return parent::buildPaginator(
            $this->buildQueryBuilder($qb),
            $this->getCount()
        );
    }

    public function buildQueryFilter(QueryBuilder $qb): Query
    {
        $qb = $this->buildQueryBuilder($qb);

        return parent::buildQueryFilter($qb);
    }

    private function buildQueryBuilder(QueryBuilder $qb): QueryBuilder
    {
        $qb->where('e.server = :SERVER')->setParameter('SERVER', $this->server);

        if ($this->category) {
            $qb->join('e.itemInfo', 'ii')
                ->join('ii.marketCategory', 'mc')
                ->andWhere('mc.id = :MARKET_CATEGORY')
                ->setParameter('MARKET_CATEGORY', $this->category);
        }

        $this->level && $qb->andWhere('e.level = :LEVEL')->setParameter('LEVEL', $this->level);
        $this->option && $qb->andWhere('e.option = :OPTION')->setParameter('OPTION', $this->option * 4);
        $this->skill && $qb->andWhere('e.skill = :SKILL')->setParameter('SKILL', $this->skill);
        $this->luck && $qb->andWhere('e.luck = :LUCK')->setParameter('LUCK', $this->luck);
        $this->excellent && $qb->andWhere('e.excellent = :EXCELLENT')->setParameter('EXCELLENT', $this->excellent);
        $this->ancient && $qb->andWhere('e.ancient = :ANCIENT')->setParameter('ANCIENT', $this->ancient);
        $this->harmony && $qb->andWhere('e.harmony = :HARMONY')->setParameter('HARMONY', $this->harmony);
        $this->pvp && $qb->andWhere('e.pvp = :PVP')->setParameter('PVP', $this->pvp);
        $this->price && $qb->andWhere('e.price <= :PRICE')->setParameter('PRICE', $this->price);
        $this->priceType && $qb->andWhere('e.priceType = :PRICE_TYPE')->setParameter('PRICE_TYPE', $this->priceType);
        null !== $this->title && $qb->andWhere("e.title LIKE :title ESCAPE '!'")->setParameter('title', $this->makeLikeParam($this->title));

        if (null !== $this->owned && true === $this->owned && null !== $this->userServerAccount) {
            $qb->andWhere('e.userServerAccount = :userServerAccount')
                ->setParameter('userServerAccount', $this->userServerAccount);
        }

        if (null !== $this->bought && true === $this->bought && null !== $this->userServerAccount) {
            $qb->andWhere('e.boughtBy = :userServerAccount')
                ->setParameter('userServerAccount', $this->userServerAccount);
        } elseif (null !== $this->bought && false === $this->bought && null !== $this->userServerAccount) {
            $qb->andWhere('e.boughtBy IS NULL');
        }

        $qb->orderBy('e.createdAt', 'DESC');

        $qb->setMaxResults($this->getCount());

        return $qb;
    }

    protected function validate()
    {
        (new OptionsResolver())
            ->setDefined(
                [
                    'title',
                    'count',
                    'level',
                    'option',
                    'skill',
                    'luck',
                    'owned',
                    'bought',
                    'excellent',
                    'ancient',
                    'harmony',
                    'pvp',
                    'price',
                    'priceType',
                    'category',
                    'server',
                    'userServerAccount',
                ]
            )
            ->setAllowedTypes('title', ['string', 'null'])
            ->setAllowedTypes('count', ['int', 'null'])
            ->setAllowedTypes('level', ['int', 'null'])
            ->setAllowedTypes('option', ['int', 'null'])
            ->setAllowedTypes('skill', ['bool', 'null'])
            ->setAllowedTypes('luck', ['bool', 'null'])
            ->setAllowedTypes('owned', ['bool', 'null'])
            ->setAllowedTypes('bought', ['bool', 'null'])
            ->setAllowedTypes('excellent', ['bool', 'null'])
            ->setAllowedTypes('ancient', ['bool', 'null'])
            ->setAllowedTypes('harmony', ['bool', 'null'])
            ->setAllowedTypes('pvp', ['bool', 'null'])
            ->setAllowedTypes('price', ['int', 'null'])
            ->setAllowedTypes('priceType', ['string', 'null'])
            ->setAllowedTypes('category', ['int', 'null'])
            ->setAllowedTypes('server', ['int'])
            ->setAllowedTypes('userServerAccount', [UserServerAccount::class, 'null'])
            ->resolve(
                [
                    'title' => $this->title,
                    'count' => $this->count,
                    'level' => $this->level,
                    'option' => $this->option,
                    'skill' => $this->skill,
                    'luck' => $this->luck,
                    'owned' => $this->owned,
                    'bought' => $this->bought,
                    'excellent' => $this->excellent,
                    'ancient' => $this->ancient,
                    'harmony' => $this->harmony,
                    'pvp' => $this->pvp,
                    'price' => $this->price,
                    'priceType' => $this->priceType,
                    'category' => $this->category,
                    'server' => $this->server,
                    'userServerAccount' => $this->userServerAccount,
                ]
            );
    }
}