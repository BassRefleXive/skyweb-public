<?php

namespace AppBundle\Filter;


use AppBundle\Filter\Interfaces\FilterInterface;
use AppBundle\Filter\Interfaces\PaginatedFilterInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\Tools\Pagination\Paginator;

class GuildMemberFilter extends AbstractFilter implements FilterInterface, PaginatedFilterInterface
{
    const DEFAULT_COUNT = 15;
    const FILTER_LIMIT = 15;

    private $count;
    private $name;

    public function __construct(int $page)
    {
        $this->setPage($page);
    }

    static public function buildFromRequest(Request $request): self
    {
        return new static(
            (int) $request->get('page', PaginatedFilterInterface::DEFAULT_PAGE)
        );
    }

    public function withName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCount()
    {
        return $this->count ?? GuildMemberFilter::DEFAULT_COUNT;
    }

    public function setCount(int $count): self
    {
        $this->count = $count;

        return $this;
    }

    public function buildPaginator(QueryBuilder $qb, int $itemsPerPage = self::DEFAULT_COUNT): Paginator
    {
        return parent::buildPaginator(
            $this->buildQueryBuilder($qb),
            self::FILTER_LIMIT
        );
    }

    public function buildQueryFilter(QueryBuilder $qb): Query
    {
        $qb = $this->buildQueryBuilder($qb);

        return parent::buildQueryFilter($qb);
    }

    private function buildQueryBuilder(QueryBuilder $qb): QueryBuilder
    {
        $qb->select('e AS entity')
            ->addSelect('guild')
            ->addSelect('character')
            ->addSelect('account')
            ->leftJoin('e.guild', 'guild')
            ->leftJoin('e.character', 'character')
            ->leftJoin('character.account', 'account')
            ->where('guild.name = :guild_name')
            ->setParameter('guild_name', $this->name)
            ->setMaxResults($this->getCount());

        return $qb;
    }

    protected function validate()
    {
        (new OptionsResolver())
            ->setDefined(
                [
                    'count',
                    'name',
                ]
            )
            ->setAllowedTypes('count', ['int', 'null'])
            ->setAllowedTypes('name', ['string'])
            ->resolve(
                [
                    'count' => $this->count,
                    'name' => $this->name,
                ]
            );
    }
}