<?php

declare(strict_types = 1);

namespace AppBundle\Filter;


use AppBundle\Filter\Interfaces\PaginatedFilterInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PrisonFilter extends AbstractFilter implements PaginatedFilterInterface
{
    const DEFAULT_COUNT = 50;
    const PRISON_FILTER_LIMIT = 50;

    private $count;

    public function __construct(int $page)
    {
        $this->setPage($page);
    }

    static public function buildFromRequest(Request $request): self
    {
        $filter = new static((int) $request->get('page', PaginatedFilterInterface::DEFAULT_PAGE));

        return $filter;
    }

    public function count()
    {
        return $this->count ?? self::DEFAULT_COUNT;
    }

    public function witCount(int $count): self
    {
        $this->count = $count;

        return $this;
    }

    public function buildQueryFilter(QueryBuilder $qb): Query
    {
        $qb = $this->buildQueryBuilder($qb);
        $qb->setMaxResults($this->count());

        return parent::buildQueryFilter($qb);
    }

    public function buildPaginator(QueryBuilder $qb, int $itemsPerPage = self::DEFAULT_COUNT): Paginator
    {
        return parent::buildPaginator(
            $this->buildQueryBuilder($qb),
            self::PRISON_FILTER_LIMIT
        );
    }

    private function buildQueryBuilder(QueryBuilder $qb): QueryBuilder
    {
        $qb
            ->select('e')
            ->where('e.expireAt > :now')
            ->orderBy('e.expireAt', 'ASC')
            ->groupBy('e.user')
            ->setParameter('now', new \DateTime());

        return $qb;
    }
    protected function validate()
    {
        (new OptionsResolver())
            ->setDefined(
                [
                    'count',
                ]
            )
            ->setAllowedTypes('count', ['int', 'null'])
            ->resolve(
                [
                    'count' => $this->count,
                ]
            );
    }
}