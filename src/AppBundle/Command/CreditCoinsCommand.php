<?php


namespace AppBundle\Command;

use AppBundle\Entity\Application\Server;
use AppBundle\Repository\Application\ServerRepository;
use AppBundle\Service\MuOnline\Funds\CoinsService;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Iterator\RecursiveDirectoryIterator;


class CreditCoinsCommand extends ContainerAwareCommand
{
    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var ServerRepository
     */
    private $serverRepository;

    /**
     * @var CoinsService
     */
    private $coinsService;

    protected function configure()
    {
        $this
            ->setName('app:coins:credit')
            ->addArgument('server_id', InputArgument::REQUIRED)
            ->addArgument('data_files_dir', InputArgument::REQUIRED)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initContainer();

        $dataFilesDirectory = $input->getArgument('data_files_dir');
        $serverId = (int) $input->getArgument('server_id');

        $server = $this->serverRepository->find($serverId);

        if (!$server instanceof Server) {
            $this->logger->emergency('Cannot determine server with given id.');
        } else {
            foreach (new RecursiveDirectoryIterator($dataFilesDirectory, RecursiveDirectoryIterator::SKIP_DOTS) as $file) {
                if ('txt' !== $file->getExtension()) {
                    continue;
                }

                $contents = file_get_contents($file);

                $accountsToCredit = explode(PHP_EOL, $contents);

                foreach ($accountsToCredit as $accountData) {
                    $data = explode(' ', preg_replace('/\s+/', ' ',$accountData));

                    if (2 !== count($data)) {
                        $this->logger->emergency(
                            sprintf(
                                'Failed to credit WCoin to account with row data: "%s". File: "%s".',
                                $accountData,
                                $file
                            )
                        );

                        continue;
                    }

                    $account = trim($data[0]);
                    $coinsToAdd = (int) trim($data[1]);

                    $this->logger->info(
                        sprintf('Crediting account "%s" with %d WCoinC', $account, $coinsToAdd)
                    );

                    $this->coinsService->add($account, $coinsToAdd, $server);
                }
            }
        }
    }

    private function initContainer()
    {
        $container = $this->getContainer();

        $this->coinsService = $container->get('sky_app.muo.service.funds.coins');
        $this->logger = $container->get('logger');
        $this->serverRepository = $container->get('sky_app.app.repository.server');
    }
}