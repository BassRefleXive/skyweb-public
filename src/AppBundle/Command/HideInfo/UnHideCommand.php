<?php

declare(strict_types = 1);

namespace AppBundle\Command\HideInfo;

use AppBundle\Service\Application\AccountService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;

class UnHideCommand extends Command
{
    private $service;

    public function __construct(AccountService $service)
    {
        parent::__construct(null);

        $this->service = $service;
    }

    protected function configure()
    {
        $this
            ->setName('app:user:accounts:unhide-expired')
            ->setDescription('Unhide accounts with expired hidden period.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Unhide accounts');

        $stopwatch = new Stopwatch();
        $stopwatch->start($this->getName());

        $event = $stopwatch->stop($this->getName());

        $count = $this->service->unHideExpiredAccounts();

        $io->newLine();
        $io->success(
            sprintf(
                'Hide status removed for %d accounts. Done in %.3f seconds, %.3f MB memory used.',
                $count,
                $event->getDuration() / 1000,
                $event->getMemory() / 1024 / 1024
            )
        );
    }
}