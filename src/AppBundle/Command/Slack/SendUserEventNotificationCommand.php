<?php


namespace AppBundle\Command\Slack;

use AppBundle\Entity\Application\Payments\Order;
use AppBundle\Entity\Application\User\User;
use AppBundle\Repository\Application\Payments\OrderRepository;
use AppBundle\Repository\Application\UserRepository;
use AppBundle\Slack\Services\PaymentNotificationService;
use AppBundle\Slack\Services\UserEventNotificationService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SendUserEventNotificationCommand extends Command
{
    private $service;
    private $userRepository;

    public function __construct(UserEventNotificationService $service, UserRepository $userRepository)
    {
        parent::__construct();

        $this->service = $service;
        $this->userRepository = $userRepository;
    }

    protected function configure()
    {
        $this
            ->setName('app:slack:send-user-event-notification')
            ->setDescription('Send user event notification command to Slack.')
            ->addArgument('user', InputArgument::REQUIRED, 'User id.')
            ->addOption('type', 't', InputOption::VALUE_REQUIRED, 'Notification type. 0 - registered.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**
         * @var User $user
         */
        $user = $this->userRepository->find($input->getArgument('user'));

        switch ((int)$input->getOption('type')) {
            case 0: {
                $this->service->registered($user);
                break;
            }
        }

    }
}