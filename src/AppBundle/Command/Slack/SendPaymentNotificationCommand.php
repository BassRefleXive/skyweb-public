<?php


namespace AppBundle\Command\Slack;

use AppBundle\Entity\Application\Payments\Order;
use AppBundle\Enum\Application\PaymentSystem;
use AppBundle\Repository\Application\Payments\OrderRepository;
use AppBundle\Slack\Services\PaymentNotificationService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SendPaymentNotificationCommand extends Command
{
    private $service;
    private $orderRepository;

    public function __construct(PaymentNotificationService $service, OrderRepository $orderRepository)
    {
        parent::__construct();

        $this->service = $service;
        $this->orderRepository = $orderRepository;
    }

    protected function configure()
    {
        $this
            ->setName('app:slack:send-payment-notification')
            ->setDescription('Send payment notification command to Slack.')
            ->addArgument('order', InputArgument::REQUIRED, 'Order id.')
            ->addArgument('payment_system', InputArgument::REQUIRED, 'Order id.')
            ->addOption('type', 't', InputOption::VALUE_REQUIRED, 'Notification type. 0 - started, 1 - success, 2 - pending, 3 - failure, 4 - credited.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**
         * @var Order $order
         */
        $order = $this->orderRepository->find($input->getArgument('order'));

        switch ((int)$input->getOption('type')) {
            case 0: {
                $this->service->paymentStarted(PaymentSystem::byValue($input->getArgument('payment_system')), $order);
                break;
            }
            case 1: {
                $this->service->paymentSuccess(PaymentSystem::byValue($input->getArgument('payment_system')), $order);
                break;
            }
            case 2: {
                $this->service->paymentPending(PaymentSystem::byValue($input->getArgument('payment_system')), $order);
                break;
            }
            case 3: {
                $this->service->paymentFailure(PaymentSystem::byValue($input->getArgument('payment_system')), $order);
                break;
            }
            case 4: {
                $this->service->paymentCredited(PaymentSystem::byValue($input->getArgument('payment_system')), $order);
                break;
            }
        }

    }
}