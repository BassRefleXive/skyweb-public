<?php

declare(strict_types = 1);

namespace AppBundle\Command\Prison;

use AppBundle\Service\Application\Manager\UserManager\BanManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;

class UnBanCommand extends Command
{
    private $manager;

    public function __construct(BanManager $manager)
    {
        parent::__construct(null);

        $this->manager = $manager;
    }

    protected function configure()
    {
        $this
            ->setName('app:user:unban-expired')
            ->setDescription('Unban users which has been banned but ban is expired.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('UnBan expired bans');

        $stopwatch = new Stopwatch();
        $stopwatch->start($this->getName());

        $count = $this->manager->unBanUsersWithExpiredBans();

        $event = $stopwatch->stop($this->getName());

        $io->newLine();
        $io->success(
            sprintf(
                'Unbanned %d users. Done in %.3f seconds, %.3f MB memory used.',
                $count,
                $event->getDuration() / 1000,
                $event->getMemory() / 1024 / 1024
            )
        );
    }
}