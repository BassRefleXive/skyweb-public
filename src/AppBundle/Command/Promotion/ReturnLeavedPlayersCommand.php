<?php

declare(strict_types = 1);


namespace AppBundle\Command\Promotion;


use AppBundle\Promotion\ReturnLeavedPlayers\SenderService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ReturnLeavedPlayersCommand extends Command
{
    private $service;

    public function __construct(SenderService $service)
    {
        parent::__construct();

        $this->service = $service;
    }

    protected function configure()
    {
        $this->setName('app:promotion:email:return-leaved-players');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->service->execute();
    }
}