<?php


namespace AppBundle\Command\Promotion;

use AppBundle\Promotion\OBTInvitation\MessagesSender;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OBTInvitationPromotionCommand extends Command
{
    private $sender;

    public function __construct(MessagesSender $sender)
    {
        parent::__construct();

        $this->sender = $sender;
    }

    protected function configure()
    {
        $this
            ->setName('app:promotion:email:obt-invitation')
            ->setDescription('Send OBT invitation promotion email');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->sender->send();
    }
}