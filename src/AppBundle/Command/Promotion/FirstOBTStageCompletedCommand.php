<?php

declare(strict_types=1);

namespace AppBundle\Command\Promotion;

use AppBundle\Promotion\UserPlainEmailSender;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class FirstOBTStageCompletedCommand extends Command
{
    private $sender;

    public function __construct(UserPlainEmailSender $sender)
    {
        parent::__construct();

        $this->sender = $sender;
    }

    protected function configure()
    {
        $this
            ->setName('app:promotion:email:first-obt-stage-completed')
            ->addOption('debug', null, InputOption::VALUE_OPTIONAL, '', false)
            ->setDescription('Send First OBT Stage Completed promotion email');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->sender->send(
            'email.second_obt_stage.0',
            '@App/email/other/first_obt_stage_completed.html.twig',
            (bool) $input->getOption('debug')
        );
    }
}