<?php

declare(strict_types = 1);

namespace AppBundle\Command\WebShop;

use AppBundle\Entity\Application\Server;
use AppBundle\Entity\Application\WebShopCategory;
use AppBundle\Entity\Application\WebShopCategoryItem;
use AppBundle\Entity\Application\WebShopConfig;
use AppBundle\Repository\Application\ServerRepository;
use Doctrine\ORM\EntityManager;
use PhpCollection\Sequence;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MigrateWebShopDataCommand extends Command
{
    private $em;
    private $serverRepository;

    public function __construct(EntityManager $em, ServerRepository $serverRepository)
    {
        parent::__construct();

        $this->em = $em;
        $this->serverRepository = $serverRepository;
    }

    protected function configure()
    {
        $this
            ->setName('app:web-shop:migrate-data')
            ->setDescription('Migrate all webshop categories with items from specified server to all others.')
            ->addArgument('source_server_id', InputArgument::REQUIRED, 'Source Server ID');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Server $sourceServer */
        $sourceServer = $this->serverRepository->find($input->getArgument('source_server_id'));

        if (null === $sourceServer) {
            throw new \InvalidArgumentException(sprintf('Server with ID #%d does not exists.', $input->getArgument('source_server_id')));
        }

        (new Sequence($this->serverRepository->findAll()))
            ->filter(function (Server $server) use ($sourceServer): bool {
                return $server->getId() !== $sourceServer->getId();
            })
            ->map(function (Server $server): Server {
                $this->clearServerWebShopCategories($server);

                return $server;
            })
            ->map(function (Server $server) use ($sourceServer): Server {
                $this->copyCategories($sourceServer->getWebShopConfig(), $server->getWebShopConfig());

                return $server;
            })
            ->map(function (Server $server) use ($sourceServer): Server {
                $this->copyConfig($sourceServer->getWebShopConfig(), $server->getWebShopConfig());

                return $server;
            });
    }

    private function copyConfig(WebShopConfig $source, WebShopConfig $destination)
    {
        $destination
            ->setMaxItemLevel($source->getMaxItemLevel())
            ->setMaxItemOption($source->getMaxItemOption())
            ->setMaxExcCount($source->getMaxExcCount())
            ->setLevelPriceAddMoney($source->getLevelPriceAddMoney())
            ->setLevelPriceAddCredits($source->getLevelPriceAddCredits())
            ->setOptionPriceAddMoney($source->getOptionPriceAddMoney())
            ->setOptionPriceAddCredits($source->getOptionPriceAddCredits())
            ->setSkillPriceMultiplier($source->getSkillPriceMultiplier())
            ->setLuckPriceMultiplier($source->getLuckPriceMultiplier())
            ->setExcellentPriceMultiplier($source->getExcellentPriceMultiplier())
            ->setAncientPriceMultiplier($source->getAncientPriceMultiplier())
            ->setHarmonyPriceMultiplier($source->getHarmonyPriceMultiplier())
            ->setPvpPriceMultiplier($source->getPvpPriceMultiplier());

        $this->em->persist($destination);
        $this->em->flush($destination);
    }

    private function copyCategory(WebShopCategory $source, WebShopConfig $destination, WebShopCategory $parent = null)
    {
        $destinationRoot = (new WebShopCategory($destination))
            ->setName($source->getName())
            ->setStatus($source->getStatus())
            ->setPosition($source->position());

        $destinationRoot->setItems(
            $source->getItems()->map(function (WebShopCategoryItem $item) use ($destinationRoot): WebShopCategoryItem {
                $item = clone $item;
                $item->setWebShopCategory($destinationRoot);

                return $item;
            })
        );

        if (null !== $parent) {
            $destinationRoot->setParentCategory($parent);
        }

        $destination->addCategory($destinationRoot);

        if ($source->childCategories()->count()) {
            $source->childCategories()->map(function (WebShopCategory $category) use ($destination, $destinationRoot) {
                $this->copyCategory($category, $destination, $destinationRoot);
            });
        }
    }

    private function copyCategories(WebShopConfig $source, WebShopConfig $destination)
    {
        $source->rootCategories()->map(function (WebShopCategory $root) use ($destination) {
            $this->copyCategory($root, $destination);

            $this->em->persist($destination);
            $this->em->flush($destination);
        });
    }

    private function clearServerWebShopCategories(Server $server)
    {
        $config = $server->getWebShopConfig();

        $config->getCategories()->map(function (WebShopCategory $category) use ($config) {
            $config->removeCategory($category);
        });

        $this->em->persist($config);
        $this->em->flush($config);
    }
}