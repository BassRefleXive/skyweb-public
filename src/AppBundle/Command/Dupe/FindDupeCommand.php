<?php

declare(strict_types = 1);

namespace AppBundle\Command\Dupe;

use AppBundle\Doctrine\Type\Mappings\Items\Item;
use AppBundle\Entity\Application\User\User;
use AppBundle\Entity\Application\UserServerAccount;
use AppBundle\Entity\MuOnline\Account;
use AppBundle\Entity\MuOnline\Character;
use AppBundle\Repository\Application\UserRepository;
use AppBundle\Service\MuOnline\AccountService;
use AppBundle\Service\MuOnline\Item\OptionsParserChain;
use AppBundle\Service\MuOnline\WarehouseService;
use AppBundle\Slack\Services\DupeNotificationService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FindDupeCommand extends Command
{
    private $itemParser;
    private $accountService;
    private $userRepository;
    private $warehouseService;
    private $serverItems;
    private $dupeNotificationService;

    public function __construct(
        OptionsParserChain $itemParser,
        AccountService $accountService,
        UserRepository $userRepository,
        WarehouseService $warehouseService,
        DupeNotificationService $dupeNotificationService
    )
    {
        parent::__construct();

        $this->itemParser = $itemParser;
        $this->accountService = $accountService;
        $this->userRepository = $userRepository;
        $this->warehouseService = $warehouseService;
        $this->dupeNotificationService = $dupeNotificationService;
    }

    protected function configure()
    {
        $this
            ->setName('app:dupe:finder')
            ->setDescription('Find duped items.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $users = $this->userRepository->findAll();

        foreach ($users as $user) {
            $this->checkUser($user, $output);
        }

        $foundDuplicates = [];

        foreach ($this->serverItems as $serverId => $serverItems) {

            foreach ($serverItems as $serial => $items) {
                if (count($items) > 1) {
                    ksort($items);

                    foreach ($items as $itemInfo) {
                        /** @var Item $item */
                        $item = $itemInfo['item'];
                        /** @var UserServerAccount $userServerAccount */
                        $userServerAccount = $itemInfo['userServerAccount'];
                        /** @var Character|null $character */
                        $character = $itemInfo['character'];

                        if (null !== $character) {
                            $foundDuplicates[] = sprintf(
                                'DUPE: S: %s; U: %s; C: %s; P: CHAR; I: %s; S: %s;',
                                $userServerAccount->getServer()->getName(),
                                $userServerAccount->getUser()->getDisplayName(),
                                $character->getName(),
                                $item->getItemInfo()->getName(),
                                sprintf('%0' . 8 . 'X', $item->getGeneralOptions()->getSerialNumber()->getFirst() . str_pad('', 8, '0'))
                            );
                        } else {
                            $foundDuplicates[] = sprintf(
                                'DUPE: S: %s; U: %s; P: WH; I: %s; S: %s;',
                                $userServerAccount->getServer()->getName(),
                                $userServerAccount->getUser()->getDisplayName(),
                                $item->getItemInfo()->getName(),
                                sprintf('%0' . 8 . 'X', $item->getGeneralOptions()->getSerialNumber()->getFirst() . str_pad('', 8, '0'))
                            );
                        }
                    }
                }
            }
        }

        if (!count($foundDuplicates)) {
            $this->dupeNotificationService->checkFinishedWithoutIssues();
        } else {
            $this->dupeNotificationService->duplicatedItemsFound($foundDuplicates);
        }
    }

    private function checkUser(User $user, OutputInterface $output): void
    {
        foreach ($user->getUserServerAccounts() as $userServerAccount) {
            $account = $this->accountService->getAccountByUser($user, $userServerAccount->getServer());

            if (null !== $account) {
                try {
                    $this->checkUserAccount($userServerAccount, $account, $output);
                } catch (\Throwable $e) {}

                foreach ($account->getCharacters() as $character) {
                    try {
                        $this->checkCharacter($userServerAccount, $character, $output);
                    } catch (\Throwable $e) {}
                }
            }
        }
    }

    private function checkUserAccount(UserServerAccount $userServerAccount, Account $account, OutputInterface $output): void
    {
        $warehouseItems = $this->warehouseService->getParsedItems($account);

        foreach ($warehouseItems as $item) {
            if (!$item->isEmpty()) {
                $this->collectItems($userServerAccount, $item);
            }
        }
    }

    private function checkCharacter(UserServerAccount $userServerAccount, Character $character, OutputInterface $output): void
    {
        /** @var Item $item */
        foreach ($character->completeInventory() as $item) {
            $parsed = $this->itemParser->parse($item);

            if (!$parsed->isEmpty()) {
                $this->collectItems($userServerAccount, $parsed, $character);
            }
        }
    }

    private function collectItems(UserServerAccount $userServerAccount, Item $item, Character $character = null): bool
    {
        if (0 === $serial = $item->getGeneralOptions()->getSerialNumber()->getFirst()) {
            return false;
        }

        $this->serverItems[$userServerAccount->getServer()->getId()][$serial][] = [
            'item' => $item,
            'userServerAccount' => $userServerAccount,
            'character' => $character,
        ];

        return count($this->serverItems[$userServerAccount->getServer()->getId()][$serial]) > 1;
    }
}