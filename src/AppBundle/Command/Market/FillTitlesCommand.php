<?php

declare(strict_types = 1);

namespace AppBundle\Command\Market;

use AppBundle\Doctrine\Type\Mappings\Items\Item;
use AppBundle\Entity\Application\MarketItem;
use AppBundle\Repository\Application\MarketItemRepository;
use AppBundle\Service\MuOnline\Item\OptionsParserChain;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;

class FillTitlesCommand extends Command
{
    private $marketItemRepository;
    private $itemParser;

    public function __construct(MarketItemRepository $marketItemRepository, OptionsParserChain $itemParser)
    {
        parent::__construct(null);

        $this->marketItemRepository = $marketItemRepository;
        $this->itemParser = $itemParser;
    }

    protected function configure()
    {
        $this
            ->setName('app:market:item:fill-titles')
            ->setDescription('Fill all market items titles.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Fill Titles');

        $stopwatch = new Stopwatch();
        $stopwatch->start($this->getName());

        $event = $stopwatch->stop($this->getName());

        $items = $this->marketItemRepository->findAll();

        /** @var MarketItem $item $item */
        foreach ($items as $item) {
            $parsed = $this->itemParser->parse(Item::createExisting()->setHex($item->getHex()));

            $item->setTitle($parsed->getItemInfo()->getName());

            $this->marketItemRepository->persist($item);
        }

        $this->marketItemRepository->flush();

        $count = count($items);

        $io->newLine();
        $io->success(
            sprintf(
                'Filled %d titles. Done in %.3f seconds, %.3f MB memory used.',
                $count,
                $event->getDuration() / 1000,
                $event->getMemory() / 1024 / 1024
            )
        );
    }
}