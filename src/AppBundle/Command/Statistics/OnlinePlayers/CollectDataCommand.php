<?php


namespace AppBundle\Command\Statistics\OnlinePlayers;

use AppBundle\Statistics\OnlinePlayers\Service\DataPersister;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CollectDataCommand extends Command
{
    private $persister;

    public function __construct(DataPersister $persister)
    {
        parent::__construct();

        $this->persister = $persister;
    }

    protected function configure()
    {
        $this
            ->setName('app:statistics:online-players:collect')
            ->setDescription('Saves current online players for all servers in database.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->persister->collectAndSave();
    }
}