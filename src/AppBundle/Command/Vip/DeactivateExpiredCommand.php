<?php

namespace AppBundle\Command\Vip;

use AppBundle\Service\Application\AccountService;
use AppBundle\Service\Application\VipService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;

class DeactivateExpiredCommand extends Command
{
    private $service;

    public function __construct(VipService $service)
    {
        parent::__construct(null);

        $this->service = $service;
    }

    protected function configure()
    {
        $this
            ->setName('app:vip:deactivate-expired')
            ->setDescription('Deactivate VIP status on accounts with expired VIP status.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Deactivate Expired VIP');

        $stopwatch = new Stopwatch();
        $stopwatch->start($this->getName());

        $event = $stopwatch->stop($this->getName());

        $count = $this->service->deactivateExpired();

        $io->newLine();
        $io->success(
            sprintf(
                'VIP status removed for %d accounts. Done in %.3f seconds, %.3f MB memory used.',
                $count,
                $event->getDuration() / 1000,
                $event->getMemory() / 1024 / 1024
            )
        );
    }
}