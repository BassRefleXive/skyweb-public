<?php

declare(strict_types = 1);


namespace AppBundle\Command\Votes;


use AppBundle\Repository\Application\VoteRepository;
use AppBundle\Service\MuOnline\Funds\CoinsService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;

class CreditVotesRewardInPeriodCommand extends Command
{
    private $voteRepository;
    private $coinsService;

    public function __construct(VoteRepository $voteRepository, CoinsService $coinsService)
    {
        parent::__construct();

        $this->voteRepository = $voteRepository;
        $this->coinsService = $coinsService;
    }

    protected function configure()
    {
        $this
            ->setName('app:vote:credit-reward-in-period')
            ->setDescription('Add reward for votes in given period.')
            ->addArgument('start-date', InputArgument::REQUIRED)
            ->addArgument('end-date', InputArgument::REQUIRED)
            ->addOption('excluded-users', null, InputOption::VALUE_OPTIONAL, '', null)
            ->addOption('dry-run', null, InputOption::VALUE_OPTIONAL, '', false);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Update item equipment settings');

        $stopwatch = new Stopwatch();
        $stopwatch->start($this->getName());

        $startDate = new \DateTimeImmutable($input->getArgument('start-date'));
        $endDate = new \DateTimeImmutable($input->getArgument('end-date'));
        $dryRun = (bool) $input->getOption('dry-run');

        if (null !== $excludedUsers = $input->getOption('excluded-users')) {
            $excludedUsers = array_map('intval', explode(',', $excludedUsers));
        }

        $votes = $this->voteRepository->findVotesInPeriod($startDate, $endDate);

        $coinsToAdd = [];

        foreach ($votes as $vote) {
            if (null !== $excludedUsers && in_array($vote->getVoter()->getUser()->getId(), $excludedUsers, true)) {
                continue;
            }

            if (!isset($coinsToAdd[$vote->getVoter()->getUser()->getUsername()])) {
                $coinsToAdd[$vote->getVoter()->getUser()->getUsername()] = 0;
            }

            $coinsToAdd[$vote->getVoter()->getUser()->getUsername()] += $vote->provider()->reward(
                $vote->getType(),
                $vote->getVoter()->getServer()->getVoteRewardConfig()
            );
        }

        if (!$dryRun) {
            foreach ($coinsToAdd as $account => $coins) {
                $this->coinsService->add((string) $account, $coins);
            }
        }

        $event = $stopwatch->stop($this->getName());

        $io->newLine();
        $io->success(
            sprintf(
                'Done in %.3f seconds, %.3f MB memory used.',
                $event->getDuration() / 1000,
                $event->getMemory() / 1024 / 1024
            )
        );
    }
}