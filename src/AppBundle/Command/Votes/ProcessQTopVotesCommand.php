<?php

namespace AppBundle\Command\Votes;

use AppBundle\Entity\Application\Server;
use AppBundle\Repository\Application\ServerRepository;
use AppBundle\Service\Application\Votes\Processor\QTopVotesProcessor;
use Collection\Sequence;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProcessQTopVotesCommand extends Command
{
    private $votesProcessor;
    private $serverRepository;

    public function __construct(QTopVotesProcessor $votesProcessor, ServerRepository $serverRepository)
    {
        parent::__construct();

        $this->votesProcessor = $votesProcessor;
        $this->serverRepository = $serverRepository;
    }

    protected function configure()
    {
        $this
            ->setName('app:vote:process-qtop')
            ->setDescription('Add vote reward for mmotop votes.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        (new Sequence($this->serverRepository->findAll()))
            ->filter(function (Server $server) {
                return $server->getVoteRewardConfig() !== null && $server->getVoteRewardConfig()->getQtopVotesFileUrl() !== null;
            })
            ->map(function (Server $server) {
                $this->votesProcessor->process($server);
            });
    }
}