<?php

declare(strict_types=1);

namespace AppBundle\Command\Votes;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Service\Application\Votes\Processor\TopServers200\TopServer200Processor;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;

class ProcessTopServers200VotesCommand extends Command
{
    private $processor;

    public function __construct(TopServer200Processor $processor)
    {
        parent::__construct();

        $this->processor = $processor;
    }

    protected function configure()
    {
        $this
            ->setName('app:vote:process:topservers200')
            ->setDescription('Add vote reward for topservers200 votes.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Process TopServers200 Votes');

        $stopwatch = new Stopwatch();
        $stopwatch->start($this->getName());

        $this->processor->process();

        $event = $stopwatch->stop($this->getName());

        $io->newLine();
        $io->success(
            sprintf(
                'Done in %.3f seconds, %.3f MB memory used.',
                $event->getDuration() / 1000,
                $event->getMemory() / 1024 / 1024
            )
        );
    }
}