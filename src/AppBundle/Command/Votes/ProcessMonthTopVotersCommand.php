<?php

declare(strict_types = 1);

namespace AppBundle\Command\Votes;


use AppBundle\Entity\Application\Server;
use AppBundle\Repository\Application\ServerRepository;
use AppBundle\Service\Application\Votes\VoterService;
use Collection\Sequence;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;

class ProcessMonthTopVotersCommand extends Command
{
    private $voterService;
    private $serverRepository;

    public function __construct(VoterService $voterService, ServerRepository $serverRepository)
    {
        parent::__construct();

        $this->voterService = $voterService;
        $this->serverRepository = $serverRepository;
    }

    protected function configure()
    {
        $this
            ->setName('app:vote:process-months-top')
            ->setDescription('Process months top voters.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Process monthly Voters Top');

        $stopwatch = new Stopwatch();
        $stopwatch->start($this->getName());

        $this->process();

        $event = $stopwatch->stop($this->getName());

        $io->newLine();
        $io->success(
            sprintf(
                'Done in %.3f seconds, %.3f MB memory used.',
                $event->getDuration() / 1000,
                $event->getMemory() / 1024 / 1024
            )
        );
    }

    private function process(): void
    {
        (new Sequence($this->serverRepository->findAll()))
            ->filter(function (Server $server) {
                return $server->getVoteRewardConfig() !== null;
            })
            ->map(function (Server $server) {
                $this->voterService->processMonthlyVotersTop($server);
            });
    }
}