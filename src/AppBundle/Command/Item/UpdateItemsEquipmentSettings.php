<?php


namespace AppBundle\Command\Item;

use AppBundle\Entity\Application\Items\ItemInfo;
use AppBundle\Enum\MuOnline\CharacterClass;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;

class UpdateItemsEquipmentSettings extends Command
{
    private $entityManager;
    private $itemInfoRepository;
    private $characterClassRepository;

    public function __construct(EntityManager $entityManager)
    {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->itemInfoRepository = $entityManager->getRepository(ItemInfo::class);
        $this->characterClassRepository = $entityManager->getRepository(\AppBundle\Entity\Application\CharacterClass::class);
    }

    protected function configure()
    {
        $this->setName('app:item:update-items-equipment-settings')
            ->addArgument('config', InputArgument::REQUIRED, 'XML File with configuration.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Update item equipment settings');

        $stopwatch = new Stopwatch();
        $stopwatch->start($this->getName());

        $this->process($io, file_get_contents($input->getArgument('config')));

        $event = $stopwatch->stop($this->getName());

        $io->newLine();
        $io->success(
            sprintf(
                'Done in %.3f seconds, %.3f MB memory used.',
                $event->getDuration() / 1000,
                $event->getMemory() / 1024 / 1024
            )
        );
    }

    private function process(SymfonyStyle $io, string $xml)
    {
        $config = new \SimpleXMLElement($xml);
        $items = $this->itemInfoRepository->findAll();
        $this->entityManager->getConnection()->exec('DELETE FROM character_class_items;');

        foreach ($config as $section) {
            $sectionAttributes = $section->attributes();

            foreach ($section as $item) {
                $attributes = $item->attributes();
                $attributesArray = current($attributes);

                $equipmentKeys = ['DarkWizard', 'DarkKnight', 'FairyElf', 'MagicGladiator', 'DarkLord', 'Summoner', 'RageFighter'];

                if (count(array_intersect($equipmentKeys, array_keys($attributesArray))) !== count($equipmentKeys)) {
                    break;
                }

                $ourItem = $this->getMatchedItem($items, (int)$sectionAttributes->Index, (int)$attributes->Index);

                if (null === $ourItem) {
                    $io->write("\n\n\n!!!WARNING!!!\t\t\t!!!SKIPPED ITEM!!!\n\n\n");

                    break;
                }

                foreach ($equipmentKeys as $class) {
                    if (!isset($attributesArray[$class])) {
                        continue;
                    }

                    $class = $this->getClassByAttribute($class, $attributesArray[$class]);

                    if (0 === $class) {
                        continue;
                    }

                    $characterClass = $this->characterClassRepository->findOneBy(['type' => $class]);

                    if (null === $characterClass) {
                        $io->write(sprintf("\n\n\n!!!WARNING!!!\t\t\t!!!CHARACTER CLASS %d NOT FOUND!!!\n\n\n", $class));

                        continue;
                    }

                    $io->writeln(sprintf('%s can be equipped by %s', $ourItem->getName(), $characterClass->getName()));

                    $ourItem->addEquippedBy($characterClass);
                }

                $this->itemInfoRepository->persist($ourItem);
            }
        }

        $this->itemInfoRepository->flush();
    }

    public function getClassByAttribute(string $name, int $value): int
    {
        return [
                'DarkWizard'     => [
                    1 => CharacterClass::DARK_WIZARD,
                    2 => CharacterClass::SOUL_MASTER,
                    3 => CharacterClass::GRAND_MASTER,
                ],
                'DarkKnight'     => [
                    1 => CharacterClass::DARK_KNIGHT,
                    2 => CharacterClass::BLADE_KNIGHT,
                    3 => CharacterClass::BLADE_MASTER,
                ],
                'FairyElf'       => [
                    1 => CharacterClass::ELF,
                    2 => CharacterClass::MUSE_ELF,
                    3 => CharacterClass::HIGH_ELF,
                ],
                'MagicGladiator' => [
                    1 => CharacterClass::MAGIC_GLADIATOR,
                    3 => CharacterClass::DIMENSION_MASTER,
                ],
                'DarkLord'       => [
                    1 => CharacterClass::DARK_LORD,
                    3 => CharacterClass::LORD_EMPEROR,
                ],
                'Summoner'       => [
                    1 => CharacterClass::SUMMONER,
                    2 => CharacterClass::BLOODY_SUMMONER,
                    3 => CharacterClass::DIMENSION_MASTER,
                ],
                'RageFighter'    => [
                    1 => CharacterClass::RAGE_FIGHTER,
                    3 => CharacterClass::FIST_MASTER,
                ],
            ][$name][$value] ?? 0;
    }

    public function getMatchedItem(array $items, int $group, int $id): ?ItemInfo
    {
        $result = null;

        /** @var ItemInfo $item */
        foreach ($items as $item) {
            if ($group === $item->getGroup()->getTypeId() && $id === $item->getItemId()) {
                $result = $item;

                break;
            }
        }

        return $result;
    }
}