<?php

declare(strict_types=1);

namespace AppBundle\Command\ServerManager;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;

class CheckMonsterPositionCommand extends Command
{
    private const GROUP_NPC = 0;
    private const GROUP_MULTIPLE_SPAWN = 1;
    private const GROUP_SINGLE_SPAWN = 2;
    private const GROUP_GOLDEN = 3;
    private const GROUP_EVENT = 4;
    private const GROUP_IMPERIAL = 5;
    private const GROUP_ACHERON = 6;

    private const SETTINGS_PER_GROUP = [
        self::GROUP_NPC => 6,
        self::GROUP_MULTIPLE_SPAWN => 9,
        self::GROUP_SINGLE_SPAWN => 6,
        self::GROUP_GOLDEN => 9,
        self::GROUP_EVENT => 6,
        self::GROUP_IMPERIAL => 8,
        self::GROUP_ACHERON => 10,
    ];

    private const SETTING_ID = 0;
    private const SETTING_MAP = 1;
    private const SETTING_MOVE_RANGE = 2;
    private const SETTING_X = 3;
    private const SETTING_Y = 4;

    private const TRAPS = [
        100,        // Lance
        101,        // Iron Stick
        102,        // Fire
        103,        // Meteorite
        104,        // Trap
        105,        // Canon Trap
        106,        // Laser Trap
        523,        // Trap
        605,        // Mining Area (Small)
        606,        // Mining Area (Medium)
        607,        // Mining Area (Large)
    ];

    protected function configure()
    {
        $this->setName('app:server-manager:check-monster-position')
            ->addArgument('monster_set_base', InputArgument::REQUIRED, 'MonsterSetBase location.')
            ->addArgument('spots_start_line', InputArgument::REQUIRED, 'Line number from which non-changeable spots are starting.')
            ->addArgument('free_range_around_spot', InputArgument::REQUIRED, 'Range around non-changeable spots free from any monsters.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Check Monster Positions');


        $stopwatch = new Stopwatch();
        $stopwatch->start($this->getName());

        $this->checkMonsterPositions(
            $io,
            file_get_contents($input->getArgument('monster_set_base')),
            (int) $input->getArgument('spots_start_line'),
            (int) $input->getArgument('free_range_around_spot')
        );

        $event = $stopwatch->stop($this->getName());

        $io->newLine();
        $io->success(
            sprintf(
                'Done in %.3f seconds, %.3f MB memory used.',
                $event->getDuration() / 1000,
                $event->getMemory() / 1024 / 1024
            )
        );
    }

    private function checkMonsterPositions(SymfonyStyle $io, string $monsters, int $spotsStartsFrom, int $freeRangeAroundSpot): void
    {
        $monsters = $this->readMonsters($monsters, $spotsStartsFrom);

        $this->findMonstersNearSpots($io, $monsters, $freeRangeAroundSpot);
    }

    private function findMonstersNearSpots(SymfonyStyle $io, array $monsters, int $freeRangeAroundSpot): void
    {
        $toRemove = [];

        foreach ($monsters as $group => $monstersData) {
            foreach ($monstersData as $map => $monstersOnMap) {
                if (!isset($monstersOnMap['permanent']) || !isset($monstersOnMap['removable'])) {
                    continue;
                }

                foreach ($monstersOnMap['permanent'] as $permanentSpotMonster) {
                    foreach ($monstersOnMap['removable'] as $removableMonster) {
                        if ($this->areMonstersOverlapEachOther($group, $permanentSpotMonster, $removableMonster, $freeRangeAroundSpot)) {
                            $x = $this->getMonsterSetting($group, self::SETTING_X, $removableMonster);
                            $y = $this->getMonsterSetting($group, self::SETTING_Y, $removableMonster);
                            $range = $this->getMonsterSetting($group, self::SETTING_MOVE_RANGE, $removableMonster);

                            $toRemove[$group][$map][md5($x . $y . $range)] = $removableMonster;
                        }
                    }
                }
            }
        }

        $total = 0;

        foreach ($toRemove as $group => $groupRemove) {
            foreach ($groupRemove as $map => $mapRemove) {
                $io->note(
                    sprintf(
                        'On map "%s" should be removed %d monsters.',
                        $map,
                        count($mapRemove)
                    )
                );

                $total += count($mapRemove);

                if ($io->isVerbose()) {
                    $io->table(
                        [
                            'ID',
                            'Map',
                            'Range',
                            'X',
                            'Y',
                        ],
                        array_map(function (array $monster) use ($group): array {
                            return [
                                $this->getMonsterSetting($group, self::SETTING_ID, $monster),
                                $this->getMonsterSetting($group, self::SETTING_MAP, $monster),
                                $this->getMonsterSetting($group, self::SETTING_MOVE_RANGE, $monster),
                                $this->getMonsterSetting($group, self::SETTING_X, $monster),
                                $this->getMonsterSetting($group, self::SETTING_Y, $monster),
                            ];
                        }, $mapRemove)
                    );
                }
            }
        }

        $io->success(sprintf('Total should be removed %d monsters.', $total));
    }

    private function areMonstersOverlapEachOther(int $group, array $permanentSpotMonster, array $removableMonster, int $freeRangeAroundSpot): bool
    {
        if (in_array($this->getMonsterSetting($group, self::SETTING_ID, $removableMonster), self::TRAPS, true)) {
            return false;
        }

        $permanentSpotMonsterX = $this->getMonsterSetting($group, self::SETTING_X, $permanentSpotMonster);
        $permanentSpotMonsterY = $this->getMonsterSetting($group, self::SETTING_Y, $permanentSpotMonster);
        $permanentSpotMonsterRange = $this->getMonsterSetting($group, self::SETTING_MOVE_RANGE, $permanentSpotMonster);

        $permanentSpotMonsterX1 = $permanentSpotMonsterX - $permanentSpotMonsterRange;
        $permanentSpotMonsterX2 = $permanentSpotMonsterX + $permanentSpotMonsterRange;
        $permanentSpotMonsterY1 = $permanentSpotMonsterY + $permanentSpotMonsterRange;
        $permanentSpotMonsterY2 = $permanentSpotMonsterY - $permanentSpotMonsterRange;


        $removableSpotMonsterX = $this->getMonsterSetting($group, self::SETTING_X, $removableMonster);
        $removableSpotMonsterY = $this->getMonsterSetting($group, self::SETTING_Y, $removableMonster);

        $removableSpotMonsterX1 = $removableSpotMonsterX - $freeRangeAroundSpot;
        $removableSpotMonsterX2 = $removableSpotMonsterX + $freeRangeAroundSpot;
        $removableSpotMonsterY1 = $removableSpotMonsterY + $freeRangeAroundSpot;
        $removableSpotMonsterY2 = $removableSpotMonsterY - $freeRangeAroundSpot;

        return (
            $permanentSpotMonsterX1 < $removableSpotMonsterX2 && $permanentSpotMonsterX2 > $removableSpotMonsterX1 &&
            $permanentSpotMonsterY1 > $removableSpotMonsterY2 && $permanentSpotMonsterY2 < $removableSpotMonsterY1
        );
    }

    private function readMonsters(string $monsters, int $spotsStartsFrom): array
    {
        $monstersList = [];

        $monstersInBase = explode(PHP_EOL, $monsters);

        $currentGroup = null;

        foreach ($monstersInBase as $lineNumber => $monsterLine) {
            $monsterLine = trim(preg_replace('/\t+/', "\t", $monsterLine));

            if ('' === $monsterLine) {
                continue;
            }

            if ('//' === substr($monsterLine, 0, 2)) {
                continue;
            }

            $explodedLine = explode("\t", trim($monsterLine));
            $explodedLine = $this->removeCommentElement($explodedLine);

            if (1 === count($explodedLine)) {
                $group = trim($explodedLine[0]);

                if ('' === $group) {
                    continue;
                }

                if ('end' === $group) {
                    if (null === $currentGroup) {
                        throw new \RuntimeException(sprintf('End group "%s" while group is not started yet. Line: %d.', $group, $lineNumber));
                    }

                    $currentGroup = null;

                    continue;
                }

                if (!is_numeric($group)) {
                    continue;
                }

                if (!in_array((int) $group, [
                    self::GROUP_NPC,
                    self::GROUP_MULTIPLE_SPAWN,
                    self::GROUP_SINGLE_SPAWN,
                    self::GROUP_GOLDEN,
                    self::GROUP_EVENT,
                    self::GROUP_IMPERIAL,
                    self::GROUP_ACHERON,
                ], true)) {
                    throw new \RuntimeException(sprintf('Start of unknown group "%s". Line: %d.', $group, $lineNumber));
                } else {
                    if (null !== $currentGroup) {
                        throw new \RuntimeException(sprintf('Start group "%s" while another group "%d" already started. Line: %d.', $group, $currentGroup, $lineNumber));
                    }

                    $currentGroup = (int) $group;

                    if (self::GROUP_NPC === $currentGroup && $lineNumber >= $spotsStartsFrom) {
                        $currentGroup = self::GROUP_SINGLE_SPAWN;
                    }

                    continue;
                }
            }

            if (null === $currentGroup) {
                throw new \RuntimeException(sprintf('Cannot process monster record while no group determined. Line: %d.', $lineNumber));
            }

            if (self::SETTINGS_PER_GROUP[$currentGroup] !== count($explodedLine)) {
                throw new \RuntimeException(
                    sprintf(
                        'Settings count in current monster line does not match expected settings count in this group. Group: %d; Settings Count: %d; Settings: "%s"; Line: %d.',
                        $currentGroup,
                        count($explodedLine),
                        $monsterLine,
                        $lineNumber
                    )
                );
            }

            if (!in_array($currentGroup, [self::GROUP_SINGLE_SPAWN], true)) {
                continue;
            }

            $monsterPriority = $lineNumber >= $spotsStartsFrom
                ? 'permanent'
                : 'removable';

            $monstersList[$currentGroup][$this->getMonsterSetting($currentGroup, self::SETTING_MAP, $explodedLine)][$monsterPriority][] = $explodedLine;
        }

        return $monstersList;
    }

    private function removeCommentElement(array $parts): array
    {
        return array_filter($parts, function (string $part): bool {
            return '//' !== substr(trim($part), 0, 2);
        });
    }

    private function getMonsterSetting(int $group, int $setting, array $monster): int
    {
        if (!in_array($setting, [self::SETTING_ID, self::SETTING_MAP, self::SETTING_MOVE_RANGE, self::SETTING_X, self::SETTING_Y], true)) {
            throw new \RuntimeException(sprintf('Unknown setting "%d".', $setting));
        }

        if (!in_array($group, [self::GROUP_SINGLE_SPAWN], true)) {
            throw new \RuntimeException(sprintf('Unknown or not supported group "%d".', $setting));
        }

        if (!isset($monster[$this->getMonsterSettingsMap()[$group][$setting]])) {
            throw new \RuntimeException(
                sprintf(
                    'Monster with settings "%s" does not have required setting "%d" in group "%d".',
                    implode("\t", $monster),
                    $setting,
                    $group
                )
            );
        }

        return (int) $monster[$this->getMonsterSettingsMap()[$group][$setting]];
    }

    private function getMonsterSettingsMap(): array
    {
        return [
            self::GROUP_SINGLE_SPAWN => [
                self::SETTING_ID => 0,
                self::SETTING_MAP => 1,
                self::SETTING_MOVE_RANGE => 2,
                self::SETTING_X => 3,
                self::SETTING_Y => 4,
            ],
        ];
    }
}