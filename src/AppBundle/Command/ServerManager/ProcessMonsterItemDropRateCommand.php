<?php

declare(strict_types=1);

namespace AppBundle\Command\ServerManager;

use DOMDocument;
use SimpleXMLElement;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;

class ProcessMonsterItemDropRateCommand extends Command
{
    protected function configure()
    {
        $this->setName('app:server-manager:process-monster-item-drop-rate')
            ->addArgument('monster_list', InputArgument::REQUIRED, 'Monster List XML Config.')
            ->addArgument('drop_list', InputArgument::REQUIRED, 'Drop List XML Config.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Process Monster Item Drop Rates');

        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion(
            'Choose action:',
            ['Display', 'Change',]
        );

        $mode = $helper->ask($input, $output, $question);

        $stopwatch = new Stopwatch();
        $stopwatch->start($this->getName());

        switch ($mode) {
            case 'Display':
                {
                    $this->processDisplay(
                        $io,
                        file_get_contents($input->getArgument('monster_list')),
                        file_get_contents($input->getArgument('drop_list'))
                    );
                    break;
                }
            case 'Change':
                {
                    $this->processChange(
                        $input,
                        $output,
                        $io,
                        file_get_contents($input->getArgument('monster_list')),
                        file_get_contents($input->getArgument('drop_list'))
                    );
                    break;
                }
        }

        $event = $stopwatch->stop($this->getName());

        $io->newLine();
        $io->success(
            sprintf(
                'Done in %.3f seconds, %.3f MB memory used.',
                $event->getDuration() / 1000,
                $event->getMemory() / 1024 / 1024
            )
        );
    }

    private function processChange(InputInterface $input, OutputInterface $output, SymfonyStyle $io, string $monsters, string $dropRates): void
    {
        $helper = $this->getHelper('question');

        $rates = [
            'bless' => [
                'minRate' => (float) $helper->ask($input, $output, new Question('Min Jewel of Bless drop rate (0-1): ', 0)),
                'maxRate' => (float) $helper->ask($input, $output, new Question('Max Jewel of Bless drop rate (0-1): ', 0)),
                'minMonsterLevel' => (int) $helper->ask($input, $output, new Question('Min Jewel of Bless Monster Level (1 - 255): ', 0)),
                'maxMonsterLevel' => (int) $helper->ask($input, $output, new Question('Max Jewel of Bless Monster Level (1 - 255): ', 0)),
            ],
            'soul' => [
                'minRate' => (float) $helper->ask($input, $output, new Question('Min Jewel of Soul drop rate (0-1): ', 0)),
                'maxRate' => (float) $helper->ask($input, $output, new Question('Max Jewel of Soul drop rate (0-1): ', 0)),
                'minMonsterLevel' => (int) $helper->ask($input, $output, new Question('Min Jewel of Soul Monster Level (1 - 255): ', 0)),
                'maxMonsterLevel' => (int) $helper->ask($input, $output, new Question('Max Jewel of Soul Monster Level (1 - 255): ', 0)),
            ],
            'life' => [
                'minRate' => (float) $helper->ask($input, $output, new Question('Min Jewel of Life drop rate (0-1): ', 0)),
                'maxRate' => (float) $helper->ask($input, $output, new Question('Max Jewel of Life drop rate (0-1): ', 0)),
                'minMonsterLevel' => (int) $helper->ask($input, $output, new Question('Min Jewel of Life Monster Level (1 - 255): ', 0)),
                'maxMonsterLevel' => (int) $helper->ask($input, $output, new Question('Max Jewel of Life Monster Level (1 - 255): ', 0)),
            ],
            'creation' => [
                'minRate' => (float) $helper->ask($input, $output, new Question('Min Jewel of Creation drop rate (0-1): ', 0)),
                'maxRate' => (float) $helper->ask($input, $output, new Question('Max Jewel of Creation drop rate (0-1): ', 0)),
                'minMonsterLevel' => (int) $helper->ask($input, $output, new Question('Min Jewel of Creation Monster Level (1 - 255): ', 0)),
                'maxMonsterLevel' => (int) $helper->ask($input, $output, new Question('Max Jewel of Creation Monster Level (1 - 255): ', 0)),
            ],
            'chaos' => [
                'minRate' => (float) $helper->ask($input, $output, new Question('Min Jewel of Chaos drop rate (0-1): ', 0)),
                'maxRate' => (float) $helper->ask($input, $output, new Question('Max Jewel of Chaos drop rate (0-1): ', 0)),
                'minMonsterLevel' => (int) $helper->ask($input, $output, new Question('Min Jewel of Chaos Monster Level (1 - 255): ', 0)),
                'maxMonsterLevel' => (int) $helper->ask($input, $output, new Question('Max Jewel of Chaos Monster Level (1 - 255): ', 0)),
            ],
        ];
//        $rates = [
//            'bless' => [
//                'minRate' => 0.00015,
//                'maxRate' => 0.00032,
//                'minMonsterLevel' => 47,
//                'maxMonsterLevel' => 148,
//            ],
//            'soul' => [
//                'minRate' => 0.0002,
//                'maxRate' => 0.00042,
//                'minMonsterLevel' => 60,
//                'maxMonsterLevel' => 148,
//            ],
//            'life' => [
//                'minRate' => 0.00005,
//                'maxRate' => 0.00012,
//                'minMonsterLevel' => 76,
//                'maxMonsterLevel' => 148,
//            ],
//            'creation' => [
//                'minRate' => 0.0,
//                'maxRate' => 0.0,
//                'minMonsterLevel' => 1,
//                'maxMonsterLevel' => 255,
//            ],
//            'chaos' => [
//                'minRate' => 0.0001,
//                'maxRate' => 0.0002,
//                'minMonsterLevel' => 35,
//                'maxMonsterLevel' => 148,
//            ],
//        ];

        $result = new SimpleXMLElement('<DropRate></DropRate>');

        $dropRates = new SimpleXMLElement($dropRates);

        foreach ($dropRates as $dropRate) {
            $attr = $dropRate->attributes();

            $level = (int) $attr->Level;
            $currentRates = [
                'book' => (double) $attr->MagicBook,
                'bless' => (double) $attr->JewelOfBless,
                'soul' => (double) $attr->JewelOfSoul,
                'life' => (double) $attr->JewelOfLife,
                'creation' => (double) $attr->JewelOfCreation,
                'chaos' => (double) $attr->JewelOfChaos,
            ];

            foreach ($rates as $rateName => $rate) {
                if ($level < $rate['minMonsterLevel'] || $level > $rate['maxMonsterLevel']) {
                    $currentRates[$rateName] = 0;

                    continue;
                }

                $percent = round(
                    ($level - $rate['minMonsterLevel']) *
                    ($rate['maxRate'] - $rate['minRate']) /
                    ($rate['maxMonsterLevel'] - $rate['minMonsterLevel']) +
                    $rate['minRate'],
                    7
                );

                $currentRates[$rateName] = $percent;
            }

            $monster = $result->addChild('Monster');

            $monster->addAttribute('Level', number_format($level, 0));
            $monster->addAttribute('MagicBook', number_format($currentRates['book'], 7, '.', ''));
            $monster->addAttribute('JewelOfBless', number_format($currentRates['bless'], 7, '.', ''));
            $monster->addAttribute('JewelOfSoul', number_format($currentRates['soul'], 7, '.', ''));
            $monster->addAttribute('JewelOfLife', number_format($currentRates['life'], 7, '.', ''));
            $monster->addAttribute('JewelOfCreation', number_format($currentRates['creation'], 7, '.', ''));
            $monster->addAttribute('JewelOfChaos', number_format($currentRates['chaos'], 7, '.', ''));
            $monster->addAttribute('Items', number_format(1 - array_sum($currentRates), 7, '.', ''));

        }

        $this->processDisplay($io, $monsters, $result->asXML());

        $dom = new DOMDocument('1.0', 'utf-8');
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;

        $dom_xml = dom_import_simplexml($result);
        $dom_xml = $dom->importNode($dom_xml, true);
        $dom_xml = $dom->appendChild($dom_xml);

        $dom->save($input->getArgument('drop_list') . '.new.xml');
    }

    private function processDisplay(SymfonyStyle $io, string $monsters, string $dropRates): void
    {
        $monsters = new \SimpleXMLElement($monsters);
        $dropRates = new \SimpleXMLElement($dropRates);

        $monstersByLevels = $this->monstersByLevels($monsters);

        $io->table(
            [
                'LvL',
                'Book',
                'Bless',
                'Soul',
                'Life',
                'Creation',
                'Chaos',
                'Items',
                'Total',
                'Mobs',
            ],
            array_map(function ($item) use ($monstersByLevels): array {
                $attr = $item->attributes();

                return [
                    $attr->Level,
                    sprintf('%.08f - %d', $attr->MagicBook, ((double) $attr->MagicBook) * 10000),
                    sprintf('%.08f - %d', $attr->JewelOfBless, ((double) $attr->JewelOfBless) * 10000),
                    sprintf('%.08f - %d', $attr->JewelOfSoul, ((double) $attr->JewelOfSoul) * 10000),
                    sprintf('%.08f - %d', $attr->JewelOfLife, ((double) $attr->JewelOfLife) * 10000),
                    sprintf('%.08f - %d', $attr->JewelOfCreation, ((double) $attr->JewelOfCreation) * 10000),
                    sprintf('%.08f - %d', $attr->JewelOfChaos, ((double) $attr->JewelOfChaos) * 10000),
//                    sprintf('%d', ((double) $attr->MagicBook) * 10000),
//                    sprintf('%d', ((double) $attr->JewelOfBless) * 10000),
//                    sprintf('%d', ((double) $attr->JewelOfSoul) * 10000),
//                    sprintf('%d', ((double) $attr->JewelOfLife) * 10000),
//                    sprintf('%d', ((double) $attr->JewelOfCreation) * 10000),
//                    sprintf('%d', ((double) $attr->JewelOfChaos) * 10000),
                    $attr->Items,
                    array_sum([
                        (double) $attr->MagicBook,
                        (double) $attr->JewelOfBless,
                        (double) $attr->JewelOfSoul,
                        (double) $attr->JewelOfLife,
                        (double) $attr->JewelOfCreation,
                        (double) $attr->JewelOfChaos,
                        (double) $attr->Items,
                    ]),
                    implode(PHP_EOL, array_map(function ($monster): string {
                        return implode(
                            "\t",
                            [
//                                (string) $monster->Level,
                                (string) $monster->Name]
                        );
                    }, $monstersByLevels[(int) $attr->Level] ?? [])),
                ];
            }, $this->xml2array($dropRates)['Monster'])
        );
    }

    private function monstersByLevels($monsters): array
    {
        $result = [];
        foreach ($monsters as $monster) {
            $attr = $monster->attributes();

            if (!isset($result[(int) $attr->Level])) {
                $result[(int) $attr->Level] = [];
            }

            $result[(int) $attr->Level][] = $attr;
        }

        return $result;
    }

    private function xml2array($xmlObject, $out = array())
    {
        foreach ((array) $xmlObject as $index => $node)
            $out[$index] = (is_object($node)) ? $this->xml2array($node) : $node;

        return $out;
    }
}