<?php


namespace AppBundle\Command\ServerManager;

use DOMDocument;
use SimpleXMLElement;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;

class ProcessMonsterListCommand extends Command
{
    private const SKIPPED = [
        // Lorencia, Noria
        0, 1, 2, 3, 4, 5, 6, 7,
        14, 26, 27, 28, 29, 30, 31,
        32, 33,
        // Elveland
        418,        // Strange Rabbit
        419,        // Hideous Rabbit
        420,        // Werewolf
        421,        // Polluted Butterfly
        422,        // Cursed Lich
        423,        // Totem Golem
        424,        // Grizzly
        425,        // Captain Grizzly
        // Chaos Castle
        162, 163, 164, 165, 166, 167,
        168, 169, 170, 171, 172, 173,
        426, 427, 644, 645,
        // CryWolf Event
        204, 205, 206, 207, 208, 209,
        // Golden Event
        652, 653, 654, 655, 656, 657, 677,
        // Snakes
        647, 648, 649, 650,
        586,        // Devil Fairy
        587,        // Elemental Beast
        588,        // Elemental Knight
        589,        // Ubaid Devil Fairy
        590,        // Ubaid Elemental Beast
        591,        // Ubaid Elemental Knight
        592,        // Undine
        593,        // Salamander
        594,        // Sylphid
        595,        // Gnome
        596,        // Hellraiser
        598,        // Fire Tower
        599,        // Water Tower
        600,        // Earth Tower
        601,        // Wind Tower
        602,        // Darkness Tower
        603,        // Arca Barrier
        // Debenter
        608, 609, 610, 611, 612, 613, 614, 615,
        618,        // [GM] Romeu
        619,        // [VM] Nox
        620,        // [VM] Redd
        621,        // [EVM] Iza
        622,        // [VM] Hail
        623,        // [VM] Fast
        624,        // [GM] Ceth
        625,        // Robot Knight Transformation Ring
        626,        // Mini Robot Transformation Ring
        627,        // Cursed Fire Tower
        628,        // Cursed Water Tower
        629,        // Cursed Earth Tower
        630,        // Cursed Wind Tower
        631,        // Cursed Darkness Tower
        632,        // Cursed Undine
        633,        // Cursed Salamander
        634,        // Cursed Sylphid
        635,        // Cursed Gnome
        636,        // Cursed Hellraiser
        637,        // Cursed Sellihoden
        638,        // Cursed Ukanva
        639,        // Cursed Silla
        640,        // Cursed Normus
        641,        // Cursed Muff
        642,        // Great Heavenly Mage Transformation Ring
        643,        // Mait
        646,        // Brown Panda Transformation Ring
        548,        // Transformed Skeleton
        503,        // Transformed Panda
        200,        // Soccer Ball

        //Unknown
        404,        // MU Allies
        405,        // Illusion Sorcerer
        372,        // Elite Skull Soldier
        597,        // Summoned Satyros
        658,        // Cursed Statue
        659,        // Captured Stone Statue
        660,        // Captured Stone Statue
        661,        // Captured Stone Statue
        662,        // Captured Stone Statue
        663,        // Captured Stone Statue
        664,        // Captured Stone Statue
        665,        // Captured Stone Statue
        666,        // Captured Stone Statue
        667,        // Captured Stone Statue
        668,        // Captured Stone Statue


    ];

    private const REGULAR_MOBS = [
        // Dungeon
        12, 17, 11, 15, 5, 13, 16, 9, 8, 10, 18,
        // Devias
        24, 22, 23, 21, 19, 20, 25,
        // Lost Tower
        34, 35, 36, 37, 38, 39, 40, 41,
        // Atlans
        45, 46, 47, 51, 52, 50, 48, 49,
        // Tarkan
        62, 60, 57, 58, 61, 59, 63,
        // Icarus
        68, 69, 71, 70, 73, 74, 72, 75, 76, 77,
        // BC 1
        84, 85, 86, 87, 88, 89,
        //BC 2
        90, 91, 92, 93, 94, 95,
        //BC 3
        96, 97, 98, 99, 111, 112,
        // BC 4
        113, 114, 115, 116, 117, 118,
        // BC 5
        119, 120, 121, 122, 123, 124,
        // BC 6
        125, 126, 127, 128, 129, 130,
        // BC 7
        138, 139, 140, 141, 142, 143,
        // BC 8
        428, 429, 430, 431, 432, 433,
        // Kalima 1
        147, 148, 146, 144, 149, 145, 160,
        // Kalima 2
        177, 178, 176, 174, 179, 175, 180,
        // Kalima 3
        185, 186, 184, 182, 187, 183, 188,
        // Kalima 4
        196, 191, 195, 190, 192, 194, 193,
        // Kalima 5
        261, 265, 260, 262, 264, 263, 266,
        // Kalima 6
        271, 272, 270, 269, 273, 268, 274,
        // Kalima 7
        331, 332, 333, 334, 335, 336, 337,
        // Land Of Trials
        293, 296, 290, 294, 291, 292,
        // Aida
        308, 307, 306, 305, 304, 309,   // Standard
        552, 551, 550, 549, // Bloody
        // CroWolf
        341, 344, 345, 311, 312, 310,
        // Kanturu
        351, 352, 353, 354, 357, 355, 356, 350, 359, 360, 358,  // Standard
        556, 555, 554, 553, // Warrior
        // Barracks of Balgass
        409, 410, 411,
        // DS+7
        434, 435, 436, 437, 438, 439, 440,
        // Raklion
        454, 455, 456, 457, 458, 460, 461, 462, // Standard
        565, 564, 563, 562, // Dark
        // Karutan
        576, 575, 574, 573, 572, 571, 570, 569,
        // Swamp of Calmness
        559,        // Shadow Master
        558,        // Ice Napin
        557,        // Sapi Queen
        447,        // Thunder Napin
        448,        // Ghost Napin
        449,        // Blaze Napin
        441,        // Sapi-Unus
        442,        // Sapi-Duo
        443,        // Sapi-Tres
        444,        // Shadow Pawn
        445,        // Shadow Knight
        446,        // Shadow Look
        // Doppelganger
        533,        // Doppelganger
        534,        // Doppelganger Elf
        535,        // Doppelganger Knight
        536,        // Doppelganger Wizard
        537,        // Doppelganger Magic Gladiator
        538,        // Doppelganger Dark Lord
        539,        // Doppelganger Summoner
        // Imperial Fortress
        504,        // Gaion Kharein
        505,        // Jerint
        506,        // Raymond
        507,        // Erkanne
        508,        // Destler
        509,        // Vermont
        510,        // Kato
        511,        // Galia
        512,        // Quartermaster
        513,        // Combat Instructor
        514,        // Knight Commander
        515,        // Grand Wizard
        516,        // Master Assassin
        517,        // Cavalry Captain
        518,        // Shield Bearer
        519,        // Medic
        520,        // Knights
        521,        // Bodyguard
        // Vulcanus
        480,        // Zombie Fighter
        481,        // Zombie Fighter
        482,        // Resurrected Gladiator
        483,        // Resurrected Gladiator
        484,        // Ash Slaughterer
        485,        // Ash Slaughterer
        486,        // Blood Assassin
        487,        // Cruel Blood Assassin
        488,        // Cruel Blood Assassin
        489,        // Burning Lava Giant
        490,        // Ruthless Lava Giant
        491,        // Ruthless Lava Giant

        // Illusion Temple
        386,        // Illusion Sorcerer Spirit A
        387,        // Illusion Sorcerer Spirit B
        388,        // Illusion Sorcerer Spirit C
        389,        // Illusion Sorcerer Spirit A
        390,        // Illusion Sorcerer Spirit B
        391,        // Illusion Sorcerer Spirit C
        392,        // Illusion Sorcerer Spirit A
        393,        // Illusion Sorcerer Spirit B
        394,        // Illusion Sorcerer Spirit C
        395,        // Illusion Sorcerer Spirit A
        396,        // Illusion Sorcerer Spirit B
        397,        // Illusion Sorcerer Spirit C
        398,        // Illusion Sorcerer Spirit A
        399,        // Illusion Sorcerer Spirit B
        400,        // Illusion Sorcerer Spirit C
        401,        // Illusion Sorcerer Spirit A
        402,        // Illusion Sorcerer Spirit B
        403,        // Illusion Sorcerer Spirit C

        // Some kind of Quest Mobs?
        463,        // Fire Flame Ghost
        676,        // Fire Flame Ghost - Not Used
        365,        // Pouch of Blessing (Enabled on Halloween)
        674,        // Moon Rabbit
        476,        // Evil Santa Claus

        // Unknown
        531,        // Ice Walker
        532,        // Larva
        529,        // Furious Slaughterer
        530,        // Slaughterer
        300,        // Hero Mutant
        301,        // Omega Wing
        302,        // Axl Hero
        303,        // Gigas Golem
        136,        // Destructive ogre soldier
        137,        // Destructive ogre archer
        150,        // Bali
        151,        // Soldier
        64,         // Ogre archer
        65,         // Elite Ogre
        675,        // Pouch of Blessing (Enabled on Halloween) - Not used
    ];

    private const BOSS_MOBS = [
        // Kalima 1
        161,
        // Kalima 2
        181,
        // Kalima 3
        189,
        // Kalima 4
        197,
        // Kalima 5
        267,
        // Kalima 6
        338,
        // Kalima 7
        275,
        // Land Of Trials
        295,
        // CryWolf
        313, 314, 315, 316, 317, 348, 349, 340,
        // Kanturu
        362, 363, 364, 361,
        // Refuge Balgass
        412,
        // RabbiT (White Rabbit)
        413,
        // Raklion
        459,
        // Season 5 Golden Mobs
        493, 494, 495, 496, 497, 498, 499, 500, 501, 502,
        // Golden Event I
        42, 43, 44, 55, 56,
        // Golden Event II
        78, 54, 53, 79, 81, 80, 83, 82,
        // Swamp of Calmness
        561,        // Medusa
        560,        // Sapi Queen

        // Other
        66,         // Cursed King
        67,         // Metal Balrog
        135,        // White Wizard
    ];

    private const TRAPS = [
        100,        // Lance
        101,        // Iron Stick
        102,        // Fire
        103,        // Meteorite
        104,        // Trap
        105,        // Canon Trap
        106,        // Laser Trap
        523,        // Trap
        605,        // Mining Area (Small)
        606,        // Mining Area (Medium)
        607,        // Mining Area (Large)
    ];

    private const NPC = [
        // Blood Castle NPC
        131,        // Castle Gate
        132,        // Statue of Saint
        133,        // Statue of Saint
        134,        // Statue of Saint

        // Other
        226,        // Trainer
        227,        // McDonald's
        228,        // Popeye's
        229,        // Malron
        230,        // Alex
        231,        // Thomson Cannel
        232,        // Archangel
        233,        // Spirit of Archangel
        234,        // Goblin Gate
        235,        // Priest Sevina
        236,        // Golden Archer
        237,        // Charon
        238,        // Chaos Goblin
        239,        // Stadium Guard
        240,        // Baz, Storage Guard
        241,        // Lawrence, Command
        242,        // Elf Lala
        243,        // Eo, Craftsman
        244,        // Lumen Barmaid
        245,        // Isabel Wizard
        246,        // Zienna Merchant
        247,        // Crossbow Guard
        248,        // Martin, Wandering
        249,        // Berdysh Guard
        250,        // Harold, Wandering
        251,        // Hanzo, Blacksmith
        252,        // monstro 252
        253,        // Amy, Potion Girl
        254,        // Pasi,Wizard
        255,        // Liaman, Barmaid
        256,        // Lahap
        257,        // Elf Soldier
        258,        // Leo, the Helper
        259,        // Priest Reira
        297,        // PK Dark Knight
        380,        // Stone Henge
        381,        // Sword Guard
        382,        // Illusion Guard
        383,        // Pedestal Spiral
        384,        // Pedestal
        385,        // Alchemist
        414,        // Elegance Allen
        415,        // Sivia
        416,        // Leah
        417,        // Marseille
        450,        // Governor Blossom
        451,        // Blossoms Tree
        452,        // Hour Mastering
        453,        // Hour Research
        464,        // Initial Helper
        479,        // Gatekeeper Titus
        492,        // Moss
        522,        // Jerry The Adviseru
        523,        // Trap
        524,        // Evil Gate
        525,        // Lion Gate
        526,        // Statue
        527,        // Star Gate
        528,        // Rush Gate
        543,        // GENS 1
        544,        // GENS 2
        545,        // Christine
        546,        // Jeweler Raul
        547,        // Market Julia
        371,        // Leo the Helper
        373,        // Jack Olantern
        374,        // Santa
        375,        // Chaos Card Master
        376,        // Pamela the Supplier
        377,        // Angela the Supplier
        378,        // GameMaster
        379,        // Natasha Firecracker Merchant
        577,        // Leina the Merchant
        578,        // Weapons Merchant Bolo
        579,        // David
        580,        // Captain Slough
        581,        // Deruvish
        582,        // Adniel
        583,        // Jin
        604,        // Jin
        584,        // Sir Lesnar
        585,        // Scarecrow
        568,        // Wandering Merchant Zyro
        567,        // Priestess Veina
        566,        // Mercenary Guild Manager Tercia
        152,        // Magic stone of 1 Kalima
        153,        // Magic stone of 2 Kalima
        154,        // Magic stone of 3 Kalima
        155,        // Magic stone of 4 Kalima
        156,        // Magic stone of 5 Kalima
        157,        // Magic stone of 6 Kalima
        158,        // Magic stone of 7 Kalima
        651,        // Private Store Bulletin Board

        // Kanturu
        367,        // Core Gate
        368,        // Elpis

        // Harmony
        369,        // Osborne
        370,        // Jerridon

        // Castle Siege
        215,        // Shield
        216,        // Crown
        217,        // Crown Switch 1
        218,        // Crown Switch 2
        219,        // Castle Gate Swit
        220,        // Guard
        221,        // Slingshot Attack
        222,        // Slingshot Defens
        223,        // Senior
        224,        // Guardsman
        277,        // Castle Gate
        278,        // Life Stone
        283,        // Guardian Statue
        284,        // monstro 284
        285,        // Guardian
        286,        // Archer
        287,        // Spearman
        288,        // Canon Tower

        // 3 Quest
        406,        // Priest Devin
        407,        // Werewolf Quarrel
        408,        // Keepergate

        // Christmas
        465,        // Santa Claus
        466,        // Evil Goblin
        467,        // Snowman
        468,        // Little Santa Yellow
        469,        // Little Santa Green
        470,        // Little Santa Red
        471,        // Little Santa Blue
        472,        // Little Santa White
        473,        // Little Santa Black
        474,        // Little Santa Orange
        475,        // Little Santa Pink
        477,        // Evil Snowman
        478,        // Lucky Coins

        // Double Goer
        540,        // Sartigan the Angel
        541,        // Silver Tresaure
        542,        // Golden Tresaure

    ];

    protected function configure()
    {
        $this->setName('app:server-manager:process-monster-list')
            ->addArgument('monster_list', InputArgument::REQUIRED, 'Monster List XML Config.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Process Monster List');

        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion(
            'Choose action:',
            ['Display', 'Change',]
        );

        $mode = $helper->ask($input, $output, $question);

        $stopwatch = new Stopwatch();
        $stopwatch->start($this->getName());

        switch ($mode) {
            case 'Display': {
                $this->processDisplay(
                    $io,
                    file_get_contents($input->getArgument('monster_list'))
                );
                break;
            }
            case 'Change': {
                $this->processChange(
                    $input,
                    $output,
                    $io,
                    file_get_contents($input->getArgument('monster_list'))
                );
                break;
            }
        }

        $event = $stopwatch->stop($this->getName());

        $io->newLine();
        $io->success(
            sprintf(
                'Done in %.3f seconds, %.3f MB memory used.',
                $event->getDuration() / 1000,
                $event->getMemory() / 1024 / 1024
            )
        );
    }

    private function processChange(InputInterface $input, OutputInterface $output, SymfonyStyle $io, string $monsters): void
    {
        $monsters = new \SimpleXMLElement($monsters);

        $result = new SimpleXMLElement('<MonsterList></MonsterList>');

        $rates = [
            'itemDropRate' => 100,
            'zenDropRate'  => 50,
            'regular'      => [
                //  <= lvl  +%
                20  => 20,
                40  => 35,
                60  => 60,
                80  => 90,
                100 => 130,
                120 => 180,
                254 => 500,
            ],
//            'boss'         => [
//                20  => 200,
//                40  => 800,
//                60  => 1200,
//                80  => 1500,
//                100 => 3000,
//                120 => 5000,
//                254 => 10000,
//            ],
            'trap'        => [
                254 => 1800,
            ],
        ];

        foreach ($monsters as $monster) {
            $attr = $monster->attributes();

            if (in_array((int)$attr->Index, array_merge(self::NPC, self::SKIPPED), true)) {
                $updatedMonster = $attr;
            } elseif (in_array((int)$attr->Index, self::REGULAR_MOBS, true)) {
                $updatedMonster = $this->updateMonsterParameters($io, $attr, $rates['regular'], $rates['itemDropRate'], $rates['zenDropRate']);
            } elseif (in_array((int)$attr->Index, self::BOSS_MOBS, true)) {
                $updatedMonster = $attr;
//                $updatedMonster = $this->updateMonsterParameters($io, $attr, $rates['boss'], $rates['itemDropRate'], $rates['zenDropRate']);
            } elseif (in_array((int)$attr->Index, self::TRAPS, true)) {
                $updatedMonster = $this->updateMonsterParameters($io, $attr, $rates['trap'], $rates['itemDropRate'], $rates['zenDropRate']);
            } else {
                throw new \LogicException(sprintf('Monster with index %d does not belong to any known group.', $attr->Index));
            }

            $record = $result->addChild('Monster');

            foreach ($updatedMonster as $property => $value) {
                $record->addAttribute($property, $value);
            }

//            if (!in_array((int)$attr->Index, array_merge(self::REGULAR_MOBS, self::BOSS_MOBS, self::TRAPS, self::NPC, self::SKIPPED), true)) {
//                $io->comment(
//                    sprintf(
//                        "%d\t\t%d\t\t%s",
//                        $attr->Index,
//                        $attr->Level,
//                        $attr->Name
//                    )
//                );
//            }
        }

//        $this->processDisplay($io, $result->asXML());

        $dom = new DOMDocument('1.0', 'utf-8');
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;

        $dom_xml = dom_import_simplexml($result);
        $dom_xml = $dom->importNode($dom_xml, true);
        $dom_xml = $dom->appendChild($dom_xml);

        $dom->save($input->getArgument('monster_list') . '.new.xml');
    }

    private function updateMonsterParameters(SymfonyStyle $io, $monster, array $percentMap, int $itemDropRate, int $zenDropRate)
    {
        $displayResult = new SimpleXMLElement('<MonsterList></MonsterList>');

        $record = $displayResult->addChild('Monster');

        foreach ($monster as $property => $value) {
            $record->addAttribute($property, $value);
        }

        $level = (int)$monster->Level;
        $updateByPercent = 0;

        foreach ($percentMap as $maxLevel => $percent) {
            if ($level < $maxLevel) {
                $updateByPercent = $percent;

                break;
            }
        }

        if ($updateByPercent < 1) {
            return $monster;
        }

        $result = clone $monster;

        $result->HP = $this->updateMonsterParameter((int)$result->HP, $updateByPercent);
        $result->MP = $this->updateMonsterParameter((int)$result->MP, $updateByPercent);
        $result->DamageMin = $this->updateMonsterParameter((int)$result->DamageMin, $updateByPercent);
        $result->DamageMax = $this->updateMonsterParameter((int)$result->DamageMax, $updateByPercent);
        $result->Defense = $this->updateMonsterParameter((int)$result->Defense, $updateByPercent);
        $result->MagicDefense = $this->updateMonsterParameter((int)$result->MagicDefense, $updateByPercent);
        $result->AttackRate = $this->updateMonsterParameter((int)$result->AttackRate, $updateByPercent);
        $result->BlockRate = $this->updateMonsterParameter((int)$result->BlockRate, $updateByPercent);
        $result->ItemDropRate = $itemDropRate;
        $result->MoneyDropRate = $zenDropRate;

        $record = $displayResult->addChild('Monster');

        foreach ($result as $property => $value) {
            $record->addAttribute($property, $value);
        }

        $this->processDisplay($io, $displayResult->asXML());

        return $result;
    }

    private function updateMonsterParameter(int $value, int $percent): int
    {
        return $value !== 0
            ? (int)round($value * (100 + $percent) / 100)
            : $value;
    }

    private function processDisplay(SymfonyStyle $io, string $monsters): void
    {
        $monsters = new \SimpleXMLElement($monsters);

        $io->table(
            [
                'Idx',                      //  Index
                'ET',                       //  ExpType
                'Trap',                     //  isTrap
                'Name',                     //  Name
                'Lv',                       //  Level
                'HP',                       //  HP
                'MP',                       //  MP
                'MiDm',                     //  DamageMin
                'MaDm',                     //  DamageMax
                'Def',                      //  Defense
                'MDf',                      //  MagicDefense
                'ARt',                      //  AttackRate
                'BRt',                      //  BlockRate
                'MRng',                     //  MoveRange
                'ATp',                      //  AttackType
                'ARng',                     //  AttackRange
                'VRng',                     //  ViewRange
                'MSp',                      //  MoveSpeed
                'ASp',                      //  AttackSpeed
                'RT',                       //  RegenTime
                'Attr',                     //  Attribute
                'DrRt',                     //  ItemDropRate
                'MDrRt',                    //  MoneyDropRate
                'MaxILv',                   //  MaxItemLevel
//                'MonsterSkill',           //  MonsterSkill
                'IR',                       //  IceRes
                'PR',                       //  PoisonRes
                'LR',                       //  LightRes
                'FR',                       //  FireRes
//                'PentagramMainAttrib',    //  PentagramMainAttrib
//                'PentagramAttribPattern', //  PentagramAttribPattern
//                'PentagramDamageMin',     //  PentagramDamageMin
//                'PentagramDamageMax',     //  PentagramDamageMax
//                'PentagramAttackRate',    //  PentagramAttackRate
//                'PentagramDefenseRate',   //  PentagramDefenseRate
//                'PentagramDefense',       //  PentagramDefense
            ],
            array_map(function ($item): array {
                $attr = $item->attributes();

                return [
                    $attr->Index,
                    $attr->ExpType,
                    $attr->isTrap,
                    mb_substr($attr->Name, 0, 10),
                    $attr->Level,
                    $attr->HP,
                    $attr->MP,
                    $attr->DamageMin,
                    $attr->DamageMax,
                    $attr->Defense,
                    $attr->MagicDefense,
                    $attr->AttackRate,
                    $attr->BlockRate,
                    $attr->MoveRange,
                    $attr->AttackType,
                    $attr->AttackRange,
                    $attr->ViewRange,
                    $attr->MoveSpeed,
                    $attr->AttackSpeed,
                    $attr->RegenTime,
                    $attr->Attribute,
                    $attr->ItemDropRate,
                    $attr->MoneyDropRate,
                    $attr->MaxItemLevel,
                    $attr->IceRes,
                    $attr->PoisonRes,
                    $attr->LightRes,
                    $attr->FireRes,
                ];
            }, $this->xml2array($monsters)['Monster'])
        );
    }

    private function xml2array($xmlObject, $out = [])
    {
        foreach ((array)$xmlObject as $index => $node)
            $out[$index] = (is_object($node)) ? $this->xml2array($node) : $node;

        return $out;
    }
}