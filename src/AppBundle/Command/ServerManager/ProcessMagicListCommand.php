<?php

declare(strict_types=1);

namespace AppBundle\Command\ServerManager;

use AppBundle\Entity\Application\MagicList\SkillBuilder;
use AppBundle\Enum\Application\MagicList\SkillAttribute;
use AppBundle\Enum\Application\MagicList\SkillType;
use AppBundle\Enum\Application\MagicList\SkillUseType;
use AppBundle\Enum\MuOnline\CharacterClass;
use AppBundle\Repository\Application\CharacterClassRepository;
use AppBundle\Repository\Application\MagicList\SkillRepository;
use SimpleXMLElement;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;

class ProcessMagicListCommand extends Command
{
    private $skillRepository;
    private $characterClassRepository;

    public function __construct(SkillRepository $skillRepository, CharacterClassRepository $characterClassRepository)
    {
        parent::__construct(null);

        $this->skillRepository = $skillRepository;
        $this->characterClassRepository = $characterClassRepository;
    }

    protected function configure()
    {
        $this->setName('app:server-manager:dump-magic-list')
            ->addArgument('skill_list', InputArgument::REQUIRED, 'IGC_SkillList.xml path.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Dump Magic List to DB');

        $stopwatch = new Stopwatch();
        $stopwatch->start($this->getName());

        $count = $this->dump($io, file_get_contents($input->getArgument('skill_list')));

        $event = $stopwatch->stop($this->getName());

        $io->newLine();
        $io->success(
            sprintf(
                'Inserted %d Skills. Done in %.3f seconds, %.3f MB memory used.',
                $count,
                $event->getDuration() / 1000,
                $event->getMemory() / 1024 / 1024
            )
        );
    }

    private function dump(SymfonyStyle $io, string $skills): int
    {
        $skills = new SimpleXMLElement($skills);

        $this->skillRepository->removeAll();

        foreach ($skills as $skill) {
            $attr = $skill->attributes();

            $skill = (new SkillBuilder())
                ->withId((int) $attr->Index)
                ->withName((string) $attr->Name)
                ->withDamage((int) $attr->Damage)
                ->withManaUsage((int) $attr->ManaUsage)
                ->withStaminaUsage((int) $attr->BPUsage)
                ->withDistance((int) $attr->Distance)
                ->withDelay((int) $attr->Delay)
                ->withRequiredLevel((int) $attr->ReqLevel)
                ->withRequiredStrength((int) $attr->ReqStrength)
                ->withRequiredAgility((int) $attr->ReqDexterity)
                ->withRequiredEnergy((int) $attr->ReqEnergy)
                ->withRequiredCommand((int) $attr->ReqCommand)
                ->withRequiredMLPoints((int) $attr->ReqMLPoint)
                ->withIconNumber((int) $attr->IconNumber)
                ->withAttribute(SkillAttribute::byValue((int) $attr->Attribute))
                ->withType(SkillType::byValue((int) $attr->Type))
                ->withUseType(SkillUseType::byValue((int) $attr->UseType))
                ->withCharacterClasses($this->retrieveCharacterClasses($io, $attr))
                ->build();

            $this->skillRepository->save($skill);

            if ($io->isVerbose()) {
                $io->note(sprintf('Added new skill "%s".', $skill->name()));
            }
        }


        return count($skills);
    }

    private function retrieveCharacterClasses(SymfonyStyle $io, $attr): array
    {
        $types = [];

        $dw = (int) $attr->DarkWizard;
        $dk = (int) $attr->DarkKnight;
        $fe = (int) $attr->FairyElf;
        $mg = (int) $attr->MagicGladiator;
        $dl = (int) $attr->DarkLord;
        $sum = (int) $attr->Summoner;
        $rf = (int) $attr->RageFighter;

        if (0 !== $dw) {
            if (1 === $dw) {
                $types[] = CharacterClass::DARK_WIZARD;
            }
            if (2 === $dw) {
                $types[] = CharacterClass::SOUL_MASTER;
            }
            if (3 === $dw) {
                $types[] = CharacterClass::GRAND_MASTER;
            }
        }

        if (0 !== $dk) {
            if (1 === $dk) {
                $types[] = CharacterClass::DARK_KNIGHT;
            }
            if (2 === $dk) {
                $types[] = CharacterClass::BLADE_KNIGHT;
            }
            if (3 === $dk) {
                $types[] = CharacterClass::BLADE_MASTER;
            }
        }

        if (0 !== $fe) {
            if (1 === $fe) {
                $types[] = CharacterClass::ELF;
            }
            if (2 === $fe) {
                $types[] = CharacterClass::MUSE_ELF;
            }
            if (3 === $fe) {
                $types[] = CharacterClass::HIGH_ELF;
            }
        }

        if (0 !== $mg) {
            if (1 === $mg) {
                $types[] = CharacterClass::MAGIC_GLADIATOR;
            }
            if (3 === $mg) {
                $types[] = CharacterClass::DUEL_MASTER;
            }
        }

        if (0 !== $dl) {
            if (1 === $dl) {
                $types[] = CharacterClass::DARK_LORD;
            }
            if (3 === $dl) {
                $types[] = CharacterClass::LORD_EMPEROR;
            }
        }

        if (0 !== $sum) {
            if (1 === $sum) {
                $types[] = CharacterClass::SUMMONER;
            }
            if (2 === $sum) {
                $types[] = CharacterClass::BLOODY_SUMMONER;
            }
            if (3 === $sum) {
                $types[] = CharacterClass::DIMENSION_MASTER;
            }
        }

        if (0 !== $rf) {
            if (1 === $rf) {
                $types[] = CharacterClass::RAGE_FIGHTER;
            }
            if (3 === $rf) {
                $types[] = CharacterClass::FIST_MASTER;
            }
        }

        if (!count($types)) {
            return [];
        }

        return $this->characterClassRepository->findByTypes($types);
    }
}




































