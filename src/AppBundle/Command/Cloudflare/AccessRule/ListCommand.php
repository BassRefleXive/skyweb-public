<?php

namespace AppBundle\Command\Cloudflare\AccessRule;

use AppBundle\Cloudflare\Model\AccessRule;
use AppBundle\Cloudflare\Service\AccessRuleManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;

class ListCommand extends Command
{
    private $manager;

    public function __construct(AccessRuleManager $manager)
    {
        parent::__construct(null);

        $this->manager = $manager;
    }

    protected function configure()
    {
        $this
            ->setName('app:cloudflare:access-rule:list')
            ->setDescription('List all access rules.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('List all access rules');

        $stopwatch = new Stopwatch();
        $stopwatch->start($this->getName());

        $event = $stopwatch->stop($this->getName());

        $rules = $this->manager->all();

        $io->table(
            [
                'ID',
                'Notes',
                'Mode',
                'Type',
                'Target',
            ],
            array_map(function (AccessRule $model): array {
                return [
                    $model->id(),
                    $model->notes(),
                    $model->mode()->getName(),
                    $model->type()->getName(),
                    $model->target(),
                ];
            }, $rules)
        );

        $io->newLine();
        $io->success(
            sprintf(
                'Done in %.3f seconds, %.3f MB memory used.',
                $event->getDuration() / 1000,
                $event->getMemory() / 1024 / 1024
            )
        );
    }
}