<?php

namespace AppBundle\Command\Cloudflare\AccessRule;

use AppBundle\Cloudflare\Enum\AccessRule\Mode;
use AppBundle\Cloudflare\Enum\AccessRule\TargetType;
use AppBundle\Cloudflare\Model\AccessRule;
use AppBundle\Cloudflare\Service\AccessRuleManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;

class CreateCommand extends Command
{
    private $manager;

    public function __construct(AccessRuleManager $manager)
    {
        parent::__construct(null);

        $this->manager = $manager;
    }

    protected function configure()
    {
        $this
            ->setName('app:cloudflare:access-rule:create')
            ->addArgument('mode', InputArgument::REQUIRED)
            ->addArgument('type', InputArgument::REQUIRED)
            ->addArgument('target', InputArgument::REQUIRED)
            ->addArgument('notes', InputArgument::OPTIONAL)
            ->setDescription('Create new Access Rule.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('List all access rules');

        $stopwatch = new Stopwatch();
        $stopwatch->start($this->getName());

        $rule = $this->manager->create(
            Mode::byName($input->getArgument('mode')),
            TargetType::byName($input->getArgument('type')),
            $input->getArgument('target'),
            $input->hasArgument('notes')
                ? $input->getArgument('notes')
                : null
        );

        $io->table(
            [
                'ID',
                'Notes',
                'Mode',
                'Type',
                'Target',
            ],
            [
                [
                    $rule->id(),
                    $rule->notes(),
                    $rule->mode()->getName(),
                    $rule->type()->getName(),
                    $rule->target(),
                ]
            ]
        );

        $event = $stopwatch->stop($this->getName());

        $io->newLine();
        $io->success(
            sprintf(
                'Done in %.3f seconds, %.3f MB memory used.',
                $event->getDuration() / 1000,
                $event->getMemory() / 1024 / 1024
            )
        );
    }
}