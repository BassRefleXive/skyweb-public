<?php

namespace AppBundle\Doctrine\EntityListeners;


use AppBundle\Entity\MuOnline\Account;
use AppBundle\Entity\MuOnline\Warehouse;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AccountListener
{

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function postLoad(Account $account)
    {
        if ($account->getWarehouse() === null) {

            $warehouse = (new Warehouse())
                ->setAccount($account)
                ->setItems($this->container->get('sky_app.muo.service.warehouse')->buildEmptyStorage());

            $account->setWarehouse($warehouse);
        }
    }

}