<?php

namespace AppBundle\Doctrine\Filters;

use AppBundle\Entity\Application\News;
use AppBundle\Entity\Application\WebShopCategory;
use AppBundle\Entity\Application\CartDiscountCoupon;
use AppBundle\Repository\Application\NewsRepository;
use AppBundle\Repository\Application\WebShopCategoryRepository;
use AppBundle\Repository\Application\CartDiscountCouponRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

class ActiveEntityFilter extends SQLFilter
{
    const NAME = 'activeEntity';

    protected $activeEntities = [
        News::class               => [['field' => 'status', 'value' => NewsRepository::STATUS_ENABLED]],
        WebShopCategory::class    => [['field' => 'status', 'value' => WebShopCategoryRepository::STATUS_ENABLED]],
        CartDiscountCoupon::class => [['field' => 'status', 'value' => CartDiscountCouponRepository::STATUS_ENABLED]],
    ];

    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        $entityName = $targetEntity->reflClass->getName();

        if (!array_key_exists($entityName, $this->activeEntities)) {
            return '';
        }

        $filters = [];
        foreach ($this->activeEntities[$entityName] as $filterData) {
            $filters[] = sprintf(
                '%s.%s %s "%s"',
                $targetTableAlias,
                $filterData['field'],
                $filterData['compare'] ?? '=',
                $filterData['value']
            );
        }

        return implode(' AND ', $filters);
    }
}