<?php

namespace AppBundle\Doctrine\Filters;

use AppBundle\Entity\Application\News;
use AppBundle\Entity\Application\Server;
use AppBundle\Entity\Application\WebShopCategory;
use AppBundle\Entity\Application\CartDiscountCoupon;
use AppBundle\Repository\Application\NewsRepository;
use AppBundle\Repository\Application\WebShopCategoryRepository;
use AppBundle\Repository\Application\CartDiscountCouponRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

class ActiveServerFilter extends SQLFilter
{
    const NAME = 'activeServer';

    protected $activeEntities = [
        Server::class             => [['field' => 'enabled', 'value' => 1]],
    ];

    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        $entityName = $targetEntity->reflClass->getName();

        if (!array_key_exists($entityName, $this->activeEntities)) {
            return '';
        }

        $filters = [];
        foreach ($this->activeEntities[$entityName] as $filterData) {
            $filters[] = sprintf(
                '%s.%s %s "%s"',
                $targetTableAlias,
                $filterData['field'],
                $filterData['compare'] ?? '=',
                $filterData['value']
            );
        }

        return implode(' AND ', $filters);
    }
}