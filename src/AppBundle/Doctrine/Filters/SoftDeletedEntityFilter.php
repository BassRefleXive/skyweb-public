<?php

declare(strict_types = 1);

namespace AppBundle\Doctrine\Filters;


use Doctrine\ORM\Query\Filter\SQLFilter;
use Doctrine\ORM\Mapping\ClassMetadata;

class SoftDeletedEntityFilter extends SQLFilter
{
    const NAME = 'softDeleted';
    const FIELD = 'deletedAt';

    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        if ($targetEntity->hasField(self::FIELD)) {
            return sprintf(
                '%s.%s IS NULL',
                $targetTableAlias,
                $targetEntity->getColumnName(self::FIELD)
            );
        }

        return '';
    }
}