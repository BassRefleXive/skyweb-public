<?php

namespace AppBundle\Doctrine\Type;

use AppBundle\Doctrine\Type\Mappings\MagicList\Builder\MagicListBuilder;
use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class MagicList extends Type
{
    protected $name = 'muo_magic_list';
    protected $storageSize;

    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return "VARBINARY";
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        return (new MagicListBuilder())
            ->withHex($value)
            ->build();
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return (string)$value;
    }

    public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform)
    {
        return sprintf('CONVERT(VARBINARY(MAX), %s, 2)', $sqlExpr);
    }

    public function convertToPHPValueSQL($sqlExpr, $platform)
    {
        return sprintf('CAST(%s AS VARBINARY(MAX))', $sqlExpr);
    }

    public function canRequireSQLConversion()
    {
        return true;
    }

    public function getName()
    {
        return $this->name;
    }
}