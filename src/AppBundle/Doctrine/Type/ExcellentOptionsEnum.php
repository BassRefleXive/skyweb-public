<?php

namespace AppBundle\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;

class ExcellentOptionsEnum extends Enum
{
    const EXC_WEAPON_MANA = 1;
    const EXC_WEAPON_LIFE = 2;
    const EXC_WEAPON_SPEED = 4;
    const EXC_WEAPON_DMG = 8;
    const EXC_WEAPON_DMG_LVL_20 = 16;
    const EXC_WEAPON_RATE = 32;

    const EXC_ARMOR_ZEN = 1;
    const EXC_ARMOR_DEF = 2;
    const EXC_ARMOR_REF = 4;
    const EXC_ARMOR_DD = 8;
    const EXC_ARMOR_MANA = 16;
    const EXC_ARMOR_HP = 32;

    const EXC_WINGS_2_LIFE = 1;
    const EXC_WINGS_2_MANA = 2;
    const EXC_WINGS_2_IGNORE = 4;
    const EXC_WINGS_2_STAMINA = 8;
    const EXC_WINGS_2_COMMAND = 8;
    const EXC_WINGS_2_WIZ_SPEED = 16;

    const EXC_FENRIR_INC_DMG = 1;
    const EXC_FENRIR_ABSORB_DMG = 2;
    const EXC_FENRIR_INC_ABSORB_DMG = 4;

    const EXC_WINGS_3_IGNORE = 1;
    const EXC_WINGS_3_RETURN = 2;
    const EXC_WINGS_3_LIFE_RECOVER = 4;
    const EXC_WINGS_3_MANA_RECOVER = 8;

    protected $name = 'enum_excellent_option';

    protected $values = [
        ExcellentOptionsEnum::EXC_WEAPON_MANA,
        ExcellentOptionsEnum::EXC_WEAPON_LIFE,
        ExcellentOptionsEnum::EXC_WEAPON_SPEED,
        ExcellentOptionsEnum::EXC_WEAPON_DMG,
        ExcellentOptionsEnum::EXC_WEAPON_DMG_LVL_20,
        ExcellentOptionsEnum::EXC_WEAPON_RATE,

        ExcellentOptionsEnum::EXC_ARMOR_ZEN,
        ExcellentOptionsEnum::EXC_ARMOR_DEF,
        ExcellentOptionsEnum::EXC_ARMOR_REF,
        ExcellentOptionsEnum::EXC_ARMOR_DD,
        ExcellentOptionsEnum::EXC_ARMOR_MANA,
        ExcellentOptionsEnum::EXC_ARMOR_HP,

        ExcellentOptionsEnum::EXC_WINGS_2_LIFE,
        ExcellentOptionsEnum::EXC_WINGS_2_MANA,
        ExcellentOptionsEnum::EXC_WINGS_2_IGNORE,
        ExcellentOptionsEnum::EXC_WINGS_2_COMMAND,
        ExcellentOptionsEnum::EXC_WINGS_2_STAMINA,
        ExcellentOptionsEnum::EXC_WINGS_2_WIZ_SPEED,

        ExcellentOptionsEnum::EXC_FENRIR_INC_DMG,
        ExcellentOptionsEnum::EXC_FENRIR_ABSORB_DMG,
        ExcellentOptionsEnum::EXC_FENRIR_INC_ABSORB_DMG,

        ExcellentOptionsEnum::EXC_WINGS_3_IGNORE,
        ExcellentOptionsEnum::EXC_WINGS_3_RETURN,
        ExcellentOptionsEnum::EXC_WINGS_3_LIFE_RECOVER,
        ExcellentOptionsEnum::EXC_WINGS_3_MANA_RECOVER,
    ];

    static public function getWeaponTypes()
    {
        return [
            ExcellentOptionsEnum::EXC_WEAPON_MANA       => 'Mana After Hunting Monsters +mana/8',
            ExcellentOptionsEnum::EXC_WEAPON_LIFE       => 'Life After Hunting Monsters +life/8',
            ExcellentOptionsEnum::EXC_WEAPON_SPEED      => 'Increase Attacking(Wizardy) speed +7',
            ExcellentOptionsEnum::EXC_WEAPON_DMG        => 'Increase Damage +2%',
            ExcellentOptionsEnum::EXC_WEAPON_DMG_LVL_20 => 'Increase Damage +Level/20',
            ExcellentOptionsEnum::EXC_WEAPON_RATE       => 'Excellent Damage Rate +10%',
        ];
    }

    static public function getArmorTypes()
    {
        return [
            ExcellentOptionsEnum::EXC_ARMOR_ZEN  => 'Increase Rate of Zen 40%',
            ExcellentOptionsEnum::EXC_ARMOR_DEF  => 'Defense Success Rate +10%',
            ExcellentOptionsEnum::EXC_ARMOR_REF  => 'Reflect Damage +5%',
            ExcellentOptionsEnum::EXC_ARMOR_DD   => 'Damage Decrease +4%',
            ExcellentOptionsEnum::EXC_ARMOR_MANA => 'Increase Max Mana +4%',
            ExcellentOptionsEnum::EXC_ARMOR_HP   => 'Increase Max Hp +4%',
        ];
    }

    static public function getSecondWingsTypes()
    {
        return [
            ExcellentOptionsEnum::EXC_WINGS_2_LIFE      => 'Increase Life',
            ExcellentOptionsEnum::EXC_WINGS_2_MANA      => 'Increase Mana',
            ExcellentOptionsEnum::EXC_WINGS_2_IGNORE    => 'Ignore Enemy\'s defense 3%',
            ExcellentOptionsEnum::EXC_WINGS_2_STAMINA   => '+50 Max Stamina',
            ExcellentOptionsEnum::EXC_WINGS_2_WIZ_SPEED => 'Wizardry Speed +5',
        ];
    }

    static public function getCapeOfLordTypes()
    {
        return [
            ExcellentOptionsEnum::EXC_WINGS_2_LIFE      => 'Increase Life',
            ExcellentOptionsEnum::EXC_WINGS_2_MANA      => 'Increase Mana',
            ExcellentOptionsEnum::EXC_WINGS_2_IGNORE    => 'Ignore Enemy\'s defense 3%',
            ExcellentOptionsEnum::EXC_WINGS_2_COMMAND   => 'Increase command',
        ];
    }

    static public function getCapeOfFighterTypes()
    {
        return [
            ExcellentOptionsEnum::EXC_WINGS_2_LIFE      => 'Increase Life',
            ExcellentOptionsEnum::EXC_WINGS_2_MANA      => 'Increase Mana',
            ExcellentOptionsEnum::EXC_WINGS_2_IGNORE    => 'Ignore Enemy\'s defense 3%',
        ];
    }

    static public function getFenrirTypes()
    {
        return [
            ExcellentOptionsEnum::EXC_FENRIR_INC_DMG        => ['Plazma Storm Skill', 'Increase final damage 10%'],
            ExcellentOptionsEnum::EXC_FENRIR_ABSORB_DMG     => ['Plazma Storm Skill', 'Absorb final damage 10%'],
            ExcellentOptionsEnum::EXC_FENRIR_INC_ABSORB_DMG => ['Plazma Storm Skill', 'Increase final damage 10%', 'Absorb final damage 10%'],
        ];
    }

    static public function getThirdWingsTypes()
    {
        return [
            ExcellentOptionsEnum::EXC_WINGS_3_IGNORE       => 'Ignor opponent\'s defensive power by 5%',
            ExcellentOptionsEnum::EXC_WINGS_3_RETURN       => 'Return\'s the enemy\'s attack power in 5%',
            ExcellentOptionsEnum::EXC_WINGS_3_LIFE_RECOVER => 'Complete recovery of life in 5% rate',
            ExcellentOptionsEnum::EXC_WINGS_3_MANA_RECOVER => 'Complete recover of Mana in 5% rate',
        ];
    }

    static public function rubyRingTypes(): array
    {
        return [
            4 => 'Increases automatic HP recovery rate by +8%',
            2 => 'Increases maximum HP by 6%',
        ];
    }

    static public function sapphireRingTypes(): array
    {
        return [
            4 => 'Increases automatic HP recovery rate by +8%',
            1 => 'Increases maximum mana by 6%',
        ];
    }

    static public function topazRingTypes(): array
    {
        return [
            1 => [
                'Increases automatic HP recovery rate by +8%',
                'Increases the amount of Zen received for killing monsters by 10%',
            ],
        ];
    }

    static public function amethystRingTypes(): array
    {
        return [
            4 => [
                'Increases automatic HP recovery rate by +8%',
                'Decreases damage received by 6%',
            ]
        ];
    }

    static public function rubyNecklaceTypes(): array
    {
        return [
            4 => 'Increases automatic HP recovery rate by +8%',
            2 => 'Increases your damage chance of doing Excellent damage by 5%',
        ];
    }

    static public function sapphireNecklaceTypes(): array
    {
        return [
            1 => ['Increases automatic HP recovery rate by +8%'],
        ];
    }

    static public function emeraldNecklaceTypes(): array
    {
        return [
            1 => 'Increases automatic HP recovery rate by +8%',
            2 => 'Increases attack speed by 10',
        ];
    }

    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return "INTEGER";
    }
}