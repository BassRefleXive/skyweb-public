<?php

namespace AppBundle\Doctrine\Type;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class GuildMark extends Type
{
    protected $name = 'muo_guild_mark';
    protected $storageSize;

    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return "VARBINARY";
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        $value = new \AppBundle\Doctrine\Type\Mappings\GuildMark($value);

        return $value;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return (string)$value;
    }

    public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform)
    {
        return 'CONVERT(varbinary(MAX), ' . $sqlExpr . ', 2)';
    }

    public function canRequireSQLConversion()
    {
        return true;
    }

    public function getName()
    {
        return $this->name;
    }
}