<?php

namespace AppBundle\Doctrine\Type;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use AppBundle\Doctrine\Type\Mappings\Items\Items as StorageItems;

class Items extends Type
{
    protected $name = 'muo_items';
    protected $storageSize;

    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return "VARBINARY";
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        /** @var \AppBundle\Doctrine\Type\Mappings\Items\Items $value */
        $value = StorageItems::buildFromString($value);
        $this->storageSize = $value->getStorageSize();

        return $value;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return (string)$value;
    }

    public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform)
    {
        return sprintf('CONVERT(VARBINARY(MAX), %s, 2)', $sqlExpr);
    }

    public function convertToPHPValueSQL($sqlExpr, $platform)
    {
        return sprintf('CAST(%s AS VARBINARY(MAX))', $sqlExpr);
    }

    public function canRequireSQLConversion()
    {
        return true;
    }

    public function getName()
    {
        return $this->name;
    }
}