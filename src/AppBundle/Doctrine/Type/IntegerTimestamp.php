<?php


namespace AppBundle\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

/**
 * It is used to support SQL Server's smalldatetime format
 */
class IntegerTimestamp extends Type
{
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return 'integer';
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?\DateTime
    {
        return (new \DateTime())->setTimestamp($value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        /** @var \DateTime $value */
        return null !== $value
            ? $value->getTimestamp()
            : null;
    }

    public function getName(): string
    {
        return 'integer_timestamp';
    }
}