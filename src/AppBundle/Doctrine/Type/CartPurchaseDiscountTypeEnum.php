<?php

namespace AppBundle\Doctrine\Type;


class CartPurchaseDiscountTypeEnum extends Enum
{
    const PURCHASE_DISCOUNT_TYPE_COUPON = 'C';
    const PURCHASE_DISCOUNT_TYPE_PERSISTENT_DISCOUNT = 'P';

    protected $name = 'enum_cart_purchase_discount_type';
    protected $values = [
        self::PURCHASE_DISCOUNT_TYPE_COUPON,
        self::PURCHASE_DISCOUNT_TYPE_PERSISTENT_DISCOUNT,
    ];


    static function getKeyByName($name)
    {
        $name = strtolower($name);

        $types = static::getTypes();
        $keys = array_flip($types);
        $keys = array_change_key_case($keys, CASE_LOWER);

        if (!array_key_exists($name, $keys)) {
            throw new \InvalidArgumentException('Given webshop purchase discount type is not supported.');
        }

        return $keys[$name];
    }

    static function getTypes()
    {
        return [
            self::PURCHASE_DISCOUNT_TYPE_COUPON              => 'Coupon',
            self::PURCHASE_DISCOUNT_TYPE_PERSISTENT_DISCOUNT => 'Persistent Discount',
        ];
    }

    static function getNameByKey($key)
    {
        $types = static::getTypes();

        if (!array_key_exists($key, $types)) {
            throw new \InvalidArgumentException('Given webshop purchase discount type is not supported');
        }

        return $types[$key];
    }
}