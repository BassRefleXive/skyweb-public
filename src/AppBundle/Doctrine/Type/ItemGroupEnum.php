<?php

namespace AppBundle\Doctrine\Type;


use Doctrine\DBAL\Platforms\AbstractPlatform;

class ItemGroupEnum extends Enum
{
    const ITEM_GROUP_SWORD = 0;
    const ITEM_GROUP_AXE = 1;
    const ITEM_GROUP_SCEPTER = 2;
    const ITEM_GROUP_SPEAR = 3;
    const ITEM_GROUP_BOW = 4;
    const ITEM_GROUP_STAFF = 5;
    const ITEM_GROUP_SHIELD = 6;
    const ITEM_GROUP_HELM = 7;
    const ITEM_GROUP_ARMOR = 8;
    const ITEM_GROUP_PANTS = 9;
    const ITEM_GROUP_GLOVES = 10;
    const ITEM_GROUP_BOOTS = 11;
    const ITEM_GROUP_12 = 12;
    const ITEM_GROUP_13 = 13;
    const ITEM_GROUP_14 = 14;
    const ITEM_GROUP_SCROLLS = 15;


    protected $name = 'enum_item_group';
    protected $values = [
        ItemGroupEnum::ITEM_GROUP_SWORD,
        ItemGroupEnum::ITEM_GROUP_AXE,
        ItemGroupEnum::ITEM_GROUP_SCEPTER,
        ItemGroupEnum::ITEM_GROUP_SPEAR,
        ItemGroupEnum::ITEM_GROUP_BOW,
        ItemGroupEnum::ITEM_GROUP_STAFF,
        ItemGroupEnum::ITEM_GROUP_SHIELD,
        ItemGroupEnum::ITEM_GROUP_HELM,
        ItemGroupEnum::ITEM_GROUP_ARMOR,
        ItemGroupEnum::ITEM_GROUP_PANTS,
        ItemGroupEnum::ITEM_GROUP_GLOVES,
        ItemGroupEnum::ITEM_GROUP_BOOTS,
        ItemGroupEnum::ITEM_GROUP_12,
        ItemGroupEnum::ITEM_GROUP_13,
        ItemGroupEnum::ITEM_GROUP_14,
        ItemGroupEnum::ITEM_GROUP_SCROLLS,
    ];


    static function getKeyByName($name)
    {
        $name = strtolower($name);

        $types = static::getTypes();
        $keys = array_flip($types);
        $keys = array_change_key_case($keys, CASE_LOWER);

        if (!array_key_exists($name, $keys)) {
            throw new \InvalidArgumentException('Given item group is not supported.');
        }

        return $keys[$name];
    }

    static function getTypes()
    {
        return [
            ItemGroupEnum::ITEM_GROUP_SWORD   => 'Swords',
            ItemGroupEnum::ITEM_GROUP_AXE     => 'Axes',
            ItemGroupEnum::ITEM_GROUP_SCEPTER => 'Scepters',
            ItemGroupEnum::ITEM_GROUP_SPEAR   => 'Spears',
            ItemGroupEnum::ITEM_GROUP_BOW     => 'Bows / Crossbows',
            ItemGroupEnum::ITEM_GROUP_STAFF   => 'Staffs',
            ItemGroupEnum::ITEM_GROUP_SHIELD  => 'Shields',
            ItemGroupEnum::ITEM_GROUP_HELM    => 'Helms',
            ItemGroupEnum::ITEM_GROUP_ARMOR   => 'Armors',
            ItemGroupEnum::ITEM_GROUP_PANTS   => 'Pants',
            ItemGroupEnum::ITEM_GROUP_GLOVES  => 'Gloves',
            ItemGroupEnum::ITEM_GROUP_BOOTS   => 'Bots',
            ItemGroupEnum::ITEM_GROUP_12      => '12',
            ItemGroupEnum::ITEM_GROUP_13      => '13',
            ItemGroupEnum::ITEM_GROUP_14      => '14',
            ItemGroupEnum::ITEM_GROUP_SCROLLS => 'Scrolls',
        ];
    }

    static function getNameByKey($key)
    {
        $types = static::getTypes();

        if (!array_key_exists($key, $types)) {
            throw new \InvalidArgumentException('Given item group is not supported');
        }

        return $types[$key];
    }

    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return "INTEGER";
    }
}