<?php


namespace AppBundle\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

/**
 * It is used to support SQL Server's smalldatetime format
 */
class SmallDateTime extends Type
{
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return 'smalldatetime';
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?\DateTime
    {
        return new \DateTime($value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        /** @var \DateTime $value */
        return null !== $value
            ? $value->format('Y-m-d H:i:s')
            : null;
    }

    public function getName(): string
    {
        return 'smalldatetime';
    }
}