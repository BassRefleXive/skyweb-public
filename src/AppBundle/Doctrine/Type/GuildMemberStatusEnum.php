<?php

namespace AppBundle\Doctrine\Type;


class GuildMemberStatusEnum extends Enum
{
    const GUILD_MEMBER_STATUS_MEMBER = 0;
    const GUILD_MEMBER_STATUS_BATTLE_MASTER = 32;
    const GUILD_MEMBER_STATUS_ASSISTANT = 64;
    const GUILD_MEMBER_STATUS_GUILD_MASTER = 128;

    protected $name = 'enum_guild_member_status';
    protected $values = [
        GuildMemberStatusEnum::GUILD_MEMBER_STATUS_MEMBER,
        GuildMemberStatusEnum::GUILD_MEMBER_STATUS_BATTLE_MASTER,
        GuildMemberStatusEnum::GUILD_MEMBER_STATUS_ASSISTANT,
        GuildMemberStatusEnum::GUILD_MEMBER_STATUS_GUILD_MASTER,
    ];


    static function getKeyByName($name)
    {
        $name = strtolower($name);

        $types = static::getTypes();
        $keys = array_flip($types);
        $keys = array_change_key_case($keys, CASE_LOWER);

        if (!array_key_exists($name, $keys)) {
            throw new \InvalidArgumentException('Given guild member status is not supported.');
        }

        return $keys[$name];
    }

    static function getTypes()
    {
        return [
            GuildMemberStatusEnum::GUILD_MEMBER_STATUS_MEMBER        => 'Member',
            GuildMemberStatusEnum::GUILD_MEMBER_STATUS_BATTLE_MASTER => 'Battle Master',
            GuildMemberStatusEnum::GUILD_MEMBER_STATUS_ASSISTANT     => 'Guild Master Assistant',
            GuildMemberStatusEnum::GUILD_MEMBER_STATUS_GUILD_MASTER  => 'Guild Master',
        ];
    }

    static function getNameByKey($key)
    {
        $types = static::getTypes();

        if (!array_key_exists($key, $types)) {
            throw new \InvalidArgumentException('Given guild member status is not supported');
        }

        return $types[$key];
    }

}