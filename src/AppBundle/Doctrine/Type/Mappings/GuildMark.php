<?php

namespace AppBundle\Doctrine\Type\Mappings;

class GuildMark
{

    private static $colors = [
        '0' => [0, 0, 0],
        '1' => [0, 0, 0],
        '2' => [128, 128, 128],
        '3' => [255, 255, 255],
        '4' => [255, 0, 0],
        '5' => [255, 128, 0],
        '6' => [255, 255, 0],
        '7' => [128, 255, 0],
        '8' => [0, 255, 0],
        '9' => [0, 255, 128],
        'a' => [0, 255, 255],
        'b' => [0, 128, 255],
        'c' => [0, 0, 255],
        'd' => [128, 0, 255],
        'e' => [255, 0, 255],
        'f' => [255, 0, 128],
    ];

    private static $defaultColor = [255, 255, 255];

    protected $hex;

    protected $cells;

    public function __construct(string $hex)
    {
        $hex = strtoupper(bin2hex($hex));

        if (substr($hex, 0, 2) == '0x') {
            $hex = substr($hex, 2);
        }

        $this->hex = $hex;
    }

    public function getCellsColors(): array
    {
        if ($this->cells === null) {
            $this->buildCells();
        }

        return $this->cells;
    }

    protected function buildCells()
    {
        for ($y = 0; $y < 8; $y++) {
            for ($x = 0; $x < 8; $x++) {
                $cellColorCode = mb_strtolower(substr($this->hex, ($y * 8) + $x, 1));

                $rgbColor = static::$colors[$cellColorCode] ?? static::$defaultColor;

                $this->cells[$y][$x] = $this->getHexColor($rgbColor);
            }
        }
    }

    private function getHexColor($color)
    {
        $rgb = dechex(($color[0] << 16) | ($color[1] << 8) | $color[2]);

        return ("#" . substr("000000" . $rgb, -6));
    }

    public function __toString()
    {
        return $this->hex;
    }

}