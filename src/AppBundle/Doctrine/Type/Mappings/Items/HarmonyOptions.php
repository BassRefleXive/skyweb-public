<?php

namespace AppBundle\Doctrine\Type\Mappings\Items;


use AppBundle\Entity\Application\Items\GroupHarmonyOptionLevel;
use AppBundle\Entity\Application\Items\HarmonyOption;

class HarmonyOptions
{

    protected $option;
    protected $level;

    public function getOption()
    {
        return $this->option;
    }

    public function setOption(HarmonyOption $option): HarmonyOptions
    {
        $this->option = $option;

        return $this;
    }

    public function getLevel()
    {
        return $this->level;
    }

    public function setLevel(GroupHarmonyOptionLevel $level): HarmonyOptions
    {
        $this->level = $level;

        return $this;
    }

}