<?php


namespace AppBundle\Doctrine\Type\Mappings\Items;


use AppBundle\Doctrine\Type\Mappings\Items\Builder\SocketOptionsBuilder;
use AppBundle\Entity\Application\Items\SocketOption;

class SocketOptions
{
    private $slots;

    public function __construct(SocketOptionsBuilder $builder)
    {
        $this->slots = $builder->slots();
    }

    public function slots(): array
    {
        return $this->slots;
    }

    public function slot(int $slot): ?SocketOption
    {
        return $this->slots[$slot];
    }

    public function isSocket(): bool
    {
        foreach ($this->slots as $slot) {
            if (null !== $slot) {
                return true;
            }
        }

        return false;
    }
}