<?php

namespace AppBundle\Doctrine\Type\Mappings\Items;


use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Exception\ApplicationException;
use Collection\Sequence;

class Items extends Sequence
{
    const SPACE_NOT_FOUND = -1;

    const STORAGE_TYPE_WAREHOUSE = 240;
    const STORAGE_TYPE_INVENTORY = 236;

    const COLS_COUNT = 8;

    protected $storageSize;
    protected $storageType;

    public function __construct(array $elements = [], int $storageSize, int $storageType)
    {
        parent::__construct($elements);
        $this->storageSize = $storageSize;
        $this->storageType = $storageType;
    }

    public static function buildEmpty(int $length): Items
    {
        $string = hex2bin(str_pad('', $length, 'F'));

        return static::buildFromString($string);
    }

    public static function buildFromString(string $items): Items
    {
        $items = strtoupper(bin2hex($items));

        if (substr($items, 0, 2) == '0x') {
            $items = substr($items, 2);
        }

        $storageSize = mb_strlen($items);
        $storageType = Items::calculateStorageType($storageSize);
        $itemSize = Items::calculateItemSize($storageSize, $storageType);

        $collection = new self([], $storageSize, $storageType);

        for ($i = 0; $i < $storageType; $i++) {
            $hex = substr($items, ($itemSize * $i), $itemSize);
            $collection->add(Item::createExisting()->setHex($hex));
        }

        return $collection;
    }

    public function removeFromStorage(Item $item): bool
    {
        if ($item->getItemInfo() === null) {
            throw new ApplicationException('ItemInfo must be set in Item.');
        }

        if (($index = $this->indexOf($item)) !== -1) {
            $this->elements[$index] = Item::createNew($this->getItemSize());

            return true;
        }

        return false;
    }

    public function addToStorage(Item $item): bool
    {
        if ($item->getItemInfo() === null) {
            throw new ApplicationException('ItemInfo must be set in Item.');
        }

        $index = $this->findIndexForItem($item);

        if ($index !== Items::SPACE_NOT_FOUND) {
            $this->elements[$index] = $item;

            return true;
        }

        return false;
    }

    private function findIndexForItem(Item $item): int
    {
        $busyCells = $this->buildBusyCellsVector();

        $result = Items::SPACE_NOT_FOUND;

        /** @var Item $element */
        foreach ($this->elements as $idx => $element) {
            if (!$element->isEmpty()) {
                continue;
            }
            $itemX = $item->getItemInfo()->getX();
            $itemY = $item->getItemInfo()->getY();

            $currentRowLastIdx = (floor(($idx) / Items::COLS_COUNT) + 1) * Items::COLS_COUNT - 1;
            $itemInThisIdxLastX = $idx + $itemX - 1;

            $tmpRes = $itemInThisIdxLastX <= $currentRowLastIdx;

            $tmpRes && $this->itemCoordsWalker($idx, $itemX, $itemY, function (int $idx) use ($busyCells, &$tmpRes) {
                $tmpRes = $tmpRes && ($busyCells[$idx] ?? false);
            });
            if ($tmpRes) {
                $result = $idx;
                break;
            }
        }

        return $result;
    }

    private function buildBusyCellsVector(): array
    {
        $result = array_fill(0, $this->storageType, true);

        /** @var Item $element */
        foreach ($this->elements as $idx => $element) {
            if (!$element->isEmpty() && $element->getItemInfo() === null) {
                throw new ApplicationException('Before adding items to storage, its items must be parsed first.');
            }
            if (!$element->isEmpty()) {
                $result[$idx] = false;
                $this->itemCoordsWalker($idx, $element->getItemInfo()->getX(), $element->getItemInfo()->getY(), function (int $idx) use (&$result) {
                    $result[$idx] = false;
                });
            }
        }

        return $result;
    }

    private function itemCoordsWalker(int $idx, int $x, int $y, callable $callback)
    {
        $i = 0;
        while ($i < $y) {
            $j = $idx + ($i * Items::COLS_COUNT);
            $maxX = $idx + $x + ($i * Items::COLS_COUNT);
            while ($j < $maxX) {
                $callback($j);
                $j++;
            }
            $i++;
        }
    }

    public function getItemSize()
    {
        return static::calculateItemSize($this->storageSize, $this->storageType);
    }

    public function getStorageSize(): int
    {
        return $this->storageSize;
    }

    public function getStorageType(): int
    {
        return $this->storageType;
    }

    public function getColsCount(): int
    {
        return Items::COLS_COUNT;
    }

    protected static function calculateItemSize(int $storageSize, int $storageType): int
    {
        return Item::ITEM_SIZE_64;

//        $itemSize = $storageSize / $storageType;
//
//        if (!in_array(
//            $itemSize,
//            [
//                Item::ITEM_SIZE_16,
//                Item::ITEM_SIZE_32,
//                Item::ITEM_SIZE_64,
//            ],
//            true
//        )
//        ) {
//            throw new ApplicationException('Unknown item size.');
//        }
//
//        if ($itemSize === Item::ITEM_SIZE_16) {
//            throw new ApplicationException('Application currently does not support items with size ' . Item::ITEM_SIZE_16 . '.');
//        }
//
//        return $itemSize;
    }

    protected static function calculateStorageType(int $storageSize): int
    {
        return in_array($storageSize, [1920, 3840, 7680, 15360], true)
            ? Items::STORAGE_TYPE_WAREHOUSE
            : Items::STORAGE_TYPE_INVENTORY;
    }

    public function isValid(): bool
    {
        return $this->length() === $this->storageType;
    }

    public function __toString(): string
    {
        if (!$this->isValid()) {
            throw new ApplicationException('Unexpected items count. Please make sure you don\'t add extra items to storage.');
        }

        $result = implode('', $this->map(function (Item $item) {
            return $item->getHex();
        })->all());

        return strtolower($result);
    }

    protected function createNew($elements)
    {
        return new static($elements, $this->storageSize, $this->storageType);
    }

}