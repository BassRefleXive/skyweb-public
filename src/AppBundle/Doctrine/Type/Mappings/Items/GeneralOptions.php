<?php

namespace AppBundle\Doctrine\Type\Mappings\Items;


class GeneralOptions extends AbstractOptions
{
    const DEFAULT_GROUP = 0;
    const DEFAULT_ID = 0;
    const DEFAULT_LEVEL = 0;
    const DEFAULT_OPTION = 0;
    const DEFAULT_SKILL = false;
    const DEFAULT_LUCK = false;
    const DEFAULT_DURABILITY = 0;

    protected $group;
    protected $id;

    protected $serialNumber;

    protected $lvl;
    protected $opt;
    protected $isSkill;
    protected $isLuck;

    protected $durability;

    public function __construct()
    {
        $this->group = GeneralOptions::DEFAULT_GROUP;
        $this->id = GeneralOptions::DEFAULT_ID;
        $this->serialNumber = new SerialNumber();
        $this->lvl = GeneralOptions::DEFAULT_LEVEL;
        $this->opt = GeneralOptions::DEFAULT_OPTION;
        $this->isSkill = GeneralOptions::DEFAULT_SKILL;
        $this->isLuck = GeneralOptions::DEFAULT_LUCK;
        $this->durability = GeneralOptions::DEFAULT_DURABILITY;
    }

    public function getGroup(): int
    {
        return $this->group;
    }

    public function setGroup(int $group): GeneralOptions
    {
        $this->group = $group;

        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): GeneralOptions
    {
        $this->id = $id;

        return $this;
    }

    public function getSerialNumber(): SerialNumber
    {
        return $this->serialNumber;
    }

    public function setSerial(SerialNumber $serialNumber): GeneralOptions
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    public function getLvl(): int
    {
        return $this->lvl;
    }

    public function setLvl(int $lvl): GeneralOptions
    {
        $this->lvl = $lvl;

        return $this;
    }

    public function getOpt(): int
    {
        return $this->opt;
    }

    public function setOpt(int $opt): GeneralOptions
    {
        $this->opt = $opt;

        return $this;
    }

    public function getIsSkill(): bool
    {
        return $this->isSkill;
    }

    public function setIsSkill(bool $isSkill): GeneralOptions
    {
        $this->isSkill = $isSkill;

        return $this;
    }

    public function getIsLuck(): bool
    {
        return $this->isLuck;
    }

    public function setIsLuck(bool $isLuck): GeneralOptions
    {
        $this->isLuck = $isLuck;

        return $this;
    }

    public function getDurability(): int
    {
        return $this->durability;
    }

    public function setDurability(int $durability): GeneralOptions
    {
        $this->durability = $durability;

        return $this;
    }

    public function isParsed(): bool
    {
        return [
            $this->getGroup(),
            $this->getId(),
            $this->getSerialNumber()->getFirst(),
            $this->getSerialNumber()->getSecond(),
            $this->getLvl(),
            $this->getOpt(),
            $this->getIsSkill(),
            $this->getIsLuck(),
            $this->getDurability(),
        ] !== [
            GeneralOptions::DEFAULT_GROUP,
            GeneralOptions::DEFAULT_ID,
            SerialNumber::DEFAULT_SERIAL,
            SerialNumber::DEFAULT_SERIAL,
            GeneralOptions::DEFAULT_LEVEL,
            GeneralOptions::DEFAULT_OPTION,
            GeneralOptions::DEFAULT_SKILL,
            GeneralOptions::DEFAULT_LUCK,
            GeneralOptions::DEFAULT_DURABILITY,
        ];
    }


}