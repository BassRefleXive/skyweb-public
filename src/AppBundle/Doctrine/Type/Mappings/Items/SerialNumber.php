<?php

namespace AppBundle\Doctrine\Type\Mappings\Items;


class SerialNumber
{
    const DEFAULT_SERIAL = 0;

    private $first;
    private $second;

    public function __construct()
    {
        $this->first = SerialNumber::DEFAULT_SERIAL;
        $this->second = SerialNumber::DEFAULT_SERIAL;
    }

    public function getFirst()
    {
        return $this->first;
    }

    public function setFirst(int $first): SerialNumber
    {
        $this->first = $first;

        return $this;
    }

    public function getSecond()
    {
        return $this->second;
    }

    public function setSecond(int $second): SerialNumber
    {
        $this->second = $second;

        return $this;
    }

    public function compound(): int
    {
        return $this->getFirst() + $this->getSecond();
    }
}