<?php

namespace AppBundle\Doctrine\Type\Mappings\Items;


class ExcellentOptions extends AbstractOptions
{
    const EXC_NO_EXC = 0;

    protected $option;

    public function __construct()
    {
        $this->option = ExcellentOptions::EXC_NO_EXC;
    }

    public function addOption(int $option): ExcellentOptions
    {
        $this->option = ($this->option | $option);

        return $this;
    }

    public function setOptions(int $options): ExcellentOptions
    {
        $this->option = $options;

        return $this;
    }

    public function removeOption(int $option): ExcellentOptions
    {
        $this->option = ($this->option & ~$option);

        return $this;
    }

    public function checkOption(int $bit): bool
    {
        return (bool)((int)$this->option & (int)$bit);
    }

    public function getOptions(): int
    {
        return $this->option;
    }

    public function isExcellent(): bool
    {
        return $this->option !== ExcellentOptions::EXC_NO_EXC;
    }

}