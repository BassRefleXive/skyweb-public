<?php

namespace AppBundle\Doctrine\Type\Mappings\Items\Builder;

use AppBundle\Doctrine\Type\Mappings\Items\SocketOptions;
use AppBundle\Entity\Application\Items\SocketOption;

class SocketOptionsBuilder
{
    private $slots;

    public function __construct()
    {
        $this->slots = [];
    }

    public function withSlot(int $slot, SocketOption $option): self
    {
        if ($slot < 0 || $slot > 4) {
            throw new \InvalidArgumentException('Slot must be between 0 and 4.');
        }
        $this->slots[$slot] = $option;

        return $this;
    }

    public function slots(): array
    {
        return $this->slots;
    }

    public function build(): SocketOptions
    {
        return new SocketOptions($this);
    }
}