<?php

namespace AppBundle\Doctrine\Type\Mappings\Items;


class ComputedOptions
{
    protected $minDmg;
    protected $maxDmg;
    protected $wizardryDmg;
    protected $durability;

    protected $reqStr;
    protected $reqAgi;
    protected $reqEne;

    protected $defense;

    protected $countable;

    public function __construct()
    {
        $this->countable = false;
    }

    public function getMinDmg()
    {
        return $this->minDmg;
    }

    public function setMinDmg(int $minDmg): ComputedOptions
    {
        $this->minDmg = $minDmg;

        return $this;
    }

    public function getMaxDmg()
    {
        return $this->maxDmg;
    }

    public function setMaxDmg(int $maxDmg): ComputedOptions
    {
        $this->maxDmg = $maxDmg;

        return $this;
    }

    public function getWizardryDmg()
    {
        return $this->wizardryDmg;
    }

    public function setWizardryDmg($wizardryDmg): ComputedOptions
    {
        $this->wizardryDmg = $wizardryDmg;

        return $this;
    }

    public function getDurability()
    {
        return $this->durability;
    }

    public function setDurability(int $durability): ComputedOptions
    {
        $this->durability = $durability;

        return $this;
    }

    public function getReqStr()
    {
        return $this->reqStr;
    }

    public function setReqStr(int $reqStr): ComputedOptions
    {
        $this->reqStr = $reqStr;

        return $this;
    }

    public function getReqAgi()
    {
        return $this->reqAgi;
    }

    public function setReqAgi(int $reqAgi): ComputedOptions
    {
        $this->reqAgi = $reqAgi;

        return $this;
    }

    public function getReqEne()
    {
        return $this->reqEne;
    }

    public function setReqEne(int $reqEne): ComputedOptions
    {
        $this->reqEne = $reqEne;

        return $this;
    }

    public function getDefense()
    {
        return $this->defense;
    }

    public function setDefense(int $defense): ComputedOptions
    {
        $this->defense = $defense;

        return $this;
    }

    public function setIsCountable(bool $countable): ComputedOptions
    {
        $this->countable = $countable;

        return $this;
    }

    public function getIsCountable(): bool
    {
        return $this->countable;
    }

}