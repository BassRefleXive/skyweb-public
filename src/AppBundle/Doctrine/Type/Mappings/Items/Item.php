<?php

namespace AppBundle\Doctrine\Type\Mappings\Items;


use AppBundle\Entity\Application\Items\AncientSet;
use AppBundle\Entity\Application\Items\ItemInfo;
use AppBundle\Entity\Application\Items\PvPOption;

class Item
{
    const ITEM_SIZE_16 = 16;
    const ITEM_SIZE_32 = 32;
    const ITEM_SIZE_64 = 64;

    private $hex;

    private $itemInfo;

    private $generalOptions;
    private $excellentOptions;
    private $ancientSet;
    private $harmonyOptions;
    private $pvpOption;
    private $socketOptions;

    private $computedOptions;

    private $isParsed;

    protected function __construct(int $itemSize = null)
    {
        $itemSize && $this->hex = str_pad('', $itemSize, 'F');
        $this->generalOptions = new GeneralOptions();
        $this->excellentOptions = new ExcellentOptions();
        $this->harmonyOptions = new HarmonyOptions();
        $this->computedOptions = new ComputedOptions();
        $this->isParsed = false;
    }

    public static function createNew(int $itemSize)
    {
        return new static($itemSize);
    }

    public static function createExisting()
    {
        return new static();
    }

    /** @return ItemInfo */
    public function getItemInfo()
    {
        return $this->itemInfo;
    }

    public function setItemInfo(ItemInfo $itemInfo): self
    {
        $this->itemInfo = $itemInfo;

        return $this;
    }


    public function getHex(): string
    {
        return $this->hex;
    }

    public function setHex(string $hex): self
    {
        $this->hex = $hex;

        return $this;
    }

    public function getSize(): int
    {
        return mb_strlen($this->hex);
    }

    public function getGeneralOptions(): GeneralOptions
    {
        return $this->generalOptions;
    }

    public function setGeneralOptions(GeneralOptions $generalOptions): self
    {
        $this->generalOptions = $generalOptions;

        return $this;
    }

    public function getExcellentOptions(): ExcellentOptions
    {
        return $this->excellentOptions;
    }

    public function setExcellentOptions(ExcellentOptions $excellentOptions): self
    {
        $this->excellentOptions = $excellentOptions;

        return $this;
    }

    public function getAncientSet()
    {
        return $this->ancientSet;
    }

    public function setAncientSet(AncientSet $ancientSet): self
    {
        $this->ancientSet = $ancientSet;

        return $this;
    }

    public function getHarmonyOptions(): HarmonyOptions
    {
        return $this->harmonyOptions;
    }

    public function setHarmonyOptions(HarmonyOptions $harmonyOptions): self
    {
        $this->harmonyOptions = $harmonyOptions;

        return $this;
    }

    public function getPVPOption()
    {
        return $this->pvpOption;
    }

    public function setPVPOption(PvPOption $pvpOption)
    {
        $this->pvpOption = $pvpOption;

        return $this;
    }

    public function getComputedOptions(): ComputedOptions
    {
        return $this->computedOptions;
    }

    public function setComputedOptions(ComputedOptions $computedOptions): self
    {
        $this->computedOptions = $computedOptions;

        return $this;
    }

    public function setSocketOptions(SocketOptions $options): self
    {
        $this->socketOptions = $options;

        return $this;
    }

    public function socketOptions(): SocketOptions
    {
        if (null === $this->socketOptions) {
            throw new \LogicException('Item must be fully parsed before viewing socket options.');
        }

        return $this->socketOptions;
    }

    public function isParsed(): bool
    {
        return $this->isParsed;
    }

    public function setParsed(): self
    {
        $this->isParsed = true;

        return $this;
    }

    public function isEmpty(): bool
    {
        return (
            $this->getHex() === str_pad('', $this->getSize(), 'F') ||
            substr_count($this->getHex(), 'F') + substr_count($this->getHex(), '0') === $this->getSize()
        );
    }

}