<?php

namespace AppBundle\Doctrine\Type\Mappings\MagicList;


use AppBundle\Doctrine\Type\Mappings\MagicList\Builder\MagicListBuilder;
use Collection\Sequence;

class MagicList extends Sequence
{
    public function __construct(MagicListBuilder $builder)
    {
        parent::__construct($builder->all());
    }

    public function normal(): self
    {
        return $this->filter(function (Skill $skill): bool {
            return $skill->info()->useType()->isNormal();
        });
    }

    public function castleSiege(): self
    {
        return $this->filter(function (Skill $skill): bool {
            return $skill->info()->useType()->isCastleSiege();
        });
    }

    public function master(): self
    {
        return $this->filter(function (Skill $skill): bool {
            return $skill->info()->useType()->isMaster();
        });
    }

    public function deletable(): self
    {
        return $this->filter(function (Skill $skill): bool {
            return $skill->info()->isDeletable();
        });
    }

    public function findById(int $id): ?Skill
    {
        return $this
            ->filter(function (Skill $skill) use ($id): bool {
                return $skill->id() === $id;
            })
            ->head();
    }

    protected function createNew($elements)
    {
        return new static(
            (new MagicListBuilder())->withSkills($elements)
        );
    }

    public function __toString(): string
    {
        $result = implode('', $this->map(function (Skill $skill) {
            return $skill->hex();
        })->all());

        return strtolower($result);
    }
}