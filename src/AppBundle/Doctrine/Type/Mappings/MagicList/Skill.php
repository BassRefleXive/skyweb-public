<?php

declare(strict_types=1);

namespace AppBundle\Doctrine\Type\Mappings\MagicList;

use AppBundle\Entity\Application\MagicList\Skill as SkillEntity;

class Skill
{
    private $hex;

    private $id;

    /**
     * Master Skills Only
     */
    private $level;

    private $info;

    public function __construct(string $hex, int $id, int $level = 0)
    {
        $this->hex = $hex;

        $this->id = $id;
        $this->level = $level;
    }

    public function hex(): string
    {
        return $this->hex;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function level(): int
    {
        return $this->level;
    }

    public function specifyLevel(int $level): self
    {
        if (!$this->info()->useType()->isMaster()) {
            throw new \LogicException('Only master Skills can have level.');
        }

        if ($level < 0 || $level > 20) {
            throw new \LogicException('Skill level must be between 0 and 20.');
        }
    }

    public function info(): SkillEntity
    {
        if (null === $this->info) {
            throw new \LogicException('MagicList containing this skill must be processed by MagicListProcessor before obtaining Skill info.');
        }

        return $this->info;
    }

    public function setInfo(SkillEntity $info): self
    {
        $this->info = $info;

        return $this;
    }
}