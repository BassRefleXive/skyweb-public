<?php

declare(strict_types = 1);

namespace AppBundle\Doctrine\Type\Mappings\MagicList\Builder;


use AppBundle\Doctrine\Type\Mappings\MagicList\MagicList;
use AppBundle\Doctrine\Type\Mappings\MagicList\Skill;
use AppBundle\Exception\ApplicationException;

class MagicListBuilder
{
    private const MAGIC_SIZE = 6;
    private const MASTER_SKILLS_START_ID = 300;

    private $hex;
    private $skills;

    public function __construct()
    {
        $this->skills = [];
    }

    public function withHex(string $hex): self
    {
        $this->hex = $hex;

        return $this;
    }

    public function withSkills(array $skills): self
    {
        $this->skills = $skills;

        return $this;
    }

    public final function build(): MagicList
    {
        return new MagicList($this);
    }

    public final function all(): array
    {
        return $this->process();
    }

    private function process(): array
    {
        if (!count($this->skills) && null === $this->hex) {
            return [];
        }

        if (count($this->skills) && null !== $this->hex) {
            throw new \LogicException('MagicList can be build from hex or Skills. Only one of them must be provided.');
        }

        if (!count($this->skills)) {
            $this->parseHex();
        }

        return $this->skills;
    }

    private function parseHex(): void
    {
        $magicList = strtoupper(bin2hex($this->hex));

        if (substr($magicList, 0, 2) == '0x') {
            $magicList = substr($magicList, 2);
        }

        $magicListSize = mb_strlen($magicList);

        if (0 !== $magicListSize % self::MAGIC_SIZE) {
            throw new ApplicationException(sprintf('Invalid magic list size. Must divide by %d without remainder. Size: %d.', self::MAGIC_SIZE, $magicListSize));
        }

        for ($i = 0; $i < $magicListSize / self::MAGIC_SIZE; $i++) {
            $hex = substr($magicList, (self::MAGIC_SIZE * $i), self::MAGIC_SIZE);

            if (null !== $skill = SkillBuilder::fromHex($hex)) {
                $this->skills[$skill->id()] = $skill;
            }
        }
    }
}