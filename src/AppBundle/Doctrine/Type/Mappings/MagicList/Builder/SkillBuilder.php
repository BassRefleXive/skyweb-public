<?php


namespace AppBundle\Doctrine\Type\Mappings\MagicList\Builder;


use AppBundle\Doctrine\Type\Mappings\MagicList\Skill;

class SkillBuilder
{
    public static final function fromHex(string $hex): ?Skill
    {
        $firstSegment = self::extractNumber($hex, 0, 2);

        if (255 !== $firstSegment) {
            return new Skill($hex, $firstSegment);
        }

        $masterSkillAddId = self::extractNumber($hex, 4, 2);

        if (0 === $masterSkillAddId) {
            return null;
        }

        $sub = self::extractNumber($hex, 2, 2) % 8;

        $masterSkillLevel = (self::extractNumber($hex, 2, 2) - $sub) / 8;
        $id = (self::extractNumber($hex, 2, 4) - (2 ** 11) * $masterSkillLevel) - $sub;

        return new Skill($hex, $id, $masterSkillLevel);
    }

    public static final function fromValues(int $id, ?int $level): Skill
    {
        $emptySkillHex = str_pad('', 6, 'F');

        // If level is not specified at all - considering that this is regular, not master skill.
        if (null === $level) {
            $hex = self::setHexValue($emptySkillHex, 0, 2, $id);

            return new Skill($hex, $id);
        }

        // Else - try to create skill with "sub" from 0 to 10. If failure - throw exception.
        // Each generated HEX code is parsed and compared is it has been parsed correctly.
        // If correctly - returned, if not - try to create another one.
        // If "sub" greated than limit - stop loop and throw exception.

        for ($i = 0; $i < 10; $i++) {
            $hex = self::setHexValue($emptySkillHex, 2, 4, ($id + (2 ** 11) * $level) + $i);

            $parsed = self::fromHex($hex);

            if (
                null === $parsed ||
                (
                    $parsed instanceof Skill &&
                    (
                        $parsed->id() !== $id || $parsed->level() !== $level
                    )
                )
            ) {
                continue;
            }

            return $parsed;
        }

        throw new \LogicException('Failed to create Skill.');
    }

    private static function extractNumber(string $hex, int $start, int $length): int
    {
        return (int)hexdec(substr($hex, $start, $length));
    }

    private static function setHexValue(string $hex, int $start, int $length, int $value): string
    {
        return substr_replace($hex, sprintf('%0' . $length . 'X', $value, str_pad('', $length, 0)), $start, $length);
    }
}