<?php

namespace AppBundle\Doctrine\Type;


class MarketPriceTypeEnum extends Enum
{
    const MARKET_PRICE_TYPE_CREDITS = 'C';
    const MARKET_PRICE_TYPE_ZEN = 'Z';

    protected $name = 'enum_market_price_type';
    protected $values = [self::MARKET_PRICE_TYPE_CREDITS, self::MARKET_PRICE_TYPE_ZEN];


    static function getKeyByName($name)
    {
        $name = strtolower($name);

        $types = static::getTypes();
        $keys = array_flip($types);
        $keys = array_change_key_case($keys, CASE_LOWER);

        if (!array_key_exists($name, $keys)) {
            throw new \InvalidArgumentException('Given market price type type is not supported.');
        }

        return $keys[$name];
    }

    static function getTypes()
    {
        return [
            self::MARKET_PRICE_TYPE_CREDITS => 'WCoin',
            self::MARKET_PRICE_TYPE_ZEN     => 'kk Zen',
        ];
    }

    static function getNameByKey($key)
    {
        $types = static::getTypes();

        if (!array_key_exists($key, $types)) {
            throw new \InvalidArgumentException('Given market price type is not supported');
        }

        return $types[$key];
    }
}