<?php

namespace AppBundle\Doctrine\Type;


use Doctrine\DBAL\Platforms\AbstractPlatform;

class HarmonyOptionEnum extends Enum
{
    const HARMONY_WEAPON_INC_MIN_DMG = 1;
    const HARMONY_WEAPON_INC_MAX_DMG = 2;
    const HARMONY_WEAPON_REDUCE_REQ_STR = 3;
    const HARMONY_WEAPON_REDUCE_REQ_AGI = 4;
    const HARMONY_WEAPON_INC_MIN_MAX_DMG = 5;
    const HARMONY_WEAPON_INC_CRITICAL_DMG = 6;
    const HARMONY_WEAPON_SKILL_DMG = 7;
    const HARMONY_WEAPON_INC_ATTACK_SUCCESS_RATE_PVP = 8;
    const HARMONY_WEAPON_INC_SD_REDUCTION = 9;
    const HARMONY_WEAPON_INC_SD_IGNORE_RATE = 10;

    const HARMONY_STAFF_INC_WIZARDRY = 1;
    const HARMONY_STAFF_REDUCE_REQ_STR = 2;
    const HARMONY_STAFF_REDUCE_REQ_AGI = 3;
    const HARMONY_STAFF_SKILL_DMG = 4;
    const HARMONY_STAFF_INC_CRITICAL_DMG = 5;
    const HARMONY_STAFF_INC_SD_REDUCTION = 6;
    const HARMONY_STAFF_INC_ATTACK_SUCCESS_RATE_PVP = 7;
    const HARMONY_STAFF_INC_SD_IGNORE_RATE = 8;

    const HARMONY_ARMOR_INC_DEFENSE = 1;
    const HARMONY_ARMOR_INC_MAX_AG = 2;
    const HARMONY_ARMOR_INC_MAX_HEALTH = 3;
    const HARMONY_ARMOR_INC_HP_REGEN_RATE = 4;
    const HARMONY_ARMOR_INC_MANA_REGEN_RATE = 5;
    const HARMONY_ARMOR_INC_DEF_SUCCESS_RATE_PVP = 6;
    const HARMONY_ARMOR_INC_DMG_REDUCTION = 7;
    const HARMONY_ARMOR_INC_SD_RATIO_RATE = 8;


    protected $name = 'enum_harmony_option';

    protected $values = [
        HarmonyOptionEnum::HARMONY_WEAPON_INC_MIN_DMG,
        HarmonyOptionEnum::HARMONY_WEAPON_INC_MAX_DMG,
        HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_STR,
        HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_AGI,
        HarmonyOptionEnum::HARMONY_WEAPON_INC_MIN_MAX_DMG,
        HarmonyOptionEnum::HARMONY_WEAPON_INC_CRITICAL_DMG,
        HarmonyOptionEnum::HARMONY_WEAPON_SKILL_DMG,
        HarmonyOptionEnum::HARMONY_WEAPON_INC_ATTACK_SUCCESS_RATE_PVP,
        HarmonyOptionEnum::HARMONY_WEAPON_INC_SD_REDUCTION,
        HarmonyOptionEnum::HARMONY_WEAPON_INC_SD_IGNORE_RATE,

        HarmonyOptionEnum::HARMONY_STAFF_INC_WIZARDRY,
        HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_STR,
        HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_AGI,
        HarmonyOptionEnum::HARMONY_STAFF_SKILL_DMG,
        HarmonyOptionEnum::HARMONY_STAFF_INC_CRITICAL_DMG,
        HarmonyOptionEnum::HARMONY_STAFF_INC_SD_REDUCTION,
        HarmonyOptionEnum::HARMONY_STAFF_INC_ATTACK_SUCCESS_RATE_PVP,
        HarmonyOptionEnum::HARMONY_STAFF_INC_SD_IGNORE_RATE,

        HarmonyOptionEnum::HARMONY_ARMOR_INC_DEFENSE,
        HarmonyOptionEnum::HARMONY_ARMOR_INC_MAX_AG,
        HarmonyOptionEnum::HARMONY_ARMOR_INC_MAX_HEALTH,
        HarmonyOptionEnum::HARMONY_ARMOR_INC_HP_REGEN_RATE,
        HarmonyOptionEnum::HARMONY_ARMOR_INC_MANA_REGEN_RATE,
        HarmonyOptionEnum::HARMONY_ARMOR_INC_DEF_SUCCESS_RATE_PVP,
        HarmonyOptionEnum::HARMONY_ARMOR_INC_DMG_REDUCTION,
        HarmonyOptionEnum::HARMONY_ARMOR_INC_SD_RATIO_RATE,
    ];


    static function getKeyByName($name)
    {
        $name = strtolower($name);

        $keys = array_merge(
            array_flip(static::getWeaponTypes()),
            array_flip(static::getStaffTypes()),
            array_flip(static::getArmorTypes())
        );

        $keys = array_change_key_case($keys, CASE_LOWER);

        if (!array_key_exists($name, $keys)) {
            throw new \InvalidArgumentException('Given harmony option is not supported.');
        }

        return $keys[$name];
    }

    static public function getWeaponTypes()
    {
        return [
            HarmonyOptionEnum::HARMONY_WEAPON_INC_MIN_DMG                 => 'Increases Minimum Damage',
            HarmonyOptionEnum::HARMONY_WEAPON_INC_MAX_DMG                 => 'Increases Maximum Damage',
            HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_STR              => 'Reduces Required Strength',
            HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_AGI              => 'Reduces Required Agility',
            HarmonyOptionEnum::HARMONY_WEAPON_INC_MIN_MAX_DMG             => 'Increases Min / Max Damage',
            HarmonyOptionEnum::HARMONY_WEAPON_INC_CRITICAL_DMG            => 'Increases Critical Damage',
            HarmonyOptionEnum::HARMONY_WEAPON_SKILL_DMG                   => 'Increases Skill Damage',
            HarmonyOptionEnum::HARMONY_WEAPON_INC_ATTACK_SUCCESS_RATE_PVP => 'Increases Attack Success Rate (PvP)',
            HarmonyOptionEnum::HARMONY_WEAPON_INC_SD_REDUCTION            => 'Increases SD Reduction',
            HarmonyOptionEnum::HARMONY_WEAPON_INC_SD_IGNORE_RATE          => 'Increases SD Ignore Rate',
        ];
    }

    static public function getStaffTypes()
    {
        return [
            HarmonyOptionEnum::HARMONY_STAFF_INC_WIZARDRY                => 'Increases Wizardry',
            HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_STR              => 'Reduces Required Strength',
            HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_AGI              => 'Reduces Required Agility',
            HarmonyOptionEnum::HARMONY_STAFF_SKILL_DMG                   => 'Increases Skill Damage',
            HarmonyOptionEnum::HARMONY_STAFF_INC_CRITICAL_DMG            => 'Increases Critical Damage',
            HarmonyOptionEnum::HARMONY_STAFF_INC_SD_REDUCTION            => 'Increases SD Reduction',
            HarmonyOptionEnum::HARMONY_STAFF_INC_ATTACK_SUCCESS_RATE_PVP => 'Increases Attack Success Rate (PvP)',
            HarmonyOptionEnum::HARMONY_STAFF_INC_SD_IGNORE_RATE          => 'Increases SD Ignore Rate',
        ];
    }

    static public function getArmorTypes()
    {
        return [
            HarmonyOptionEnum::HARMONY_ARMOR_INC_DEFENSE              => 'Increases Defense',
            HarmonyOptionEnum::HARMONY_ARMOR_INC_MAX_AG               => 'Increases Max AG',
            HarmonyOptionEnum::HARMONY_ARMOR_INC_MAX_HEALTH           => 'Increases Max Health',
            HarmonyOptionEnum::HARMONY_ARMOR_INC_HP_REGEN_RATE        => 'Increases Health Regeneration Rate',
            HarmonyOptionEnum::HARMONY_ARMOR_INC_MANA_REGEN_RATE      => 'Increases Mana Regeneration Rate',
            HarmonyOptionEnum::HARMONY_ARMOR_INC_DEF_SUCCESS_RATE_PVP => 'Increases Defense Success Rate (PvP)',
            HarmonyOptionEnum::HARMONY_ARMOR_INC_DMG_REDUCTION        => 'Increases Damage Reduction',
            HarmonyOptionEnum::HARMONY_ARMOR_INC_SD_RATIO_RATE        => 'Increases SD Ratio Rate',
        ];
    }

    static public function getWeaponTypeNameByKey($key)
    {
        return HarmonyOptionEnum::getNameByKey(HarmonyOptionEnum::getWeaponTypes(), $key);
    }

    static public function getStaffTypeNameByKey($key)
    {
        return HarmonyOptionEnum::getNameByKey(HarmonyOptionEnum::getStaffTypes(), $key);
    }

    static public function getArmorTypeNameByKey($key)
    {
        return HarmonyOptionEnum::getNameByKey(HarmonyOptionEnum::getArmorTypes(), $key);
    }

    static protected function getNameByKey(array $types, $key)
    {
        if (!array_key_exists($key, $types)) {
            throw new \InvalidArgumentException('Given harmony option is not supported');
        }

        return $types[$key];
    }

    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return "INTEGER";
    }
}