<?php

namespace AppBundle\Doctrine\Type;


class ItemToBuyTypeEnum extends Enum
{
    const TYPE_ITEM = 'I';
    const TYPE_STATS = 'S';
    const TYPE_WCOIN = 'WC';

    protected $name = 'enum_item_to_buy_type';
    protected $values = [
        ItemToBuyTypeEnum::TYPE_ITEM,
        ItemToBuyTypeEnum::TYPE_STATS,
        ItemToBuyTypeEnum::TYPE_WCOIN,
    ];


    static function getKeyByName($name)
    {
        $name = strtolower($name);

        $types = static::getTypes();
        $keys = array_flip($types);
        $keys = array_change_key_case($keys, CASE_LOWER);

        if (!array_key_exists($name, $keys)) {
            throw new \InvalidArgumentException('Given cart item type is not supported.');
        }

        return $keys[$name];
    }

    static function getTypes()
    {
        return [
            ItemToBuyTypeEnum::TYPE_ITEM  => 'Item',
            ItemToBuyTypeEnum::TYPE_STATS => 'Stats',
            ItemToBuyTypeEnum::TYPE_WCOIN => 'WCoin',
        ];
    }

    static function getNameByKey($key)
    {
        $types = static::getTypes();

        if (!array_key_exists($key, $types)) {
            throw new \InvalidArgumentException('Given cart item type is not supported');
        }

        return $types[$key];
    }
}