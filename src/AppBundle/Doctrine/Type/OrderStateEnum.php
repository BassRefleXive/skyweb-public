<?php

declare(strict_types = 1);

namespace AppBundle\Doctrine\Type;


class OrderStateEnum extends Enum
{
    const ORDER_STATE_INIT = 'init';
    const ORDER_STATE_SUCCESS = 'success';
    const ORDER_STATE_FAIL = 'fail';
    const ORDER_STATE_STARTED = 'started';

    protected $name = 'enum_order_state';
    protected $values = [self::ORDER_STATE_INIT, self::ORDER_STATE_SUCCESS, self::ORDER_STATE_FAIL, self::ORDER_STATE_STARTED];


    static function getKeyByName($name)
    {
        $name = strtolower($name);

        $types = static::getTypes();
        $keys = array_flip($types);
        $keys = array_change_key_case($keys, CASE_LOWER);

        if (!array_key_exists($name, $keys)) {
            throw new \InvalidArgumentException('Given order state is not supported.');
        }

        return $keys[$name];
    }

    static function getTypes()
    {
        return [
            self::ORDER_STATE_INIT => 'Initial',
            self::ORDER_STATE_SUCCESS => 'Succeeded',
            self::ORDER_STATE_FAIL => 'Failed',
            self::ORDER_STATE_STARTED => 'Started',
        ];
    }

    static function getNameByKey($key)
    {
        $types = static::getTypes();

        if (!array_key_exists($key, $types)) {
            throw new \InvalidArgumentException('Given order state is not supported');
        }

        return $types[$key];
    }
}