<?php

namespace AppBundle\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;

class CharacterClassEnum extends Enum
{
    const CHARACTER_CLASS_DARK_WIZARD = 0;
    const CHARACTER_CLASS_SOUL_MASTER = 1;
    const CHARACTER_CLASS_GRAND_MASTER = 2;

    const CHARACTER_CLASS_DARK_KNIGHT = 16;
    const CHARACTER_CLASS_BLADE_KNIGHT = 17;
    const CHARACTER_CLASS_BLADE_MASTER = 18;

    const CHARACTER_CLASS_ELF = 32;
    const CHARACTER_CLASS_MUSE_ELF = 33;
    const CHARACTER_CLASS_HIGH_ELF = 34;

    const CHARACTER_CLASS_MAGIC_GLADIATOR = 48;
    const CHARACTER_CLASS_DUEL_MASTER = 50;

    const CHARACTER_CLASS_DARK_LORD = 64;
    const CHARACTER_CLASS_LORD_EMPEROR = 66;

    const CHARACTER_CLASS_SUMMONER = 80;
    const CHARACTER_CLASS_BLOODY_SUMMONER = 81;
    const CHARACTER_CLASS_DIMENSION_MASTER = 82;

    const CHARACTER_CLASS_RAGE_FIGHTER = 96;
    const CHARACTER_CLASS_FIST_MASTER = 98;

    protected $name = 'enum_character_class';

    protected $values = [
        CharacterClassEnum::CHARACTER_CLASS_DARK_WIZARD,
        CharacterClassEnum::CHARACTER_CLASS_SOUL_MASTER,
        CharacterClassEnum::CHARACTER_CLASS_GRAND_MASTER,
        CharacterClassEnum::CHARACTER_CLASS_DARK_KNIGHT,
        CharacterClassEnum::CHARACTER_CLASS_BLADE_KNIGHT,
        CharacterClassEnum::CHARACTER_CLASS_BLADE_MASTER,
        CharacterClassEnum::CHARACTER_CLASS_ELF,
        CharacterClassEnum::CHARACTER_CLASS_MUSE_ELF,
        CharacterClassEnum::CHARACTER_CLASS_HIGH_ELF,
        CharacterClassEnum::CHARACTER_CLASS_MAGIC_GLADIATOR,
        CharacterClassEnum::CHARACTER_CLASS_DUEL_MASTER,
        CharacterClassEnum::CHARACTER_CLASS_DARK_LORD,
        CharacterClassEnum::CHARACTER_CLASS_LORD_EMPEROR,
        CharacterClassEnum::CHARACTER_CLASS_SUMMONER,
        CharacterClassEnum::CHARACTER_CLASS_BLOODY_SUMMONER,
        CharacterClassEnum::CHARACTER_CLASS_DIMENSION_MASTER,
        CharacterClassEnum::CHARACTER_CLASS_RAGE_FIGHTER,
        CharacterClassEnum::CHARACTER_CLASS_FIST_MASTER,
    ];


    public static function getKeyByName(string $name)
    {
        $name = strtolower($name);

        $types = static::getTypes();
        $keys = array_flip($types);
        $keys = array_change_key_case($keys, CASE_LOWER);

        if (!array_key_exists($name, $keys)) {
            throw new \InvalidArgumentException(
                sprintf(
                    'Given character class (%s) is not supported',
                    $name
                )
            );
        }

        return $keys[$name];
    }

    public static function getKeyByAbbreviation(string $name)
    {
        $name = strtolower($name);

        $types = static::getAbbreviations();
        $keys = array_flip($types);
        $keys = array_change_key_case($keys, CASE_LOWER);

        if (!array_key_exists($name, $keys)) {
            throw new \InvalidArgumentException(
                sprintf(
                    'Given character class (%s) is not supported',
                    $name
                )
            );
        }

        return $keys[$name];
    }

    public static function getTypes()
    {
        return [
            CharacterClassEnum::CHARACTER_CLASS_DARK_KNIGHT      => 'Dark Knight',
            CharacterClassEnum::CHARACTER_CLASS_BLADE_KNIGHT     => 'Blade Knight',
            CharacterClassEnum::CHARACTER_CLASS_BLADE_MASTER     => 'Blade Master',
            CharacterClassEnum::CHARACTER_CLASS_DARK_WIZARD      => 'Dark Wizard',
            CharacterClassEnum::CHARACTER_CLASS_SOUL_MASTER      => 'Soul Master',
            CharacterClassEnum::CHARACTER_CLASS_GRAND_MASTER     => 'Grand Master',
            CharacterClassEnum::CHARACTER_CLASS_ELF              => 'Elf',
            CharacterClassEnum::CHARACTER_CLASS_MUSE_ELF         => 'Muse Elf',
            CharacterClassEnum::CHARACTER_CLASS_HIGH_ELF         => 'High Elf',
            CharacterClassEnum::CHARACTER_CLASS_MAGIC_GLADIATOR  => 'Magic Gladiator',
            CharacterClassEnum::CHARACTER_CLASS_DUEL_MASTER      => 'Duel Master',
            CharacterClassEnum::CHARACTER_CLASS_DARK_LORD        => 'Dark Lord',
            CharacterClassEnum::CHARACTER_CLASS_LORD_EMPEROR     => 'Lord Emperror',
            CharacterClassEnum::CHARACTER_CLASS_SUMMONER         => 'Summoner',
            CharacterClassEnum::CHARACTER_CLASS_BLOODY_SUMMONER  => 'Bloody Summoner',
            CharacterClassEnum::CHARACTER_CLASS_DIMENSION_MASTER => 'Dimension Master',
            CharacterClassEnum::CHARACTER_CLASS_RAGE_FIGHTER     => 'Rage Fighter',
            CharacterClassEnum::CHARACTER_CLASS_FIST_MASTER      => 'Fist Master',
        ];
    }

    public static function getAbbreviations()
    {
        return [
            CharacterClassEnum::CHARACTER_CLASS_DARK_KNIGHT      => 'DK',
            CharacterClassEnum::CHARACTER_CLASS_BLADE_KNIGHT     => 'BK',
            CharacterClassEnum::CHARACTER_CLASS_BLADE_MASTER     => 'BM',
            CharacterClassEnum::CHARACTER_CLASS_DARK_WIZARD      => 'DW',
            CharacterClassEnum::CHARACTER_CLASS_SOUL_MASTER      => 'SM',
            CharacterClassEnum::CHARACTER_CLASS_GRAND_MASTER     => 'GM',
            CharacterClassEnum::CHARACTER_CLASS_ELF              => 'FE',
            CharacterClassEnum::CHARACTER_CLASS_MUSE_ELF         => 'ME',
            CharacterClassEnum::CHARACTER_CLASS_HIGH_ELF         => 'HE',
            CharacterClassEnum::CHARACTER_CLASS_MAGIC_GLADIATOR  => 'MG',
            CharacterClassEnum::CHARACTER_CLASS_DUEL_MASTER      => 'DM',
            CharacterClassEnum::CHARACTER_CLASS_DARK_LORD        => 'DL',
            CharacterClassEnum::CHARACTER_CLASS_LORD_EMPEROR     => 'LE',
            CharacterClassEnum::CHARACTER_CLASS_SUMMONER         => 'SUM',
            CharacterClassEnum::CHARACTER_CLASS_BLOODY_SUMMONER  => 'BS',
            CharacterClassEnum::CHARACTER_CLASS_DIMENSION_MASTER => 'DimM',
            CharacterClassEnum::CHARACTER_CLASS_RAGE_FIGHTER     => 'RF',
            CharacterClassEnum::CHARACTER_CLASS_FIST_MASTER      => 'FM',
        ];
    }

    public static function getUniqueUnLeveledCharacterClassesCount()
    {
        return 6;
    }

    public static function getNameByKey($key)
    {
        $types = static::getTypes();

        if (!array_key_exists($key, $types)) {
            throw new \InvalidArgumentException(
                sprintf(
                    'Given character class (%s) is not supported',
                    $key
                )
            );
        }

        return $types[$key];
    }

    public static function getAbbreviationByKey($key)
    {
        $types = static::getAbbreviations();

        if (!array_key_exists($key, $types)) {
            throw new \InvalidArgumentException(
                sprintf(
                    'Given character class (%s) is not supported',
                    $key
                )
            );
        }

        return $types[$key];
    }

    public static function getBaseClassAbbreviation($key)
    {
        switch ($key) {
            case CharacterClassEnum::CHARACTER_CLASS_DARK_KNIGHT:
            case CharacterClassEnum::CHARACTER_CLASS_BLADE_KNIGHT:
            case CharacterClassEnum::CHARACTER_CLASS_BLADE_MASTER:
                return static::getAbbreviationByKey(CharacterClassEnum::CHARACTER_CLASS_DARK_KNIGHT);
            case CharacterClassEnum::CHARACTER_CLASS_DARK_WIZARD:
            case CharacterClassEnum::CHARACTER_CLASS_SOUL_MASTER:
            case CharacterClassEnum::CHARACTER_CLASS_GRAND_MASTER:
                return static::getAbbreviationByKey(CharacterClassEnum::CHARACTER_CLASS_DARK_WIZARD);
            case CharacterClassEnum::CHARACTER_CLASS_ELF:
            case CharacterClassEnum::CHARACTER_CLASS_MUSE_ELF:
            case CharacterClassEnum::CHARACTER_CLASS_HIGH_ELF:
                return static::getAbbreviationByKey(CharacterClassEnum::CHARACTER_CLASS_ELF);
            case CharacterClassEnum::CHARACTER_CLASS_MAGIC_GLADIATOR:
            case CharacterClassEnum::CHARACTER_CLASS_DUEL_MASTER:
                return static::getAbbreviationByKey(CharacterClassEnum::CHARACTER_CLASS_MAGIC_GLADIATOR);
            case CharacterClassEnum::CHARACTER_CLASS_DARK_LORD:
            case CharacterClassEnum::CHARACTER_CLASS_LORD_EMPEROR:
                return static::getAbbreviationByKey(CharacterClassEnum::CHARACTER_CLASS_DARK_LORD);
            case CharacterClassEnum::CHARACTER_CLASS_SUMMONER:
            case CharacterClassEnum::CHARACTER_CLASS_BLOODY_SUMMONER:
            case CharacterClassEnum::CHARACTER_CLASS_DIMENSION_MASTER:
                return static::getAbbreviationByKey(CharacterClassEnum::CHARACTER_CLASS_SUMMONER);
            case CharacterClassEnum::CHARACTER_CLASS_RAGE_FIGHTER:
            case CharacterClassEnum::CHARACTER_CLASS_FIST_MASTER:
                return static::getAbbreviationByKey(CharacterClassEnum::CHARACTER_CLASS_RAGE_FIGHTER);
        }

        throw new \InvalidArgumentException(
            sprintf(
                'Given character class (%s) is not supported',
                $key
            )
        );
    }

    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return "INTEGER";
    }
}