<?php

namespace AppBundle\Doctrine\Type;


class UserTypeEnum extends Enum
{
    const USER_TYPE_ADMIN = 'A';
    const USER_TYPE_MOD = 'M';
    const USER_TYPE_REGULAR = 'R';

    protected $name = 'enum_user_type';
    protected $values = [self::USER_TYPE_ADMIN, self::USER_TYPE_MOD, self::USER_TYPE_REGULAR];


    static function getKeyByName($name)
    {
        $name = strtolower($name);

        $types = static::getTypes();
        $keys = array_flip($types);
        $keys = array_change_key_case($keys, CASE_LOWER);

        if (!array_key_exists($name, $keys)) {
            throw new \InvalidArgumentException('Given user type name is not supported.');
        }

        return $keys[$name];
    }

    static function getTypes()
    {
        return [
            self::USER_TYPE_ADMIN   => 'Administrator',
            self::USER_TYPE_MOD     => 'Moderator',
            self::USER_TYPE_REGULAR => 'Regular',
        ];
    }

    static function getNameByKey($key)
    {
        $types = static::getTypes();

        if (!array_key_exists($key, $types)) {
            throw new \InvalidArgumentException('Given user type is not supported');
        }

        return $types[$key];
    }
}