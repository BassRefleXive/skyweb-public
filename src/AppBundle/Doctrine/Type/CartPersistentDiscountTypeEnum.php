<?php

namespace AppBundle\Doctrine\Type;


class CartPersistentDiscountTypeEnum extends Enum
{
    const PERSISTENT_DISCOUNT_TYPE_MONEY = 'M';
    const PERSISTENT_DISCOUNT_TYPE_CREDITS = 'C';

    protected $name = 'enum_cart_persistent_discount_type';
    protected $values = [self::PERSISTENT_DISCOUNT_TYPE_MONEY, self::PERSISTENT_DISCOUNT_TYPE_CREDITS];


    static function getKeyByName($name)
    {
        $name = strtolower($name);

        $types = static::getTypes();
        $keys = array_flip($types);
        $keys = array_change_key_case($keys, CASE_LOWER);

        if (!array_key_exists($name, $keys)) {
            throw new \InvalidArgumentException('Given webshop persistent discount type is not supported.');
        }

        return $keys[$name];
    }

    static function getTypes()
    {
        return [
            self::PERSISTENT_DISCOUNT_TYPE_MONEY   => 'Money',
            self::PERSISTENT_DISCOUNT_TYPE_CREDITS => 'WCoin',
        ];
    }

    static function getNameByKey($key)
    {
        $types = static::getTypes();

        if (!array_key_exists($key, $types)) {
            throw new \InvalidArgumentException('Given webshop persistent discount type is not supported');
        }

        return $types[$key];
    }
}