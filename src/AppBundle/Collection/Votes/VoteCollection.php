<?php


namespace AppBundle\Collection\Votes;

use AppBundle\Model\Application\Votes\Builder\VoteModelBuilder;
use Collection\Map;

class VoteCollection extends Map
{
    public function buildFromString(string $string, VoteModelBuilder $builder): self
    {
        $votes = explode("\n", $string);

        $result = new self();

        if (count($votes) > 0) {
            foreach ($votes as $voteString) {
                if (mb_strlen(trim($voteString)) > 0) {
                    $voteModel = $builder->fromString(trim($voteString));
                    $result->set($voteModel->id(), $voteModel);
                }
            }
        }

        return $result;
    }
}