<?php

declare(strict_types=1);

namespace AppBundle\Location\Model;

use AppBundle\Location\Enum\CountryStatus;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\AppBundle\Location\Repository\Doctrine\CountryRepository")
 * @ORM\Table(name="Country")
 */
class Country
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=3)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $codeIso3;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $codeIso2;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=512)
     */
    private $nameOfficial;

    /**
     * @ORM\Column(type="decimal", precision=2, scale=8)
     */
    private $latitude;

    /**
     * @ORM\Column(type="decimal", precision=2, scale=8)
     */
    private $longitude;

    /**
     * @ORM\Column(type="smallint")
     */
    private $zoom;

    /**
     * @ORM\Column(type="country_status_type")
     */
    private $state;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $locale;

    public function codeIso3(): string
    {
        return $this->codeIso3;
    }

    public function codeIso2(): string
    {
        return $this->codeIso2;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function nameOfficial(): string
    {
        return $this->nameOfficial;
    }

    public function latitude(): float
    {
        return $this->latitude;
    }

    public function longitude(): float
    {
        return $this->longitude;
    }

    public function getZoom(): int
    {
        return $this->zoom;
    }

    public function state(): CountryStatus
    {
        return $this->state;
    }

    public function locale(): string
    {
        return $this->locale;
    }
}