<?php

declare(strict_types=1);

namespace AppBundle\Location\Discoverer\Gateway;

use AppBundle\Location\Discoverer\Gateway\Response\Location;

interface GatewayInterface
{
    public function discover(string $ip): Location;
}