<?php

declare(strict_types=1);

namespace AppBundle\Location\Discoverer\Gateway\IpApiCom;

use AppBundle\Http\Assembler\RequestAssembler;
use AppBundle\Http\Criteria\RequestCriteria;
use AppBundle\Http\HttpClientInterface;
use AppBundle\Http\Response\JsonResponseParser;
use AppBundle\Location\Discoverer\Gateway\GatewayInterface;
use AppBundle\Location\Discoverer\Gateway\IpApiCom\Criteria\UriCriteria;
use AppBundle\Location\Discoverer\Gateway\IpApiCom\Factory\LocationFactory;
use AppBundle\Location\Discoverer\Gateway\Response\Location;
use Symfony\Component\HttpFoundation\Request;

class IpApiComGateway implements GatewayInterface
{
    private $client;
    private $requestAssembler;
    private $responseParser;
    private $locationFactory;

    public function __construct(HttpClientInterface $client, RequestAssembler $requestAssembler)
    {
        $this->client = $client;
        $this->requestAssembler = $requestAssembler;
        $this->responseParser = new JsonResponseParser();
        $this->locationFactory = new LocationFactory();
    }

    public function discover(string $ip): Location
    {
        $requestCriteria = new RequestCriteria(
            UriCriteria::discover($ip),
            Request::METHOD_GET
        );

        $response = $this->client->send($this->requestAssembler->fromCriteria($requestCriteria));

        return $this->locationFactory->fromArray($this->responseParser->responseArray($response));
    }
}