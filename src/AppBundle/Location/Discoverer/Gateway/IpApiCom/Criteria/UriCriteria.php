<?php

declare(strict_types=1);

namespace AppBundle\Location\Discoverer\Gateway\IpApiCom\Criteria;

use AppBundle\Http\Criteria\UriCriteria as BaseUriCriteria;
use AppBundle\Http\Criteria\UriCriteriaInterface;

final class UriCriteria extends BaseUriCriteria implements UriCriteriaInterface
{
    public static function discover(string $ip): self
    {
        return new self(
            'json/{ip}',
            [
                'ip' => $ip,
            ]
        );
    }
}