<?php

declare(strict_types=1);

namespace AppBundle\Location\Discoverer\Gateway\IpApiCom\Exception;

use AppBundle\Http\Exception\ResponseException as BaseResponseException;
use AppBundle\Location\Discoverer\Gateway\Exception\GatewayExceptionInterface;

class ResponseException extends BaseResponseException implements GatewayExceptionInterface
{
    public static function missingCountryCode(): self
    {
        return new self('Missing countryCode in response.');
    }

    public static function error(string $error): self
    {
        return new self(sprintf('Error "%s" received from gateway.', $error));
    }
}