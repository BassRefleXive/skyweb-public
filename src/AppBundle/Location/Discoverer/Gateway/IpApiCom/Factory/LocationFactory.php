<?php

declare(strict_types=1);

namespace AppBundle\Location\Discoverer\Gateway\IpApiCom\Factory;

use AppBundle\Location\Discoverer\Gateway\IpApiCom\Exception\ResponseException;
use AppBundle\Location\Discoverer\Gateway\Response\Location;

class LocationFactory
{
    public function fromArray(array $data): Location
    {
        if (isset($data['message'])) {
            throw ResponseException::error($data['message']);
        }

        if (!isset($data['countryCode'])) {
            throw ResponseException::missingCountryCode();
        }

        return new Location($data['countryCode']);
    }
}