<?php

declare(strict_types=1);

namespace AppBundle\Location\Discoverer\Gateway\Exception;

interface GatewayExceptionInterface extends \Throwable
{

}