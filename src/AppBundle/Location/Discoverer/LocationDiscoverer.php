<?php

declare(strict_types=1);

namespace AppBundle\Location\Discoverer;

use AppBundle\Location\Discoverer\Exception\DiscovererException;
use AppBundle\Location\Discoverer\Gateway\Exception\GatewayExceptionInterface;
use AppBundle\Location\Discoverer\Gateway\GatewayInterface;
use AppBundle\Location\Exception\LocationExceptionInterface;
use AppBundle\Location\Model\Country;
use AppBundle\Location\Repository\CountryRepositoryInterface;
use Psr\Log\LoggerInterface;

class LocationDiscoverer
{
    private $gateway;
    private $countryRepository;
    private $logger;

    public function __construct(GatewayInterface $gateway, CountryRepositoryInterface $countryRepository, LoggerInterface $logger)
    {
        $this->gateway = $gateway;
        $this->countryRepository = $countryRepository;
        $this->logger = $logger;
    }

    public function country(string $ip): Country
    {
        try {
            $gatewayCountry = $this->gateway->discover($ip);
        } catch (GatewayExceptionInterface $e) {
            $this->logger->error('Failed to discover country by ip {ip}. Gateway error: {error}', [
                'ip' => $ip,
                'error' => $e->getMessage(),
            ]);

            throw DiscovererException::gateway($e);
        }

        try {
            $country = $this->countryRepository->getByIso2Code($gatewayCountry->countryCode());
        } catch (LocationExceptionInterface $e) {
            $this->logger->error('Failed to discover country by ip {ip}. Country with code {code} not found in our database.', [
                'ip' => $ip,
                'code' => $gatewayCountry->countryCode(),
            ]);

            throw DiscovererException::missingCountry($e);
        }

        return $country;
    }
}