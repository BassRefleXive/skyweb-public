<?php

declare(strict_types=1);

namespace AppBundle\Location\Discoverer\Exception;

interface DiscovererExceptionInterface extends \Throwable
{

}