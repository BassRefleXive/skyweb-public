<?php

declare(strict_types=1);

namespace AppBundle\Location\Exception;

interface LocationExceptionInterface extends \Throwable
{

}