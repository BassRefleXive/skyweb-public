<?php

declare(strict_types=1);

namespace AppBundle\Location\Enum\Doctrine;

use AppBundle\Location\Enum\CountryStatus;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class CountryStatusType extends Type
{
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return Type::INTEGER;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?CountryStatus
    {
        return null !== $value
            ? CountryStatus::byValue((int) $value)
            : null;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        /** @var CountryStatus $value */
        return null !== $value
            ? $value->getValue()
            : null;
    }

    public function getName(): string
    {
        return 'country_status';
    }
}