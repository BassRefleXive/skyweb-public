<?php

declare(strict_types=1);

namespace AppBundle\Location\Repository\Doctrine;

use AppBundle\Location\Exception\CountryNotFoundException;
use AppBundle\Location\Model\Country;
use AppBundle\Location\Repository\CountryRepositoryInterface;
use AppBundle\Repository\BaseDoctrineRepository;

class CountryRepository extends BaseDoctrineRepository implements CountryRepositoryInterface
{
    public function getByIso2Code(string $iso2Code): Country
    {
        /** @var Country|null $country */
        $country = $this->findOneBy([
            'codeIso2' => $iso2Code,
        ]);

        if (null === $country) {
            throw CountryNotFoundException::missingIso2Code(mb_strtolower($iso2Code));
        }

        return $country;
    }
}