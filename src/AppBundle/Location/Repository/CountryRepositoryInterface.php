<?php

declare(strict_types=1);

namespace AppBundle\Location\Repository;

use AppBundle\Location\Model\Country;
use AppBundle\Repository\BaseRepositoryInterface;

interface CountryRepositoryInterface extends BaseRepositoryInterface
{
    public function getByIso2Code(string $iso2Code): Country;
}