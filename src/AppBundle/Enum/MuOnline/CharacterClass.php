<?php

declare(strict_types = 1);

namespace AppBundle\Enum\MuOnline;


use MabeEnum\Enum;

final class CharacterClass extends Enum
{
    const DARK_WIZARD = 0;
    const SOUL_MASTER = 1;
    const GRAND_MASTER = 2;

    const DARK_KNIGHT = 16;
    const BLADE_KNIGHT = 17;
    const BLADE_MASTER = 18;

    const ELF = 32;
    const MUSE_ELF = 33;
    const HIGH_ELF = 34;

    const MAGIC_GLADIATOR = 48;
    const DUEL_MASTER = 50;

    const DARK_LORD = 64;
    const LORD_EMPEROR = 66;

    const SUMMONER = 80;
    const BLOODY_SUMMONER = 81;
    const DIMENSION_MASTER = 82;

    const RAGE_FIGHTER = 96;
    const FIST_MASTER = 98;

    public function isDarkWizard(bool $includeEvolution = true): bool
    {
        return $includeEvolution
            ? $this->is(self::DARK_WIZARD) || $this->is(self::SOUL_MASTER) || $this->is(self::GRAND_MASTER)
            : $this->is(self::DARK_WIZARD);
    }

    public function isDarkKnight(bool $includeEvolution = true): bool
    {
        return $includeEvolution
            ? $this->is(self::DARK_KNIGHT) || $this->is(self::BLADE_KNIGHT) || $this->is(self::BLADE_MASTER)
            : $this->is(self::DARK_KNIGHT);
    }

    public function isFairyElf(bool $includeEvolution = true): bool
    {
        return $includeEvolution
            ? $this->is(self::ELF) || $this->is(self::MUSE_ELF) || $this->is(self::HIGH_ELF)
            : $this->is(self::ELF);
    }

    public function isMagicGladiator(bool $includeEvolution = true): bool
    {
        return $includeEvolution
            ? $this->is(self::MAGIC_GLADIATOR) || $this->is(self::DUEL_MASTER)
            : $this->is(self::MAGIC_GLADIATOR);
    }

    public function isDarkLord(bool $includeEvolution = true): bool
    {
        return $includeEvolution
            ? $this->is(self::DARK_LORD) || $this->is(self::LORD_EMPEROR)
            : $this->is(self::DARK_LORD);
    }

    public function isSummoner(bool $includeEvolution = true): bool
    {
        return $includeEvolution
            ? $this->is(self::SUMMONER) || $this->is(self::BLOODY_SUMMONER) || $this->is(self::DIMENSION_MASTER)
            : $this->is(self::SUMMONER);
    }

    public function isRageFighter(bool $includeEvolution = true): bool
    {
        return $includeEvolution
            ? $this->is(self::RAGE_FIGHTER) || $this->is(self::FIST_MASTER)
            : $this->is(self::RAGE_FIGHTER);
    }

    public function isThirdClass(): bool
    {
        return $this->is(self::GRAND_MASTER) ||
            $this->is(self::BLADE_MASTER) ||
            $this->is(self::HIGH_ELF) ||
            $this->is(self::DUEL_MASTER) ||
            $this->is(self::LORD_EMPEROR) ||
            $this->is(self::DIMENSION_MASTER) ||
            $this->is(self::FIST_MASTER);
    }

    public function name(): string
    {
        return [
            self::DARK_WIZARD      => 'Dark Wizard',
            self::SOUL_MASTER      => 'Soul Master',
            self::GRAND_MASTER     => 'Grand Master',
            self::DARK_KNIGHT      => 'Dark Knight',
            self::BLADE_KNIGHT     => 'Blade Knight',
            self::BLADE_MASTER     => 'Blade Master',
            self::ELF              => 'Elf',
            self::MUSE_ELF         => 'Muse Elf',
            self::HIGH_ELF         => 'High Elf',
            self::MAGIC_GLADIATOR  => 'Magic Gladiator',
            self::DUEL_MASTER      => 'Duel Master',
            self::DARK_LORD        => 'Dark Lord',
            self::LORD_EMPEROR     => 'Lord Emperor',
            self::SUMMONER         => 'Summoner',
            self::BLOODY_SUMMONER  => 'Bloody Summoner',
            self::DIMENSION_MASTER => 'Dimension Master',
            self::RAGE_FIGHTER     => 'Rage Fighter',
            self::FIST_MASTER      => 'Fist Master',
        ][$this->getValue()];
    }

    public function abbreviation(): string
    {
        return [
            self::DARK_WIZARD      => 'DW',
            self::SOUL_MASTER      => 'SM',
            self::GRAND_MASTER     => 'GM',
            self::DARK_KNIGHT      => 'DK',
            self::BLADE_KNIGHT     => 'BK',
            self::BLADE_MASTER     => 'BM',
            self::ELF              => 'FE',
            self::MUSE_ELF         => 'ME',
            self::HIGH_ELF         => 'HE',
            self::MAGIC_GLADIATOR  => 'MG',
            self::DUEL_MASTER      => 'DM',
            self::DARK_LORD        => 'DL',
            self::LORD_EMPEROR     => 'LE',
            self::SUMMONER         => 'SUM',
            self::BLOODY_SUMMONER  => 'BS',
            self::DIMENSION_MASTER => 'DimM',
            self::RAGE_FIGHTER     => 'RF',
            self::FIST_MASTER      => 'FM',
        ][$this->getValue()];
    }

    public function baseClassAbbreviation(): string
    {
        switch ($this->getValue()) {
            case self::DARK_WIZARD:
            case self::SOUL_MASTER:
            case self::GRAND_MASTER:
                return 'DW';
            case self::DARK_KNIGHT:
            case self::BLADE_KNIGHT:
            case self::BLADE_MASTER:
                return 'DK';
            case self::ELF:
            case self::MUSE_ELF:
            case self::HIGH_ELF:
                return 'FE';
            case self::MAGIC_GLADIATOR:
            case self::DUEL_MASTER:
                return 'MG';
            case self::DARK_LORD:
            case self::LORD_EMPEROR:
                return 'DL';
            case self::SUMMONER:
            case self::BLOODY_SUMMONER:
            case self::DIMENSION_MASTER:
                return 'SUM';
            case self::RAGE_FIGHTER:
            case self::FIST_MASTER:
                return 'RF';

        }

        throw new \LogicException(sprintf('Cannot find Base class abbreviation for class "%s".', $this->getValue()));
    }

    public function availableClasses(): array
    {
        return [
            self::DARK_WIZARD  => [
                self::DARK_KNIGHT,
                self::ELF,
                self::MAGIC_GLADIATOR,
                self::DARK_LORD,
            ],
            self::SOUL_MASTER  => [
                self::BLADE_KNIGHT,
                self::MUSE_ELF,
                self::MAGIC_GLADIATOR,
                self::DARK_LORD,
            ],
            self::GRAND_MASTER => [
                self::BLADE_MASTER,
                self::HIGH_ELF,
                self::DUEL_MASTER,
                self::LORD_EMPEROR,
            ],

            self::DARK_KNIGHT  => [
                self::DARK_WIZARD,
                self::ELF,
                self::MAGIC_GLADIATOR,
                self::DARK_LORD,
            ],
            self::BLADE_KNIGHT => [
                self::SOUL_MASTER,
                self::MUSE_ELF,
                self::MAGIC_GLADIATOR,
                self::DARK_LORD,
            ],
            self::BLADE_MASTER => [
                self::GRAND_MASTER,
                self::HIGH_ELF,
                self::DUEL_MASTER,
                self::LORD_EMPEROR,
            ],

            self::ELF      => [
                self::DARK_WIZARD,
                self::DARK_KNIGHT,
                self::MAGIC_GLADIATOR,
                self::DARK_LORD,
            ],
            self::MUSE_ELF => [
                self::SOUL_MASTER,
                self::BLADE_KNIGHT,
                self::MAGIC_GLADIATOR,
                self::DARK_LORD,
            ],
            self::HIGH_ELF => [
                self::GRAND_MASTER,
                self::BLADE_MASTER,
                self::DUEL_MASTER,
                self::LORD_EMPEROR,
            ],

            self::MAGIC_GLADIATOR => [
                self::DARK_WIZARD,
                self::DARK_KNIGHT,
                self::ELF,
                self::DARK_LORD,
            ],
            self::DUEL_MASTER     => [
                self::GRAND_MASTER,
                self::BLADE_MASTER,
                self::HIGH_ELF,
                self::LORD_EMPEROR,
            ],

            self::DARK_LORD    => [
                self::DARK_WIZARD,
                self::DARK_KNIGHT,
                self::ELF,
                self::MAGIC_GLADIATOR,
            ],
            self::LORD_EMPEROR => [
                self::GRAND_MASTER,
                self::BLADE_MASTER,
                self::HIGH_ELF,
                self::DUEL_MASTER,
            ],
        ][$this->getValue()];
    }

    public function masterSkillIdsMap(): array
    {
        return [
            self::GRAND_MASTER     => [
                'Peace'    => [
                    [300, null, null, 301],
                    [null, 302, 303, 304],
                    [305, 306, 307, 308],
                    [null, 309, 310, 311],
                    [312, 313, null, null],
                ],
                'Wisdom'   => [
                    [325, null, null, null],
                    [378, 379, 380, null],
                    [381, 382, 383, 384],
                    [385, 386, 334, 387],
                    [null, 388, 338, 389],
                ],
                'Overcome' => [
                    [347, null, null, null],
                    [397, 398, 399, null],
                    [400, 401, 402, null],
                    [403, 357, 358, 359],
                    [404, 405, null, 362],
                ],
            ],
            self::BLADE_MASTER     => [
                'Protection' => [
                    [300, null, null, 301],
                    [null, 302, 303, 304],
                    [305, 306, 307, 308],
                    [null, 309, 310, 311],
                    [312, 313, null, null],
                ],
                'Bravery'    => [
                    [325, null, null, null],
                    [326, 327, 328, 329],
                    [330, 331, null, null],
                    [332, 333, 334, 335],
                    [336, 337, 338, null],
                ],
                'Anger'      => [
                    [347, null, null, null],
                    [348, 349, 350, 351],
                    [352, 353, 354, 355],
                    [356, 357, 358, 359],
                    [360, 361, null, 362],
                ],
            ],
            self::HIGH_ELF         => [
                'Blessing'  => [
                    [300, null, null, 301],
                    [null, 302, 303, 304],
                    [305, 306, 307, 308],
                    [null, 309, 310, 311],
                    [312, 313, null, null],
                ],
                'Salvation' => [
                    [325, null, null, null],
                    [413, null, 414, 415],
                    [416, 417, 418, 419],
                    [420, null, 421, 334],
                    [422, 423, 424, 338],
                ],
                'Storm'     => [
                    [347, null, null, null],
                    [435, 436, 437, null],
                    [438, 439, 440, null],
                    [null, 357, 358, 359],
                    [441, 442, null, 362],
                ],
            ],
            self::DUEL_MASTER      => [
                'Solidity'        => [
                    [300, null, null, 301],
                    [null, 302, 303, 304],
                    [305, 306, 307, 308],
                    [null, 309, 310, 311],
                    [312, 313, null, null],
                ],
                'Fighting Spirit' => [
                    [325, null, null, null],
                    [479, 480, 481, 482],
                    [483, 484, null, 485],
                    [486, 487, 488, 334],
                    [null, 490, 489, 338],
                ],
                'Ultimatum'       => [
                    [347, null, null, null],
                    [348, 349, 397, 398],
                    [352, 353, 400, 401],
                    [null, 357, 358, 359],
                    [405, 361, null, 362],
                ],
            ],
            self::LORD_EMPEROR     => [
                'Determination' => [
                    [300, null, null, 301],
                    [null, 302, 303, 304],
                    [305, 306, 307, 308],
                    [null, 309, 310, 311],
                    [312, 313, null, null],
                ],
                'Justice'       => [
                    [325, null, null, null],
                    [508, 509, 510, null],
                    [null, 511, 512, 513],
                    [514, 515, 516, 334],
                    [null, 517, 518, 338],
                ],
                'Conquer'       => [
                    [347, null, null, null],
                    [526, 527, 528, 529],
                    [530, 531, 532, 533],
                    [null, 357, 358, 359],
                    [534, 535, 361, 362],
                ],
            ],
            self::DIMENSION_MASTER => [
                'Guardian' => [
                    [300, null, null, 301],
                    [null, 302, 303, 304],
                    [305, 306, 307, 308],
                    [null, 309, 310, 311],
                    [312, 313, null, null],
                ],
                'Chaos'    => [
                    [325, null, null, null],
                    [448, 449, 450, null],
                    [451, 452, 453, 454],
                    [455, 456, null, 334],
                    [457, null, 458, 338],
                ],
                'Honor'    => [
                    [347, null, null, null],
                    [465, 466, null, null],
                    [467, 468, null, null],
                    [469, 357, 358, 359],
                    [470, 471, null, 362],
                ],
            ],
            self::FIST_MASTER      => [
                'Willpower'     => [
                    [578, null, null, 579],
                    [null, 580, 581, 582],
                    [583, 584, 585, 586],
                    [null, 587, 588, 589],
                    [590, 591, null, null],
                ],
                'Determination' => [
                    [599, null, null, null],
                    [551, 552, null, null],
                    [554, 555, null, null],
                    [null, null, 600, 557],
                    [558, 559, 601, 560],
                ],
                'Destruction'   => [
                    [603, null, null, null],
                    [568, 569, null, null],
                    [571, 572, null, null],
                    [573, 604, 605, 606],
                    [null, 607, null, 608],
                ],
            ],
        ][$this->getValue()];
    }
}