<?php

declare(strict_types=1);

namespace AppBundle\Enum\MuOnline\CastleSiege;

use MabeEnum\Enum;

final class RegisteredGuildState extends Enum
{
    public const ACTIVE = 0;
    public const GIVE_UP = 1;

    public function isActive(): bool
    {
        return $this->is(self::ACTIVE);
    }

    public function isGiveUp(): bool
    {
        return $this->is(self::GIVE_UP);
    }
}