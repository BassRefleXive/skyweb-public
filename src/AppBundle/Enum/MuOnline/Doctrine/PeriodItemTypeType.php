<?php

declare(strict_types = 1);

namespace AppBundle\Enum\MuOnline\Doctrine;

use AppBundle\Enum\MuOnline\PeriodItemType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class PeriodItemTypeType extends Type
{
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return Type::INTEGER;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?PeriodItemType
    {
        return 0 !== $value && null !== $value
            ? PeriodItemType::byValue($value)
            : null;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        /** @var PeriodItemType $value */
        return null !== $value
            ? $value->getValue()
            : null;
    }

    public function getName(): string
    {
        return 'period_item_type';
    }
}