<?php

declare(strict_types = 1);

namespace AppBundle\Enum\MuOnline\Doctrine\Gens;

use AppBundle\Enum\MuOnline\Gens\Family;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class FamilyType extends Type
{
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return Type::INTEGER;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?Family
    {
        return 0 !== $value && null !== $value
            ? Family::byValue($value)
            : null;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        /** @var Family $value */
        return null !== $value
            ? $value->getValue()
            : null;
    }

    public function getName(): string
    {
        return 'gens_family';
    }
}