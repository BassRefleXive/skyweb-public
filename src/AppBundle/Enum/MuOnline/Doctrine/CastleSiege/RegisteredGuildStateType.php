<?php

declare(strict_types = 1);

namespace AppBundle\Enum\MuOnline\Doctrine\CastleSiege;

use AppBundle\Enum\MuOnline\CastleSiege\RegisteredGuildState;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class RegisteredGuildStateType extends Type
{
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return Type::INTEGER;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?RegisteredGuildState
    {
        return null !== $value
            ? RegisteredGuildState::byValue((int) $value)
            : null;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        /** @var RegisteredGuildState $value */
        return null !== $value
            ? $value->getValue()
            : null;
    }

    public function getName(): string
    {
        return 'registered_guild_state';
    }
}