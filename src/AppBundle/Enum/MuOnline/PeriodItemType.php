<?php

declare(strict_types=1);

namespace AppBundle\Enum\MuOnline;


use MabeEnum\Enum;

final class PeriodItemType extends Enum
{
    public const SEAL = 1;
    public const ITEM = 2;
    public const CHARACTER_CARD = 3;
    public const VIP_CARD = 4;
}