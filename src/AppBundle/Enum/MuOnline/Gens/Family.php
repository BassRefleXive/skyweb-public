<?php


namespace AppBundle\Enum\MuOnline\Gens;


use MabeEnum\Enum;

final class Family extends Enum
{
    public const DUPRIAN = 1;
    public const VANERT = 2;
}