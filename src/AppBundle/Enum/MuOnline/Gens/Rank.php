<?php


namespace AppBundle\Enum\MuOnline\Gens;


use AppBundle\Entity\MuOnline\Gens;
use MabeEnum\Enum;

final class Rank extends Enum
{
    public const GRAND_DUKE = 'Grand Duke';
    public const DUKE = 'Duke';
    public const MARQUIS = 'Marquis';
    public const COUNT = 'Count';
    public const VISCOUNT = 'Viscount';
    public const SUMMONER = 'Summoner';
    public const BARON = 'Baron';
    public const KNIGHT_COMMANDER = 'Knight Commander';
    public const SUPERIOR_KNIGHT = 'Superior Knight';
    public const KNIGHT = 'Knight';
    public const OFFICER = 'Officer';
    public const LIEUTENANT = 'Lieutenant';
    public const SERGEANT = 'Sergeant';
    public const PRIVATE = 'Private';

    private const POINTS_REQUIREMENT = [
        self::GRAND_DUKE       => 10000,
        self::DUKE             => 10000,
        self::MARQUIS          => 10000,
        self::COUNT            => 10000,
        self::VISCOUNT         => 10000,
        self::SUMMONER         => 10000,
        self::BARON            => 10000,
        self::KNIGHT_COMMANDER => 10000,
        self::SUPERIOR_KNIGHT  => 10000,
        self::KNIGHT           => 6000,
        self::OFFICER          => 3000,
        self::LIEUTENANT       => 1500,
        self::SERGEANT         => 500,
    ];

    private const RANK_REQUIREMENT = [
        self::GRAND_DUKE       => 1,
        self::DUKE             => 2,
        self::MARQUIS          => 3,
        self::COUNT            => 4,
        self::VISCOUNT         => 5,
        self::SUMMONER         => 6,
        self::BARON            => 7,
        self::KNIGHT_COMMANDER => 8,
    ];

    public static function byGens(Gens $gens): self
    {
        $pointsRequirements = self::POINTS_REQUIREMENT;
        rsort($pointsRequirements);

        $rankRequirements = self::RANK_REQUIREMENT;

        foreach ($pointsRequirements as $pointsRequirementRank => $pointsRequirement) {
            if (
                $gens->points() >= $pointsRequirements &&
                (
                    (isset($rankRequirements[$pointsRequirementRank]) && $gens->rank() === $rankRequirements[$pointsRequirementRank]) ||
                    !isset($rankRequirements[$pointsRequirementRank])
                )
            ) {
                return self::byValue($pointsRequirementRank);
            }
        }

        return self::byValue(self::PRIVATE);
    }

    public function isGrandDuke() { return $this->is(self::GRAND_DUKE); }
    public function isDuke() { return $this->is(self::DUKE); }
    public function isMarquis() { return $this->is(self::MARQUIS); }
    public function isCount() { return $this->is(self::COUNT); }
    public function isViscount() { return $this->is(self::VISCOUNT); }
    public function isSummoner() { return $this->is(self::SUMMONER); }
    public function isBaron() { return $this->is(self::BARON); }
    public function isKnightCommander() { return $this->is(self::KNIGHT_COMMANDER); }
    public function isSuperiorKnight() { return $this->is(self::SUPERIOR_KNIGHT); }
    public function isKnight() { return $this->is(self::KNIGHT); }
    public function isOfficer() { return $this->is(self::OFFICER); }
    public function isLieutenant() { return $this->is(self::LIEUTENANT); }
    public function isSergeant() { return $this->is(self::SERGEANT); }
    public function isPrivate() { return $this->is(self::PRIVATE); }
}