<?php

declare(strict_types = 1);


namespace AppBundle\Enum\Application;


use MabeEnum\Enum;

final class EmailState extends Enum
{
    public const ACTIVE = 'A';
    public const UNSUBSCRIBED = 'U';
    public const INVALID = 'I';

    public function isActive(): bool
    {
        return $this->is(self::ACTIVE);
    }

    public function isUnSubscribed(): bool
    {
        return $this->is(self::UNSUBSCRIBED);
    }

    public function isInvalid(): bool
    {
        return $this->is(self::INVALID);
    }
}