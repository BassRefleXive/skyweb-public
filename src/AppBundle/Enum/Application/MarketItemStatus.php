<?php

declare(strict_types = 1);

namespace AppBundle\Enum\Application;


use MabeEnum\Enum;

final class MarketItemStatus extends Enum
{
    public const ACTIVE = 0;
    public const SOLD = 1;
}