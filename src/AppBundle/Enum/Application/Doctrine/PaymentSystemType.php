<?php

declare(strict_types = 1);

namespace AppBundle\Enum\Application\Doctrine;

use AppBundle\Enum\Application\PaymentSystem;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class PaymentSystemType extends Type
{
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return 'VARCHAR(255)';
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?PaymentSystem
    {
        return null !== $value
            ? PaymentSystem::byValue($value)
            : null;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        /** @var PaymentSystem $value */
        return null !== $value
            ? $value->getValue()
            : null;
    }

    public function getName(): string
    {
        return 'vote_type';
    }
}