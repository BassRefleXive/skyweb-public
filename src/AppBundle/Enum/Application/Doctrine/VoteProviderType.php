<?php

declare(strict_types = 1);

namespace AppBundle\Enum\Application\Doctrine;

use AppBundle\Enum\Application\VoteProvider;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class VoteProviderType extends Type
{
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return 'VARCHAR(255)';
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?VoteProvider
    {
        return null !== $value
            ? VoteProvider::byValue($value)
            : null;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        /** @var VoteProvider $value */
        return null !== $value
            ? $value->getValue()
            : null;
    }

    public function getName(): string
    {
        return 'vote_provider';
    }
}