<?php

namespace AppBundle\Enum\Application\Doctrine\Config\CharacterEvolution;

use AppBundle\Enum\Application\Config\CharacterEvolution\ResetPaymentForm;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class ResetPaymentFormType extends Type
{
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return Type::INTEGER;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?ResetPaymentForm
    {
        return null !== $value
            ? ResetPaymentForm::byValue((int) $value)
            : null;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        /** @var ResetPaymentForm $value */
        return null !== $value
            ? $value->getValue()
            : null;
    }

    public function getName(): string
    {
        return 'reset_payment_form';
    }
}