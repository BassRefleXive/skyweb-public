<?php

namespace AppBundle\Enum\Application\Doctrine\Config\CharacterEvolution;

use AppBundle\Enum\Application\Config\CharacterEvolution\ResetForm;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class ResetFormType extends Type
{
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return Type::INTEGER;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?ResetForm
    {
        return null !== $value
            ? ResetForm::byValue((int) $value)
            : null;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        /** @var ResetForm $value */
        return null !== $value
            ? $value->getValue()
            : null;
    }

    public function getName(): string
    {
        return 'reset_form';
    }
}