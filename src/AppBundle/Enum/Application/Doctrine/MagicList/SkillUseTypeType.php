<?php

declare(strict_types = 1);

namespace AppBundle\Enum\Application\Doctrine\MagicList;

use AppBundle\Enum\Application\MagicList\SkillUseType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class SkillUseTypeType extends Type
{
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return Type::INTEGER;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?SkillUseType
    {
        return null !== $value
            ? SkillUseType::byValue((int) $value)
            : null;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?int
    {
        /** @var SkillUseType $value */
        return null !== $value
            ? $value->getValue()
            : null;
    }

    public function getName(): string
    {
        return 'magic_list_skill_use_type';
    }
}