<?php

declare(strict_types = 1);

namespace AppBundle\Enum\Application\Doctrine\MagicList;

use AppBundle\Enum\Application\MagicList\SkillType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class SkillTypeType extends Type
{
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return Type::INTEGER;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?SkillType
    {
        return null !== $value
            ? SkillType::byValue((int) $value)
            : null;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?int
    {
        /** @var SkillType $value */
        return null !== $value
            ? $value->getValue()
            : null;
    }

    public function getName(): string
    {
        return 'magic_list_skill_type';
    }
}