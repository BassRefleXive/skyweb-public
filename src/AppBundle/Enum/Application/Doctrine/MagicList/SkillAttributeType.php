<?php

declare(strict_types = 1);

namespace AppBundle\Enum\Application\Doctrine\MagicList;

use AppBundle\Enum\Application\MagicList\SkillAttribute;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class SkillAttributeType extends Type
{
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return Type::INTEGER;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?SkillAttribute
    {
        return null !== $value
            ? SkillAttribute::byValue((int) $value)
            : null;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?int
    {
        /** @var SkillAttribute $value */
        return null !== $value
            ? $value->getValue()
            : null;
    }

    public function getName(): string
    {
        return 'magic_list_skill_attribute';
    }
}