<?php

declare(strict_types = 1);

namespace AppBundle\Enum\Application;


use MabeEnum\Enum;

final class PaymentSystem extends Enum
{
    public const INTERKASSA = 'interkassa';
    public const PAYPAL = 'paypal';

    public function isInterKassa(): bool
    {
        return $this->is(self::INTERKASSA);
    }

    public function isPayPal(): bool
    {
        return $this->is(self::PAYPAL);
    }
}