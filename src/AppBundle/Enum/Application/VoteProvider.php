<?php


namespace AppBundle\Enum\Application;


use AppBundle\Entity\Application\VoteRewardConfig;
use AppBundle\Exception\Application\Vote\UnknownVoteRewardException;
use MabeEnum\Enum;

final class VoteProvider extends Enum
{
    public const MMOTOP = 'mmotop';
    public const QTOP = 'qtop';
    public const XTREMETOP100 = 'xtremetop100';
    public const GTOP100 = 'gtop100';
    public const TOPSERVERS200 = 'topservers200';
    public const TOPG = 'topg';

    public function isMmoTop(): bool
    {
        return $this->is(self::MMOTOP);
    }

    public function isQTop(): bool
    {
        return $this->is(self::QTOP);
    }

    public function isXtremeTop100(): bool
    {
        return $this->is(self::XTREMETOP100);
    }

    public function isGtop100(): bool
    {
        return $this->is(self::GTOP100);
    }

    public function isTopServers200(): bool
    {
        return $this->is(self::TOPSERVERS200);
    }

    public function isTopG(): bool
    {
        return $this->is(self::TOPG);
    }

    public function reward(VoteType $type, VoteRewardConfig $config): int
    {
        switch (true) {
            case $this->isMmoTop():
                {
                    return $type->isRegular()
                        ? $config->getMmotopRewardRegularVote()
                        : $config->getMmotopRewardSmsVote();
                }
            case $this->isQTop():
                {
                    return $type->isRegular()
                        ? $config->getQtopRewardRegularVote()
                        : $config->getQtopRewardSmsVote();
                }
            case $this->isXtremeTop100():
                {
                    return $config->getRewardXtremeTop100Vote();
                }
            case $this->isGtop100():
                {
                    return $config->getRewardGTop100Vote();
                }
            case $this->isTopServers200():
                {
                    return $config->getRewardTopServers200Vote();
                }
            case $this->isTopG():
                {
                    return $config->getRewardTopGVote();
                }
            default:
                {
                    throw UnknownVoteRewardException::create($this, $type);
                }
        }
    }
}