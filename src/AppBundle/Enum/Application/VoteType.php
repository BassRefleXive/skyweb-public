<?php


namespace AppBundle\Enum\Application;


use MabeEnum\Enum;

final class VoteType extends Enum
{
    public const REGULAR = 'R';
    public const PAID = 'S';

    public function isRegular(): bool
    {
        return $this->is(self::REGULAR);
    }

    public function isPaid(): bool
    {
        return $this->is(self::PAID);
    }
}