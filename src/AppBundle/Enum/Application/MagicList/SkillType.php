<?php

declare(strict_types=1);

namespace AppBundle\Enum\Application\MagicList;

use MabeEnum\Enum;

final class SkillType extends Enum
{
    public const UNKNOWN__1 = -1;
    public const UNKNOWN_0 = 0;
    public const UNKNOWN_1 = 1;
}