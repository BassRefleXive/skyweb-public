<?php

declare(strict_types=1);

namespace AppBundle\Enum\Application\MagicList;

use MabeEnum\Enum;

final class SkillUseType extends Enum
{
    public const NORMAL = 0;
    public const CASTLE_SIEGE = 1;
    public const NOT_USED = 2;
    public const ML_PASSIVE = 3;
    public const ML_ACTIVE = 4;

    public function isNormal(): bool
    {
        return $this->is(self::NORMAL);
    }

    public function isCastleSiege(): bool
    {
        return $this->is(self::CASTLE_SIEGE);
    }

    public function isMasterPassive(): bool
    {
        return $this->is(self::ML_PASSIVE);
    }

    public function isMasterActive(): bool
    {
        return $this->is(self::ML_ACTIVE);
    }

    public function isMaster(): bool
    {
        return $this->isMasterActive() || $this->isMasterPassive();
    }
}