<?php

declare(strict_types=1);

namespace AppBundle\Enum\Application\MagicList;

use MabeEnum\Enum;

final class SkillAttribute extends Enum
{
    public const NONE = -1;
    public const ICE = 0;
    public const POISON = 1;
    public const LIGHTNING = 2;
    public const FIRE = 3;
    public const EARTH = 4;
    public const WIND = 5;
    public const WATER = 6;

    public function none(): bool
    {
        return $this->is(self::NONE);
    }

    public function isIce(): bool
    {
        return $this->is(self::ICE);
    }

    public function isPoison(): bool
    {
        return $this->is(self::POISON);
    }

    public function isLightning(): bool
    {
        return $this->is(self::LIGHTNING);
    }

    public function isFire(): bool
    {
        return $this->is(self::FIRE);
    }

    public function isEarth(): bool
    {
        return $this->is(self::EARTH);
    }

    public function isWind(): bool
    {
        return $this->is(self::WIND);
    }

    public function isWater(): bool
    {
        return $this->is(self::WATER);
    }
}