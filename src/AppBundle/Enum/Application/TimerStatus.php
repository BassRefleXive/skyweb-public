<?php


namespace AppBundle\Enum\Application;


use MabeEnum\Enum;

final class TimerStatus extends Enum
{
    public const ACTIVE = 0;
    public const PAUSED = 1;
}