<?php

declare(strict_types=1);

namespace AppBundle\Enum\Application;


use MabeEnum\Enum;

final class FundsType extends Enum
{
    public const ZEN = 'zen';
    public const WCOIN = 'wcoin';

    public function isZen(): bool
    {
        return $this->is(self::ZEN);
    }

    public function isWCoin(): bool
    {
        return $this->is(self::WCOIN);
    }
}