<?php

namespace AppBundle\Enum\Application\Config\CharacterEvolution;

use MabeEnum\Enum;

final class ResetForm extends Enum
{
    public const KEEP_POINTS = 0;
    public const KEEP_POINTS_ADD_RESET_POINTS = 1;
    public const RESET_POINTS_ADD_RESET_POINTS = 2;

    public function isKeepPoints(): bool
    {
        return $this->is(self::KEEP_POINTS);
    }

    public function isKeepPointsAddResetPoints(): bool
    {
        return $this->is(self::KEEP_POINTS_ADD_RESET_POINTS);
    }

    public function isResetPointsAddResetPoints(): bool
    {
        return $this->is(self::RESET_POINTS_ADD_RESET_POINTS);
    }
}