<?php

namespace AppBundle\Enum\Application\Config\CharacterEvolution;

use MabeEnum\Enum;

final class ResetPaymentForm extends Enum
{
    public const FREE = 0;
    public const CONSTANT = 1;
    public const MULTIPLIER = 2;

    public function isFree(): bool
    {
        return $this->is(self::FREE);
    }

    public function isConstant(): bool
    {
        return $this->is(self::CONSTANT);
    }

    public function isMultiplier(): bool
    {
        return $this->is(self::MULTIPLIER);
    }
}