<?php

declare(strict_types = 1);

namespace AppBundle\Enum\Application;


use MabeEnum\Enum;

final class ZenBankOperationType extends Enum
{
    public const DEPOSIT = 0;
    public const WITHDRAWAL = 1;

    public function isDeposit(): bool
    {
        return $this->is(self::DEPOSIT);
    }

    public function isWithdrawal(): bool
    {
        return $this->is(self::WITHDRAWAL);
    }
}