<?php

declare(strict_types = 1);

namespace AppBundle\Enum\Application;

use MabeEnum\Enum;

final class UserStatus extends Enum
{
    public const ACTIVE = 1;
    public const BANNED = 0;
}