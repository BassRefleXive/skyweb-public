<?php

namespace AppBundle\TrackingScript;

class Configuration
{
    private $googleCode;
    private $yandexCode;
    private $mmotopCode;
    private $qtopCode;
    private $chat;

    public function __construct(string $googleCode, string $yandexCode, string $mmotopCode, string $qtopCode, Chat $chat)
    {
        $this->googleCode = $googleCode;
        $this->yandexCode = $yandexCode;
        $this->mmotopCode = $mmotopCode;
        $this->qtopCode = $qtopCode;
        $this->chat = $chat;
    }

    public function googleCode(): string
    {
        return $this->googleCode;
    }

    public function yandexCode(): string
    {
        return $this->yandexCode;
    }

    public function mmotopCode(): string
    {
        return $this->mmotopCode;
    }

    public function qtopCode(): string
    {
        return $this->qtopCode;
    }

    public function chat(): Chat
    {
        return $this->chat;
    }
}