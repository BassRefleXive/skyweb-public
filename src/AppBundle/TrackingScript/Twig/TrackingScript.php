<?php


namespace AppBundle\TrackingScript\Twig;

use AppBundle\Entity\Application\User\User;
use AppBundle\TrackingScript\Configuration;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class TrackingScript extends \Twig_Extension
{
    private const GOOGLE = 'google';
    private const YANDEX = 'yandex';
    private const MMOTOP = 'mmotop';
    private const QTOP = 'qtop';
    private const TAWK = 'tawk';

    /*private */const PROVIDERS = [
        self::GOOGLE,
        self::YANDEX,
        self::MMOTOP,
        self::QTOP,
        self::TAWK,
    ];

    private $configuration;
    private $twig;
    private $tokenStorage;

    public function __construct(
        Configuration $configuration,
        \Twig_Environment $twig,
        TokenStorageInterface $tokenStorage
    )
    {
        $this->configuration = $configuration;
        $this->twig = $twig;
        $this->tokenStorage = $tokenStorage;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('tracking_script', [$this, 'renderTrackingScript'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('vote_block', [$this, 'renderVoteBlock'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('chat_block', [$this, 'renderChatBlock'], ['is_safe' => ['html']]),
        ];
    }

    public function renderTrackingScript(string $provider): string
    {
        if (!in_array($provider, self::PROVIDERS, true)) {
            throw new \InvalidArgumentException(
                sprintf('Unknown provider "%s". Known providers are: %s', $provider, self::PROVIDERS)
            );
        }

        switch ($provider) {
            case self::GOOGLE: {
                return $this->renderGoogleAnalyticsTrackingScript();
            }
            case self::YANDEX: {
                return $this->renderYandexWebvizorTrackingScript();
            }
            case self::MMOTOP: {
                return $this->renderMMOTopCode();
            }
            case self::QTOP: {
                return $this->renderQTopCode();
            }
            case self::TAWK: {
                return $this->renderChatTawk();
            }
        }

        return '';
    }

    public function renderVoteBlock(string $provider): string
    {
        if (!in_array($provider, self::PROVIDERS, true)) {
            throw new \InvalidArgumentException(
                sprintf('Unknown provider "%s". Known providers are: %s', $provider, self::PROVIDERS)
            );
        }

        switch ($provider) {
            case self::MMOTOP: {
                return $this->renderMMOTopCode();
            }
            case self::QTOP: {
                return $this->renderQTopCode();
            }
        }

        return '';
    }

    public function renderChatBlock(string $provider): string
    {
        if (!in_array($provider, self::PROVIDERS, true)) {
            throw new \InvalidArgumentException(
                sprintf('Unknown provider "%s". Known providers are: %s', $provider, self::PROVIDERS)
            );
        }

        switch ($provider) {
            case self::TAWK: {
                return $this->renderChatTawk();
            }
        }

        return '';
    }

    private function renderGoogleAnalyticsTrackingScript(): string
    {
        return $this->twig->render(
            '@App/tracking_scripts/google/code.html.twig',
            [
                'code' => $this->configuration->googleCode(),
                'user' => $this->user(),
            ]
        );
    }

    private function renderYandexWebvizorTrackingScript(): string
    {
        return $this->twig->render(
            '@App/tracking_scripts/yandex/code.html.twig',
            [
                'code' => $this->configuration->yandexCode(),
            ]
        );
    }

    private function renderMMOTopCode(): string
    {
        return $this->twig->render(
            '@App/tracking_scripts/mmotop/code.html.twig',
            [
                'code' => $this->configuration->mmotopCode(),
            ]
        );
    }

    private function renderQTopCode(): string
    {
        return $this->twig->render(
            '@App/tracking_scripts/qtop/code.html.twig',
            [
                'code' => $this->configuration->qtopCode(),
            ]
        );
    }

    private function renderChatTawk(): string
    {
        return $this->twig->render(
            '@App/tracking_scripts/chat/tawk.html.twig',
            [
                'id' => $this->configuration->chat()->tawk(),
            ]
        );
    }

    private function user(): ?User
    {
        if (null === $token = $this->tokenStorage->getToken()) {
            return null;
        }

        if (null === $user = $token->getUser()) {
            return null;
        }

        if (!$user instanceof User) {
            return null;
        }

        return $user;
    }
}