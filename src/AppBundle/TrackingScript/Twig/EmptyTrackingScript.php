<?php


namespace AppBundle\TrackingScript\Twig;

class EmptyTrackingScript extends TrackingScript
{
    public function renderTrackingScript(string $provider): string
    {
        return '';
    }
}