<?php


namespace AppBundle\TrackingScript;


class Chat
{
    private $tawk;

    public function __construct(string $tawk)
    {
        $this->tawk = $tawk;
    }

    public function tawk(): string
    {
        return $this->tawk;
    }
}