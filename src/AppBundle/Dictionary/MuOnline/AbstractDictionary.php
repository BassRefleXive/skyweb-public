<?php

namespace AppBundle\Dictionary\MuOnline;


abstract class AbstractDictionary
{
    protected static $values = [];

    public static function getValueByKey($key)
    {
        if (!array_key_exists($key, static::$values)) {
            throw new \InvalidArgumentException(sprintf(
                    'Key %s not found in dictionary.',
                    $key
                )
            );
        }

        return static::$values[$key];
    }

    public static function getKeyByValue($value)
    {
        $keys = array_flip(static::$values);

        if (!array_key_exists($value, $keys)) {
            throw new \InvalidArgumentException(
                sprintf(
                    'Cannot determine key for given value %s.',
                    $value
                )
            );
        }

        return $keys[$value];
    }

    public static function getValues(): array
    {
        return static::$values;
    }
}