<?php

namespace AppBundle\Dictionary\MuOnline;


class CharacterKillerLevelDictionary extends AbstractDictionary
{

    const PK_HERO = 0;
    const PK_HERO_LVL_2 = 1;
    const PK_HERO_LVL_1 = 2;
    const PK_COMMONER = 3;
    const PK_LEVEL_1 = 4;
    const PK_LEVEL_2 = 5;
    const PK_MURDER = 6;
    const PK_PHONOMAN = 7;

    protected static $values = [
        CharacterKillerLevelDictionary::PK_HERO       => 'Hero',
        CharacterKillerLevelDictionary::PK_HERO_LVL_2 => 'Hero Level 2',
        CharacterKillerLevelDictionary::PK_HERO_LVL_1 => 'Hero Level 1',
        CharacterKillerLevelDictionary::PK_COMMONER   => 'Commoner',
        CharacterKillerLevelDictionary::PK_LEVEL_1    => 'PK Level 1',
        CharacterKillerLevelDictionary::PK_LEVEL_2    => 'PK Level 2',
        CharacterKillerLevelDictionary::PK_MURDER     => 'Murder',
        CharacterKillerLevelDictionary::PK_PHONOMAN   => 'Phonoman',
    ];

}