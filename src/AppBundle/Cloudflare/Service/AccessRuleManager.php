<?php

declare(strict_types=1);

namespace AppBundle\Cloudflare\Service;

use AppBundle\Cloudflare\Api\Gateway\AccessRuleGateway;
use AppBundle\Cloudflare\Enum\AccessRule\Mode;
use AppBundle\Cloudflare\Enum\AccessRule\TargetType;
use AppBundle\Cloudflare\Model\AccessRule;

class AccessRuleManager
{
    private $gateway;

    public function __construct(AccessRuleGateway $gateway)
    {
        $this->gateway = $gateway;
    }

    public function all(): array
    {
        return $this->gateway->all();
    }

    public function create(Mode $mode, TargetType $type, string $target, string $notes = null): AccessRule
    {
        return $this->gateway->create(
            AccessRule::new($mode, $type, $target, $notes)
        );
    }
}