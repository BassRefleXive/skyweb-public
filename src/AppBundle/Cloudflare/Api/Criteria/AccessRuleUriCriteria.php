<?php

declare(strict_types=1);

namespace AppBundle\Cloudflare\Api\Criteria;

use AppBundle\Http\Criteria\UriCriteria as BaseUriCriteria;
use AppBundle\Http\Criteria\UriCriteriaInterface;

final class AccessRuleUriCriteria extends BaseUriCriteria implements UriCriteriaInterface
{
    public static function all(): self
    {
        return new self('/user/firewall/access_rules/rules');
    }

    public static function create(): self
    {
        return new self('/user/firewall/access_rules/rules');
    }
}