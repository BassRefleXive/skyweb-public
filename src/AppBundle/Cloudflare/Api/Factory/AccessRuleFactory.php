<?php

declare(strict_types=1);

namespace AppBundle\Cloudflare\Api\Factory;


use AppBundle\Cloudflare\Enum\AccessRule\Mode;
use AppBundle\Cloudflare\Enum\AccessRule\TargetType;
use AppBundle\Cloudflare\Model\AccessRule;

class AccessRuleFactory
{
    public function fromArray(array $data): AccessRule
    {
        return AccessRule::existing(
            $data['id'],
            Mode::byValue($data['mode']),
            TargetType::byValue($data['configuration']['target']),
            $data['configuration']['value'],
            $data['notes']
        );
    }
}