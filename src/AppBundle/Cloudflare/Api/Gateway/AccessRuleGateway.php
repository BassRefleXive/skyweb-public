<?php

declare(strict_types=1);

namespace AppBundle\Cloudflare\Api\Gateway;

use AppBundle\Cloudflare\Api\Criteria\AccessRuleUriCriteria;
use AppBundle\Cloudflare\Api\Factory\AccessRuleFactory;
use AppBundle\Cloudflare\Api\RequestBody\AccessRule\Factory\CreateBodyParamsFactory;
use AppBundle\Cloudflare\Model\AccessRule;
use AppBundle\Http\Assembler\RequestAssembler;
use AppBundle\Http\Criteria\RequestCriteria;
use AppBundle\Http\HttpClientInterface;
use AppBundle\Http\RequestBody\Factory\JsonBodyFactory;
use AppBundle\Http\Response\JsonResponseParser;
use Symfony\Component\HttpFoundation\Request;

class AccessRuleGateway
{
    private $client;
    private $requestAssembler;
    private $responseParser;
    private $accessRuleFactory;
    private $jsonRequestBodyFactory;

    public function __construct(HttpClientInterface $client, RequestAssembler $requestAssembler)
    {
        $this->client = $client;
        $this->requestAssembler = $requestAssembler;
        $this->responseParser = new JsonResponseParser();
        $this->jsonRequestBodyFactory = new JsonBodyFactory();
        $this->accessRuleFactory = new AccessRuleFactory();
    }

    public function all(): array
    {
        $requestCriteria = new RequestCriteria(
            AccessRuleUriCriteria::all(),
            Request::METHOD_GET
        );

        $response = $this->client->send($this->requestAssembler->fromCriteria($requestCriteria));

        return array_map([$this->accessRuleFactory, 'fromArray'], $this->responseParser->responseArray($response)['result']);
    }

    public function create(AccessRule $rule): AccessRule
    {
        $requestCriteria = new RequestCriteria(
            AccessRuleUriCriteria::create(),
            Request::METHOD_POST,
            $this->jsonRequestBodyFactory->create(
                (new CreateBodyParamsFactory())->fromRule($rule)
            )
        );

        $response = $this->client->send($this->requestAssembler->fromCriteria($requestCriteria));

        return $this->accessRuleFactory->fromArray($this->responseParser->responseArray($response)['result']);
    }
}