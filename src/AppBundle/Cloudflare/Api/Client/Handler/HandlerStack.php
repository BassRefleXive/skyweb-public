<?php

declare(strict_types=1);

namespace AppBundle\Cloudflare\Api\Client\Handler;

use GuzzleHttp\HandlerStack as GuzzleHandlerStack;

class HandlerStack extends GuzzleHandlerStack
{

}