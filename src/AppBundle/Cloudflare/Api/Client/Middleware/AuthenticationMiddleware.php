<?php

declare(strict_types=1);

namespace AppBundle\Cloudflare\Api\Client\Middleware;

use GuzzleHttp\Promise\PromiseInterface;
use Psr\Http\Message\RequestInterface;

class AuthenticationMiddleware
{
    private $email;
    private $key;

    public function __construct(string $email, string $key)
    {
        $this->email = $email;
        $this->key = $key;
    }

    public function __invoke(callable $handler): callable
    {
        return function (RequestInterface $request, array $options) use ($handler): PromiseInterface {
            $request = $request
                ->withHeader('X-Auth-Email', $this->email)
                ->withHeader('X-Auth-Key', $this->key);

            return $handler($request, $options);
        };
    }
}