<?php

declare(strict_types=1);

namespace AppBundle\Cloudflare\Api\RequestBody\AccessRule;

use AppBundle\Cloudflare\Enum\AccessRule\Mode;
use AppBundle\Cloudflare\Enum\AccessRule\TargetType;
use AppBundle\Http\RequestBody\BodyParamsInterface;

class CreateBodyParams implements BodyParamsInterface
{
    private $mode;
    private $type;
    private $value;
    private $notes;

    public function __construct(Mode $mode, TargetType $type, string $value, string $notes = null)
    {
        $this->mode = $mode;
        $this->type = $type;
        $this->value = $value;
        $this->notes = $notes;
    }

    public function params(): array
    {
        return array_merge(
            [
                'mode' => $this->mode->getValue(),
                'configuration' => [
                    'target' => $this->type->getValue(),
                    'value' => $this->value,
                ],

            ],
            null !== $this->notes
                ? ['notes' => $this->notes,]
                : []
        );
    }
}