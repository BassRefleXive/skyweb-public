<?php

declare(strict_types=1);

namespace AppBundle\Cloudflare\Api\RequestBody\AccessRule\Factory;

use AppBundle\Cloudflare\Api\RequestBody\AccessRule\CreateBodyParams;
use AppBundle\Cloudflare\Model\AccessRule;

class CreateBodyParamsFactory
{
    public function fromRule(AccessRule $rule): CreateBodyParams
    {
        return new CreateBodyParams(
            $rule->mode(),
            $rule->type(),
            $rule->target(),
            $rule->notes()
        );
    }
}