<?php

declare(strict_types=1);

namespace AppBundle\Cloudflare\Enum\AccessRule;

use MabeEnum\Enum;

final class Mode extends Enum
{
    public const BLOCK = 'block';
    public const CHALLENGE = 'challenge';
    public const WHITELIST = 'whitelist';
    public const JS_CHALLENGE = 'js_challenge';
}