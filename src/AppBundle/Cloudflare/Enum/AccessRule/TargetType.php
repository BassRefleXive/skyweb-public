<?php

declare(strict_types=1);

namespace AppBundle\Cloudflare\Enum\AccessRule;


use MabeEnum\Enum;

final class TargetType extends Enum
{
    public const IP = 'ip';
    public const IP_RANGE = 'ip_range';
    public const ASN = 'asn';
    public const COUNTRY = 'country';
}