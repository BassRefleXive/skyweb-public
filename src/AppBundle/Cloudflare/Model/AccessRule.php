<?php

declare(strict_types=1);

namespace AppBundle\Cloudflare\Model;

use AppBundle\Cloudflare\Enum\AccessRule\Mode;
use AppBundle\Cloudflare\Enum\AccessRule\TargetType;

class AccessRule
{
    private $id;
    private $notes;
    private $mode;
    private $type;
    private $target;

    public static function new(Mode $mode, TargetType $type, string $target, string $notes = null): self
    {
        $model = new self();

        $model->notes = $notes;
        $model->mode = $mode;
        $model->type = $type;
        $model->target = $target;

        return $model;
    }

    public static function existing(string $id, Mode $mode, TargetType $type, string $target, ?string $notes): self
    {
        $model = new self();

        $model->id = $id;
        $model->notes = $notes;
        $model->mode = $mode;
        $model->type = $type;
        $model->target = $target;

        return $model;
    }

    public function id(): ?string
    {
        return $this->id;
    }

    public function notes(): ?string
    {
        return $this->notes;
    }

    public function mode(): Mode
    {
        return $this->mode;
    }

    public function type(): TargetType
    {
        return $this->type;
    }

    public function target(): string
    {
        return $this->target;
    }
}