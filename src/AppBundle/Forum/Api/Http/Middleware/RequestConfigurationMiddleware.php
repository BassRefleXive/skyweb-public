<?php

declare(strict_types=1);

namespace AppBundle\Forum\Api\Http\Middleware;

use GuzzleHttp\Promise\PromiseInterface;
use Psr\Http\Message\RequestInterface;

class RequestConfigurationMiddleware
{
    public function __invoke(callable $handler): callable
    {
        return function (RequestInterface $request, array $options) use ($handler): PromiseInterface {
            if (in_array($request->getMethod(), ['POST', 'PUT'], true)) {
                $request = $request->withHeader('Content-Type', 'application/x-www-form-urlencoded');
            }

            return $handler($request, $options);
        };
    }
}