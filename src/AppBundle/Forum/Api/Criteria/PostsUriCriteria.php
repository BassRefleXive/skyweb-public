<?php

declare(strict_types = 1);

namespace AppBundle\Forum\Api\Criteria;


use AppBundle\Http\Criteria\UriCriteria;

class PostsUriCriteria extends UriCriteria
{
    public static function posts(string $key, array $forums = []): self
    {
        return new self(
            '',
            [],
            [
                '/forums/posts' => null,
//                'forums' => implode(',', $forums),
                'key' => $key,
                'sortBy' => 'date',
                'sortDir' => 'desc',
            ]
        );
    }
}