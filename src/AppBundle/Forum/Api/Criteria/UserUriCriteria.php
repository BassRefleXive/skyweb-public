<?php

declare(strict_types = 1);

namespace AppBundle\Forum\Api\Criteria;


use AppBundle\Http\Criteria\UriCriteria;

class UserUriCriteria extends UriCriteria
{
    public static function create(string $key): self
    {
        return new self(
            '',
            [],
            [
                '/core/members' => null,
                'key' => $key,
            ]
        );
    }

    public static function update(int $id, string $key): self
    {
        return new self(
            '',
            [],
            [
                sprintf('/core/members/%d', $id) => null,
                'key' => $key,
            ]
        );
    }
}