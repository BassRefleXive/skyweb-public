<?php

declare(strict_types=1);

namespace AppBundle\Forum\Api\RequestBody\User;

use AppBundle\Http\RequestBody\BodyParamsInterface;

class UpdateBodyParams implements BodyParamsInterface
{
    private $name;
    private $email;
    private $password;
    private $group;

    public function __construct(string $name, string $email, string $password, int $group)
    {
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
        $this->group = $group;
    }

    public function params(): array
    {
        return [
            'name' => $this->name,
            'email' => $this->email,
            'password' => $this->password,
            'group' => $this->group,
        ];
    }
}