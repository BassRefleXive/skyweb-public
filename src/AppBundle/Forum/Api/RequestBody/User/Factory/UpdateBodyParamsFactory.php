<?php

declare(strict_types=1);

namespace AppBundle\Forum\Api\RequestBody\User\Factory;

use AppBundle\Entity\Application\User\User;
use AppBundle\Forum\Api\RequestBody\User\UpdateBodyParams;
use AppBundle\Forum\Enum\Group;

class UpdateBodyParamsFactory
{
    public function fromUser(User $user): UpdateBodyParams
    {
        return new UpdateBodyParams(
            $user->getDisplayName(),
            $user->email(),
            $user->getPassword(),
            Group::MEMBER
        );
    }
}