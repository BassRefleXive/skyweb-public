<?php

declare(strict_types=1);

namespace AppBundle\Forum\Api\RequestBody\User\Factory;

use AppBundle\Entity\Application\User\User;
use AppBundle\Forum\Api\RequestBody\User\CreateBodyParams;
use AppBundle\Forum\Enum\Group;

class CreateBodyParamsFactory
{
    public function fromUser(User $user): CreateBodyParams
    {
        return new CreateBodyParams(
            $user->getDisplayName(),
            $user->email(),
            $user->getPassword(),
            Group::MEMBER
        );
    }
}