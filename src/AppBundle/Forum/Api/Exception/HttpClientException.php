<?php

declare(strict_types = 1);

namespace AppBundle\Forum\Api\Exception;

class HttpClientException extends \RuntimeException
{

}