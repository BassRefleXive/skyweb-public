<?php

declare(strict_types = 1);

namespace AppBundle\Forum\Api\Exception;


class ResponseException extends \RuntimeException
{
    public static function invalidFormat(string $format): self
    {
        return new self(sprintf('Invalid response format. Expected "%s".', $format));
    }

    public static function invalidStatus(int $expected, int $received): self
    {
        $exception = new self(sprintf('Invalid response status. Expected %d received %d.', $expected, $received));

        return $exception;
    }
}