<?php

declare(strict_types=1);

namespace AppBundle\Forum\Api\Exception;


class CreateUserException extends \RuntimeException
{
    public static final function create(\Throwable $parent): self
    {
        return new self('Failed to create user.', $parent->getCode(), $parent);
    }
}