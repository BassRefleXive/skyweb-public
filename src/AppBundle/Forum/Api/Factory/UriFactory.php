<?php

declare(strict_types = 1);

namespace AppBundle\Forum\Api\Factory;

use AppBundle\Http\Criteria\UriCriteriaInterface;
use GuzzleHttp\Psr7\Uri;

class UriFactory
{
    protected $endpointUrl;

    public function __construct(string $endpointUrl)
    {
        $this->endpointUrl = $endpointUrl;
    }

    public function create(UriCriteriaInterface $uriCriteria): string
    {
        $uri = new Uri($this->endpointUrl);

        return (string) $uri
            ->withPath(implode('', array_merge(
                [$uri->getPath()],
                [$uriCriteria->path()]
            )))
            ->withQuery($uriCriteria->query());
    }
}