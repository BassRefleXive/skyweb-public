<?php

declare(strict_types = 1);

namespace AppBundle\Forum\Api\Factory;

use AppBundle\Http\Criteria\RequestCriteria;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\RequestInterface;

class RequestFactory
{
    private $uriFactory;

    public function __construct(UriFactory $uriFactory)
    {
        $this->uriFactory = $uriFactory;
    }

    public function fromCriteria(RequestCriteria $criteria): RequestInterface
    {
        return new Request(
            $criteria->method(),
            $this->uriFactory->create($criteria->uriCriteria()),
            $criteria->headers(),
            $criteria->body()
        );
    }
}