<?php

declare(strict_types=1);

namespace AppBundle\Forum\Api\Gateway;

use AppBundle\Forum\Api\Http\Client;
use AppBundle\Forum\Api\Factory\RequestFactory;
use AppBundle\Forum\Api\Exception\ResponseException;
use Psr\Http\Message\ResponseInterface;

abstract class ForumGateway
{
    protected $client;
    protected $requestAssembler;
    protected $key;

    public function __construct(Client $client, RequestFactory $requestAssembler, string $key)
    {
        $this->client = $client;
        $this->requestAssembler = $requestAssembler;
        $this->key = $key;
    }

    protected final function responseArray(ResponseInterface $response, int $expectedStatus): array
    {
        if ($response->getStatusCode() !== $expectedStatus) {
            throw ResponseException::invalidStatus($expectedStatus, $response->getStatusCode());
        }

        $data = $response->getBody()->getContents();

        if (null === $data = json_decode($data, true)) {
            throw ResponseException::invalidFormat('json');
        }

        return $data;
    }
}