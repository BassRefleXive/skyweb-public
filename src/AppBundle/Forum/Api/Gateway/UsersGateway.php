<?php

declare(strict_types=1);

namespace AppBundle\Forum\Api\Gateway;

use AppBundle\Entity\Application\User\User as UserModel;
use AppBundle\Forum\Api\Criteria\UserUriCriteria;
use AppBundle\Forum\Api\Exception\CreateUserException;
use AppBundle\Forum\Api\Exception\ResponseException;
use AppBundle\Forum\Api\Http\Client;
use AppBundle\Forum\Api\Factory\RequestFactory;
use AppBundle\Forum\Api\RequestBody\User\Factory\CreateBodyParamsFactory;
use AppBundle\Forum\Api\RequestBody\User\Factory\UpdateBodyParamsFactory;
use AppBundle\Forum\Model\Builder\UserBuilder;
use AppBundle\Forum\Model\User;
use AppBundle\Http\Criteria\RequestCriteria;
use AppBundle\Http\Exception\HttpClientException;
use AppBundle\Http\RequestBody\Factory\UrlencodedBodyFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UsersGateway extends ForumGateway
{
    private $userBuilder;
    private $bodyFactory;

    public function __construct(Client $client, RequestFactory $requestAssembler, string $key)
    {
        parent::__construct($client, $requestAssembler, $key);

        $this->userBuilder = new UserBuilder();
        $this->bodyFactory = new UrlencodedBodyFactory();
    }

    public function create(UserModel $user): User
    {
        $requestCriteria = new RequestCriteria(
            UserUriCriteria::create($this->key),
            Request::METHOD_POST,
            $this->bodyFactory->create(
                (new CreateBodyParamsFactory())->fromUser($user)
            )
        );


        try {
            $response = $this->client->send($this->requestAssembler->fromCriteria($requestCriteria));

            return $this->userBuilder->fromArray($this->responseArray($response, Response::HTTP_CREATED))->build();
        } catch (HttpClientException|ResponseException $e) {
            throw CreateUserException::create($e);
        }
    }

    public function update(UserModel $user): User {
        $requestCriteria = new RequestCriteria(
            UserUriCriteria::update($user->forumId(), $this->key),
            Request::METHOD_POST,
            $this->bodyFactory->create(
                (new UpdateBodyParamsFactory())->fromUser($user)
            )
        );


        try {
            $response = $this->client->send($this->requestAssembler->fromCriteria($requestCriteria));

            return $this->userBuilder->fromArray($this->responseArray($response, Response::HTTP_CREATED))->build();
        } catch (HttpClientException|ResponseException $e) {
            throw CreateUserException::create($e);
        }
    }
}