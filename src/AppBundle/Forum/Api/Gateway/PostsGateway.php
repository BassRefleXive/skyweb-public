<?php

declare(strict_types=1);

namespace AppBundle\Forum\Api\Gateway;

use AppBundle\Forum\Api\Criteria\PostsUriCriteria;
use AppBundle\Forum\Api\Exception\HttpClientException;
use AppBundle\Forum\Api\Factory\RequestFactory;
use AppBundle\Forum\Api\Http\Client;
use AppBundle\Forum\Model\Builder\PostBuilder;
use AppBundle\Forum\Model\Post;
use AppBundle\Http\Criteria\RequestCriteria;
use Collection\Sequence;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PostsGateway extends ForumGateway
{
    private $postBuilder;

    public function __construct(Client $client, RequestFactory $requestAssembler, string $key)
    {
        parent::__construct($client, $requestAssembler, $key);

        $this->postBuilder = new PostBuilder();
    }

    public function posts(array $forums = []): Sequence
    {
        $requestCriteria = new RequestCriteria(
            PostsUriCriteria::posts($this->key, $forums),
            Request::METHOD_GET
        );

        try {
            $response = $this->client->send($this->requestAssembler->fromCriteria($requestCriteria));

            return new Sequence(
                array_map(function (array $element): Post {
                    return $this->postBuilder->fromArray($element)->build();
                }, $this->responseArray($response, Response::HTTP_OK)['results'])
            );
        } catch (HttpClientException $e) {
            if (false !== strpos(strtolower($e->getMessage()), 'timed out')) {
                return new Sequence();
            }

            throw $e;
        }
    }
}