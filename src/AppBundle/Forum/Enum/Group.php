<?php

declare(strict_types=1);

namespace AppBundle\Forum\Enum;

use MabeEnum\Enum;

final class Group extends Enum
{
    public const GUEST = 2;
    public const MEMBER = 3;
    public const ADMINISTRATOR = 4;
    public const MODERATOR = 6;
}