<?php

declare(strict_types=1);

namespace AppBundle\Forum\Model\Builder;


use AppBundle\Forum\Model\User;

class UserBuilder
{
    private $id;
    private $name;
    private $formattedName;
    private $email;
    private $joined;

    public function fromArray(array $data): self
    {
        $this->id = (int) $data['id'];
        $this->name = $data['name'];
        $this->formattedName = $data['formattedName'];
        $this->email = $data['email'];
        $this->joined = new \DateTimeImmutable($data['joined']);

        return $this;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function formattedName(): string
    {
        return $this->formattedName;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function joined(): \DateTimeImmutable
    {
        return $this->joined;
    }

    public function build(): User
    {
        return new User($this);
    }
}