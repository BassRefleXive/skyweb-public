<?php

declare(strict_types = 1);

namespace AppBundle\Forum\Model\Builder;

use AppBundle\Forum\Model\Post;

class PostBuilder
{
    private $id;
    private $author;
    private $date;
    private $content;
    private $url;

    public function fromArray(array $data): self
    {
        $this->id = (int) $data['id'];
        $this->author = $data['author']['name'];
        $this->date = new \DateTimeImmutable($data['date']);
        $this->content = $data['content'];
        $this->url = $data['url'];

        return $this;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function author(): string
    {
        return $this->author;
    }

    public function date(): \DateTimeImmutable
    {
        return $this->date;
    }

    public function content(): string
    {
        return $this->content;
    }

    public function url(): string
    {
        return $this->url;
    }

    public function build(): Post
    {
        return new Post($this);
    }
}