<?php

declare(strict_types=1);

namespace AppBundle\Forum\Model;


use AppBundle\Forum\Model\Builder\UserBuilder;

class User
{
    private $id;
    private $name;
    private $formattedName;
    private $email;
    private $joined;

    public function __construct(UserBuilder $builder)
    {
        $this->id = $builder->id();
        $this->name = $builder->name();
        $this->formattedName = $builder->formattedName();
        $this->email = $builder->email();
        $this->joined = $builder->joined();
    }

    public function id(): int
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function formattedName(): string
    {
        return $this->formattedName;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function joined(): \DateTimeImmutable
    {
        return $this->joined;
    }
}