<?php

declare(strict_types = 1);

namespace AppBundle\Forum\Model;

use AppBundle\Forum\Model\Builder\PostBuilder;

class Post
{
    private $id;
    private $author;
    private $date;
    private $content;
    private $url;

    public function __construct(PostBuilder $builder)
    {
        $this->id = $builder->id();
        $this->author = $builder->author();
        $this->date = $builder->date();
        $this->content = $builder->content();
        $this->url = $builder->url();
    }

    public function id(): int
    {
        return $this->id;
    }

    public function author(): string
    {
        return $this->author;
    }

    public function date(): \DateTimeImmutable
    {
        return $this->date;
    }

    public function content(): string
    {
        return $this->content;
    }

    public function url(): string
    {
        return $this->url;
    }
}