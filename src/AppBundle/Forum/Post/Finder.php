<?php

declare(strict_types = 1);

namespace AppBundle\Forum\Post;

use AppBundle\Forum\Api\Gateway\PostsGateway;
use Collection\SequenceInterface;

class Finder
{
    private $gateway;

    public function __construct(PostsGateway $gateway)
    {
        $this->gateway = $gateway;
    }

    public function lastFive(array $forums = []): SequenceInterface
    {
        return $this->gateway->posts($forums)->take(5);
    }
}