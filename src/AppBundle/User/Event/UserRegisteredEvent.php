<?php

declare(strict_types = 1);

namespace AppBundle\User\Event;

use AppBundle\Entity\Application\User\User;
use Symfony\Component\EventDispatcher\Event;

class UserRegisteredEvent extends Event
{
    public const USER_REGISTERED = 'event.user.registered';

    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function user(): User
    {
        return $this->user;
    }
}