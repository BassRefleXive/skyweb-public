<?php

declare(strict_types = 1);

namespace AppBundle\User\Listener\Registration;

use AppBundle\Forum\Api\Exception\CreateUserException;
use AppBundle\Forum\Api\Gateway\UsersGateway;
use AppBundle\Repository\Application\UserRepository;
use AppBundle\User\Event\UserRegisteredEvent;

class RegisterOnForumListener
{
    private $userRepository;
    private $usersForumGateway;

    public function __construct(UserRepository $userRepository, UsersGateway $usersForumGateway)
    {
        $this->userRepository = $userRepository;
        $this->usersForumGateway = $usersForumGateway;
    }

    public function onRegistration(UserRegisteredEvent $event): void
    {
        try {
            $forumUser = $this->usersForumGateway->create($event->user());

            $event->user()->registeredOnForum($forumUser);

            $this->userRepository->save($event->user());
        } catch (\Throwable $e) {
        }
    }
}