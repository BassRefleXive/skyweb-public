<?php

declare(strict_types = 1);

namespace AppBundle\User\Listener\Registration;

use AppBundle\Entity\Application\Config\RegistrationConfig;
use AppBundle\Repository\Application\Config\RegistrationConfigRepository;
use AppBundle\Repository\Application\UserRepository;
use AppBundle\Service\Application\VipService;
use AppBundle\User\Event\UserRegisteredEvent;

class GrantVipListener
{
    private $userRepository;
    private $registrationConfigRepository;
    private $vipService;

    public function __construct(
        UserRepository $userRepository,
        RegistrationConfigRepository $registrationConfigRepository,
        VipService $vipService
    )
    {
        $this->userRepository = $userRepository;
        $this->registrationConfigRepository = $registrationConfigRepository;
        $this->vipService = $vipService;
    }

    public function onRegistration(UserRegisteredEvent $event): void
    {
        /** @var RegistrationConfig $config */
        $config = $this->registrationConfigRepository->findAll()[0];

        if ($config->getAddVipDays() <= 0) {
            return;
        }

        $this->vipService->activate(
            $config->getAddVipType(),
            $event->user()->getLastUserServerAccount(),
            $config->getAddVipDays()
        );
    }
}