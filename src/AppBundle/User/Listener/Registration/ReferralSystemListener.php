<?php

declare(strict_types = 1);

namespace AppBundle\User\Listener\Registration;

use AppBundle\Entity\Application\User\User;
use AppBundle\Repository\Application\UserRepository;
use AppBundle\User\Event\UserRegisteredEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class ReferralSystemListener
{
    private $userRepository;
    private $requestStack;

    public function __construct(
        UserRepository $userRepository,
        RequestStack $requestStack
    )
    {
        $this->userRepository = $userRepository;
        $this->requestStack = $requestStack;
    }

    public function onRegistration(UserRegisteredEvent $event): void
    {
        if (null === $referredBy = $this->request()->cookies->get('referredBy')) {
            return;
        }

        $referredBy = $this->userRepository->find($referredBy);

        if (!$referredBy instanceof User) {
            return;
        }

        $referredBy->addReferral($event->user());

        $this->userRepository->save($event->user());
    }

    private function request(): Request
    {
        return $this->requestStack->getCurrentRequest();
    }
}