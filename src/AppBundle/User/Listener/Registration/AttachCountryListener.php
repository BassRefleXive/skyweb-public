<?php

declare(strict_types = 1);

namespace AppBundle\User\Listener\Registration;

use AppBundle\Location\Discoverer\Exception\DiscovererExceptionInterface;
use AppBundle\Location\Discoverer\LocationDiscoverer;
use AppBundle\Repository\Application\UserRepository;
use AppBundle\User\Event\UserRegisteredEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class AttachCountryListener
{
    private $userRepository;
    private $requestStack;
    private $locationDiscoverer;

    public function __construct(
        UserRepository $userRepository,
        LocationDiscoverer $locationDiscoverer,
        RequestStack $requestStack
    )
    {
        $this->userRepository = $userRepository;
        $this->locationDiscoverer = $locationDiscoverer;
        $this->requestStack = $requestStack;
    }

    public function onRegistration(UserRegisteredEvent $event): void
    {
        if (null === $ip = $this->request()->getClientIp()) {
            return;
        }

        try {
            $event->user()->fromCountry($this->locationDiscoverer->country($ip));

            $this->userRepository->save($event->user());
        } catch (DiscovererExceptionInterface $e) {
        }
    }

    private function request(): Request
    {
        return $this->requestStack->getCurrentRequest();
    }
}