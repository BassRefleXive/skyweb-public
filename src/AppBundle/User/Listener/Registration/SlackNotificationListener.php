<?php

declare(strict_types = 1);


namespace AppBundle\User\Listener\Registration;


use AppBundle\Slack\Services\UserEventNotificationService;
use AppBundle\User\Event\UserRegisteredEvent;

class SlackNotificationListener
{
    private $userEventNotificationService;

    public function __construct(UserEventNotificationService $userEventNotificationService)
    {
        $this->userEventNotificationService = $userEventNotificationService;
    }

    public function onRegistration(UserRegisteredEvent $event): void
    {
        $this->userEventNotificationService->registered($event->user());
    }
}