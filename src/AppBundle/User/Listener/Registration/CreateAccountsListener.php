<?php

declare(strict_types = 1);


namespace AppBundle\User\Listener\Registration;


use AppBundle\Entity\Application\Server;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Service\Application\ServerService;
use AppBundle\Service\MuOnline\AccountService;
use AppBundle\Slack\Services\UserEventNotificationService;
use AppBundle\User\Event\UserRegisteredEvent;

class CreateAccountsListener
{
    private $accountService;
    private $serverService;
    private $userEventNotificationService;

    public function __construct(
        AccountService $accountService,
        ServerService $serverService,
        UserEventNotificationService $userEventNotificationService
    )
    {
        $this->accountService = $accountService;
        $this->serverService = $serverService;
        $this->userEventNotificationService = $userEventNotificationService;
    }

    public function onRegistration(UserRegisteredEvent $event): void
    {
        $this->serverService->getList()->map(function (Server $server) use ($event) {
            try {
                $this->accountService->createAccount($event->user(), $server);
            } catch (DisplayableException $e) {
                throw $e;
            } catch (\Throwable $e) {
                $this->userEventNotificationService->serverAccountCreationFailed($event->user(), $server);

                throw new DisplayableException('errors.47');
            }
        });
    }
}