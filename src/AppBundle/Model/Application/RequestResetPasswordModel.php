<?php


namespace AppBundle\Model\Application;


class RequestResetPasswordModel
{
    private $username;
    private $email;

    public function getUsername()
    {
        return $this->username;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }
}