<?php

namespace AppBundle\Model\Application;

class StatsShopModel
{
    protected $step;

    public function getStep()
    {
        return $this->step;
    }

    public function setStep(int $step): StatsShopModel
    {
        $this->step = $step;

        return $this;
    }

}