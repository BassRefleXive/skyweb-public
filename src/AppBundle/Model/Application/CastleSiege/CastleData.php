<?php

declare(strict_types=1);

namespace AppBundle\Model\Application\CastleSiege;

use AppBundle\Entity\Application\Config\CastleSiegeCycle;
use AppBundle\Entity\MuOnline\CastleSiege\CastleData as CastleDataEntity;
use AppBundle\Exception\Application\CastleSiege\UnknownCycleException;
use League\Period\Period;

class CastleData
{
    private $data;
    private $cycles;

    public function __construct(CastleDataEntity $data, array $cycles)
    {
        $this->data = $data;
        $this->cycles = $cycles;
    }

    public function data(): CastleDataEntity
    {
        return $this->data;
    }

    public function cycles(): array
    {
        return array_slice($this->cycles, 0, count($this->cycles) - 1);
    }

    public function appliedCycle(int $stage): Period
    {
        try {
            $startDateCycle = $this->getCycleForStage($stage);
        } catch (UnknownCycleException $e) {
            $startDateCycle = $this->cycles[0];
        }

        try {
            $endDateCycle = $this->getCycleForStage($stage + 1);
        } catch (UnknownCycleException $e) {
            $endDateCycle = $this->cycles[count($this->cycles) - 1];
        }

        return new Period(
            (clone $this->data->startDate())->add(new \DateInterval(sprintf('P%dDT%dH%dM', $startDateCycle->getDay(), $startDateCycle->getHour(), $startDateCycle->getMinute()))),
            (clone $this->data->startDate())->add(new \DateInterval(sprintf('P%dDT%dH%dM', $endDateCycle->getDay(), $endDateCycle->getHour(), $endDateCycle->getMinute())))
        );
    }

    public function secondsTillNextCycle(int $stage): int {
        $stage = $this->appliedCycle($stage);

        $reference = new \DateTimeImmutable;
        $endTime = $reference->add($stage->getDateInterval());

        return $stage->getEndDate()->getTimestamp() - $reference->getTimestamp();
    }

    public function isActivePeriod(Period $period): bool
    {
        return $period->contains(new \DateTimeImmutable());
    }

    private function getCycleForStage(int $stage): CastleSiegeCycle
    {
        $cycles = array_filter(
            $this->cycles,
            function (CastleSiegeCycle $cycle) use ($stage): bool {
                return $cycle->getStage() === $stage;
            }
        );

        if (!count($cycles)) {
            throw UnknownCycleException::missingByStage($stage);
        }

        if (count($cycles) > 1) {
            throw new \LogicException(sprintf('There is more than one cycle with stage #%d.', $stage));
        }

        return array_pop($cycles);
    }
}