<?php


namespace AppBundle\Model\Application;


class ContactRequestModel
{
    private $email;

    private $subject;

    private $text;

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail(string $email): ContactRequestModel
    {
        $this->email = $email;

        return $this;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function setSubject(string $subject): ContactRequestModel
    {
        $this->subject = $subject;

        return $this;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText(string $text): ContactRequestModel
    {
        $this->text = $text;

        return $this;
    }
}