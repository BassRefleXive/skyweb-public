<?php

declare(strict_types=1);

namespace AppBundle\Model\Application;


use AppBundle\Entity\Application\Items\ItemInfo;
use AppBundle\Entity\MuOnline\PeriodItemInfo;

class BuffEffectItem
{
    private $itemInfo;
    private $periodItemInfo;

    public function __construct(ItemInfo $itemInfo, PeriodItemInfo $periodItemInfo)
    {
        $this->itemInfo = $itemInfo;
        $this->periodItemInfo = $periodItemInfo;
    }

    public function itemInfo(): ItemInfo
    {
        return $this->itemInfo;
    }

    public function periodItemInfo(): PeriodItemInfo
    {
        return $this->periodItemInfo;
    }
}