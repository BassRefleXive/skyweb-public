<?php

namespace AppBundle\Model\Application;


class LocaleModel
{
    protected $locale;

    public function getLocale()
    {
        return $this->locale;
    }

    public function setLocale(string $locale): LocaleModel
    {
        $this->locale = $locale;

        return $this;
    }

}