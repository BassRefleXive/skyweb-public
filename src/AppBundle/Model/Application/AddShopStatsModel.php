<?php

namespace AppBundle\Model\Application;


use AppBundle\Entity\MuOnline\Character;

class AddShopStatsModel
{
    protected $character;
    protected $stats;

    public function getCharacter()
    {
        return $this->character;
    }

    public function setCharacter(Character $character): AddShopStatsModel
    {
        $this->character = $character;

        return $this;
    }

    public function getStats()
    {
        return $this->stats;
    }

    public function setStats(int $stats): AddShopStatsModel
    {
        $this->stats = $stats;

        return $this;
    }

}