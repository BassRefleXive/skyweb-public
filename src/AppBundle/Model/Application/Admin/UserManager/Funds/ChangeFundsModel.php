<?php

declare(strict_types = 1);

namespace AppBundle\Model\Application\Admin\UserManager\Funds;


abstract class ChangeFundsModel
{
    private $account;

    public function setAccount(string $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getAccount(): ?string
    {
        return $this->account;
    }
}