<?php

declare(strict_types = 1);

namespace AppBundle\Model\Application\Admin\UserManager\Funds;


class ChangeCoinsModel extends ChangeFundsModel
{
    private $wCoinC;
    private $wCoinP;
    private $wCoinG;

    public function getWCoinC(): ?int
    {
        return $this->wCoinC;
    }

    public function setWCoinC(int $wCoinC): self
    {
        $this->wCoinC = $wCoinC;

        return $this;
    }

    public function getWCoinP(): ?int
    {
        return $this->wCoinP;
    }

    public function setWCoinP(int $wCoinP): self
    {
        $this->wCoinP = $wCoinP;

        return $this;
    }

    public function getWCoinG(): ?int
    {
        return $this->wCoinG;
    }

    public function setWCoinG(int $wCoinG): self
    {
        $this->wCoinG = $wCoinG;

        return $this;
    }
}