<?php

declare(strict_types = 1);

namespace AppBundle\Model\Application\Admin\UserManager\Funds;


class ChangeZenBankModel
{
    private $account;
    private $amount;

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function setAccount(int $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getAccount(): ?int
    {
        return $this->account;
    }
}