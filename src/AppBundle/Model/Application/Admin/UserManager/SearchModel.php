<?php

declare(strict_types = 1);

namespace AppBundle\Model\Application\Admin\UserManager;

class SearchModel
{
    private $keyword;

    public function setKeyword(string $keyword): self
    {
        $this->keyword = $keyword;

        return $this;
    }

    public function getKeyword(): ?string
    {
        return $this->keyword;
    }
}