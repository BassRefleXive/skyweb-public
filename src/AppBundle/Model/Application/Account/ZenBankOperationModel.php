<?php

declare(strict_types = 1);

namespace AppBundle\Model\Application\Account;


use AppBundle\Enum\Application\ZenBankOperationType;

class ZenBankOperationModel
{
    private $amount;
    private $type;

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function amount(): ?int
    {
        return $this->amount;
    }

    public function setType(int $type): self
    {
        $this->type = ZenBankOperationType::byValue($type);

        return $this;
    }

    public function type(): ?ZenBankOperationType
    {
        return $this->type;
    }
}