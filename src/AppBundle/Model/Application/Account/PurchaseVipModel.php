<?php


namespace AppBundle\Model\Application\Account;


class PurchaseVipModel
{
    private $days;
    private $dailyRate;

    public function days(): ?int
    {
        return $this->days;
    }

    public function setDays(int $days): self
    {
        $this->days = $days;

        return $this;
    }

    public function dailyRate(): int
    {
        return $this->dailyRate;
    }

    public function setDailyRate(int $dailyRate): self
    {
        $this->dailyRate = $dailyRate;

        return $this;
    }
}