<?php


namespace AppBundle\Model\Application\Account;


class WCoinShopPurchaseModel
{
    private $coinsPerUsd;
    private $minAmount;
    private $coins;

    public function coinsPerUsd(): ?int
    {
        return $this->coinsPerUsd;
    }

    public function setCoinsPerUsd(int $coinsPerUsd): self
    {
        $this->coinsPerUsd = $coinsPerUsd;

        return $this;
    }

    public function minAmount(): ?float
    {
        return $this->minAmount;
    }

    public function setMinAmount(float $minAmount): self
    {
        $this->minAmount = $minAmount;

        return $this;
    }

    public function coins(): ?int
    {
        return $this->coins;
    }

    public function setCoins(int $coins): self
    {
        $this->coins = $coins;

        return $this;
    }
}