<?php

declare(strict_types = 1);

namespace AppBundle\Model\Application\Account;


class HideInfoModel
{
    private $days;
    private $dailyRate;

    public function __construct(int $dailyRate)
    {
        $this->dailyRate = $dailyRate;
    }

    public function dailyRate(): int
    {
        return $this->dailyRate;
    }

    public function setDailyRate(int $dailyRate): self
    {
        $this->dailyRate = $dailyRate;

        return $this;
    }

    public function days(): ?int
    {
        return $this->days;
    }

    public function setDays(int $days): self
    {
        $this->days = $days;

        return $this;
    }
}