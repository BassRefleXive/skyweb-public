<?php

declare(strict_types=1);

namespace AppBundle\Model\Application\Account;


use AppBundle\Entity\Application\UserServerAccount;
use AppBundle\Enum\Application\FundsType;

class FundsTransferModel
{
    private $currency;
    private $amount;
    private $from;
    private $to;

    public function __construct(UserServerAccount $from)
    {
        $this->from = $from;
    }

    public function setCurrency(string $currency): void
    {
        $this->currency = FundsType::byValue($currency);
    }

    public function currency(): ?FundsType
    {
        return $this->currency;
    }

    public function setAmount(int $amount): void
    {
        $this->amount = $amount;
    }

    public function amount(): ?int
    {
        return $this->amount;
    }

    public function from(): UserServerAccount
    {
        return $this->from;
    }

    public function setTo(string $to): void
    {
        $this->to = $to;
    }

    public function to(): ?string
    {
        return $this->to;
    }
}