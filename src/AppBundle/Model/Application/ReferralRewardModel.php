<?php

namespace AppBundle\Model\Application;

class ReferralRewardModel
{
    protected $reward;

    public function getReward()
    {
        return $this->reward;
    }

    public function setReward(int $reward): ReferralRewardModel
    {
        $this->reward = $reward;

        return $this;
    }

}