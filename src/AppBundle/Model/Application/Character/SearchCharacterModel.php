<?php

namespace AppBundle\Model\Application\Character;

class SearchCharacterModel
{
    private $nick;

    public function setNick(string $nick): self
    {
        $this->nick = $nick;

        return $this;
    }

    public function getNick(): ?string
    {
        return $this->nick;
    }
}