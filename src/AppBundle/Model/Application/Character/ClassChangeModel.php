<?php

declare(strict_types = 1);

namespace AppBundle\Model\Application\Character;


class ClassChangeModel extends CharacterModel
{
    private $class;
    private $price;

    public function getClass()
    {
        return $this->class;
    }

    public function setClass(int $class): self
    {
        $this->class = $class;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }
}