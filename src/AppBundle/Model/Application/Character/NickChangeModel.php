<?php

namespace AppBundle\Model\Application\Character;


class NickChangeModel extends CharacterModel
{
    private $nick;
    private $price;

    public function getNick()
    {
        return $this->nick;
    }

    public function setNick(string $nick): NickChangeModel
    {
        $this->nick = $nick;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice(int $price): NickChangeModel
    {
        $this->price = $price;

        return $this;
    }
}