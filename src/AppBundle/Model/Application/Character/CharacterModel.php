<?php


namespace AppBundle\Model\Application\Character;


use AppBundle\Entity\MuOnline\Character;

abstract class CharacterModel
{
    private $character;

    public function getCharacter(): ?Character
    {
        return $this->character;
    }

    public function setCharacter(?Character $character): self
    {
        $this->character = $character;

        return $this;
    }
}