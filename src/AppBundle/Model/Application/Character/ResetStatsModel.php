<?php

declare(strict_types = 1);

namespace AppBundle\Model\Application\Character;


class ResetStatsModel extends CharacterModel
{
    private $price;

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }
}