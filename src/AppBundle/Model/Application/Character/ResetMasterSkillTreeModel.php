<?php


namespace AppBundle\Model\Application\Character;


class ResetMasterSkillTreeModel extends CharacterModel
{
    private $price;

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }
}