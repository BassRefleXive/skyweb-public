<?php


namespace AppBundle\Model\Application\Votes\Builder;


use AppBundle\Enum\Application\VoteType;
use AppBundle\Model\Application\Votes\VoteModel;

class MmoTopVoteModelBuilder extends VoteModelBuilder
{
    public function fromString(string $data): VoteModel
    {
        $data = explode("\t", $data);

        if (count($data) !== 5) {
            throw new \RuntimeException('Invalid vote string. Must be exactly 5 data attributes.');
        }

        return $this->withId((int)trim($data[0]))
            ->withDateTime(new \DateTime(trim($data[1])))
            ->withIp((string)trim($data[2]))
            ->withName((string)trim($data[3]))
            ->withType(
                VoteType::byValue(
                    1 === ((int)trim($data[4]))
                        ? VoteType::REGULAR
                        : VoteType::PAID
                )
            )
            ->build();
    }
}