<?php


namespace AppBundle\Model\Application\Votes\Builder;

use AppBundle\Enum\Application\VoteType;
use AppBundle\Model\Application\Votes\VoteModel;

abstract class VoteModelBuilder
{
    private $id;
    private $dateTime;
    private $ip;
    private $name;
    private $type;

    public function withId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function withDateTime(\DateTimeInterface $dateTime): self
    {
        $this->dateTime = $dateTime;

        return $this;
    }

    public function dateTime(): \DateTimeInterface
    {
        return $this->dateTime;
    }

    public function withIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function ip(): string
    {
        return $this->ip;
    }

    public function withName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function withType(VoteType $voteType): self
    {
        $this->type = $voteType;

        return $this;
    }

    public function type(): VoteType
    {
        return $this->type;
    }

    public function build(): VoteModel
    {
        $this->validate();

        return new VoteModel($this);
    }

    private function validate()
    {
        foreach ($this as $property => $value) {
            if (null === $value) {
                throw new \InvalidArgumentException(sprintf('Property "%s" must be set when building "%s".', $property, self::class));
            }
        }
    }

    abstract public function fromString(string $data): VoteModel;
}