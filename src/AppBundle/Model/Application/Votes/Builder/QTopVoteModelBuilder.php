<?php


namespace AppBundle\Model\Application\Votes\Builder;


use AppBundle\Enum\Application\VoteType;
use AppBundle\Model\Application\Votes\VoteModel;

class QTopVoteModelBuilder extends VoteModelBuilder
{
    public function fromString(string $data): VoteModel
    {
        $data = explode("||", $data);

        if (6 !== count($data)) {
            throw new \RuntimeException('Invalid vote string. Must be exactly 6 data attributes.');
        }

        return $this->withId((int)trim($data[0]))
            ->withDateTime(new \DateTime(trim($data[1]) . ' ' . trim($data[2])))
            ->withIp((string)trim($data[3]))
            ->withName((string)trim($data[4]))
            ->withType(
                VoteType::byValue(
                    1 === ((int)trim($data[5]))
                        ? VoteType::REGULAR
                        : VoteType::PAID
                )
            )
            ->build();
    }
}