<?php

namespace AppBundle\Model\Application\Votes;

use AppBundle\Enum\Application\VoteType;
use AppBundle\Model\Application\Votes\Builder\VoteModelBuilder;

class VoteModel
{
    private $id;
    private $dateTime;
    private $ip;
    private $name;
    private $type;

    public function __construct(VoteModelBuilder $builder)
    {
        $this->id = $builder->id();
        $this->dateTime = $builder->dateTime();
        $this->ip = $builder->ip();
        $this->name = $builder->name();
        $this->type = $builder->type();
    }

    public function id(): int
    {
        return $this->id;
    }

    public function dateTime(): \DateTimeInterface
    {
        return $this->dateTime;
    }

    public function ip(): string
    {
        return $this->ip;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function type(): VoteType
    {
        return $this->type;
    }
}