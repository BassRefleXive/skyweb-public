<?php


namespace AppBundle\Model\Application;


class ResetPasswordModel
{
    private $password;

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        return $this->password = $password;
    }
}