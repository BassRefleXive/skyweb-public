<?php


namespace AppBundle\Form\Application\Account;


use AppBundle\Model\Application\Account\WCoinShopPurchaseModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class WCoinShopPurchaseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('coinsPerUsd', HiddenType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                ],
            ])
            ->add('minAmount', HiddenType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                ],
            ])
            ->add('coins', IntegerType::class, [
                'required'    => true,
                'attr' => [
                    'min' => 0,
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                ],
                'label' => 'wcoin_shop.index.2',
            ])
            ->add('money', NumberType::class, [
                'required'    => true,
                'mapped' => false,
                'attr' => [
                    'min' => 0,
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                ],
                'label' => 'wcoin_shop.index.4',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => WCoinShopPurchaseModel::class,
        ]);
    }
}