<?php

declare(strict_types=1);

namespace AppBundle\Form\Application\Account;

use AppBundle\Enum\Application\FundsType;
use AppBundle\Model\Application\Account\FundsTransferModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class FundsTransferType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('amount', IntegerType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 1,
                    ]),
                ],
                'attr'        => [
                    'placeholder' => 'funds_transfer.2',
                    'min' => 1,
                ],
                'label' => 'funds_transfer.2',
            ])
            ->add('currency', ChoiceType::class, [
                'required'                  => true,
                'choices'                   => [
                    'funds_transfer.1' => FundsType::WCOIN,
                    'funds_transfer.0' => FundsType::ZEN,
                ],
                'constraints'               => [
                    new Assert\NotBlank(),
                ],
                'label' => 'funds_transfer.3',
            ])
            ->add('to', TextType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 3,
                        'max' => 10,
                    ]),
                ],
                'attr'        => [
                    'placeholder' => 'funds_transfer.4',
                ],
                'label' => 'funds_transfer.4',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FundsTransferModel::class,
        ]);
    }
}