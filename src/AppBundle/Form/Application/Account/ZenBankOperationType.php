<?php

declare(strict_types = 1);

namespace AppBundle\Form\Application\Account;


use AppBundle\Model\Application\Account\ZenBankOperationModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Enum\Application\ZenBankOperationType as ZenBankOperationTypeEnum;

class ZenBankOperationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('amount', IntegerType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 1,
                    ]),
                ],
                'attr'        => [
                    'placeholder' => 'zen_bank.2',
                    'min' => 1,
                ],
                'label' => 'zen_bank.2',
            ])
            ->add('type', ChoiceType::class, [
                'required'                  => true,
                'choices'                   => [
                    'zen_bank.3' => ZenBankOperationTypeEnum::WITHDRAWAL,
                    'zen_bank.4' => ZenBankOperationTypeEnum::DEPOSIT,
                ],
                'constraints'               => [
                    new Assert\NotBlank(),
                ],
                'label' => 'zen_bank.1',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ZenBankOperationModel::class,
        ]);
    }
}