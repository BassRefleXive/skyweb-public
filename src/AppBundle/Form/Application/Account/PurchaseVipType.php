<?php


namespace AppBundle\Form\Application\Account;

use AppBundle\Model\Application\Account\PurchaseVipModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class PurchaseVipType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dailyRate', HiddenType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank(),
                ],
            ])
            ->add('days', ChoiceType::class, [
                'required' => true,
                'choices' => array_flip(range(1, 30)),
                'label' => 'vip.purchase.0',
                'choice_translation_domain' => false,
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PurchaseVipModel::class,
        ]);
    }
}