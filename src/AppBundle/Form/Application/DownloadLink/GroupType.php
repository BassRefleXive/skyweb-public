<?php

namespace AppBundle\Form\Application\DownloadLink;

use AppBundle\Entity\Application\DownloadLink\Group;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class GroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 3,
                        'max' => 250,
                    ]),
                ],
            ])
            ->add('links', CollectionType::class, [
                'attr'         => [
                    'style' => 'display: none;',
                ],
                'required'     => false,
                'entry_type'   => LinkType::class,
                'allow_add'    => true,
                'prototype'    => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label'        => false,
            ])
            ->add('size', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 65535 MB',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 65535,
                    ]),
                ],
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Group::class,
        ]);
    }
}