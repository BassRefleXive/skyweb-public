<?php

declare(strict_types = 1);

namespace AppBundle\Form\Application\Admin\UserManager\Funds;


use AppBundle\Model\Application\Admin\UserManager\Funds\ChangeZenBankModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class ChangeZenBankType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('account', HiddenType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                ],
                'label'       => 'Account',
            ])
            ->add('amount', IntegerType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 10000000,
                    ]),
                ],
                'label'       => 'kk Zen',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ChangeZenBankModel::class,
        ]);
    }
}