<?php

declare(strict_types = 1);

namespace AppBundle\Form\Application\Admin\UserManager\Funds;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

abstract class ChangeFundsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('account', HiddenType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 4,
                        'max' => 10,
                    ]),
                    new Assert\Regex([
                        'pattern' => '/^[a-z0-9]{4,10}$/',
                        'message' => 'user.login.pattern',
                    ]),
                ],
                'label'       => 'Account',
            ]);
    }
}