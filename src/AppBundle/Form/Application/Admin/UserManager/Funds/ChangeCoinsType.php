<?php

declare(strict_types = 1);

namespace AppBundle\Form\Application\Admin\UserManager\Funds;


use AppBundle\Model\Application\Admin\UserManager\Funds\ChangeCoinsModel;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class ChangeCoinsType extends ChangeFundsType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('wCoinC', IntegerType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 10000000,
                    ]),
                ],
                'label'       => 'WCoinC',
            ])
            ->add('wCoinP', HiddenType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 10000000,
                    ]),
                ],
                'label'       => 'WCoinP',
            ])
            ->add('wCoinG', HiddenType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 10000000,
                    ]),
                ],
                'label'       => 'WCoinG',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ChangeCoinsModel::class,
        ]);
    }
}