<?php

namespace AppBundle\Form\Application;


use AppBundle\Entity\Application\DataServerCredentials;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class DataServerCredentialsType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ip', TextType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 3,
                        'max' => 255,
                    ]),
                ],
            ])
            ->add('port', IntegerType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 99999,
                    ]),
                ],
            ])
            ->add('timeout', IntegerType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 99999,
                    ]),
                ],
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DataServerCredentials::class,
        ]);
    }
}