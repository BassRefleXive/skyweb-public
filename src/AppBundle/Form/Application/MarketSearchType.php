<?php

namespace AppBundle\Form\Application;

use AppBundle\Doctrine\Type\MarketPriceTypeEnum;
use AppBundle\Entity\Application\MarketCategory;
use AppBundle\Filter\Interfaces\PaginatedFilterInterface;
use AppBundle\Filter\MarketFilter;
use AppBundle\Service\Application\PartialTranslatorService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class MarketSearchType extends AbstractType
{
    private $partialTranslatorService;

    public function __construct(PartialTranslatorService $partialTranslatorService)
    {
        $this->partialTranslatorService = $partialTranslatorService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $levelOptions = [];
        $optionOptions = [];

        for ($i = 1; $i <= 15; $i++) {
            $levelOptions[$i] = '+' . $i;
        }
        for ($i = 1; $i <= 7; $i++) {
            $optionOptions[$i] = '+' . $i * 4;
        }

        $builder
            ->add('level', ChoiceType::class, [
                'required' => false,
                'choices' => array_flip($levelOptions),
                'label' => 'market.search_form.2',
                'choice_translation_domain' => false,
            ])
            ->add('option', ChoiceType::class, [
                'required' => false,
                'choices' => array_flip($optionOptions),
                'label' => 'market.search_form.3',
                'choice_translation_domain' => false,
            ])
            ->add('skill', CheckboxType::class, [
                'required' => false,
                'constraints' => [
                    new Assert\Type([
                        'type' => 'bool',
                    ]),
                ],
                'label' => 'market.search_form.10',
            ])
            ->add('luck', CheckboxType::class, [
                'required' => false,
                'constraints' => [
                    new Assert\Type([
                        'type' => 'bool',
                    ]),
                ],
                'label' => 'market.search_form.11',
            ])
            ->add('excellent', CheckboxType::class, [
                'required' => false,
                'constraints' => [
                    new Assert\Type([
                        'type' => 'bool',
                    ]),
                ],
                'label' => 'market.search_form.6',
            ])
            ->add('ancient', CheckboxType::class, [
                'required' => false,
                'constraints' => [
                    new Assert\Type([
                        'type' => 'bool',
                    ]),
                ],
                'label' => 'market.search_form.7',
            ])
            ->add('harmony', CheckboxType::class, [
                'required' => false,
                'constraints' => [
                    new Assert\Type([
                        'type' => 'bool',
                    ]),
                ],
                'label' => 'market.search_form.8',
            ])
            ->add('pvp', CheckboxType::class, [
                'required' => false,
                'constraints' => [
                    new Assert\Type([
                        'type' => 'bool',
                    ]),
                ],
                'label' => 'market.search_form.9',
            ])
            ->add('price', IntegerType::class, [
                'required' => false,
                'constraints' => [
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                ],
                'label' => 'market.search_form.4',
            ])
            ->add('priceType', ChoiceType::class, [
                'required' => false,
                'choices' => array_flip(MarketPriceTypeEnum::getTypes()),
                'label' => 'market.search_form.5',
                'choice_translation_domain' => false,
            ])
            ->add('category', EntityType::class, [
                'required' => false,
                'class' => MarketCategory::class,
                'choice_label' => function (MarketCategory $marketCategory): string {
                    return $this->partialTranslatorService->translateMarketCategory($marketCategory->getName());
                },
                'multiple' => false,
                'label' => 'market.search_form.1',
            ])
            ->add('title', TextType::class, [
                'required'    => false,
                'constraints' => [
                    new Assert\Length([
                        'min' => 3,
                        'max' => 20,
                    ]),
                ],
                'label' => 'market.search_form.12',
            ])
            ->add('owned', CheckboxType::class, [
                'required' => false,
                'constraints' => [
                    new Assert\Type([
                        'type' => 'bool',
                    ]),
                ],
                'label' => 'market.search_form.13',
            ])
            ->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
                /** @var MarketFilter $marketFilter */
                $marketFilter = $event->getData();
                $marketFilter = $marketFilter->setPage(PaginatedFilterInterface::DEFAULT_PAGE);
                $event->setData($marketFilter);
            });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MarketFilter::class,
        ]);
    }
}