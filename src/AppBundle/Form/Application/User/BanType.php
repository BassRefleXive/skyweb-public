<?php

declare(strict_types = 1);

namespace AppBundle\Form\Application\User;


use AppBundle\Entity\Application\User\Ban;
use AppBundle\Entity\Application\User\Moderator;
use AppBundle\Entity\Application\User\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints as Assert;

class BanType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', EntityType::class, [
                'required' => true,
                'class' => User::class,
                'choice_label' => 'getDisplayName',
                'mapped' => false,
            ])
            ->add('bannedBy', EntityType::class, [
                'required' => true,
                'class' => Moderator::class,
                'choice_label' => 'getDisplayName',
                'mapped' => false,
            ])
            ->add('reason', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 10,
                        'max' => 255,
                    ]),
                ],
            ])
            ->add('link', TextType::class, [
                'required' => false,
                'constraints' => [
                    new Assert\Length([
                        'min' => 10,
                        'max' => 255,
                    ]),
                ],
            ])
            ->add('expireAt', DateTimeType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => '+ 1 hours',
                    ]),
                ],
                'data' => new \DateTime('+ 6 hours'),
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ban::class,
        ]);
    }
}