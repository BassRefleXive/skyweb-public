<?php

namespace AppBundle\Form\Application\User;


use AppBundle\Entity\Application\User\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints as Assert;

class LoginType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 4,
                        'max' => 10,
                    ]),
                    new Assert\Regex([
                        'pattern' => '/^[a-z0-9]{4,10}$/',
                        'message' => 'user.login.pattern',
                    ]),
                ],
                'label'       => 'users.register.2',
                'attr'        => [
                    'placeholder' => 'users.register.2',
                ],
            ])
            ->add('password', PasswordType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 4,
                        'max' => 10,
                    ]),
                    new Assert\Regex([
                        'pattern' => '/^[a-z0-9]{4,10}$/',
                    ]),
                ],
                'label'       => 'users.register.4',
                'attr'        => [
                    'placeholder' => 'users.register.2',
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}