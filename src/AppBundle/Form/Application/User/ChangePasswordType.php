<?php


namespace AppBundle\Form\Application\User;

use AppBundle\Model\Application\User\ChangePasswordModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints as Assert;

class ChangePasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('old', PasswordType::class, [
                'constraints'    => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 4,
                        'max' => 10,
                    ]),
                    new Assert\Regex([
                        'pattern' => '/^[a-zA-Z0-9]{4,10}$/',
                    ]),
                ],
                'label' => 'users.change_password.1',
            ])
            ->add('new', RepeatedType::class, [
                'constraints'    => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 4,
                        'max' => 10,
                    ]),
                    new Assert\Regex([
                        'pattern' => '/^[a-zA-Z0-9]{4,10}$/',
                    ]),
                ],
                'type'           => PasswordType::class,
                'first_options'  => ['label' => 'users.register.4'],
                'second_options' => ['label' => 'users.register.5'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ChangePasswordModel::class,
        ]);
    }
}