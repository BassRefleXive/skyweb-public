<?php

namespace AppBundle\Form\Application\User;


use AppBundle\Entity\Application\User\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints as Assert;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label'       => 'users.register.3',
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Email(),
                ],
            ])
            ->add('username', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 4,
                        'max' => 10,
                    ]),
                    new Assert\Regex([
                        'pattern' => '/^[a-z0-9]{4,10}$/',
                        'message' => 'user.login.pattern',
                    ]),
                ],
                'label'       => 'users.register.2',
            ])
            ->add('displayName', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 3,
                        'max' => 10,
                    ]),
                ],
                'label'       => 'users.register.1',
            ])
            ->add('plainPassword', RepeatedType::class, [
                'constraints'    => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 4,
                        'max' => 10,
                    ]),
                    new Assert\Regex([
                        'pattern' => '/^[a-zA-Z0-9]{4,10}$/',
                        'message' => 'user.password.pattern',
                    ]),
                ],
                'type'           => PasswordType::class,
                'first_options'  => ['label' => 'users.register.4'],
                'second_options' => ['label' => 'users.register.5'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}