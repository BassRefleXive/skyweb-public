<?php


namespace AppBundle\Form\Application\User;

use AppBundle\Model\Application\RequestResetPasswordModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class RequestResetPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 3,
                        'max' => 10,
                    ]),
                    new Assert\Regex([
                        'pattern' => '/^[a-z0-9]{4,10}$/',
                    ]),
                ],
                'label'       => 'users.reset_password.0',
            ])
            ->add('email', EmailType::class, [
                'label' => 'users.reset_password.1',
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Email(),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'      => RequestResetPasswordModel::class,
            'csrf_protection' => true,
        ]);
    }
}