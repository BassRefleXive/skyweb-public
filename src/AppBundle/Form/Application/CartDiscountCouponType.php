<?php

namespace AppBundle\Form\Application;


use AppBundle\Doctrine\Type\CartDiscountCouponTypeEnum;
use AppBundle\Entity\Application\CartDiscountCoupon;
use AppBundle\Repository\Application\CartDiscountCouponRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class CartDiscountCouponType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('discount', IntegerType::class, [
                'required'    => true,
                'attr' => [
                    'min' => 1,
                    'max' => 100,
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 1,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 100,
                    ]),
                ],
            ])
            ->add('status', ChoiceType::class, [
                'required'    => true,
                'choices'     => [
                    'Disabled' => CartDiscountCouponRepository::STATUS_DISABLED,
                    'Enabled'  => CartDiscountCouponRepository::STATUS_ENABLED,
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                ],
            ])
            ->add('type', ChoiceType::class, [
                'required'    => true,
                'choices'     => array_flip(CartDiscountCouponTypeEnum::getTypes()),
                'constraints' => [
                    new Assert\NotBlank(),
                ],
            ])
            ->add('expireAt', DateTimeType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => '+ 24 hours',
                    ]),
                ],
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CartDiscountCoupon::class,
        ]);
    }

}