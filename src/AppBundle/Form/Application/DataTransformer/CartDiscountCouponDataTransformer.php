<?php

namespace AppBundle\Form\Application\DataTransformer;

use AppBundle\Entity\Application\CartDiscountCoupon;
use AppBundle\Repository\Application\CartDiscountCouponRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class CartDiscountCouponDataTransformer implements DataTransformerInterface
{
    protected $cartDiscountCouponRepo;

    public function __construct(CartDiscountCouponRepository $cartDiscountCouponRepo)
    {
        $this->cartDiscountCouponRepo = $cartDiscountCouponRepo;
    }

    public function transform($entity)
    {
        if (null === $entity || !$entity instanceof CartDiscountCoupon) {
            return null;
        }

        return $entity->getCoupon();
    }

    public function reverseTransform($coupon)
    {
        if (!$coupon) {
            return null;
        }

        $entity = $this->cartDiscountCouponRepo->findByText($coupon);

        if (null === $entity) {
            throw new TransformationFailedException(sprintf(
                'A CartDiscountCoupon "%s" does not exist!',
                $coupon
            ));
        }

        return $entity;
    }
}