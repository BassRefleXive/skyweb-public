<?php

namespace AppBundle\Form\Application\DataTransformer;


use AppBundle\Entity\MuOnline\Character;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class CharacterDataTransformer implements DataTransformerInterface
{

    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function transform($entity)
    {
        if (null === $entity || !$entity instanceof Character) {
            return null;
        }

        return $entity->getName();
    }

    public function reverseTransform($name)
    {
        if (!$name) {
            return null;
        }

        $entity = $this->em->find(Character::class, $name);

        if (null === $entity) {
            throw new TransformationFailedException(sprintf(
                'A Character "%s" does not exist!',
                $name
            ));
        }

        return $entity;
    }

}