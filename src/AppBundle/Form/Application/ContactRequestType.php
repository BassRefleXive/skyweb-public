<?php


namespace AppBundle\Form\Application;

use AppBundle\Model\Application\ContactRequestModel;
use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue as RecaptchaValid;

class ContactRequestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'contact.request.0',
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Email(),
                ],
            ])
            ->add('subject', TextType::class, [
                'label' => 'contact.request.1',
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min'        => 3,
                        'max'        => 50,
                    ])
                ],
            ])
            ->add('text', TextareaType::class, [
                'label' => 'contact.request.2',
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min'        => 3,
                        'max'        => 1000,
                    ])
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'      => ContactRequestModel::class,
            'csrf_protection' => false,
        ]);
    }
}