<?php

namespace AppBundle\Form\Application;

use AppBundle\Entity\Application\MarketItem;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints as Assert;

class MarketBuyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('item', EntityType::class, [
                'required'     => true,
                'class'        => MarketItem::class,
                'choice_label' => 'getItemInfo.name',
            ])
            ->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {

                /** @var MarketItem $item */
                $item = $event->getForm()->get('item')->getData();

                $expectedCommission = 0;

                if ($item) {
                    $commissionPercent = $item->getServer()->getMarketConfig()->getCommission();
                    $expectedCommission = ceil(($item->getPrice() * (100 + $commissionPercent)) / 100);
                }

                $event->getForm()
                      ->add('price', HiddenType::class, [
                          'required'    => true,
                          'constraints' => [
                              new Assert\NotBlank(),
                              new Assert\EqualTo([
                                  'value' => $expectedCommission,
                              ]),
                          ],
                          'data'        => $expectedCommission,
                      ]);
            });
    }
}