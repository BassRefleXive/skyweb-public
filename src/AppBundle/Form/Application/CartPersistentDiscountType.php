<?php

namespace AppBundle\Form\Application;


use AppBundle\Doctrine\Type\CartPersistentDiscountTypeEnum;
use AppBundle\Entity\Application\CartPersistentDiscount;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class CartPersistentDiscountType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('discount', IntegerType::class, [
                'required'    => true,
                'attr' => [
                    'min' => 0,
                    'max' => 100,
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 100,
                    ]),
                ],
            ])
            ->add('spentFrom', IntegerType::class, [
                'required'    => true,
                'attr' => [
                    'min' => 0,
                    'max' => 1000000,
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 1000000,
                    ]),
                ],
            ])
            ->add('spentTo', IntegerType::class, [
                'required'    => true,
                'attr' => [
                    'min' => 0,
                    'max' => 1000000,
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 1000000,
                    ]),
                ],
            ])
            ->add('type', ChoiceType::class, [
                'required'    => true,
                'choices'     => array_flip(CartPersistentDiscountTypeEnum::getTypes()),
                'constraints' => [
                    new Assert\NotBlank(),
                ],
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CartPersistentDiscount::class,
        ]);
    }

}