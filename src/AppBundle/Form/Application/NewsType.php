<?php

namespace AppBundle\Form\Application;


use AppBundle\Entity\Application\Server;
use AppBundle\Repository\Application\NewsRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class NewsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titleRus', TextType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 1,
                        'max' => 100,
                    ]),
                ],
            ])
            ->add('shortTextRus', TextareaType::class, [
                'required'    => true,
                'attr'        => [
                    'class'      => 'tinymce',
                    'data-theme' => 'bbcode',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                ],
            ])
            ->add('fullTextRus', TextareaType::class, [
                'required'    => true,
                'attr'        => [
                    'class'      => 'tinymce',
                    'data-theme' => 'bbcode',
                ],
            ])
            ->add('titleEng', TextType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 1,
                        'max' => 100,
                    ]),
                ],
            ])
            ->add('shortTextEng', TextareaType::class, [
                'required'    => true,
                'attr'        => [
                    'class'      => 'tinymce',
                    'data-theme' => 'bbcode',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                ],
            ])
            ->add('fullTextEng', TextareaType::class, [
                'required'    => true,
                'attr'        => [
                    'class'      => 'tinymce',
                    'data-theme' => 'bbcode',
                ],
            ])
            ->add('status', ChoiceType::class, [
                'required'    => true,
                'choices'     => [
                    'Disabled' => NewsRepository::STATUS_DISABLED,
                    'Enabled'  => NewsRepository::STATUS_ENABLED,
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                ],
            ])
            ->add('server', EntityType::class, [
                'required'     => false,
                'class'        => Server::class,
                'choice_label' => 'name',
                'placeholder'  => 'Any server',
            ]);
    }

}