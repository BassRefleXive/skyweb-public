<?php

namespace AppBundle\Form\Application;


use AppBundle\Entity\Application\Items\ItemInfo;
use AppBundle\Entity\Application\MarketCategory;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class MarketCategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 3,
                        'max' => 50,
                    ]),
                ],
            ])
            ->add('items', EntityType::class, [
                'attr'          => [
                    'style' => 'height: 500px;',
                ],
                'required'      => true,
                'class'         => ItemInfo::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('i')
                              ->addOrderBy('i.group', 'ASC')
                              ->addOrderBy('i.itemId', 'ASC');
                },
                'choice_label'  => 'name',
                'group_by'      => function (ItemInfo $item, $key, $index) {
                    return $item->getGroup()->getName();
                },
                'multiple'      => true,
                'label'         => false,
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MarketCategory::class,
        ]);
    }

}