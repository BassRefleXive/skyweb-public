<?php

namespace AppBundle\Form\Application;


use AppBundle\Doctrine\Type\MarketPriceTypeEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class MarketSellType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('price', IntegerType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                ],
                'label' => 'market.sell.3',
            ])
            ->add('priceType', ChoiceType::class, [
                'required'                  => true,
                'choices'                   => array_flip(MarketPriceTypeEnum::getTypes()),
                'constraints'               => [
                    new Assert\NotBlank(),
                ],
                'label' => 'market.sell.4',
                'choice_translation_domain' => false,
            ]);
    }
}