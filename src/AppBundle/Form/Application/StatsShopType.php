<?php

namespace AppBundle\Form\Application;

use AppBundle\Model\Application\StatsShopModel;
use AppBundle\Service\Application\ServerService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Constraints as Assert;

class StatsShopType extends AbstractType
{
    protected $serverService;
    protected $translator;

    public function __construct(ServerService $serverService, Translator $translator)
    {
        $this->serverService = $serverService;
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $statsShopConfig = $this->serverService->getCurrentServer()->getStatsShopConfig();

        $choices = [];

        for ($i = 1; $i <= 15; $i++) {
            $choices[$i] = $this->translator->trans('stats_shop.index.3', [
                'COUNT' => $statsShopConfig->getStep() * $i,
                'WCOIN' => $statsShopConfig->getStepPriceCredits() * $i,
                'MONEY' => $statsShopConfig->getStepPriceMoney() * $i,
            ]);
        }

        $builder
            ->add('step', ChoiceType::class, [
                'required'                  => true,
                'choices'                   => array_flip($choices),
                'constraints'               => [
                    new Assert\NotBlank(),
                ],
                'choice_translation_domain' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => StatsShopModel::class,
        ]);
    }
}