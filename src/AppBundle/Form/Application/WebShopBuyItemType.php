<?php

namespace AppBundle\Form\Application;


use AppBundle\Entity\Application\Items\AncientSetItem;
use AppBundle\Entity\Application\Items\GroupHarmonyOption;
use AppBundle\Entity\Application\Items\ItemInfo;
use AppBundle\Entity\Application\Items\PvPOption;
use AppBundle\Entity\Application\WebShopCategory;
use AppBundle\Entity\Application\WebShopCategoryItem;
use AppBundle\Entity\Application\WebShopConfig;
use AppBundle\Validator\Constraints\WebShopPrice;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Constraints as Assert;

class WebShopBuyItemType extends AbstractType
{
    protected $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('item', EntityType::class, [
                'required'     => false,
                'class'        => WebShopCategoryItem::class,
                'mapped'       => false,
                'choice_label' => 'getItem.name',
            ]);


        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $formData = $event->getData();

            $item = $formData['item'] ?? null;

            /** @var WebShopCategoryItem $item */
            if ($item) {
                /** @var WebShopCategory $category */
                $category = $item->getWebShopCategory();
                /** @var WebShopConfig $config */
                $config = $category->getConfig();
                /** @var ItemInfo $itemInfo */
                $itemInfo = $item->getItem();


                $event->getForm()->add('priceCredits', IntegerType::class, [
                    'required'    => true,
                    'attr'        => [
                        'style' => 'display: none;',
                    ],
                    'constraints' => [
                        new Assert\NotBlank(),
                        new Assert\GreaterThanOrEqual([
                            'value' => ceil($item->getPriceCredits() * (100 - $config->globalCreditsDiscountPercent())) / 100,
                        ]),
                        new Assert\Type([
                            'type' => 'integer',
                        ]),
                        new WebShopPrice([
                            'config'     => $config,
                            'type'       => WebShopPrice::PRICE_CREDITS,
                            'base_price' => (float)$item->getPriceCredits(),
                        ]),
                    ],
                    'label'       => false,
                    'data'        => $item->getPriceCredits(),
                ]);

                $event->getForm()->add('priceMoney', NumberType::class, [
                    'required'    => true,
                    'attr'        => [
                        'style' => 'display: none;',
                    ],
                    'constraints' => [
                        new Assert\NotBlank(),
                        new Assert\GreaterThanOrEqual([
                            'value' => ceil($item->getPriceMoney() * (100 - $config->globalMoneyDiscountPercent())) / 100,
                        ]),
                        new Assert\Type([
                            'type' => 'float',
                        ]),
                        new WebShopPrice([
                            'config'     => $config,
                            'type'       => WebShopPrice::PRICE_MONEY,
                            'base_price' => (float)$item->getPriceMoney(),
                        ]),
                    ],
                    'label'       => false,
                    'data'        => $item->getPriceMoney(),
                ]);

                /**
                 * Add level selection
                 */
                if ($config->getMaxItemLevel() > 0 && $item->isAddLevelAllowed()) {
                    $options = [];

                    for ($i = 1; $i <= $config->getMaxItemLevel(); $i++) {
                        $options[$i] = '+' . $i;
                    }

                    count($options) > 0 && $event->getForm()
                                                 ->add('level', ChoiceType::class, [
                                                     'required'                  => false,
                                                     'choices'                   => array_flip($options),
                                                     'label'                     => 'web_shop.buy.4',
                                                     'choice_translation_domain' => false,
                                                 ]);
                }

                /**
                 * Add option selection
                 */
                if ($config->getMaxItemOption() > 0 && $item->isAddOptionAllowed()) {
                    $options = [];

                    for ($i = 1; $i <= $config->getMaxItemOption(); $i++) {
                        $options[$i * 4] = '+' . $i * 4;
                    }

                    count($options) > 0 && $event->getForm()
                                                 ->add('option', ChoiceType::class, [
                                                     'required'                  => false,
                                                     'choices'                   => array_flip($options),
                                                     'label'                     => 'web_shop.buy.5',
                                                     'choice_translation_domain' => false,
                                                 ]);
                }

                /**
                 * Add skill selection
                 */
                if ($item->isAddSkillAllowed()) {
                    $event->getForm()
                          ->add('skill', CheckboxType::class, [
                              'required'           => false,
                              'constraints'        => [
                                  new Assert\Type([
                                      'type' => 'bool',
                                  ]),
                              ],
                              'label'              => $itemInfo->getSkill()
                                  ? $this->translator->trans('web_shop.buy.7', ['SKILL' => $itemInfo->getSkill()])
                                  : $this->translator->trans('web_shop.buy.6'),
                              'translation_domain' => false,
                          ]);
                }

                /**
                 * Add luck selection
                 */
                if ($item->isAddLuckAllowed()) {
                    $event->getForm()
                          ->add('luck', CheckboxType::class, [
                              'required'    => false,
                              'constraints' => [
                                  new Assert\Type([
                                      'type' => 'bool',
                                  ]),
                              ],
                              'label'       => 'web_shop.buy.8',
                          ]);
                }

                /**
                 * Add Excellent options selection
                 */
                if ($config->getMaxExcCount() > 0 && $item->isAddExcellentAllowed()) {
                    $event->getForm()
                          ->add('excellent', ChoiceType::class, [
                              'required'                  => false,
                              'choices'                   => array_flip($itemInfo->getAvailableExcellentOptions()),
                              'multiple'                  => true,
                              'expanded'                  => true,
                              'label'                     => 'web_shop.buy.9',
                              'choice_translation_domain' => false,
                          ]);
                }

                /**
                 * Add Ancient options selection
                 */
                if ($item->isAddAncientAllowed()) {
                    $options = [];

                    /** @var AncientSetItem $ancientSetItem */
                    foreach ($itemInfo->getAncientSets() as $ancientSetItem) {
                        $options[$ancientSetItem->getAncientSet()->getId()] = $ancientSetItem->getAncientSet()->getName();
                    }

                    count($options) > 0 && $event->getForm()
                                                 ->add('ancient', ChoiceType::class, [
                                                     'required'                  => false,
                                                     'choices'                   => array_flip($options),
                                                     'label'                     => 'web_shop.buy.10',
                                                     'choice_translation_domain' => false,
                                                 ]);
                }

                /**
                 * Add Harmony options selection
                 */
                if ($item->isAddHarmonyAllowed()) {
                    $options = [];

                    /** @var GroupHarmonyOption $groupHarmonyOption */
                    foreach ($itemInfo->getGroup()->getHarmonyOptions() as $groupHarmonyOption) {
                        $harmonyOption = $groupHarmonyOption->getHarmonyOption();
                        $options[$harmonyOption->getId()] = $harmonyOption->getName();
                    }

                    count($options) > 0 && $event->getForm()
                                                 ->add('harmony', ChoiceType::class, [
                                                     'required'                  => false,
                                                     'choices'                   => array_flip($options),
                                                     'label'                     => 'web_shop.buy.11',
                                                     'choice_translation_domain' => false,
                                                 ]);

                }

                /**
                 * Add PvP options selection
                 * @var PvPOption $pvpOption
                 */
                if ($item->isAddPvPAllowed() && ($pvpOption = $itemInfo->getPvpOption())) {
                    $event->getForm()
                          ->add('pvp', CheckboxType::class, [
                              'required'           => false,
                              'constraints'        => [
                                  new Assert\Type([
                                      'type' => 'bool',
                                  ]),
                              ],
                              'label'              => implode(' & ', $pvpOption->getOptions()),
                              'translation_domain' => false,
                          ]);

                }

            }

        });

    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);

        $view->children['priceCredits']->vars['value'] = $form->getRoot()->get('priceCredits')->getConfig()->getOption('data');
        $view->children['priceMoney']->vars['value'] = $form->getRoot()->get('priceMoney')->getConfig()->getOption('data');
    }
}