<?php

namespace AppBundle\Form\Application;

use AppBundle\Model\Application\LocaleModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class SetLocaleType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('locale', ChoiceType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                ],
                'choices'     => [
                    'English' => 'en',
                    'Русский' => 'ru',
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'      => LocaleModel::class,
            'csrf_protection' => false,
        ]);
    }
}