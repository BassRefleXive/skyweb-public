<?php

namespace AppBundle\Form\Application;

use AppBundle\Entity\Application\ReferralConfig;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class ReferralConfigType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('requiredReset', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 65535',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 65535,
                    ]),
                ],
            ])
            ->add('requiredGrandReset', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 65535',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 65535,
                    ]),
                ],
            ])
            ->add('requiredOnlineTime', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 65535',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 65535,
                    ]),
                ],
            ])
            ->add('requiredVotes', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 65535',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 65535,
                    ]),
                ],
            ])
            ->add('reward', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 65535',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 65535,
                    ]),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ReferralConfig::class,
        ]);
    }
}