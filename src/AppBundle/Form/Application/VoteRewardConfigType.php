<?php


namespace AppBundle\Form\Application;


use AppBundle\Entity\Application\VoteRewardConfig;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class VoteRewardConfigType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rewardMonthTop1', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 65535',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 65535,
                    ]),
                ],
            ])
            ->add('rewardMonthTop2', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 65535',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 65535,
                    ]),
                ],
            ])
            ->add('rewardMonthTop3', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 65535',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 65535,
                    ]),
                ],
            ])
            ->add('mmotopRewardRegularVote', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 65535',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 65535,
                    ]),
                ],
            ])
            ->add('mmotopRewardSmsVote', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 65535',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 65535,
                    ]),
                ],
            ])
            ->add('mmotopVotesFileUrl', TextType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'https://mmotop.ru/votes/XXX.txt',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 3,
                        'max' => 150,
                    ]),
                ],
            ])
            ->add('qtopRewardRegularVote', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 65535',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 65535,
                    ]),
                ],
            ])
            ->add('qtopRewardSmsVote', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 65535',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 65535,
                    ]),
                ],
            ])
            ->add('qtopVotesFileUrl', TextType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'http://q-top.ru/profiles/XXXX/XXXX/XXXXXXXXXX.txt',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 3,
                        'max' => 150,
                    ]),
                ],
            ])
            ->add('rewardXtremeTop100Vote', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 65535',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 65535,
                    ]),
                ],
            ])
            ->add('rewardGTop100Vote', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 65535',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 65535,
                    ]),
                ],
            ])
            ->add('rewardTopServers200Vote', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 65535',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 65535,
                    ]),
                ],
            ])
            ->add('topServers200SiteId', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Site id',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                ],
            ])
            ->add('rewardTopGVote', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 65535',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => VoteRewardConfig::class,
        ]);
    }
}