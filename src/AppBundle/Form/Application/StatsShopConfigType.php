<?php

namespace AppBundle\Form\Application;

use AppBundle\Entity\Application\StatsShopConfig;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class StatsShopConfigType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('step', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 65535',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 65535,
                    ]),
                ],
            ])
            ->add('stepPriceCredits', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 100000',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 100000,
                    ]),
                ],
            ])
            ->add('stepPriceMoney', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 100000',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 100000,
                    ]),
                ],
            ])
            ->add('enabled', CheckboxType::class, [
                'required'           => false,
                'constraints'        => [
                    new Assert\Type([
                        'type' => 'bool',
                    ]),
                ],
                'label'              => 'Enabled',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => StatsShopConfig::class,
        ]);
    }
}