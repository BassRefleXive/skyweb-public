<?php

namespace AppBundle\Form\Application;


use AppBundle\Entity\Application\DatabaseCredential;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class DatabaseCredentialType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('host', TextType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 3,
                        'max' => 255,
                    ]),
                ],
            ])
            ->add('databaseName', TextType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 3,
                        'max' => 255,
                    ]),
                ],
            ])
            ->add('port', IntegerType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 99999,
                    ]),
                ],
            ])
            ->add('user', TextType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 2,
                        'max' => 255,
                    ]),
                ],
            ])
            ->add('password', TextType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 3,
                        'max' => 255,
                    ]),
                ],
            ])
            ->add('charset', TextType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 3,
                        'max' => 255,
                    ]),
                ],
                'data'  => DatabaseCredential::CHARSET_UTF_8,
            ])
            ->add('driver', TextType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 3,
                        'max' => 255,
                    ]),
                ],
                'data'  => DatabaseCredential::DRIVER_MSSQL,
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DatabaseCredential::class,
        ]);
    }
}