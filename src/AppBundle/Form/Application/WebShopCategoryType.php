<?php

namespace AppBundle\Form\Application;


use AppBundle\Entity\Application\WebShopCategory;
use AppBundle\Repository\Application\WebShopCategoryRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints as Assert;

class WebShopCategoryType extends AbstractType
{
    protected $webShopCategoryRepository;

    public function __construct(WebShopCategoryRepository $webShopCategoryRepository)
    {
        $this->webShopCategoryRepository = $webShopCategoryRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 3,
                        'max' => 50,
                    ]),
                ],
            ])
            ->add('status', ChoiceType::class, [
                'required' => true,
                'choices' => [
                    'Disabled' => WebShopCategoryRepository::STATUS_DISABLED,
                    'Enabled' => WebShopCategoryRepository::STATUS_ENABLED,
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                ],
            ])
            ->add('items', CollectionType::class, [
                'attr' => [
                    'style' => 'display: none;',
                ],
                'required' => false,
                'entry_type' => WebShopCategoryItemType::class,
                'allow_add' => true,
                'prototype' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => false,
            ])
            ->add('position', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 65535',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 65535,
                    ]),
                ],
            ]);


        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /**
             * @var WebShopCategory $category
             */
            $category = $event->getData();

            $event->getForm()
                ->add('parentCategory', EntityType::class, [
                    'required' => false,
                    'class' => WebShopCategory::class,
                    'choice_label' => 'name',
                    'property_path' => 'parentCategory',
                    'choices' => $this->webShopCategoryRepository->findPossibleParentCategories(
                        $category->getConfig()->getServer(),
                        $category->isNew()
                            ? null
                            : $category
                    ),
                ]);
        });

    }

}