<?php

namespace AppBundle\Form\Application;


use AppBundle\Entity\Application\User\User;
use AppBundle\Entity\MuOnline\Character;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Exception\Application\NeedAccountException;
use AppBundle\Exception\Application\RequireLoginException;
use AppBundle\Form\Application\DataTransformer\CharacterDataTransformer;
use AppBundle\Model\Application\AddShopStatsModel;
use AppBundle\Service\Application\ServerService;
use AppBundle\Service\MuOnline\AccountService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraints as Assert;

class AddShopStatsType extends AbstractType
{
    protected $tokenStorage;
    protected $accountService;
    protected $serverService;

    public function __construct(TokenStorageInterface $tokenStorage, AccountService $accountService, ServerService $serverService)
    {
        $this->tokenStorage = $tokenStorage;
        $this->accountService = $accountService;
        $this->serverService = $serverService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $server = $this->serverService->getCurrentServer();

        /** @var User|null $user */
        $user = $this->tokenStorage->getToken()->getUser();

        if (!$user) {
            throw new RequireLoginException('errors.2');
        }

        $userServerAccount = $user->getUserServerAccountByServer($server);

        if (!$userServerAccount) {
            throw new NeedAccountException('errors.3');
        }

        $account = $this->accountService->getAccountByUserServerAccount($userServerAccount);

        if (!$account) {
            throw new DisplayableException('errors.13');
        }

        $account->getCharacters()
                ->map(function (Character $character) use (&$charactersChoice) {
                    $charactersChoice[$character->getName()] = $character->getName();
                });


        $builder
            ->add('character', ChoiceType::class, [
                'required'    => true,
                'choices'     => $charactersChoice,
                'constraints' => [
                    new Assert\NotBlank(),
                ],
                'label'       => 'add_shop_stats.2',
            ])
            ->add('stats', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'max' => $userServerAccount->getPurchasedStats(),
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual(
                        [
                            'value' => 0,
                        ]
                    ),
                    new Assert\LessThanOrEqual(
                        [
                            'value' => $userServerAccount->getPurchasedStats(),
                        ]
                    ),
                ],
                'data'        => $userServerAccount->getPurchasedStats(),
                'label'       => 'add_shop_stats.3',
            ]);

        $builder->get('character')->addModelTransformer(
            new CharacterDataTransformer($this->serverService->getCurrentServerDataBaseManager())
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AddShopStatsModel::class,
        ]);
    }

}