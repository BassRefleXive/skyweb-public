<?php

namespace AppBundle\Form\Application;


use AppBundle\Doctrine\Type\CartPurchaseTypeEnum;
use AppBundle\Entity\Application\User\User;
use AppBundle\Exception\Application\NeedAccountException;
use AppBundle\Exception\Application\RequireLoginException;
use AppBundle\Form\Application\DataTransformer\CartDiscountCouponDataTransformer;
use AppBundle\Repository\Application\CartDiscountCouponRepository;
use AppBundle\Service\Application\AccountService;
use AppBundle\Service\Application\ServerService;
use AppBundle\Validator\Constraints\CartPrice;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class BuyCartType extends AbstractType
{
    private $cartDiscountCouponRepo;
    protected $tokenStorage;
    private $accountService;
    private $serverService;

    public function __construct(
        CartDiscountCouponRepository $cartDiscountCouponRepo,
        TokenStorageInterface $tokenStorage,
        AccountService $accountService,
        ServerService $serverService
    )
    {
        $this->cartDiscountCouponRepo = $cartDiscountCouponRepo;
        $this->tokenStorage = $tokenStorage;
        $this->accountService = $accountService;
        $this->serverService = $serverService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('coupon', TextType::class, [
                'required' => false,
                'label' => 'cart.9',
            ])
            ->add('type', HiddenType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Choice([
                        'choices' => array_flip(CartPurchaseTypeEnum::getTypes()),
                    ]),
                ],
            ])
            ->add('priceCredits', HiddenType::class, [
                'required' => true,
            ])
            ->add('priceMoney', HiddenType::class, [
                'required' => true,
            ]);

        $builder->get('coupon')->addModelTransformer(
            new CartDiscountCouponDataTransformer($this->cartDiscountCouponRepo)
        );

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $server = $this->serverService->getCurrentServer();

            /** @var User|null $user */
            $user = $this->tokenStorage->getToken()->getUser();

            if (!$user) {
                throw new RequireLoginException('errors.2');
            }

            $userServerAccount = $user->getUserServerAccountByServer($server);

            if (!$userServerAccount) {
                throw new NeedAccountException('errors.3');
            }

            $formData = $event->getData();

            $priceCredits = $formData['priceCredits'];
            $priceMoney = $formData['priceMoney'];

            $event->getForm()->add('priceCredits', HiddenType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\LessThanOrEqual([
                        'value' => $priceCredits,
                    ]),
                    new CartPrice([
                        'type' => CartPrice::PRICE_CREDITS,
                        'base_price' => (float) $priceCredits,
                        'items' => $userServerAccount->getCart()->getItems(),
                    ]),
                ],
                'data' => $priceCredits,
            ]);

            $event->getForm()->add('priceMoney', HiddenType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\LessThanOrEqual([
                        'value' => $priceCredits,
                    ]),
                    new CartPrice([
                        'type' => CartPrice::PRICE_MONEY,
                        'base_price' => (float) $priceMoney,
                        'items' => $userServerAccount->getCart()->getItems(),
                    ]),
                ],
                'data' => $priceMoney,
            ]);

        });

    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);

        $view->children['priceCredits']->vars['value'] = $form->getRoot()->get('priceCredits')->getConfig()->getOption('data');
        $view->children['priceMoney']->vars['value'] = $form->getRoot()->get('priceMoney')->getConfig()->getOption('data');
    }
}