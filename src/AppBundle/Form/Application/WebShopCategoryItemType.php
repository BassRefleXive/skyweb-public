<?php

namespace AppBundle\Form\Application;


use AppBundle\Entity\Application\Items\ItemInfo;
use AppBundle\Entity\Application\WebShopCategoryItem;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class WebShopCategoryItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('item', EntityType::class, [
                'required'      => true,
                'class'         => ItemInfo::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('i')
                              ->addOrderBy('i.group', 'ASC')
                              ->addOrderBy('i.itemId', 'ASC');
                },
                'choice_label'  => 'name',
                'group_by'      => function (ItemInfo $item, $key, $index) {
                    return $item->getGroup()->getName();
                },
                'multiple'      => false,
                'label'         => false,
            ])
            ->add('options', ChoiceType::class, [
                'label'    => false,
                'required' => false,
                'mapped'   => false,
                'choices'  => [
                    'LVL'   => WebShopCategoryItem::ALLOWED_LEVEL,
                    'Opt'   => WebShopCategoryItem::ALLOWED_OPTION,
                    'Skill' => WebShopCategoryItem::ALLOWED_SKILL,
                    'Luck'  => WebShopCategoryItem::ALLOWED_LUCK,
                    'Exc'   => WebShopCategoryItem::ALLOWED_EXCELLENT,
                    'Anc'   => WebShopCategoryItem::ALLOWED_ANCIENT,
                    'Harm'  => WebShopCategoryItem::ALLOWED_HARMONY,
                    'PvP'   => WebShopCategoryItem::ALLOWED_PVP,
                ],
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('priceCredits', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Price WCoin',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                    new Assert\LessThan([
                        'value' => 100000,
                    ]),
                ],
                'label'       => false,
            ])
            ->add('priceMoney', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Price $',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                    new Assert\LessThan([
                        'value' => 100000,
                    ]),
                ],
                'label'       => false,
            ])
            ->add('position', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 65535',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 65535,
                    ]),
                ],
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => WebShopCategoryItem::class,
        ]);
    }
}