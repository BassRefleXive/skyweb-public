<?php

namespace AppBundle\Form\Application;

use AppBundle\Entity\Application\Server;
use AppBundle\Entity\Application\Timer;
use AppBundle\Enum\Application\TimerStatus;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class TimerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titleEng', TextType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 1,
                        'max' => 50,
                    ]),
                ],
            ])
            ->add('titleRus', TextType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 1,
                        'max' => 50,
                    ]),
                ],
            ])
            ->add('time', DateTimeType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => '+ 1 hours',
                    ]),
                ],
            ])
            ->add('status', ChoiceType::class, [
                'required'    => true,
                'choices'     => [
                    'Active' => TimerStatus::ACTIVE,
                    'Paused' => TimerStatus::PAUSED,
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                ],
            ])
            ->add('server', EntityType::class, [
                'required'     => false,
                'class'        => Server::class,
                'choice_label' => 'name',
                'placeholder'  => 'Any server',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Timer::class,
        ]);
    }
}