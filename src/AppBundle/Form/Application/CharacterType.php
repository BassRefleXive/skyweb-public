<?php

namespace AppBundle\Form\Application;


use AppBundle\Entity\Application\User\User;
use AppBundle\Exception\Application\NeedAccountException;
use AppBundle\Exception\Application\RequireLoginException;
use AppBundle\Form\Application\DataTransformer\CharacterDataTransformer;
use AppBundle\Service\Application\ServerService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraints as Assert;

class CharacterType extends AbstractType
{
    protected $tokenStorage;
    protected $serverService;

    public function __construct(TokenStorageInterface $tokenStorage, ServerService $serverService)
    {
        $this->tokenStorage = $tokenStorage;
        $this->serverService = $serverService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $server = $this->serverService->getCurrentServer();

        /** @var User|null $user */
        $user = $this->tokenStorage->getToken()->getUser();

        if (!$user) {
            throw new RequireLoginException('errors.2');
        }

        $userServerAccount = $user->getUserServerAccountByServer($server);

        if (!$userServerAccount) {
            throw new NeedAccountException('errors.3');
        }

        $builder->addModelTransformer(
            new CharacterDataTransformer($this->serverService->getCurrentServerDataBaseManager())
        );

    }

}