<?php

namespace AppBundle\Form\Application;

use AppBundle\Entity\Application\Server;
use AppBundle\Form\Application\Config\CharacterEvolutionConfigType;
use AppBundle\Form\Application\Config\WCoinShopConfigType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class ServerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('exp', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Exp',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 100000,
                    ]),
                ],
                'label'       => 'Server Exp',
            ])
            ->add('drop', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Drop',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 100000,
                    ]),
                ],
                'label'       => 'Server Drop',
            ])
            ->add('version', TextType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Version',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 3,
                        'max' => 50,
                    ]),
                ],
                'label'       => 'Server Version',
            ])
            ->add('name', TextType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Name',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 3,
                        'max' => 50,
                    ]),
                ],
                'label'       => 'Server Name',
            ])
            ->add('description', TextareaType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Description',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 3,
                        'max' => 255,
                    ]),
                ],
                'label'       => 'Server Description',
            ])
            ->add('maxOnline', NumberType::class, [
                'required'      => true,
                'attr'          => [
                    'placeholder' => 'Max Online Players',
                ],
                'constraints'   => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 100000,
                    ]),
                ],
                'label'         => 'Max Online Players',
            ])
            ->add('addOnline', NumberType::class, [
                'required'      => true,
                'attr'          => [
                    'placeholder' => 'Add Online Players',
                ],
                'constraints'   => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 100000,
                    ]),
                ],
                'label'         => 'Add Online Players',
            ])
            ->add('enabled', CheckboxType::class, [
                'required'           => false,
                'constraints'        => [
                    new Assert\Type([
                        'type' => 'bool',
                    ]),
                ],
                'label'              => 'Enabled',
            ])
            ->add('webShopConfig', WebShopConfigType::class, [
                'required' => true,
                'label'    => 'WebShop Configuration',
            ])
            ->add('marketConfig', MarketConfigType::class, [
                'required' => true,
                'label'    => 'Market Configuration',
            ])
            ->add('database', DatabaseCredentialType::class, [
                'required' => true,
                'label'    => 'Database Configuration',
            ])
            ->add('statsShopConfig', StatsShopConfigType::class, [
                'required' => true,
                'label'    => 'Stats Shop',
            ])
            ->add('accountMenuConfig', AccountMenuConfigType::class, [
                'required' => true,
                'label'    => 'Account Menu',
            ])
            ->add('referralConfig', ReferralConfigType::class, [
                'required' => true,
                'label'    => 'Referral Config',
            ])
            ->add('voteRewardConfig', VoteRewardConfigType::class, [
                'required' => true,
                'label'    => 'Referral Config',
            ])
            ->add('dataServer', DataServerCredentialsType::class, [
                'required' => true,
                'label'    => 'DataServer Connection',
            ])
            ->add('characterEvolutionConfig', CharacterEvolutionConfigType::class, [
                'required' => true,
                'label'    => 'Character Evolution Config',
            ])
            ->add('wCoinShopConfig', WCoinShopConfigType::class, [
                'required' => true,
                'label'    => 'WCoin Shop',
            ])
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Server::class,
        ]);
    }
}