<?php

namespace AppBundle\Form\Application;

use AppBundle\Entity\Application\WebShopConfig;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class WebShopConfigType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('maxItemLevel', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Max Level',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 15,
                    ]),
                ],
                'label'       => 'Max Item Level',
            ])
            ->add('maxItemOption', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Max Option',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 7,
                    ]),
                ],
                'label'       => 'Max Item Option',
            ])
            ->add('maxExcCount', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Max Exc',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 6,
                    ]),
                ],
                'label'       => 'Max Exc Count',
            ])
            ->add('levelPriceAddMoney', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Level Price Add Money',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\Type([
                        'type' => 'float',
                    ]),
                ],
                'label'       => 'Level Price Add Money',
            ])
            ->add('levelPriceAddCredits', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Level Price Add Credits',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 1,
                    ]),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                ],
                'label'       => 'Level Price Add Credits',
            ])
            ->add('optionPriceAddMoney', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Option Price Add Money',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\Type([
                        'type' => 'float',
                    ]),
                ],
                'label'       => 'Option Price Add Money',
            ])
            ->add('optionPriceAddCredits', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Option Price Add Credits',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 1,
                    ]),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                ],
                'label'       => 'Option Price Add Credits',
            ])
            ->add('skillPriceMultiplier', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Skill Price Multiplier',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\Type([
                        'type' => 'float',
                    ]),
                ],
                'label'       => 'Skill Price Multiplier',
            ])
            ->add('luckPriceMultiplier', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Luck Price Multiplier',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\Type([
                        'type' => 'float',
                    ]),
                ],
                'label'       => 'Luck Price Multiplier',
            ])
            ->add('excellentPriceMultiplier', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Excellent Price Multiplier',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\Type([
                        'type' => 'float',
                    ]),
                ],
                'label'       => 'Excellent Price Multiplier',
            ])
            ->add('ancientPriceMultiplier', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Ancient Price Multiplier',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\Type([
                        'type' => 'float',
                    ]),
                ],
                'label'       => 'Ancient Price Multiplier',
            ])
            ->add('harmonyPriceMultiplier', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Harmony Price Multiplier',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\Type([
                        'type' => 'float',
                    ]),
                ],
                'label'       => 'Harmony Price Multiplier',
            ])
            ->add('pvpPriceMultiplier', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'PvP Price Multiplier',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\Type([
                        'type' => 'float',
                    ]),
                ],
                'label'       => 'PvP Price Multiplier',
            ])
            ->add('globalCreditsDiscountPercent', NumberType::class, [
                'required'      => true,
                'attr'          => [
                    'placeholder' => 'Credits Discount Percent',
                ],
                'constraints'   => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 100,
                    ]),
                ],
                'label'         => 'Credits Discount Percent',
                'property_path' => 'globalCreditsDiscountPercent',
            ])
            ->add('globalMoneyDiscountPercent', NumberType::class, [
                'required'      => true,
                'attr'          => [
                    'placeholder' => 'Money Discount Percent',
                ],
                'constraints'   => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 100,
                    ]),
                ],
                'label'         => 'Money Discount Percent',
                'property_path' => 'globalMoneyDiscountPercent',
            ])
            ->add('enabled', CheckboxType::class, [
                'required'           => false,
                'constraints'        => [
                    new Assert\Type([
                        'type' => 'bool',
                    ]),
                ],
                'label'              => 'Enabled',
            ])
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => WebShopConfig::class,
        ]);
    }
}