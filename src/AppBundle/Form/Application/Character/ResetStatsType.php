<?php

namespace AppBundle\Form\Application\Character;


use AppBundle\Form\Application\CharacterType;
use AppBundle\Model\Application\Character\ResetStatsModel;
use AppBundle\Service\Application\ServerService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class ResetStatsType extends AbstractType
{
    protected $serverService;

    public function __construct(ServerService $serverService)
    {
        $this->serverService = $serverService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $accountMenuConfig = $this->serverService->getCurrentServer()->getAccountMenuConfig();

        $builder
            ->add('character', CharacterType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank(),
                ],
            ])
            ->add('price', HiddenType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\EqualTo(
                        [
                            'value' => $accountMenuConfig->getResetStatsPrice(),
                        ]
                    ),
                ],
                'data' => $accountMenuConfig->getResetStatsPrice(),
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ResetStatsModel::class,
        ]);
    }
}