<?php

namespace AppBundle\Form\Application\Character;

use AppBundle\Model\Application\Character\SearchCharacterModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class SearchCharacterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nick', TextType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 4,
                        'max' => 100,
                    ]),
                    new Assert\Regex([
                        'pattern' => '/^[,a-zA-Z0-9]{4,100}$/i',
                    ]),
                ],
                'label' => 'character.search.0',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SearchCharacterModel::class,
        ]);
    }
}