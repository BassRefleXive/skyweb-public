<?php

namespace AppBundle\Form\Application\Character;


use AppBundle\Doctrine\Type\CharacterClassEnum;
use AppBundle\Form\Application\CharacterType;
use AppBundle\Model\Application\Character\ClassChangeModel;
use AppBundle\Service\Application\ServerService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Enum\MuOnline\CharacterClass as CharacterClassMabeEnum;

class ClassChangeType extends AbstractType
{
    protected $serverService;

    public function __construct(ServerService $serverService)
    {
        $this->serverService = $serverService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $accountMenuConfig = $this->serverService->getCurrentServer()->getAccountMenuConfig();

        $builder
            ->add('character', CharacterType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank(),
                ],
            ])
            ->add('price', HiddenType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\EqualTo(
                        [
                            'value' => $accountMenuConfig->getClassChangePrice(),
                        ]
                    ),
                ],
                'data' => $accountMenuConfig->getClassChangePrice(),
            ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /** @var ClassChangeModel $formData */
            $formData = $event->getData();

            $options = [];

            foreach (CharacterClassMabeEnum::byValue($formData->getCharacter()->getClass())->availableClasses() as $class) {
                $options[$class] = CharacterClassEnum::getNameByKey($class);
            }

            $event->getForm()
                ->add('class', ChoiceType::class, [
                    'required' => true,
                    'choices' => array_flip($options),
                    'label' => 'change_class.3',
                    'choice_translation_domain' => false,
                    'constraints' => [
                        new Assert\NotBlank(),
                    ],
                ]);
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ClassChangeModel::class,
        ]);
    }
}