<?php

namespace AppBundle\Form\Application;

use AppBundle\Entity\Application\MarketConfig;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class MarketConfigType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('commission', NumberType::class, [
                'required' => true,
                'attr' => [
                    'placeholder' => '0 - 100',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 100,
                    ]),
                ],
                'label' => 'Commission %',
            ])
            ->add('maxItems', NumberType::class, [
                'required' => true,
                'attr' => [
                    'placeholder' => '0 - 1000',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 1000,
                    ]),
                ],
                'label' => 'Max items per account',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MarketConfig::class,
        ]);
    }
}