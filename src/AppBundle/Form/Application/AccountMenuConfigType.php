<?php

namespace AppBundle\Form\Application;

use AppBundle\Entity\Application\AccountMenuConfig;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class AccountMenuConfigType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nickChangePrice', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 100000',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 100000,
                    ]),
                ],
            ])
            ->add('onlineHoursRate', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 100000',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 100000,
                    ]),
                ],
            ])
            ->add('classChangePrice', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 100000',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 100000,
                    ]),
                ],
            ])
            ->add('resetStatsPrice', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 100000',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 100000,
                    ]),
                ],
            ])
            ->add('hideInfoDailyRate', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 100000',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 100000,
                    ]),
                ],
            ])
            ->add('resetMasterSkillTreePrice', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 100000',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 100000,
                    ]),
                ],
            ])
            ->add('registrationAddWCoin', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 1000000',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 1000000,
                    ]),
                ],
            ])
            ->add('registrationAddZen', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 1000000',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 1000000,
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AccountMenuConfig::class,
        ]);
    }
}