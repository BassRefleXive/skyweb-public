<?php


namespace AppBundle\Form\Application\Config;

use AppBundle\Entity\Application\Config\WCoinShopConfig;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;


class WCoinShopConfigType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('coinsPerUsd', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 1000000',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 1000000,
                    ]),
                ],
            ])
            ->add('minAmount', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 1000000',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 1000000,
                    ]),
                ],
                'label'       => 'Min $ amount',
            ])
            ->add('enabled', CheckboxType::class, [
                'required'    => false,
                'constraints' => [
                    new Assert\Type([
                        'type' => 'bool',
                    ]),
                ],
                'label'       => 'Enabled',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => WCoinShopConfig::class,
        ]);
    }
}