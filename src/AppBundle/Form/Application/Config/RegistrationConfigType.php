<?php

declare(strict_types = 1);


namespace AppBundle\Form\Application\Config;

use AppBundle\Entity\Application\Config\RegistrationConfig;
use AppBundle\Entity\Application\Config\VipLevelConfig;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;


class RegistrationConfigType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('addVipDays', IntegerType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '0 - 1000000',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\Type([
                        'type' => 'integer',
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 1000000,
                    ]),
                ],
            ])
            ->add('addVipType', EntityType::class, [
                'required' => false,
                'class' => VipLevelConfig::class,
                'multiple' => false,
                'choice_label' => 'title',
                'label' => 'VIP Level',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => RegistrationConfig::class,
        ]);
    }
}