<?php

namespace AppBundle\Form\Application\Config;

use AppBundle\Entity\Application\Config\CharacterEvolutionConfig;
use AppBundle\Enum\Application\Config\CharacterEvolution\ResetForm;
use AppBundle\Enum\Application\Config\CharacterEvolution\ResetPaymentForm;
use AppBundle\Form\Application\PropertyAccessor\CustomPropertyAccessor;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataMapper\PropertyPathMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class CharacterEvolutionConfigType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('resetLevel', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Reset Level',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 1,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 400,
                    ]),
                ],
                'label'       => 'Min reset level',
            ])
            ->add('resetType', ChoiceType::class, [
                'required'      => true,
                'choices'       => [
                    'Keep points, no not add reset points' => ResetForm::KEEP_POINTS,
                    'Keep points, add reset points'        => ResetForm::KEEP_POINTS_ADD_RESET_POINTS,
                    'Reset points, add reset points'       => ResetForm::RESET_POINTS_ADD_RESET_POINTS,
                ],
                'constraints'   => [
                    new Assert\NotBlank(),
                ],
                'label'         => 'Reset mode',
            ])
            ->add('resetPointsDK', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'DK Reset Points',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 50000,
                    ]),
                ],
                'label'       => 'DK Reset Points',
            ])
            ->add('resetPointsDW', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'DW Reset Points',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 50000,
                    ]),
                ],
                'label'       => 'DW Reset Points',
            ])
            ->add('resetPointsFE', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'FE Reset Points',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 50000,
                    ]),
                ],
                'label'       => 'FE Reset Points',
            ])
            ->add('resetPointsMG', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'MG Reset Points',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 50000,
                    ]),
                ],
                'label'       => 'MG Reset Points',
            ])
            ->add('resetPointsDL', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'DL Reset Points',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 50000,
                    ]),
                ],
                'label'       => 'DL Reset Points',
            ])
            ->add('resetPointsSUM', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'SUM Reset Points',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 50000,
                    ]),
                ],
                'label'       => 'SUM Reset Points',
            ])
            ->add('resetPointsRF', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'RF Reset Points',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 50000,
                    ]),
                ],
                'label'       => 'RF Reset Points',
            ])
            ->add('resetPaymentType', ChoiceType::class, [
                'required'    => true,
                'choices'     => [
                    'Free'                           => ResetPaymentForm::FREE,
                    'Pay constant price every reset' => ResetPaymentForm::CONSTANT,
                    'Price * Reset'                  => ResetPaymentForm::MULTIPLIER,
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                ],
                'label'       => 'Reset payment mode',
            ])
            ->add('resetPrice', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'kk Zen',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 50000,
                    ]),
                ],
                'label'       => 'Reset price in kk Zen',
            ])
            ->add('resetReward', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '500',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 50000,
                    ]),
                ],
                'label'       => 'Reset reward in WCoin',
            ])
            ->add('resetLimit', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '500',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 50000,
                    ]),
                ],
                'label'       => 'Maximum reset could be achieved',
            ])
            ->add('grandResetReset', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '500',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 50000,
                    ]),
                ],
                'label'       => 'Reset required for grand reset',
            ])
            ->add('grandResetPrice', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '500',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 50000,
                    ]),
                ],
                'label'       => 'Price in kk per grand reset',
            ])
            ->add('grandResetPoints', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '500',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 50000,
                    ]),
                ],
                'label'       => 'Level UP points per grand reset',
            ])
            ->add('grandResetReward', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => '500',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 50000,
                    ]),
                ],
                'label'       => 'WCoin reward per grand reset',
            ])
            ->setDataMapper(new PropertyPathMapper(
                new CustomPropertyAccessor(
                    [
                        'resetType' => 'setResetType',
                        'resetPaymentType' => 'setResetPaymentType',
                    ],
                    [
                        'resetType' => 'getResetType.getValue',
                        'resetPaymentType' => 'getResetPaymentType.getValue',
                    ]
                )
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CharacterEvolutionConfig::class,
        ]);
    }
}