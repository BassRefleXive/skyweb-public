<?php

namespace AppBundle\Form\Application\Config;

use AppBundle\Entity\Application\Config\CastleSiegeCycle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class CastleSiegeCycleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('stage', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Stage',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 1,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 9,
                    ]),
                ],
                'label'       => 'Stage',
            ])
            ->add('day', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Day',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 31,
                    ]),
                ],
                'label'       => 'Day',
            ])
            ->add('hour', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Hour',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 23,
                    ]),
                ],
                'label'       => 'Hour',
            ])
            ->add('minute', NumberType::class, [
                'required'    => true,
                'attr'        => [
                    'placeholder' => 'Minute',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 59,
                    ]),
                ],
                'label'       => 'Minute',
            ])
            ->add('title', TextType::class, [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 1,
                        'max' => 128,
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CastleSiegeCycle::class,
        ]);
    }
}