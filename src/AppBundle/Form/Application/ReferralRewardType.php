<?php

namespace AppBundle\Form\Application;

use AppBundle\Model\Application\ReferralRewardModel;
use AppBundle\Service\Application\ServerService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class ReferralRewardType extends AbstractType
{
    protected $serverService;

    public function __construct(ServerService $serverService)
    {
        $this->serverService = $serverService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $referralsConfig = $this->serverService->getCurrentServer()->getReferralConfig();

        $builder
            ->add('reward', HiddenType::class, [
                'required'    => true,
                'data'        => $referralsConfig->getReward(),
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\EqualTo([
                        'value' => $referralsConfig->getReward(),
                    ]),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'      => ReferralRewardModel::class,
            'csrf_protection' => false,
        ]);
    }
}