<?php


namespace AppBundle\Service\Common\Funds;


use AppBundle\Entity\Application\Server;
use AppBundle\Service\Application\ServerService;
use Psr\Log\LoggerInterface;

abstract class FundsService
{
    private $serverService;
    private $logger;

    public function __construct(ServerService $serverService, LoggerInterface $logger)
    {
        $this->serverService = $serverService;
        $this->logger = $logger;
    }

    abstract public function add(string $accountId, int $amount, Server $server = null, bool $writeLog = true): void;

    abstract public function sub(string $accountId, int $amount, Server $server = null, bool $writeLog = true): void;

    abstract public function get(string $accountId, Server $server = null, bool $writeLog = true);

    abstract public function set(string $accountId, int $amount, Server $server = null, bool $writeLog = true): void;

    abstract protected function databaseConnection(Server $server = null, bool $writeLog = true);

    protected function logger(): LoggerInterface
    {
        return $this->logger;
    }

    final protected function serverService(): ServerService
    {
        return $this->serverService;
    }

    final protected function server(?Server $server): Server
    {
        if (null !== $server) {
            return $server;
        }

        return $this->serverService()->getCurrentServer();
    }
}