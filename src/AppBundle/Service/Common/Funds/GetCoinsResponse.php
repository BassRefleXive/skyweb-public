<?php

namespace AppBundle\Service\Common\Funds;


class GetCoinsResponse
{
    private $wCoinC;
    private $wCoinP;
    private $wCoinG;

    public function __construct(int $wCoinC, int $wCoinP = 0, int $wCoinG)
    {
        $this->wCoinC = $wCoinC;
        $this->wCoinP = $wCoinP;
        $this->wCoinG = $wCoinG;
    }

    public function wCoinC(): int
    {
        return $this->wCoinC;
    }

    public function wCoinP(): int
    {
        return $this->wCoinP;
    }

    public function wCoinG(): int
    {
        return $this->wCoinG;
    }
}