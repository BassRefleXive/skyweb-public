<?php

declare(strict_types=1);

namespace AppBundle\Service\Application\CastleSiege;

use AppBundle\Entity\Application\Server;
use AppBundle\Entity\MuOnline\CastleSiege\SiegeRegisteredGuild;
use AppBundle\Model\Application\CastleSiege\CastleData;
use AppBundle\Repository\MuOnline\CastleSiege\CastleDataRepository;
use AppBundle\Repository\MuOnline\CastleSiege\SiegeRegisteredGuildRepository;
use AppBundle\Service\Application\ServerService;
use AppBundle\Entity\MuOnline\CastleSiege\CastleData as CastleDataEntity;

class CastleDataProvider
{
    private $serverService;
    private $castleDataCacheTtl;
    private $registeredGuildsCacheTtl;

    public function __construct(
        ServerService $serverService,
        int $castleDataCacheTtl,
        int $registeredGuildsCacheTtl
    )
    {
        $this->serverService = $serverService;
        $this->castleDataCacheTtl = $castleDataCacheTtl;
        $this->registeredGuildsCacheTtl = $registeredGuildsCacheTtl;
    }

    public function provide(Server $server): CastleData
    {
        $castleData = $this->castleData($server);

        return new CastleData(
            $castleData,
            $server->castleSiegeCycles()->toArray()
        );
    }

    public function registeredGuilds(Server $server): array
    {
        $manager = $this->serverService->getServerDataBaseManager($server);

        /** @var SiegeRegisteredGuildRepository $repository */
        $repository = $manager->getRepository(SiegeRegisteredGuild::class);
        $repository->setTtl($this->registeredGuildsCacheTtl);

        return $repository->registered();
    }

    private function castleData(Server $server): CastleDataEntity
    {
        $manager = $this->serverService->getServerDataBaseManager($server);

        /** @var CastleDataRepository $repository */
        $repository = $manager->getRepository(CastleDataEntity::class);
        $repository->setTtl($this->castleDataCacheTtl);

        return $repository->get();
    }
}