<?php

namespace AppBundle\Service\Application;


use AppBundle\Entity\Application\User\User;
use AppBundle\Exception\Application\DisplayableException;

class StatsShopService
{

    protected $cartService;
    protected $serverService;

    public function __construct(CartService $cartService, ServerService $serverService)
    {
        $this->cartService = $cartService;
        $this->serverService = $serverService;
    }

    public function buy(User $user, int $step)
    {
        if (!$this->serverService->getCurrentServer()->getStatsShopConfig()->isEnabled()) {
            throw new DisplayableException('stats_shop.index.5');
        }

        $statsShopConfig = $this->serverService->getCurrentServer()->getStatsShopConfig();

        $priceCredits = $step * $statsShopConfig->getStepPriceCredits();
        $priceMoney = $step * $statsShopConfig->getStepPriceMoney();
        $stats = $step * $statsShopConfig->getStep();

        $cart = $this->cartService->addStats($user, $stats, $priceCredits, $priceMoney);

        return $cart;
    }

}