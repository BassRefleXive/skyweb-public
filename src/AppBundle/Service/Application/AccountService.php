<?php

declare(strict_types = 1);

namespace AppBundle\Service\Application;


use AppBundle\Entity\Application\UserServerAccount;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Model\Application\Account\HideInfoModel;
use AppBundle\Repository\Application\UserServerAccountRepository;
use AppBundle\Service\MuOnline\Funds\CoinsService;

class AccountService
{
    private $coinsService;
    private $userServerAccountRepository;

    public function __construct(CoinsService $coinsService, UserServerAccountRepository $userServerAccountRepository)
    {
        $this->coinsService = $coinsService;
        $this->userServerAccountRepository = $userServerAccountRepository;
    }

    public function hideInfo(UserServerAccount $account, HideInfoModel $model): void
    {
        $model->setDays($model->days() + 1);

        if (null !== $account->hiddenTill()) {
            throw new DisplayableException('errors.50');
        }

        $price = $account->getServer()->getAccountMenuConfig()->hideInfoDailyRate() * $model->days();

        $coins = $this->coinsService->get($account->getUser()->getUsername(), $account->getServer());

        if ($coins->wCoinC() < $price) {
            throw new DisplayableException('errors.14');
        }

        $this->coinsService->sub($account->getUser()->getUsername(), $price, $account->getServer());

        $account->hideInfo($model->days());

        $this->userServerAccountRepository->save($account);
    }

    public function unHideExpiredAccounts(): int
    {
        return $this->userServerAccountRepository->unHideHiddenExpiredAccounts();
    }
}