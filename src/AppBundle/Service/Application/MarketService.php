<?php

namespace AppBundle\Service\Application;


use AppBundle\Doctrine\Type\Mappings\Items\Item;
use AppBundle\Doctrine\Type\MarketPriceTypeEnum;
use AppBundle\Entity\Application\Items\AncientSet;
use AppBundle\Entity\Application\Items\HarmonyOption;
use AppBundle\Entity\Application\Items\PvPOption;
use AppBundle\Entity\Application\MarketItem;
use AppBundle\Entity\Application\UserServerAccount;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Filter\MarketFilter;
use AppBundle\Service\MuOnline\Funds\CoinsService;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;

class MarketService
{
    private $em;
    private $coinsService;
    private $logger;

    public function __construct(EntityManager $em, CoinsService $coinsService, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->coinsService = $coinsService;
        $this->logger = $logger;
    }

    public function getByFilter(MarketFilter $filter): array
    {
        $repo = $this->em->getRepository(MarketItem::class);

        return $repo->findByQueryFilter($filter);
    }

    public function buyItem(UserServerAccount $userServerAccount, MarketItem $item, int $price)
    {
        $this->logItemAction('Buy item request.', $userServerAccount, $item);

        $this->payForTheItem($userServerAccount, $item, $price);
        $this->updateSellerFunds($item->getUserServerAccount(), $item);

        $this->logItemAction('Buy item prepare move.', $userServerAccount, $item);
        $userServerAccount->addBoughtMarketItem($item);

        $this->em->persist($item);
        $this->em->flush($item);

        $this->logItemAction('Item bought.', $userServerAccount, $item);
    }

    public function returnItem(UserServerAccount $userServerAccount, MarketItem $item, int $commission)
    {
        $this->logItemAction('Return item request.', $userServerAccount, $item);

        $this->payForTheItem($userServerAccount, $item, $commission);

        $this->logItemAction('Item return prepare.', $userServerAccount, $item);
        $userServerAccount->removeMarketItem($item);

        $this->em->persist($userServerAccount);
        $this->em->flush($userServerAccount);

        $this->logItemAction('Item returned.', $userServerAccount, $item);
    }

    public function sell(UserServerAccount $userServerAccount, Item $item, int $price, string $priceType)
    {
        if ($userServerAccount->activeMarketItems()->count() >= $userServerAccount->getServer()->getMarketConfig()->getMaxItems()) {
            throw new DisplayableException('errors.52');
        }

        $marketItem = (new MarketItem())
            ->setHex($item->getHex())
            ->setLevel($item->getGeneralOptions()->getLvl())
            ->setOption($item->getGeneralOptions()->getOpt())
            ->setSkill($item->getGeneralOptions()->getIsSkill())
            ->setLuck($item->getGeneralOptions()->getIsLuck())
            ->setExcellent($item->getExcellentOptions()->isExcellent())
            ->setAncient($item->getAncientSet() instanceof AncientSet)
            ->setHarmony($item->getHarmonyOptions()->getOption() instanceof HarmonyOption)
            ->setPvp($item->getPVPOption() instanceof PvPOption)
            ->setTitle($item->getItemInfo()->getName())
            ->setPrice($price)
            ->setPriceType($priceType)
            ->setItemInfo($item->getItemInfo())
            ->setUserServerAccount($userServerAccount)
            ->setServer($userServerAccount->getServer());

        $this->logItemAction('Sell item request.', $userServerAccount, $marketItem);

        $this->em->persist($marketItem);
        $this->em->flush($marketItem);

        $this->logItemAction('Item sold.', $userServerAccount, $marketItem);
    }

    private function updateSellerFunds(UserServerAccount $account, MarketItem $item)
    {
        switch ($item->getPriceType()) {
            case MarketPriceTypeEnum::MARKET_PRICE_TYPE_CREDITS:
                {
                    $this->logItemAction('Start updating seller WCoin funds.', $account, $item);

                    $this->coinsService->add($account->getUser()->getUsername(), $item->getPrice());

                    $this->logItemAction('Seller WCoin funds updated.', $account, $item);

                    break;
                }
            case MarketPriceTypeEnum::MARKET_PRICE_TYPE_ZEN:
                {
                    $this->logItemAction('Start updating seller Zen funds.', $account, $item);
                    $account->depositZen($item->getPrice());

                    $this->em->persist($account);
                    $this->em->flush($account);
                    $this->logItemAction('Seller Zen funds updated.', $account, $item);

                    break;
                }
            default:
                {
                    throw new \InvalidArgumentException('Unknown market item price type');
                }
        }
    }

    private function payForTheItem(UserServerAccount $account, MarketItem $item, int $price)
    {
        switch ($item->getPriceType()) {
            case MarketPriceTypeEnum::MARKET_PRICE_TYPE_CREDITS:
                {
                    $this->logItemAction('Pay with WCoins.', $account, $item);
                    $coins = $this->coinsService->get($account->getUser()->getUsername());

                    if ($coins->wCoinC() < $price) {
                        $this->logItemAction('Not enough WCoin to buy.', $account, $item);

                        throw new DisplayableException('errors.14');
                    }

                    $this->logItemAction('Payment preparing.', $account, $item);
                    $this->coinsService->set($account->getUser()->getUsername(), $coins->wCoinC() - $price);
                    $this->logItemAction('Payment successful.', $account, $item);

                    break;
                }
            case MarketPriceTypeEnum::MARKET_PRICE_TYPE_ZEN:
                {
                    $this->logItemAction('Pay with Zen.', $account, $item);

                    if ($account->zen() < $price) {
                        $this->logItemAction('Not enough Zen to buy.', $account, $item);

                        throw new DisplayableException('errors.15');
                    }

                    $this->logItemAction('Payment preparing.', $account, $item);
                    $account->withdrawZen($price);

                    $this->em->persist($account);
                    $this->em->flush($account);
                    $this->logItemAction('Payment successful.', $account, $item);

                    break;
                }
            default:
                {
                    throw new \InvalidArgumentException('Unknown market item price type');
                }
        }
    }

    private function logItemAction(string $action, UserServerAccount $account, MarketItem $item): void
    {
        $this->logger->info(
            $action,
            [
                'user_id' => $account->getUser()->getId(),
                'user_server_account_id' => $account->getId(),
                'item' => [
                    'id' => $item->getId(),
                    'hex' => $item->getHex(),
                    'level' => $item->getLevel(),
                    'title' => $item->getTitle(),
                    'option' => $item->getOption(),
                    'skill' => $item->getSkill(),
                    'luck' => $item->getLuck(),
                    'excellent' => $item->getExcellent(),
                    'ancient' => $item->getAncient(),
                    'harmony' => $item->getHarmony(),
                    'pvp' => $item->getPvp(),
                    'price' => $item->getPrice(),
                    'price_type' => $item->getPriceType(),
                ]
            ]

        );
    }
}