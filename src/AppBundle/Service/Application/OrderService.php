<?php

declare(strict_types = 1);

namespace AppBundle\Service\Application;


use AppBundle\Entity\Application\Payments\Order;

class OrderService
{
    private $purchaseProcessor;

    public function __construct(PurchaseProcessor $purchaseProcessor)
    {
        $this->purchaseProcessor = $purchaseProcessor;
    }

    public function processPayedOrder(Order $order)
    {
        $this->purchaseProcessor->assignOrderItems($order, $order->amount());
    }
}