<?php

declare(strict_types = 1);

namespace AppBundle\Service\Application\Payments;

use AppBundle\Doctrine\Type\OrderStateEnum;
use AppBundle\Entity\Application\Cart;
use AppBundle\Entity\Application\CartDiscountCoupon;
use AppBundle\Entity\Application\Payments\Order;
use AppBundle\Enum\Application\PaymentSystem;
use AppBundle\Exception\Application\Payments\PaymentSystem\PaymentAlreadyProcessedException;
use AppBundle\Exception\Application\Payments\PaymentSystem\PaymentNotificationException;
use AppBundle\Repository\Application\Payments\OrderRepository;
use AppBundle\Service\Application\OrderService;
use AppBundle\Service\Application\Payments\PaymentSystem\PaymentNotificationInterface;
use AppBundle\Service\Application\Payments\PaymentSystem\PaymentSystemInterface;
use Psr\Log\LoggerInterface;

class PaymentProcessor
{
    private $paymentSystem;
    private $orderRepository;
    private $orderService;
    private $logger;

    public function __construct(
        PaymentSystemInterface $paymentSystem,
        OrderRepository $orderRepository,
        OrderService $orderService,
        LoggerInterface $logger
    )
    {
        $this->paymentSystem = $paymentSystem;
        $this->orderRepository = $orderRepository;
        $this->orderService = $orderService;
        $this->logger = $logger;
    }

    public function createPaymentUrl(Order $order): string
    {
        $url = $this->paymentSystem->requestUrl($order);

        return $url;
    }

    public function preparePaymentOrder(Cart $cart, float $price, CartDiscountCoupon $coupon = null): Order
    {
        $order = $this->paymentSystem->createOrder($cart, $price, $coupon);

        $this->orderRepository->save($order);

        return $order;
    }

    public function processPaymentNotification(PaymentNotificationInterface $paymentNotification)
    {
        if ($paymentNotification->sign() !== $this->paymentSystem->generateSignature($paymentNotification->raw())) {
            throw PaymentNotificationException::signMismatch();
        }

        if (!$this->paymentSystem->checkoutValid($paymentNotification)) {
            throw PaymentNotificationException::checkoutMismatch();
        }

        if (!$this->paymentSystem->stateValid($paymentNotification)) {
            throw PaymentNotificationException::invalidState();
        }

        /** @var Order $order */
        $order = $this->orderRepository->find($paymentNotification->orderId());

        if (!$order) {
            throw PaymentNotificationException::missingOrder($paymentNotification->orderId());
        }

        try {
            if ($order->state() !== OrderStateEnum::ORDER_STATE_INIT) {
                throw PaymentAlreadyProcessedException::create($order->id());
            }

            if ($order->amount() !== $paymentNotification->orderAmount()) {
                throw PaymentNotificationException::amountMismatch($order->amount(), $paymentNotification->orderAmount());
            }
            $order->finalizeInterKassaPayment($paymentNotification);

            $this->orderService->processPayedOrder($order);
            $this->logger->info(sprintf('Order #%d purchased items added to user.', $order->id()));

            $this->orderRepository->save($order);

            $this->logger->info(sprintf('Order #%d completed.', $order->id()));
        } catch (\Throwable $e) {
            $order->markFailed(PaymentSystem::byValue(PaymentSystem::INTERKASSA));
            $this->orderRepository->save($order);

            $this->logger->error(sprintf('Order #%d failed. Reason: "%s".', $order->id(), $e->getMessage()));

            throw $e;
        }
    }
}