<?php


namespace AppBundle\Service\Application\Payments\PaymentSystem\Interkassa;


use AppBundle\Exception\Application\Payments\PaymentSystem\PaymentNotificationException;
use AppBundle\Service\Application\Payments\PaymentSystem\PaymentNotificationInterface;
use Collection\Map;
use Symfony\Component\HttpFoundation\Request;

class PaymentNotification implements PaymentNotificationInterface
{
    private $raw;
    private $checkoutId;
    private $orderId;
    private $transactionId;
    private $orderAmount;
    private $paymentSystemAmount;
    private $cartAmount;
    private $paymentMethod;
    private $state;
    private $sign;
    private $processedAt;

    private function __construct(
        array $raw,
        string $checkoutId,
        int $orderId,
        string $transactionId,
        float $orderAmount,
        float $paymentSystemAmount,
        float $cartAmount,
        string $paymentMethod,
        string $state,
        string $sign,
        \DateTime $processedAt
    )
    {
        $this->raw = $raw;
        $this->checkoutId = $checkoutId;
        $this->orderId = $orderId;
        $this->transactionId = $transactionId;
        $this->orderAmount = $orderAmount;
        $this->paymentSystemAmount = $paymentSystemAmount;
        $this->cartAmount = $cartAmount;
        $this->paymentMethod = $paymentMethod;
        $this->state = $state;
        $this->sign = $sign;
        $this->processedAt = $processedAt;
    }

    public function orderId(): int
    {
        return $this->orderId;
    }

    public function checkoutId(): string
    {
        return $this->checkoutId;
    }

    public function transactionId(): string
    {
        return $this->transactionId;
    }

    public function orderAmount(): float
    {
        return $this->orderAmount;
    }

    public function paymentSystemAmount(): float
    {
        return $this->paymentSystemAmount;
    }

    public function cartAmount(): float
    {
        return $this->cartAmount;
    }

    public function paymentMethod(): string
    {
        return $this->paymentMethod;
    }

    public function state(): string
    {
        return $this->state;
    }

    public function sign(): string
    {
        return $this->sign;
    }

    public function raw(): array
    {
        return $this->raw;
    }

    public function processedAt(): \DateTime
    {
        return $this->processedAt;
    }

    public static function fromRequest(Request $request): self
    {
        $params = new Map($request->request->all());

        return new self(
            $params->all(),
            $params->get('ik_co_id')->getOrThrow(PaymentNotificationException::missingParameter('ik_co_id')),
            (int)$params->get('ik_pm_no')->getOrThrow(PaymentNotificationException::missingParameter('ik_pm_no')),
            $params->get('ik_trn_id')->getOrThrow(PaymentNotificationException::missingParameter('ik_trn_id')),
            (float)$params->get('ik_am')->getOrThrow(PaymentNotificationException::missingParameter('ik_am')),
            (float)$params->get('ik_ps_price')->getOrThrow(PaymentNotificationException::missingParameter('ik_ps_price')),
            (float)$params->get('ik_co_rfn')->getOrThrow(PaymentNotificationException::missingParameter('ik_co_rfn')),
            $params->get('ik_pw_via')->getOrThrow(PaymentNotificationException::missingParameter('ik_pw_via')),
            $params->get('ik_inv_st')->getOrThrow(PaymentNotificationException::missingParameter('ik_inv_st')),
            $params->get('ik_sign')->getOrThrow(PaymentNotificationException::missingParameter('ik_sign')),
            new \DateTime($params->get('ik_inv_prc')->getOrThrow(PaymentNotificationException::missingParameter('ik_inv_prc')))
        );
    }
}