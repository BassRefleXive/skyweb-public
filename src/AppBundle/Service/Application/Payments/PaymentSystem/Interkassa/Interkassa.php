<?php

declare(strict_types = 1);

namespace AppBundle\Service\Application\Payments\PaymentSystem\Interkassa;


use AppBundle\Entity\Application\Cart;
use AppBundle\Entity\Application\CartDiscountCoupon;
use AppBundle\Entity\Application\Payments\Order;
use AppBundle\Service\Application\Payments\Factory\OrderFactory;
use AppBundle\Service\Application\Payments\PaymentSystem\PaymentNotificationInterface;
use AppBundle\Service\Application\Payments\PaymentSystem\PaymentSystemInterface;

class Interkassa implements PaymentSystemInterface
{
    const TEST_PAYMENT_SYSTEM_IDENTIFIER = 'test_interkassa_test_xts';
    const PAYMENT_STATE_SUCCESS = 'success';

    private $orderFactory;
    private $checkoutId;
    private $currency;
    private $url;
    private $secretKey;
    private $testSecretKey;

    public function __construct(
        OrderFactory $orderFactory,
        string $checkoutId,
        string $currency,
        string $url,
        string $secretKey,
        string $testSecretKey
    )
    {
        $this->orderFactory = $orderFactory;
        $this->checkoutId = $checkoutId;
        $this->currency = $currency;
        $this->url = $url;
        $this->secretKey = $secretKey;
        $this->testSecretKey = $testSecretKey;
    }

    public function requestUrl(Order $order): string
    {
        $params = [
            'ik_co_id' => $this->checkoutId,
            'ik_pm_no' => $order->id(),
            'ik_am'    => $order->amount(),
            'ik_cur'   => $this->currency,
            'ik_desc'  => $order->description(),
        ];

        $params = array_merge(
            $params,
            [
                'ik_sign' => $this->generateSignature($params),
            ]
        );

        return $this->url . '?' . http_build_query($params);
    }

    public function createOrder(Cart $cart, float $price, CartDiscountCoupon $coupon = null): Order
    {
        return $this->orderFactory->createByCart($cart, $price, $this->currency, $coupon);
    }

    public function generateSignature(array $params): string
    {
        if (array_key_exists('ik_sign', $params)) {
            unset($params['ik_sign']);
        }

        $key = $this->secretKey;

        if (array_key_exists('ik_pw_via', $params) && $params['ik_pw_via'] === self::TEST_PAYMENT_SYSTEM_IDENTIFIER) {
            $key = $this->testSecretKey;
        }

        ksort($params, SORT_STRING);
        array_push($params, $key);

        return base64_encode(
            md5(
                implode(':', $params),
                true
            )
        );
    }

    public function checkoutValid(PaymentNotificationInterface $paymentNotification): bool
    {
        return $this->checkoutId === $paymentNotification->checkoutId();
    }

    public function stateValid(PaymentNotificationInterface $paymentNotification): bool
    {
        return self::PAYMENT_STATE_SUCCESS === $paymentNotification->state();
    }
}