<?php


namespace AppBundle\Service\Application\Payments\PaymentSystem;


interface PaymentNotificationInterface
{
    public function checkoutId(): string;

    public function orderId(): int;

    public function transactionId(): string;

    public function paymentSystemAmount(): float;

    public function orderAmount(): float;

    public function cartAmount(): float;

    public function paymentMethod(): string;

    public function state(): string;

    public function sign(): string;

    public function raw(): array;

    public function processedAt(): \DateTime;
}