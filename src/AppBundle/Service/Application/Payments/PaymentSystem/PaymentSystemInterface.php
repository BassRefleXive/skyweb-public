<?php

declare(strict_types = 1);

namespace AppBundle\Service\Application\Payments\PaymentSystem;

use AppBundle\Entity\Application\Cart;
use AppBundle\Entity\Application\CartDiscountCoupon;
use AppBundle\Entity\Application\Payments\Order;

interface PaymentSystemInterface
{
    public function requestUrl(Order $order): string;

    public function createOrder(Cart $cart, float $price, CartDiscountCoupon $coupon = null): Order;

    public function generateSignature(array $params): string;

    public function checkoutValid(PaymentNotificationInterface $paymentNotification): bool;

    public function stateValid(PaymentNotificationInterface $paymentNotification): bool;
}