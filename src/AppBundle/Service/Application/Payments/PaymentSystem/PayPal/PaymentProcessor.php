<?php

declare(strict_types=1);

namespace AppBundle\Service\Application\Payments\PaymentSystem\PayPal;

use AppBundle\Doctrine\Type\ItemToBuyTypeEnum;
use AppBundle\Doctrine\Type\Mappings\Items\Item;
use AppBundle\Doctrine\Type\OrderStateEnum;
use AppBundle\Entity\Application\ItemToBuy;
use AppBundle\Entity\Application\Payments\Order;
use AppBundle\Enum\Application\Doctrine\PaymentSystemType;
use AppBundle\Enum\Application\PaymentSystem;
use AppBundle\Exception\Application\Payments\PaymentSystem\PaymentAlreadyProcessedException;
use AppBundle\Exception\Application\Payments\PaymentSystem\PaymentNotificationException;
use AppBundle\Repository\Application\Payments\OrderRepository;
use AppBundle\Service\Application\ItemService;
use AppBundle\Service\Application\OrderService;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item as PayPalItem;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Exception\PayPalConnectionException;
use PayPal\Rest\ApiContext;
use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;

class PaymentProcessor
{
    private const ENV_SANDBOX = 'sandbox';
    private const ENV_PRODUCTION = 'production';

    private const MODE_SANDBOX = 'sandbox';
    private const MODE_PRODUCTION = 'live';

    private $itemService;
    private $router;
    private $orderService;
    private $orderRepository;
    private $logger;
    private $currency;
    private $clientId;
    private $secret;
    private $environment;

    public function __construct(
        ItemService $itemService,
        Router $router,
        OrderService $orderService,
        OrderRepository $orderRepository,
        LoggerInterface $logger,
        string $currency,
        string $clientId,
        string $secret,
        string $environment
    )
    {
        $this->itemService = $itemService;
        $this->router = $router;
        $this->orderService = $orderService;
        $this->orderRepository = $orderRepository;
        $this->logger = $logger;
        $this->currency = $currency;
        $this->clientId = $clientId;
        $this->secret = $secret;
        $this->environment = $environment;
    }

    public function initialize(Order $order): string
    {
        try {
            $this->logger->info(sprintf('Order #%d initialization request received.', $order->id()));

            if (OrderStateEnum::ORDER_STATE_STARTED === $order->state() && null !== $order->paymentSystem() && $order->paymentSystem()->isPayPal()) {
                $this->logger->info(sprintf('Order #%d payment already initialized. Returning previously generated ID.', $order->id()));

                return $order->transactionId();
            }

            if (OrderStateEnum::ORDER_STATE_INIT !== $order->state()) {
                throw PaymentAlreadyProcessedException::create($order->id());
            }

            $payer = new Payer();
            $payer->setPaymentMethod('paypal');

            $itemList = new ItemList();

            /** @var ItemToBuy $orderItem */
            foreach ($order->items() as $orderItem) {
                $itemName = 'Unknown';

                switch ($orderItem->getType()) {
                    case ItemToBuyTypeEnum::TYPE_ITEM:
                        {
                            $itemName = $this->itemService
                                ->parse(Item::createExisting()->setHex($orderItem->getItem()))
                                ->getItemInfo()
                                ->getName();

                            break;
                        }

                    case ItemToBuyTypeEnum::TYPE_STATS:
                        {
                            $itemName = sprintf('%d LevelUP Points', $orderItem->getStats());

                            break;
                        }

                    case ItemToBuyTypeEnum::TYPE_WCOIN:
                        {
                            $itemName = sprintf('%d WCoin', $orderItem->getStats());

                            break;
                        }
                }

                $item = (new PayPalItem())
                    ->setName($itemName)
                    ->setCurrency($this->currency)
                    ->setQuantity(1)
                    ->setPrice(
                        $orderItem->withAppliedDiscount(
                            null !== $order->discountCoupon()
                                ? $order->discountCoupon()->getDiscount()
                                : 0
                        )
                    );

                $itemList->addItem($item);
            }

            $details = (new Details())
                ->setShipping(0)
                ->setTax(0)
                ->setSubtotal($order->discountedAmount());

            $amount = (new Amount())
                ->setCurrency($this->currency)
                ->setTotal($order->discountedAmount())
                ->setDetails($details);

            $transaction = (new Transaction())
                ->setAmount($amount)
                ->setItemList($itemList)
                ->setDescription($order->description())
                ->setInvoiceNumber($order->id());

            $redirectUrls = (new RedirectUrls())
                ->setReturnUrl($this->router->generate('sky_integrations_payments_paypal_success', [], UrlGeneratorInterface::ABSOLUTE_URL))
                ->setCancelUrl($this->router->generate('sky_integrations_payments_paypal_fail', ['id' => $order->id()], UrlGeneratorInterface::ABSOLUTE_URL));

            $payment = (new Payment())
                ->setIntent('sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirectUrls)
                ->setTransactions([$transaction]);

            $payment = $payment->create($this->getContext());

            $order->startPayPalPayment($payment);
            $this->orderRepository->save($order);

            $this->logger->info(sprintf('Order #%d payment initialized.', $order->id()));

            return $payment->getId();
        } catch (\Throwable $e) {
            $this->logger->error(sprintf('Order #%d initialization failed. Reason: "%s".', $order->id(), $e->getMessage()));

            throw $e;
        }
    }

    public function finalize(Order $order, string $paymentId, string $payerId): Payment
    {
        $this->logger->info(sprintf('Order #%d finalization request received.', $order->id()));

        try {
            if (OrderStateEnum::ORDER_STATE_STARTED !== $order->state()) {
                throw PaymentNotificationException::invalidState();
            }

            if (null === $order->paymentSystem()) {
                throw PaymentNotificationException::unknownPaymentSystem();
            }

            if (!$order->paymentSystem()->isPayPal()) {
                throw PaymentNotificationException::invalidPaymentSystem(PaymentSystem::PAYPAL, $order->paymentSystem()->getValue());
            }

            try {
                $apiContext = $this->getContext();

                $payment = Payment::get($paymentId, $apiContext);

                $execution = (new PaymentExecution())->setPayerId($payerId);

                $payment->execute($execution, $apiContext);

                $this->logger->info(sprintf('Order #%d finalization API request succeeded.', $order->id()));
            } catch (PayPalConnectionException $e) {
                $this->logger->error(sprintf('Order #%d finalization API request failed.', $order->id()));

                throw PaymentNotificationException::unexpectedResponse($e->getMessage(), $e->getData());
            }

            if ('approved' !== $payment->getState()) {
                throw PaymentNotificationException::invalidState();
            }

            $this->orderRepository->save($order);

            $order->finalizePayPalPayment();

            $this->orderService->processPayedOrder($order);
            $this->logger->info(sprintf('Order #%d purchased items added to user.', $order->id()));


            $this->orderRepository->save($order);

            $this->logger->info(sprintf('Order #%d completed.', $order->id()));

            return $payment;
        } catch (\Throwable $e) {
            $order->markFailed(PaymentSystem::byValue(PaymentSystem::PAYPAL));
            $this->orderRepository->save($order);

            $this->logger->error(sprintf('Order #%d finalization failed. Reason: "%s".', $order->id(), $e->getMessage()));

            throw $e;
        }
    }

    private function getContext(): ApiContext
    {
        $context = new ApiContext(new OAuthTokenCredential($this->clientId, $this->secret));
        $context->setConfig([
            'mode' => $this->environment === self::ENV_PRODUCTION
                ? self::MODE_PRODUCTION
                : self::MODE_SANDBOX
        ]);

        return $context;
    }
}