<?php

declare(strict_types = 1);

namespace AppBundle\Service\Application\Payments\Factory;

use AppBundle\Doctrine\Type\ItemToBuyTypeEnum;
use AppBundle\Doctrine\Type\Mappings\Items\Item;
use AppBundle\Entity\Application\Cart;
use AppBundle\Entity\Application\CartDiscountCoupon;
use AppBundle\Entity\Application\ItemToBuy;
use AppBundle\Entity\Application\Payments\Order;
use AppBundle\Service\MuOnline\Item\OptionsParserChain;
use Collection\Sequence;

class OrderFactory
{
    private $itemParser;

    public function __construct(OptionsParserChain $itemParser)
    {
        $this->itemParser = $itemParser;
    }

    public function createByCart(Cart $cart, float $price, string $currency, CartDiscountCoupon $coupon = null): Order
    {
        $order = new Order($cart->getUserServerAccount(), $coupon);

        $description = implode(
            ', ',
            (new Sequence($cart->getItems()->toArray()))
                ->map(function (ItemToBuy $item) use ($order): string {
                    $item = clone $item;
                    $order->addItem($item);

                    switch ($item->getType()) {
                        case ItemToBuyTypeEnum::TYPE_ITEM: {
                            $parsedItem = $this->itemParser->parse(Item::createExisting()->setHex($item->getItem()));

                            return implode(' ', array_merge(
                                $parsedItem->getExcellentOptions()->isExcellent()
                                    ? ['Excellent']
                                    : [],
                                $parsedItem->getAncientSet()
                                    ? ['Ancient']
                                    : [],
                                [$parsedItem->getItemInfo()->getName()]
                            ));
                        }
                        case ItemToBuyTypeEnum::TYPE_STATS: {
                            return 'Stats: ' . $item->getStats();
                        }
                        case ItemToBuyTypeEnum::TYPE_WCOIN: {
                            return 'WCoin: ' . $item->getStats();
                        }
                    }

                    return '';
                })
                ->all()
        );

        $order->changeDescription($description);
        $order->changeAmount($price);
        $order->changeCurrency($currency);

        return $order;
    }
}