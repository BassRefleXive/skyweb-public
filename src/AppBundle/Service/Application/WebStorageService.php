<?php

namespace AppBundle\Service\Application;


use AppBundle\Doctrine\Type\Mappings\Items\Item;
use AppBundle\Entity\Application\UserServerAccount;
use AppBundle\Entity\Application\WebStorageItem;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Service\MuOnline\Item\OptionsParserChain;
use AppBundle\Service\MuOnline\WarehouseService;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;

class WebStorageService
{
    private $em;
    private $optionsParser;
    private $logger;

    public function __construct(OptionsParserChain $optionsParser, EntityManager $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->optionsParser = $optionsParser;
        $this->logger = $logger;
    }

    public function remove(UserServerAccount $userServerAccount, WebStorageItem $item): Item
    {
        $parsedItem = $this->validateItem($item);
        $this->logItemAction('Item remove request.', $userServerAccount, $parsedItem, $item->getId());

        $webStorage = $userServerAccount->getWebStorage();
        $webStorage->removeItem($item);

        $this->logItemAction('Item remove start.', $userServerAccount, $parsedItem, $item->getId());

        $this->em->persist($webStorage);
        $this->em->flush($webStorage);

        $this->logItemAction('Item removed.', $userServerAccount, $parsedItem, $item->getId());

        return $parsedItem;
    }

    public function add(UserServerAccount $userServerAccount, Item $item): void
    {
        $this->logItemAction('Item add request.', $userServerAccount, $item);

        $webStorage = $userServerAccount->getWebStorage();

        $webStorage->addItem(
            (new WebStorageItem())->setItem($item->getHex())
        );

        $this->logItemAction('Item add start.', $userServerAccount, $item);

        $this->em->persist($webStorage);
        $this->em->flush($webStorage);

        $this->logItemAction('Item added.', $userServerAccount, $item);
    }

    private function validateItem(WebStorageItem $item): Item
    {
        $webStorageItem = $this->optionsParser->parse(
            Item::createExisting()->setHex($item->getItem())
        );

        if (!$webStorageItem->getGeneralOptions()->getSerialNumber()->compound()) {
            throw new DisplayableException('errors.59');
        }

        return $webStorageItem;
    }

    private function logItemAction(string $action, UserServerAccount $account, Item $item, int $webStorageId = null): void
    {
        $this->logger->info(
            $action,
            array_merge(
                [
                    'user_id' => $account->getUser()->getId(),
                    'user_server_account_id' => $account->getId(),
                    'item' => [
                        'serial' => [
                            'first' => $item->getGeneralOptions()->getSerialNumber()->getFirst(),
                            'second' => $item->getGeneralOptions()->getSerialNumber()->getSecond(),
                            'compound' => $item->getGeneralOptions()->getSerialNumber()->compound(),
                        ],
                        'hex' => $item->getHex(),
                        'name' => $item->getItemInfo()->getName(),
                    ]
                ],
                null !== $webStorageId
                    ? ['id' => $webStorageId]
                    : []
            )
        );
    }
}