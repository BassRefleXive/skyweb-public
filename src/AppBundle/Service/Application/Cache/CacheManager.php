<?php

declare(strict_types = 1);

namespace AppBundle\Service\Application\Cache;


use SensioLabs\AnsiConverter\AnsiToHtmlConverter;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;

class CacheManager
{
    private $kernel;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    public function warmUp()
    {
        $application = $this->bootstrapApplication();

        $input = new ArrayInput([
            'command' => 'cache:warmup',
            '--env' => $this->kernel->getEnvironment(),
        ]);
        $output = $this->createOutput();
        $application->run($input, $output);

        return $output->fetch();
    }

    public function clear()
    {
        $application = $this->bootstrapApplication();

        $input = new ArrayInput([
            'command' => 'cache:clear',
            '--no-warmup' => true,
            '--env' => $this->kernel->getEnvironment(),
        ]);

        $output = $this->createOutput();
        $application->run($input, $output);

        return $output->fetch();
    }

    private function createOutput(): BufferedOutput
    {
        return new BufferedOutput(OutputInterface::VERBOSITY_NORMAL, true);
    }

    private function bootstrapApplication(): Application
    {
        $application = new Application($this->kernel);
        $application->setAutoExit(false);

        return $application;
    }
}