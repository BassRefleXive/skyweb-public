<?php

namespace AppBundle\Service\Application\Cache\Storage;


use AppBundle\Exception\ApplicationException;

use Predis\Client as PredisClient;
use Predis\Response\Status;

class RedisCacheStorage extends AbstractCacheStorage implements CacheStorage
{

    private $client;

    public function __construct(PredisClient $client)
    {
        $this->client = $client;
    }

    public function get(string $key)
    {
        if (!$this->has($key)) {
            throw new ApplicationException(
                sprintf(
                    'No data found in Redis cache by %s key.',
                    $key
                )
            );
        }

        return $this->unserialize($this->client->get($key));
    }

    public function set(string $key, $data, int $ttl = CacheStorage::DEFAULT_TTL): bool
    {
        $data = $this->serialize($data);

        /** @var Status $status */
        $status = $this->client->set($key, $data);
        $this->client->expire($key, $ttl);

        return $status->getPayload() === 'OK';
    }

    public function has(string $key): bool
    {
        return $this->client->get($key) !== null;
    }

}