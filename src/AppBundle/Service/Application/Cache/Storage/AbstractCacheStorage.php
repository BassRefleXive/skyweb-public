<?php

namespace AppBundle\Service\Application\Cache\Storage;


abstract class AbstractCacheStorage implements CacheStorage
{

    protected final function serialize($data): string
    {
        if (is_array($data)) {
            return json_encode($data);
        }

        return serialize($data);
    }

    protected function unserialize(string $data)
    {
        $result = json_decode($data);

        if ($result === null) {
            $result = unserialize($data);
        }

        return $result;
    }

}