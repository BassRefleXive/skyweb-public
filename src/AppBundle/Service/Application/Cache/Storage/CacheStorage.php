<?php

namespace AppBundle\Service\Application\Cache\Storage;

interface CacheStorage
{
    const DEFAULT_TTL = 60;

    public function get(string $key);

    public function set(string $key, $data, int $ttl = CacheStorage::DEFAULT_TTL): bool;

    public function has(string $key): bool;
}