<?php

namespace AppBundle\Service\Application\Cache\Storage;


use AppBundle\Exception\ApplicationException;

class InMemoryCacheStorage extends AbstractCacheStorage implements CacheStorage
{
    private $storage;

    public function __construct()
    {
        $this->storage = [];
    }

    public function get(string $key)
    {
        if (!$this->has($key)) {
            throw new ApplicationException(
                sprintf(
                    'No data found in Redis cache by %s key.',
                    $key
                )
            );
        }

        return $this->unserialize($this->storage[$key]);
    }

    public function set(string $key, $data, int $ttl = CacheStorage::DEFAULT_TTL): bool
    {
        $data = $this->serialize($data);

        $this->storage[$key] = $data;

        return true;
    }

    public function has(string $key): bool
    {
        return isset($this->storage[$key]);
    }
}