<?php

declare(strict_types = 1);

namespace AppBundle\Service\Application\Cache\Provider;


use Doctrine\Common\Cache\CacheProvider;
use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\Common\Cache\RedisCache;

class DoctrineCacheProvider
{
    private $cacheProvider;

    public function __construct(CacheProvider $cacheProvider)
    {
        $this->cacheProvider = $cacheProvider;
    }

    public function getProvider(string $namespacePrefix): CacheProvider
    {
        $provider = clone $this->cacheProvider;

        if (!in_array(get_class($provider), [RedisCache::class, FilesystemCache::class], true)) {
            throw new \RuntimeException('Unknown CacheProvider passed.');
        }

        $provider->setNamespace($namespacePrefix);

        return $provider;
    }
}