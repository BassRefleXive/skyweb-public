<?php

namespace AppBundle\Service\Application;


use AppBundle\Entity\Application\Server;
use AppBundle\Entity\Application\User\User;
use AppBundle\Entity\Application\UserServerAccount;
use AppBundle\Exception\ApplicationException;
use AppBundle\Repository\Application\ServerRepository;
use AppBundle\ServerManager\Connection\Manager;
use AppBundle\ServerManager\Connection\ManagerRegistry;
use Collection\Sequence;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ServerService
{
    private $em;
    private $serverRepo;
    private $container;
    private $sessionService;
    private $managerRegistry;

    public function __construct(
        EntityManager $em,
        ServerRepository $serverRepo,
        ContainerInterface $container,
        SessionService $sessionService,
        ManagerRegistry $managerRegistry
    )
    {
        $this->em = $em;
        $this->serverRepo = $serverRepo;
        $this->container = $container;
        $this->sessionService = $sessionService;
        $this->managerRegistry = $managerRegistry;
    }

    public function getServerForConnection(): \Generator
    {
        foreach ($this->serverRepo->findAll() as $server) {
            yield $server;
        }
    }

    public function getDefaultServer(): Server
    {
        /** @var Server $server */
        $server = $this->serverRepo->findFirst();

        if (!$server) {
            throw new ApplicationException('No default server found in database.');
        }

        return $server;
    }

    public function getList(): Sequence
    {
        return new Sequence($this->serverRepo->findAll());
    }

    public function getCurrentServer(): Server
    {
        if ($hash = $this->sessionService->getServerHash()) {
            return $this->getServerByHash($hash);
        }

        return $this->getDefaultServer();
    }

    public function getCurrentServerDataBaseManager(): EntityManager
    {
        return $this->getManagerFromContainer($this->getCurrentServer()->getHash());
    }

    public function getServerDataBaseManager(Server $server): EntityManager
    {
        return $this->getManagerFromContainer($server->getHash());
    }

    public function serverDirectManager(Server $server = null): Manager
    {
        return null !== $server
            ? $this->managerRegistry->get($server)
            : $this->managerRegistry->get($this->getCurrentServer());
    }

    private function getServerByHash(string $hash): Server
    {
        return (new Sequence($this->serverRepo->findAll()))->find(function (Server $server) use ($hash) {
            return $server->getHash() === $hash;
        })->getOrThrow(new ApplicationException('Cannot obtain server. Not found by hash.'));
    }

    private function getManagerFromContainer(string $hash): EntityManager
    {
        if (!$this->container->has($hash)) {
            throw new ApplicationException('Cannot obtain server manager. Not found in container.');
        }

        /** @var EntityManager $manager */
        $manager = $this->container->get($hash);

        return $manager;
    }

    public function setCurrentServer(Server $server, User $user = null)
    {
        $this->sessionService->setServerHash($server);

        if ($user !== null) {
            $userServerAccount = $user->getUserServerAccountByServer($server);
            if ($userServerAccount) {
                $userServerAccount->setSelectedAt(new \DateTime());

                $this->em->persist($userServerAccount);
                $this->em->flush($userServerAccount);
            }
        }

    }

}