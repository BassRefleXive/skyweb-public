<?php


namespace AppBundle\Service\Application;


use AppBundle\Entity\Application\Server;
use AppBundle\Entity\Application\User\User;
use AppBundle\Entity\Application\UserServerAccount;
use AppBundle\Entity\MuOnline\Account;
use AppBundle\Entity\MuOnline\Character;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Forum\Api\Exception\CreateUserException;
use AppBundle\Forum\Api\Gateway\UsersGateway;
use AppBundle\Mailer\Factory\MailerFactory;
use AppBundle\Model\Application\RequestResetPasswordModel;
use AppBundle\Model\Application\ResetPasswordModel;
use AppBundle\Model\Application\User\ChangePasswordModel;
use AppBundle\Repository\Application\UserRepository;
use AppBundle\User\Event\UserRegisteredEvent;
use Collection\Map;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UserService
{
    private $userRepository;
    private $mailerFactory;
    private $serverService;
    private $usersForumGateway;
    private $eventDispatcher;
    private $resetPasswordInterval;
    private $registrationEnabled;

    public function __construct(
        UserRepository $userRepository,
        MailerFactory $mailerFactory,
        ServerService $serverService,
        UsersGateway $usersForumGateway,
        EventDispatcherInterface $eventDispatcher,
        int $resetPasswordInterval,
        bool $registrationEnabled
    )
    {
        $this->userRepository = $userRepository;
        $this->mailerFactory = $mailerFactory;
        $this->serverService = $serverService;
        $this->usersForumGateway = $usersForumGateway;
        $this->eventDispatcher = $eventDispatcher;
        $this->resetPasswordInterval = $resetPasswordInterval;
        $this->registrationEnabled = $registrationEnabled;
    }

    public function requestResetPassword(RequestResetPasswordModel $model)
    {
        $user = $this->userRepository->findByLoginAndEmail($model->getUsername(), $model->getEmail());

        if (!$user) {
            throw new DisplayableException('errors.41');
        }

        if (
            $user->resetPasswordToken() &&
            $user->resetPasswordDate() &&
            $user->resetPasswordDate()->add(new \DateInterval(sprintf('PT%dM', $this->resetPasswordInterval))) > new \DateTime()
        ) {
            throw new DisplayableException('errors.42');
        }

        $user->resetPassword();

        $this->mailerFactory
            ->createResetPasswordMailer()
            ->setUser($user)
            ->send();

        $this->userRepository->saveUser($user);
    }

    public function resetPassword(string $token, ResetPasswordModel $model): User
    {
        $user = $this->userRepository->findByPasswordResetToken($token);

        $user->finalizePasswordReset($model);

        /** @var UserServerAccount $userServerAccount */
        foreach ($user->getUserServerAccounts() as $userServerAccount) {
            $manager = $this->serverService->getServerDataBaseManager($userServerAccount->getServer());

            $account = $manager->getRepository(Account::class)->find($userServerAccount->getAccountId());

            $account->setPassword($model->getPassword());

            $manager->persist($account);
            $manager->flush($account);
        }

        $this->userRepository->saveUser($user);

        if ($user->isRegisteredInForum()) {
            try {
                $this->usersForumGateway->update($user);
            } catch (CreateUserException $e) {}
        }

        return $user;
    }

    public function register(User $user, string $locale): User
    {
        if (!$this->registrationEnabled) {
            throw new DisplayableException('users.register.temporally_disabled');
        }

        $user->setPassword($user->getPlainPassword());
        $user->setUsername(mb_strtolower($user->getUsername()));
        $user->setLocale($locale);

        $this->userRepository->save($user);

        $this->eventDispatcher->dispatch(
            UserRegisteredEvent::USER_REGISTERED,
            new UserRegisteredEvent($user)
        );

        return $user;
    }

    public function changePassword(User $user, ChangePasswordModel $changePasswordModel): void
    {
        if ($user->getPassword() !== $changePasswordModel->getOld()) {
            throw new DisplayableException('users.change_password.4');
        }


        /** @var UserServerAccount $userServerAccount */
        foreach ($user->getUserServerAccounts() as $userServerAccount) {
            $manager = $this->serverService->getServerDataBaseManager($userServerAccount->getServer());

            $account = $manager->getRepository(Account::class)->find($userServerAccount->getAccountId());

            $account->setPassword($changePasswordModel->getNew());

            $manager->persist($account);
            $manager->flush($account);
        }

        $user->setPassword($changePasswordModel->getNew());

        $this->userRepository->saveUser($user);

        if ($user->isRegisteredInForum()) {
            try {
                $this->usersForumGateway->update($user);
            } catch (CreateUserException $e) {}
        }
    }

    public function getUsersByCharacters(array $characters): Map
    {
        $userNames = array_unique(
            array_map(function (Character $character): string {
                return $character->getAccount()->getMemberId();
            }, $characters)
        );

        $users = $this->userRepository->findByUserNames($userNames);

        $result = new Map();

        /** @var User $user */
        foreach ($users as $user) {
            $result->set(
                $user->getUsername(),
                [
                    'user' => $user,
                    'currentServerAccount' => $user->getUserServerAccountByServer($this->serverService->getCurrentServer())
                ]
            );
        }

        return $result;
    }
}