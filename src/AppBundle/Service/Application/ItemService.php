<?php

namespace AppBundle\Service\Application;


use AppBundle\Doctrine\Type\Mappings\Items\ExcellentOptions;
use AppBundle\Doctrine\Type\Mappings\Items\GeneralOptions;
use AppBundle\Doctrine\Type\Mappings\Items\HarmonyOptions;
use AppBundle\Doctrine\Type\Mappings\Items\Item;
use AppBundle\Doctrine\Type\Mappings\Items\Items;
use AppBundle\Entity\Application\Items\AncientSet;
use AppBundle\Entity\Application\Items\PvPOption;
use AppBundle\Exception\ApplicationException;
use AppBundle\Service\MuOnline\Item\OptionsParserChain;
use Doctrine\ORM\EntityManager;

class ItemService
{

    protected $em;
    protected $optionsParser;
    protected $serverService;

    public function __construct(EntityManager $em, OptionsParserChain $optionsParser, ServerService $serverService)
    {
        $this->em = $em;
        $this->optionsParser = $optionsParser;
        $this->serverService = $serverService;
    }

    public function parse(Item $item): Item
    {
        return $this->optionsParser->parse($item);
    }

    public function create(
        int $itemSize,
        GeneralOptions $generalOptions,
        ExcellentOptions $excellentOptions = null,
        HarmonyOptions $harmonyOptions = null,
        AncientSet $ancientSet = null,
        PvPOption $pvpOption = null
    ): Item
    {
        $generalOptions->getSerialNumber()->setFirst($this->obtainSerial());

        $item = Item::createNew($itemSize)->setGeneralOptions($generalOptions);

        $excellentOptions !== null && $item->setExcellentOptions($excellentOptions);
        $harmonyOptions !== null && $item->setHarmonyOptions($harmonyOptions);
        $ancientSet !== null && $item->setAncientSet($ancientSet);
        $pvpOption !== null && $item->setPVPOption($pvpOption);

        return $this->optionsParser->build($item);
    }

    private function obtainSerial(): int
    {
        $manager = $this->serverService->getCurrentServerDataBaseManager();

        $stmt = $manager->getConnection()->prepare('exec WZ_GetItemSerial');
        $stmt->execute();
        $result = array_values($stmt->fetch());

        if (count($result) !== 1) {
            throw new ApplicationException('Expected that WZ_GetItemSerial will return exactly one result.');
        }

        return (int)$result[0];
    }

}