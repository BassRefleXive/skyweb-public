<?php

declare(strict_types=1);

namespace AppBundle\Service\Application;


use AppBundle\Entity\Application\UserServerAccount;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Exception\Application\User\UserNotFoundException;
use AppBundle\Model\Application\Account\FundsTransferModel;
use AppBundle\Repository\Application\UserRepository;
use AppBundle\Repository\Application\UserServerAccountRepository;
use AppBundle\Service\MuOnline\Funds\CoinsService;
use Psr\Log\LoggerInterface;

class FundsTransferService
{
    private $userServerAccountRepository;
    private $userRepository;
    private $coinsService;
    private $logger;

    public function __construct(
        UserServerAccountRepository $userServerAccountRepository,
        UserRepository $userRepository,
        CoinsService $coinsService,
        LoggerInterface $logger
    )
    {
        $this->userServerAccountRepository = $userServerAccountRepository;
        $this->userRepository = $userRepository;
        $this->coinsService = $coinsService;
        $this->logger = $logger;
    }

    public function transfer(FundsTransferModel $model): void
    {
        try {
            $destination = $this->userRepository->getByDisplayName($model->to());
        } catch (UserNotFoundException $e) {
            throw new DisplayableException('funds_transfer.9');
        }

        switch (true) {
            case $model->currency()->isZen(): {
                $this->transferZen($model, $destination->getUserServerAccountByServer($model->from()->getServer()));

                break;
            }
            case $model->currency()->isWCoin(): {
                $this->transferWCoin($model, $destination->getUserServerAccountByServer($model->from()->getServer()));

                break;
            }
            default: {
                throw new DisplayableException('funds_transfer.11');
            }
        }

        $this->logger->info(
            sprintf(
                'Transferred %d %s from #%d to #%d account. Requested destination: %s.',
                $model->amount(),
                $model->currency()->getName(),
                $model->from()->getId(),
                $destination->getId(),
                $model->to()
            ),
            [
                'amount' => $model->amount(),
                'currency' => $model->currency()->getName(),
                'source' => $model->from()->getId(),
                'destination' => $destination->getId(),
                'display_name' => $model->to(),
            ]
        );
    }

    private function transferZen(FundsTransferModel $model, UserServerAccount $to): void
    {
        if ($model->amount() > $model->from()->zen()) {
            throw new DisplayableException('funds_transfer.10');
        }

        $model->from()->withdrawZen($model->amount());
        $to->depositZen($model->amount());

        $this->userServerAccountRepository->save($model->from());
        $this->userServerAccountRepository->save($to);
    }

    private function transferWCoin(FundsTransferModel $model, UserServerAccount $to): void
    {
        $availableCoins = $this->coinsService->get($model->from()->getUser()->getUsername());

        if ($model->amount() > $availableCoins->wCoinC()) {
            throw new DisplayableException('funds_transfer.10');
        }

        $this->coinsService->sub($model->from()->getUser()->getUsername(), $model->amount(), $model->from()->getServer());
        $this->coinsService->add($to->getUser()->getUsername(), $model->amount(), $model->from()->getServer());
    }
}