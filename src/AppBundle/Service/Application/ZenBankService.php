<?php

declare(strict_types=1);

namespace AppBundle\Service\Application;


use AppBundle\Entity\Application\UserServerAccount;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Model\Application\Account\ZenBankOperationModel;
use AppBundle\Repository\Application\UserServerAccountRepository;
use AppBundle\Service\MuOnline\AccountService;
use AppBundle\Service\MuOnline\WarehouseService;
use Psr\Log\LoggerInterface;

class ZenBankService
{
    private const MAX_WAREHOUSE_ZEN = 2000000000;
    private const ZEN_KK_MULTIPLIER = 1000000;

    private $accountService;
    private $userServerAccountRepository;
    private $warehouseService;
    private $logger;

    public function __construct(
        AccountService $accountService,
        UserServerAccountRepository $userServerAccountRepository,
        WarehouseService $warehouseService,
        LoggerInterface $logger
    )
    {
        $this->accountService = $accountService;
        $this->userServerAccountRepository = $userServerAccountRepository;
        $this->warehouseService = $warehouseService;
        $this->logger = $logger;
    }

    public function processOperation(ZenBankOperationModel $operation, UserServerAccount $userServerAccount): void
    {
        if ($operation->type()->isDeposit()) {
            $this->processDeposit($operation, $userServerAccount);
        } else {
            $this->processWithdrawal($operation, $userServerAccount);
        }

        $this->logger->info(
            sprintf(
                'Performed ZenBank %s operation with amount: %d.',
                $operation->type()->getName(),
                $operation->amount()
            ),
            [
                'type' => $operation->type()->getName(),
                'amount' => $operation->amount(),
            ]
        );
    }

    private function processDeposit(ZenBankOperationModel $operation, UserServerAccount $userServerAccount): void
    {
        $account = $this->accountService->getAccountByUserServerAccount($userServerAccount);
        $storageZen = $account->getWarehouse()->getMoney();

        if ((int) floor($storageZen / self::ZEN_KK_MULTIPLIER) < $operation->amount()) {
            throw new DisplayableException('errors.54');
        }

        $account->getWarehouse()->setMoney($storageZen - $operation->amount() * self::ZEN_KK_MULTIPLIER);
        $this->warehouseService->save($account->getWarehouse());

        $userServerAccount->depositZen($operation->amount());
        $this->userServerAccountRepository->save($userServerAccount);
    }

    private function processWithdrawal(ZenBankOperationModel $operation, UserServerAccount $userServerAccount): void
    {
        if ($userServerAccount->zen() < $operation->amount()) {
            throw new DisplayableException('errors.55');
        }

        $account = $this->accountService->getAccountByUserServerAccount($userServerAccount);

        $storageMoney = $account->getWarehouse()->getMoney();
        $storageResultMoney = $storageMoney + $operation->amount() * self::ZEN_KK_MULTIPLIER;

        if ($storageResultMoney > self::MAX_WAREHOUSE_ZEN) {
            throw new DisplayableException('errors.56');
        }

        $account->getWarehouse()->setMoney($storageResultMoney);
        $this->warehouseService->save($account->getWarehouse());

        $userServerAccount->withdrawZen($operation->amount());
        $this->userServerAccountRepository->save($userServerAccount);
    }
}