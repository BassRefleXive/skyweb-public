<?php


namespace AppBundle\Service\Application;


use AppBundle\Entity\Application\Config\VipLevelConfig;
use AppBundle\Entity\Application\UserServerAccount;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Model\Application\Account\PurchaseVipModel;
use AppBundle\Repository\Application\UserServerAccountRepository;
use AppBundle\Service\MuOnline\Funds\CoinsService;
use AppBundle\Service\MuOnline\VipService as MuOnlineVipService;

class VipService
{
    private $coinsService;
    private $userServerAccountRepository;
    private $muoVipService;


    public function __construct(
        CoinsService $coinsService,
        UserServerAccountRepository $userServerAccountRepository,
        MuOnlineVipService $muoVipService
    )
    {
        $this->coinsService = $coinsService;
        $this->userServerAccountRepository = $userServerAccountRepository;
        $this->muoVipService = $muoVipService;
    }

    public function buy(VipLevelConfig $vipLevel, PurchaseVipModel $model, UserServerAccount $userServerAccount): void {
        $price = $model->dailyRate() * ($model->days() + 1);
        $expectedPrice = $vipLevel->getDailyCoinsPrice() * ($model->days() + 1);

        if ($price !== $expectedPrice) {
            throw new DisplayableException('vip.purchase.2');
        }

        if ($this->coinsService->get($userServerAccount->getUser()->getUsername(), $userServerAccount->getServer())->wCoinC() < $price) {
            throw new DisplayableException('errors.57');
        }

        $this->coinsService->sub(
            $userServerAccount->getUser()->getUsername(),
            $price,
            $userServerAccount->getServer()
        );

        $userServerAccount->addSpentCredits($price);

        $this->activate($vipLevel, $userServerAccount, $model->days() + 1);
    }

    public function activate(VipLevelConfig $vipLevel, UserServerAccount $userServerAccount, int $days): void
    {
        if ($userServerAccount->isVip()) {
            throw new DisplayableException('vip.buy.2');
        }

        $this->muoVipService->activate($vipLevel, $days, $userServerAccount);

        $userServerAccount->activateVip($vipLevel, $days);

        $this->userServerAccountRepository->save($userServerAccount);
    }

    public function extend(VipLevelConfig $vipLevel, PurchaseVipModel $model, UserServerAccount $userServerAccount): void
    {
        if (!$userServerAccount->isVip()) {
            throw new DisplayableException('vip.renew.3');
        }

        $price = $model->dailyRate() * ($model->days() + 1);
        $expectedPrice = ((int)ceil($vipLevel->getDailyCoinsPrice() * (100 - ($userServerAccount->vipDays() >= 30 ? $vipLevel->getRenewalDiscount() : 0))) / 100) * ($model->days() + 1);

        if ($price !== $expectedPrice) {
            throw new DisplayableException('vip.purchase.2');
        }

        if ($this->coinsService->get($userServerAccount->getUser()->getUsername(), $userServerAccount->getServer())->wCoinC() < $price) {
            throw new DisplayableException('errors.57');
        }

        $this->coinsService->sub(
            $userServerAccount->getUser()->getUsername(),
            $price,
            $userServerAccount->getServer()
        );

        $this->muoVipService->extend($vipLevel, $model, $userServerAccount);

        $userServerAccount->extendVip($model->days() + 1);
        $userServerAccount->addSpentCredits($price);

        $this->userServerAccountRepository->save($userServerAccount);
    }

    public function deactivateExpired(): int
    {
        return $this->userServerAccountRepository->deactivateExpiredVIP();
    }
}