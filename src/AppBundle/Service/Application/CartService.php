<?php

namespace AppBundle\Service\Application;


use AppBundle\Doctrine\Type\ItemToBuyTypeEnum;
use AppBundle\Doctrine\Type\Mappings\Items\Item;
use AppBundle\Doctrine\Type\CartPurchaseTypeEnum;
use AppBundle\Entity\Application\Cart;
use AppBundle\Entity\Application\ItemToBuy;
use AppBundle\Entity\Application\User\User;
use AppBundle\Entity\Application\CartDiscountCoupon;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Exception\Application\NeedAccountException;
use AppBundle\Exception\ApplicationException;
use AppBundle\Repository\Application\CartDiscountCouponRepository;
use AppBundle\Service\Application\Payments\PaymentProcessor;
use AppBundle\Service\MuOnline\Funds\CoinsService;
use Doctrine\ORM\EntityManager;

class CartService
{
    private $em;
    private $serverService;
    private $paymentProcessor;
    private $purchaseProcessor;
    private $coinsService;

    public function __construct(
        EntityManager $em,
        ServerService $serverService,
        PaymentProcessor $paymentProcessor,
        PurchaseProcessor $purchaseProcessor,
        CoinsService $coinsService
    )
    {
        $this->em = $em;
        $this->serverService = $serverService;
        $this->paymentProcessor = $paymentProcessor;
        $this->purchaseProcessor = $purchaseProcessor;
        $this->coinsService = $coinsService;
    }

    public function addStats(User $user, int $stats, int $priceCredits, float $priceMoney): Cart
    {
        $cartItem = (new ItemToBuy())
            ->setType(ItemToBuyTypeEnum::TYPE_STATS)
            ->setPriceCredits($priceCredits)
            ->setPriceMoney($priceMoney)
            ->setStats($stats);


        $server = $this->serverService->getCurrentServer();

        if (!$userServerAccount = $user->getUserServerAccountByServer($server)) {
            throw new NeedAccountException('errors.3');
        }

        $cart = $userServerAccount->getCart();

        $cart->addItem($cartItem);

        $this->em->persist($cart);
        $this->em->flush($cart);

        return $cart;
    }

    public function addWCoin(User $user, int $wCoin, float $priceMoney): Cart
    {
        $cartItem = (new ItemToBuy())
            ->setType(ItemToBuyTypeEnum::TYPE_WCOIN)
            ->setPriceCredits($wCoin)
            ->setPriceMoney($priceMoney)
            ->setStats($wCoin);

        $server = $this->serverService->getCurrentServer();

        if (!$userServerAccount = $user->getUserServerAccountByServer($server)) {
            throw new NeedAccountException('errors.3');
        }

        $cart = $userServerAccount->getCart();

        $cart->addItem($cartItem);

        $this->em->persist($cart);
        $this->em->flush($cart);

        return $cart;
    }

    public function addItem(User $user, Item $item, int $priceCredits, float $priceMoney): Cart
    {
        $cartItem = (new ItemToBuy())
            ->setType(ItemToBuyTypeEnum::TYPE_ITEM)
            ->setPriceCredits($priceCredits)
            ->setPriceMoney($priceMoney)
            ->setItem($item->getHex());

        $server = $this->serverService->getCurrentServer();

        if (!$userServerAccount = $user->getUserServerAccountByServer($server)) {
            throw new NeedAccountException('errors.3');
        }

        $cart = $userServerAccount->getCart();

        $cart->addItem($cartItem);

        $this->em->persist($cart);
        $this->em->flush($cart);

        return $cart;
    }

    public function buy(Cart $cart, float $priceCredits, float $priceMoney, string $type, CartDiscountCoupon $coupon = null)
    {
        if (!array_key_exists($type, CartPurchaseTypeEnum::getTypes())) {
            throw new ApplicationException('Unknown buying type');
        }

        switch ($type) {
            case CartPurchaseTypeEnum::PURCHASE_TYPE_CREDITS: {
                $this->buyForTheCredits($cart, $priceCredits, $coupon);

                break;
            }
            case CartPurchaseTypeEnum::PURCHASE_TYPE_MONEY: {
                $order = $this->paymentProcessor->preparePaymentOrder($cart, $priceMoney, $coupon);

                if ($coupon !== null) {
                    $coupon->setStatus(CartDiscountCouponRepository::STATUS_DISABLED);
                    $this->em->persist($coupon);
                    $this->em->flush($coupon);
                }

                $cart->removeItems();
                $this->em->persist($cart);
                $this->em->flush($cart);

                return $order->id();
            }
        }

        return null;
    }

    private function buyForTheCredits(Cart $cart, float $priceCredits, CartDiscountCoupon $coupon = null)
    {
        $coins = $this->coinsService->get($cart->getUserServerAccount()->getAccountId());

        if ($priceCredits > $coins->wCoinC()) {
            throw new DisplayableException('errors.14');
        }

        $this->coinsService->set($cart->getUserServerAccount()->getAccountId(), $coins->wCoinC() - $priceCredits);

        $this->purchaseProcessor->assignCartItems($cart, $priceCredits, $coupon);
    }
}