<?php

namespace AppBundle\Service\Application;


use AppBundle\Entity\Application\CartDiscountCoupon;
use AppBundle\Repository\Application\CartDiscountCouponRepository;
use Doctrine\ORM\EntityManager;

class CartDiscountCouponService
{
    protected $cartDiscountCouponRepo;

    public function __construct(CartDiscountCouponRepository $cartDiscountCouponRepo)
    {
        $this->cartDiscountCouponRepo = $cartDiscountCouponRepo;
    }

    public function getUniqueCoupon(): string
    {
        do {
            $str = $this->generateRandomString(5);
        } while ($this->cartDiscountCouponRepo->findByText($str) !== null);

        return $str;
    }

    private function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

}