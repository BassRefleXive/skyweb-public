<?php

declare(strict_types = 1);

namespace AppBundle\Service\Application;


use Symfony\Component\Translation\Translator;

class PartialTranslatorService
{
    private $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    public function translateWebShopCategory(string $name): string
    {
        return $this->translate($name, 'web_shop.categories.');
    }

    public function translateMarketCategory(string $name): string
    {
        return $this->translate($name, 'market.categories.');
    }

    private function translate(string $name, string $prefix): string
    {
        $locale = $this->translator->getLocale();
        $catalogue = $this->translator->getCatalogue($locale);

        $label = $prefix . $name;

        if ($catalogue->defines($label)) {
            return $this->translator->trans($label);
        }

        return $name;
    }
}