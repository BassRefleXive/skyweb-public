<?php


namespace AppBundle\Service\Application\Votes\Processor;


use AppBundle\Collection\Votes\VoteCollection;
use AppBundle\Entity\Application\Server;
use AppBundle\Entity\Application\User\User;
use AppBundle\Entity\Application\Vote;
use AppBundle\Entity\Application\VoteRewardConfig;
use AppBundle\Entity\MuOnline\Account;
use AppBundle\Enum\Application\VoteProvider;
use AppBundle\Enum\Application\VoteType;
use AppBundle\Model\Application\Votes\Builder\VoteModelBuilder;
use AppBundle\Model\Application\Votes\VoteModel;
use AppBundle\Repository\Application\UserRepository;
use AppBundle\Repository\Application\VoteRepository;
use AppBundle\Service\Application\Votes\VoterService;
use AppBundle\Service\MuOnline\AccountService;
use AppBundle\Service\MuOnline\Funds\CoinsService;
use GuzzleHttp\Client;

abstract class VotesProcessor
{
    private $accountService;
    private $coinsService;
    private $voteRepository;
    private $userRepository;
    private $restClient;
    private $voterService;

    public function __construct(
        AccountService $accountService,
        CoinsService $coinsService,
        Client $restClient,
        VoteRepository $voteRepository,
        UserRepository $userRepository,
        VoterService $voterService
    )
    {
        $this->accountService = $accountService;
        $this->coinsService = $coinsService;
        $this->restClient = $restClient;
        $this->voteRepository = $voteRepository;
        $this->userRepository = $userRepository;
        $this->voterService = $voterService;
    }

    public function process(Server $server)
    {
        $votesFromRemote = $this->downloadServerVotes($server);

        $votesInDb = $this->voteRepository->findByCollection($votesFromRemote);

        $votesInDb->map(function (Vote $vote) use (&$votesFromRemote) {
            $votesFromRemote->remove($vote->providerId());
        });

        $usersByVotes = $this->userRepository->findByVoteCollection($votesFromRemote);

        $votesFromRemote
            ->map(function ($idx, VoteModel $model) use ($usersByVotes, $server) {

                $user = $usersByVotes
                    ->find(function (User $user) use ($model) {
                        return $user->getUsername() === $model->name();
                    })
                    ->getOrElse(null);

                if (!$user instanceof User) {
                    return;
                }

                $vote = (new Vote())
                    ->setVoter($user->getUserServerAccountByServer($server))
                    ->setDateTime($model->dateTime())
                    ->setIp($model->ip())
                    ->setProviderId($model->id())
                    ->setType($model->type())
                    ->setProvider($this->provider())
                ;

                $this->voterService->addNewVote($vote);
            });
    }

    private function downloadServerVotes(Server $server): VoteCollection
    {
        $url = $this->voteFileUrl($server->getVoteRewardConfig());

        if (null === $url || '' === $url) {
            throw new \RuntimeException('Cannot download server votes. No config or url in config.');
        }

        $data = $this->restClient->get($url)->getBody()->getContents();

        return (new VoteCollection())->buildFromString($data, $this->voteBuilder());
    }

    abstract protected function voteFileUrl(VoteRewardConfig $config): string;

    abstract protected function voteBuilder(): VoteModelBuilder;

    abstract protected function provider(): VoteProvider;
}