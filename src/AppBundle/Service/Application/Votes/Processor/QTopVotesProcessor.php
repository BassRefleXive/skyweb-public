<?php


namespace AppBundle\Service\Application\Votes\Processor;


use AppBundle\Entity\Application\VoteRewardConfig;
use AppBundle\Enum\Application\VoteProvider;
use AppBundle\Enum\Application\VoteType;
use AppBundle\Model\Application\Votes\Builder\QTopVoteModelBuilder;
use AppBundle\Model\Application\Votes\Builder\VoteModelBuilder;

class QTopVotesProcessor extends VotesProcessor
{
    protected function voteFileUrl(VoteRewardConfig $config): string
    {
        return $config->getQtopVotesFileUrl();
    }

    protected function voteBuilder(): VoteModelBuilder
    {
        return new QTopVoteModelBuilder();
    }

    protected function provider(): VoteProvider
    {
        return VoteProvider::byValue(VoteProvider::QTOP);
    }
}