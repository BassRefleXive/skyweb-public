<?php

declare(strict_types=1);

namespace AppBundle\Service\Application\Votes\Processor\TopServers200\Api\Response;

use AppBundle\Service\Application\Votes\Processor\TopServers200\Api\Enum\VoteStatus;

class Vote
{
    private $id;
    private $siteId;
    private $ip;
    private $date;
    private $referrer;
    private $status;

    public function __construct(
        int $id,
        int $siteId,
        string $ip,
        \DateTimeImmutable $date,
        string $referrer,
        VoteStatus $status
    )
    {
        $this->id = $id;
        $this->siteId = $siteId;
        $this->ip = $ip;
        $this->date = $date;
        $this->referrer = $referrer;
        $this->status = $status;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function siteId(): int
    {
        return $this->siteId;
    }

    public function ip(): string
    {
        return $this->ip;
    }

    public function date(): \DateTimeImmutable
    {
        return $this->date;
    }

    public function referrer(): string
    {
        return $this->referrer;
    }

    public function status(): VoteStatus
    {
        return $this->status;
    }
}