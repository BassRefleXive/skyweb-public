<?php

declare(strict_types=1);

namespace AppBundle\Service\Application\Votes\Processor\TopServers200\Api\Criteria;

use AppBundle\Http\Criteria\UriCriteria as BaseUriCriteria;
use AppBundle\Http\Criteria\UriCriteriaInterface;

final class UriCriteria extends BaseUriCriteria implements UriCriteriaInterface
{
    public static function checkIp(string $ip): self
    {
        return new self(
            '/api/vote',
            [],
            [
                'ip' => $ip,
            ]
        );
    }
}