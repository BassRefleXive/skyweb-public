<?php

declare(strict_types=1);

namespace AppBundle\Service\Application\Votes\Processor\TopServers200\Api;

use AppBundle\Http\Assembler\RequestAssembler;
use AppBundle\Http\Criteria\RequestCriteria;
use AppBundle\Http\HttpClientInterface;
use AppBundle\Http\Response\XmlResponseParser;
use AppBundle\Service\Application\Votes\Processor\TopServers200\Api\Criteria\UriCriteria;
use AppBundle\Service\Application\Votes\Processor\TopServers200\Api\Factory\VoteFactory;
use AppBundle\Service\Application\Votes\Processor\TopServers200\Api\Response\Vote;
use Symfony\Component\HttpFoundation\Request;

class TopServer200Api
{
    private $client;
    private $requestAssembler;
    private $responseParser;
    private $voteFactory;

    public function __construct(HttpClientInterface $client, RequestAssembler $requestAssembler)
    {
        $this->client = $client;
        $this->requestAssembler = $requestAssembler;
        $this->responseParser = new XmlResponseParser();
        $this->voteFactory = new VoteFactory();
    }

    public function checkIp(string $ip): Vote
    {
        $requestCriteria = new RequestCriteria(
            UriCriteria::checkIp($ip),
            Request::METHOD_GET
        );

        $response = $this->client->send($this->requestAssembler->fromCriteria($requestCriteria));

        $array = $this->responseParser->responseArray($response);

        if (count($array) > 1) {
            print_r([
                $array,
                count($array),
            ]);exit;
        }

        return $this->voteFactory->fromArray($array);
    }
}