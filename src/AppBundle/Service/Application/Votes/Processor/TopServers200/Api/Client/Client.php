<?php

declare(strict_types=1);

namespace AppBundle\Service\Application\Votes\Processor\TopServers200\Api\Client;

use AppBundle\Http\GuzzleHttp\Client as BaseClient;

class Client extends BaseClient
{
}