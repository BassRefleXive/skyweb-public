<?php

declare(strict_types=1);

namespace AppBundle\Service\Application\Votes\Processor\TopServers200\Api\Exception;

use AppBundle\Http\Exception\ResponseException as BaseResponseException;

class ResponseException extends BaseResponseException
{
}