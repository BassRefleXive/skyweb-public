<?php

declare(strict_types=1);

namespace AppBundle\Service\Application\Votes\Processor\TopServers200\Api\Factory;

use AppBundle\Http\Factory\UriFactory as BaseUriFactory;

class UriFactory extends BaseUriFactory
{
}