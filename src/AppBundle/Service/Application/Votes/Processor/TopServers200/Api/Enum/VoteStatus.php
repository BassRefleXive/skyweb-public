<?php

declare(strict_types=1);

namespace AppBundle\Service\Application\Votes\Processor\TopServers200\Api\Enum;

use MabeEnum\Enum;

final class VoteStatus extends Enum
{
    public const SUCCESS = 0;
    public const FAILURE = 1;

    public function isSuccess(): bool
    {
        return $this->is(self::SUCCESS);
    }

    public function isFailure(): bool
    {
        return $this->is(self::FAILURE);
    }
}