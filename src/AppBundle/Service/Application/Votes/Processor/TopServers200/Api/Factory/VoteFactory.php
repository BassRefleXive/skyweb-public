<?php

declare(strict_types=1);

namespace AppBundle\Service\Application\Votes\Processor\TopServers200\Api\Factory;

use AppBundle\Service\Application\Votes\Processor\TopServers200\Api\Enum\VoteStatus;
use AppBundle\Service\Application\Votes\Processor\TopServers200\Api\Response\Vote;

class VoteFactory
{
    public function fromArray(array $data): Vote
    {
        $data = $data['vote'];

        return new Vote(
            (int) $data['vote_id'],
            (int) $data['site_id'],
            $data['ip'],
            false !== strtotime($data['date'])
            ? new \DateTimeImmutable($data['date'])
            : new \DateTimeImmutable('1970-01-01 00:00:00'),
            $data['referrer'],
            VoteStatus::byValue((int) $data['status'])
        );
    }
}