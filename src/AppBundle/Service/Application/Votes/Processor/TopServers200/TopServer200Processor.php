<?php

declare(strict_types=1);

namespace AppBundle\Service\Application\Votes\Processor\TopServers200;

use AppBundle\Entity\Application\User\User;
use AppBundle\Entity\Application\Vote;
use AppBundle\Enum\Application\VoteProvider;
use AppBundle\Enum\Application\VoteType;
use AppBundle\Exception\Application\Vote\VoteNotFoundException;
use AppBundle\Repository\Application\AuthLogRepository;
use AppBundle\Repository\Application\UserRepository;
use AppBundle\Repository\Application\VoteRepository;
use AppBundle\Service\Application\Votes\Processor\TopServers200\Api\TopServer200Api;
use AppBundle\Service\Application\Votes\VoterService;
use Psr\Log\LoggerInterface;

class TopServer200Processor
{
    private $userRepository;
    private $authLogRepository;
    private $voteRepository;
    private $topServer200Api;
    private $voterService;
    private $logger;

    public function __construct(
        UserRepository $userRepository,
        AuthLogRepository $authLogRepository,
        VoteRepository $voteRepository,
        TopServer200Api $topServer200Api,
        VoterService $voterService,
        LoggerInterface $logger
    )
    {
        $this->userRepository = $userRepository;
        $this->authLogRepository = $authLogRepository;
        $this->voteRepository = $voteRepository;
        $this->topServer200Api = $topServer200Api;
        $this->voterService = $voterService;
        $this->logger = $logger;
    }

    public function process(): void
    {
        /** @var User[] $users */
        $users = $this->userRepository->findAll();

        $requests = 0;

        foreach ($users as $user) {
            $auths = $this->authLogRepository->findUserSuccessfulInPeriod($user->getUsername());

            $this->logger->info('User {username} auths count {count}.', [
                'username' => $user->getUsername(),
                'count' => count($auths),
            ]);

            foreach ($auths as $authLog) {
                try {
                    $this->voteRepository->getProviderVoteByIpInPeriod(
                        $authLog->stringIp(),
                        VoteProvider::byValue(VoteProvider::TOPSERVERS200),
                        (new \DateTimeImmutable())->sub(new \DateInterval('PT12H')),
                        new \DateTimeImmutable()
                    );

                    continue;
                } catch (VoteNotFoundException $e) {
                }

                $vote = $this->topServer200Api->checkIp($authLog->stringIp());

                if (!$vote->status()->isSuccess()) {
                    continue;
                }

                $this->logger->info('User {username} last voted with IP {ip_our} {date}. Vote id: {vote_id}. Site id: {site_id}.', [
                    'username' => $user->getUsername(),
                    'ip_our' => $authLog->stringIp(),
                    'ip_their' => $vote->ip(),
                    'date' => $vote->date()->format('Y-m-d H:i:s'),
                    'vote_id' => $vote->id(),
                    'site_id' => $vote->siteId(),
                ]);

                try {
                    $this->voteRepository->getProviderVoteById($vote->id(), VoteProvider::byValue(VoteProvider::TOPSERVERS200));

                    continue;
                } catch (VoteNotFoundException $e) {
                    if ($vote->siteId() !== $user->getLastUserServerAccount()->getServer()->getVoteRewardConfig()->getTopServers200SiteId()) {
                        continue;
                    }

                    $this->logger->info('User {username} vote {vote_id} with IP {ip_our} is new. Processing.', [
                        'username' => $user->getUsername(),
                        'ip_our' => $authLog->stringIp(),
                        'ip_their' => $vote->ip(),
                        'date' => $vote->date()->format('Y-m-d H:i:s'),
                        'vote_id' => $vote->id(),
                    ]);
                }

                $voteModel = (new Vote())
                    ->setVoter($user->getLastUserServerAccount())
                    ->setDateTime(new \DateTime())
                    ->setIp($vote->ip())
                    ->setProviderId($vote->id())
                    ->setType(VoteType::byValue(VoteType::REGULAR))
                    ->setProvider(VoteProvider::byValue(VoteProvider::TOPSERVERS200));

                $this->logger->info('Created vote object for crediting to "{username}" for vote with IP "{ip}" on top "{top}".', [
                    'username' => $authLog->login(),
                    'ip' => $voteModel->getIp(),
                    'top' => $voteModel->provider()->getName(),
                ]);

                $this->voterService->addNewVote($voteModel);

                $this->logger->info('Vote #{id} for "{username}" for vote with IP "{ip}" on top "{top}" successfully added and reward credited.', [
                    'id' => $voteModel->getId(),
                    'username' => $authLog->login(),
                    'ip' => $voteModel->getIp(),
                    'top' => $voteModel->provider()->getName(),
                ]);
            }

            $requests += count($auths);
        }

        $this->logger->info('Total performed {count} API requests.', [
            'count' => $requests,
        ]);
    }
}