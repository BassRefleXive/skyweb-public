<?php


namespace AppBundle\Service\Application\Votes\Processor;


use AppBundle\Entity\Application\VoteRewardConfig;
use AppBundle\Enum\Application\VoteProvider;
use AppBundle\Enum\Application\VoteType;
use AppBundle\Model\Application\Votes\Builder\MmoTopVoteModelBuilder;
use AppBundle\Model\Application\Votes\Builder\VoteModelBuilder;

class MmoTopVotesProcessor extends VotesProcessor
{
    protected function voteFileUrl(VoteRewardConfig $config): string
    {
        return $config->getMmotopVotesFileUrl();
    }

    protected function voteBuilder(): VoteModelBuilder
    {
        return new MmoTopVoteModelBuilder();
    }

    protected function provider(): VoteProvider
    {
        return VoteProvider::byValue(VoteProvider::MMOTOP);
    }
}