<?php


namespace AppBundle\Service\Application\Votes;


use AppBundle\Entity\Application\Server;
use AppBundle\Entity\Application\TopVoter;
use AppBundle\Entity\Application\UserServerAccount;
use AppBundle\Entity\Application\Vote;
use AppBundle\Entity\MuOnline\Account;
use AppBundle\Filter\VoterFilter;
use AppBundle\Repository\Application\TopVoterRepository;
use AppBundle\Repository\Application\UserServerAccountRepository;
use AppBundle\Repository\Application\VoteRepository;
use AppBundle\Service\MuOnline\AccountService;
use AppBundle\Service\MuOnline\Funds\CoinsService;

class VoterService
{
    private $userServerAccountRepository;
    private $topVoterRepo;
    private $accountService;
    private $coinsService;
    private $voteRepository;

    public function __construct(
        UserServerAccountRepository $userServerAccountRepository,
        TopVoterRepository $topVoterRepo,
        AccountService $accountService,
        CoinsService $coinsService,
        VoteRepository $voteRepository
    )
    {
        $this->userServerAccountRepository = $userServerAccountRepository;
        $this->topVoterRepo = $topVoterRepo;
        $this->accountService = $accountService;
        $this->coinsService = $coinsService;
        $this->voteRepository = $voteRepository;
    }

    public function getLastMonthTopVotersByServer(Server $server): array
    {
        return $this->userServerAccountRepository->findLastMonthTopVotersByServer($server);
    }

    public function processMonthlyVotersTop(Server $server): void
    {
        $periodStart = new \DateTime(date('Y-m-d 00:00:00', strtotime('first day of last month')));
        $periodEnd = new \DateTime(date('Y-m-d 23:59:59', strtotime('last day of last month')));

        $sortedVoters = (new VoterFilter(1))
            ->withServer($server)
            ->setCount(1000)
            ->withStartDate($periodStart)
            ->withEndDate($periodEnd);

        $sortedVoters = $this->userServerAccountRepository->findByQueryFilter($sortedVoters);

        $firstPlaces = [];

        $votesCount = 0;

        foreach ($sortedVoters as $voter) {
            if (3 === count($firstPlaces) && $votesCount !== $voter['regularVotes'] + $voter['smsVotes']) {
                break;
            }

            $votesCount = $voter['regularVotes'] + $voter['smsVotes'];

            $firstPlaces[$votesCount][] = $voter;
        }

        $place = 1;

        foreach ($firstPlaces as $votesCount => $winners) {
            $reward = 0;

            switch ($place) {
                case 1: {
                    $reward = $server->getVoteRewardConfig()->getRewardMonthTop1();
                    break;
                }
                case 2: {
                    $reward = $server->getVoteRewardConfig()->getRewardMonthTop2();
                    break;
                }
                case 3: {
                    $reward = $server->getVoteRewardConfig()->getRewardMonthTop3();
                    break;
                }
            }

            if ($reward <= 0) {
                break;
            }

            $reward = round($reward / count($winners));

            foreach ($winners as $winner) {
                if (!isset($winner['entity'])) {
                    throw new \RuntimeException('Entity not found, cannot update top voters.');
                }
                /** @var UserServerAccount $userServerAccount */
                $userServerAccount = $winner['entity'];

                /** @var Account $account */
                $account = $this->accountService->getAccountByUserServerAccount($userServerAccount, $server);

                $this->coinsService->add($account->getMemberId(), $reward, $server);

                $topVoter = (new TopVoter())
                    ->setRegularVotes($winner['regularVotes'])
                    ->setSmsVotes($winner['smsVotes'])
                    ->setPlace($place)
                    ->setReward($reward)
                    ->setUserServerAccount($userServerAccount)
                    ->setPeriod($periodStart, $periodEnd)
                ;

                $this->topVoterRepo->save($topVoter);
            }

            ++$place;
        }
    }

    public function addNewVote(Vote $vote)
    {
        $server = $vote->getVoter()->getServer();

        $voteConfig = $server->getVoteRewardConfig();

        if (!$voteConfig) {
            throw new \RuntimeException('Cannot add vote. No config.');
        }

        /** @var Account $account */
        $account = $this->accountService->getAccountByUserServerAccount($vote->getVoter(), $server);

        if (!$account) {
            throw new \RuntimeException(sprintf(
                'Cannot add vote. User %s (id: %d) does not have account on server %s (id: %s).',
                $vote->getVoter()->getUser()->getUsername(),
                $vote->getVoter()->getUser()->getId(),
                $server->getName(),
                $server->getId()
            ));
        }

        $this->coinsService->add(
            $account->getMemberId(),
            $vote->provider()->reward($vote->getType(), $voteConfig),
            $server
        );

        $this->voteRepository->save($vote);
    }
}