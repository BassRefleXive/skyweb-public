<?php

declare(strict_types=1);

namespace AppBundle\Service\Application\Votes\NotificationHandler\Provider\TopG\Factory;

use AppBundle\Enum\Application\VoteProvider;
use AppBundle\Enum\Application\VoteType;
use AppBundle\Service\Application\Votes\NotificationHandler\Model\Request;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class RequestFactory
{
    final public static function build(SymfonyRequest $request): Request
    {
        return new Request(
            $request->query->get('ip'),
            new \DateTimeImmutable(),
            VoteType::byValue(VoteType::REGULAR),
            VoteProvider::byValue(VoteProvider::TOPG)
        );
    }
}