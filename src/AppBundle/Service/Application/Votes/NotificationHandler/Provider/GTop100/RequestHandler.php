<?php

declare(strict_types=1);

namespace AppBundle\Service\Application\Votes\NotificationHandler\Provider\GTop100;

use AppBundle\Service\Application\Votes\NotificationHandler\Model\Request as BaseRequest;
use AppBundle\Service\Application\Votes\NotificationHandler\Provider\GTop100\Model\Request;
use AppBundle\Service\Application\Votes\NotificationHandler\Service\RequestHandler as BaseRequestHandler;

class RequestHandler extends BaseRequestHandler
{
    /**
     * @param Request $request
     */
    public function handle(BaseRequest $request): void
    {
        if (!$request->isSuccess()) {
            $this->logger->notice('Received not successful voting notification. Reason: {reason}. Aborting.', [
                'reason' => $request->reason(),
            ]);

            return;
        }

        parent::handle($request);
    }
}