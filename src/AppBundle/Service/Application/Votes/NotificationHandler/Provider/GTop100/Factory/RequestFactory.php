<?php

declare(strict_types=1);

namespace AppBundle\Service\Application\Votes\NotificationHandler\Provider\GTop100\Factory;

use AppBundle\Enum\Application\VoteProvider;
use AppBundle\Enum\Application\VoteType;
use AppBundle\Service\Application\Votes\NotificationHandler\Provider\GTop100\Model\Request;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class RequestFactory
{
    final public static function build(SymfonyRequest $request): Request
    {
        return new Request(
            $request->request->get('VoterIP'),
            new \DateTimeImmutable(),
            VoteType::byValue(VoteType::REGULAR),
            VoteProvider::byValue(VoteProvider::GTOP100),
            0 === ((int) $request->request->get('Successful')),
            $request->request->get('Reason', '')
        );
    }
}