<?php

declare(strict_types=1);

namespace AppBundle\Service\Application\Votes\NotificationHandler\Provider\GTop100\Model;

use AppBundle\Enum\Application\VoteProvider;
use AppBundle\Enum\Application\VoteType;
use AppBundle\Service\Application\Votes\NotificationHandler\Model\Request as BaseRequest;

class Request extends BaseRequest
{
    private $isSuccess;
    private $reason;

    public function __construct(string $ip, \DateTimeImmutable $date, VoteType $type, VoteProvider $provider, bool $isSuccess, string $reason)
    {
        parent::__construct($ip, $date, $type, $provider);

        $this->isSuccess = $isSuccess;
        $this->reason = $reason;
    }

    public function isSuccess(): bool
    {
        return $this->isSuccess;
    }

    public function reason(): string
    {
        return $this->reason;
    }
}