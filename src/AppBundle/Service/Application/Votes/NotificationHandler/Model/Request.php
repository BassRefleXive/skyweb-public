<?php

declare(strict_types=1);

namespace AppBundle\Service\Application\Votes\NotificationHandler\Model;

use AppBundle\Enum\Application\VoteProvider;
use AppBundle\Enum\Application\VoteType;

class Request
{
    private $ip;
    private $date;
    private $type;
    private $provider;

    public function __construct(string $ip, \DateTimeImmutable $date, VoteType $type, VoteProvider $provider)
    {
        $this->ip = $ip;
        $this->date = $date;
        $this->type = $type;
        $this->provider = $provider;
    }

    public function ip(): string
    {
        return $this->ip;
    }

    public function numericIp(): int
    {
        return ip2long($this->ip());
    }

    public function date(): \DateTimeImmutable
    {
        return $this->date;
    }

    public function type(): VoteType
    {
        return $this->type;
    }

    public function provider(): VoteProvider
    {
        return $this->provider;
    }

    /**
     * @ToDo Can be implemented for further integrated tops
     */
    public function providerId(): int
    {
        return 0;
    }
}