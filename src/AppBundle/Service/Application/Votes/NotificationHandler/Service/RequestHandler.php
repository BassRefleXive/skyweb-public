<?php

declare(strict_types=1);

namespace AppBundle\Service\Application\Votes\NotificationHandler\Service;

use AppBundle\Entity\Application\Vote;
use AppBundle\Exception\Application\AuthLog\AuthLogNotFoundException;
use AppBundle\Exception\Application\Vote\VoteNotFoundException;
use AppBundle\Repository\Application\AuthLogRepository;
use AppBundle\Repository\Application\UserRepository;
use AppBundle\Repository\Application\VoteRepository;
use AppBundle\Service\Application\Votes\NotificationHandler\Model\Request;
use AppBundle\Service\Application\Votes\VoterService;
use Psr\Log\LoggerInterface;

class RequestHandler
{
    private $voteRepository;
    private $authLogRepository;
    private $userRepository;
    private $voterService;
    protected $logger;

    public function __construct(
        VoteRepository $voteRepository,
        AuthLogRepository $authLogRepository,
        UserRepository $userRepository,
        VoterService $voterService,
        LoggerInterface $logger
    )
    {
        $this->voteRepository = $voteRepository;
        $this->authLogRepository = $authLogRepository;
        $this->userRepository = $userRepository;
        $this->voterService = $voterService;
        $this->logger = $logger;
    }

    public function handle(Request $request): void
    {
        try {
            $vote = $this->voteRepository->getProviderVoteByIpInPeriod(
                $request->ip(),
                $request->provider(),
                (clone $request->date())->sub(new \DateInterval('PT12H')),
                $request->date()
            );

            $this->logger->info('Found previous vote made with IP "{ip}" on top "{top}" at "{date}". Skipping vote.', [
                'ip' => $request->ip(),
                'top' => $request->provider()->getName(),
                'date' => $vote->getDateTime()->format('Y-m-d H:i:s'),
            ]);

            return;
        } catch (VoteNotFoundException $e) {
            $this->logger->info('No previous previous votes made with IP "{ip}" on top "{top}" found. Processing vote.', [
                'ip' => $request->ip(),
                'top' => $request->provider()->getName(),
            ]);
        }

        try {
            $authLog = $this->authLogRepository->getLastSuccessfulForIp($request->numericIp());

            $this->logger->info('Found user "{username}" who logged in with IP "{ip}" while processing vote on top "{top}". Processing vote.', [
                'username' => $authLog->login(),
                'ip' => $request->ip(),
                'top' => $request->provider()->getName(),
                'authLogId' => $authLog->id(),
            ]);

            $vote = (new Vote())
                ->setVoter($this->userRepository->getByLogin($authLog->login())->getLastUserServerAccount())
                ->setDateTime($request->date())
                ->setIp($request->ip())
                ->setProviderId($request->providerId())
                ->setType($request->type())
                ->setProvider($request->provider());

            $this->logger->info('Created vote object for crediting to "{username}" for vote with IP "{ip}" on top "{top}".', [
                'username' => $authLog->login(),
                'ip' => $vote->getIp(),
                'top' => $vote->provider()->getName(),
            ]);

            $this->voterService->addNewVote($vote);

            $this->logger->info('Vote #{id} for "{username}" for vote with IP "{ip}" on top "{top}" successfully added and reward credited.', [
                'id' => $vote->getId(),
                'username' => $authLog->login(),
                'ip' => $vote->getIp(),
                'top' => $vote->provider()->getName(),
            ]);
        } catch (AuthLogNotFoundException $e) {
            $this->logger->info('No users who logged in with IP "{ip}" found while processing vote on top "{top}". Skipping vote.', [
                'ip' => $request->ip(),
                'top' => $request->provider()->getName(),
            ]);

            return;
        }
    }
}