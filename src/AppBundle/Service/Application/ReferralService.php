<?php

namespace AppBundle\Service\Application;


use AppBundle\Entity\Application\ReferralConfig;
use AppBundle\Entity\Application\ReferralReward;
use AppBundle\Entity\Application\UserServerAccount;
use AppBundle\Entity\MuOnline\Account;
use AppBundle\Entity\MuOnline\Character;
use AppBundle\Exception\Application\RedirectBackException;
use AppBundle\Service\MuOnline\AccountService;
use AppBundle\Service\MuOnline\Funds\CoinsService;
use Collection\Sequence;
use Doctrine\ORM\EntityManager;

class ReferralService
{
    private $em;
    private $serverService;
    private $accountService;
    private $coinsService;

    public function __construct(
        EntityManager $em,
        ServerService $serverService,
        AccountService $accountService,
        CoinsService $coinsService
    ) {
        $this->em = $em;
        $this->serverService = $serverService;
        $this->accountService = $accountService;
        $this->coinsService = $coinsService;
    }

    public function getReward(UserServerAccount $receiver, UserServerAccount $referred, int $reward)
    {
        $server = $this->serverService->getCurrentServer();

        $referralRewardConfig = $server->getReferralConfig();

        $referredAccount = $this->accountService->getAccountByUserServerAccount($referred);

        if (!$this->isAccountHasEnoughOnlineTime($referralRewardConfig, $referredAccount)) {
            throw new RedirectBackException('errors.30');
        }
        if (!$this->isAccountHasCharacterWithEnoughResets($referralRewardConfig, $referredAccount)) {
            throw new RedirectBackException('errors.32');
        }
        if (!$this->isAccountHasCharacterWithEnoughGrandResets($referralRewardConfig, $referredAccount)) {
            throw new RedirectBackException('errors.34');
        }
        if (!$this->isAccountHasEnoughVotes($referralRewardConfig, $referred)) {
            throw new RedirectBackException('errors.36');
        }

        $receiverAccount = $this->accountService->getAccountByUserServerAccount($receiver);

        if (!$this->isAccountHasEnoughOnlineTime($referralRewardConfig, $receiverAccount)) {
            throw new RedirectBackException('errors.31');
        }
        if (!$this->isAccountHasCharacterWithEnoughResets($referralRewardConfig, $receiverAccount)) {
            throw new RedirectBackException('errors.33');
        }
        if (!$this->isAccountHasCharacterWithEnoughGrandResets($referralRewardConfig, $receiverAccount)) {
            throw new RedirectBackException('errors.35');
        }
        if (!$this->isAccountHasEnoughVotes($referralRewardConfig, $receiver)) {
            throw new RedirectBackException('errors.37');
        }

        $referralReward = (new ReferralReward())
            ->setReferral($receiver)
            ->setReferred($referred)
            ->setReward($reward);

        $this->coinsService->add($receiverAccount->getMemberId(), $reward);

        $this->em->persist($referralReward);
        $this->em->flush($referralReward);
    }

    private function isAccountHasEnoughOnlineTime(ReferralConfig $config, Account $account): bool
    {
        return $account->getState()->getTotalSpentTime() >= $config->getRequiredOnlineTime();
    }

    private function isAccountHasCharacterWithEnoughResets(ReferralConfig $config, Account $account)
    {
        return (new Sequence($account->getCharacters()->toArray()))
            ->find(function (Character $character) use ($config) {
                return $character->getReset() >= $config->getRequiredReset();
            })
            ->getOrElse(false);
    }

    private function isAccountHasCharacterWithEnoughGrandResets(ReferralConfig $config, Account $account)
    {
        return (new Sequence($account->getCharacters()->toArray()))
            ->find(function (Character $character) use ($config) {
                return $character->getGrandReset() >= $config->getRequiredGrandReset();
            })
            ->getOrElse(false);
    }

    private function isAccountHasEnoughVotes(ReferralConfig $config, UserServerAccount $userServerAccount)
    {
        return $userServerAccount->getVotes()->count() >= $config->getRequiredVotes();
    }

}