<?php


namespace AppBundle\Service\Application;


use AppBundle\Mailer\Factory\MailerFactory;
use AppBundle\Model\Application\ContactRequestModel;
use AppBundle\Slack\Services\UserEventNotificationService;

class ContactService
{
    private $mailerFactory;
    private $eventNotificationService;

    public function __construct(MailerFactory $mailerFactory, UserEventNotificationService $eventNotificationService)
    {
        $this->mailerFactory = $mailerFactory;
        $this->eventNotificationService = $eventNotificationService;
    }

    public function sendContactRequest(ContactRequestModel $model)
    {
        try {
            $result = $this->mailerFactory
                ->createContactRequestMailer()
                ->setModel($model)
                ->send();

            $result
                ? $this->eventNotificationService->contactRequestSucceeded($model)
                : $this->eventNotificationService->contactRequestFailed($model);
        } catch (\Throwable $e) {
            $this->eventNotificationService->contactRequestFailed($model);

            throw $e;
        }
    }
}