<?php

declare(strict_types = 1);

namespace AppBundle\Service\Application;


use AppBundle\Doctrine\Type\CartDiscountCouponTypeEnum;
use AppBundle\Doctrine\Type\CartPersistentDiscountTypeEnum;
use AppBundle\Doctrine\Type\CartPurchaseDiscountTypeEnum;
use AppBundle\Doctrine\Type\CartPurchaseTypeEnum;
use AppBundle\Doctrine\Type\ItemToBuyTypeEnum;
use AppBundle\Doctrine\Type\Mappings\Items\Item;
use AppBundle\Entity\Application\Cart;
use AppBundle\Entity\Application\CartDiscountCoupon;
use AppBundle\Entity\Application\CartPersistentDiscount;
use AppBundle\Entity\Application\CartPurchaseDiscount;
use AppBundle\Entity\Application\ItemToBuy;
use AppBundle\Entity\Application\Payments\Order;
use AppBundle\Entity\Application\PurchaseLog\CartPurchaseLog;
use AppBundle\Entity\Application\PurchaseLog\StatsPurchaseLogItem;
use AppBundle\Entity\Application\PurchaseLog\WCoinPurchaseLogItem;
use AppBundle\Entity\Application\PurchaseLog\WebShopPurchaseLogItem;
use AppBundle\Entity\Application\UserServerAccount;
use AppBundle\Entity\Application\WebStorageItem;
use AppBundle\Repository\Application\CartDiscountCouponRepository;
use AppBundle\Repository\Application\CartPersistentDiscountRepository;
use AppBundle\Service\MuOnline\Funds\CoinsService;
use AppBundle\Service\MuOnline\Item\OptionsParserChain;
use AppBundle\Slack\Services\PaymentNotificationService;
use Collection\Sequence;
use Doctrine\ORM\EntityManager;

class PurchaseProcessor
{
    private $em;
    private $itemParser;
    private $persistentDiscountRepo;
    private $paymentNotificationService;
    private $coinsService;

    public function __construct(
        EntityManager $em,
        OptionsParserChain $itemParser,
        CartPersistentDiscountRepository $cartPersistentDiscountRepository,
        PaymentNotificationService $paymentNotificationService,
        CoinsService $coinsService
    )
    {
        $this->em = $em;
        $this->itemParser = $itemParser;
        $this->persistentDiscountRepo = $cartPersistentDiscountRepository;
        $this->paymentNotificationService = $paymentNotificationService;
        $this->coinsService = $coinsService;
    }

    public function assignOrderItems(Order $order, float $price)
    {
        $userServerAccount = $order->getUserServerAccount();
        $basePrice = $order->calculateRequiredMoney();

        $cartPurchaseLog = (new CartPurchaseLog())
            ->setPriceMoney($basePrice)
            ->setPriceCredits($order->calculateRequiredCreditsCount())
            ->setFinalPrice($price)
            ->setType(CartPurchaseTypeEnum::PURCHASE_TYPE_MONEY);

        $this->assignPurchasedItems(
            new Sequence($order->items()->toArray()),
            $userServerAccount,
            $cartPurchaseLog,
            $price,
            CartPurchaseTypeEnum::PURCHASE_TYPE_MONEY
        );

        $this->logAppliedCoupon($order->discountCoupon(), $cartPurchaseLog, $basePrice);
        $this->logAppliedPersistentDiscount(
            $userServerAccount,
            $cartPurchaseLog,
            $basePrice,
            CartPersistentDiscountTypeEnum::PERSISTENT_DISCOUNT_TYPE_MONEY
        );

        $userServerAccount->addCartPurchaseLog($cartPurchaseLog);

        $userServerAccount->addSpentMoney($price);

        $this->em->persist($userServerAccount);
        $this->em->flush();

        $this->paymentNotificationService->paymentCredited($order->paymentSystem(), $order);
    }

    public function assignCartItems(Cart $cart, int $price, CartDiscountCoupon $coupon = null)
    {
        $userServerAccount = $cart->getUserServerAccount();
        $basePrice = $cart->calculateRequiredCreditsCount();

        $cartPurchaseLog = (new CartPurchaseLog())
            ->setPriceMoney($cart->calculateRequiredMoney())
            ->setPriceCredits($basePrice)
            ->setFinalPrice($price)
            ->setType(CartPurchaseTypeEnum::PURCHASE_TYPE_CREDITS);

        $this->assignPurchasedItems(
            new Sequence($cart->getItems()->toArray()),
            $userServerAccount,
            $cartPurchaseLog,
            $price,
            CartPurchaseTypeEnum::PURCHASE_TYPE_CREDITS
        );

        $this->logAppliedCoupon($coupon, $cartPurchaseLog, $basePrice);
        $this->logAppliedPersistentDiscount(
            $userServerAccount,
            $cartPurchaseLog,
            $basePrice,
            CartPersistentDiscountTypeEnum::PERSISTENT_DISCOUNT_TYPE_CREDITS
        );

        $cart->removeItems();
        $userServerAccount->addCartPurchaseLog($cartPurchaseLog);

        $userServerAccount->addSpentCredits($price);

        $coupon !== null && $this->em->persist($coupon);
        $this->em->persist($userServerAccount);
        $this->em->flush();
    }

    private function logAppliedPersistentDiscount(UserServerAccount $userServerAccount, CartPurchaseLog $purchaseLog, float $price, string $discountType)
    {
        if ($persistentDiscount = $this->persistentDiscountRepo->findInRange(
            $userServerAccount->getServer(),
            $userServerAccount->getSpentCredits(),
            $discountType
        )
        ) {
            $webShopPurchaseDiscount = (new CartPurchaseDiscount())
                ->setDiscountAmount($price - ceil($this->calculateDiscountedPrice($price, $persistentDiscount->getDiscount())))
                ->setDiscountPercent($persistentDiscount->getDiscount())
                ->setType(CartPurchaseDiscountTypeEnum::PURCHASE_DISCOUNT_TYPE_PERSISTENT_DISCOUNT);

            $purchaseLog->addDiscount($webShopPurchaseDiscount);
        }
    }

    private function logAppliedCoupon(CartDiscountCoupon $coupon = null, CartPurchaseLog $purchaseLog, float $price)
    {
        if ($coupon && $coupon->getType() === CartDiscountCouponTypeEnum::DISCOUNT_COUPON_TYPE_CREDITS) {
            $couponDiscount = ceil($this->calculateDiscountedPrice($price, $coupon->getDiscount()));
            $webShopPurchaseDiscount = (new CartPurchaseDiscount())
                ->setDiscountAmount($price - $couponDiscount)
                ->setDiscountPercent($coupon->getDiscount())
                ->setType(CartPurchaseDiscountTypeEnum::PURCHASE_DISCOUNT_TYPE_COUPON)
                ->setDiscountCoupon($coupon);

            $coupon->setStatus(CartDiscountCouponRepository::STATUS_DISABLED);

            $purchaseLog->addDiscount($webShopPurchaseDiscount);
        }
        if ($coupon && $coupon->getType() === CartDiscountCouponTypeEnum::DISCOUNT_COUPON_TYPE_MONEY) {
            $couponDiscount = ceil($this->calculateDiscountedPrice($price, $coupon->getDiscount()));
            $webShopPurchaseDiscount = (new CartPurchaseDiscount())
                ->setDiscountAmount($price - $couponDiscount)
                ->setDiscountPercent($coupon->getDiscount())
                ->setType(CartPurchaseDiscountTypeEnum::PURCHASE_DISCOUNT_TYPE_COUPON)
                ->setDiscountCoupon($coupon);

            $purchaseLog->addDiscount($webShopPurchaseDiscount);
        }
    }

    private function assignPurchasedItems(
        Sequence $items,
        UserServerAccount $userServerAccount,
        CartPurchaseLog $purchaseLog,
        float $price,
        string $priceType
    )
    {

        $items->map(function (ItemToBuy $item) use ($purchaseLog, $price, $userServerAccount, $priceType) {
            if ($item->getType() === ItemToBuyTypeEnum::TYPE_ITEM) {
                $userServerAccount->getWebStorage()->addItem(
                    (new WebStorageItem())->setItem($item->getItem())
                );

                $parsedItem = $this->itemParser->parse(Item::createExisting()->setHex($item->getItem()));

                $webShopPurchaseLogItem = (new WebShopPurchaseLogItem())
                    ->setItem($item->getItem())
                    ->setFirstSerial($parsedItem->getGeneralOptions()->getSerialNumber()->getFirst())
                    ->setSecondSerial($parsedItem->getGeneralOptions()->getSerialNumber()->getSecond());

                if ($priceType === CartPurchaseTypeEnum::PURCHASE_TYPE_CREDITS) {
                    $webShopPurchaseLogItem
                        ->setPriceCredits((int) $price)
                        ->setPriceMoney($item->getPriceMoney());
                } else {
                    $webShopPurchaseLogItem
                        ->setPriceCredits($item->getPriceCredits())
                        ->setPriceMoney($price);
                }

                $purchaseLog->addItem($webShopPurchaseLogItem);
            }

            if ($item->getType() === ItemToBuyTypeEnum::TYPE_STATS) {
                $userServerAccount->addPurchasedStats($item->getStats());

                $statsPurchaseLogItem = (new StatsPurchaseLogItem())
                    ->setStats($item->getStats());

                if ($priceType === CartPurchaseTypeEnum::PURCHASE_TYPE_CREDITS) {
                    $statsPurchaseLogItem
                        ->setPriceCredits((int) $price)
                        ->setPriceMoney($item->getPriceMoney());
                } else {
                    $statsPurchaseLogItem
                        ->setPriceCredits($item->getPriceCredits())
                        ->setPriceMoney($price);
                }

                $purchaseLog->addStats($statsPurchaseLogItem);
            }

            if ($item->getType() === ItemToBuyTypeEnum::TYPE_WCOIN) {
                $this->coinsService->add($userServerAccount->getAccountId(), $item->getStats());

                $coinsPurchaseLogItem = (new WCoinPurchaseLogItem())->setCoins($item->getStats());

                if ($priceType === CartPurchaseTypeEnum::PURCHASE_TYPE_CREDITS) {
                    $coinsPurchaseLogItem
                        ->setPriceCredits((int) $price)
                        ->setPriceMoney($item->getPriceMoney());
                } else {
                    $coinsPurchaseLogItem
                        ->setPriceCredits($item->getPriceCredits())
                        ->setPriceMoney($price);
                }

                $purchaseLog->addCoins($coinsPurchaseLogItem);
            }
        });
    }

    private function calculateDiscountedPrice(float $price, int $discount): float
    {
        return ceil($price * (100 - $discount)) / 100;
    }
}