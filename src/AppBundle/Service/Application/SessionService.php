<?php

namespace AppBundle\Service\Application;


use AppBundle\Entity\Application\Server;
use AppBundle\Filter\MarketFilter;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class SessionService
{

    const SERVER_STORAGE_KEY = 'CURRENT_SELECTED_SERVER';
    const MARKET_SEARCH_CRITERIA_STORAGE_KEY = 'CURRENT_MARKET_SEARCH_CRITERIA';
    const LOCALE_STORAGE_KEY = '_locale';

    private $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public function setServerHash(Server $server): SessionService
    {
        $this->session && $this->session->set(SessionService::SERVER_STORAGE_KEY, $server->getHash());

        return $this;
    }

    public function getServerHash()
    {
        return $this->session && $this->session->has(SessionService::SERVER_STORAGE_KEY)
            ? $this->session->get(SessionService::SERVER_STORAGE_KEY)
            : null;
    }

    public function setMarketFilter(MarketFilter $filter): SessionService
    {
        $this->session && $this->session->set(
            SessionService::MARKET_SEARCH_CRITERIA_STORAGE_KEY,
            $this->createMarketFilterSerializer()->serialize($filter ?? [], 'json')
        );

        return $this;
    }

    public function getMarketFilter()
    {
        $serialized = $this->session && $this->session->has(SessionService::MARKET_SEARCH_CRITERIA_STORAGE_KEY)
            ? $this->session->get(SessionService::MARKET_SEARCH_CRITERIA_STORAGE_KEY)
            : null;

        if (null === $serialized) {
            return null;
        }

        return $this->createMarketFilterSerializer()->deserialize($serialized, MarketFilter::class, 'json');
    }

    public function setLocale(string $locale): void
    {
        $this->session && $this->session->set(self::LOCALE_STORAGE_KEY, $locale);
    }

    public function getLocale(): ?string
    {
        return $this->session && $this->session->has(self::LOCALE_STORAGE_KEY)
            ? $this->session->get(self::LOCALE_STORAGE_KEY)
            : null;
    }

    public function purgeLocale(): void
    {
        $this->session && $this->session->remove(self::LOCALE_STORAGE_KEY);
    }

    private function createMarketFilterSerializer(): SerializerInterface
    {
        $normalizer = new ObjectNormalizer();
        $normalizer->setIgnoredAttributes([
            'userServerAccount',
        ]);

        $encoder = new JsonEncoder();

        return new Serializer(array($normalizer), array($encoder));
    }
}