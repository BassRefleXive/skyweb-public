<?php

namespace AppBundle\Service\Application;


use AppBundle\Doctrine\Type\Mappings\Items\ExcellentOptions;
use AppBundle\Doctrine\Type\Mappings\Items\GeneralOptions;
use AppBundle\Doctrine\Type\Mappings\Items\HarmonyOptions;
use AppBundle\Entity\Application\Cart;
use AppBundle\Entity\Application\Items\AncientSet;
use AppBundle\Entity\Application\Items\HarmonyOption;
use AppBundle\Entity\Application\Items\PvPOption;
use AppBundle\Entity\Application\User\User;
use AppBundle\Entity\Application\WebShopCategory;
use AppBundle\Entity\Application\WebShopCategoryItem;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Service\MuOnline\AccountService;
use Doctrine\ORM\EntityManager;

class WebShopService
{
    protected $em;
    protected $accountService;
    protected $itemService;
    protected $cartService;

    public function __construct(
        EntityManager $em,
        AccountService $accountService,
        ItemService $itemService,
        CartService $cartService
    )
    {
        $this->em = $em;
        $this->accountService = $accountService;
        $this->itemService = $itemService;
        $this->cartService = $cartService;
    }

    public function buy(
        User $user,
        WebShopCategoryItem $item,
        int $priceCredits,
        float $priceMoney,
        int $level = null,
        int $option = null,
        bool $skill = false,
        bool $luck = false,
        array $excellent = [],
        int $ancient = null,
        int $harmony = null,
        bool $pvp = false
    ): Cart
    {
        if (!$item->getWebShopCategory()->getConfig()->isEnabled()) {
            throw new DisplayableException('web_shop.buy.17');
        }

        $itemInfo = $item->getItem();

        $generalOptions = (new GeneralOptions())
            ->setGroup($itemInfo->getGroup()->getTypeId())
            ->setId($itemInfo->getItemId())
            ->setLvl($level ? $level : $itemInfo->getLevel())
            ->setOpt($option ?? 0)
            ->setIsSkill($skill)
            ->setIsLuck($luck);

        $excellentOptions = null;
        $harmonyOptions = null;
        $ancientSet = null;
        $pvpOption = null;

        if (count($excellent) > 0) {
            $excellentOptions = new ExcellentOptions();
            foreach ($excellent as $excellentOption) {
                $excellentOptions->addOption($excellentOption);
            }
        }

        if ($harmony) {
            $harmonyOptions = (new HarmonyOptions())->setOption($this->em->find(HarmonyOption::class, $harmony));
        }
        if ($ancient) {
            $ancientSet = $this->em->find(AncientSet::class, $ancient);
        }
        if ($pvp) {
            $pvpOption = $this->em->find(PvPOption::class, 1);
        }

        $account = $this->accountService->getAccountByUser($user);

        $items = $account->getWarehouse()->getItems();

        $item = $this->itemService->create(
            $items->getItemSize(),
            $generalOptions,
            $excellentOptions,
            $harmonyOptions,
            $ancientSet,
            $pvpOption
        );

        $cart = $this->cartService->addItem($user, $item, $priceCredits, $priceMoney);

        return $cart;

    }

    public function flattenCategories(array $categories, array $results = []): array
    {
        if (!count($categories)) {
            return $results;
        }

        $level = count($results);

        $results[$level] = [];
        $next = [];

        /** @var WebShopCategory $category */
        foreach ($categories as $category) {
            $results[$level][] = $category;
            $next = array_merge($next, $category->childCategories()->toArray());
        }

        return $this->flattenCategories(
            $next,
            $results
        );
    }
}