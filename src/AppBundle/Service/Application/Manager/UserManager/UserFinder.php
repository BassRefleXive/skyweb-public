<?php

declare(strict_types = 1);

namespace AppBundle\Service\Application\Manager\UserManager;

use AppBundle\Model\Application\Admin\UserManager\SearchModel;
use AppBundle\Repository\Application\UserRepository;
use Collection\Sequence;

class UserFinder
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function find(SearchModel $searchModel): Sequence
    {
        return new Sequence($this->userRepository->findByKeyword($searchModel->getKeyword()));
    }
}