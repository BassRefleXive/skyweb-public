<?php

declare(strict_types = 1);

namespace AppBundle\Service\Application\Manager\UserManager;


use AppBundle\Entity\Application\User\Ban;
use AppBundle\Entity\Application\User\User;
use AppBundle\Entity\MuOnline\Account;
use AppBundle\Repository\Application\User\BanRepository;
use AppBundle\Repository\Application\UserRepository;
use AppBundle\Service\Application\ServerService;
use AppBundle\Service\MuOnline\AccountService;

class BanManager
{
    private $banRepository;
    private $userRepository;
    private $accountService;
    private $serverService;

    public function __construct(
        BanRepository $banRepository,
        UserRepository $userRepository,
        AccountService $accountService,
        ServerService $serverService
    )
    {
        $this->banRepository = $banRepository;
        $this->userRepository = $userRepository;
        $this->accountService = $accountService;
        $this->serverService = $serverService;
    }

    public function banUser(Ban $ban): void
    {
        foreach ($this->serverService->getList() as $server) {
            $account = $this->accountService->getAccountByUser($ban->getUser(), $server);

            if (null === $account) {
                continue;
            }

            $account->setBlocCode(Account::STATUS_BLOCKED);

            $manager = $this->serverService->getServerDataBaseManager($server);

            $manager->getRepository(Account::class)->save($account);
        }

        $this->banRepository->save($ban);
    }

    public function unBanUsersWithExpiredBans(): int
    {
        $users = $this->userRepository->findUsersWithExpiredBans(new \DateTime());

        foreach ($users as $user) {
            $this->unBanUser($user);
        }

        return count($users);
    }

    private function unBanUser(User $user)
    {
        /** @var Ban $ban */
        foreach ($this->banRepository->findByUser($user) as $ban) {
            foreach ($this->serverService->getList() as $server) {
                $account = $this->accountService->getAccountByUser($ban->getUser(), $server);

                if (null === $account) {
                    continue;
                }

                $account->setBlocCode(Account::STATUS_ACTIVE);

                $manager = $this->serverService->getServerDataBaseManager($server);

                $manager->getRepository(Account::class)->save($account);
            }
            $ban->unBan();

            $this->banRepository->save($ban);
        }
    }
}