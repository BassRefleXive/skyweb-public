<?php

namespace AppBundle\Service\Application;


use AppBundle\Entity\Application\User\User;
use AppBundle\Entity\Application\UserServerAccount;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Model\Application\Account\WCoinShopPurchaseModel;

class WCoinShopService
{

    protected $cartService;
    protected $serverService;

    public function __construct(CartService $cartService, ServerService $serverService)
    {
        $this->cartService = $cartService;
        $this->serverService = $serverService;
    }

    public function buy(UserServerAccount $userServerAccount, WCoinShopPurchaseModel $model)
    {
        $config = $userServerAccount->getServer()->getWCoinShopConfig();

        if (!$config->isEnabled()) {
            throw new DisplayableException('wcoin_shop.index.1');
        }

        $priceMoney = ceil($model->coins() / $config->getCoinsPerUsd() * 100) / 100;

        if ($priceMoney < $config->getMinAmount()) {
            throw new DisplayableException('wcoin_shop.index.7');
        }

        $cart = $this->cartService->addWCoin($userServerAccount->getUser(), $model->coins(), $priceMoney);

        return $cart;
    }

}