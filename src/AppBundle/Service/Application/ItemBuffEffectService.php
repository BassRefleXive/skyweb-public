<?php

declare(strict_types=1);

namespace AppBundle\Service\Application;


use AppBundle\Entity\Application\Items\ItemInfo;
use AppBundle\Entity\MuOnline\Character;
use AppBundle\Entity\MuOnline\PeriodItemInfo;
use AppBundle\Model\Application\BuffEffectItem;
use AppBundle\Repository\Application\ItemInfoRepository;
use AppBundle\Service\MuOnline\CharacterService;
use Collection\Sequence;

class ItemBuffEffectService
{
    private $itemInfoRepository;
    private $characterService;

    public function __construct(ItemInfoRepository $itemInfoRepository, CharacterService $characterService)
    {
        $this->itemInfoRepository = $itemInfoRepository;
        $this->characterService = $characterService;
    }

    public function characterUsedBuffItems(Character $character): Sequence
    {
        $result = new Sequence();

        $activePeriodItems = $this->characterService->findActivePeriodItems($character);

        if (!$activePeriodItems->length()) {
            return $result;
        }

        $knownItemBuffEffectItems = $this->itemInfoRepository->findByCombinedItemCodes(
            array_map(
                function (PeriodItemInfo $periodItemInfo): int {
                    return $periodItemInfo->code();
                },
                $activePeriodItems->all()
            )
        );

        if (!$knownItemBuffEffectItems->length()) {
            return $result;
        }

        /** @var ItemInfo $knownItemBuffEffectItem */
        foreach ($knownItemBuffEffectItems as $knownItemBuffEffectItem) {
            /** @var PeriodItemInfo $activePeriodItem */
            foreach ($activePeriodItems as $activePeriodItem) {
                if ($knownItemBuffEffectItem->getGroup()->getTypeId() * 512 + $knownItemBuffEffectItem->getItemId() !== $activePeriodItem->code()) {
                    continue;
                }

                $result->add(new BuffEffectItem($knownItemBuffEffectItem, $activePeriodItem));

                break;
            }
        }

        return $result;
    }
}