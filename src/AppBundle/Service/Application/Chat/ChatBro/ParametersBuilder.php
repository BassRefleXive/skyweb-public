<?php

namespace AppBundle\Service\Application\Chat\ChatBro;

use AppBundle\Entity\Application\User\User;
use AppBundle\Service\Application\Chat\ParametersBuilderInterface;
use AppBundle\Service\Application\Chat\ParametersInterface;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class ParametersBuilder implements ParametersBuilderInterface
{
    private $router;
    private $tokenStorage;
    private $chatId;
    private $key;
    private $users;

    public function __construct(
        Router $router,
        TokenStorageInterface $tokenStorage,
        string $chatId,
        string $key,
        array $users = []
    )
    {
        $this->router = $router;
        $this->tokenStorage = $tokenStorage;
        $this->chatId = $chatId;
        $this->key = $key;
        $this->users = $users;
    }

    public function build(): ParametersInterface
    {
        if (!($user = $this->user()) instanceof User) {
            return new Parameters($this->chatId, $this->host(), $this->key);
        }

        return new Parameters(
            $this->chatId,
            $this->host(),
            $this->key,
            $user->getId(),
            $user->getDisplayName(),
            $this->router->generate('sky_users_view', ['id' => $user->getId()]),
            $this->users[$user->getDisplayName()] ?? []
        );
    }

    /**
     * @return User|null
     */
    private function user()
    {
        if (!($token = $this->tokenStorage->getToken()) instanceof TokenInterface) {
            return null;
        }

        return $user = $token->getUser();
    }

    private function host(): string
    {
        return $this->router->getContext()->getHost();
    }
}