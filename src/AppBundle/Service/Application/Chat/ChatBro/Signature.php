<?php


namespace AppBundle\Service\Application\Chat\ChatBro;


class Signature
{
    public static function generate(Parameters $parameters, string $key): string
    {
        return md5(
            implode(
                '',
                [
                    $parameters->siteDomain(),
                    $parameters->siteUserExternalId(),
                    $parameters->siteUserFullName(),
                    $parameters->siteUserProfileUrl(),
                    implode('', $parameters->permissions()),
                    $key,
                ]
            )
        );
    }
}