<?php


namespace AppBundle\Service\Application\Chat\ChatBro;


use AppBundle\Service\Application\Chat\ParametersInterface;

class Parameters implements ParametersInterface
{
    private $encodedChatId;
    private $siteDomain;
    private $signature;
    private $siteUserExternalId;
    private $siteUserFullName;
    private $siteUserProfileUrl;
    private $showChatHeader;
    private $showChatOuterFrame;
    private $permissions;

    public function __construct(
        string $encodedChatId,
        string $domain,
        string $key,
        int $siteUserExternalId = null,
        string $siteUserFullName = null,
        string $siteUserProfileUrl = null,
        array $permissions = []
    )
    {
        $this->showChatHeader = false;
        $this->showChatOuterFrame = false;

        $this->encodedChatId = $encodedChatId;
        $this->siteDomain = $domain;
        $this->siteUserExternalId = $siteUserExternalId;
        $this->siteUserFullName = $siteUserFullName;
        $this->siteUserProfileUrl = $siteUserProfileUrl;
        $this->permissions = $permissions;

        $this->signature = Signature::generate($this, $key);
    }

    public function all(): array
    {
        $result = [];

        foreach ($this as $key => $value) {
            $result[$key] = $value;
        }

        return $result;
    }

    public function siteDomain(): string
    {
        return $this->siteDomain;
    }

    public function siteUserExternalId()
    {
        return $this->siteUserExternalId;
    }

    public function siteUserFullName()
    {
        return $this->siteUserFullName;
    }

    public function siteUserProfileUrl()
    {
        return $this->siteUserProfileUrl;
    }

    public function permissions(): array
    {
        return $this->permissions;
    }
}