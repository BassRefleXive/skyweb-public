<?php


namespace AppBundle\Service\Application\Chat;


interface ParametersBuilderInterface
{
    public function build(): ParametersInterface;
}