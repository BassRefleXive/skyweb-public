<?php

namespace AppBundle\Service\Application\Chat;

interface ParametersInterface
{
    public function all(): array;
}