<?php

declare(strict_types=1);

namespace AppBundle\Service\MuOnline\MagicList;

use AppBundle\Doctrine\Type\Mappings\MagicList\MagicList;
use AppBundle\Doctrine\Type\Mappings\MagicList\Skill;
use AppBundle\Repository\Application\MagicList\SkillRepository;
use Collection\Map;

class MagicListProcessor
{
    private $skillRepository;

    public function __construct(SkillRepository $skillRepository)
    {
        $this->skillRepository = $skillRepository;
    }

    public function process(MagicList $magicList): MagicList
    {
        $ids = array_map(function (Skill $skill): int {
            return $skill->id();
        }, $magicList->all());

        $skillInfo = count($ids) > 0
            ? $this->skillRepository->findByIds($ids)
            : new Map();

        return $magicList
            ->map(function (Skill $skill) use ($skillInfo): Skill {
                return $skill->setInfo(
                    $skillInfo
                        ->get($skill->id())
                        ->getOrThrow(
                            new \RuntimeException(
                                sprintf(
                                    'Failed to obtain info for skill with id #%d. Not found in storage.',
                                    $skill->id()
                                )
                            )
                        )
                );
            });
    }
}