<?php

namespace AppBundle\Service\MuOnline;


use AppBundle\Entity\Application\Server;
use AppBundle\Entity\MuOnline\AccountState;
use AppBundle\Repository\MuOnline\AccountStateRepository;
use AppBundle\Service\Application\ServerService;

class AccountStateService
{
    private $serverService;
    private $cacheTtl;

    public function __construct(ServerService $serverService, int $cacheTtl)
    {
        $this->serverService = $serverService;
        $this->cacheTtl = $cacheTtl;
    }

    public function getOnlineCountByServer(Server $server): int
    {
        $manager = $this->serverService->getServerDataBaseManager($server);
        /** @var AccountStateRepository $repo */
        $repo = $manager->getRepository(AccountState::class);
        $repo->setTtl($this->cacheTtl);

        return $repo->findOnlineCount();
    }

    public function getOnlineByServer(Server $server): array
    {
        $manager = $this->serverService->getServerDataBaseManager($server);
        /** @var AccountStateRepository $repo */
        $repo = $manager->getRepository(AccountState::class);

        return $repo->findOnline();
    }
}