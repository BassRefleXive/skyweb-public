<?php


namespace AppBundle\Service\MuOnline\Funds;


use AppBundle\Entity\Application\Server;
use AppBundle\Service\Common\Funds\FundsService;
use AppBundle\Service\Common\Funds\GetCoinsResponse;
use Doctrine\DBAL\Connection;

class CoinsService extends FundsService
{
    public function add(string $accountId, int $amount, Server $server = null, bool $writeLog = true): void
    {
        $server = $this->server($server);

        $writeLog && $this->logger()->info(sprintf('Request to add %d WCoins to account "%s" on server "%s".', $amount, $accountId, $server->getName()));

        $points = $this->get($accountId, $server, $writeLog);
        $this->set($accountId, $points->wCoinC() + $amount, $server, $writeLog);

        $writeLog && $this->logger()->info(sprintf('%d WCoins added to account "%s" on server "%s".', $amount, $accountId, $server->getName()));
    }

    public function sub(string $accountId, int $amount, Server $server = null, bool $writeLog = true): void
    {
        $server = $this->server($server);

        $writeLog && $this->logger()->info(sprintf('Request to subtract %d WCoins to account "%s" on server "%s".', $amount, $accountId, $server->getName()));

        $points = $this->get($accountId, $server, $writeLog);
        $this->set($accountId, $points->wCoinC() - $amount, $server, $writeLog);

        $writeLog && $this->logger()->info(sprintf('%d WCoins subtracted from account "%s" on server "%s".', $amount, $accountId, $server->getName()));
    }

    public function get(string $accountId, Server $server = null, bool $writeLog = true): GetCoinsResponse
    {
        $server = $this->server($server);

        $conn = $this->databaseConnection($server, $writeLog);

        $writeLog && $this->logger()->info(sprintf('Request to obtain account "%s" WCoins on server "%s".', $accountId, $server->getName()));

        $result = $conn->createQueryBuilder()
            ->select('WCoinC, WCoinP, GoblinPoint AS WCoinG')
            ->from('T_InGameShop_Point')
            ->where('AccountID = :account')
            ->setParameter('account', $accountId)
            ->execute()
            ->fetch();

        if (false === $result) {
            $result = new GetCoinsResponse(0, 0, 0);

            $conn->createQueryBuilder()
                ->insert('T_InGameShop_Point')
                ->setValue('WCoinC', ':wcoinc')
                ->setValue('WCoinP', ':wcoinp')
                ->setValue('GoblinPoint', ':wcoing')
                ->setValue('AccountID', ':accountId')
                ->setParameter('wcoinc', 0)
                ->setParameter('wcoinp', 0)
                ->setParameter('wcoing', 0)
                ->setParameter('accountId', $accountId)
                ->execute();

        } else {
            $result = new GetCoinsResponse((int) $result['WCoinC'], (int) $result['WCoinP'], (int) $result['WCoinG']);
        }

        $writeLog && $this->logger()->info(sprintf('Account "%s" WCoins: %d on server "%s".', $accountId, $result->wCoinC(), $server->getName()));

        return $result;
    }

    public function set(string $accountId, int $amount, Server $server = null, bool $writeLog = true): void
    {
        $server = $this->server($server);

        $writeLog && $this->logger()->info(sprintf('Request to set account "%s" WCoins to %d on server "%s".', $accountId, $amount, $server->getName()));

        $this->databaseConnection()->createQueryBuilder()
            ->update('T_InGameShop_Point')
            ->set('WCoinC', $amount)
            ->set('WCoinP', 0)
            ->set('GoblinPoint', 0)
            ->where('AccountID = :account')
            ->setParameter('account', $accountId)
            ->execute();

        $writeLog && $this->logger()->info(sprintf('%d WCoins set to account "%s" on server "%s".', $amount, $accountId, $server->getName()));
    }

    final protected function databaseConnection(Server $server = null, bool $writeLog = true): Connection
    {
        return null === $server
            ? $this->serverService()->getCurrentServerDataBaseManager()->getConnection()
            : $this->serverService()->getServerDataBaseManager($server)->getConnection();
    }
}