<?php

namespace AppBundle\Service\MuOnline;


use AppBundle\Entity\Application\Server;
use AppBundle\Entity\Application\User\User;
use AppBundle\Entity\Application\UserServerAccount;
use AppBundle\Entity\MuOnline\Account;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Service\Application\ServerService;
use AppBundle\Service\MuOnline\Funds\CoinsService;
use Doctrine\ORM\EntityManager;

class AccountService
{
    private $serverService;
    private $coinsService;
    private $em;

    public function __construct(ServerService $serverService, CoinsService $coinsService, EntityManager $em)
    {
        $this->serverService = $serverService;
        $this->coinsService = $coinsService;
        $this->em = $em;
    }

    public function saveAccount(Account $account, Server $server = null): Account
    {
        if (!$server) {
            $server = $this->serverService->getCurrentServer();
        }

        $manager = $this->serverService->getServerDataBaseManager($server);

        $manager->persist($account);
        $manager->flush($account);

        return $account;
    }

    /**
     * @param UserServerAccount $userServerAccount
     * @param Server|null $server
     * @return null|Account
     */
    public function getAccountByUserServerAccount(UserServerAccount $userServerAccount, Server $server = null)
    {
        if (!$server) {
            $server = $this->serverService->getCurrentServer();
        }

        $manager = $this->serverService->getServerDataBaseManager($server);

        return $manager->getRepository(Account::class)->find($userServerAccount->getAccountId());
    }

    /**
     * @param User $user
     * @param Server|null $server
     * @return null|Account
     */
    public function getAccountByUser(User $user, Server $server = null)
    {
        if (!$server) {
            $server = $this->serverService->getCurrentServer();
        }

        /** @var UserServerAccount $userServerAccount */
        $userServerAccount = $user->getUserServerAccountByServer($server);

        $account = null;

        if ($userServerAccount) {
            $account = $this->getAccountByUserServerAccount($userServerAccount, $server);
        }

        return $account;
    }

    public function createAccount(User $user, Server $server = null): Account
    {
        if ($this->getAccountByUser($user, $server)) {
            throw new DisplayableException('errors.16');
        }

        if (!$server) {
            $server = $this->serverService->getCurrentServer();
        }

        $manager = $this->serverService->getServerDataBaseManager($server);

        $account = (new Account())
            ->setMemberId($user->getUsername())
            ->setPassword($user->getPassword())
            ->setName($user->getUsername())
            ->setEmail($user->email())
            ->setBlocCode(Account::STATUS_ACTIVE)
            ->setCtl1Code(1);

        $manager->persist($account);
        $manager->flush($account);

        $userServerAccount = (new UserServerAccount())
            ->setUser($user)
            ->setServer($server)
            ->setAccountId($account->getMemberId());

        $userServerAccount->depositZen($server->getAccountMenuConfig()->getRegistrationAddZen());

        $user->addUserServerAccount($userServerAccount);

        $this->em->persist($user);
        $this->em->flush($user);

        $this->coinsService->add($account->getMemberId(), $server->getAccountMenuConfig()->getRegistrationAddWCoin());

        return $account;
    }
}