<?php

namespace AppBundle\Service\MuOnline;


use AppBundle\Dictionary\MuOnline\MapsDictionary;
use AppBundle\Doctrine\Type\Mappings\Items\GeneralOptions;
use AppBundle\Doctrine\Type\Mappings\MagicList\Builder\SkillBuilder;
use AppBundle\Doctrine\Type\Mappings\MagicList\Skill;
use AppBundle\Entity\Application\Config\CharacterEvolutionConfig;
use AppBundle\Entity\Application\Server;
use AppBundle\Entity\Application\UserServerAccount;
use AppBundle\Entity\Application\WebStorageItem;
use AppBundle\Entity\MuOnline\Character;
use AppBundle\Entity\MuOnline\PeriodItemInfo;
use AppBundle\Enum\MuOnline\CharacterClass;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Filter\CharacterFilter;
use AppBundle\Repository\Application\UserServerAccountRepository;
use AppBundle\Repository\MuOnline\AccountStateRepository;
use AppBundle\Repository\MuOnline\CharacterRepository;
use AppBundle\Service\Application\ItemService;
use AppBundle\Service\Application\ServerService;
use AppBundle\Service\Application\WebStorageService;
use AppBundle\Service\MuOnline\Funds\CoinsService;
use AppBundle\Service\MuOnline\MagicList\MagicListProcessor;
use Collection\Sequence;
use Doctrine\ORM\Tools\Pagination\Paginator;

class CharacterService
{
    private $serverService;
    private $coinsService;
    private $itemService;
    private $userServerAccountRepository;
    private $magicListProcessor;
    private $webStorageService;
    private $characterCacheTtl;

    public function __construct(
        ServerService $serverService,
        CoinsService $coinsService,
        ItemService $itemService,
        UserServerAccountRepository $userServerAccountRepository,
        MagicListProcessor $magicListProcessor,
        WebStorageService $webStorageService,
        int $characterCacheTtl
    )
    {
        $this->serverService = $serverService;
        $this->coinsService = $coinsService;
        $this->itemService = $itemService;
        $this->userServerAccountRepository = $userServerAccountRepository;
        $this->magicListProcessor = $magicListProcessor;
        $this->webStorageService = $webStorageService;
        $this->characterCacheTtl = $characterCacheTtl;
    }

    public function getByFilter(CharacterFilter $filter, Server $server = null): array
    {
        if (!$server) {
            $server = $this->serverService->getCurrentServer();
        }

        $manager = $this->serverService->getServerDataBaseManager($server);
        /** @var CharacterRepository $repo */
        $repo = $manager->getRepository(Character::class);

        $repo->setTtl($this->characterCacheTtl);

        return $repo->findByQueryFilter($filter);
    }

    public function getByFilterPaginated(CharacterFilter $filter, Server $server = null): Paginator
    {
        if (!$server) {
            $server = $this->serverService->getCurrentServer();
        }

        $manager = $this->serverService->getServerDataBaseManager($server);
        /** @var CharacterRepository $repo */
        $repo = $manager->getRepository(Character::class);

        $repo->setTtl($this->characterCacheTtl);

        return $repo->findByQueryFilerPaginated($filter);
    }

    public function addPermanentStats(Character $character, int $points)
    {
        $character
            ->setPermanentStats($character->getPermanentStats() + $points)
            ->setPoints($character->getPoints() + $points);

        $manager = $this->serverService->getCurrentServerDataBaseManager();
        $manager->persist($character);
        $manager->flush($character);
    }

    public function changeNick(Character $character, string $nick, int $price)
    {
        $manager = $this->serverService->getCurrentServerDataBaseManager();

        $repo = $manager->getRepository(Character::class);
        $characterCheck = $repo->findOneBy(['name' => $nick]);

        if ($characterCheck) {
            throw new DisplayableException('errors.17');
        }

        $coins = $this->coinsService->get($character->getAccount()->getMemberId());

        if ($coins->wCoinC() < $price) {
            throw new DisplayableException('errors.14');
        }

        $this->coinsService->set($character->getAccount()->getMemberId(), $coins->wCoinC() - $price);

        $repo->changeName($character, $nick);
    }

    public function changeClass(Character $character, int $class, int $price)
    {
        $coins = $this->coinsService->get($character->getAccount()->getMemberId());

        if ($coins->wCoinC() < $price) {
            throw new DisplayableException('errors.14');
        }

        if (!$character->equippedItemsCellsEmpty($this->itemService)) {
            throw new DisplayableException('errors.49');
        }

        $character = $this->populateCharacterWithResettedFreePoints($character);

        $character->setStamina(
            in_array($class, [CharacterClass::DARK_LORD, CharacterClass::LORD_EMPEROR], true)
                ? 25
                : 0
        );

        $character->setClass($class);

        $manager = $this->serverService->getCurrentServerDataBaseManager();

        $this->coinsService->set($character->getAccount()->getMemberId(), $coins->wCoinC() - $price);
        $repo = $manager->getRepository(Character::class);

        $repo->save($character);
    }

    public function resetStats(Character $character, int $price)
    {
        $coins = $this->coinsService->get($character->getAccount()->getMemberId());

        if ($coins->wCoinC() < $price) {
            throw new DisplayableException('errors.14');
        }

        if (!$character->equippedItemsCellsEmpty($this->itemService)) {
            throw new DisplayableException('errors.49');
        }

        $character = $this->populateCharacterWithResettedFreePoints($character);

        $manager = $this->serverService->getCurrentServerDataBaseManager();

        $this->coinsService->set($character->getAccount()->getMemberId(), $coins->wCoinC() - $price);
        $repo = $manager->getRepository(Character::class);

        $repo->save($character);
    }

    public function reset(Character $character, UserServerAccount $userServerAccount): void
    {
        $config = $this->serverService->getCurrentServer()->getCharacterEvolutionConfig();

        if ($character->getLevel() < $config->resetLevel($character)) {
            throw new DisplayableException('reset.18');
        }

        if ($character->getReset() >= $config->getResetLimit()) {
            throw new DisplayableException('reset.19');
        }

        if (AccountStateRepository::STATE_OFFLINE !== $character->getAccount()->getState()->getConnectionState()) {
            throw new DisplayableException('reset.20');
        }

        if (($price = $this->resetPrice($character, $config, $userServerAccount)) > $userServerAccount->zen()) {
            throw new DisplayableException('reset.21');
        }

        if ($character->classInfo()->isDarkLord()) {
            if (null !== $item = $character->getInventory()->get(1)) {
                $shieldSlotItem = $this->itemService->parse($item);

                if (!$shieldSlotItem->isEmpty() && $shieldSlotItem->getItemInfo()->getGroup()->isGroup13() && 5 === $shieldSlotItem->getItemInfo()->getItemId()) {
                    throw new DisplayableException('reset.24');
                }
            }
        }

        if (true === $config->getResetType()->isResetPointsAddResetPoints()) {
            $character = $this->populateCharacterWithResettedFreePoints($character);
        }

        $character
            ->setReset($character->getReset() + 1)
            ->setPoints($this->pointsAfterEvolution($character, $config, $userServerAccount))
            ->setLevel(1)
            ->setMapNumber(MapsDictionary::MAP_LORENCIA)
            ->setMapX(125)
            ->setMapY(125);

        $userServerAccount->withdrawZen($price);
        $this->userServerAccountRepository->save($userServerAccount);

        $manager = $this->serverService->getCurrentServerDataBaseManager();
        $repo = $manager->getRepository(Character::class);
        $repo->save($character);

        $this->coinsService->add($character->getAccount()->getMemberId(), $config->getResetReward(), $config->getServer(), true);
    }

    public function grandReset(Character $character, UserServerAccount $userServerAccount): void
    {
        $config = $this->serverService->getCurrentServer()->getCharacterEvolutionConfig();

        if ($character->getLevel() < $config->resetLevel($character)) {
            throw new DisplayableException('reset.18');
        }

        if ($character->getReset() < $config->getResetLimit()) {
            throw new DisplayableException('grand_reset.15');
        }

        if (AccountStateRepository::STATE_OFFLINE !== $character->getAccount()->getState()->getConnectionState()) {
            throw new DisplayableException('reset.20');
        }

        if (($price = $this->grandResetPrice($character, $config, $userServerAccount)) > $userServerAccount->zen()) {
            throw new DisplayableException('grand_reset.16');
        }

        $character = $this->populateCharacterWithResettedFreePoints($character);

        $character
            ->setGrandReset($character->getGrandReset() + 1)
            ->setReset(0)
            ->setPoints($this->pointsAfterEvolution($character, $config, $userServerAccount))
            ->setLevel(1)
            ->setMapNumber(MapsDictionary::MAP_LORENCIA)
            ->setMapX(125)
            ->setMapY(125);

        $userServerAccount->withdrawZen($price);
        $this->userServerAccountRepository->save($userServerAccount);

        $manager = $this->serverService->getCurrentServerDataBaseManager();
        $repo = $manager->getRepository(Character::class);
        $repo->save($character);

        $this->coinsService->add($character->getAccount()->getMemberId(), $config->getGrandResetReward(), $config->getServer(), true);
    }

    public function findActivePeriodItems(Character $character): Sequence
    {
        $manager = $this->serverService->getCurrentServerDataBaseManager();

        $repo = $manager->getRepository(PeriodItemInfo::class);

        return $repo->findActiveByCharacter($character);
    }

    public function resetMasterSkillTree(Character $character, UserServerAccount $userServerAccount): void
    {
        if (!$character->classInfo()->isThirdClass()) {
            throw new DisplayableException('reset_master_skill_tree.5');
        }

        $config = $this->serverService->getCurrentServer()->getAccountMenuConfig();

        $coins = $this->coinsService->get($character->getAccount()->getMemberId());

        if ($config->getResetMasterSkillTreePrice() > $coins->wCoinC()) {
            throw new DisplayableException('reset_master_skill_tree.6');
        }

        $magicList = $this->magicListProcessor->process($character->magicList());

        $points = $magicList
            ->foldLeft(
                0,
                function (int $value, Skill $skill): int {
                    $value += $skill->info()->useType()->isMaster()
                        ? $skill->level()
                        : 0;

                    return $value;
                }
            );

        if ($points <= 0) {
            throw new DisplayableException('reset_master_skill_tree.7');
        }

        $magicList = $magicList->filter(function (Skill $skill): bool {
             return !$skill->info()->useType()->isMaster();
        });

        $character
            ->updateMagicList($magicList)
            ->updateMasterPoints($character->masterPoints() + $points);

        $manager = $this->serverService->getCurrentServerDataBaseManager();
        $repo = $manager->getRepository(Character::class);
        $repo->save($character);

        $this->coinsService->set($character->getAccount()->getMemberId(), $coins->wCoinC() - $config->getResetMasterSkillTreePrice());
    }

    public function resetSkills(Character $character, UserServerAccount $userServerAccount, array $skills): void
    {
        $magicList = $this->magicListProcessor->process($character->magicList());

        $magicList = $magicList->filter(function (Skill $skill) use ($userServerAccount, $character, $skills): bool {
            $result = !in_array($skill->id(), $skills, true);

            if (!$result) {
                $itemOptions = new GeneralOptions();
                $itemOptions->setGroup($skill->info()->learnItem()->getGroup()->getTypeId())
                    ->setId($skill->info()->learnItem()->getItemId())
                    ->setLvl($skill->info()->learnItem()->getLevel());

                $this->webStorageService->add(
                    $userServerAccount,
                    $this->itemService->create(
                        $character->getAccount()->getWarehouse()->getItems()->getItemSize(),
                        $itemOptions
                    )
                );
            }

            return $result;
        });

        $character->updateMagicList($magicList);

        $manager = $this->serverService->getCurrentServerDataBaseManager();
        $repo = $manager->getRepository(Character::class);
        $repo->save($character);
    }

    /**
     * This method should be called after reset & grand reset is increased in Character object.
     *
     * @param Character $character
     * @param CharacterEvolutionConfig $config
     * @param UserServerAccount $userServerAccount
     *
     * @return int
     */
    private function pointsAfterEvolution(Character $character, CharacterEvolutionConfig $config, UserServerAccount $userServerAccount): int
    {
        $result = $character->getGrandReset() * $config->getGrandResetPoints();

        $class = CharacterClass::byValue($character->getClass());

        if (false === $config->getResetType()->isKeepPoints()) {
            switch (true) {
                case $class->isDarkWizard():
                    {
                        $result += $character->getReset() * $config->getResetPointsDW();

                        break;
                    }
                case $class->isDarkKnight():
                    {
                        $result += $character->getReset() * $config->getResetPointsDK();

                        break;
                    }
                case $class->isFairyElf():
                    {
                        $result += $character->getReset() * $config->getResetPointsFE();

                        break;
                    }
                case $class->isMagicGladiator():
                    {
                        $result += $character->getReset() * $config->getResetPointsMG();

                        break;
                    }
                case $class->isDarkLord():
                    {
                        $result += $character->getReset() * $config->getResetPointsDL();

                        break;
                    }
                case $class->isSummoner():
                    {
                        $result += $character->getReset() * $config->getResetPointsSUM();

                        break;
                    }
                case $class->isRageFighter():
                    {
                        $result += $character->getReset() * $config->getResetPointsRF();

                        break;
                    }
            }
        }

        if ($userServerAccount->isVip()) {
            $result += ($character->getReset() * $userServerAccount->vipLevel()->getResetPointsBonus());
            $result += ($character->getGrandReset() * $userServerAccount->vipLevel()->getGrandResetPointsBonus());
        }

        return $result;
    }

    private function resetPrice(Character $character, CharacterEvolutionConfig $config, UserServerAccount $userServerAccount): int
    {
        $result = 0;

        if ($config->getResetPaymentType()->isConstant()) {
            $result = $config->getResetPrice();
        } elseif ($config->getResetPaymentType()->isMultiplier()) {
            $result = $config->getResetPrice() * ($character->getReset() + 1);
        }
        if ($userServerAccount->isVip()) {
            $result = ceil(($result * (100 - $userServerAccount->vipLevel()->getResetPriceDiscount())) / 100);
        }

        return $result;
    }

    private function grandResetPrice(Character $character, CharacterEvolutionConfig $config, UserServerAccount $userServerAccount): int
    {
        $result = $config->getGrandResetPrice() * ($character->getGrandReset() + 1);

        if ($userServerAccount->isVip()) {
            $result = ceil(($result * (100 - $userServerAccount->vipLevel()->getGrandResetPriceDiscount())) / 100);
        }

        return $result;
    }

    private function populateCharacterWithResettedFreePoints(Character $character): Character
    {
        $freePoints = $character->getPoints();
        $freePoints += $character->getStrength();
        $freePoints += $character->getAgility();
        $freePoints += $character->getVitality();
        $freePoints += $character->getEnergy();

        $freePoints -= 100;

        if (in_array($character->getClass(), [CharacterClass::DARK_LORD, CharacterClass::LORD_EMPEROR], true)) {
            $freePoints += $character->getStamina();
            $freePoints -= 25;
        }

        $character->setStrength(25)
            ->setAgility(25)
            ->setVitality(25)
            ->setEnergy(25);

        $character->setStamina(
            in_array($character->getClass(), [CharacterClass::DARK_LORD, CharacterClass::LORD_EMPEROR], true)
                ? 25
                : 0
        );

        $character->setPoints($freePoints);

        return $character;
    }
}