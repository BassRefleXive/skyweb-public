<?php

namespace AppBundle\Service\MuOnline\Item;


use AppBundle\Doctrine\Type\Mappings\Items\Item;

class ExcellentOptionsParser extends OptionsParser
{

    public function doBuild(Item $item): Item
    {
        $item->setHex(
            $this->setHexValue(
                $item->getHex(),
                14,
                2,
                $item->getExcellentOptions()->getOptions() + $this->getHexValue($item->getHex(), 14, 2)
            )
        );

        return $item;
    }

    public function doParse(Item $item): Item
    {
        $descriptor = $this->getExcellentDescriptor($item);
        $item->getExcellentOptions()->setOptions($descriptor);
        return $item;
    }

    private function getExcellentDescriptor(Item $item): int
    {
        $descriptor = $this->getHexValue($item->getHex(), 14, 2);

        return $descriptor > 63
            ? $descriptor - 64
            : $descriptor;
    }

}