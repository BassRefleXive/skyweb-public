<?php

namespace AppBundle\Service\MuOnline\Item;


use AppBundle\Doctrine\Type\Mappings\Items\Item;
use AppBundle\Exception\ApplicationException;

class PVPOptionsParser extends OptionsParser
{

    public function doBuild(Item $item): Item
    {
        $item->setHex($this->setPVPOption($item));

        return $item;
    }

    public function doParse(Item $item): Item
    {
        if ($pvpOption = $this->getPVPOption($item)) {
            $item->setPVPOption($pvpOption);
        }

        return $item;
    }

    private function getPVPOption(Item $item)
    {
        $result = null;

        $option = $this->getHexValue($item->getHex(), 19, 1);

        if ($option && $pvpOption = $item->getItemInfo()->getPvpOption()) {
            $result = $pvpOption;
        }

        return $result;
    }

    private function setPVPOption(Item $item): string
    {
        $result = 0;

        if ($item->getPVPOption() !== null) {
            if ($item->getItemInfo()->getPvpOption() === null) {
                $generalOptions = $item->getGeneralOptions();

                throw new ApplicationException(
                    sprintf(
                        'PvP Option cannot be added to item with group %d and it %d.',
                        $generalOptions->getGroup(),
                        $generalOptions->getId()
                    )
                );
            }

            $result = 8;
        }

        return $this->setHexValue($item->getHex(), 19, 1, $result);
    }

}