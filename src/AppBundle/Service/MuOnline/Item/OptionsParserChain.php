<?php

namespace AppBundle\Service\MuOnline\Item;


use AppBundle\Doctrine\Type\Mappings\Items\Item;

class OptionsParserChain
{
    protected $first = null;

    public function __construct(array $parsers)
    {
        $current = null;

        foreach ($parsers as $parser) {
            if (is_null($this->first)) {
                $this->first = $parser;
            }

            if (!is_null($current)) {
                $current->setNext($parser);
            }
            $current = $parser;
        }
    }

    public function parse(Item $item): Item
    {
        if ($item->isEmpty()) {
            return $item;
        }

        if ($this->first) {
            return $this->first->parse($item);
        }
        throw new \RuntimeException('No item info parsers defined.');
    }

    public function build(Item $item): Item
    {
        if (!$item->isEmpty()) {
            return $item;
        }

        if ($this->first) {
            return $this->first->build($item);
        }
        throw new \RuntimeException('No item info parsers defined.');
    }
}