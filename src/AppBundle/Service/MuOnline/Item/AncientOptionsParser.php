<?php

namespace AppBundle\Service\MuOnline\Item;


use AppBundle\Doctrine\Type\Mappings\Items\Item;
use AppBundle\Entity\Application\Items\AncientSet;
use AppBundle\Entity\Application\Items\AncientSetItem;
use AppBundle\Exception\ApplicationException;
use Collection\Sequence;

class AncientOptionsParser extends OptionsParser
{

    public function doBuild(Item $item): Item
    {
        $item->setHex($this->setAncientSet($item));

        return $item;
    }

    public function doParse(Item $item): Item
    {
        if ($ancientSet = $this->getAncientSet($item)) {
            $item->setAncientSet($ancientSet);
        }

        return $item;
    }

    private function setAncientSet(Item $item): string
    {
        $result = 0;

        /** @var AncientSet $ancientSet */
        if (($ancientSet = $item->getAncientSet()) !== null) {
            $generalOptions = $item->getGeneralOptions();
            $group = $item->getItemInfo()->getGroup();

            $result = (new Sequence($ancientSet->getItems()->toArray()))
                ->find(function (AncientSetItem $setItem) use ($group, $generalOptions) {
                    return $setItem->getItem()->getGroup()->getId() === $group->getId() && $setItem->getItem()->getItemId() === $generalOptions->getId()
                        ? true
                        : false;
                })->getOrThrow(
                    new ApplicationException(
                        sprintf(
                            'Ancient option cannot be added to item with group %d and id %d.',
                            $generalOptions->getGroup(),
                            $generalOptions->getId()
                        )
                    )
                )->getBonus();
        }

        return $this->setHexValue($item->getHex(), 17, 1, $result);
    }

    private function getAncientSet(Item $item)
    {
        $result = null;

        $bonus = $this->getAncientBonus($item);

        if ($bonus && !$item->getItemInfo()->getAncientSets()->isEmpty()) {
            /** @var AncientSetItem $result */
            $result = (new Sequence($item->getItemInfo()->getAncientSets()->toArray()))
                ->find(function (AncientSetItem $ancientSetItem) use ($bonus) {
                    return $bonus === $ancientSetItem->getBonus();
                })
                ->getOrElse(null);
        }

        return $result !== null
            ? $result->getAncientSet()
            : $result;
    }

    private function getAncientBonus(Item $item)
    {
        $desc = $this->getHexValue($item->getHex(), 17, 1);

        if ($desc === 5 || $desc === 9) {

            return $desc === 5
                ? AncientSet::ANCIENT_BONUS_5
                : AncientSet::ANCIENT_BONUS_10;

        } elseif ($desc === 6 || $desc === 10) {

            return $desc === 10
                ? AncientSet::ANCIENT_BONUS_10
                : AncientSet::ANCIENT_BONUS_5;

        }

        return null;
    }

}