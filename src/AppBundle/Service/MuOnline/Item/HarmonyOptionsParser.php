<?php

namespace AppBundle\Service\MuOnline\Item;


use AppBundle\Doctrine\Type\Mappings\Items\Item;
use AppBundle\Entity\Application\Items\GroupHarmonyOption;
use AppBundle\Entity\Application\Items\GroupHarmonyOptionLevel;
use AppBundle\Entity\Application\Items\HarmonyOption;
use AppBundle\Exception\ApplicationException;
use AppBundle\Repository\Application\GroupHarmonyOptionRepository;
use AppBundle\Repository\Application\ItemInfoRepository;

class HarmonyOptionsParser extends OptionsParser
{
    private $groupHarmonyOptionRepo;

    public function __construct(ItemInfoRepository $itemInfoRepo, GroupHarmonyOptionRepository $groupHarmonyOptionRepo)
    {
        parent::__construct($itemInfoRepo);

        $this->groupHarmonyOptionRepo = $groupHarmonyOptionRepo;
    }

    public function doBuild(Item $item): Item
    {
        $item
            ->setHex($this->setHarmonyOption($item))
            ->setHex($this->setHarmonyLevel($item));

        return $item;
    }

    public function doParse(Item $item): Item
    {

        if ($groupHarmonyOption = $this->getHarmonyOption($item)) {
            $item->getHarmonyOptions()
                 ->setOption($groupHarmonyOption->getHarmonyOption())
                 ->setLevel($this->getGroupHarmonyOptionLevelByValue($this->getItemHarmonyLevelValue($item), $groupHarmonyOption));
        }

        return $item;
    }

    private function getHarmonyOption(Item $item)
    {
        $result = null;

        $harmonyOption = $this->getHexValue($item->getHex(), 20, 1);

        if ($harmonyOption) {
            /** @var GroupHarmonyOption $result */
            $result = $this->getGroupHarmonyOptionByGroupAndType($item->getItemInfo()->getGroup()->getId(), $harmonyOption);
        }

        return $result;
    }

    private function setHarmonyOption(Item $item): string
    {
        $result = 0;

        /** @var HarmonyOption $harmonyOption */
        if (($harmonyOption = $item->getHarmonyOptions()->getOption()) !== null) {
            $result = $this->getGroupHarmonyOptionByGropAndHarmonyOption($item->getItemInfo()->getGroup()->getId(), $harmonyOption, true)->getType();
        }

        return $this->setHexValue($item->getHex(), 20, 1, $result);
    }

    private function setHarmonyLevel(Item $item): string
    {
        $result = 0;

        /** @var HarmonyOption $harmonyOption */
        if (($harmonyOption = $item->getHarmonyOptions()->getOption()) !== null) {
            $level = $this
                ->getGroupHarmonyOptionByGropAndHarmonyOption($item->getItemInfo()->getGroup()->getId(), $harmonyOption, true)
                ->getLowestLevel();
            $item->getHarmonyOptions()->setLevel($level);
            $result = $level->getValue();
        }

        return $this->setHexValue($item->getHex(), 21, 1, $result);
    }

    private function getGroupHarmonyOptionByGroupAndType(int $itemGroup, int $harmonyOptionType, bool $throwNotFoundException = false)
    {
        $result = $this->groupHarmonyOptionRepo
            ->findByItemGroupAndHarmonyType($itemGroup, $harmonyOptionType);

        if ($result === null && $throwNotFoundException) {
            throw new ApplicationException(
                sprintf(
                    'Cannot determine GroupHarmonyOption by given group (%d) and harmony option type (%d).',
                    $itemGroup,
                    $harmonyOptionType
                )
            );
        }

        return $result;
    }

    private function getGroupHarmonyOptionByGropAndHarmonyOption(int $itemGroup, HarmonyOption $harmonyOption, bool $throwNotFoundException = false)
    {
        $result = $this->groupHarmonyOptionRepo
            ->findByItemGroupAndHarmonyOption($itemGroup, $harmonyOption);

        if ($result === null && $throwNotFoundException) {
            throw new ApplicationException(
                sprintf(
                    'Cannot determine GroupHarmonyOption by given group (%d) and harmony option (%d).',
                    $itemGroup,
                    $harmonyOption->getId()
                )
            );
        }

        return $result;
    }

    private function getGroupHarmonyOptionLevelByValue(int $value, GroupHarmonyOption $option): GroupHarmonyOptionLevel
    {
        $result = $option->getLevelByValue($value);

        if ($result === null) {
            throw new ApplicationException(
                sprintf(
                    'Cannot determine GroupHarmonyOptionLevel by given value (%d).',
                    $value
                )
            );
        }

        return $result;
    }

    private function getItemHarmonyLevelValue(Item $item): int
    {
        return $this->getHexValue($item->getHex(), 21, 1);
    }

}