<?php

namespace AppBundle\Service\MuOnline\Item;


use AppBundle\Doctrine\Type\Mappings\Items\Item;
use AppBundle\Doctrine\Type\Mappings\Items\SerialNumber;

class GeneralOptionsParser extends OptionsParser
{

    public function doBuild(Item $item): Item
    {
        $generalOptions = $item->getGeneralOptions();

        $item
            ->setHex($this->setGroup($item, $generalOptions->getGroup()))
            ->setHex($this->setId($item, $generalOptions->getId()))
            ->setHex($this->setSerial($item, $generalOptions->getSerialNumber()))
            ->setHex($this->setLevel($item, $generalOptions->getLvl()))
            ->setHex($this->setOption($item, $generalOptions->getOpt()))
            ->setHex($this->setSkill($item, $generalOptions->getIsSkill()))
            ->setHex($this->setLuck($item, $generalOptions->getIsLuck()))
            ->setHex($this->setDurability($item, $generalOptions->getDurability()));

        return $item;
    }

    public function doParse(Item $item): Item
    {
        $generalOptions = $item->getGeneralOptions();

        $optionsDescriptor = $this->getLevelSkillLuckDescriptor($item);

        $generalOptions
            ->setGroup($this->getGroup($item))
            ->setId($this->getId($item))
            ->setSerial($this->getSerial($item))
            ->setLvl($this->getLevel($optionsDescriptor))
            ->setOpt($this->getOption($item, $optionsDescriptor))
            ->setIsSkill($this->getIsSkill($optionsDescriptor))
            ->setIsLuck($this->getIsLuck($optionsDescriptor))
            ->setDurability($this->getDurability($item));

        return $item;
    }

    private function getGroup(Item $item): int
    {
        return $this->getHexValue($item->getHex(), 18, 1);
    }

    private function setGroup(Item $item, int $group): string
    {
        return $this->setHexValue($item->getHex(), 18, 2, $group * 16);
    }

    private function getId(Item $item): int
    {
        return $this->getHexValue($item->getHex(), 0, 2);
    }

    private function setId(Item $item, int $id): string
    {
        return $this->setHexValue($item->getHex(), 0, 2, $id);
    }

    private function getSerial(Item $item): SerialNumber
    {
        $serial = (new SerialNumber())
            ->setFirst(hexdec(substr($item->getHex(), 6, 8)));

        if ($item->getSize() === Item::ITEM_SIZE_64) {
            $serial->setSecond(hexdec(substr($item->getHex(), 32, 8)));
        }

        return $serial;
    }

    private function setSerial(Item $item, SerialNumber $serial): string
    {
        $item->setHex($this->setHexValue($item->getHex(), 6, 8, $serial->getFirst()));

        if ($item->getSize() === Item::ITEM_SIZE_64) {
            $item->setHex($this->setHexValue($item->getHex(), 32, 8, $serial->getSecond()));
        }

        return $item->getHex();
    }

    private function getLevel(int $descriptor): int
    {
        $descriptor >= 128 and $descriptor -= 128;

        return (int)($descriptor / 8);
    }

    private function setLevel(Item $item, int $level): string
    {
        $levelToSet = $level > 0
            ? $level
            : $item->getItemInfo()->getLevel();

        return $this->setHexValue($item->getHex(), 2, 2, $levelToSet * 8);
    }

    private function getIsSkill(int $descriptor): bool
    {
        return $descriptor >= 128;
    }

    private function setSkill(Item $item, bool $isSkill): string
    {
        $descriptor = $this->getLevelSkillLuckDescriptor($item);

        if ($isSkill) {
            $descriptor += 128;
        }

        return $this->setHexValue($item->getHex(), 2, 2, $descriptor);
    }

    private function getIsLuck(int $descriptor): bool
    {
        return $descriptor % 8 > 3;
    }

    private function setLuck(Item $item, bool $isLuck): string
    {
        $descriptor = $this->getLevelSkillLuckDescriptor($item);

        if ($isLuck) {
            $descriptor += 4;
        }

        return $this->setHexValue($item->getHex(), 2, 2, $descriptor);
    }

    private function getOption(Item $item, int $descriptor): int
    {
        $exc = $this->getHexValue($item->getHex(), 14, 2);
        ($descriptor %= 8) > 3 and $descriptor -= 4;

        $mult = $this->getLifeOptionMultiplicator($item->getGeneralOptions()->getGroup(), $item->getGeneralOptions()->getId());

        $opt = 0;

        if ($exc > 63) {
            $opt = $mult * $descriptor + $mult * 4;
        } elseif ($descriptor > 0) {
            $opt = $mult * $descriptor;
        }

        return $opt;
    }

    private function setOption(Item $item, int $option): string
    {
        $mult = $this->getLifeOptionMultiplicator($item->getGeneralOptions()->getGroup(), $item->getGeneralOptions()->getId());
        $option = (int)($option / $mult);

        $descriptor = $this->getLevelSkillLuckDescriptor($item);

        $item->setHex($this->setHexValue($item->getHex(), 14, 2, '00'));

        if ($option > 0 && $option <= 7) {
            if ($option >= 4) {
                $descriptor += $option - 4;
                $item->setHex($this->setHexValue($item->getHex(), 14, 2, 64));
            } else {
                $descriptor += $option;
            }
        }

        return $this->setHexValue($item->getHex(), 2, 2, $descriptor);
    }

    private function getDurability(Item $item): int
    {
        return $this->getHexValue($item->getHex(), 4, 2);
    }

    private function setDurability(Item $item, int $durability): string
    {
        $value = $item->getItemInfo()->getDurability()
            ? $item->getItemInfo()->getDurability()
            : 255;

        if ($durability) {
            $value = $durability;
        }

        return $this->setHexValue($item->getHex(), 4, 2, $value);
    }

    private function getLevelSkillLuckDescriptor(Item $item): int
    {
        return $this->getHexValue($item->getHex(), 2, 2);
    }

}