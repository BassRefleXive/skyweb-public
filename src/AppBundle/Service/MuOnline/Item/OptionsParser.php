<?php

namespace AppBundle\Service\MuOnline\Item;

use AppBundle\Doctrine\Type\Mappings\Items\Item;
use AppBundle\Entity\Application\Items\ItemInfo;
use AppBundle\Exception\ApplicationException;
use AppBundle\Repository\Application\ItemInfoRepository;
use Doctrine\ORM\EntityManager;

abstract class OptionsParser
{
    /** @var  ItemInfoRepository */
    protected $itemInfoRepo;

    /** @var OptionsParser */
    protected $next;

    public function __construct(ItemInfoRepository $itemInfoRepo)
    {
        $this->itemInfoRepo = $itemInfoRepo;
    }

    public function setNext(OptionsParser $next)
    {
        $this->next = $next;
    }

    public function build(Item $item): Item
    {
        $item = $this->setItemInfo($item)->doBuild($item);

        if (is_null($this->next)) {
            return $item;
        }

        return $this->next->build($item);
    }

    public function parse(Item $item): Item
    {
        $item = $this->setItemInfo($item)->doParse($item);

        if (is_null($this->next)) {
            return $item;
        }

        return $this->next->parse($item);
    }

    protected function setHexValue(string $hex, int $start, int $length, int $value): string
    {
        return substr_replace($hex, sprintf('%0' . $length . 'X', $value, str_pad('', $length, 0)), $start, $length);
    }

    protected function getHexValue(string $hex, int $start, int $length): int
    {
        return hexdec($this->extractHexString($hex, $start, $length));
    }

    protected function extractHexString(string $hex, int $start, int $length): string
    {
        return substr($hex, $start, $length);
    }

    protected function getLifeOptionMultiplicator(int $group, int $id): int
    {
        return ($group !== 13) || ($group === 13 && $id === 30)
            ? 4
            : 1;
    }

    private function setItemInfo(Item $item): OptionsParser
    {
        if (!$item->getGeneralOptions()->isParsed()) {
            return $this;
        }

        if ($itemInfo = $item->getItemInfo()) {
            return $this;
        }

        $itemInfo = $this->itemInfoRepo->findByItem($item);

        if ($itemInfo === null) {
            throw new ApplicationException(
                sprintf(
                    'ItemInfo not found for item with group %d and id %d',
                    $item->getGeneralOptions()->getGroup(),
                    $item->getGeneralOptions()->getId()
                )
            );
        }

        $item->setItemInfo($itemInfo)->setParsed();

        return $this;
    }

    abstract public function doParse(Item $item): Item;

    abstract public function doBuild(Item $item): Item;
}