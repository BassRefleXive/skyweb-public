<?php

namespace AppBundle\Service\MuOnline\Item;


use AppBundle\Doctrine\Type\Mappings\Items\Item;
use Doctrine\ORM\EntityManager;

class ComputedOptionsParser extends OptionsParser
{

    public function doBuild(Item $item): Item
    {
        return $this->doParse($item);
    }

    public function doParse(Item $item): Item
    {
        $this
            ->setDamage($item)
            ->setDurability($item)
            ->setRequirements($item)
            ->setDefense($item);

        return $item;
    }

    private function setDefense(Item $item): ComputedOptionsParser
    {
        $itemInfo = $item->getItemInfo();

        if ($itemInfo->getDefense()) {
            $itemLevel = $item->getGeneralOptions()->getLvl();

            $defense = $itemInfo->getDefense();

            if ($itemInfo->getGroup()->isShields()) {
                $defense += $itemLevel;
            } else {
                for ($i = 1; $i <= $itemLevel; $i++) {
                    $defense += $i <= 9
                        ? 3
                        : $i - 9 + 3;
                }
            }

            $item->getComputedOptions()->setDefense($defense);
        }

        return $this;
    }

    private function setRequirements(Item $item): ComputedOptionsParser
    {
        $itemInfo = $item->getItemInfo();

        $itemLevel = $item->getGeneralOptions()->getLvl();
        $dropLevel = $itemInfo->getDropLevel();
        $reqStr = $itemInfo->getReqStr();
        $reqAgi = $itemInfo->getReqAgi();
        $reqEne = $itemInfo->getReqEne();

        if ($dropLevel) {
            if ($reqStr) {
                $item->getComputedOptions()->setReqStr((int)(($reqStr * ($dropLevel + $itemLevel * 3) * 3) / 100 + 20));
            }
            if ($reqAgi) {
                if ($itemInfo->getGroup()->isWeapons()) {
                    $item->getComputedOptions()->setReqAgi((int)(($reqAgi * ($dropLevel + $itemLevel * 3) * 3) / 100 + 20));
                } else {
                    $item->getComputedOptions()->setReqAgi((int)((0.03 * $dropLevel * $reqAgi) + 20 + 0.09 * $itemLevel * $reqAgi));
                }
            }
            if ($reqEne && $itemInfo->getGroup()->isScrolls()) {
                $item->getComputedOptions()->setReqEne($reqEne);
            }
        }

        return $this;
    }

    private function setDamage(Item $item): ComputedOptionsParser
    {
        $itemInfo = $item->getItemInfo();

        if ($itemInfo->getGroup()->isWeapons()) {
            $dmg = 0;
            $wizDmg = null;

            for ($i = 1; $i <= $item->getGeneralOptions()->getLvl(); $i++) {
                $dmg += $i <= 9
                    ? 3
                    : $i - 9 + 3;
            }

            if ($itemInfo->getWizardryDmg()) {
                $wizDmg = $this->calculateWizardryDamage($itemInfo->getWizardryDmg(), $item->getGeneralOptions()->getLvl());
            }

            if ($item->getExcellentOptions()->isExcellent()) {
                $dmg += 30;

                if ($itemInfo->getWizardryExcellentDmg()) {
                    $wizDmg = $this->calculateWizardryDamage($itemInfo->getWizardryExcellentDmg(), $item->getGeneralOptions()->getLvl());
                }
            }

            $item->getComputedOptions()
                 ->setMinDmg($dmg + $itemInfo->getMinDmg())
                 ->setMaxDmg($dmg + $itemInfo->getMaxDmg());

            $wizDmg && $item->getComputedOptions()->setWizardryDmg($wizDmg);

        }

        return $this;
    }

    private function calculateWizardryDamage(int $initialDamage, int $itemLevel)
    {
        $wizDmg = $initialDamage / 2;

        for ($i = 0; $i < $itemLevel; $i++) {
            if ($i < 10) {
                if ($i % 2 === 0) {
                    $wizDmg += 3;
                } else {
                    $wizDmg += 4;
                }
            } else {
                $wizDmg += 5;
            }
        }

        return $wizDmg;
    }

    private function setDurability(Item $item): ComputedOptionsParser
    {
        $itemInfo = $item->getItemInfo();


        if ($itemInfo->getDurability()) {
            $result = $itemInfo->getDurability();

            if (!in_array($itemInfo->getItemId(), $itemInfo->getGroup()->getCountableItemIds())) {
                for ($i = 1; $i <= $item->getGeneralOptions()->getLvl(); $i++) {
                    if ($i <= 5) {
                        $result += 1;
                    } elseif ($i <= 8) {
                        $result += 2;
                    } else {
                        $result += $i - 7;
                    }
                }

                if ($item->getGeneralOptions()->getDurability() > $result) {
                    $result = $item->getGeneralOptions()->getDurability();
                }

            } else {
                $item->getComputedOptions()->setIsCountable(true);
            }

            $item->getComputedOptions()->setDurability($result);

        }

        return $this;
    }


}