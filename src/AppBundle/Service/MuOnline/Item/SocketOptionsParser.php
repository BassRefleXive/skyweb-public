<?php


namespace AppBundle\Service\MuOnline\Item;


use AppBundle\Doctrine\Type\Mappings\Items\Builder\SocketOptionsBuilder;
use AppBundle\Doctrine\Type\Mappings\Items\Item;
use AppBundle\Doctrine\Type\Mappings\Items\SocketOptions;
use AppBundle\Entity\Application\Items\SocketOption;
use AppBundle\Repository\Application\Item\SocketOptionsRepository;
use AppBundle\Repository\Application\ItemInfoRepository;

class SocketOptionsParser extends OptionsParser
{
    private const EMPTY_0 = '0000000000';
    private const EMPTY_1 = 'FFFFFFFFFF';

    private $socketOptionsRepository;
    private $socketOptions;

    public function __construct(ItemInfoRepository $itemInfoRepo, SocketOptionsRepository $socketOptionsRepository)
    {
        parent::__construct($itemInfoRepo);

        $this->socketOptionsRepository = $socketOptionsRepository;
    }

    public function doBuild(Item $item): Item
    {
        return $item;
    }

    public function doParse(Item $item): Item
    {
        return $item->setSocketOptions($this->extractSocketOptions($item));
    }

    private function extractSocketOptions(Item $item): SocketOptions
    {
        $builder = new SocketOptionsBuilder();

        $options = $this->extractHexString($item->getHex(), 22, 10);

        if (in_array($options, [self::EMPTY_0, self::EMPTY_1], true)) {
            return $builder->build();
        }

        $slotIdx = 0;
        do {
            if (null !== $option = $this->socketOption($this->getHexValue($options, $slotIdx * 2, 2))) {
                $builder->withSlot($slotIdx, $option);
            }

            ++$slotIdx;
        } while ($slotIdx <= 4);

        return $builder->build();
    }

    private function socketOption(int $option): ?SocketOption
    {
        if (null === $this->socketOptions) {
            $this->socketOptions = $this->socketOptionsRepository->all();
        }

        return $this->socketOptions->get($option)->getOrElse(null);
    }
}