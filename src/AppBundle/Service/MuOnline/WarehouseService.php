<?php

namespace AppBundle\Service\MuOnline;


use AppBundle\Doctrine\Type\Mappings\Items\Item;
use AppBundle\Entity\Application\Server;
use AppBundle\Entity\Application\UserServerAccount;
use AppBundle\Entity\MuOnline\Account;
use AppBundle\Entity\MuOnline\Warehouse;
use AppBundle\Exception\Application\DisplayableException;
use AppBundle\Exception\ApplicationException;
use AppBundle\Exception\MuOnline\WarehouseFullException;
use AppBundle\Service\Application\Cache\Storage\AbstractCacheStorage;
use AppBundle\Service\Application\ServerService;
use AppBundle\Service\MuOnline\Item\OptionsParserChain;
use Doctrine\ORM\EntityManager;
use AppBundle\Doctrine\Type\Mappings\Items\Items as StorageItems;
use Psr\Log\LoggerInterface;

class WarehouseService
{

    const WAREHOUSE_LENGTH_KEY = 'SERVER_WAREHOUSE_LENGTH';

    private $em;
    private $serverService;
    private $optionsParser;
    private $accountService;
    private $redisCache;
    private $logger;
    private $serverWarehouseLengthCacheTtl;

    public function __construct(
        EntityManager $em,
        ServerService $serverService,
        AccountService $accountService,
        OptionsParserChain $optionsParser,
        AbstractCacheStorage $redisCache,
        LoggerInterface $logger,
        int $serverWarehouseLengthCacheTtl
    )
    {
        $this->em = $em;
        $this->serverService = $serverService;
        $this->optionsParser = $optionsParser;
        $this->accountService = $accountService;
        $this->redisCache = $redisCache;
        $this->logger = $logger;
        $this->serverWarehouseLengthCacheTtl = $serverWarehouseLengthCacheTtl;
    }

    public function buildEmptyStorage(): StorageItems
    {
        $server = $this->serverService->getCurrentServer();

        $cacheKey = $this->getWarehouseLengthCacheKey($server);

        if ($this->redisCache->has($cacheKey)) {
            $length = $this->redisCache->get($cacheKey);
        } else {
            $manager = $this->serverService->getServerDataBaseManager($server);

            $stmt = $manager->getConnection()->prepare('SELECT character_maximum_length as len FROM information_schema.columns WHERE table_name = \'warehouse\' AND column_name = \'Items\'');
            $stmt->execute();
            $result = array_values($stmt->fetch());

            if (count($result) !== 1) {
                throw new ApplicationException('Expected that warehouse.Items length query will return exactly one result.');
            }

            if (!is_integer($result[0])) {
                throw new ApplicationException('Expected that warehouse.Items length query will return integer.');
            }

            $length = $result[0];

            $this->redisCache->set($cacheKey, $length, $this->serverWarehouseLengthCacheTtl);
        }

        return StorageItems::buildEmpty($length);
    }

    public function add(UserServerAccount $userServerAccount, Item $item)
    {
        $this->logItemAction('Item add request.', $userServerAccount, $item);

        if (!$item->isParsed()) {
            $item = $this->optionsParser->parse($item);
        }

        $account = $this->accountService->getAccountByUserServerAccount($userServerAccount);

        $items = $this->getParsedItems($account);

        if ($items->addToStorage($item) === false) {
            $this->logItemAction('Warehouse is full.', $userServerAccount, $item);

            throw new WarehouseFullException();
        }

        $account->getWarehouse()->setItems($items);

        $this->logItemAction('Item add start.', $userServerAccount, $item);

        $this->save($account->getWarehouse());

        $this->logItemAction('Item added.', $userServerAccount, $item);
    }

    public function remove(UserServerAccount $userServerAccount, Item $item)
    {
        $this->logItemAction('Item remove request.', $userServerAccount, $item);

        if (!$item->isParsed()) {
            $item = $this->optionsParser->parse($item);
        }

        $account = $this->accountService->getAccountByUserServerAccount($userServerAccount);

        $items = $this->getParsedItems($account);

        if ($items->removeFromStorage($item) === false) {
            $this->logItemAction('Item not found in warehouse.', $userServerAccount, $item);

            throw new DisplayableException('errors.19');
        }

        $account->getWarehouse()->setItems($items);

        $this->logItemAction('Item remove start.', $userServerAccount, $item);

        $this->save($account->getWarehouse());

        $this->logItemAction('Item removed.', $userServerAccount, $item);
    }

    public function save(Warehouse $warehouse): void
    {
        $manager = $this->serverService->getCurrentServerDataBaseManager();

        $manager->persist($warehouse);
        $manager->flush($warehouse);
    }

    /**
     * @param Account $account
     * @return Item[]
     */
    public function getParsedItems(Account $account)
    {
        $items = $account->getWarehouse()->getItems();

        $items = $items->map(function (Item $item) {
            return $this->optionsParser->parse($item);
        });

        return $items;
    }

    public function findItemBySerial(UserServerAccount $userServerAccount, int $serial)
    {
        $account = $this->accountService->getAccountByUserServerAccount($userServerAccount);

        $items = $this->getParsedItems($account);

        return $items
            ->filter(function (Item $item) use ($serial) {
                return $item->getGeneralOptions()->getSerialNumber()->compound() === $serial;
            })
            ->headOption()
            ->getOrElse(null);
    }

    private function getWarehouseLengthCacheKey(Server $server)
    {
        return WarehouseService::WAREHOUSE_LENGTH_KEY . '_' . $server->getHash();
    }

    private function logItemAction(string $action, UserServerAccount $account, Item $item): void
    {
        $this->logger->info($action, [
            'user_id' => $account->getUser()->getId(),
            'user_server_account_id' => $account->getId(),
            'item' => [
                'serial' => [
                    'first' => $item->getGeneralOptions()->getSerialNumber()->getFirst(),
                    'second' => $item->getGeneralOptions()->getSerialNumber()->getSecond(),
                    'compound' => $item->getGeneralOptions()->getSerialNumber()->compound(),
                ],
                'hex' => $item->getHex(),
                'name' => $item->getItemInfo()->getName(),
            ]
        ]);
    }
}