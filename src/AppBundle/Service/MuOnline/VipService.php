<?php


namespace AppBundle\Service\MuOnline;


use AppBundle\Entity\Application\Config\VipLevelConfig;
use AppBundle\Entity\Application\UserServerAccount;
use AppBundle\Model\Application\Account\PurchaseVipModel;
use AppBundle\Service\Application\ServerService;

class VipService
{
    private $serverService;
    private $accountService;

    public function __construct(ServerService $serverService, AccountService $accountService)
    {
        $this->serverService = $serverService;
        $this->accountService = $accountService;
    }

    public function activate(VipLevelConfig $vipLevel, int $days, UserServerAccount $userServerAccount): void
    {
        $account = $this->accountService->getAccountByUserServerAccount($userServerAccount);

        $manager = $this->serverService->getServerDataBaseManager($userServerAccount->getServer());

        $account->activateVip($vipLevel, $days);

        $manager->persist($account->getVipState());
        $manager->flush($account->getVipState());
    }

    public function extend(VipLevelConfig $vipLevel, PurchaseVipModel $model, UserServerAccount $userServerAccount): void
    {
        $this->activate($vipLevel, $model->days(), $userServerAccount);
    }
}