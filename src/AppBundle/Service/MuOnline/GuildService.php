<?php

namespace AppBundle\Service\MuOnline;


use AppBundle\Entity\Application\Server;
use AppBundle\Entity\MuOnline\Guild;
use AppBundle\Entity\MuOnline\GuildMember;
use AppBundle\Filter\GuildFilter;
use AppBundle\Filter\GuildMemberFilter;
use AppBundle\Repository\MuOnline\GuildRepository;
use AppBundle\Service\Application\ServerService;
use Collection\Sequence;
use Doctrine\ORM\Tools\Pagination\Paginator;

class GuildService
{
    protected $serverService;
    protected $guildCacheTtl;

    public function __construct(ServerService $serverService, int $guildCacheTtl)
    {
        $this->serverService = $serverService;
        $this->guildCacheTtl = $guildCacheTtl;
    }

    public function getByFilter(GuildFilter $filter, Server $server = null): array
    {
        if (!$server) {
            $server = $this->serverService->getCurrentServer();
        }

        $manager = $this->serverService->getServerDataBaseManager($server);
        /** @var GuildRepository $repo */
        $repo = $manager->getRepository(Guild::class);

        $repo->setTtl($this->guildCacheTtl);

        return $repo->findByQueryFilter($filter);
    }

    public function findByQueryFilerPaginated(GuildFilter $filter, Server $server = null): Paginator
    {
        if (!$server) {
            $server = $this->serverService->getCurrentServer();
        }

        $manager = $this->serverService->getServerDataBaseManager($server);
        /** @var GuildRepository $repo */
        $repo = $manager->getRepository(Guild::class);

        $repo->setTtl($this->guildCacheTtl);

        return $repo->findByQueryFilerPaginated($filter);
    }

    public function findMembersByQueryFilerPaginated(GuildMemberFilter $filter, Server $server = null): Paginator
    {
        if (!$server) {
            $server = $this->serverService->getCurrentServer();
        }

        $manager = $this->serverService->getServerDataBaseManager($server);
        /** @var GuildRepository $repo */
        $repo = $manager->getRepository(GuildMember::class);

        $repo->setTtl($this->guildCacheTtl);

        return $repo->findByQueryFilerPaginated($filter);
    }

    public function extractEntities(array $result): array
    {
        return array_map(function ($result) {
            return $result['entity'];
        }, $result);
    }

    public function findByName(string $name)
    {
        $repo = $this->serverService->getCurrentServerDataBaseManager()->getRepository(Guild::class);
        $repo->setTtl($this->guildCacheTtl);

        return $repo->findByName($name);
    }

}