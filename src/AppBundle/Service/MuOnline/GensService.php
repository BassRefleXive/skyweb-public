<?php

namespace AppBundle\Service\MuOnline;

use AppBundle\Entity\Application\Server;
use AppBundle\Entity\MuOnline\Gens;
use AppBundle\Filter\GensFilter;
use AppBundle\Repository\MuOnline\CharacterRepository;
use AppBundle\Service\Application\ServerService;
use Doctrine\ORM\Tools\Pagination\Paginator;

class GensService
{
    private $serverService;
    private $gensCacheTtl;

    public function __construct(
        ServerService $serverService,
        int $gensCacheTtl
    )
    {
        $this->serverService = $serverService;
        $this->gensCacheTtl = $gensCacheTtl;
    }

    public function getByFilter(GensFilter $filter, Server $server = null): array
    {
        if (!$server) {
            $server = $this->serverService->getCurrentServer();
        }

        $manager = $this->serverService->getServerDataBaseManager($server);
        /** @var CharacterRepository $repo */
        $repo = $manager->getRepository(Gens::class);

        $repo->setTtl($this->gensCacheTtl);

        return $repo->findByQueryFilter($filter);
    }

    public function getByFilterPaginated(GensFilter $filter, Server $server = null): Paginator
    {
        if (!$server) {
            $server = $this->serverService->getCurrentServer();
        }

        $manager = $this->serverService->getServerDataBaseManager($server);
        /** @var CharacterRepository $repo */
        $repo = $manager->getRepository(Gens::class);

        $repo->setTtl($this->gensCacheTtl);

        return $repo->findByQueryFilerPaginated($filter);
    }
}