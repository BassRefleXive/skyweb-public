<?php

namespace AppBundle\Statistics\OnlinePlayers\Model;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Application\Server;
use AppBundle\Entity\Traits\Timestampable;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Statistics\OnlinePlayers\Repository\OnlinePlayersRepository")
 * @ORM\Table(name="ServerOnlineStatistics")
 */
class OnlinePlayers
{
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="json_array", name="`online_players`")
     */
    private $online;

    /**
     * @ORM\Column(type="integer", name="online_count")
     */
    private $onlineCount;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\Server", inversedBy="onlinePlayersStatistics")
     * @ORM\JoinColumn(name="server_id", referencedColumnName="id")
     */
    private $server;

    public function __construct(Server $server, array $online = [])
    {
        $this->online = $online;
        $this->server = $server;
        $this->onlineCount = count($online);
    }

    public function id(): int
    {
        return $this->id;
    }

    public function online(): array
    {
        return $this->online;
    }

    public function onlineCount(): int
    {
        return $this->onlineCount;
    }

    public function server(): Server
    {
        return $this->server;
    }
}