<?php

namespace AppBundle\Statistics\OnlinePlayers\Service;

use AppBundle\Entity\Application\Server;
use AppBundle\Entity\MuOnline\AccountState;
use AppBundle\Service\Application\ServerService;
use AppBundle\Service\MuOnline\AccountStateService;
use AppBundle\Statistics\OnlinePlayers\Model\OnlinePlayers;
use Collection\Sequence;

class DataCollector
{
    private $serverService;
    private $accountStateService;

    public function __construct(ServerService $serverService, AccountStateService $accountStateService)
    {
        $this->serverService = $serverService;
        $this->accountStateService = $accountStateService;
    }

    public function collect(): Sequence
    {
        $servers = $this->serverService->getList();

        return $servers->map(function (Server $server): OnlinePlayers {
            return new OnlinePlayers(
                $server,
                array_map(function (AccountState $accountState): string {
                    return $accountState->getAccount()->getMemberId();
                }, $this->accountStateService->getOnlineByServer($server))
            );
        });
    }
}