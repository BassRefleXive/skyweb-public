<?php

namespace AppBundle\Statistics\OnlinePlayers\Service;

use AppBundle\Statistics\OnlinePlayers\Model\OnlinePlayers;
use AppBundle\Statistics\OnlinePlayers\Repository\OnlinePlayersRepository;

class DataPersister
{
    private $dataCollector;
    private $repository;

    public function __construct(DataCollector $dataCollector, OnlinePlayersRepository $repository)
    {
        $this->dataCollector = $dataCollector;
        $this->repository = $repository;
    }

    public function collectAndSave(): void
    {
        $this->dataCollector->collect()->map(function (OnlinePlayers $onlinePlayers): void {
            $this->repository->persist($onlinePlayers);
        });

        $this->repository->flush();
    }
}