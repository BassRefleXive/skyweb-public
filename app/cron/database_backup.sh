#!/bin/sh

src="$1"
dst="$2"

dir="$dst"/"$(date +%Y)"/"$(date +%m)"/"$(date +%d)"

mkdir -p $dir

archive_file="$dst"/"$(date +%Y)"/"$(date +%m)"/"$(date +%d)"/"$(date +%T)".tar.gz

echo "Backing up $src to $archive_file"
date
echo

env GZIP=-9 tar cvzf $archive_file $src