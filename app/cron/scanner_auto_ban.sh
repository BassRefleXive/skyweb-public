#!/bin/sh

iptables=`which iptables`
[ -x "$iptables" ] || exit

chain="$1"
file="$2"
[ -f "$file" ] || exit
echo "READ FROM: $file"

"$iptables" -F "$chain"
echo "CHAIN: $chain"
if [ "$?" -eq "0" ]; then
    echo "RESET RULES..."
else
    echo "INVALID CHAIN!"
    exit 1
fi

while read line; do
    ip=`echo "$line" | awk '{ print $3 }'`
    echo "DENY: $ip"
    "$iptables" -A "$chain" -s "$ip" -j DROP
done < "$file"

"$iptables" -A "$chain" -j RETURN
