<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170129183612 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('ALTER TABLE FaqInfo ADD reset_zen_price SMALLINT NOT NULL DEFAULT 0;');
        $this->addSql('ALTER TABLE FaqInfo ADD reset_credits_reward SMALLINT NOT NULL DEFAULT 0;');
        $this->addSql('ALTER TABLE FaqInfo ADD gr_reset SMALLINT NOT NULL DEFAULT 0;');
        $this->addSql('ALTER TABLE FaqInfo ADD gr_stats_reward SMALLINT NOT NULL DEFAULT 0;');
        $this->addSql('ALTER TABLE FaqInfo ADD gr_credits_reward SMALLINT NOT NULL DEFAULT 0;');
        $this->addSql('ALTER TABLE FaqInfo ADD gr_zen_required SMALLINT NOT NULL DEFAULT 0;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('ALTER TABLE FaqInfo DROP reset_zen_price;');
        $this->addSql('ALTER TABLE FaqInfo DROP reset_credits_reward;');
        $this->addSql('ALTER TABLE FaqInfo DROP gr_reset;');
        $this->addSql('ALTER TABLE FaqInfo DROP gr_stats_reward;');
        $this->addSql('ALTER TABLE FaqInfo DROP gr_credits_reward;');
        $this->addSql('ALTER TABLE FaqInfo DROP gr_zen_required;');
    }
}
