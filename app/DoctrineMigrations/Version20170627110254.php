<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170627110254 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_34E8BC9CB1BB7436');
        $this->addSql('CREATE TEMPORARY TABLE __temp__Order AS SELECT id, user_server_account_id, transaction_id, amount, payment_system_amount, cart_amount, currency, payment_method, description, created_at, processed_at, updated_at, state FROM "Order"');
        $this->addSql('DROP TABLE "Order"');
        $this->addSql('CREATE TABLE "Order" (id INTEGER NOT NULL, discount_coupon_id INTEGER DEFAULT NULL, user_server_account_id INTEGER DEFAULT NULL, transaction_id VARCHAR(255) DEFAULT NULL COLLATE BINARY, amount DOUBLE PRECISION NOT NULL, payment_system_amount DOUBLE PRECISION DEFAULT NULL, cart_amount DOUBLE PRECISION DEFAULT NULL, currency VARCHAR(3) NOT NULL COLLATE BINARY, payment_method VARCHAR(255) DEFAULT NULL COLLATE BINARY, description VARCHAR(255) NOT NULL COLLATE BINARY, created_at DATETIME NOT NULL, processed_at DATETIME DEFAULT NULL, updated_at DATETIME NOT NULL, state VARCHAR(255) NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_34E8BC9C61F1DE6 FOREIGN KEY (discount_coupon_id) REFERENCES CartDiscountCoupon (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_34E8BC9CB1BB7436 FOREIGN KEY (user_server_account_id) REFERENCES UserServerAccount (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO "Order" (id, user_server_account_id, transaction_id, amount, payment_system_amount, cart_amount, currency, payment_method, description, created_at, processed_at, updated_at, state) SELECT id, user_server_account_id, transaction_id, amount, payment_system_amount, cart_amount, currency, payment_method, description, created_at, processed_at, updated_at, state FROM __temp__Order');
        $this->addSql('DROP TABLE __temp__Order');
        $this->addSql('CREATE INDEX IDX_34E8BC9CB1BB7436 ON "Order" (user_server_account_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_34E8BC9C61F1DE6 ON "Order" (discount_coupon_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_34E8BC9C61F1DE6');
        $this->addSql('DROP INDEX IDX_34E8BC9CB1BB7436');
        $this->addSql('CREATE TEMPORARY TABLE __temp__Order AS SELECT id, user_server_account_id, transaction_id, amount, payment_system_amount, cart_amount, currency, payment_method, description, state, processed_at, created_at, updated_at FROM "Order"');
        $this->addSql('DROP TABLE "Order"');
        $this->addSql('CREATE TABLE "Order" (id INTEGER NOT NULL, transaction_id VARCHAR(255) DEFAULT NULL, amount DOUBLE PRECISION NOT NULL, payment_system_amount DOUBLE PRECISION DEFAULT NULL, cart_amount DOUBLE PRECISION DEFAULT NULL, currency VARCHAR(3) NOT NULL, payment_method VARCHAR(255) DEFAULT NULL, description VARCHAR(255) NOT NULL, processed_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, user_server_account_id INTEGER NOT NULL, state VARCHAR(255) NOT NULL COLLATE BINARY, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO "Order" (id, user_server_account_id, transaction_id, amount, payment_system_amount, cart_amount, currency, payment_method, description, state, processed_at, created_at, updated_at) SELECT id, user_server_account_id, transaction_id, amount, payment_system_amount, cart_amount, currency, payment_method, description, state, processed_at, created_at, updated_at FROM __temp__Order');
        $this->addSql('DROP TABLE __temp__Order');
        $this->addSql('CREATE INDEX IDX_34E8BC9CB1BB7436 ON "Order" (user_server_account_id)');
    }
}
