<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171027004630 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('UPDATE ItemInfo SET x = 1 WHERE group_id = 14 AND item_id = 8 AND level = 0;');

        $setsToUpdate = [
            [
                'id' => 49,
                'name' => 'Knight',
                'options' => [
                    'Increase Stamina +30',
                    'Increase Strength +25',
                    'Increase Defensive Skill +30',
                    'Increase Maximum Damage +30',
                    'Increase Critical Damage Rate 10%',
                    'Increase Critical Damage +20',
                    'Increase Excellent Damage Rate 10%',
                    'Increase Excellent Damage +20',
                    'Increase Skill Attacking Rate +30',
                    'Double Damage Rate 9%',
                ],
            ],
            [
                'id' => 50,
                'name' => 'Carnage',
                'options' => [
                    'Increase Strength +60',
                    'Increase Stamina +60',
                    'Increase Defensive Skill +60',
                    'Increase Critical Damage +60',
                    'Increase Excellent Damage +60',
                    'Increase Critical Damage Rate 12%',
                    'Increase Excellent Damage Rate 12%',
                    'Double Damage Rate 10%',
                    'Increase Damage +60',
                    'Ignore Enemy Defensive Skill 1%',
                ],
            ],
            [
                'id' => 51,
                'name' => 'Venom',
                'options' => [
                    'Increase Strength +90',
                    'Increase Stamina +90',
                    'Increase Defensive Skill +90',
                    'Increase Critical Damage +90',
                    'Increase Excellent Damage +90',
                    'Increase Critical Damage Rate 14%',
                    'Increase Excellent Damage Rate 14%',
                    'Double Damage Rate 11%',
                    'Increase Damage +90',
                    'Ignore Enemy Defensive Skill 2%',
                ],
            ],
            [
                'id' => 52,
                'name' => 'Rage',
                'options' => [
                    'Increase Strength +120',
                    'Increase Stamina +120',
                    'Increase Defensive Skill +120',
                    'Increase Maximum Life +120',
                    'Increase Critical Damage +120',
                    'Increase Excellent Damage +120',
                    'Increase Critical Damage Rate +16%',
                    'Increase Excellent Damage Rate +16%',
                    'Double Damage Rate + 13%',
                    'Ignore Enemy Defensive Skill +4%',
                    'Increase Damage with Double Handed Weapon +10%',
                ],
            ],
            [
                'id' => 53,
                'name' => 'Shadow',
                'options' => [
                    'Shadow',
                    'Increase Stamina +30',
                    'Increase Energy +30',
                    'Increase Defensive Skill +35',
                    'Increase AG Increase Rate +10',
                    'Increase Critical Damage Rate 13%',
                    'Increase Critical Damage +25',
                    'Increase Excellent Damage Rate 13%',
                    'Increase Excellent Damage +25',
                    'Double Damage Rate 10%',
                    'Increase Wizardry Damage 25%',
                ],
            ],
            [
                'id' => 54,
                'name' => 'Angel',
                'options' => [
                    'Increase Energy +90',
                    'Increase Stamina +90',
                    'Increase Defensive Skill +90',
                    'Increase Critical Damage +90',
                    'Increase Excellent Damage +90',
                    'Increase Critical Damage Rate 15%',
                    'Increase Excellent Damage Rate 15%',
                    'Double Damage Rate 11%',
                    'Increase Wizardry Damage 30%',
                    'Ignore Enemy Defensive Skill 2%',
                ],
            ],
            [
                'id' => 55,
                'name' => 'Master',
                'options' => [
                    'Increase Energy +120',
                    'Increase Stamina +120',
                    'Increase Defensive Skill +120',
                    'Increase Maximum Life +120',
                    'Increase Critical Damage +120',
                    'Increase Excellent Damage +120',
                    'Increase Critical Damage Rate +17',
                    'Increase Excellent Damage Rate +17%',
                    'Double Damage Rate + 13%',
                    'Ignore Enemy Defensive Skill +4%',
                    'Increase Wizardry Damage +35%',
                ],
            ],
            [
                'id' => 62,
                'name' => 'Mystic',
                'options' => [
                    'Increase Strength +60',
                    'Increase Energy +60',
                    'Increase Defensive Skill +60',
                    'Increase Critical Damage +60',
                    'Increase Excellent Damage +60',
                    'Increase Critical Damage Rate 12%',
                    'Increase Excellent Damage Rate 12%',
                    'Double Damage Rate 10%',
                    'Increase Maximum Life +110',
                    'Ignore Enemy Defensive Skill 1%',
                ],
            ],
            [
                'id' => 63,
                'name' => 'Chaos',
                'options' => [
                    'Increase Strength +90',
                    'Increase Energy +90',
                    'Increase Defensive Skill +90',
                    'Increase Critical Damage +90',
                    'Increase Excellent Damage +90',
                    'Increase Critical Damage Rate 14%',
                    'Increase Excellent Damage Rate 14%',
                    'Double Damage Rate 11%',
                    'Increase Maximum Life +110',
                    'Ignore Enemy Defensive Skill 2%',
                ],
            ],
            [
                'id' => 56,
                'name' => 'Fairy',
                'options' => [
                    'Increase Energy +35',
                    'Increase Agility +50',
                    'Increase Defensive Skill +50',
                    'Increase Damage +35',
                    'Increase Critical Damage Rate 15%',
                    'Increase Critical Damage +25',
                    'Increase Excellent Damage Rate 15%',
                    'Increase Excellent Damage +35',
                    'Double Damage Rate 15%',
                    'Ignore Enemy Defensive Skill 10%',
                ],
            ],
        ];

        $stmt = $this->connection->prepare('UPDATE AncientSet SET options = :options, name = :name WHERE id = :id');

        foreach ($setsToUpdate as $set) {
            $stmt->execute([
                'options' => json_encode($set['options']),
                'name' => $set['name'],
                'id' => $set['id'],
            ]);
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('UPDATE ItemInfo SET x = 3 WHERE group_id = 14 AND item_id = 8 AND level = 0;');
    }
}
