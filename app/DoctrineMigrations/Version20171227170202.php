<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171227170202 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE SocketOption (option INTEGER NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(option))');

        foreach ($this->socketOptions() as $option => $title) {
            $this->addSql(sprintf('INSERT INTO SocketOption (`option`, `title`) VALUES ("%d", "%s");', $option, $title));
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE SocketOption');
    }

    private function socketOptions(): array
    {
        return [  0 => 'Fire((Level type)Attack/Wizardry increase +19)',
                  1 => 'Fire(Attack speed increase +7)',
                  2 => 'Fire(Maximum attack/Wizardry increase +30)',
                  3 => 'Fire(Minimum attack/Wizardry increase +20)',
                  4 => 'Fire(Attack/Wizardry increase +20)',
                  5 => 'Fire(AG cost decrease +40%)',
                  10 => 'Water(Block rating increase +10%)',
                  11 => 'Water(Defense increase +30)',
                  12 => 'Water(Shield protection increase +7%)',
                  13 => 'Water(Damage reduction +4%)',
                  14 => 'Water(Damage reflection +5%)',
                  16 => 'Ice(Monster destruction for the Life increase +5)',
                  17 => 'Ice(Monster destruction for the Mana increase +5)',
                  18 => 'Ice(Skill attack increase +37)',
                  19 => 'Ice(Attack rating increase +25)',
                  20 => 'Ice(Item durability increase +30%)',
                  21 => 'Wind(Automatic Life recovery increase +8)',
                  22 => 'Wind(Maximum Life increase +4%)',
                  23 => 'Wind(Maximum Mana increase +4%)',
                  24 => 'Wind(Automatic Mana recovery increase +7)',
                  25 => 'Wind(Maximum AG increase +25)',
                  26 => 'Wind(AG value increase +3)',
                  29 => 'Lightning(Excellent damage increase +15)',
                  30 => 'Lighting(Excellent damage rate increase +10%)',
                  31 => 'Lighting(Critical damage increase +30)',
                  32 => 'Lighting(Critical damage rate increase +8%)',
                  36 => 'Ground(Health increase +30)',
                  50 => 'Fire((Level type)Attack/Wizardry increase +20)',
                  51 => 'Fire(Attack speed increase +8)',
                  52 => 'Fire(Maximum attack/Wizardry increase +32)',
                  53 => 'Fire(Minimum attack/Wizardry increase +22)',
                  54 => 'Fire(Attack/Wizardry increase +22)',
                  55 => 'Fire(AG cost decrease +41%)',
                  60 => 'Water(Block rating increase +11%)',
                  61 => 'Water(Defense increase +33)',
                  62 => 'Water(Shield protection increase +10%)',
                  63 => 'Water(Damage reduction +5%)',
                  64 => 'Water(Damage reflection +6%)',
                  66 => 'Ice(Monster destruction for the Life increase +6)',
                  67 => 'Ice(Monster destruction for the Mana increase +6)',
                  68 => 'Ice(Skill attack increase +40)',
                  69 => 'Ice(Attack rating increase +27)',
                  70 => 'Ice(Item durability increase +32%)',
                  71 => 'Wind(Automatic Life recovery increase +10)',
                  72 => 'Wind(Maximum Life increase +5%)',
                  73 => 'Wind(Maximum Mana increase +5%)',
                  74 => 'Wind(Automatic Mana recovery increase +14)',
                  75 => 'Wind(Maximum AG increase +30)',
                  76 => 'Wind(AG value increase +5)',
                  79 => 'Lightning(Excellent damage increase +20)',
                  80 => 'Lighting(Excellent damage rate increase +11%)',
                  81 => 'Lighting(Critical damage increase +32)',
                  82 => 'Lighting(Critical damage rate increase +9%)',
                  89 => 'Ground(Health increase +32)',
                  100 => 'Fire((Level type)Attack/Wizardry increase +21)',
                  101 => 'Fire(Attack speed increase +9)',
                  102 => 'Fire(Maximum attack/Wizardry increase +35)',
                  103 => 'Fire(Minimum attack/Wizardry increase +25)',
                  104 => 'Fire(Attack/Wizardry increase +25)',
                  105 => 'Fire(AG cost decrease +42%)',
                  110 => 'Water(Block rating increase +12%)',
                  111 => 'Water(Defense increase +36)',
                  112 => 'Water(Shield protection increase +15%)',
                  113 => 'Water(Damage reduction +6%)',
                  114 => 'Water(Damage reflection +7%)',
                  116 => 'Ice(Monster destruction for the Life increase +7)',
                  117 => 'Ice(Monster destruction for the Mana increase +7)',
                  118 => 'Ice(Skill attack increase +45)',
                  119 => 'Ice(Attack rating increase +30)',
                  120 => 'Ice(Item durability increase +34%)',
                  121 => 'Wind(Automatic Life recovery increase +13)',
                  122 => 'Wind(Maximum Life increase +6%)',
                  123 => 'Wind(Maximum Mana increase +6%)',
                  124 => 'Wind(Automatic Mana recovery increase +21)',
                  125 => 'Wind(Maximum AG increase +35)',
                  126 => 'Wind(AG value increase +7)',
                  129 => 'Lightning(Excellent damage increase +25)',
                  130 => 'Lighting(Excellent damage rate increase +12%)',
                  131 => 'Lighting(Critical damage increase +35)',
                  132 => 'Lighting(Critical damage rate increase +10%)',
                  136 => 'Ground(Health increase +34)',
                  150 => 'Fire((Level type)Attack/Wizardry increase +22)',
                  151 => 'Fire(Attack speed increase +10)',
                  152 => 'Fire(Maximum attack/Wizardry increase +40)',
                  153 => 'Fire(Minimum attack/Wizardry increase +30)',
                  154 => 'Fire(Attack/Wizardry increase +30)',
                  155 => 'Fire(AG cost decrease +43%)',
                  160 => 'Water(Block rating increase +13%)',
                  161 => 'Water(Defense increase +39)',
                  162 => 'Water(Shield protection increase +20%)',
                  163 => 'Water(Damage reduction +7%)',
                  164 => 'Water(Damage reflection +8%)',
                  166 => 'Ice(Monster destruction for the Life increase +9)',
                  167 => 'Ice(Monster destruction for the Mana increase +9)',
                  168 => 'Ice(Skill attack increase +50)',
                  169 => 'Ice(Attack rating increase +35)',
                  170 => 'Ice(Item durability increase +36%)',
                  171 => 'Wind(Automatic Life recovery increase +16)',
                  172 => 'Wind(Maximum Life increase +7%)',
                  173 => 'Wind(Maximum Mana increase +7%)',
                  174 => 'Wind(Automatic Mana recovery increase +28)',
                  175 => 'Wind(Maximum AG increase +40)',
                  176 => 'Wind(AG value increase +10)',
                  179 => 'Lightning(Excellent damage increase +30)',
                  180 => 'Lighting(Excellent damage rate increase +13%)',
                  181 => 'Lighting(Critical damage increase +40)',
                  182 => 'Lighting(Critical damage rate increase +11%)',
                  186 => 'Ground(Health increase +36)',
                  200 => 'Fire((Level type)Attack/Wizardry increase +27)',
                  201 => 'Fire(Attack speed increase +11)',
                  202 => 'Fire(Maximum attack/Wizardry increase +50)',
                  203 => 'Fire(Minimum attack/Wizardry increase +35)',
                  204 => 'Fire(Attack/Wizardry increase +35)',
                  205 => 'Fire(AG cost decrease +44%)',
                  210 => 'Water(Block rating increase +14%)',
                  211 => 'Water(Defense increase +42)',
                  212 => 'Water(Shield protection increase +30%)',
                  213 => 'Water(Damage reduction +8%)',
                  214 => 'Water(Damage reflection +9%)',
                  216 => 'Ice(Monster destruction for the Life increase +11)',
                  217 => 'Ice(Monster destruction for the Mana increase +11)',
                  218 => 'Ice(Skill attack increase +60)',
                  219 => 'Ice(Attack rating increase +40)',
                  220 => 'Ice(Item durability increase +38%)',
                  221 => 'Wind(Automatic Life recovery increase +20)',
                  222 => 'Wind(Maximum Life increase +8%)',
                  223 => 'Wind(Maximum Mana increase +8%)',
                  224 => 'Wind(Automatic Mana recovery increase +35)',
                  225 => 'Wind(Maximum AG increase +50)',
                  226 => 'Wind(AG value increase +15)',
                  229 => 'Lightning(Excellent damage increase +40)',
                  230 => 'Lighting(Excellent damage rate increase +14%)',
                  231 => 'Lighting(Critical damage increase +50)',
                  232 => 'Lighting(Critical damage rate increase +12%)',
                  236 => 'Ground(Health increase +38)',
        ];
    }
}
