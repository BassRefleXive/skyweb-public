<?php

namespace Application\Migrations;

use AppBundle\Doctrine\Type\HarmonyOptionEnum;
use AppBundle\Doctrine\Type\ItemGroupEnum;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Driver\Statement;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161219145553 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $options = array_unique(
            array_merge(HarmonyOptionEnum::getWeaponTypes(), HarmonyOptionEnum::getStaffTypes(), HarmonyOptionEnum::getArmorTypes())
        );

        $stmt = $this->connection->prepare('INSERT INTO HarmonyOption (name) VALUES (:name)');
        foreach ($options as $name) {
            $stmt->bindParam('name', $name);
            $stmt->execute();
        }

        $stmt = $this->connection->prepare('INSERT INTO GroupHarmonyOption (group_id, harmony_option_id, type) VALUES (
                (SELECT id FROM ItemGroup WHERE type_id = :group_type_id),
                (SELECT id FROM HarmonyOption WHERE name = :harmony_option_name),
                :type
            )');

        $weaponItemGroups = [
            ItemGroupEnum::ITEM_GROUP_SWORD,
            ItemGroupEnum::ITEM_GROUP_AXE,
            ItemGroupEnum::ITEM_GROUP_SCEPTER,
            ItemGroupEnum::ITEM_GROUP_SPEAR,
            ItemGroupEnum::ITEM_GROUP_BOW,
        ];

        $staffItemGroups = [
            ItemGroupEnum::ITEM_GROUP_STAFF,
        ];

        $armorItemGroups = [
            ItemGroupEnum::ITEM_GROUP_SHIELD,
            ItemGroupEnum::ITEM_GROUP_HELM,
            ItemGroupEnum::ITEM_GROUP_ARMOR,
            ItemGroupEnum::ITEM_GROUP_PANTS,
            ItemGroupEnum::ITEM_GROUP_GLOVES,
            ItemGroupEnum::ITEM_GROUP_BOOTS,
        ];

        foreach ($weaponItemGroups as $group) {
            foreach (HarmonyOptionEnum::getWeaponTypes() as $typeId => $name) {
                $stmt->bindParam('group_type_id', $group);
                $stmt->bindParam('harmony_option_name', $name);
                $stmt->bindParam('type', $typeId);
                $stmt->execute();
            }
        }

        foreach ($staffItemGroups as $group) {
            foreach (HarmonyOptionEnum::getStaffTypes() as $typeId => $name) {
                $stmt->bindParam('group_type_id', $group);
                $stmt->bindParam('harmony_option_name', $name);
                $stmt->bindParam('type', $typeId);
                $stmt->execute();
            }
        }

        foreach ($armorItemGroups as $group) {
            foreach (HarmonyOptionEnum::getArmorTypes() as $typeId => $name) {
                $stmt->bindParam('group_type_id', $group);
                $stmt->bindParam('harmony_option_name', $name);
                $stmt->bindParam('type', $typeId);
                $stmt->execute();
            }
        }

        $stmt = $this->connection->prepare('INSERT INTO GroupHarmonyOptionLevel (group_harmony_option_id, value, level) VALUES (
                (
                  SELECT id FROM GroupHarmonyOption WHERE 
                  group_id = (SELECT id FROM ItemGroup WHERE type_id = :group_type_id) AND 
                  harmony_option_id = (SELECT id FROM HarmonyOption WHERE name = :harmony_option_name)
                ),
                :value,
                :level
            )');


        foreach ($weaponItemGroups as $group) {
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MIN_DMG), $group, 0, 2);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MIN_DMG), $group, 1, 3);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MIN_DMG), $group, 2, 4);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MIN_DMG), $group, 3, 5);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MIN_DMG), $group, 4, 6);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MIN_DMG), $group, 5, 7);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MIN_DMG), $group, 6, 9);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MIN_DMG), $group, 7, 11);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MIN_DMG), $group, 8, 12);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MIN_DMG), $group, 9, 14);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MIN_DMG), $group, 10, 15);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MIN_DMG), $group, 11, 16);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MIN_DMG), $group, 12, 17);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MIN_DMG), $group, 13, 20);

            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MAX_DMG), $group, 0, 3);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MAX_DMG), $group, 1, 4);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MAX_DMG), $group, 2, 5);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MAX_DMG), $group, 3, 6);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MAX_DMG), $group, 4, 7);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MAX_DMG), $group, 5, 8);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MAX_DMG), $group, 6, 10);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MAX_DMG), $group, 7, 12);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MAX_DMG), $group, 8, 14);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MAX_DMG), $group, 9, 17);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MAX_DMG), $group, 10, 20);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MAX_DMG), $group, 11, 23);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MAX_DMG), $group, 12, 26);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MAX_DMG), $group, 13, 29);

            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_STR), $group, 0, 6);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_STR), $group, 1, 8);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_STR), $group, 2, 10);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_STR), $group, 3, 12);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_STR), $group, 4, 14);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_STR), $group, 5, 16);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_STR), $group, 6, 20);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_STR), $group, 7, 23);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_STR), $group, 8, 26);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_STR), $group, 9, 29);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_STR), $group, 10, 32);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_STR), $group, 11, 35);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_STR), $group, 12, 37);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_STR), $group, 13, 40);

            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_AGI), $group, 0, 6);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_AGI), $group, 1, 8);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_AGI), $group, 2, 10);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_AGI), $group, 3, 12);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_AGI), $group, 4, 14);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_AGI), $group, 5, 16);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_AGI), $group, 6, 20);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_AGI), $group, 7, 23);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_AGI), $group, 8, 26);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_AGI), $group, 9, 29);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_AGI), $group, 10, 32);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_AGI), $group, 11, 35);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_AGI), $group, 12, 37);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_REDUCE_REQ_AGI), $group, 13, 40);

            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MIN_MAX_DMG), $group, 6, 7);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MIN_MAX_DMG), $group, 7, 8);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MIN_MAX_DMG), $group, 8, 9);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MIN_MAX_DMG), $group, 9, 11);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MIN_MAX_DMG), $group, 10, 12);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MIN_MAX_DMG), $group, 11, 14);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MIN_MAX_DMG), $group, 12, 16);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_MIN_MAX_DMG), $group, 13, 19);

            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_CRITICAL_DMG), $group, 6, 12);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_CRITICAL_DMG), $group, 7, 14);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_CRITICAL_DMG), $group, 8, 16);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_CRITICAL_DMG), $group, 9, 18);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_CRITICAL_DMG), $group, 10, 20);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_CRITICAL_DMG), $group, 11, 22);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_CRITICAL_DMG), $group, 12, 24);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_CRITICAL_DMG), $group, 13, 30);

            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_SKILL_DMG), $group, 9, 12);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_SKILL_DMG), $group, 10, 14);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_SKILL_DMG), $group, 11, 16);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_SKILL_DMG), $group, 12, 18);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_SKILL_DMG), $group, 13, 22);

            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_ATTACK_SUCCESS_RATE_PVP), $group, 9, 5);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_ATTACK_SUCCESS_RATE_PVP), $group, 10, 7);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_ATTACK_SUCCESS_RATE_PVP), $group, 11, 9);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_ATTACK_SUCCESS_RATE_PVP), $group, 12, 11);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_ATTACK_SUCCESS_RATE_PVP), $group, 13, 14);

            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_SD_REDUCTION), $group, 9, 3);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_SD_REDUCTION), $group, 10, 5);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_SD_REDUCTION), $group, 11, 7);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_SD_REDUCTION), $group, 12, 9);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_SD_REDUCTION), $group, 13, 10);

            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getWeaponTypeNameByKey(HarmonyOptionEnum::HARMONY_WEAPON_INC_SD_IGNORE_RATE), $group, 13, 10);
        }

        foreach ($staffItemGroups as $group) {

            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_WIZARDRY), $group, 0, 6);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_WIZARDRY), $group, 1, 8);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_WIZARDRY), $group, 2, 10);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_WIZARDRY), $group, 3, 12);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_WIZARDRY), $group, 4, 14);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_WIZARDRY), $group, 5, 16);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_WIZARDRY), $group, 6, 17);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_WIZARDRY), $group, 7, 18);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_WIZARDRY), $group, 8, 19);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_WIZARDRY), $group, 9, 21);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_WIZARDRY), $group, 10, 23);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_WIZARDRY), $group, 11, 25);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_WIZARDRY), $group, 12, 27);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_WIZARDRY), $group, 13, 31);

            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_STR), $group, 0, 6);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_STR), $group, 1, 8);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_STR), $group, 2, 10);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_STR), $group, 3, 12);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_STR), $group, 4, 14);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_STR), $group, 5, 16);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_STR), $group, 6, 20);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_STR), $group, 7, 23);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_STR), $group, 8, 26);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_STR), $group, 9, 29);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_STR), $group, 10, 32);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_STR), $group, 11, 35);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_STR), $group, 12, 37);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_STR), $group, 13, 40);

            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_AGI), $group, 0, 6);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_AGI), $group, 1, 8);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_AGI), $group, 2, 10);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_AGI), $group, 3, 12);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_AGI), $group, 4, 14);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_AGI), $group, 5, 16);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_AGI), $group, 6, 20);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_AGI), $group, 7, 23);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_AGI), $group, 8, 26);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_AGI), $group, 9, 29);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_AGI), $group, 10, 32);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_AGI), $group, 11, 35);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_AGI), $group, 12, 37);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_REDUCE_REQ_AGI), $group, 13, 40);

            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_SKILL_DMG), $group, 6, 7);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_SKILL_DMG), $group, 7, 10);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_SKILL_DMG), $group, 8, 13);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_SKILL_DMG), $group, 9, 16);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_SKILL_DMG), $group, 10, 19);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_SKILL_DMG), $group, 11, 22);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_SKILL_DMG), $group, 12, 25);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_SKILL_DMG), $group, 13, 30);

            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_CRITICAL_DMG), $group, 6, 10);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_CRITICAL_DMG), $group, 7, 12);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_CRITICAL_DMG), $group, 8, 14);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_CRITICAL_DMG), $group, 9, 16);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_CRITICAL_DMG), $group, 10, 18);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_CRITICAL_DMG), $group, 11, 20);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_CRITICAL_DMG), $group, 12, 22);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_CRITICAL_DMG), $group, 13, 28);

            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_SD_REDUCTION), $group, 9, 4);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_SD_REDUCTION), $group, 10, 6);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_SD_REDUCTION), $group, 11, 8);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_SD_REDUCTION), $group, 12, 10);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_SD_REDUCTION), $group, 13, 13);

            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_ATTACK_SUCCESS_RATE_PVP), $group, 9, 5);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_ATTACK_SUCCESS_RATE_PVP), $group, 10, 7);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_ATTACK_SUCCESS_RATE_PVP), $group, 11, 9);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_ATTACK_SUCCESS_RATE_PVP), $group, 12, 11);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_ATTACK_SUCCESS_RATE_PVP), $group, 13, 14);

            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getStaffTypeNameByKey(HarmonyOptionEnum::HARMONY_STAFF_INC_SD_IGNORE_RATE), $group, 13, 15);
        }

        foreach ($armorItemGroups as $group) {
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_DEFENSE), $group, 0, 3);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_DEFENSE), $group, 1, 4);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_DEFENSE), $group, 2, 5);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_DEFENSE), $group, 3, 6);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_DEFENSE), $group, 4, 7);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_DEFENSE), $group, 5, 8);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_DEFENSE), $group, 6, 10);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_DEFENSE), $group, 7, 12);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_DEFENSE), $group, 8, 14);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_DEFENSE), $group, 9, 16);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_DEFENSE), $group, 10, 18);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_DEFENSE), $group, 11, 20);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_DEFENSE), $group, 12, 22);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_DEFENSE), $group, 13, 25);

            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MAX_AG), $group, 3, 4);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MAX_AG), $group, 4, 6);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MAX_AG), $group, 5, 8);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MAX_AG), $group, 6, 10);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MAX_AG), $group, 7, 12);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MAX_AG), $group, 8, 14);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MAX_AG), $group, 9, 16);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MAX_AG), $group, 10, 18);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MAX_AG), $group, 11, 20);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MAX_AG), $group, 12, 22);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MAX_AG), $group, 13, 25);

            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MAX_HEALTH), $group, 3, 7);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MAX_HEALTH), $group, 4, 9);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MAX_HEALTH), $group, 5, 11);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MAX_HEALTH), $group, 6, 13);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MAX_HEALTH), $group, 7, 15);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MAX_HEALTH), $group, 8, 17);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MAX_HEALTH), $group, 9, 19);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MAX_HEALTH), $group, 10, 21);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MAX_HEALTH), $group, 11, 23);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MAX_HEALTH), $group, 12, 25);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MAX_HEALTH), $group, 13, 30);

            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_HP_REGEN_RATE), $group, 6, 1);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_HP_REGEN_RATE), $group, 7, 2);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_HP_REGEN_RATE), $group, 8, 3);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_HP_REGEN_RATE), $group, 9, 4);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_HP_REGEN_RATE), $group, 10, 5);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_HP_REGEN_RATE), $group, 11, 6);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_HP_REGEN_RATE), $group, 12, 7);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_HP_REGEN_RATE), $group, 13, 8);

            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MANA_REGEN_RATE), $group, 9, 1);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MANA_REGEN_RATE), $group, 10, 2);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MANA_REGEN_RATE), $group, 11, 3);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MANA_REGEN_RATE), $group, 12, 4);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_MANA_REGEN_RATE), $group, 13, 5);

            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_DEF_SUCCESS_RATE_PVP), $group, 9, 3);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_DEF_SUCCESS_RATE_PVP), $group, 10, 4);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_DEF_SUCCESS_RATE_PVP), $group, 11, 5);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_DEF_SUCCESS_RATE_PVP), $group, 12, 6);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_DEF_SUCCESS_RATE_PVP), $group, 13, 8);

            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_DMG_REDUCTION), $group, 9, 3);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_DMG_REDUCTION), $group, 10, 4);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_DMG_REDUCTION), $group, 11, 5);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_DMG_REDUCTION), $group, 12, 6);
            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_DMG_REDUCTION), $group, 13, 7);

            $this->insertGroupHarmonyOptionLevel($stmt, HarmonyOptionEnum::getArmorTypeNameByKey(HarmonyOptionEnum::HARMONY_ARMOR_INC_SD_RATIO_RATE), $group, 13, 5);

        }

    }

    protected function insertGroupHarmonyOptionLevel(Statement $stmt, string $harmonyOptionName, int $groupTypeId, int $value, int $level) {
        $stmt->bindParam('group_type_id', $groupTypeId);
        $stmt->bindParam('harmony_option_name', $harmonyOptionName);
        $stmt->bindParam('value', $value);
        $stmt->bindParam('level', $level);
        $stmt->execute();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DELETE FROM GroupHarmonyOptionLevel');
        $this->addSql('DELETE FROM GroupHarmonyOption');
        $this->addSql('DELETE FROM HarmonyOption');
    }
}
