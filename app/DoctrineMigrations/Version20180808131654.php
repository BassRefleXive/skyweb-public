<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180808131654 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('UPDATE ItemInfo SET `x`=1 WHERE `group_id`=1 AND `item_id`=18;');
        $this->addSql('UPDATE ItemInfo SET `x`=1 WHERE `group_id`=1 AND `item_id`=21;');
        $this->addSql('UPDATE ItemInfo SET `x`=1 WHERE `group_id`=1 AND `item_id`=23;');
        $this->addSql('UPDATE ItemInfo SET `x`=1 WHERE `group_id`=1 AND `item_id`=25;');
        $this->addSql('UPDATE ItemInfo SET `x`=1 WHERE `group_id`=1 AND `item_id`=28;');
        $this->addSql('UPDATE ItemInfo SET `x`=1 WHERE `group_id`=1 AND `item_id`=31;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('UPDATE ItemInfo SET `x`=2 WHERE `group_id`=1 AND `item_id`=18;');
        $this->addSql('UPDATE ItemInfo SET `x`=2 WHERE `group_id`=1 AND `item_id`=21;');
        $this->addSql('UPDATE ItemInfo SET `x`=2 WHERE `group_id`=1 AND `item_id`=23;');
        $this->addSql('UPDATE ItemInfo SET `x`=2 WHERE `group_id`=1 AND `item_id`=25;');
        $this->addSql('UPDATE ItemInfo SET `x`=2 WHERE `group_id`=1 AND `item_id`=28;');
        $this->addSql('UPDATE ItemInfo SET `x`=2 WHERE `group_id`=1 AND `item_id`=31;');
    }
}
