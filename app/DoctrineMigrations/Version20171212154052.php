<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171212154052 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('UPDATE ItemInfo SET durability=10 WHERE group_id=15 AND item_id=88;');
        $this->addSql('UPDATE ItemInfo SET durability=30 WHERE group_id=15 AND item_id=89;');
        $this->addSql('UPDATE ItemInfo SET durability=255 WHERE group_id=15 AND item_id=90;');
        $this->addSql('UPDATE ItemInfo SET durability=5 WHERE group_id=15 AND item_id=29;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('UPDATE ItemInfo SET durability=NULL WHERE group_id=15 AND item_id=88;');
        $this->addSql('UPDATE ItemInfo SET durability=NULL WHERE group_id=15 AND item_id=89;');
        $this->addSql('UPDATE ItemInfo SET durability=NULL WHERE group_id=15 AND item_id=90;');
        $this->addSql('UPDATE ItemInfo SET durability=NULL WHERE group_id=15 AND item_id=29;');
    }
}
