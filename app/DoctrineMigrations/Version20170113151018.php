<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170113151018 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');


        $this->addSql('CREATE TABLE StatsPurchaseLogItem (id INTEGER NOT NULL, cart_purchase_item_log_id INTEGER DEFAULT NULL, stats INTEGER NOT NULL, price_credits SMALLINT NOT NULL, price_money SMALLINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_223FBA7439A45F57 ON StatsPurchaseLogItem (cart_purchase_item_log_id)');
        $this->addSql('CREATE TABLE CartPersistentDiscount (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, type VARCHAR(255) NOT NULL, discount INTEGER NOT NULL, spent_from INTEGER NOT NULL, spent_to INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_536BAB1C1844E6B7 ON CartPersistentDiscount (server_id)');
        $this->addSql('CREATE UNIQUE INDEX CartPersistentDiscount_range_UNIQUE ON CartPersistentDiscount (server_id, spent_from, spent_to)');
        $this->addSql('CREATE TABLE CartDiscountCoupon (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, coupon VARCHAR(5) NOT NULL, discount INTEGER NOT NULL, type VARCHAR(255) NOT NULL, status INTEGER NOT NULL, expire_at DATETIME NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C96BD8971844E6B7 ON CartDiscountCoupon (server_id)');
        $this->addSql('CREATE UNIQUE INDEX CartDiscountCoupon_coupon_UNIQUE ON CartDiscountCoupon (coupon)');
        $this->addSql('CREATE TABLE CartPurchaseDiscount (id INTEGER NOT NULL, cart_purchase_item_log_id INTEGER DEFAULT NULL, cart_discount_coupon_id INTEGER DEFAULT NULL, type VARCHAR(255) NOT NULL, discount_percent INTEGER NOT NULL, discount_amount DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7E6AB73639A45F57 ON CartPurchaseDiscount (cart_purchase_item_log_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7E6AB7366764858B ON CartPurchaseDiscount (cart_discount_coupon_id)');
        $this->addSql('CREATE TABLE StatsShopConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, step SMALLINT NOT NULL, step_price_credits INTEGER NOT NULL, step_price_money INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1AC2CB721844E6B7 ON StatsShopConfig (server_id)');
        $this->addSql('CREATE TABLE CartPurchaseLog (id INTEGER NOT NULL, user_server_account_id INTEGER DEFAULT NULL, price_credits SMALLINT NOT NULL, price_money DOUBLE PRECISION NOT NULL, type VARCHAR(255) NOT NULL, final_price DOUBLE PRECISION NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_89C4646FB1BB7436 ON CartPurchaseLog (user_server_account_id)');
        $this->addSql('DROP TABLE WebShopDiscountCoupon');
        $this->addSql('DROP TABLE WebShopPersistentDiscount');
        $this->addSql('DROP TABLE WebShopPurchaseDiscount');
        $this->addSql('DROP TABLE WebShopPurchaseLog');


        $this->addSql('DROP INDEX IDX_758ED6D9A76ED395');
        $this->addSql('DROP INDEX IDX_758ED6D91844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__UserServerAccount AS SELECT id, user_id, server_id, selected_at, spent_money, spent_credits, account_id FROM UserServerAccount');
        $this->addSql('DROP TABLE UserServerAccount');
        $this->addSql('CREATE TABLE UserServerAccount (id INTEGER NOT NULL, user_id INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, selected_at DATETIME NOT NULL, spent_money DOUBLE PRECISION NOT NULL, spent_credits INTEGER NOT NULL, account_id VARCHAR(10) NOT NULL COLLATE BINARY, purchased_stats INTEGER NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_758ED6D9A76ED395 FOREIGN KEY (user_id) REFERENCES User (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_758ED6D91844E6B7 FOREIGN KEY (server_id) REFERENCES Server (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO UserServerAccount (id, user_id, server_id, selected_at, spent_money, spent_credits, account_id, purchased_stats) SELECT id, user_id, server_id, selected_at, spent_money, spent_credits, account_id, 0 FROM __temp__UserServerAccount');
        $this->addSql('DROP TABLE __temp__UserServerAccount');
        $this->addSql('CREATE INDEX IDX_758ED6D9A76ED395 ON UserServerAccount (user_id)');
        $this->addSql('CREATE INDEX IDX_758ED6D91844E6B7 ON UserServerAccount (server_id)');


        $this->addSql('DROP INDEX WebShopPurchaseLogItem_serial_UNIQUE');
        $this->addSql('DROP INDEX IDX_ED5E349C963592CE');
        $this->addSql('CREATE TEMPORARY TABLE __temp__WebShopPurchaseLogItem AS SELECT id, webshop_purchase_item_log_id, item, price_credits, price_money, first_serial, second_serial FROM WebShopPurchaseLogItem');
        $this->addSql('DROP TABLE WebShopPurchaseLogItem');
        $this->addSql('CREATE TABLE WebShopPurchaseLogItem (id INTEGER NOT NULL, cart_purchase_item_log_id INTEGER DEFAULT NULL, item VARCHAR(255) NOT NULL COLLATE BINARY, price_credits SMALLINT NOT NULL, price_money DOUBLE PRECISION NOT NULL, first_serial INTEGER NOT NULL, second_serial INTEGER DEFAULT NULL, PRIMARY KEY(id), CONSTRAINT FK_ED5E349C39A45F57 FOREIGN KEY (cart_purchase_item_log_id) REFERENCES CartPurchaseLog (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO WebShopPurchaseLogItem (id, cart_purchase_item_log_id, item, price_credits, price_money, first_serial, second_serial) SELECT id, webshop_purchase_item_log_id, item, price_credits, price_money, first_serial, second_serial FROM __temp__WebShopPurchaseLogItem');
        $this->addSql('DROP TABLE __temp__WebShopPurchaseLogItem');
        $this->addSql('CREATE UNIQUE INDEX WebShopPurchaseLogItem_serial_UNIQUE ON WebShopPurchaseLogItem (first_serial, second_serial)');
        $this->addSql('CREATE INDEX IDX_ED5E349C39A45F57 ON WebShopPurchaseLogItem (cart_purchase_item_log_id)');

        $this->addSql('DROP INDEX IDX_3B24E2CF1AD5CDBF');
        $this->addSql('CREATE TEMPORARY TABLE __temp__CartItem AS SELECT id, cart_id, item, price_credits, price_money FROM CartItem');
        $this->addSql('DROP TABLE CartItem');
        $this->addSql('CREATE TABLE CartItem (id INTEGER NOT NULL, cart_id INTEGER DEFAULT NULL, price_credits SMALLINT NOT NULL, price_money DOUBLE PRECISION NOT NULL, item VARCHAR(255) DEFAULT NULL, stats INTEGER DEFAULT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_3B24E2CF1AD5CDBF FOREIGN KEY (cart_id) REFERENCES Cart (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO CartItem (id, cart_id, item, price_credits, price_money, type) SELECT id, cart_id, item, price_credits, price_money, \'I\' FROM __temp__CartItem');
        $this->addSql('DROP TABLE __temp__CartItem');
        $this->addSql('CREATE INDEX IDX_3B24E2CF1AD5CDBF ON CartItem (cart_id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

//        $this->addSql('CREATE TABLE WebShopDiscountCoupon (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, coupon VARCHAR(5) NOT NULL COLLATE BINARY, discount INTEGER NOT NULL, type VARCHAR(255) NOT NULL COLLATE BINARY, status INTEGER NOT NULL, expire_at DATETIME NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id))');
//        $this->addSql('CREATE UNIQUE INDEX WebShopDiscountCoupon_coupon_UNIQUE ON WebShopDiscountCoupon (coupon)');
//        $this->addSql('CREATE INDEX IDX_82476BA1844E6B7 ON WebShopDiscountCoupon (server_id)');
//        $this->addSql('CREATE TABLE WebShopPersistentDiscount (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, type VARCHAR(255) NOT NULL COLLATE BINARY, discount INTEGER NOT NULL, spent_from INTEGER NOT NULL, spent_to INTEGER NOT NULL, PRIMARY KEY(id))');
//        $this->addSql('CREATE UNIQUE INDEX WebShopPersistentDiscount_range_UNIQUE ON WebShopPersistentDiscount (server_id, spent_from, spent_to)');
//        $this->addSql('CREATE INDEX IDX_49570D791844E6B7 ON WebShopPersistentDiscount (server_id)');
//        $this->addSql('CREATE TABLE WebShopPurchaseDiscount (id INTEGER NOT NULL, webshop_purchase_item_log_id INTEGER DEFAULT NULL, web_shop_discount_coupon_id INTEGER DEFAULT NULL, type VARCHAR(255) NOT NULL COLLATE BINARY, discount_percent INTEGER NOT NULL, discount_amount DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
//        $this->addSql('CREATE UNIQUE INDEX UNIQ_6F2EA27938D5CA98 ON WebShopPurchaseDiscount (web_shop_discount_coupon_id)');
//        $this->addSql('CREATE INDEX IDX_6F2EA279963592CE ON WebShopPurchaseDiscount (webshop_purchase_item_log_id)');
//        $this->addSql('CREATE TABLE WebShopPurchaseLog (id INTEGER NOT NULL, user_server_account_id INTEGER DEFAULT NULL, price_credits SMALLINT NOT NULL, price_money DOUBLE PRECISION NOT NULL, type VARCHAR(255) NOT NULL COLLATE BINARY, final_price DOUBLE PRECISION NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id))');
//        $this->addSql('CREATE INDEX IDX_6FBF006BB1BB7436 ON WebShopPurchaseLog (user_server_account_id)');
//        $this->addSql('DROP TABLE Character');
//        $this->addSql('DROP TABLE StatsPurchaseLogItem');
//        $this->addSql('DROP TABLE CartPersistentDiscount');
//        $this->addSql('DROP TABLE CartDiscountCoupon');
//        $this->addSql('DROP TABLE CartPurchaseDiscount');
//        $this->addSql('DROP TABLE StatsShopConfig');
//        $this->addSql('DROP TABLE CartPurchaseLog');
//        $this->addSql('CREATE TEMPORARY TABLE __temp__AncientSet AS SELECT id, name, options FROM AncientSet');
//        $this->addSql('DROP TABLE AncientSet');
//        $this->addSql('CREATE TABLE AncientSet (id INTEGER NOT NULL, name VARCHAR(100) NOT NULL, options CLOB NOT NULL COLLATE BINARY, PRIMARY KEY(id))');
//        $this->addSql('INSERT INTO AncientSet (id, name, options) SELECT id, name, options FROM __temp__AncientSet');
//        $this->addSql('DROP TABLE __temp__AncientSet');
//        $this->addSql('DROP INDEX IDX_DE03DB73BFF8E8BE');
//        $this->addSql('DROP INDEX IDX_DE03DB7356D6E7AC');
//        $this->addSql('CREATE TEMPORARY TABLE __temp__AncientSetItem AS SELECT id, ancient_set_id, item_info_id, bonus FROM AncientSetItem');
//        $this->addSql('DROP TABLE AncientSetItem');
//        $this->addSql('CREATE TABLE AncientSetItem (id INTEGER NOT NULL, ancient_set_id INTEGER DEFAULT NULL, item_info_id INTEGER DEFAULT NULL, bonus INTEGER NOT NULL, PRIMARY KEY(id))');
//        $this->addSql('INSERT INTO AncientSetItem (id, ancient_set_id, item_info_id, bonus) SELECT id, ancient_set_id, item_info_id, bonus FROM __temp__AncientSetItem');
//        $this->addSql('DROP TABLE __temp__AncientSetItem');
//        $this->addSql('CREATE INDEX IDX_DE03DB73BFF8E8BE ON AncientSetItem (ancient_set_id)');
//        $this->addSql('CREATE INDEX IDX_DE03DB7356D6E7AC ON AncientSetItem (item_info_id)');
//        $this->addSql('DROP INDEX UNIQ_AB912789B1BB7436');
//        $this->addSql('CREATE TEMPORARY TABLE __temp__Cart AS SELECT id, user_server_account_id FROM Cart');
//        $this->addSql('DROP TABLE Cart');
//        $this->addSql('CREATE TABLE Cart (id INTEGER NOT NULL, user_server_account_id INTEGER DEFAULT NULL, PRIMARY KEY(id))');
//        $this->addSql('INSERT INTO Cart (id, user_server_account_id) SELECT id, user_server_account_id FROM __temp__Cart');
//        $this->addSql('DROP TABLE __temp__Cart');
//        $this->addSql('CREATE UNIQUE INDEX UNIQ_AB912789B1BB7436 ON Cart (user_server_account_id)');
//        $this->addSql('DROP INDEX IDX_3B24E2CF1AD5CDBF');
//        $this->addSql('CREATE TEMPORARY TABLE __temp__CartItem AS SELECT id, cart_id, item, price_credits, price_money FROM CartItem');
//        $this->addSql('DROP TABLE CartItem');
//        $this->addSql('CREATE TABLE CartItem (id INTEGER NOT NULL, cart_id INTEGER DEFAULT NULL, price_credits SMALLINT NOT NULL, price_money DOUBLE PRECISION NOT NULL, item VARCHAR(255) NOT NULL COLLATE BINARY, PRIMARY KEY(id))');
//        $this->addSql('INSERT INTO CartItem (id, cart_id, item, price_credits, price_money) SELECT id, cart_id, item, price_credits, price_money FROM __temp__CartItem');
//        $this->addSql('DROP TABLE __temp__CartItem');
//        $this->addSql('CREATE INDEX IDX_3B24E2CF1AD5CDBF ON CartItem (cart_id)');
//        $this->addSql('DROP INDEX UNIQ_AB372AC0BCF3411D');
//        $this->addSql('CREATE TEMPORARY TABLE __temp__CharacterClass AS SELECT id, name, abbreviation, type FROM CharacterClass');
//        $this->addSql('DROP TABLE CharacterClass');
//        $this->addSql('CREATE TABLE CharacterClass (id INTEGER NOT NULL, name VARCHAR(50) NOT NULL, abbreviation VARCHAR(5) NOT NULL, type INTEGER NOT NULL, PRIMARY KEY(id))');
//        $this->addSql('INSERT INTO CharacterClass (id, name, abbreviation, type) SELECT id, name, abbreviation, type FROM __temp__CharacterClass');
//        $this->addSql('DROP TABLE __temp__CharacterClass');
//        $this->addSql('CREATE UNIQUE INDEX UNIQ_AB372AC0BCF3411D ON CharacterClass (abbreviation)');
//        $this->addSql('DROP INDEX IDX_E1A21D50FE54D947');
//        $this->addSql('DROP INDEX IDX_E1A21D503A93697B');
//        $this->addSql('CREATE TEMPORARY TABLE __temp__GroupHarmonyOption AS SELECT id, group_id, harmony_option_id, type FROM GroupHarmonyOption');
//        $this->addSql('DROP TABLE GroupHarmonyOption');
//        $this->addSql('CREATE TABLE GroupHarmonyOption (id INTEGER NOT NULL, group_id INTEGER DEFAULT NULL, harmony_option_id INTEGER DEFAULT NULL, type INTEGER NOT NULL, PRIMARY KEY(id))');
//        $this->addSql('INSERT INTO GroupHarmonyOption (id, group_id, harmony_option_id, type) SELECT id, group_id, harmony_option_id, type FROM __temp__GroupHarmonyOption');
//        $this->addSql('DROP TABLE __temp__GroupHarmonyOption');
//        $this->addSql('CREATE INDEX IDX_E1A21D50FE54D947 ON GroupHarmonyOption (group_id)');
//        $this->addSql('CREATE INDEX IDX_E1A21D503A93697B ON GroupHarmonyOption (harmony_option_id)');
//        $this->addSql('DROP INDEX IDX_5CD2E83DE4D74647');
//        $this->addSql('CREATE TEMPORARY TABLE __temp__GroupHarmonyOptionLevel AS SELECT id, group_harmony_option_id, value, level FROM GroupHarmonyOptionLevel');
//        $this->addSql('DROP TABLE GroupHarmonyOptionLevel');
//        $this->addSql('CREATE TABLE GroupHarmonyOptionLevel (id INTEGER NOT NULL, group_harmony_option_id INTEGER DEFAULT NULL, value INTEGER NOT NULL, level INTEGER NOT NULL, PRIMARY KEY(id))');
//        $this->addSql('INSERT INTO GroupHarmonyOptionLevel (id, group_harmony_option_id, value, level) SELECT id, group_harmony_option_id, value, level FROM __temp__GroupHarmonyOptionLevel');
//        $this->addSql('DROP TABLE __temp__GroupHarmonyOptionLevel');
//        $this->addSql('CREATE INDEX IDX_5CD2E83DAD00F165 ON GroupHarmonyOptionLevel (group_harmony_option_id)');
//        $this->addSql('DROP INDEX UNIQ_BBE5DD825E237E06');
//        $this->addSql('CREATE TEMPORARY TABLE __temp__ItemGroup AS SELECT id, name, type_id FROM ItemGroup');
//        $this->addSql('DROP TABLE ItemGroup');
//        $this->addSql('CREATE TABLE ItemGroup (id INTEGER NOT NULL, name VARCHAR(20) NOT NULL, type_id INTEGER NOT NULL, PRIMARY KEY(id))');
//        $this->addSql('INSERT INTO ItemGroup (id, name, type_id) SELECT id, name, type_id FROM __temp__ItemGroup');
//        $this->addSql('DROP TABLE __temp__ItemGroup');
//        $this->addSql('CREATE UNIQUE INDEX UNIQ_BBE5DD825E237E06 ON ItemGroup (name)');
//        $this->addSql('DROP INDEX IDX_38B39EF7FE54D947');
//        $this->addSql('DROP INDEX IDX_38B39EF7890798E5');
//        $this->addSql('DROP INDEX IDX_38B39EF76293FF9');
//        $this->addSql('DROP INDEX idx_item_id');
//        $this->addSql('CREATE TEMPORARY TABLE __temp__ItemInfo AS SELECT id, group_id, pvp_option_id, market_category_id, item_id, name, level, x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill FROM ItemInfo');
//        $this->addSql('DROP TABLE ItemInfo');
//        $this->addSql('CREATE TABLE ItemInfo (id INTEGER NOT NULL, group_id INTEGER DEFAULT NULL, pvp_option_id INTEGER DEFAULT NULL, market_category_id INTEGER DEFAULT NULL, item_id INTEGER NOT NULL, name VARCHAR(100) NOT NULL, level INTEGER NOT NULL, x INTEGER NOT NULL, y INTEGER NOT NULL, drop_level INTEGER DEFAULT NULL, durability INTEGER DEFAULT NULL, attack_speed INTEGER DEFAULT NULL, defense INTEGER DEFAULT NULL, min_dmg INTEGER DEFAULT NULL, max_dmg INTEGER DEFAULT NULL, wizardry_dmg INTEGER DEFAULT NULL, wizardry_excellent_dmg INTEGER DEFAULT NULL, req_str INTEGER DEFAULT NULL, req_agi INTEGER DEFAULT NULL, req_ene INTEGER DEFAULT NULL, req_lvl INTEGER DEFAULT NULL, skill VARCHAR(100) DEFAULT NULL, PRIMARY KEY(id))');
//        $this->addSql('INSERT INTO ItemInfo (id, group_id, pvp_option_id, market_category_id, item_id, name, level, x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill) SELECT id, group_id, pvp_option_id, market_category_id, item_id, name, level, x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill FROM __temp__ItemInfo');
//        $this->addSql('DROP TABLE __temp__ItemInfo');
//        $this->addSql('CREATE INDEX IDX_38B39EF7FE54D947 ON ItemInfo (group_id)');
//        $this->addSql('CREATE INDEX IDX_38B39EF7890798E5 ON ItemInfo (pvp_option_id)');
//        $this->addSql('CREATE INDEX IDX_38B39EF76293FF9 ON ItemInfo (market_category_id)');
//        $this->addSql('CREATE INDEX idx_item_id ON ItemInfo (item_id)');
//        $this->addSql('DROP INDEX UNIQ_8A01A25B1844E6B7');
//        $this->addSql('CREATE TEMPORARY TABLE __temp__MarketConfig AS SELECT id, server_id, commission FROM MarketConfig');
//        $this->addSql('DROP TABLE MarketConfig');
//        $this->addSql('CREATE TABLE MarketConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, commission SMALLINT NOT NULL, PRIMARY KEY(id))');
//        $this->addSql('INSERT INTO MarketConfig (id, server_id, commission) SELECT id, server_id, commission FROM __temp__MarketConfig');
//        $this->addSql('DROP TABLE __temp__MarketConfig');
//        $this->addSql('CREATE UNIQUE INDEX UNIQ_8A01A25B1844E6B7 ON MarketConfig (server_id)');
//        $this->addSql('DROP INDEX IDX_6E1472A956D6E7AC');
//        $this->addSql('DROP INDEX IDX_6E1472A9B1BB7436');
//        $this->addSql('DROP INDEX IDX_6E1472A91844E6B7');
//        $this->addSql('DROP INDEX MarketItem_index_item_specs');
//        $this->addSql('CREATE TEMPORARY TABLE __temp__MarketItem AS SELECT id, item_info_id, user_server_account_id, server_id, hex, level, option, skill, luck, excellent, ancient, harmony, pvp, price, price_type, created_at, updated_at FROM MarketItem');
//        $this->addSql('DROP TABLE MarketItem');
//        $this->addSql('CREATE TABLE MarketItem (id INTEGER NOT NULL, item_info_id INTEGER DEFAULT NULL, user_server_account_id INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, hex VARCHAR(100) NOT NULL, level SMALLINT NOT NULL, option SMALLINT NOT NULL, skill BOOLEAN NOT NULL, luck BOOLEAN NOT NULL, excellent BOOLEAN NOT NULL, ancient BOOLEAN NOT NULL, harmony BOOLEAN NOT NULL, pvp BOOLEAN NOT NULL, price INTEGER NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, price_type VARCHAR(255) NOT NULL COLLATE BINARY, PRIMARY KEY(id))');
//        $this->addSql('INSERT INTO MarketItem (id, item_info_id, user_server_account_id, server_id, hex, level, option, skill, luck, excellent, ancient, harmony, pvp, price, price_type, created_at, updated_at) SELECT id, item_info_id, user_server_account_id, server_id, hex, level, option, skill, luck, excellent, ancient, harmony, pvp, price, price_type, created_at, updated_at FROM __temp__MarketItem');
//        $this->addSql('DROP TABLE __temp__MarketItem');
//        $this->addSql('CREATE INDEX IDX_6E1472A956D6E7AC ON MarketItem (item_info_id)');
//        $this->addSql('CREATE INDEX IDX_6E1472A9B1BB7436 ON MarketItem (user_server_account_id)');
//        $this->addSql('CREATE INDEX IDX_6E1472A91844E6B7 ON MarketItem (server_id)');
//        $this->addSql('CREATE INDEX MarketItem_index_item_specs ON MarketItem (level, option, skill, luck, excellent, ancient, harmony, pvp, price, price_type)');
//        $this->addSql('DROP INDEX IDX_BDE1366EA76ED395');
//        $this->addSql('DROP INDEX IDX_BDE1366E1844E6B7');
//        $this->addSql('CREATE TEMPORARY TABLE __temp__News AS SELECT id, user_id, server_id, text, status, created_at, updated_at FROM News');
//        $this->addSql('DROP TABLE News');
//        $this->addSql('CREATE TABLE News (id INTEGER NOT NULL, user_id INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, text CLOB NOT NULL, status SMALLINT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id))');
//        $this->addSql('INSERT INTO News (id, user_id, server_id, text, status, created_at, updated_at) SELECT id, user_id, server_id, text, status, created_at, updated_at FROM __temp__News');
//        $this->addSql('DROP TABLE __temp__News');
//        $this->addSql('CREATE INDEX IDX_BDE1366EA76ED395 ON News (user_id)');
//        $this->addSql('CREATE INDEX IDX_BDE1366E1844E6B7 ON News (server_id)');
//        $this->addSql('CREATE TEMPORARY TABLE __temp__PvPOption AS SELECT id, options FROM PvPOption');
//        $this->addSql('DROP TABLE PvPOption');
//        $this->addSql('CREATE TABLE PvPOption (id INTEGER NOT NULL, options CLOB NOT NULL COLLATE BINARY, PRIMARY KEY(id))');
//        $this->addSql('INSERT INTO PvPOption (id, options) SELECT id, options FROM __temp__PvPOption');
//        $this->addSql('DROP TABLE __temp__PvPOption');
//        $this->addSql('DROP INDEX UNIQ_5DC1D0C0BD138F07');
//        $this->addSql('CREATE TEMPORARY TABLE __temp__Server AS SELECT id, database_credential_id, exp, "drop", version, name, description FROM Server');
//        $this->addSql('DROP TABLE Server');
//        $this->addSql('CREATE TABLE Server (id INTEGER NOT NULL, database_credential_id INTEGER NOT NULL, exp INTEGER NOT NULL, "drop" INTEGER NOT NULL, version VARCHAR(50) NOT NULL, name VARCHAR(50) NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
//        $this->addSql('INSERT INTO Server (id, database_credential_id, exp, "drop", version, name, description) SELECT id, database_credential_id, exp, "drop", version, name, description FROM __temp__Server');
//        $this->addSql('DROP TABLE __temp__Server');
//        $this->addSql('CREATE UNIQUE INDEX UNIQ_5DC1D0C0BD138F07 ON Server (database_credential_id)');
//        $this->addSql('DROP INDEX UNIQ_2DA17977F85E0677');
//        $this->addSql('DROP INDEX UNIQ_2DA17977D5499347');
//        $this->addSql('DROP INDEX UNIQ_2DA17977E7927C74');
//        $this->addSql('CREATE TEMPORARY TABLE __temp__User AS SELECT id, username, display_name, email, password, logged_at, created_at, updated_at, type_id FROM User');
//        $this->addSql('DROP TABLE User');
//        $this->addSql('CREATE TABLE User (id INTEGER NOT NULL, username VARCHAR(10) NOT NULL, display_name VARCHAR(10) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(10) NOT NULL, logged_at DATETIME NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, type_id VARCHAR(255) NOT NULL COLLATE BINARY, PRIMARY KEY(id))');
//        $this->addSql('INSERT INTO User (id, username, display_name, email, password, logged_at, created_at, updated_at, type_id) SELECT id, username, display_name, email, password, logged_at, created_at, updated_at, type_id FROM __temp__User');
//        $this->addSql('DROP TABLE __temp__User');
//        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977F85E0677 ON User (username)');
//        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977D5499347 ON User (display_name)');
//        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977E7927C74 ON User (email)');
//        $this->addSql('DROP INDEX IDX_758ED6D9A76ED395');
//        $this->addSql('DROP INDEX IDX_758ED6D91844E6B7');
//        $this->addSql('CREATE TEMPORARY TABLE __temp__UserServerAccount AS SELECT id, user_id, server_id, account_id, selected_at, spent_money, spent_credits FROM UserServerAccount');
//        $this->addSql('DROP TABLE UserServerAccount');
//        $this->addSql('CREATE TABLE UserServerAccount (id INTEGER NOT NULL, user_id INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, account_id VARCHAR(10) NOT NULL, selected_at DATETIME NOT NULL, spent_money DOUBLE PRECISION NOT NULL, spent_credits INTEGER NOT NULL, PRIMARY KEY(id))');
//        $this->addSql('INSERT INTO UserServerAccount (id, user_id, server_id, account_id, selected_at, spent_money, spent_credits) SELECT id, user_id, server_id, account_id, selected_at, spent_money, spent_credits FROM __temp__UserServerAccount');
//        $this->addSql('DROP TABLE __temp__UserServerAccount');
//        $this->addSql('CREATE INDEX IDX_758ED6D9A76ED395 ON UserServerAccount (user_id)');
//        $this->addSql('CREATE INDEX IDX_758ED6D91844E6B7 ON UserServerAccount (server_id)');
//        $this->addSql('DROP INDEX IDX_4F3FA1C940319467');
//        $this->addSql('CREATE TEMPORARY TABLE __temp__WebShopCategory AS SELECT id, web_shop_config_id, name, status FROM WebShopCategory');
//        $this->addSql('DROP TABLE WebShopCategory');
//        $this->addSql('CREATE TABLE WebShopCategory (id INTEGER NOT NULL, web_shop_config_id INTEGER DEFAULT NULL, name VARCHAR(50) NOT NULL, status SMALLINT NOT NULL, PRIMARY KEY(id))');
//        $this->addSql('INSERT INTO WebShopCategory (id, web_shop_config_id, name, status) SELECT id, web_shop_config_id, name, status FROM __temp__WebShopCategory');
//        $this->addSql('DROP TABLE __temp__WebShopCategory');
//        $this->addSql('CREATE INDEX IDX_4F3FA1C940319467 ON WebShopCategory (web_shop_config_id)');
//        $this->addSql('DROP INDEX IDX_D26B6AC6609AADCB');
//        $this->addSql('DROP INDEX IDX_D26B6AC656D6E7AC');
//        $this->addSql('CREATE TEMPORARY TABLE __temp__WebShopCategoryItem AS SELECT id, webshop_category_id, item_info_id, allowed_options, price_credits, price_money FROM WebShopCategoryItem');
//        $this->addSql('DROP TABLE WebShopCategoryItem');
//        $this->addSql('CREATE TABLE WebShopCategoryItem (id INTEGER NOT NULL, webshop_category_id INTEGER DEFAULT NULL, item_info_id INTEGER DEFAULT NULL, allowed_options INTEGER NOT NULL, price_credits INTEGER NOT NULL, price_money INTEGER NOT NULL, PRIMARY KEY(id))');
//        $this->addSql('INSERT INTO WebShopCategoryItem (id, webshop_category_id, item_info_id, allowed_options, price_credits, price_money) SELECT id, webshop_category_id, item_info_id, allowed_options, price_credits, price_money FROM __temp__WebShopCategoryItem');
//        $this->addSql('DROP TABLE __temp__WebShopCategoryItem');
//        $this->addSql('CREATE INDEX IDX_D26B6AC6609AADCB ON WebShopCategoryItem (webshop_category_id)');
//        $this->addSql('CREATE INDEX IDX_D26B6AC656D6E7AC ON WebShopCategoryItem (item_info_id)');
//        $this->addSql('DROP INDEX UNIQ_F11BB4681844E6B7');
//        $this->addSql('CREATE TEMPORARY TABLE __temp__WebShopConfig AS SELECT id, server_id, max_item_level, max_item_option, max_exc_count, level_price_add_money, option_price_add_money, level_price_add_credits, option_price_add_credits, skill_price_multiplier, luck_price_multiplier, excellent_price_multiplier, ancient_price_multiplier, harmony_price_multiplier, pvp_price_multiplier FROM WebShopConfig');
//        $this->addSql('DROP TABLE WebShopConfig');
//        $this->addSql('CREATE TABLE WebShopConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, max_item_level SMALLINT NOT NULL, max_item_option SMALLINT NOT NULL, max_exc_count SMALLINT NOT NULL, level_price_add_money DOUBLE PRECISION NOT NULL, option_price_add_money DOUBLE PRECISION NOT NULL, level_price_add_credits SMALLINT NOT NULL, option_price_add_credits SMALLINT NOT NULL, skill_price_multiplier DOUBLE PRECISION NOT NULL, luck_price_multiplier DOUBLE PRECISION NOT NULL, excellent_price_multiplier DOUBLE PRECISION NOT NULL, ancient_price_multiplier DOUBLE PRECISION NOT NULL, harmony_price_multiplier DOUBLE PRECISION NOT NULL, pvp_price_multiplier DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
//        $this->addSql('INSERT INTO WebShopConfig (id, server_id, max_item_level, max_item_option, max_exc_count, level_price_add_money, option_price_add_money, level_price_add_credits, option_price_add_credits, skill_price_multiplier, luck_price_multiplier, excellent_price_multiplier, ancient_price_multiplier, harmony_price_multiplier, pvp_price_multiplier) SELECT id, server_id, max_item_level, max_item_option, max_exc_count, level_price_add_money, option_price_add_money, level_price_add_credits, option_price_add_credits, skill_price_multiplier, luck_price_multiplier, excellent_price_multiplier, ancient_price_multiplier, harmony_price_multiplier, pvp_price_multiplier FROM __temp__WebShopConfig');
//        $this->addSql('DROP TABLE __temp__WebShopConfig');
//        $this->addSql('CREATE UNIQUE INDEX UNIQ_F11BB4681844E6B7 ON WebShopConfig (server_id)');
//        $this->addSql('DROP INDEX IDX_ED5E349C39A45F57');
//        $this->addSql('DROP INDEX WebShopPurchaseLogItem_serial_UNIQUE');
//        $this->addSql('CREATE TEMPORARY TABLE __temp__WebShopPurchaseLogItem AS SELECT id, cart_purchase_item_log_id, item, price_credits, price_money, first_serial, second_serial FROM WebShopPurchaseLogItem');
//        $this->addSql('DROP TABLE WebShopPurchaseLogItem');
//        $this->addSql('CREATE TABLE WebShopPurchaseLogItem (id INTEGER NOT NULL, webshop_purchase_item_log_id INTEGER DEFAULT NULL, item VARCHAR(255) NOT NULL, price_credits SMALLINT NOT NULL, price_money DOUBLE PRECISION NOT NULL, first_serial INTEGER NOT NULL, second_serial INTEGER DEFAULT NULL, PRIMARY KEY(id), CONSTRAINT FK_ED5E349C39A45F57 FOREIGN KEY (webshop_purchase_item_log_id) REFERENCES CartPurchaseLog (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
//        $this->addSql('INSERT INTO WebShopPurchaseLogItem (id, webshop_purchase_item_log_id, item, price_credits, price_money, first_serial, second_serial) SELECT id, cart_purchase_item_log_id, item, price_credits, price_money, first_serial, second_serial FROM __temp__WebShopPurchaseLogItem');
//        $this->addSql('DROP TABLE __temp__WebShopPurchaseLogItem');
//        $this->addSql('CREATE UNIQUE INDEX WebShopPurchaseLogItem_serial_UNIQUE ON WebShopPurchaseLogItem (first_serial, second_serial)');
//        $this->addSql('CREATE INDEX IDX_ED5E349C963592CE ON WebShopPurchaseLogItem (webshop_purchase_item_log_id)');
//        $this->addSql('DROP INDEX UNIQ_3AD36BBCB1BB7436');
//        $this->addSql('CREATE TEMPORARY TABLE __temp__WebStorage AS SELECT id, user_server_account_id FROM WebStorage');
//        $this->addSql('DROP TABLE WebStorage');
//        $this->addSql('CREATE TABLE WebStorage (id INTEGER NOT NULL, user_server_account_id INTEGER DEFAULT NULL, PRIMARY KEY(id))');
//        $this->addSql('INSERT INTO WebStorage (id, user_server_account_id) SELECT id, user_server_account_id FROM __temp__WebStorage');
//        $this->addSql('DROP TABLE __temp__WebStorage');
//        $this->addSql('CREATE UNIQUE INDEX UNIQ_3AD36BBCB1BB7436 ON WebStorage (user_server_account_id)');
//        $this->addSql('DROP INDEX IDX_C01610ADDDA5619C');
//        $this->addSql('CREATE TEMPORARY TABLE __temp__WebStorageItem AS SELECT id, web_storage_id, item FROM WebStorageItem');
//        $this->addSql('DROP TABLE WebStorageItem');
//        $this->addSql('CREATE TABLE WebStorageItem (id INTEGER NOT NULL, web_storage_id INTEGER DEFAULT NULL, item VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
//        $this->addSql('INSERT INTO WebStorageItem (id, web_storage_id, item) SELECT id, web_storage_id, item FROM __temp__WebStorageItem');
//        $this->addSql('DROP TABLE __temp__WebStorageItem');
//        $this->addSql('CREATE INDEX IDX_C01610AD481289F8 ON WebStorageItem (web_storage_id)');
//        $this->addSql('DROP INDEX IDX_3A362D756D6E7AC');
//        $this->addSql('DROP INDEX IDX_3A362D7B201E281');
//        $this->addSql('CREATE TEMPORARY TABLE __temp__character_class_items AS SELECT item_info_id, character_class_id FROM character_class_items');
//        $this->addSql('DROP TABLE character_class_items');
//        $this->addSql('CREATE TABLE character_class_items (item_info_id INTEGER NOT NULL, character_class_id INTEGER NOT NULL, PRIMARY KEY(item_info_id, character_class_id))');
//        $this->addSql('INSERT INTO character_class_items (item_info_id, character_class_id) SELECT item_info_id, character_class_id FROM __temp__character_class_items');
//        $this->addSql('DROP TABLE __temp__character_class_items');
//        $this->addSql('CREATE INDEX IDX_3A362D756D6E7AC ON character_class_items (item_info_id)');
//        $this->addSql('CREATE INDEX IDX_3A362D7B201E281 ON character_class_items (character_class_id)');
    }
}
