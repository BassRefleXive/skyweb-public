<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170118135438 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');


        $this->addSql('DROP INDEX UNIQ_2DA17977D5499347');
        $this->addSql('DROP INDEX UNIQ_2DA17977F85E0677');
        $this->addSql('DROP INDEX UNIQ_2DA17977E7927C74');
        $this->addSql('CREATE TEMPORARY TABLE __temp__User AS SELECT id, email, password, logged_at, created_at, updated_at, username, type_id, display_name FROM User');
        $this->addSql('DROP TABLE User');
        $this->addSql('CREATE TABLE User (id INTEGER NOT NULL, email VARCHAR(255) NOT NULL COLLATE BINARY, password VARCHAR(10) NOT NULL COLLATE BINARY, logged_at DATETIME NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, username VARCHAR(10) NOT NULL COLLATE BINARY, display_name VARCHAR(10) NOT NULL COLLATE BINARY, type_id VARCHAR(255) NOT NULL, locale VARCHAR(6) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO User (id, email, password, logged_at, created_at, updated_at, username, type_id, display_name, locale) SELECT id, email, password, logged_at, created_at, updated_at, username, type_id, display_name, "ru" FROM __temp__User');
        $this->addSql('DROP TABLE __temp__User');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977D5499347 ON User (display_name)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977F85E0677 ON User (username)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977E7927C74 ON User (email)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_2DA17977F85E0677');
        $this->addSql('DROP INDEX UNIQ_2DA17977D5499347');
        $this->addSql('DROP INDEX UNIQ_2DA17977E7927C74');
        $this->addSql('CREATE TEMPORARY TABLE __temp__User AS SELECT id, username, display_name, email, password, logged_at, created_at, updated_at, type_id FROM User');
        $this->addSql('DROP TABLE User');
        $this->addSql('CREATE TABLE User (id INTEGER NOT NULL, username VARCHAR(10) NOT NULL, display_name VARCHAR(10) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(10) NOT NULL, logged_at DATETIME NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, type_id VARCHAR(255) NOT NULL COLLATE BINARY, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO User (id, username, display_name, email, password, logged_at, created_at, updated_at, type_id) SELECT id, username, display_name, email, password, logged_at, created_at, updated_at, type_id FROM __temp__User');
        $this->addSql('DROP TABLE __temp__User');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977F85E0677 ON User (username)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977D5499347 ON User (display_name)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977E7927C74 ON User (email)');
    }
}
