<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170620182114 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $insertItemStmt = $this->connection->prepare('
            INSERT INTO ItemInfo (
                group_id, 
                item_id, 
                name, 
                level, 
                x, 
                y, 
                drop_level, 
                durability, 
                attack_speed, 
                defense, 
                min_dmg, 
                max_dmg, 
                wizardry_dmg, 
                req_str, 
                req_agi, 
                req_ene, 
                req_lvl, 
                skill
            ) VALUES (
                (SELECT id FROM ItemGroup WHERE type_id = :groupId), 
                :itemId, 
                :name, 
                0, 
                :x, 
                :y, 
                :dropLevel, 
                :durability, 
                :attackSpeed, 
                :defense, 
                :minDmg, 
                :maxDmg, 
                :wizardryDmg, 
                :reqStr, 
                :reqAgi, 
                :reqEne, 
                :reqLvl,
                :skill
            )
        ');

        foreach ($this->items() as $groupId => $groupItems) {
            foreach ($groupItems as $itemId => $itemData) {
                $insertItemStmt->execute([
                    'groupId'     => $groupId,
                    'itemId'      => $itemId,
                    'name'        => $itemData['name'],
                    'x'           => $itemData['x'],
                    'y'           => $itemData['y'],
                    'dropLevel'   => $itemData['dropLevel'] ?? null,
                    'durability'  => $itemData['durability'] ?? null,
                    'attackSpeed' => $itemData['attackSpeed'] ?? null,
                    'defense'     => $itemData['defense'] ?? null,
                    'minDmg'      => $itemData['minDmg'] ?? null,
                    'maxDmg'      => $itemData['maxDmg'] ?? null,
                    'wizardryDmg' => $itemData['wizardryDmg'] ?? null,
                    'reqStr'      => $itemData['reqStr'] ?? null,
                    'reqAgi'      => $itemData['reqAgi'] ?? null,
                    'reqEne'      => $itemData['reqEne'] ?? null,
                    'reqLvl'      => $itemData['reqLvl'] ?? null,
                    'skill'       => $itemData['skill'] ?? null,
                ]);
            }
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        foreach ($this->items() as $groupId => $groupItems) {
            foreach ($groupItems as $itemId => $itemData) {
                $this->addSql('DELETE FROM ItemInfo WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . $groupId . ') AND item_id = ' . $itemId . '');
            }
        }
    }

    private function items(): array
    {
        return [
            14 => [
                180 => ['name' => 'Jewel Of Exellent', 'x' => 1, 'y' => 1],
                181 => ['name' => 'Jewel Of Mistic', 'x' => 1, 'y' => 1],
                182 => ['name' => 'Jewel Of Luck', 'x' => 1, 'y' => 1],
                183 => ['name' => 'Jewel Of Skill', 'x' => 1, 'y' => 1],
                184 => ['name' => 'Jewel Of Level', 'x' => 1, 'y' => 1],
                185 => ['name' => 'Jewel Of Evalution', 'x' => 1, 'y' => 1],
                186 => ['name' => 'Jewel Of Ancent', 'x' => 1, 'y' => 1],
                187 => ['name' => 'Jewel Of Option', 'x' => 1, 'y' => 1],
            ],
        ];
    }
}
