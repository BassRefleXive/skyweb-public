<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170118183414 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE ForumTopic (id INTEGER NOT NULL, user_server_account_id INTEGER DEFAULT NULL, forum_category_id INTEGER DEFAULT NULL, title VARCHAR(100) NOT NULL, text CLOB NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8D182203B1BB7436 ON ForumTopic (user_server_account_id)');
        $this->addSql('CREATE INDEX IDX_8D18220314721E40 ON ForumTopic (forum_category_id)');
        $this->addSql('CREATE TABLE ForumComment (id INTEGER NOT NULL, user_server_account_id INTEGER DEFAULT NULL, forum_topic_id INTEGER DEFAULT NULL, text CLOB NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A264645AB1BB7436 ON ForumComment (user_server_account_id)');
        $this->addSql('CREATE INDEX IDX_A264645A38A6ADDA ON ForumComment (forum_topic_id)');
        $this->addSql('CREATE TABLE ForumCategory (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL, description CLOB NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C9C09C6E1844E6B7 ON ForumCategory (server_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE ForumTopic');
        $this->addSql('DROP TABLE ForumComment');
        $this->addSql('DROP TABLE ForumCategory');
    }
}
