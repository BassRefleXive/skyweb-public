<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161228111603 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE WebShopCategoryItem (id INTEGER NOT NULL, webshop_category_id INTEGER DEFAULT NULL, item_info_id INTEGER DEFAULT NULL, allowed_options INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D26B6AC6609AADCB ON WebShopCategoryItem (webshop_category_id)');
        $this->addSql('CREATE INDEX IDX_D26B6AC656D6E7AC ON WebShopCategoryItem (item_info_id)');
        $this->addSql('DROP TABLE webshop_category_items');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE webshop_category_items (webshop_category_id INTEGER NOT NULL, item_info_id INTEGER NOT NULL, PRIMARY KEY(webshop_category_id, item_info_id))');
        $this->addSql('CREATE INDEX IDX_6E92731556D6E7AC ON webshop_category_items (item_info_id)');
        $this->addSql('CREATE INDEX IDX_6E927315609AADCB ON webshop_category_items (webshop_category_id)');
        $this->addSql('DROP TABLE WebShopCategoryItem');
    }
}
