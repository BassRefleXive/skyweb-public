<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170124003558 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE Vote (id INTEGER NOT NULL, user_server_account_id INTEGER DEFAULT NULL, mmo_top_id BIGINT NOT NULL, date_time DATETIME NOT NULL, ip VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FA222A5AB1BB7436 ON Vote (user_server_account_id)');

        $this->addSql('DROP INDEX IDX_758ED6D9A76ED395');
        $this->addSql('DROP INDEX IDX_758ED6D91844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__UserServerAccount AS SELECT id, user_id, server_id, account_id, selected_at, spent_money, spent_credits, purchased_stats FROM UserServerAccount');
        $this->addSql('DROP TABLE UserServerAccount');
        $this->addSql('CREATE TABLE UserServerAccount (id INTEGER NOT NULL, user_id INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, account_id VARCHAR(10) NOT NULL COLLATE BINARY, selected_at DATETIME NOT NULL, spent_money DOUBLE PRECISION NOT NULL, spent_credits INTEGER NOT NULL, purchased_stats INTEGER NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_758ED6D9A76ED395 FOREIGN KEY (user_id) REFERENCES User (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_758ED6D91844E6B7 FOREIGN KEY (server_id) REFERENCES Server (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO UserServerAccount (id, user_id, server_id, account_id, selected_at, spent_money, spent_credits, purchased_stats) SELECT id, user_id, server_id, account_id, selected_at, spent_money, spent_credits, purchased_stats FROM __temp__UserServerAccount');
        $this->addSql('DROP TABLE __temp__UserServerAccount');
        $this->addSql('CREATE INDEX IDX_758ED6D9A76ED395 ON UserServerAccount (user_id)');
        $this->addSql('CREATE INDEX IDX_758ED6D91844E6B7 ON UserServerAccount (server_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE Vote');

        $this->addSql('DROP INDEX IDX_758ED6D9A76ED395');
        $this->addSql('DROP INDEX IDX_758ED6D91844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__UserServerAccount AS SELECT id, user_id, server_id, account_id, selected_at, spent_money, spent_credits, purchased_stats FROM UserServerAccount');
        $this->addSql('DROP TABLE UserServerAccount');
        $this->addSql('CREATE TABLE UserServerAccount (id INTEGER NOT NULL, user_id INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, account_id VARCHAR(10) NOT NULL, selected_at DATETIME NOT NULL, spent_money DOUBLE PRECISION NOT NULL, spent_credits INTEGER NOT NULL, purchased_stats INTEGER NOT NULL, regular_votes INTEGER NOT NULL, sms_votes INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO UserServerAccount (id, user_id, server_id, account_id, selected_at, spent_money, spent_credits, purchased_stats, regular_votes, sms_votes) SELECT id, user_id, server_id, account_id, selected_at, spent_money, spent_credits, purchased_stats, 0, 0 FROM __temp__UserServerAccount');
        $this->addSql('DROP TABLE __temp__UserServerAccount');
        $this->addSql('CREATE INDEX IDX_758ED6D9A76ED395 ON UserServerAccount (user_id)');
        $this->addSql('CREATE INDEX IDX_758ED6D91844E6B7 ON UserServerAccount (server_id)');
    }
}
