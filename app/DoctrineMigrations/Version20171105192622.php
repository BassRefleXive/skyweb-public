<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171105192622 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_F11BB4681844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__WebShopConfig AS SELECT id, server_id, max_item_level, max_item_option, max_exc_count, level_price_add_money, option_price_add_money, level_price_add_credits, option_price_add_credits, skill_price_multiplier, luck_price_multiplier, excellent_price_multiplier, ancient_price_multiplier, harmony_price_multiplier, pvp_price_multiplier, global_credits_discount_percent, global_money_discount_percent FROM WebShopConfig');
        $this->addSql('DROP TABLE WebShopConfig');
        $this->addSql('CREATE TABLE WebShopConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, max_item_level SMALLINT NOT NULL, max_item_option SMALLINT NOT NULL, max_exc_count SMALLINT NOT NULL, level_price_add_money DOUBLE PRECISION NOT NULL, option_price_add_money DOUBLE PRECISION NOT NULL, level_price_add_credits SMALLINT NOT NULL, option_price_add_credits SMALLINT NOT NULL, skill_price_multiplier DOUBLE PRECISION NOT NULL, luck_price_multiplier DOUBLE PRECISION NOT NULL, excellent_price_multiplier DOUBLE PRECISION NOT NULL, ancient_price_multiplier DOUBLE PRECISION NOT NULL, harmony_price_multiplier DOUBLE PRECISION NOT NULL, pvp_price_multiplier DOUBLE PRECISION NOT NULL, global_credits_discount_percent SMALLINT NOT NULL, global_money_discount_percent SMALLINT NOT NULL, enabled BOOLEAN NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_F11BB4681844E6B7 FOREIGN KEY (server_id) REFERENCES Server (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO WebShopConfig (id, server_id, max_item_level, max_item_option, max_exc_count, level_price_add_money, option_price_add_money, level_price_add_credits, option_price_add_credits, skill_price_multiplier, luck_price_multiplier, excellent_price_multiplier, ancient_price_multiplier, harmony_price_multiplier, pvp_price_multiplier, global_credits_discount_percent, global_money_discount_percent, enabled) SELECT id, server_id, max_item_level, max_item_option, max_exc_count, level_price_add_money, option_price_add_money, level_price_add_credits, option_price_add_credits, skill_price_multiplier, luck_price_multiplier, excellent_price_multiplier, ancient_price_multiplier, harmony_price_multiplier, pvp_price_multiplier, global_credits_discount_percent, global_money_discount_percent, 0 FROM __temp__WebShopConfig');
        $this->addSql('DROP TABLE __temp__WebShopConfig');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F11BB4681844E6B7 ON WebShopConfig (server_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_F11BB4681844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__WebShopConfig AS SELECT id, server_id, max_item_level, max_item_option, max_exc_count, level_price_add_money, option_price_add_money, level_price_add_credits, option_price_add_credits, skill_price_multiplier, luck_price_multiplier, excellent_price_multiplier, ancient_price_multiplier, harmony_price_multiplier, pvp_price_multiplier, global_credits_discount_percent, global_money_discount_percent FROM WebShopConfig');
        $this->addSql('DROP TABLE WebShopConfig');
        $this->addSql('CREATE TABLE WebShopConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, max_item_level SMALLINT NOT NULL, max_item_option SMALLINT NOT NULL, max_exc_count SMALLINT NOT NULL, level_price_add_money DOUBLE PRECISION NOT NULL, option_price_add_money DOUBLE PRECISION NOT NULL, level_price_add_credits SMALLINT NOT NULL, option_price_add_credits SMALLINT NOT NULL, skill_price_multiplier DOUBLE PRECISION NOT NULL, luck_price_multiplier DOUBLE PRECISION NOT NULL, excellent_price_multiplier DOUBLE PRECISION NOT NULL, ancient_price_multiplier DOUBLE PRECISION NOT NULL, harmony_price_multiplier DOUBLE PRECISION NOT NULL, pvp_price_multiplier DOUBLE PRECISION NOT NULL, global_credits_discount_percent SMALLINT DEFAULT 0 NOT NULL, global_money_discount_percent SMALLINT DEFAULT 0 NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO WebShopConfig (id, server_id, max_item_level, max_item_option, max_exc_count, level_price_add_money, option_price_add_money, level_price_add_credits, option_price_add_credits, skill_price_multiplier, luck_price_multiplier, excellent_price_multiplier, ancient_price_multiplier, harmony_price_multiplier, pvp_price_multiplier, global_credits_discount_percent, global_money_discount_percent) SELECT id, server_id, max_item_level, max_item_option, max_exc_count, level_price_add_money, option_price_add_money, level_price_add_credits, option_price_add_credits, skill_price_multiplier, luck_price_multiplier, excellent_price_multiplier, ancient_price_multiplier, harmony_price_multiplier, pvp_price_multiplier, global_credits_discount_percent, global_money_discount_percent FROM __temp__WebShopConfig');
        $this->addSql('DROP TABLE __temp__WebShopConfig');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F11BB4681844E6B7 ON WebShopConfig (server_id)');
    }
}
