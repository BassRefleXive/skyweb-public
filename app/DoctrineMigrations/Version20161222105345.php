<?php

namespace Application\Migrations;

use AppBundle\Doctrine\Type\ItemGroupEnum;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161222105345 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg = 142 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_SWORD . ') AND item_id = 21');
        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg = 160 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_SWORD . ') AND item_id = 23');
        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg = 176 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_SWORD . ') AND item_id = 25');

        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg = 34 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_SCEPTER . ') AND item_id = 8');
        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg = 52 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_SCEPTER . ') AND item_id = 9');
        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg = 70 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_SCEPTER . ') AND item_id = 10');
        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg = 44 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_SCEPTER . ') AND item_id = 11');
        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg = 100 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_SCEPTER . ') AND item_id = 12');
        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg = 138 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_SCEPTER . ') AND item_id = 13');
        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg = 112 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_SCEPTER . ') AND item_id = 14');
        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg = 78 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_SCEPTER . ') AND item_id = 15');
        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg = 86 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_SCEPTER . ') AND item_id = 18');

        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg = 36 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_STAFF . ') AND item_id = 0');
        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg = 52 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_STAFF . ') AND item_id = 1');
        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg = 66 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_STAFF . ') AND item_id = 2');
        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg = 78 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_STAFF . ') AND item_id = 3');
        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg = 90 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_STAFF . ') AND item_id = 4');
        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg = 88 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_STAFF . ') AND item_id = 5');
        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg = 98 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_STAFF . ') AND item_id = 6');
        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg = 100 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_STAFF . ') AND item_id = 7');
        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg = 134 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_STAFF . ') AND item_id = 8');
        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg = 120 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_STAFF . ') AND item_id = 9');
        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg = 106 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_STAFF . ') AND item_id = 10');
        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg = 136 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_STAFF . ') AND item_id = 11');
        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg = 156 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_STAFF . ') AND item_id = 12');
        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg = 152 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_STAFF . ') AND item_id = 13');

        $this->addSql('UPDATE ItemInfo SET name = "Ring of Ice" WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_13 . ') AND item_id = 8');
        $this->addSql('UPDATE ItemInfo SET name = "Bundle Jewel of Bless (10)" WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_12 . ') AND item_id = 30 AND level = 0');
        $this->addSql('UPDATE ItemInfo SET name = "Bundle Jewel of Soul (10)" WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_12 . ') AND item_id = 31 AND level = 0');
        $this->addSql('UPDATE ItemInfo SET name = "Lower Refining Stone" WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_14 . ') AND item_id = 43 AND level = 0');
        $this->addSql('UPDATE ItemInfo SET name = "Higher Refining Stone" WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_14 . ') AND item_id = 44 AND level = 0');
        $this->addSql('UPDATE ItemInfo SET name = "Red Ribbon Box" WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_12 . ') AND item_id = 32 AND level
         = 0');
        $this->addSql('UPDATE ItemInfo SET name = "Green Ribbon Box" WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_12 . ') AND item_id = 33 AND level
         = 0');
        $this->addSql('UPDATE ItemInfo SET name = "Blue Ribbon Box" WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_12 . ') AND item_id = 34 AND level
         = 0');
        $this->addSql('UPDATE ItemInfo SET name = "Blue Ribbon Box" WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_12 . ') AND item_id = 34 AND level
         = 0');
        $this->addSql('UPDATE ItemInfo SET name = "Spirit of Dark Horse" WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_13 . ') AND item_id = 31 AND level
         = 0');
        $this->addSql('UPDATE ItemInfo SET durability = 20 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_13 . ') AND item_id = 32 AND 
        level
         = 0');
        $this->addSql('UPDATE ItemInfo SET durability = 20 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_13 . ') AND item_id = 33 AND 
        level
         = 0');
        $this->addSql('UPDATE ItemInfo SET durability = 10 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_13 . ') AND item_id = 34 AND 
        level
         = 0');
        $this->addSql('UPDATE ItemInfo SET durability = 0 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_13 . ') AND item_id = 35 AND 
        level
         = 0');
        $this->addSql('UPDATE ItemInfo SET durability = 0 WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_13 . ') AND item_id = 36 AND 
        level
         = 0');
        $this->addSql('UPDATE ItemInfo SET name = "Buggle Dragon Transformation Ring" WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_13 . ') AND item_id = 10
         AND
         level = 0');
        $this->addSql('UPDATE ItemInfo SET name = "Orb of Advanced Defense" WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_12 . ') AND item_id = 9
         AND
         level = 0');
        $this->addSql('UPDATE ItemInfo SET name = "Orb of Summoning Goblin" WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_12 . ') AND item_id = 11
         AND
         level = 0');
        $this->addSql('UPDATE ItemInfo SET name = "Fruit of Streight" WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_13 . ') AND item_id = 15
         AND
         level = 0');

        $insertItemInfoEquippedStmt = $this->connection->prepare('INSERT INTO character_class_items (item_info_id, character_class_id) VALUES (
             (SELECT id FROM ItemInfo WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = :groupId) AND item_id = :itemId AND level = :level),
             (SELECT id FROM CharacterClass WHERE abbreviation = :abbreviation)
        )');

        $insertItemStmt = $this->connection->prepare('INSERT INTO ItemInfo (group_id, item_id, name, level, x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, 
        req_str, req_agi, req_ene, req_lvl, pvp_option_id, skill) VALUES ((SELECT id FROM ItemGroup WHERE type_id = :groupId), :itemId, :name, :level, :x, :y, :dropLevel, :durability, :attackSpeed, 
        :defense, 
        :minDmg, 
        :maxDmg, :wizardryDmg, :reqStr, :reqAgi, :reqEne, :reqLvl,
        (SELECT id from PvPOption WHERE options = :pvpOption), :skill)');


        $insertItemStmt->execute([
            'groupId'    => ItemGroupEnum::ITEM_GROUP_12,
            'itemId'     => 30,
            'level'      => 1,
            'name'       => 'Bundle Jewel of Bless (20)',
            'x'          => 1,
            'y'          => 1,
            'dropLevel'  => 25,
            'durability' => 0, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne' => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'    => ItemGroupEnum::ITEM_GROUP_12,
            'itemId'     => 30,
            'level'      => 2,
            'name'       => 'Bundle Jewel of Bless (30)',
            'x'          => 1,
            'y'          => 1,
            'dropLevel'  => 25,
            'durability' => 0, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne' => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'    => ItemGroupEnum::ITEM_GROUP_12,
            'itemId'     => 31,
            'level'      => 1,
            'name'       => 'Bundle Jewel of Soul (20)',
            'x'          => 1,
            'y'          => 1,
            'dropLevel'  => 25,
            'durability' => 0, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne' => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'    => ItemGroupEnum::ITEM_GROUP_12,
            'itemId'     => 31,
            'level'      => 2,
            'name'       => 'Bundle Jewel of Soul (30)',
            'x'          => 1,
            'y'          => 1,
            'dropLevel'  => 25,
            'durability' => 0, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne' => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);

        $insertItemStmt->execute([
            'groupId'    => ItemGroupEnum::ITEM_GROUP_13,
            'itemId'     => 20,
            'level'      => 1,
            'name'       => 'Ring of Warrior 40',
            'x'          => 1,
            'y'          => 1,
            'dropLevel'  => null,
            'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne' => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);

        $insertItemStmt->execute([
            'groupId'    => ItemGroupEnum::ITEM_GROUP_13,
            'itemId'     => 20,
            'level'      => 2,
            'name'       => 'Ring of Warrior 80',
            'x'          => 1,
            'y'          => 1,
            'dropLevel'  => null,
            'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne' => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);

        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_14,
            'itemId'    => 11,
            'level'     => 1,
            'name'      => 'Star',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne' => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_14,
            'itemId'    => 11,
            'level'     => 2,
            'name'      => 'FireCracker',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne' => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_14,
            'itemId'    => 11,
            'level'     => 3,
            'name'      => 'Heart of Love',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne' => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_14,
            'itemId'    => 11,
            'level'     => 5,
            'name'      => 'Silver Medal',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne' => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_14,
            'itemId'    => 11,
            'level'     => 6,
            'name'      => 'Gold Medal',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne' => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_14,
            'itemId'    => 11,
            'level'     => 7,
            'name'      => 'Box of Heaven',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne' => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_14,
            'itemId'    => 11,
            'level'     => 8,
            'name'      => 'Box of Kundun +1',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne' => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_14,
            'itemId'    => 11,
            'level'     => 9,
            'name'      => 'Box of Kundun +2',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne' => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_14,
            'itemId'    => 11,
            'level'     => 10,
            'name'      => 'Box of Kundun +3',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne' => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_14,
            'itemId'    => 11,
            'level'     => 11,
            'name'      => 'Box of Kundun +4',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne' => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_14,
            'itemId'    => 11,
            'level'     => 12,
            'name'      => 'Box of Kundun +5',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne' => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_14,
            'itemId'    => 11,
            'level'     => 13,
            'name'      => 'Heart of Lord',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne' => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_14,
            'itemId'    => 32,
            'level'     => 1,
            'name'      => 'Pink Candy Box',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne' => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_14,
            'itemId'    => 33,
            'level'     => 1,
            'name'      => 'Red Candy Box',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne' => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_14,
            'itemId'    => 34,
            'level'     => 1,
            'name'      => 'Blue Candy Box',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne' => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_13,
            'itemId'    => 31,
            'level'     => 1,
            'name'      => 'Spirit of Dark Raven',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne' => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemInfoEquippedStmt->execute([
            'abbreviation' => 'DL',
            'groupId'      => ItemGroupEnum::ITEM_GROUP_13,
            'itemId'       => 31,
            'level'        => 1,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_13,
            'itemId'    => 14,
            'level'     => 1,
            'name'      => 'Crest of Monarch',
            'x'         => 1,
            'y'         => 2,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne' => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_14,
            'itemId'    => 21,
            'level'     => 3,
            'name'      => 'Sign of Lord',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => 255, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne'
            => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemInfoEquippedStmt->execute([
            'abbreviation' => 'DL',
            'groupId'      => ItemGroupEnum::ITEM_GROUP_13,
            'itemId'       => 5,
            'level'        => 0,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_13,
            'itemId'    => 10,
            'level'     => 1,
            'name'      => 'Giant Transformation Ring',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => 200, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne'
                        => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_13,
            'itemId'    => 10,
            'level'     => 2,
            'name'      => 'Skeleton Transformation Ring',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => 200, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne'
                        => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_13,
            'itemId'    => 10,
            'level'     => 3,
            'name'      => 'Bull Fighter Transformation Ring',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => 200, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne'
                        => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_13,
            'itemId'    => 10,
            'level'     => 4,
            'name'      => 'Lich Fighter Transformation Ring',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => 200, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne'
                        => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_13,
            'itemId'    => 10,
            'level'     => 5,
            'name'      => 'Death Cow Transformation Ring',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => 200, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne'
                        => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_12,
            'itemId'    => 11,
            'level'     => 1,
            'name'      => 'Orb of Summoning Stone Golem',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne'
                        => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemInfoEquippedStmt->execute([
            'abbreviation' => 'ELF',
            'groupId'      => ItemGroupEnum::ITEM_GROUP_12,
            'itemId'       => 11,
            'level'        => 1,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_12,
            'itemId'    => 11,
            'level'     => 2,
            'name'      => 'Orb of Summoning Assassin',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne'
                        => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemInfoEquippedStmt->execute([
            'abbreviation' => 'ELF',
            'groupId'      => ItemGroupEnum::ITEM_GROUP_12,
            'itemId'       => 11,
            'level'        => 2,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_12,
            'itemId'    => 11,
            'level'     => 3,
            'name'      => 'Orb of Summoning Bali',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne'
                        => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemInfoEquippedStmt->execute([
            'abbreviation' => 'ELF',
            'groupId'      => ItemGroupEnum::ITEM_GROUP_12,
            'itemId'       => 11,
            'level'        => 3,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_12,
            'itemId'    => 11,
            'level'     => 4,
            'name'      => 'Orb of Summoning Soldier',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne'
                        => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemInfoEquippedStmt->execute([
            'abbreviation' => 'ELF',
            'groupId'      => ItemGroupEnum::ITEM_GROUP_12,
            'itemId'       => 11,
            'level'        => 4,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_12,
            'itemId'    => 11,
            'level'     => 5,
            'name'      => 'Orb of Summoning Yeti',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne'
                        => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemInfoEquippedStmt->execute([
            'abbreviation' => 'ELF',
            'groupId'      => ItemGroupEnum::ITEM_GROUP_12,
            'itemId'       => 11,
            'level'        => 5,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_12,
            'itemId'    => 11,
            'level'     => 6,
            'name'      => 'Orb of Summoning Dark Knight',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne'
                        => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemInfoEquippedStmt->execute([
            'abbreviation' => 'ELF',
            'groupId'      => ItemGroupEnum::ITEM_GROUP_12,
            'itemId'       => 11,
            'level'        => 6,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_13,
            'itemId'    => 15,
            'level'     => 1,
            'name'      => 'Fruit of Agility',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne'
                        => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_13,
            'itemId'    => 15,
            'level'     => 2,
            'name'      => 'Fruit of Energy',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne'
                        => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_13,
            'itemId'    => 15,
            'level'     => 3,
            'name'      => 'Fruit of Stamina',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne'
                        => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_13,
            'itemId'    => 15,
            'level'     => 4,
            'name'      => 'Fruit of Command',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne'
                        => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);
        $insertItemStmt->execute([
            'groupId'   => ItemGroupEnum::ITEM_GROUP_14,
            'itemId'    => 9,
            'level'     => 1,
            'name'      => 'Olivas',
            'x'         => 1,
            'y'         => 1,
            'dropLevel' => null, 'durability' => null, 'attackSpeed' => null, 'defense' => null, 'minDmg' => null, 'maxDmg' => null, 'wizardryDmg' => null, 'reqStr' => null, 'reqAgi' => null, 'reqEne'
                        => null, 'reqLvl' => null, 'pvpOption' => null, 'skill' => null,
        ]);

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('UPDATE ItemInfo SET wizardry_excellent_dmg=NULL');
        $this->addSql('DELETE FROM character_class_items WHERE item_info_id = (SELECT id FROM ItemInfo WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_13 . ') AND item_id = 31 AND level = 1)');
        $this->addSql('DELETE FROM character_class_items WHERE item_info_id = (SELECT id FROM ItemInfo WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_13 . ') AND item_id = 5 AND level = 0)');

        $this->addSql('DELETE FROM character_class_items WHERE item_info_id = (SELECT id FROM ItemInfo WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_12 .
            ') AND item_id = 11 AND level = 1)');
        $this->addSql('DELETE FROM character_class_items WHERE item_info_id = (SELECT id FROM ItemInfo WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_12 .
            ') AND item_id = 11 AND level = 2)');
        $this->addSql('DELETE FROM character_class_items WHERE item_info_id = (SELECT id FROM ItemInfo WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_12 .
            ') AND item_id = 11 AND level = 3)');
        $this->addSql('DELETE FROM character_class_items WHERE item_info_id = (SELECT id FROM ItemInfo WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_12 .
            ') AND item_id = 11 AND level = 4)');
        $this->addSql('DELETE FROM character_class_items WHERE item_info_id = (SELECT id FROM ItemInfo WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_12 .
            ') AND item_id = 11 AND level = 5)');
        $this->addSql('DELETE FROM character_class_items WHERE item_info_id = (SELECT id FROM ItemInfo WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = ' . ItemGroupEnum::ITEM_GROUP_12 .
            ') AND item_id = 11 AND level = 6)');

        $this->addSql('DELETE FROM ItemInfo WHERE level != 0');

    }
}
