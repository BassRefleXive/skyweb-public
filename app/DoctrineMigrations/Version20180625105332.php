<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180625105332 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('INSERT INTO ItemInfo
    (group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(13, NULL, 26, 73, \'Sphere (Tetra)\', 0, 1, 1, 132, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL)');
        $this->addSql('INSERT INTO ItemInfo
    (group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(13, NULL, 26, 74, \'Sphere (Penta)\', 0, 1, 1, 152, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL)');

        $this->addSql('INSERT INTO ItemInfo
(group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(13, NULL, 26, 118, \'Seed Sphere (Fire)\', 0, 1, 1, 4, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);
');
        $this->addSql('INSERT INTO ItemInfo
(group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(13, NULL, 26, 119, \'Seed Sphere (Water)\', 0, 1, 1, 4, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);
');
        $this->addSql('INSERT INTO ItemInfo
(group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(13, NULL, 26, 120, \'Seed Sphere (Ice)\', 0, 1, 1, 4, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);
');
        $this->addSql('INSERT INTO ItemInfo
(group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(13, NULL, 26, 121, \'Seed Sphere (Wind)\', 0, 1, 1, 4, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);
');
        $this->addSql('INSERT INTO ItemInfo
(group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(13, NULL, 26, 122, \'Seed Sphere (Lightning)\', 0, 1, 1, 4, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);
');
        $this->addSql('INSERT INTO ItemInfo
(group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(13, NULL, 26, 123, \'Seed Sphere (Earth)\', 0, 1, 1, 4, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);
');
        $this->addSql('INSERT INTO ItemInfo
(group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(13, NULL, 26, 124, \'Seed Sphere (Fire)\', 0, 1, 1, 5, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);
');
        $this->addSql('INSERT INTO ItemInfo
(group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(13, NULL, 26, 125, \'Seed Sphere (Water)\', 0, 1, 1, 5, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);
');
        $this->addSql('INSERT INTO ItemInfo
(group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(13, NULL, 26, 126, \'Seed Sphere (Ice)\', 0, 1, 1, 5, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);
');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

    }
}
