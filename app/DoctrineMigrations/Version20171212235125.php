<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171212235125 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX Vote_mmo_top_id_UNIQUE;');
        $this->addSql('DROP INDEX IDX_FA222A5AB1BB7436');
        $this->addSql('CREATE TEMPORARY TABLE __temp__Vote AS SELECT id, user_server_account_id, mmo_top_id, date_time, ip, type FROM Vote');
        $this->addSql('DROP TABLE Vote');
        $this->addSql('CREATE TABLE Vote (id INTEGER NOT NULL, user_server_account_id INTEGER DEFAULT NULL, date_time DATETIME NOT NULL, ip VARCHAR(255) NOT NULL COLLATE BINARY, provider VARCHAR(255) NOT NULL, provider_id BIGINT NOT NULL, type VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_FA222A5AB1BB7436 FOREIGN KEY (user_server_account_id) REFERENCES UserServerAccount (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO Vote (id, user_server_account_id, provider_id, date_time, ip, type, provider, created_at, updated_at) SELECT id, user_server_account_id, mmo_top_id, date_time, ip, type, "mmotop", date_time, date_time FROM __temp__Vote');
        $this->addSql('DROP TABLE __temp__Vote');
        $this->addSql('CREATE INDEX IDX_FA222A5AB1BB7436 ON Vote (user_server_account_id)');
     }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_FA222A5AB1BB7436');
        $this->addSql('CREATE TEMPORARY TABLE __temp__Vote AS SELECT id, user_server_account_id, date_time, ip, provider_id, type FROM Vote');
        $this->addSql('DROP TABLE Vote');
        $this->addSql('CREATE TABLE Vote (id INTEGER NOT NULL, user_server_account_id INTEGER DEFAULT NULL, date_time DATETIME NOT NULL, ip VARCHAR(255) NOT NULL, mmo_top_id BIGINT NOT NULL, type VARCHAR(255) NOT NULL COLLATE BINARY, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO Vote (id, user_server_account_id, date_time, ip, mmo_top_id, type) SELECT id, user_server_account_id, date_time, ip, provider_id, type FROM __temp__Vote');
        $this->addSql('DROP TABLE __temp__Vote');
        $this->addSql('CREATE INDEX IDX_FA222A5AB1BB7436 ON Vote (user_server_account_id)');
        $this->addSql('CREATE UNIQUE INDEX Vote_mmo_top_id_UNIQUE ON Vote (mmo_top_id)');
      }
}
