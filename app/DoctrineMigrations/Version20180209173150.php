<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180209173150 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE ItemBuffEffect (id INTEGER NOT NULL, item_info_id INTEGER DEFAULT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B13F13C056D6E7AC ON ItemBuffEffect (item_info_id)');

        $this->addSql('INSERT INTO ItemBuffEffect (`id`, `item_info_id`, `description`) VALUES (1, 783, \'EXP gain Increase 15%\')'); // 13 43
        $this->addSql('INSERT INTO ItemBuffEffect (`id`, `item_info_id`, `description`) VALUES (2, 673, \'Master EXP gain Increase 15%\')'); // 13 93
        $this->addSql('INSERT INTO ItemBuffEffect (`id`, `item_info_id`, `description`) VALUES (3, 761, \'EXP gain Increase 10%\nItem gain Increase 15%\')'); // 13 44
        $this->addSql('INSERT INTO ItemBuffEffect (`id`, `item_info_id`, `description`) VALUES (4, 714, \'Master EXP gain Increase 10%\nItem gain Increase 15%\')'); // 13 94
        $this->addSql('INSERT INTO ItemBuffEffect (`id`, `item_info_id`, `description`) VALUES (5, 651, \'EXP gain disabled\')'); // 13 45
        $this->addSql('INSERT INTO ItemBuffEffect (`id`, `item_info_id`, `description`) VALUES (6, 672, \'Mobility enabled\')'); // 13 59
        $this->addSql('INSERT INTO ItemBuffEffect (`id`, `item_info_id`, `description`) VALUES (8, 712, \'EXP gain Increase 5%\nAutomatic Life recovery at 10%\')'); // 13 62
        $this->addSql('INSERT INTO ItemBuffEffect (`id`, `item_info_id`, `description`) VALUES (9, 646, \'Item gain Increase at 10%\nAutomatic Mana recovery at 15%\')'); // 13 63
        $this->addSql('INSERT INTO ItemBuffEffect (`id`, `item_info_id`, `description`) VALUES (10, 742, \'Increases Attack Rate and Defense Rate\')'); // 13 96
        $this->addSql('INSERT INTO ItemBuffEffect (`id`, `item_info_id`, `description`) VALUES (11, 219, \'Attack Speed Increase +150\')'); // 14 72
        $this->addSql('INSERT INTO ItemBuffEffect (`id`, `item_info_id`, `description`) VALUES (12, 224, \'Defensibility Increase +200\')'); // 14 73
        $this->addSql('INSERT INTO ItemBuffEffect (`id`, `item_info_id`, `description`) VALUES (13, 155, \'Attack Power Increase +75\')'); // 14 74
        $this->addSql('INSERT INTO ItemBuffEffect (`id`, `item_info_id`, `description`) VALUES (14, 111, \'Wizardry Increase +75\')'); // 14 75
        $this->addSql('INSERT INTO ItemBuffEffect (`id`, `item_info_id`, `description`) VALUES (15, 169, \'HP Increase +500\')'); // 14 76
        $this->addSql('INSERT INTO ItemBuffEffect (`id`, `item_info_id`, `description`) VALUES (16, 137, \'MP Increase +750\')'); // 14 77
        $this->addSql('INSERT INTO ItemBuffEffect (`id`, `item_info_id`, `description`) VALUES (17, 225, \'Critical Damage Increase at 100%\')'); // 14 97
        $this->addSql('INSERT INTO ItemBuffEffect (`id`, `item_info_id`, `description`) VALUES (18, 100, \'Excellent Damage Increase at 100%\')'); // 14 98
        $this->addSql('INSERT INTO ItemBuffEffect (`id`, `item_info_id`, `description`) VALUES (19, 97, \'+5% HP Regen\n+500 HP\')'); // 14 98
        $this->addSql('INSERT INTO ItemBuffEffect (`id`, `item_info_id`, `description`) VALUES (20, 231, \'Strength +50\')'); // 14 78
        $this->addSql('INSERT INTO ItemBuffEffect (`id`, `item_info_id`, `description`) VALUES (21, 82, \'Agility +50\')'); // 14 79
        $this->addSql('INSERT INTO ItemBuffEffect (`id`, `item_info_id`, `description`) VALUES (22, 238, \'Vitality +50\')'); // 14 80
        $this->addSql('INSERT INTO ItemBuffEffect (`id`, `item_info_id`, `description`) VALUES (23, 133, \'Energy +50\')'); // 14 81
        $this->addSql('INSERT INTO ItemBuffEffect (`id`, `item_info_id`, `description`) VALUES (24, 253, \'Command +50\')'); // 14 82
        $this->addSql('INSERT INTO ItemBuffEffect (`id`, `item_info_id`, `description`) VALUES (25, 675, \'Increases your max AG.\')'); // 13 104
        $this->addSql('INSERT INTO ItemBuffEffect (`id`, `item_info_id`, `description`) VALUES (26, 716, \'Increases your max SD.\')'); // 13 105
      }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE ItemBuffEffect');
    }
}
