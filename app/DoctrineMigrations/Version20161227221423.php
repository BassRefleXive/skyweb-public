<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161227221423 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');


        $this->addSql('DROP INDEX IDX_4F3FA1C91844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__WebShopCategory AS SELECT id, server_id, name, status FROM WebShopCategory');
        $this->addSql('DROP TABLE WebShopCategory');
        $this->addSql('CREATE TABLE WebShopCategory (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, name VARCHAR(50) NOT NULL COLLATE BINARY, status SMALLINT NOT NULL, allowed_options INTEGER NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_4F3FA1C91844E6B7 FOREIGN KEY (server_id) REFERENCES Server (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO WebShopCategory (id, server_id, name, status, allowed_options) SELECT id, server_id, name, status, 0 FROM __temp__WebShopCategory');
        $this->addSql('DROP TABLE __temp__WebShopCategory');
        $this->addSql('CREATE INDEX IDX_4F3FA1C91844E6B7 ON WebShopCategory (server_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_4F3FA1C91844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__WebShopCategory AS SELECT id, server_id, name, status FROM WebShopCategory');
        $this->addSql('DROP TABLE WebShopCategory');
        $this->addSql('CREATE TABLE WebShopCategory (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, name VARCHAR(50) NOT NULL, status SMALLINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO WebShopCategory (id, server_id, name, status) SELECT id, server_id, name, status FROM __temp__WebShopCategory');
        $this->addSql('DROP TABLE __temp__WebShopCategory');
    }
}
