<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161228140732 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_D26B6AC656D6E7AC');
        $this->addSql('DROP INDEX IDX_D26B6AC6609AADCB');
        $this->addSql('CREATE TEMPORARY TABLE __temp__WebShopCategoryItem AS SELECT id, webshop_category_id, item_info_id, allowed_options FROM WebShopCategoryItem');
        $this->addSql('DROP TABLE WebShopCategoryItem');
        $this->addSql('CREATE TABLE WebShopCategoryItem (id INTEGER NOT NULL, webshop_category_id INTEGER DEFAULT NULL, item_info_id INTEGER DEFAULT NULL, allowed_options INTEGER NOT NULL, price_credits INTEGER NOT NULL, price_money INTEGER NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_D26B6AC6609AADCB FOREIGN KEY (webshop_category_id) REFERENCES WebShopCategory (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_D26B6AC656D6E7AC FOREIGN KEY (item_info_id) REFERENCES ItemInfo (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO WebShopCategoryItem (id, webshop_category_id, item_info_id, allowed_options, price_credits, price_money) SELECT id, webshop_category_id, item_info_id, allowed_options, 0, 0 FROM __temp__WebShopCategoryItem');
        $this->addSql('DROP TABLE __temp__WebShopCategoryItem');
        $this->addSql('CREATE INDEX IDX_D26B6AC656D6E7AC ON WebShopCategoryItem (item_info_id)');
        $this->addSql('CREATE INDEX IDX_D26B6AC6609AADCB ON WebShopCategoryItem (webshop_category_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_D26B6AC6609AADCB');
        $this->addSql('DROP INDEX IDX_D26B6AC656D6E7AC');
        $this->addSql('CREATE TEMPORARY TABLE __temp__WebShopCategoryItem AS SELECT id, webshop_category_id, item_info_id, allowed_options FROM WebShopCategoryItem');
        $this->addSql('DROP TABLE WebShopCategoryItem');
        $this->addSql('CREATE TABLE WebShopCategoryItem (id INTEGER NOT NULL, webshop_category_id INTEGER DEFAULT NULL, item_info_id INTEGER DEFAULT NULL, allowed_options INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO WebShopCategoryItem (id, webshop_category_id, item_info_id, allowed_options) SELECT id, webshop_category_id, item_info_id, allowed_options FROM __temp__WebShopCategoryItem');
        $this->addSql('DROP TABLE __temp__WebShopCategoryItem');
        $this->addSql('CREATE INDEX IDX_D26B6AC6609AADCB ON WebShopCategoryItem (webshop_category_id)');
        $this->addSql('CREATE INDEX IDX_D26B6AC656D6E7AC ON WebShopCategoryItem (item_info_id)');
    }
}
