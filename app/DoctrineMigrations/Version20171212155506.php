<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171212155506 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_8A01A25B1844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__MarketConfig AS SELECT id, server_id, commission FROM MarketConfig');
        $this->addSql('DROP TABLE MarketConfig');
        $this->addSql('CREATE TABLE MarketConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, commission SMALLINT NOT NULL, max_items SMALLINT NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_8A01A25B1844E6B7 FOREIGN KEY (server_id) REFERENCES Server (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO MarketConfig (id, server_id, commission, max_items) SELECT id, server_id, commission, 50 FROM __temp__MarketConfig');
        $this->addSql('DROP TABLE __temp__MarketConfig');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8A01A25B1844E6B7 ON MarketConfig (server_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_8A01A25B1844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__MarketConfig AS SELECT id, server_id, commission FROM MarketConfig');
        $this->addSql('DROP TABLE MarketConfig');
        $this->addSql('CREATE TABLE MarketConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, commission SMALLINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO MarketConfig (id, server_id, commission) SELECT id, server_id, commission FROM __temp__MarketConfig');
        $this->addSql('DROP TABLE __temp__MarketConfig');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8A01A25B1844E6B7 ON MarketConfig (server_id)');
    }
}
