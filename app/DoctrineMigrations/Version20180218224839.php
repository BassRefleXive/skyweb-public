<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180218224839 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_C6AAADDD1844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__AccountMenuConfig AS SELECT id, server_id, nick_change_price, class_change_price, reset_stats_price, hide_info_daily_rate, online_hours_rate FROM AccountMenuConfig');
        $this->addSql('DROP TABLE AccountMenuConfig');
        $this->addSql('CREATE TABLE AccountMenuConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, nick_change_price INTEGER NOT NULL, class_change_price INTEGER NOT NULL, reset_stats_price INTEGER NOT NULL, hide_info_daily_rate INTEGER NOT NULL, online_hours_rate INTEGER NOT NULL, reset_master_skill_tree_price INTEGER NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_C6AAADDD1844E6B7 FOREIGN KEY (server_id) REFERENCES Server (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO AccountMenuConfig (id, server_id, nick_change_price, class_change_price, reset_stats_price, hide_info_daily_rate, online_hours_rate, reset_master_skill_tree_price) SELECT id, server_id, nick_change_price, class_change_price, reset_stats_price, hide_info_daily_rate, online_hours_rate, 0 FROM __temp__AccountMenuConfig');
        $this->addSql('DROP TABLE __temp__AccountMenuConfig');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C6AAADDD1844E6B7 ON AccountMenuConfig (server_id)');
     }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_C6AAADDD1844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__AccountMenuConfig AS SELECT id, server_id, nick_change_price, class_change_price, reset_stats_price, hide_info_daily_rate, online_hours_rate FROM AccountMenuConfig');
        $this->addSql('DROP TABLE AccountMenuConfig');
        $this->addSql('CREATE TABLE AccountMenuConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, nick_change_price INTEGER NOT NULL, class_change_price INTEGER NOT NULL, reset_stats_price INTEGER NOT NULL, hide_info_daily_rate INTEGER NOT NULL, online_hours_rate INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO AccountMenuConfig (id, server_id, nick_change_price, class_change_price, reset_stats_price, hide_info_daily_rate, online_hours_rate) SELECT id, server_id, nick_change_price, class_change_price, reset_stats_price, hide_info_daily_rate, online_hours_rate FROM __temp__AccountMenuConfig');
        $this->addSql('DROP TABLE __temp__AccountMenuConfig');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C6AAADDD1844E6B7 ON AccountMenuConfig (server_id)');
      }
}
