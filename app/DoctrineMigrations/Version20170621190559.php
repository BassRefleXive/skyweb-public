<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170621190559 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE ItemToBuy (id INTEGER NOT NULL, cart_id INTEGER DEFAULT NULL, order_id CHAR(36) DEFAULT NULL, item VARCHAR(255) DEFAULT NULL, stats INTEGER DEFAULT NULL, type VARCHAR(255) NOT NULL, price_credits SMALLINT NOT NULL, price_money DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_779FD2851AD5CDBF ON ItemToBuy (cart_id)');
        $this->addSql('CREATE INDEX IDX_779FD2858D9F6D38 ON ItemToBuy (order_id)');

        $this->addSql('DROP TABLE CartItem');

        $this->addSql('DROP INDEX IDX_758ED6D91844E6B7');
        $this->addSql('DROP INDEX IDX_758ED6D9A76ED395');
        $this->addSql('CREATE TEMPORARY TABLE __temp__UserServerAccount AS SELECT id, user_id, server_id, account_id, selected_at, spent_money, spent_credits, purchased_stats FROM UserServerAccount');
        $this->addSql('DROP TABLE UserServerAccount');
        $this->addSql('CREATE TABLE UserServerAccount (id INTEGER NOT NULL, user_id INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, account_id VARCHAR(10) NOT NULL COLLATE BINARY, selected_at DATETIME NOT NULL, spent_money DOUBLE PRECISION NOT NULL, spent_credits INTEGER NOT NULL, purchased_stats INTEGER NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_758ED6D9A76ED395 FOREIGN KEY (user_id) REFERENCES User (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_758ED6D91844E6B7 FOREIGN KEY (server_id) REFERENCES Server (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO UserServerAccount (id, user_id, server_id, account_id, selected_at, spent_money, spent_credits, purchased_stats) SELECT id, user_id, server_id, account_id, selected_at, spent_money, spent_credits, purchased_stats FROM __temp__UserServerAccount');
        $this->addSql('DROP TABLE __temp__UserServerAccount');
        $this->addSql('CREATE INDEX IDX_758ED6D91844E6B7 ON UserServerAccount (server_id)');
        $this->addSql('CREATE INDEX IDX_758ED6D9A76ED395 ON UserServerAccount (user_id)');

        $this->addSql('DROP INDEX UNIQ_AB912789B1BB7436');
        $this->addSql('CREATE TEMPORARY TABLE __temp__Cart AS SELECT id, user_server_account_id FROM Cart');
        $this->addSql('DROP TABLE Cart');
        $this->addSql('CREATE TABLE Cart (id INTEGER NOT NULL, user_server_account_id INTEGER DEFAULT NULL, PRIMARY KEY(id), CONSTRAINT FK_AB912789B1BB7436 FOREIGN KEY (user_server_account_id) REFERENCES UserServerAccount (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO Cart (id, user_server_account_id) SELECT id, user_server_account_id FROM __temp__Cart');
        $this->addSql('DROP TABLE __temp__Cart');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AB912789B1BB7436 ON Cart (user_server_account_id)');

        $this->addSql('DROP TABLE "Order"');
        $this->addSql('CREATE TABLE "Order" (id INTEGER NOT NULL, user_server_account_id INTEGER NOT NULL, transaction_id VARCHAR(255) DEFAULT NULL COLLATE BINARY, amount DOUBLE PRECISION NOT NULL, payment_system_amount DOUBLE PRECISION DEFAULT NULL, cart_amount DOUBLE PRECISION DEFAULT NULL, currency VARCHAR(3) NOT NULL COLLATE BINARY, payment_method VARCHAR(255) DEFAULT NULL COLLATE BINARY, description VARCHAR(255) NOT NULL COLLATE BINARY, created_at DATETIME NOT NULL, processed_at DATETIME DEFAULT NULL, updated_at DATETIME NOT NULL, state VARCHAR(255) NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_34E8BC9CB1BB7436 FOREIGN KEY (user_server_account_id) REFERENCES UserServerAccount (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_34E8BC9CB1BB7436 ON "Order" (user_server_account_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE CartItem (id INTEGER NOT NULL, cart_id INTEGER DEFAULT NULL, item VARCHAR(255) DEFAULT NULL COLLATE BINARY, stats INTEGER DEFAULT NULL, price_credits SMALLINT NOT NULL, price_money DOUBLE PRECISION NOT NULL, type VARCHAR(255) NOT NULL COLLATE BINARY, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3B24E2CF1AD5CDBF ON CartItem (cart_id)');

        $this->addSql('DROP TABLE ItemToBuy');

        $this->addSql('DROP INDEX UNIQ_AB912789B1BB7436');
        $this->addSql('CREATE TEMPORARY TABLE __temp__Cart AS SELECT id, user_server_account_id FROM Cart');
        $this->addSql('DROP TABLE Cart');
        $this->addSql('CREATE TABLE Cart (id INTEGER NOT NULL, user_server_account_id INTEGER DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO Cart (id, user_server_account_id) SELECT id, user_server_account_id FROM __temp__Cart');
        $this->addSql('DROP TABLE __temp__Cart');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AB912789B1BB7436 ON Cart (user_server_account_id)');

        $this->addSql('DROP INDEX IDX_34E8BC9CB1BB7436');
        $this->addSql('CREATE TEMPORARY TABLE __temp__Order AS SELECT id, transaction_id, amount, payment_system_amount, cart_amount, currency, payment_method, description, state, created_at, processed_at FROM "Order"');
        $this->addSql('DROP TABLE "Order"');
        $this->addSql('CREATE TABLE "Order" (id CHAR(36) NOT NULL COLLATE BINARY, transaction_id VARCHAR(255) DEFAULT NULL, amount DOUBLE PRECISION NOT NULL, payment_system_amount DOUBLE PRECISION DEFAULT NULL, cart_amount DOUBLE PRECISION DEFAULT NULL, currency VARCHAR(3) NOT NULL, payment_method VARCHAR(255) DEFAULT NULL, description VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, processed_at DATETIME DEFAULT NULL, state VARCHAR(255) NOT NULL COLLATE BINARY, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO "Order" (id, transaction_id, amount, payment_system_amount, cart_amount, currency, payment_method, description, state, created_at, processed_at) SELECT id, transaction_id, amount, payment_system_amount, cart_amount, currency, payment_method, description, state, created_at, processed_at FROM __temp__Order');
        $this->addSql('DROP TABLE __temp__Order');

        $this->addSql('DROP INDEX IDX_758ED6D9A76ED395');
        $this->addSql('DROP INDEX IDX_758ED6D91844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__UserServerAccount AS SELECT id, user_id, server_id, account_id, selected_at, spent_money, spent_credits, purchased_stats FROM UserServerAccount');
        $this->addSql('DROP TABLE UserServerAccount');
        $this->addSql('CREATE TABLE UserServerAccount (id INTEGER NOT NULL, user_id INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, account_id VARCHAR(10) NOT NULL, selected_at DATETIME NOT NULL, spent_money DOUBLE PRECISION NOT NULL, spent_credits INTEGER NOT NULL, purchased_stats INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO UserServerAccount (id, user_id, server_id, account_id, selected_at, spent_money, spent_credits, purchased_stats) SELECT id, user_id, server_id, account_id, selected_at, spent_money, spent_credits, purchased_stats FROM __temp__UserServerAccount');
        $this->addSql('DROP TABLE __temp__UserServerAccount');
        $this->addSql('CREATE INDEX IDX_758ED6D9A76ED395 ON UserServerAccount (user_id)');
        $this->addSql('CREATE INDEX IDX_758ED6D91844E6B7 ON UserServerAccount (server_id)');
    }
}
