<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161230170129 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE CartPurchaseLog (id INTEGER NOT NULL, user_server_account_id INTEGER DEFAULT NULL, price_credits SMALLINT NOT NULL, price_money DOUBLE PRECISION NOT NULL, type VARCHAR(255) NOT NULL, final_price DOUBLE PRECISION NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6FBF006BB1BB7436 ON CartPurchaseLog (user_server_account_id)');
        $this->addSql('CREATE TABLE WebShopPurchaseLogItem (id INTEGER NOT NULL, webshop_purchase_item_log_id INTEGER DEFAULT NULL, item VARCHAR(255) NOT NULL, price_credits SMALLINT NOT NULL, price_money DOUBLE PRECISION NOT NULL, first_serial INTEGER NOT NULL, second_serial INTEGER DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_ED5E349C963592CE ON WebShopPurchaseLogItem (webshop_purchase_item_log_id)');
        $this->addSql('CREATE UNIQUE INDEX WebShopPurchaseLogItem_serial_UNIQUE ON WebShopPurchaseLogItem (first_serial, second_serial)');
        $this->addSql('DROP TABLE BoughtItemSerial');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE BoughtItemSerial (id INTEGER NOT NULL, first_serial INTEGER NOT NULL, second_serial INTEGER DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX serial_UNIQUE ON BoughtItemSerial (first_serial, second_serial)');
        $this->addSql('DROP TABLE CartPurchaseLog');
        $this->addSql('DROP TABLE WebShopPurchaseLogItem');
    }
}
