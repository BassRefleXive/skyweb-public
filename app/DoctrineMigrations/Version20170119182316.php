<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170119182316 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_FCB45F831844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__ReferralConfig AS SELECT id, server_id, required_reset, required_grand_reset, required_online_time, required_votes FROM ReferralConfig');
        $this->addSql('DROP TABLE ReferralConfig');
        $this->addSql('CREATE TABLE ReferralConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, required_reset SMALLINT NOT NULL, required_grand_reset SMALLINT NOT NULL, required_online_time SMALLINT NOT NULL, required_votes SMALLINT NOT NULL, reward SMALLINT NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_FCB45F831844E6B7 FOREIGN KEY (server_id) REFERENCES Server (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO ReferralConfig (id, server_id, required_reset, required_grand_reset, required_online_time, required_votes, reward) SELECT id, server_id, required_reset, required_grand_reset, required_online_time, required_votes, 0 FROM __temp__ReferralConfig');
        $this->addSql('DROP TABLE __temp__ReferralConfig');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FCB45F831844E6B7 ON ReferralConfig (server_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_FCB45F831844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__ReferralConfig AS SELECT id, server_id, required_reset, required_grand_reset, required_online_time, required_votes FROM ReferralConfig');
        $this->addSql('DROP TABLE ReferralConfig');
        $this->addSql('CREATE TABLE ReferralConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, required_reset SMALLINT NOT NULL, required_grand_reset SMALLINT NOT NULL, required_online_time SMALLINT NOT NULL, required_votes SMALLINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO ReferralConfig (id, server_id, required_reset, required_grand_reset, required_online_time, required_votes) SELECT id, server_id, required_reset, required_grand_reset, required_online_time, required_votes FROM __temp__ReferralConfig');
        $this->addSql('DROP TABLE __temp__ReferralConfig');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FCB45F831844E6B7 ON ReferralConfig (server_id)');
    }
}
