<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170821161557 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_8D182203111D17F9');
        $this->addSql('DROP INDEX IDX_8D182203B1BB7436');
        $this->addSql('DROP INDEX IDX_8D18220314721E40');
        $this->addSql('CREATE TEMPORARY TABLE __temp__ForumTopic AS SELECT id, user_server_account_id, forum_category_id, last_comment_id, title, text, created_at, updated_at, description, views_count FROM ForumTopic');
        $this->addSql('DROP TABLE ForumTopic');
        $this->addSql('CREATE TABLE ForumTopic (id INTEGER NOT NULL, user_server_account_id INTEGER DEFAULT NULL, forum_category_id INTEGER DEFAULT NULL, last_comment_id INTEGER DEFAULT NULL, title VARCHAR(100) NOT NULL COLLATE BINARY, text CLOB NOT NULL COLLATE BINARY, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, description VARCHAR(100) NOT NULL COLLATE BINARY, views_count INTEGER NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id), CONSTRAINT FK_8D182203B1BB7436 FOREIGN KEY (user_server_account_id) REFERENCES UserServerAccount (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_8D18220314721E40 FOREIGN KEY (forum_category_id) REFERENCES ForumCategory (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_8D182203111D17F9 FOREIGN KEY (last_comment_id) REFERENCES ForumComment (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO ForumTopic (id, user_server_account_id, forum_category_id, last_comment_id, title, text, created_at, updated_at, description, views_count) SELECT id, user_server_account_id, forum_category_id, last_comment_id, title, text, created_at, updated_at, description, views_count FROM __temp__ForumTopic');
        $this->addSql('DROP TABLE __temp__ForumTopic');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D182203111D17F9 ON ForumTopic (last_comment_id)');
        $this->addSql('CREATE INDEX IDX_8D182203B1BB7436 ON ForumTopic (user_server_account_id)');
        $this->addSql('CREATE INDEX IDX_8D18220314721E40 ON ForumTopic (forum_category_id)');
        $this->addSql('DROP INDEX IDX_A264645AB1BB7436');
        $this->addSql('DROP INDEX IDX_A264645A38A6ADDA');
        $this->addSql('CREATE TEMPORARY TABLE __temp__ForumComment AS SELECT id, user_server_account_id, forum_topic_id, text, created_at, updated_at FROM ForumComment');
        $this->addSql('DROP TABLE ForumComment');
        $this->addSql('CREATE TABLE ForumComment (id INTEGER NOT NULL, user_server_account_id INTEGER DEFAULT NULL, forum_topic_id INTEGER DEFAULT NULL, text CLOB NOT NULL COLLATE BINARY, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id), CONSTRAINT FK_A264645AB1BB7436 FOREIGN KEY (user_server_account_id) REFERENCES UserServerAccount (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_A264645A38A6ADDA FOREIGN KEY (forum_topic_id) REFERENCES ForumTopic (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO ForumComment (id, user_server_account_id, forum_topic_id, text, created_at, updated_at) SELECT id, user_server_account_id, forum_topic_id, text, created_at, updated_at FROM __temp__ForumComment');
        $this->addSql('DROP TABLE __temp__ForumComment');
        $this->addSql('CREATE INDEX IDX_A264645AB1BB7436 ON ForumComment (user_server_account_id)');
        $this->addSql('CREATE INDEX IDX_A264645A38A6ADDA ON ForumComment (forum_topic_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_A264645AB1BB7436');
        $this->addSql('DROP INDEX IDX_A264645A38A6ADDA');
        $this->addSql('CREATE TEMPORARY TABLE __temp__ForumComment AS SELECT id, user_server_account_id, forum_topic_id, text, created_at, updated_at FROM ForumComment');
        $this->addSql('DROP TABLE ForumComment');
        $this->addSql('CREATE TABLE ForumComment (id INTEGER NOT NULL, user_server_account_id INTEGER DEFAULT NULL, forum_topic_id INTEGER DEFAULT NULL, text CLOB NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO ForumComment (id, user_server_account_id, forum_topic_id, text, created_at, updated_at) SELECT id, user_server_account_id, forum_topic_id, text, created_at, updated_at FROM __temp__ForumComment');
        $this->addSql('DROP TABLE __temp__ForumComment');
        $this->addSql('CREATE INDEX IDX_A264645AB1BB7436 ON ForumComment (user_server_account_id)');
        $this->addSql('CREATE INDEX IDX_A264645A38A6ADDA ON ForumComment (forum_topic_id)');
        $this->addSql('DROP INDEX IDX_8D182203B1BB7436');
        $this->addSql('DROP INDEX IDX_8D18220314721E40');
        $this->addSql('DROP INDEX UNIQ_8D182203111D17F9');
        $this->addSql('CREATE TEMPORARY TABLE __temp__ForumTopic AS SELECT id, user_server_account_id, forum_category_id, last_comment_id, title, description, text, views_count, created_at, updated_at FROM ForumTopic');
        $this->addSql('DROP TABLE ForumTopic');
        $this->addSql('CREATE TABLE ForumTopic (id INTEGER NOT NULL, user_server_account_id INTEGER DEFAULT NULL, forum_category_id INTEGER DEFAULT NULL, last_comment_id INTEGER DEFAULT NULL, title VARCHAR(100) NOT NULL, description VARCHAR(100) NOT NULL, text CLOB NOT NULL, views_count INTEGER NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO ForumTopic (id, user_server_account_id, forum_category_id, last_comment_id, title, description, text, views_count, created_at, updated_at) SELECT id, user_server_account_id, forum_category_id, last_comment_id, title, description, text, views_count, created_at, updated_at FROM __temp__ForumTopic');
        $this->addSql('DROP TABLE __temp__ForumTopic');
        $this->addSql('CREATE INDEX IDX_8D182203B1BB7436 ON ForumTopic (user_server_account_id)');
        $this->addSql('CREATE INDEX IDX_8D18220314721E40 ON ForumTopic (forum_category_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D182203111D17F9 ON ForumTopic (last_comment_id)');
    }
}
