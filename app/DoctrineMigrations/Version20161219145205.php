<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161219145205 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE HarmonyOption (id INTEGER NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AF98DF05E237E06 ON HarmonyOption (name)');
        $this->addSql('CREATE TABLE GroupHarmonyOptionLevel (id INTEGER NOT NULL, group_harmony_option_id INTEGER DEFAULT NULL, value INTEGER NOT NULL, level INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5CD2E83DAD00F165 ON GroupHarmonyOptionLevel (group_harmony_option_id)');
        $this->addSql('CREATE TABLE GroupHarmonyOption (id INTEGER NOT NULL, group_id INTEGER DEFAULT NULL, harmony_option_id INTEGER DEFAULT NULL, type INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E1A21D50FE54D947 ON GroupHarmonyOption (group_id)');
        $this->addSql('CREATE INDEX IDX_E1A21D503A93697B ON GroupHarmonyOption (harmony_option_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE HarmonyOption');
        $this->addSql('DROP TABLE GroupHarmonyOptionLevel');
        $this->addSql('DROP TABLE GroupHarmonyOption');
    }
}
