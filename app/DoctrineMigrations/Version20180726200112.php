<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180726200112 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE RegistrationConfig (id INTEGER NOT NULL, add_vip_level_id INTEGER DEFAULT NULL, add_vip_days INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8BF2DE0E898486ED ON RegistrationConfig (add_vip_level_id)');
        $this->addSql('INSERT INTO RegistrationConfig (`id`, `add_vip_level_id`, `add_vip_days`) SELECT 1, `id`, 7 FROM VipLevelConfig WHERE `type` = 4;');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE RegistrationConfig');
    }
}
