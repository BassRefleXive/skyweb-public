<?php

namespace Application\Migrations;

use AppBundle\Doctrine\Type\CharacterClassEnum;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use PDO;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161220162032 extends AbstractMigration implements ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');


        $stmt = $this->connection->prepare('INSERT INTO CharacterClass (name, abbreviation, type) VALUES (:name, :abbreviation, :type)');
        foreach (CharacterClassEnum::getTypes() as $typeId => $name) {
            $stmt->execute([
                'name'         => $name,
                'abbreviation' => CharacterClassEnum::getAbbreviationByKey($typeId),
                'type'         => $typeId,
            ]);
        }

        $stmt = $this->connection->prepare('INSERT INTO AncientSet (`name`, options) VALUES (:name, :options)');
        foreach ($this->getAncientSets() as $name => $options) {
            $options = json_encode($options);
            $stmt->bindParam('name', $name, PDO::PARAM_STR);
            $stmt->bindParam('options', $options, PDO::PARAM_STR);
            $stmt->execute();
        }


        $pvpOptions = [];

        foreach ($this->getItems() as $groupId => $groupItems) {
            foreach ($groupItems as $itemId => $itemData) {

                if ($itemData['pvp'] && !array_key_exists(md5($itemData['pvp']), $pvpOptions)) {
                    $pvpOptions[md5($itemData['pvp'])] = array_filter(explode('|', $itemData['pvp']), function ($val) {
                        return $val !== 'no';
                    });
                }

            }
        }

        $insertPvPOptionStmt = $this->connection->prepare('INSERT INTO PvPOption (options) VALUES (:options)');
        foreach ($pvpOptions as $option) {
            $insertPvPOptionStmt->execute([
                'options' => json_encode($option),
            ]);
        }

        $insertItemStmt = $this->connection->prepare('INSERT INTO ItemInfo (group_id, item_id, name, level, x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, 
        req_str, req_agi, req_ene, req_lvl, pvp_option_id, skill) VALUES ((SELECT id FROM ItemGroup WHERE type_id = :groupId), :itemId, :name, 0, :x, :y, :dropLevel, :durability, :attackSpeed, 
        :defense, 
        :minDmg, 
        :maxDmg, :wizardryDmg, :reqStr, :reqAgi, :reqEne, :reqLvl,
        (SELECT id from PvPOption WHERE options = :pvpOption), :skill)');

        $insertAncientSetItemStmt = $this->connection->prepare('INSERT INTO AncientSetItem (bonus, ancient_set_id, item_info_id) VALUES (
            :bonus, 
            (SELECT id FROM AncientSet WHERE name = :ancientSetName),
            (SELECT id FROM ItemInfo WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = :groupId) AND item_id = :itemId)
        )');

        $insertItemInfoEquippedStmt = $this->connection->prepare('INSERT INTO character_class_items (item_info_id, character_class_id) VALUES (
             (SELECT id FROM ItemInfo WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = :groupId) AND item_id = :itemId),
             (SELECT id FROM CharacterClass WHERE abbreviation = :abbreviation)
        )');

        foreach ($this->getItems() as $groupId => $groupItems) {
            foreach ($groupItems as $itemId => $itemData) {

                $pvpOption = null;

                if ($itemData['pvp']) {
                    $pvpOption = $pvpOptions[md5($itemData['pvp'])];
                }

                $insertItemStmt->execute([
                    'groupId'     => $groupId,
                    'itemId'      => $itemId,
                    'name'        => $itemData['name'],
                    'x'           => $itemData['x'],
                    'y'           => $itemData['y'],
                    'dropLevel'   => $itemData['dropLevel'],
                    'durability'  => $itemData['durability'],
                    'attackSpeed' => $itemData['attackSpeed'],
                    'defense'     => $itemData['defense'],
                    'minDmg'      => $itemData['minDmg'],
                    'maxDmg'      => $itemData['maxDmg'],
                    'wizardryDmg' => $itemData['wizardryDmg'],
                    'reqStr'      => $itemData['reqStr'],
                    'reqAgi'      => $itemData['reqAgi'],
                    'reqEne'      => $itemData['reqEne'],
                    'reqLvl'      => $itemData['reqLvl'],
                    'pvpOption'   => json_encode($pvpOption),
                    'skill'       => $itemData['skill'],
                ]);

                if (is_array($itemData['anc'])) {
                    foreach ($itemData['anc'] as $bonus => $name) {
                        if ($name && $name !== 'no') {
                            $insertAncientSetItemStmt->execute([
                                'bonus'          => $bonus,
                                'ancientSetName' => $name,
                                'groupId'        => $groupId,
                                'itemId'         => $itemId,
                            ]);
                        }
                    }
                }

                if (is_array($itemData['equipped'])) {
                    foreach ($itemData['equipped'] as $classAbbreviation) {
                        if ($classAbbreviation && $classAbbreviation !== 'no') {
                            $insertItemInfoEquippedStmt->execute([
                                'abbreviation' => mb_strtoupper($classAbbreviation),
                                'groupId'      => $groupId,
                                'itemId'       => $itemId,
                            ]);
                        }
                    }
                }

            }
        }


    }

    protected function getAncientSets(): array
    {
        $setsFile = $this->container->getParameter('kernel.root_dir') . '/data/source_ancient.json';

        if (!file_exists($setsFile)) {
            throw new \RuntimeException('No source_ancient.json found. Cannot fill database.');
        }

        return json_decode(file_get_contents($setsFile), true);
    }

    protected function getItems(): array
    {
        $setsFile = $this->container->getParameter('kernel.root_dir') . '/data/source_items.json';

        if (!file_exists($setsFile)) {
            throw new \RuntimeException('No source_items.json found. Cannot fill database.');
        }

        return json_decode(file_get_contents($setsFile), true);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DELETE FROM character_class_items');
        $this->addSql('DELETE FROM AncientSetItem');
        $this->addSql('DELETE FROM AncientSet');
        $this->addSql('DELETE FROM PvPOption');
        $this->addSql('DELETE FROM ItemInfo');
        $this->addSql('DELETE FROM CharacterClass');
    }
}
