<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180621183905 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_DCFCB9A51844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__VoteRewardConfig AS SELECT id, server_id, reward_month_top1, reward_month_top2, reward_month_top3, reward_mmotop_regular_vote, reward_mmotop_sms_vote, votes_mmotop_file_url, reward_qtop_regular_vote, reward_qtop_sms_vote, votes_qtop_file_url, reward_xtreme_top100vote, reward_gtop100vote FROM VoteRewardConfig');
        $this->addSql('DROP TABLE VoteRewardConfig');
        $this->addSql('CREATE TABLE VoteRewardConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, reward_month_top1 SMALLINT NOT NULL, reward_month_top2 SMALLINT NOT NULL, reward_month_top3 SMALLINT NOT NULL, reward_mmotop_regular_vote SMALLINT NOT NULL, reward_mmotop_sms_vote SMALLINT NOT NULL, votes_mmotop_file_url VARCHAR(255) NOT NULL COLLATE BINARY, reward_qtop_regular_vote SMALLINT NOT NULL, reward_qtop_sms_vote SMALLINT NOT NULL, votes_qtop_file_url VARCHAR(255) NOT NULL COLLATE BINARY, reward_xtreme_top100vote SMALLINT NOT NULL, reward_gtop100vote SMALLINT NOT NULL, reward_top_servers200vote SMALLINT NOT NULL, top_servers200site_id INTEGER NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_DCFCB9A51844E6B7 FOREIGN KEY (server_id) REFERENCES Server (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO VoteRewardConfig (id, server_id, reward_month_top1, reward_month_top2, reward_month_top3, reward_mmotop_regular_vote, reward_mmotop_sms_vote, votes_mmotop_file_url, reward_qtop_regular_vote, reward_qtop_sms_vote, votes_qtop_file_url, reward_xtreme_top100vote, reward_gtop100vote, reward_top_servers200vote, top_servers200site_id) SELECT id, server_id, reward_month_top1, reward_month_top2, reward_month_top3, reward_mmotop_regular_vote, reward_mmotop_sms_vote, votes_mmotop_file_url, reward_qtop_regular_vote, reward_qtop_sms_vote, votes_qtop_file_url, reward_xtreme_top100vote, reward_gtop100vote, 0, 19974 FROM __temp__VoteRewardConfig');
        $this->addSql('DROP TABLE __temp__VoteRewardConfig');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DCFCB9A51844E6B7 ON VoteRewardConfig (server_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_DCFCB9A51844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__VoteRewardConfig AS SELECT id, server_id, reward_month_top1, reward_month_top2, reward_month_top3, reward_mmotop_regular_vote, reward_mmotop_sms_vote, votes_mmotop_file_url, reward_qtop_regular_vote, reward_qtop_sms_vote, votes_qtop_file_url, reward_xtreme_top100vote, reward_gtop100vote FROM VoteRewardConfig');
        $this->addSql('DROP TABLE VoteRewardConfig');
        $this->addSql('CREATE TABLE VoteRewardConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, reward_month_top1 SMALLINT NOT NULL, reward_month_top2 SMALLINT NOT NULL, reward_month_top3 SMALLINT NOT NULL, reward_mmotop_regular_vote SMALLINT NOT NULL, reward_mmotop_sms_vote SMALLINT NOT NULL, votes_mmotop_file_url VARCHAR(255) NOT NULL, reward_qtop_regular_vote SMALLINT NOT NULL, reward_qtop_sms_vote SMALLINT NOT NULL, votes_qtop_file_url VARCHAR(255) NOT NULL, reward_xtreme_top100vote SMALLINT NOT NULL, reward_gtop100vote SMALLINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO VoteRewardConfig (id, server_id, reward_month_top1, reward_month_top2, reward_month_top3, reward_mmotop_regular_vote, reward_mmotop_sms_vote, votes_mmotop_file_url, reward_qtop_regular_vote, reward_qtop_sms_vote, votes_qtop_file_url, reward_xtreme_top100vote, reward_gtop100vote) SELECT id, server_id, reward_month_top1, reward_month_top2, reward_month_top3, reward_mmotop_regular_vote, reward_mmotop_sms_vote, votes_mmotop_file_url, reward_qtop_regular_vote, reward_qtop_sms_vote, votes_qtop_file_url, reward_xtreme_top100vote, reward_gtop100vote FROM __temp__VoteRewardConfig');
        $this->addSql('DROP TABLE __temp__VoteRewardConfig');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DCFCB9A51844E6B7 ON VoteRewardConfig (server_id)');
    }
}
