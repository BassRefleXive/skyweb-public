<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171105194015 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_1AC2CB721844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__StatsShopConfig AS SELECT id, server_id, step, step_price_credits, step_price_money FROM StatsShopConfig');
        $this->addSql('DROP TABLE StatsShopConfig');
        $this->addSql('CREATE TABLE StatsShopConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, step SMALLINT NOT NULL, step_price_credits INTEGER NOT NULL, step_price_money INTEGER NOT NULL, enabled BOOLEAN NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_1AC2CB721844E6B7 FOREIGN KEY (server_id) REFERENCES Server (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO StatsShopConfig (id, server_id, step, step_price_credits, step_price_money, enabled) SELECT id, server_id, step, step_price_credits, step_price_money, 0 FROM __temp__StatsShopConfig');
        $this->addSql('DROP TABLE __temp__StatsShopConfig');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1AC2CB721844E6B7 ON StatsShopConfig (server_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_1AC2CB721844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__StatsShopConfig AS SELECT id, server_id, step, step_price_credits, step_price_money FROM StatsShopConfig');
        $this->addSql('DROP TABLE StatsShopConfig');
        $this->addSql('CREATE TABLE StatsShopConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, step SMALLINT NOT NULL, step_price_credits INTEGER NOT NULL, step_price_money INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO StatsShopConfig (id, server_id, step, step_price_credits, step_price_money) SELECT id, server_id, step, step_price_credits, step_price_money FROM __temp__StatsShopConfig');
        $this->addSql('DROP TABLE __temp__StatsShopConfig');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1AC2CB721844E6B7 ON StatsShopConfig (server_id)');
    }
}
