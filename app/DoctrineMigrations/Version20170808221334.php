<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170808221334 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $insertAncientSetItemStmt = $this->connection->prepare('INSERT INTO AncientSetItem (bonus, ancient_set_id, item_info_id) VALUES (
            :bonus, 
            (SELECT id FROM AncientSet WHERE name = :ancientSetName),
            (SELECT id FROM ItemInfo WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = :groupId) AND item_id = :itemId)
        )');

        $insertAncientSetItemStmt->execute(['bonus' => 10, 'ancientSetName' => 'Minet\'s', 'groupId' => 8, 'itemId' => 7]);
        $insertAncientSetItemStmt->execute(['bonus' => 10, 'ancientSetName' => 'Minet\'s', 'groupId' => 9, 'itemId' => 7]);
        $insertAncientSetItemStmt->execute(['bonus' => 10, 'ancientSetName' => 'Minet\'s', 'groupId' => 11, 'itemId' => 7]);


        $stmt = $this->connection->prepare('INSERT INTO AncientSet (`name`, `options`) VALUES (:name, :options)');
        $stmt->execute([
            'name'    => 'Hera',
            'options' => json_encode([
                'Increase Strength +15',
                'Increase Wizardry Damage +10%',
                'Increase Defense with Shield +5%',
                'Increase Energy +15',
                'Increase Damage Success Rate +50',
                'Increase Critical Damage Rate +10%',
                'Increase Excellent Damage Rate +10%',
                'Increase HP Recovery +50',
                'Increase Mana Recovery +50',
            ]),
        ]);

        $insertAncientSetItemStmt->execute(['bonus' => 5, 'ancientSetName' => 'Hera', 'groupId' => 7, 'itemId' => 7]);
        $insertAncientSetItemStmt->execute(['bonus' => 5, 'ancientSetName' => 'Hera', 'groupId' => 8, 'itemId' => 7]);
        $insertAncientSetItemStmt->execute(['bonus' => 5, 'ancientSetName' => 'Hera', 'groupId' => 9, 'itemId' => 7]);
        $insertAncientSetItemStmt->execute(['bonus' => 5, 'ancientSetName' => 'Hera', 'groupId' => 10, 'itemId' => 7]);
        $insertAncientSetItemStmt->execute(['bonus' => 5, 'ancientSetName' => 'Hera', 'groupId' => 11, 'itemId' => 7]);
        $insertAncientSetItemStmt->execute(['bonus' => 5, 'ancientSetName' => 'Hera', 'groupId' => 6, 'itemId' => 6]);

        $this->addSql('UPDATE AncientSet SET `name` = SUBSTR(`name`, -2, -LENGTH(`name`)) WHERE `name` LIKE "%\'s";');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');
    }
}
