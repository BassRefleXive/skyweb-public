<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180215214651 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TEMPORARY TABLE __temp__Skill AS SELECT id, name, damage, mana_usage, stamina_usage, distance, delay, required_level, required_strength, required_agility, required_energy, required_command, required_mlpoints, attribute, type, use_type FROM Skill');
        $this->addSql('DROP TABLE Skill');
        $this->addSql('CREATE TABLE Skill (id INTEGER NOT NULL, name VARCHAR(50) NOT NULL COLLATE BINARY, damage INTEGER NOT NULL, mana_usage INTEGER NOT NULL, stamina_usage INTEGER NOT NULL, distance INTEGER NOT NULL, delay INTEGER NOT NULL, required_level INTEGER NOT NULL, required_strength INTEGER NOT NULL, required_agility INTEGER NOT NULL, required_energy INTEGER NOT NULL, required_command INTEGER NOT NULL, required_mlpoints INTEGER NOT NULL, attribute integer NOT NULL, type integer NOT NULL, use_type integer NOT NULL, icon_number INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO Skill (id, name, damage, mana_usage, stamina_usage, distance, delay, required_level, required_strength, required_agility, required_energy, required_command, required_mlpoints, attribute, type, use_type, icon_number) SELECT id, name, damage, mana_usage, stamina_usage, distance, delay, required_level, required_strength, required_agility, required_energy, required_command, required_mlpoints, attribute, type, use_type, 0 FROM __temp__Skill');
        $this->addSql('DROP TABLE __temp__Skill');
     }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TEMPORARY TABLE __temp__Skill AS SELECT id, name, damage, mana_usage, stamina_usage, distance, delay, required_level, required_strength, required_agility, required_energy, required_command, required_mlpoints, attribute, type, use_type FROM Skill');
        $this->addSql('DROP TABLE Skill');
        $this->addSql('CREATE TABLE Skill (id INTEGER NOT NULL, name VARCHAR(50) NOT NULL, damage INTEGER NOT NULL, mana_usage INTEGER NOT NULL, stamina_usage INTEGER NOT NULL, distance INTEGER NOT NULL, delay INTEGER NOT NULL, required_level INTEGER NOT NULL, required_strength INTEGER NOT NULL, required_agility INTEGER NOT NULL, required_energy INTEGER NOT NULL, required_command INTEGER NOT NULL, required_mlpoints INTEGER NOT NULL, attribute INTEGER NOT NULL, type INTEGER NOT NULL, use_type INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO Skill (id, name, damage, mana_usage, stamina_usage, distance, delay, required_level, required_strength, required_agility, required_energy, required_command, required_mlpoints, attribute, type, use_type) SELECT id, name, damage, mana_usage, stamina_usage, distance, delay, required_level, required_strength, required_agility, required_energy, required_command, required_mlpoints, attribute, type, use_type FROM __temp__Skill');
        $this->addSql('DROP TABLE __temp__Skill');
    }
}
