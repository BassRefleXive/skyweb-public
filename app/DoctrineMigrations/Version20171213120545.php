<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171213120545 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');


        $this->addSql('DROP TABLE ForumCategory');
        $this->addSql('DROP TABLE ForumComment');
        $this->addSql('DROP TABLE ForumTopic');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE ForumCategory (id INTEGER NOT NULL, last_comment_id INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL COLLATE BINARY, description CLOB NOT NULL COLLATE BINARY, total_comments INTEGER NOT NULL, total_topics INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C9C09C6E111D17F9 ON ForumCategory (last_comment_id)');
        $this->addSql('CREATE INDEX IDX_C9C09C6E1844E6B7 ON ForumCategory (server_id)');
        $this->addSql('CREATE TABLE ForumComment (id INTEGER NOT NULL, user_server_account_id INTEGER DEFAULT NULL, forum_topic_id INTEGER DEFAULT NULL, deleted_by INTEGER DEFAULT NULL, text CLOB NOT NULL COLLATE BINARY, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A264645A1F6FA0AF ON ForumComment (deleted_by)');
        $this->addSql('CREATE INDEX IDX_A264645AB1BB7436 ON ForumComment (user_server_account_id)');
        $this->addSql('CREATE INDEX IDX_A264645A38A6ADDA ON ForumComment (forum_topic_id)');
        $this->addSql('CREATE TABLE ForumTopic (id INTEGER NOT NULL, user_server_account_id INTEGER DEFAULT NULL, forum_category_id INTEGER DEFAULT NULL, last_comment_id INTEGER DEFAULT NULL, deleted_by INTEGER DEFAULT NULL, title VARCHAR(100) NOT NULL COLLATE BINARY, text CLOB NOT NULL COLLATE BINARY, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, description VARCHAR(100) NOT NULL COLLATE BINARY, views_count INTEGER NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8D1822031F6FA0AF ON ForumTopic (deleted_by)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D182203111D17F9 ON ForumTopic (last_comment_id)');
        $this->addSql('CREATE INDEX IDX_8D182203B1BB7436 ON ForumTopic (user_server_account_id)');
        $this->addSql('CREATE INDEX IDX_8D18220314721E40 ON ForumTopic (forum_category_id)');
    }
}
