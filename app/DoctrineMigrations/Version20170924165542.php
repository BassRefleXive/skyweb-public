<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170924165542 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE DownloadLinkGroup (id INTEGER NOT NULL, name VARCHAR(255) NOT NULL, size INTEGER NOT NULL, PRIMARY KEY(id))');

        $this->addSql('CREATE TEMPORARY TABLE __temp__DownloadLink AS SELECT id, link FROM DownloadLink');
        $this->addSql('DROP TABLE DownloadLink');
        $this->addSql('CREATE TABLE DownloadLink (id INTEGER NOT NULL, group_id INTEGER DEFAULT NULL, link VARCHAR(255) NOT NULL COLLATE BINARY, provider VARCHAR(255) NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_E9D28190FE54D947 FOREIGN KEY (group_id) REFERENCES DownloadLinkGroup (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO DownloadLink (id, link) SELECT id, link FROM __temp__DownloadLink');
        $this->addSql('DROP TABLE __temp__DownloadLink');
        $this->addSql('CREATE INDEX IDX_E9D28190FE54D947 ON DownloadLink (group_id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE DownloadLinkGroup');

        $this->addSql('DROP INDEX IDX_E9D28190FE54D947');
        $this->addSql('CREATE TEMPORARY TABLE __temp__DownloadLink AS SELECT id, link, provider FROM DownloadLink');
        $this->addSql('DROP TABLE DownloadLink');
        $this->addSql('CREATE TABLE DownloadLink (id INTEGER NOT NULL, link VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL COLLATE BINARY, description VARCHAR(255) NOT NULL COLLATE BINARY, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO DownloadLink (id, link, name) SELECT id, link, provider FROM __temp__DownloadLink');
        $this->addSql('DROP TABLE __temp__DownloadLink');
    }
}
