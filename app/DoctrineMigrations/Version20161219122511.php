<?php

namespace Application\Migrations;

use AppBundle\Doctrine\Type\ItemGroupEnum;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161219122511 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        foreach (ItemGroupEnum::getTypes() as $typeId => $name) {
            $this->addSql('INSERT INTO ItemGroup (name, type_id) VALUES ("' . $name . '", ' . $typeId . ')');
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DELETE FROM ItemGroup');

    }
}
