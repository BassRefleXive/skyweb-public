<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171220235825 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_32A293A1844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__CharacterEvolutionConfig AS SELECT id, server_id, reset_level, reset_type, reset_points_dk, reset_points_dw, reset_points_fe, reset_points_mg, reset_points_dl, reset_points_sum, reset_points_rf, reset_payment_type, reset_price, reset_reward, reset_limit FROM CharacterEvolutionConfig');
        $this->addSql('DROP TABLE CharacterEvolutionConfig');
        $this->addSql('CREATE TABLE CharacterEvolutionConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, reset_level INTEGER NOT NULL, reset_points_dk INTEGER NOT NULL, reset_points_dw INTEGER NOT NULL, reset_points_fe INTEGER NOT NULL, reset_points_mg INTEGER NOT NULL, reset_points_dl INTEGER NOT NULL, reset_points_sum INTEGER NOT NULL, reset_points_rf INTEGER NOT NULL, reset_price INTEGER NOT NULL, reset_reward INTEGER NOT NULL, reset_limit INTEGER NOT NULL, reset_type integer NOT NULL, reset_payment_type integer NOT NULL, grand_reset_reset integer NOT NULL, grand_reset_price integer NOT NULL, grand_reset_points integer NOT NULL, grand_reset_reward integer NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_32A293A1844E6B7 FOREIGN KEY (server_id) REFERENCES Server (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO CharacterEvolutionConfig (id, server_id, reset_level, reset_type, reset_points_dk, reset_points_dw, reset_points_fe, reset_points_mg, reset_points_dl, reset_points_sum, reset_points_rf, reset_payment_type, reset_price, reset_reward, reset_limit, grand_reset_reset, grand_reset_price, grand_reset_points, grand_reset_reward) SELECT id, server_id, reset_level, reset_type, reset_points_dk, reset_points_dw, reset_points_fe, reset_points_mg, reset_points_dl, reset_points_sum, reset_points_rf, reset_payment_type, reset_price, reset_reward, reset_limit, 0, 0, 0, 0 FROM __temp__CharacterEvolutionConfig');
        $this->addSql('DROP TABLE __temp__CharacterEvolutionConfig');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_32A293A1844E6B7 ON CharacterEvolutionConfig (server_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_32A293A1844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__CharacterEvolutionConfig AS SELECT id, server_id, reset_level, reset_type, reset_points_dk, reset_points_dw, reset_points_fe, reset_points_mg, reset_points_dl, reset_points_sum, reset_points_rf, reset_payment_type, reset_price, reset_reward, reset_limit FROM CharacterEvolutionConfig');
        $this->addSql('DROP TABLE CharacterEvolutionConfig');
        $this->addSql('CREATE TABLE CharacterEvolutionConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, reset_level INTEGER NOT NULL, reset_points_dk INTEGER NOT NULL, reset_points_dw INTEGER NOT NULL, reset_points_fe INTEGER NOT NULL, reset_points_mg INTEGER NOT NULL, reset_points_dl INTEGER NOT NULL, reset_points_sum INTEGER NOT NULL, reset_points_rf INTEGER NOT NULL, reset_price INTEGER NOT NULL, reset_reward INTEGER NOT NULL, reset_limit INTEGER NOT NULL, reset_type INTEGER NOT NULL, reset_payment_type INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO CharacterEvolutionConfig (id, server_id, reset_level, reset_type, reset_points_dk, reset_points_dw, reset_points_fe, reset_points_mg, reset_points_dl, reset_points_sum, reset_points_rf, reset_payment_type, reset_price, reset_reward, reset_limit) SELECT id, server_id, reset_level, reset_type, reset_points_dk, reset_points_dw, reset_points_fe, reset_points_mg, reset_points_dl, reset_points_sum, reset_points_rf, reset_payment_type, reset_price, reset_reward, reset_limit FROM __temp__CharacterEvolutionConfig');
        $this->addSql('DROP TABLE __temp__CharacterEvolutionConfig');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_32A293A1844E6B7 ON CharacterEvolutionConfig (server_id)');
    }
}
