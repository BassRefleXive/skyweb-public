<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170105155517 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE MarketItem (id INTEGER NOT NULL, item_info_id INTEGER DEFAULT NULL, user_server_account_id INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, hex VARCHAR(100) NOT NULL, level SMALLINT NOT NULL, option SMALLINT NOT NULL, skill BOOLEAN NOT NULL, luck BOOLEAN NOT NULL, excellent BOOLEAN NOT NULL, ancient BOOLEAN NOT NULL, harmony BOOLEAN NOT NULL, pvp BOOLEAN NOT NULL, price INTEGER NOT NULL, price_type VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6E1472A956D6E7AC ON MarketItem (item_info_id)');
        $this->addSql('CREATE INDEX IDX_6E1472A9B1BB7436 ON MarketItem (user_server_account_id)');
        $this->addSql('CREATE INDEX IDX_6E1472A91844E6B7 ON MarketItem (server_id)');
        $this->addSql('CREATE INDEX MarketItem_index_item_specs ON MarketItem (level, option, skill, luck, excellent, ancient, harmony, pvp, price, price_type)');
        $this->addSql('CREATE TABLE MarketCategory (id INTEGER NOT NULL, name VARCHAR(50) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE MarketConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, commission SMALLINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8A01A25B1844E6B7 ON MarketConfig (server_id)');

        $this->addSql('DROP INDEX IDX_38B39EF7FE54D947');
        $this->addSql('DROP INDEX IDX_38B39EF7890798E5');
        $this->addSql('DROP INDEX idx_item_id');
        $this->addSql('CREATE TEMPORARY TABLE __temp__ItemInfo AS SELECT id, group_id, pvp_option_id, item_id, name, level, x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, req_str, req_agi, req_ene, req_lvl, skill, wizardry_excellent_dmg FROM ItemInfo');
        $this->addSql('DROP TABLE ItemInfo');
        $this->addSql('CREATE TABLE ItemInfo (id INTEGER NOT NULL, group_id INTEGER DEFAULT NULL, pvp_option_id INTEGER DEFAULT NULL, market_category_id INTEGER DEFAULT NULL, item_id INTEGER NOT NULL, name VARCHAR(100) NOT NULL COLLATE BINARY, level INTEGER NOT NULL, x INTEGER NOT NULL, y INTEGER NOT NULL, drop_level INTEGER DEFAULT NULL, durability INTEGER DEFAULT NULL, attack_speed INTEGER DEFAULT NULL, defense INTEGER DEFAULT NULL, min_dmg INTEGER DEFAULT NULL, max_dmg INTEGER DEFAULT NULL, wizardry_dmg INTEGER DEFAULT NULL, req_str INTEGER DEFAULT NULL, req_agi INTEGER DEFAULT NULL, req_ene INTEGER DEFAULT NULL, req_lvl INTEGER DEFAULT NULL, skill VARCHAR(100) DEFAULT NULL COLLATE BINARY, wizardry_excellent_dmg INTEGER DEFAULT NULL, PRIMARY KEY(id), CONSTRAINT FK_38B39EF7FE54D947 FOREIGN KEY (group_id) REFERENCES ItemGroup (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_38B39EF7890798E5 FOREIGN KEY (pvp_option_id) REFERENCES PvPOption (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_38B39EF76293FF9 FOREIGN KEY (market_category_id) REFERENCES MarketCategory (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO ItemInfo (id, group_id, pvp_option_id, item_id, name, level, x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, req_str, req_agi, req_ene, req_lvl, skill, wizardry_excellent_dmg) SELECT id, group_id, pvp_option_id, item_id, name, level, x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, req_str, req_agi, req_ene, req_lvl, skill, wizardry_excellent_dmg FROM __temp__ItemInfo');
        $this->addSql('DROP TABLE __temp__ItemInfo');
        $this->addSql('CREATE INDEX IDX_38B39EF7FE54D947 ON ItemInfo (group_id)');
        $this->addSql('CREATE INDEX IDX_38B39EF7890798E5 ON ItemInfo (pvp_option_id)');
        $this->addSql('CREATE INDEX idx_item_id ON ItemInfo (item_id)');
        $this->addSql('CREATE INDEX IDX_38B39EF76293FF9 ON ItemInfo (market_category_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE MarketItem');
        $this->addSql('DROP TABLE MarketCategory');
        $this->addSql('DROP TABLE MarketConfig');

        $this->addSql('DROP INDEX IDX_38B39EF7FE54D947');
        $this->addSql('DROP INDEX IDX_38B39EF7890798E5');
        $this->addSql('DROP INDEX IDX_38B39EF76293FF9');
        $this->addSql('DROP INDEX idx_item_id');
        $this->addSql('CREATE TEMPORARY TABLE __temp__ItemInfo AS SELECT id, group_id, pvp_option_id, item_id, name, level, x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill FROM ItemInfo');
        $this->addSql('DROP TABLE ItemInfo');
        $this->addSql('CREATE TABLE ItemInfo (id INTEGER NOT NULL, group_id INTEGER DEFAULT NULL, pvp_option_id INTEGER DEFAULT NULL, item_id INTEGER NOT NULL, name VARCHAR(100) NOT NULL, level INTEGER NOT NULL, x INTEGER NOT NULL, y INTEGER NOT NULL, drop_level INTEGER DEFAULT NULL, durability INTEGER DEFAULT NULL, attack_speed INTEGER DEFAULT NULL, defense INTEGER DEFAULT NULL, min_dmg INTEGER DEFAULT NULL, max_dmg INTEGER DEFAULT NULL, wizardry_dmg INTEGER DEFAULT NULL, wizardry_excellent_dmg INTEGER DEFAULT NULL, req_str INTEGER DEFAULT NULL, req_agi INTEGER DEFAULT NULL, req_ene INTEGER DEFAULT NULL, req_lvl INTEGER DEFAULT NULL, skill VARCHAR(100) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO ItemInfo (id, group_id, pvp_option_id, item_id, name, level, x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill) SELECT id, group_id, pvp_option_id, item_id, name, level, x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill FROM __temp__ItemInfo');
        $this->addSql('DROP TABLE __temp__ItemInfo');
        $this->addSql('CREATE INDEX IDX_38B39EF7FE54D947 ON ItemInfo (group_id)');
        $this->addSql('CREATE INDEX IDX_38B39EF7890798E5 ON ItemInfo (pvp_option_id)');
        $this->addSql('CREATE INDEX idx_item_id ON ItemInfo (item_id)');
    }
}
