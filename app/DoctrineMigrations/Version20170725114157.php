<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170725114157 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('UPDATE ItemInfo SET `name` = "Search Event Box" WHERE `group_id` = 13 AND `item_id` = 32;');
        $this->addSql('UPDATE ItemInfo SET `name` = "Blood Castle Box" WHERE `group_id` = 13 AND `item_id` = 33;');
        $this->addSql('UPDATE ItemInfo SET `name` = "TvT Box" WHERE `group_id` = 13 AND `item_id` = 34;');

        $this->addSql('UPDATE ItemInfo SET `name` = "Quest Reward Box #1" WHERE `group_id` = 15 AND `item_id` = 32 AND `level` = 0;');
        $this->addSql('UPDATE ItemInfo SET `name` = "Quest Reward Box #2" WHERE `group_id` = 15 AND `item_id` = 33 AND `level` = 0;');
        $this->addSql('UPDATE ItemInfo SET `name` = "Quest Reward Box #3" WHERE `group_id` = 15 AND `item_id` = 34 AND `level` = 0;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('UPDATE ItemInfo SET `name` = "Red Ribbon Box" WHERE `group_id` = 13 AND `item_id` = 32;');
        $this->addSql('UPDATE ItemInfo SET `name` = "Green Ribbon Box" WHERE `group_id` = 13 AND `item_id` = 33;');
        $this->addSql('UPDATE ItemInfo SET `name` = "Blue Ribbon Box" WHERE `group_id` = 13 AND `item_id` = 34;');

        $this->addSql('UPDATE ItemInfo SET `name` = "Pink Chocolate Box" WHERE `group_id` = 15 AND `item_id` = 32 AND `level` = 0;');
        $this->addSql('UPDATE ItemInfo SET `name` = "Red Chocolate Box" WHERE `group_id` = 15 AND `item_id` = 33 AND `level` = 0;');
        $this->addSql('UPDATE ItemInfo SET `name` = "Blue Chocolate Box" WHERE `group_id` = 15 AND `item_id` = 34 AND `level` = 0;');
    }
}
