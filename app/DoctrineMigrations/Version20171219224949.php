<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171219224949 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_758ED6D9FA0AD537');
        $this->addSql('DROP INDEX IDX_758ED6D91844E6B7');
        $this->addSql('DROP INDEX IDX_758ED6D9A76ED395');
        $this->addSql('CREATE TEMPORARY TABLE __temp__UserServerAccount AS SELECT id, user_id, server_id, vip_level_id, account_id, selected_at, spent_money, spent_credits, purchased_stats, hidden_till, zen, vip_expire_date FROM UserServerAccount');
        $this->addSql('DROP TABLE UserServerAccount');
        $this->addSql('CREATE TABLE UserServerAccount (id INTEGER NOT NULL, user_id INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, vip_level_id INTEGER DEFAULT NULL, account_id VARCHAR(10) NOT NULL COLLATE BINARY, selected_at DATETIME NOT NULL, spent_money DOUBLE PRECISION NOT NULL, spent_credits INTEGER NOT NULL, purchased_stats INTEGER NOT NULL, hidden_till DATETIME DEFAULT NULL, zen INTEGER NOT NULL, vip_expire_date DATETIME DEFAULT NULL, vip_days SMALLINT DEFAULT NULL, PRIMARY KEY(id), CONSTRAINT FK_758ED6D9A76ED395 FOREIGN KEY (user_id) REFERENCES User (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_758ED6D91844E6B7 FOREIGN KEY (server_id) REFERENCES Server (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_758ED6D9FA0AD537 FOREIGN KEY (vip_level_id) REFERENCES VipLevelConfig (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO UserServerAccount (id, user_id, server_id, vip_level_id, account_id, selected_at, spent_money, spent_credits, purchased_stats, hidden_till, zen, vip_expire_date) SELECT id, user_id, server_id, vip_level_id, account_id, selected_at, spent_money, spent_credits, purchased_stats, hidden_till, zen, vip_expire_date FROM __temp__UserServerAccount');
        $this->addSql('DROP TABLE __temp__UserServerAccount');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_758ED6D9FA0AD537 ON UserServerAccount (vip_level_id)');
        $this->addSql('CREATE INDEX IDX_758ED6D91844E6B7 ON UserServerAccount (server_id)');
        $this->addSql('CREATE INDEX IDX_758ED6D9A76ED395 ON UserServerAccount (user_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_758ED6D9A76ED395');
        $this->addSql('DROP INDEX IDX_758ED6D91844E6B7');
        $this->addSql('DROP INDEX UNIQ_758ED6D9FA0AD537');
        $this->addSql('CREATE TEMPORARY TABLE __temp__UserServerAccount AS SELECT id, user_id, server_id, vip_level_id, account_id, selected_at, spent_money, spent_credits, purchased_stats, hidden_till, zen, vip_expire_date FROM UserServerAccount');
        $this->addSql('DROP TABLE UserServerAccount');
        $this->addSql('CREATE TABLE UserServerAccount (id INTEGER NOT NULL, user_id INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, vip_level_id INTEGER DEFAULT NULL, account_id VARCHAR(10) NOT NULL, selected_at DATETIME NOT NULL, spent_money DOUBLE PRECISION NOT NULL, spent_credits INTEGER NOT NULL, purchased_stats INTEGER NOT NULL, hidden_till DATETIME DEFAULT NULL, zen INTEGER NOT NULL, vip_expire_date DATETIME DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO UserServerAccount (id, user_id, server_id, vip_level_id, account_id, selected_at, spent_money, spent_credits, purchased_stats, hidden_till, zen, vip_expire_date) SELECT id, user_id, server_id, vip_level_id, account_id, selected_at, spent_money, spent_credits, purchased_stats, hidden_till, zen, vip_expire_date FROM __temp__UserServerAccount');
        $this->addSql('DROP TABLE __temp__UserServerAccount');
        $this->addSql('CREATE INDEX IDX_758ED6D9A76ED395 ON UserServerAccount (user_id)');
        $this->addSql('CREATE INDEX IDX_758ED6D91844E6B7 ON UserServerAccount (server_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_758ED6D9FA0AD537 ON UserServerAccount (vip_level_id)');
    }
}
