<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170803141552 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE BonusCode (code VARCHAR(10) NOT NULL, activated_by INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, type VARCHAR(255) NOT NULL, amount INTEGER NOT NULL, status SMALLINT NOT NULL, expire_at DATETIME NOT NULL, activated_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(code))');
        $this->addSql('CREATE INDEX IDX_D7D1FC57B1BB7436 ON BonusCode (activated_by)');
        $this->addSql('CREATE INDEX IDX_D7D1FC571844E6B7 ON BonusCode (server_id)');
   }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE BonusCode');
    }
}
