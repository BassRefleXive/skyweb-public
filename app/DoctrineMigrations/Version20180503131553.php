<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180503131553 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('INSERT INTO AncientSetItem (ancient_set_id, item_info_id, bonus) SELECT ancient_set_id, item_info_id, 10 FROM AncientSetItem WHERE ancient_set_id = 66;');
        $this->addSql('INSERT INTO AncientSetItem (ancient_set_id, item_info_id, bonus) SELECT ancient_set_id, item_info_id, 10 FROM AncientSetItem WHERE ancient_set_id = 41;');
        $this->addSql('INSERT INTO AncientSetItem (ancient_set_id, item_info_id, bonus) SELECT ancient_set_id, item_info_id, 10 FROM AncientSetItem WHERE ancient_set_id = 32;');
        $this->addSql('INSERT INTO AncientSetItem (ancient_set_id, item_info_id, bonus) SELECT ancient_set_id, item_info_id, 10 FROM AncientSetItem WHERE ancient_set_id = 12;');
        $this->addSql('INSERT INTO AncientSetItem (ancient_set_id, item_info_id, bonus) SELECT ancient_set_id, item_info_id, 10 FROM AncientSetItem WHERE ancient_set_id = 38;');
        $this->addSql('INSERT INTO AncientSetItem (ancient_set_id, item_info_id, bonus) SELECT ancient_set_id, item_info_id, 10 FROM AncientSetItem WHERE ancient_set_id = 34;');
        $this->addSql('INSERT INTO AncientSetItem (ancient_set_id, item_info_id, bonus) SELECT ancient_set_id, item_info_id, 10 FROM AncientSetItem WHERE ancient_set_id = 4;');
        $this->addSql('INSERT INTO AncientSetItem (ancient_set_id, item_info_id, bonus) SELECT ancient_set_id, item_info_id, 10 FROM AncientSetItem WHERE ancient_set_id = 24;');
        $this->addSql('INSERT INTO AncientSetItem (ancient_set_id, item_info_id, bonus) SELECT ancient_set_id, item_info_id, 10 FROM AncientSetItem WHERE ancient_set_id = 42;');
        $this->addSql('INSERT INTO AncientSetItem (ancient_set_id, item_info_id, bonus) SELECT ancient_set_id, item_info_id, 10 FROM AncientSetItem WHERE ancient_set_id = 9;');
        $this->addSql('INSERT INTO AncientSetItem (ancient_set_id, item_info_id, bonus) SELECT ancient_set_id, item_info_id, 10 FROM AncientSetItem WHERE ancient_set_id = 21;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DELETE FROM AncientSetItem WHERE ancient_set_id = 66 AND bonus = 10;');
        $this->addSql('DELETE FROM AncientSetItem WHERE ancient_set_id = 41 AND bonus = 10;');
        $this->addSql('DELETE FROM AncientSetItem WHERE ancient_set_id = 32 AND bonus = 10;');
        $this->addSql('DELETE FROM AncientSetItem WHERE ancient_set_id = 12 AND bonus = 10;');
        $this->addSql('DELETE FROM AncientSetItem WHERE ancient_set_id = 38 AND bonus = 10;');
        $this->addSql('DELETE FROM AncientSetItem WHERE ancient_set_id = 34 AND bonus = 10;');
        $this->addSql('DELETE FROM AncientSetItem WHERE ancient_set_id = 4 AND bonus = 10;');
        $this->addSql('DELETE FROM AncientSetItem WHERE ancient_set_id = 24 AND bonus = 10;');
        $this->addSql('DELETE FROM AncientSetItem WHERE ancient_set_id = 42 AND bonus = 10;');
        $this->addSql('DELETE FROM AncientSetItem WHERE ancient_set_id = 9 AND bonus = 10;');
        $this->addSql('DELETE FROM AncientSetItem WHERE ancient_set_id = 21 AND bonus = 10;');
    }
}
