<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171214012213 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE CharacterEvolutionConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, reset_level INTEGER NOT NULL, reset_type integer NOT NULL, reset_points_dk INTEGER NOT NULL, reset_points_dw INTEGER NOT NULL, reset_points_fe INTEGER NOT NULL, reset_points_mg INTEGER NOT NULL, reset_points_dl INTEGER NOT NULL, reset_points_sum INTEGER NOT NULL, reset_points_rf INTEGER NOT NULL, reset_payment_type integer NOT NULL, reset_price INTEGER NOT NULL, reset_reward INTEGER NOT NULL, reset_limit INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_32A293A1844E6B7 ON CharacterEvolutionConfig (server_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE CharacterEvolutionConfig');
        $this->addSql('DROP INDEX UNIQ_C6AAADDD1844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__AccountMenuConfig AS SELECT id, server_id, nick_change_price, class_change_price, reset_stats_price, hide_info_daily_rate, online_hours_rate FROM AccountMenuConfig');
        $this->addSql('DROP TABLE AccountMenuConfig');
        $this->addSql('CREATE TABLE AccountMenuConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, nick_change_price INTEGER NOT NULL, class_change_price INTEGER NOT NULL, reset_stats_price INTEGER NOT NULL, hide_info_daily_rate INTEGER NOT NULL, online_hours_rate INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO AccountMenuConfig (id, server_id, nick_change_price, class_change_price, reset_stats_price, hide_info_daily_rate, online_hours_rate) SELECT id, server_id, nick_change_price, class_change_price, reset_stats_price, hide_info_daily_rate, online_hours_rate FROM __temp__AccountMenuConfig');
        $this->addSql('DROP TABLE __temp__AccountMenuConfig');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C6AAADDD1844E6B7 ON AccountMenuConfig (server_id)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__AncientSet AS SELECT id, name, enabled, options FROM AncientSet');
        $this->addSql('DROP TABLE AncientSet');
        $this->addSql('CREATE TABLE AncientSet (id INTEGER NOT NULL, name VARCHAR(100) NOT NULL, enabled BOOLEAN NOT NULL, options CLOB NOT NULL COLLATE BINARY, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO AncientSet (id, name, enabled, options) SELECT id, name, enabled, options FROM __temp__AncientSet');
        $this->addSql('DROP TABLE __temp__AncientSet');
        $this->addSql('DROP INDEX IDX_DE03DB73BFF8E8BE');
        $this->addSql('DROP INDEX IDX_DE03DB7356D6E7AC');
        $this->addSql('CREATE TEMPORARY TABLE __temp__AncientSetItem AS SELECT id, ancient_set_id, item_info_id, bonus FROM AncientSetItem');
        $this->addSql('DROP TABLE AncientSetItem');
        $this->addSql('CREATE TABLE AncientSetItem (id INTEGER NOT NULL, ancient_set_id INTEGER DEFAULT NULL, item_info_id INTEGER DEFAULT NULL, bonus INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO AncientSetItem (id, ancient_set_id, item_info_id, bonus) SELECT id, ancient_set_id, item_info_id, bonus FROM __temp__AncientSetItem');
        $this->addSql('DROP TABLE __temp__AncientSetItem');
        $this->addSql('CREATE INDEX IDX_DE03DB73BFF8E8BE ON AncientSetItem (ancient_set_id)');
        $this->addSql('CREATE INDEX IDX_DE03DB7356D6E7AC ON AncientSetItem (item_info_id)');
        $this->addSql('DROP INDEX IDX_5AB39605A76ED395');
        $this->addSql('DROP INDEX IDX_5AB396059DCE7562');
        $this->addSql('CREATE TEMPORARY TABLE __temp__Ban AS SELECT id, user_id, banned_by, reason, link, expire_at, un_ban_processed, created_at, updated_at FROM Ban');
        $this->addSql('DROP TABLE Ban');
        $this->addSql('CREATE TABLE Ban (id INTEGER NOT NULL, user_id INTEGER DEFAULT NULL, banned_by INTEGER DEFAULT NULL, reason VARCHAR(255) NOT NULL, link VARCHAR(255) DEFAULT NULL, expire_at DATETIME NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, un_ban_processed INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO Ban (id, user_id, banned_by, reason, link, expire_at, un_ban_processed, created_at, updated_at) SELECT id, user_id, banned_by, reason, link, expire_at, un_ban_processed, created_at, updated_at FROM __temp__Ban');
        $this->addSql('DROP TABLE __temp__Ban');
        $this->addSql('CREATE INDEX IDX_5AB39605A76ED395 ON Ban (user_id)');
        $this->addSql('CREATE INDEX IDX_5AB396059DCE7562 ON Ban (banned_by)');
        $this->addSql('DROP INDEX IDX_D7D1FC57255FA5E4');
        $this->addSql('DROP INDEX IDX_D7D1FC571844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__BonusCode AS SELECT code, activated_by, server_id, amount, status, expire_at, activated_at, created_at, updated_at FROM BonusCode');
        $this->addSql('DROP TABLE BonusCode');
        $this->addSql('CREATE TABLE BonusCode (code VARCHAR(10) NOT NULL, activated_by INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, amount INTEGER NOT NULL, status SMALLINT NOT NULL, expire_at DATETIME NOT NULL, activated_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(code))');
        $this->addSql('INSERT INTO BonusCode (code, activated_by, server_id, amount, status, expire_at, activated_at, created_at, updated_at) SELECT code, activated_by, server_id, amount, status, expire_at, activated_at, created_at, updated_at FROM __temp__BonusCode');
        $this->addSql('DROP TABLE __temp__BonusCode');
        $this->addSql('CREATE INDEX IDX_D7D1FC57255FA5E4 ON BonusCode (activated_by)');
        $this->addSql('CREATE INDEX IDX_D7D1FC571844E6B7 ON BonusCode (server_id)');
        $this->addSql('DROP INDEX UNIQ_AB912789B1BB7436');
        $this->addSql('CREATE TEMPORARY TABLE __temp__Cart AS SELECT id, user_server_account_id FROM Cart');
        $this->addSql('DROP TABLE Cart');
        $this->addSql('CREATE TABLE Cart (id INTEGER NOT NULL, user_server_account_id INTEGER DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO Cart (id, user_server_account_id) SELECT id, user_server_account_id FROM __temp__Cart');
        $this->addSql('DROP TABLE __temp__Cart');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AB912789B1BB7436 ON Cart (user_server_account_id)');
        $this->addSql('DROP INDEX IDX_C96BD8971844E6B7');
        $this->addSql('DROP INDEX CartDiscountCoupon_coupon_UNIQUE');
        $this->addSql('CREATE TEMPORARY TABLE __temp__CartDiscountCoupon AS SELECT id, server_id, coupon, discount, type, status, expire_at, created_at, updated_at FROM CartDiscountCoupon');
    }
}
