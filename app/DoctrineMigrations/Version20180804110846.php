<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180804110846 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_66EF02AC4D6EF82');
        $this->addSql('DROP INDEX UNIQ_66EF02AC2D6699C7');

        $this->addSql('CREATE UNIQUE INDEX UNIQ_66EF02AC4D6EF82 ON ReferralReward (referral_user_server_account_id, referred_user_server_account_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_66EF02AC4D6EF82');

        $this->addSql('CREATE UNIQUE INDEX UNIQ_66EF02AC4D6EF82 ON ReferralReward (referral_user_server_account_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_66EF02AC2D6699C7 ON ReferralReward (referred_user_server_account_id)');
    }
}
