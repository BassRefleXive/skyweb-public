<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180214175649 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');


        $this->addSql('CREATE TABLE Skill (id INTEGER NOT NULL, name VARCHAR(50) NOT NULL, damage INTEGER NOT NULL, mana_usage INTEGER NOT NULL, stamina_usage INTEGER NOT NULL, distance INTEGER NOT NULL, delay INTEGER NOT NULL, required_level INTEGER NOT NULL, required_strength INTEGER NOT NULL, required_agility INTEGER NOT NULL, required_energy INTEGER NOT NULL, required_command INTEGER NOT NULL, required_mlpoints INTEGER NOT NULL, attribute integer NOT NULL, type integer NOT NULL, use_type integer NOT NULL, PRIMARY KEY(id))');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE Skill');
      }
}
