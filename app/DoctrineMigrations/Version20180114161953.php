<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180114161953 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('INSERT OR REPLACE INTO CharacterEvolutionConfig
(id, server_id, reset_level, reset_points_dk, reset_points_dw, reset_points_fe, reset_points_mg, reset_points_dl, reset_points_sum, reset_points_rf, reset_price, reset_reward, reset_limit, reset_type, reset_payment_type, grand_reset_reset, grand_reset_price, grand_reset_points, grand_reset_reward)
VALUES(1, 1, 400, 500, 500, 500, 700, 700, 500, 500, 50, 20, 50, 2, 2, 50, 3000, 5000, 2000);');

        $this->addSql('INSERT OR REPLACE INTO CharacterEvolutionConfig
(id, server_id, reset_level, reset_points_dk, reset_points_dw, reset_points_fe, reset_points_mg, reset_points_dl, reset_points_sum, reset_points_rf, reset_price, reset_reward, reset_limit, reset_type, reset_payment_type, grand_reset_reset, grand_reset_price, grand_reset_points, grand_reset_reward)
VALUES(2, 2, 400, 500, 500, 500, 700, 700, 500, 500, 50, 20, 50, 2, 2, 50, 3000, 5000, 2000);');

        $this->addSql('INSERT OR REPLACE INTO CharacterEvolutionConfig
(id, server_id, reset_level, reset_points_dk, reset_points_dw, reset_points_fe, reset_points_mg, reset_points_dl, reset_points_sum, reset_points_rf, reset_price, reset_reward, reset_limit, reset_type, reset_payment_type, grand_reset_reset, grand_reset_price, grand_reset_points, grand_reset_reward)
VALUES(3, 3, 400, 500, 500, 500, 700, 700, 500, 500, 50, 20, 50, 2, 2, 50, 3000, 5000, 2000);');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

    }
}
