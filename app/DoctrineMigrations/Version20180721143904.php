<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180721143904 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_2DA179778C0C9F8A');
        $this->addSql('DROP INDEX UNIQ_2DA17977F85E0677');
        $this->addSql('DROP INDEX UNIQ_2DA17977D5499347');
        $this->addSql('DROP INDEX UNIQ_2DA17977E7927C74');
        $this->addSql('DROP INDEX UNIQ_2DA17977452C9EC5');
        $this->addSql('DROP INDEX IDX_2DA17977F92F3E70');
        $this->addSql('CREATE TEMPORARY TABLE __temp__User AS SELECT id, referred_by, country_id, username, display_name, email, password, logged_at, locale, created_at, updated_at, reset_password_token, reset_password_date, ip_address, type_id, forum_id FROM User');
        $this->addSql('DROP TABLE User');
        $this->addSql('CREATE TABLE User (id INTEGER NOT NULL, referred_by INTEGER DEFAULT NULL, country_id VARCHAR(3) DEFAULT NULL COLLATE BINARY, username VARCHAR(10) NOT NULL COLLATE BINARY, display_name VARCHAR(10) NOT NULL COLLATE BINARY, email VARCHAR(255) NOT NULL COLLATE BINARY, password VARCHAR(10) NOT NULL COLLATE BINARY, logged_at DATETIME NOT NULL, locale VARCHAR(6) NOT NULL COLLATE BINARY, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, reset_password_token VARCHAR(36) DEFAULT NULL COLLATE BINARY, reset_password_date DATETIME DEFAULT NULL, ip_address INTEGER NOT NULL, forum_id INTEGER DEFAULT NULL, type_id VARCHAR(255) NOT NULL --(DC2Type:enum_user_type)
        , email_state VARCHAR(255) NOT NULL --(DC2Type:email_state_type)
        , PRIMARY KEY(id), CONSTRAINT FK_2DA179778C0C9F8A FOREIGN KEY (referred_by) REFERENCES User (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_2DA17977F92F3E70 FOREIGN KEY (country_id) REFERENCES Country (code_iso3) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO User (id, referred_by, country_id, username, display_name, email, password, logged_at, locale, created_at, updated_at, reset_password_token, reset_password_date, ip_address, type_id, forum_id, email_state) SELECT id, referred_by, country_id, username, display_name, email, password, logged_at, locale, created_at, updated_at, reset_password_token, reset_password_date, ip_address, type_id, forum_id, \'A\' FROM __temp__User');
        $this->addSql('DROP TABLE __temp__User');
        $this->addSql('CREATE INDEX IDX_2DA179778C0C9F8A ON User (referred_by)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977F85E0677 ON User (username)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977D5499347 ON User (display_name)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977E7927C74 ON User (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977452C9EC5 ON User (reset_password_token)');
        $this->addSql('CREATE INDEX IDX_2DA17977F92F3E70 ON User (country_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_2DA17977F85E0677');
        $this->addSql('DROP INDEX UNIQ_2DA17977D5499347');
        $this->addSql('DROP INDEX UNIQ_2DA17977E7927C74');
        $this->addSql('DROP INDEX UNIQ_2DA17977452C9EC5');
        $this->addSql('DROP INDEX IDX_2DA179778C0C9F8A');
        $this->addSql('DROP INDEX IDX_2DA17977F92F3E70');
        $this->addSql('CREATE TEMPORARY TABLE __temp__User AS SELECT id, referred_by, country_id, username, display_name, email, password, logged_at, locale, reset_password_token, reset_password_date, ip_address, forum_id, created_at, updated_at, type_id FROM User');
        $this->addSql('DROP TABLE User');
        $this->addSql('CREATE TABLE User (id INTEGER NOT NULL, referred_by INTEGER DEFAULT NULL, country_id VARCHAR(3) DEFAULT NULL, username VARCHAR(10) NOT NULL, display_name VARCHAR(10) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(10) NOT NULL, logged_at DATETIME NOT NULL, locale VARCHAR(6) NOT NULL, reset_password_token VARCHAR(36) DEFAULT NULL, reset_password_date DATETIME DEFAULT NULL, ip_address INTEGER NOT NULL, forum_id INTEGER DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, type_id VARCHAR(255) NOT NULL COLLATE BINARY, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO User (id, referred_by, country_id, username, display_name, email, password, logged_at, locale, reset_password_token, reset_password_date, ip_address, forum_id, created_at, updated_at, type_id) SELECT id, referred_by, country_id, username, display_name, email, password, logged_at, locale, reset_password_token, reset_password_date, ip_address, forum_id, created_at, updated_at, type_id FROM __temp__User');
        $this->addSql('DROP TABLE __temp__User');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977F85E0677 ON User (username)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977D5499347 ON User (display_name)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977E7927C74 ON User (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977452C9EC5 ON User (reset_password_token)');
        $this->addSql('CREATE INDEX IDX_2DA179778C0C9F8A ON User (referred_by)');
        $this->addSql('CREATE INDEX IDX_2DA17977F92F3E70 ON User (country_id)');
    }
}
