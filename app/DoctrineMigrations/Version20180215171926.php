<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180215171926 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE ItemBuffEffect');



        $this->addSql('DROP INDEX IDX_38B39EF7FE54D947');
        $this->addSql('DROP INDEX IDX_38B39EF7890798E5');
        $this->addSql('DROP INDEX IDX_38B39EF76293FF9');
        $this->addSql('DROP INDEX idx_item_id');
        $this->addSql('CREATE TEMPORARY TABLE __temp__ItemInfo AS SELECT id, group_id, pvp_option_id, market_category_id, item_id, name, level, x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill, buff_effect_description FROM ItemInfo');
        $this->addSql('DROP TABLE ItemInfo');
        $this->addSql('CREATE TABLE ItemInfo (id INTEGER NOT NULL, group_id INTEGER DEFAULT NULL, pvp_option_id INTEGER DEFAULT NULL, market_category_id INTEGER DEFAULT NULL, item_id INTEGER NOT NULL, name VARCHAR(100) NOT NULL COLLATE BINARY, level INTEGER NOT NULL, x INTEGER NOT NULL, y INTEGER NOT NULL, drop_level INTEGER DEFAULT NULL, durability INTEGER DEFAULT NULL, attack_speed INTEGER DEFAULT NULL, defense INTEGER DEFAULT NULL, min_dmg INTEGER DEFAULT NULL, max_dmg INTEGER DEFAULT NULL, wizardry_dmg INTEGER DEFAULT NULL, wizardry_excellent_dmg INTEGER DEFAULT NULL, req_str INTEGER DEFAULT NULL, req_agi INTEGER DEFAULT NULL, req_ene INTEGER DEFAULT NULL, req_lvl INTEGER DEFAULT NULL, skill VARCHAR(100) DEFAULT NULL COLLATE BINARY, buff_effect VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id), CONSTRAINT FK_38B39EF7FE54D947 FOREIGN KEY (group_id) REFERENCES ItemGroup (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_38B39EF7890798E5 FOREIGN KEY (pvp_option_id) REFERENCES PvPOption (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_38B39EF76293FF9 FOREIGN KEY (market_category_id) REFERENCES MarketCategory (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO ItemInfo (id, group_id, pvp_option_id, market_category_id, item_id, name, level, x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill, buff_effect) SELECT id, group_id, pvp_option_id, market_category_id, item_id, name, level, x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill, buff_effect_description FROM __temp__ItemInfo');
        $this->addSql('DROP TABLE __temp__ItemInfo');
        $this->addSql('CREATE INDEX IDX_38B39EF7FE54D947 ON ItemInfo (group_id)');
        $this->addSql('CREATE INDEX IDX_38B39EF7890798E5 ON ItemInfo (pvp_option_id)');
        $this->addSql('CREATE INDEX IDX_38B39EF76293FF9 ON ItemInfo (market_category_id)');
        $this->addSql('CREATE INDEX idx_item_id ON ItemInfo (item_id)');
     }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE ItemBuffEffect (id INTEGER NOT NULL, item_info_id INTEGER DEFAULT NULL, description VARCHAR(255) NOT NULL COLLATE BINARY, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B13F13C056D6E7AC ON ItemBuffEffect (item_info_id)');

        $this->addSql('INSERT INTO ItemBuffEffect (id, item_info_id, description) VALUES(1, 783, \'EXP gain Increase 15%\'); ');
        $this->addSql('INSERT INTO ItemBuffEffect (id, item_info_id, description) VALUES(2, 673, \'Master EXP gain Increase 15%\'); ');
        $this->addSql('INSERT INTO ItemBuffEffect (id, item_info_id, description) VALUES(3, 761, \'EXP gain Increase 10%\nItem gain Increase 15%\'); ');
        $this->addSql('INSERT INTO ItemBuffEffect (id, item_info_id, description) VALUES(4, 714, \'Master EXP gain Increase 10%\nItem gain Increase 15%\'); ');
        $this->addSql('INSERT INTO ItemBuffEffect (id, item_info_id, description) VALUES(5, 651, \'EXP gain disabled\'); ');
        $this->addSql('INSERT INTO ItemBuffEffect (id, item_info_id, description) VALUES(6, 672, \'Mobility enabled\'); ');
        $this->addSql('INSERT INTO ItemBuffEffect (id, item_info_id, description) VALUES(8, 712, \'EXP gain Increase 5%\nAutomatic Life recovery at 10%\'); ');
        $this->addSql('INSERT INTO ItemBuffEffect (id, item_info_id, description) VALUES(9, 646, \'Item gain Increase at 10%\nAutomatic Mana recovery at 15%\'); ');
        $this->addSql('INSERT INTO ItemBuffEffect (id, item_info_id, description) VALUES(10, 742, \'Increases Attack Rate and Defense Rate\'); ');
        $this->addSql('INSERT INTO ItemBuffEffect (id, item_info_id, description) VALUES(11, 219, \'Attack Speed Increase +150\'); ');
        $this->addSql('INSERT INTO ItemBuffEffect (id, item_info_id, description) VALUES(12, 224, \'Defensibility Increase +200\'); ');
        $this->addSql('INSERT INTO ItemBuffEffect (id, item_info_id, description) VALUES(13, 155, \'Attack Power Increase +75\'); ');
        $this->addSql('INSERT INTO ItemBuffEffect (id, item_info_id, description) VALUES(14, 111, \'Wizardry Increase +75\'); ');
        $this->addSql('INSERT INTO ItemBuffEffect (id, item_info_id, description) VALUES(15, 169, \'HP Increase +500\'); ');
        $this->addSql('INSERT INTO ItemBuffEffect (id, item_info_id, description) VALUES(16, 137, \'MP Increase +750\'); ');
        $this->addSql('INSERT INTO ItemBuffEffect (id, item_info_id, description) VALUES(17, 225, \'Critical Damage Increase at 100%\'); ');
        $this->addSql('INSERT INTO ItemBuffEffect (id, item_info_id, description) VALUES(18, 100, \'Excellent Damage Increase at 100%\'); ');
        $this->addSql('INSERT INTO ItemBuffEffect (id, item_info_id, description) VALUES(19, 231, \'Strength Status +50\'); ');
        $this->addSql('INSERT INTO ItemBuffEffect (id, item_info_id, description) VALUES(20, 82, \'Agility Status +50\'); ');
        $this->addSql('INSERT INTO ItemBuffEffect (id, item_info_id, description) VALUES(21, 238, \'Vitality Status +50\'); ');
        $this->addSql('INSERT INTO ItemBuffEffect (id, item_info_id, description) VALUES(22, 133, \'Energy Status +50\'); ');
        $this->addSql('INSERT INTO ItemBuffEffect (id, item_info_id, description) VALUES(23, 253, \'Command Status +50\');');
        $this->addSql('INSERT INTO ItemBuffEffect (id, item_info_id, description) VALUES(24, 675, \'Increases your max AG.\'); ');
        $this->addSql('INSERT INTO ItemBuffEffect (id, item_info_id, description) VALUES(25, 716, \'Increases your max SD.\');');


        $this->addSql('DROP INDEX IDX_38B39EF7FE54D947');
        $this->addSql('DROP INDEX IDX_38B39EF7890798E5');
        $this->addSql('DROP INDEX IDX_38B39EF76293FF9');
        $this->addSql('DROP INDEX idx_item_id');
        $this->addSql('CREATE TEMPORARY TABLE __temp__ItemInfo AS SELECT id, group_id, pvp_option_id, market_category_id, item_id, name, level, x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill, buff_effect FROM ItemInfo');
        $this->addSql('DROP TABLE ItemInfo');
        $this->addSql('CREATE TABLE ItemInfo (id INTEGER NOT NULL, group_id INTEGER DEFAULT NULL, pvp_option_id INTEGER DEFAULT NULL, market_category_id INTEGER DEFAULT NULL, item_id INTEGER NOT NULL, name VARCHAR(100) NOT NULL, level INTEGER NOT NULL, x INTEGER NOT NULL, y INTEGER NOT NULL, drop_level INTEGER DEFAULT NULL, durability INTEGER DEFAULT NULL, attack_speed INTEGER DEFAULT NULL, defense INTEGER DEFAULT NULL, min_dmg INTEGER DEFAULT NULL, max_dmg INTEGER DEFAULT NULL, wizardry_dmg INTEGER DEFAULT NULL, wizardry_excellent_dmg INTEGER DEFAULT NULL, req_str INTEGER DEFAULT NULL, req_agi INTEGER DEFAULT NULL, req_ene INTEGER DEFAULT NULL, req_lvl INTEGER DEFAULT NULL, skill VARCHAR(100) DEFAULT NULL, buff_effect_description VARCHAR(255) DEFAULT NULL COLLATE BINARY, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO ItemInfo (id, group_id, pvp_option_id, market_category_id, item_id, name, level, x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill, buff_effect_description) SELECT id, group_id, pvp_option_id, market_category_id, item_id, name, level, x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill, buff_effect FROM __temp__ItemInfo');
        $this->addSql('DROP TABLE __temp__ItemInfo');
        $this->addSql('CREATE INDEX IDX_38B39EF7FE54D947 ON ItemInfo (group_id)');
        $this->addSql('CREATE INDEX IDX_38B39EF7890798E5 ON ItemInfo (pvp_option_id)');
        $this->addSql('CREATE INDEX IDX_38B39EF76293FF9 ON ItemInfo (market_category_id)');
        $this->addSql('CREATE INDEX idx_item_id ON ItemInfo (item_id)');
        $this->addSql('DROP INDEX IDX_779FD2851AD5CDBF');
        $this->addSql('DROP INDEX IDX_779FD2858D9F6D38');
     }
}
