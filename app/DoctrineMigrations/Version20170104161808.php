<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170104161808 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE CartPersistentDiscount (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, type VARCHAR(255) NOT NULL, discount INTEGER NOT NULL, spent_from INTEGER NOT NULL, spent_to INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_49570D791844E6B7 ON CartPersistentDiscount (server_id)');
        $this->addSql('CREATE UNIQUE INDEX WebShopPersistentDiscount_range_UNIQUE ON CartPersistentDiscount (server_id, spent_from, spent_to)');


        $this->addSql('DROP INDEX IDX_758ED6D9A76ED395');
        $this->addSql('DROP INDEX IDX_758ED6D91844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__UserServerAccount AS SELECT id, user_id, server_id, account_id, selected_at FROM UserServerAccount');
        $this->addSql('DROP TABLE UserServerAccount');
        $this->addSql('CREATE TABLE UserServerAccount (id INTEGER NOT NULL, user_id INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, account_id INTEGER NOT NULL, selected_at DATETIME NOT NULL, spent_money DOUBLE PRECISION NOT NULL, spent_credits INTEGER NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_758ED6D9A76ED395 FOREIGN KEY (user_id) REFERENCES User (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_758ED6D91844E6B7 FOREIGN KEY (server_id) REFERENCES Server (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO UserServerAccount (id, user_id, server_id, account_id, selected_at, spent_money, spent_credits) SELECT id, user_id, server_id, account_id, selected_at, 0, 0 FROM __temp__UserServerAccount');
        $this->addSql('DROP TABLE __temp__UserServerAccount');
        $this->addSql('CREATE INDEX IDX_758ED6D9A76ED395 ON UserServerAccount (user_id)');
        $this->addSql('CREATE INDEX IDX_758ED6D91844E6B7 ON UserServerAccount (server_id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE CartPersistentDiscount');

        $this->addSql('DROP INDEX IDX_758ED6D9A76ED395');
        $this->addSql('DROP INDEX IDX_758ED6D91844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__UserServerAccount AS SELECT id, user_id, server_id, account_id, selected_at FROM UserServerAccount');
        $this->addSql('DROP TABLE UserServerAccount');
        $this->addSql('CREATE TABLE UserServerAccount (id INTEGER NOT NULL, user_id INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, account_id INTEGER NOT NULL, selected_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO UserServerAccount (id, user_id, server_id, account_id, selected_at) SELECT id, user_id, server_id, account_id, selected_at FROM __temp__UserServerAccount');
        $this->addSql('DROP TABLE __temp__UserServerAccount');
        $this->addSql('CREATE INDEX IDX_758ED6D9A76ED395 ON UserServerAccount (user_id)');
        $this->addSql('CREATE INDEX IDX_758ED6D91844E6B7 ON UserServerAccount (server_id)');
    }
}
