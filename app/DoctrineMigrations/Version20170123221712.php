<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170123221712 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE TopVoter (id INTEGER NOT NULL, voter_user_server_account_id INTEGER DEFAULT NULL, place SMALLINT NOT NULL, reward SMALLINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E3FD81D840C2021F ON TopVoter (voter_user_server_account_id)');
        $this->addSql('CREATE TABLE VoteRewardConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, reward_regular_vote SMALLINT NOT NULL, reward_sms_vote SMALLINT NOT NULL, reward_month_top1 SMALLINT NOT NULL, reward_month_top2 SMALLINT NOT NULL, reward_month_top3 SMALLINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DCFCB9A51844E6B7 ON VoteRewardConfig (server_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE TopVoter');
        $this->addSql('DROP TABLE VoteRewardConfig');
    }
}
