<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171214225245 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('UPDATE ItemInfo SET name=\'Box of Red Ribbon\' WHERE id=935;');
        $this->addSql('UPDATE ItemInfo SET name=\'Box of Green Ribbon\' WHERE id=899;');
        $this->addSql('UPDATE ItemInfo SET name=\'Box of Blue Ribbon\' WHERE id=900;');

        $this->addSql('INSERT INTO ItemInfo
    (group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(13, NULL, 26, 73, \'Sphere (Tetra)\', 0, 1, 1, 132, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL)');
        $this->addSql('INSERT INTO ItemInfo
    (group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(13, NULL, 26, 74, \'Sphere (Penta)\', 0, 1, 1, 152, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL)');


        $this->addSql('INSERT INTO ItemInfo
(group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(13, NULL, 26, 118, \'Seed Sphere (Fire)\', 0, 1, 1, 4, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);
');
        $this->addSql('INSERT INTO ItemInfo
(group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(13, NULL, 26, 119, \'Seed Sphere (Water)\', 0, 1, 1, 4, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);
');
        $this->addSql('INSERT INTO ItemInfo
(group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(13, NULL, 26, 120, \'Seed Sphere (Ice)\', 0, 1, 1, 4, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);
');
        $this->addSql('INSERT INTO ItemInfo
(group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(13, NULL, 26, 121, \'Seed Sphere (Wind)\', 0, 1, 1, 4, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);
');
        $this->addSql('INSERT INTO ItemInfo
(group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(13, NULL, 26, 122, \'Seed Sphere (Lightning)\', 0, 1, 1, 4, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);
');
        $this->addSql('INSERT INTO ItemInfo
(group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(13, NULL, 26, 123, \'Seed Sphere (Earth)\', 0, 1, 1, 4, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);
');
        $this->addSql('INSERT INTO ItemInfo
(group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(13, NULL, 26, 124, \'Seed Sphere (Fire)\', 0, 1, 1, 5, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);
');
        $this->addSql('INSERT INTO ItemInfo
(group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(13, NULL, 26, 125, \'Seed Sphere (Water)\', 0, 1, 1, 5, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);
');
        $this->addSql('INSERT INTO ItemInfo
(group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(13, NULL, 26, 126, \'Seed Sphere (Ice)\', 0, 1, 1, 5, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);
');
        $this->addSql('INSERT INTO ItemInfo
(group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(13, NULL, 26, 127, \'Seed Sphere (Wind)\', 0, 1, 1, 5, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);
');
        $this->addSql('INSERT INTO ItemInfo
(group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(13, NULL, 26, 128, \'Seed Sphere (Lightning)\', 0, 1, 1, 5, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);
');
        $this->addSql('INSERT INTO ItemInfo
(group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(13, NULL, 26, 129, \'Seed Sphere (Earth)\', 0, 1, 1, 5, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);
');

        $this->addSql('DELETE FROM ItemInfo WHERE id=757;');

        $this->addSql('INSERT INTO ItemInfo
(group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(14, NULL, 26, 11, \'Order (Guardian/Life Stone)\', 0, 1, 1, 0, 255, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);
');

        $this->addSql('INSERT INTO ItemInfo
(group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(14, NULL, 26, 71, \'Sword/Mace/Spear\', 0, 2, 3, NULL, 255, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
');
        $this->addSql('INSERT INTO ItemInfo
(group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(14, NULL, 26, 73, \'Bow/Crossbow\', 0, 2, 3, NULL, 255, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
');

        $this->addSql('INSERT INTO ItemInfo
(group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill)
VALUES(14, NULL, 26, 99, \'Character Slot Unlock Key\', 0, 1, 1, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
');

        $this->addSql('UPDATE ItemInfo SET name=\'Access Ticket to Varka Map 7\' WHERE id=659;');

        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 171, \'Necklace of Agony\', 0, 1, 1, NULL, 60, 122);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 172, \'Solid Symbol\', 0, 1, 1, NULL, 60, 110);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 173, \'Ring of Ultimatum\', 0, 1, 1, NULL, 60, 98);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 174, \'Ring of Block\', 0, 1, 1, NULL, 60, 86);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 175, \'Protector Protection Ring\', 0, 1, 1, NULL, 60, 74);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 176, \'Protection Ring\', 0, 1, 1, NULL, 60, 62);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 177, \'Talisman of Ascension I\', 0, 1, 1, NULL, 255, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 178, \'Talisman of Ascension II\', 0, 1, 1, NULL, 255, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 179, \'Talisman of Ascension III\', 0, 1, 1, NULL, 255, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 180, \'Seal of Ascension II\', 0, 1, 1, NULL, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 181, \'Master Seal of Ascension II\', 0, 1, 1, NULL, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 210, \'WereRabbit Egg\', 0, 1, 1, NULL, 255, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 211, \'Evolution Stone\', 0, 1, 1, NULL, 255, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 212, \'Luki\', 0, 1, 1, NULL, 255, 1);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 213, \'Luki (Evolved)\', 0, 1, 1, NULL, 255, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 214, \'Tony\', 0, 1, 1, NULL, 255, 1);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 215, \'Tony (Evolved)\', 0, 1, 1, NULL, 255, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 216, \'Nymph\', 0, 1, 1, NULL, 255, 1);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 217, \'Nymph (Evolved)\', 0, 1, 1, NULL, 255, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 218, \'Safi\', 0, 1, 1, NULL, 255, 1);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 219, \'Safi (Evolved)\', 0, 1, 1, NULL, 255, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 220, \'Lucky Bag Egg\', 0, 1, 1, NULL, 255, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 221, \'Fire Flame Ghost Egg\', 0, 1, 1, NULL, 255, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 222, \'Golden Goblin Egg\', 0, 1, 1, NULL, 255, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 223, \'William\', 0, 1, 1, NULL, 255, 1);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 224, \'William (Evolved)\', 0, 1, 1, NULL, 255, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 225, \'Paul\', 0, 1, 1, NULL, 255, 1);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 226, \'Paul (Evolved)\', 0, 1, 1, NULL, 255, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 227, \'Chiron\', 0, 1, 1, NULL, 255, 1);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 228, \'Chiron (Evolved)\', 0, 1, 1, NULL, 255, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 231, \'Tibetton\', 0, 1, 1, NULL, 255, 1);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 232, \'Tibetton (Evolved)\', 0, 1, 1, NULL, 255, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 233, \'Witch\', 0, 1, 1, NULL, 255, 1);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 234, \'Witch (Evolved)\', 0, 1, 1, NULL, 255, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 235, \'Skull\', 0, 1, 1, NULL, 255, 1);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 236, \'Skull (Evolved)\', 0, 1, 1, NULL, 255, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 237, \'Pumpy\', 0, 1, 1, NULL, 255, 1);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 238, \'Pumpy (Evolved)\', 0, 1, 1, NULL, 255, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 241, \'Savath\', 0, 1, 1, NULL, 255, 1);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 242, \'Savath (Evolved)\', 0, 1, 1, NULL, 255, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 245, \'Torby\', 0, 1, 1, NULL, 255, 1);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 246, \'Torby (Evolved)\', 0, 1, 1, NULL, 255, 0);');

        $this->addSql('DELETE FROM ItemInfo WHERE id=244;');

        $this->addSql('UPDATE ItemInfo SET name=\'Pink Chocolate Box\' WHERE id=200;');
        $this->addSql('UPDATE ItemInfo SET name=\'Red Chocolate Box\' WHERE id=175;');
        $this->addSql('UPDATE ItemInfo SET name=\'Blue Chocolate Box\' WHERE id=203;');

        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 102, \'Gaion\'\'s Order\', 0, 1, 1, NULL, 0, 0);');

        $this->addSql('UPDATE ItemInfo SET name=\'30-Day Pass - Points\' WHERE id=101;');
        $this->addSql('UPDATE ItemInfo SET name=\'90-Day Pass - Points\' WHERE id=188;');
        $this->addSql('UPDATE ItemInfo SET name=\'Lv. 1 Letter of Request\' WHERE id=182;');
        $this->addSql('UPDATE ItemInfo SET name=\'Lv. 1 Completion Certificate\' WHERE id=187;');

        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 164, \'Ancient Statue\', 0, 1, 2, NULL, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 165, \'Magic Cloth\', 0, 1, 1, NULL, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 166, \'Space Cloth\', 0, 1, 1, NULL, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 167, \'Phase Cloth\', 0, 1, 1, NULL, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 168, \'Dimension Cloth\', 0, 1, 1, NULL, 0, 0);');

        $this->addSql('UPDATE ItemInfo SET name=\'Lv. 2 Letter of Request\' WHERE id=227;');
        $this->addSql('UPDATE ItemInfo SET name=\'Lv. 2 Completion Certificate\' WHERE id=201;');

        $this->addSql('DELETE FROM ItemInfo WHERE id=1128;');
        $this->addSql('DELETE FROM ItemInfo WHERE id=1129;');
        $this->addSql('DELETE FROM ItemInfo WHERE id=1130;');
        $this->addSql('DELETE FROM ItemInfo WHERE id=1131;');
        $this->addSql('DELETE FROM ItemInfo WHERE id=1132;');
        $this->addSql('DELETE FROM ItemInfo WHERE id=1133;');
        $this->addSql('DELETE FROM ItemInfo WHERE id=1134;');
        $this->addSql('DELETE FROM ItemInfo WHERE id=1135;');
        $this->addSql('DELETE FROM ItemInfo WHERE id=1136;');
        $this->addSql('DELETE FROM ItemInfo WHERE id=1137;');
        $this->addSql('DELETE FROM ItemInfo WHERE id=1138;');

        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 180, \'Ice Walker\'\'s Mane\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 181, \'Mammoth\'\'s Bundle of Fur\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 182, \'Giant\'\'s Icicle\', 0, 1, 2, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 183, \'Coolutin\'\'s Poison\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 184, \'Iron Knight\'\'s Heart\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 185, \'Dark Mammoth\'\'s Horn\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 186, \'Dark Giant\'\'s Axe\', 0, 1, 2, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 187, \'Dark Coolutin\'\'s Blood\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 188, \'Dark Iron Knight\'\'s Sword\', 0, 1, 4, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 189, \'Elemental Talisman of Luck\', 0, 1, 1, 10, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 190, \'Elemental Talisman of Chaos\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 191, \'Sonic Combination Note\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 192, \'Cyclone Combination Note\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 193, \'Blast Combination Note\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 194, \'Magma Combination Note\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 195, \'Horn Combination Note\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 196, \'Angelic Combination Note\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 197, \'Devil Cross Combination Note\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 198, \'Miracle Combination Note\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 199, \'Spite Combination Note\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 200, \'Asura Combination Note\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 201, \'Thunder Combination Note\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 202, \'Soul Combination Note\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 209, \'Tradeable Seal\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 210, \'Summon Scroll (Lv1)\', 0, 1, 1, 32, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 211, \'Summon Scroll (Lv2)\', 0, 1, 1, 32, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 212, \'Summon Scroll (Lv3)\', 0, 1, 1, 32, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 213, \'Summon Scroll (Lv4)\', 0, 1, 1, 32, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 215, \'Card Piece\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 216, \'Card Deck\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 217, \'Titan\'\'s Anger\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 218, \'Tantalose\'\'s Punishment\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 219, \'Erohim\'\'s Nightmare\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 220, \'Hell Maine\'\'s Insanity\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 221, \'Kundun\'\'s Greed\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 222, \'Unique Accessory Box\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 223, \'Cursed Castle Water\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 224, \'Blessing of Light\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 225, \'Scroll Master of Defense\', 0, 2, 2, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 226, \'Scroll Master of Wizardry\', 0, 2, 2, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 227, \'Scroll Master of Health\', 0, 2, 2, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 228, \'Scroll Master of Mana\', 0, 2, 2, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 229, \'Scroll Master of Wrath\', 0, 2, 2, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 230, \'Scroll Master of Recovery\', 0, 2, 2, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 231, \'Scroll Master of Battle\', 0, 2, 2, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 232, \'Scroll Master of Strenghener\', 0, 2, 2, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 233, \'Unique Weapon Box\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 234, \'Book of Monster Invocation\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 235, \'Scroll Master of Quickness\', 0, 2, 2, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 236, \'Reward Pot\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 237, \'Reward Pot\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 238, \'Reward Pot\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 239, \'Reward Pot\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 240, \'Reward Pot\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 241, \'Rare Reward Pot\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 242, \'Golden Apple\', 0, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 26, 243, \'Golden Apple(2)\', 0, 1, 1, 0, 0, 0);');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

    }
}
