<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171125190413 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        // Swords
        $this->addSql('UPDATE ItemInfo SET `min_dmg`=113, `max_dmg`=125 WHERE `id`=1010;');
        $this->addSql('UPDATE ItemInfo SET `min_dmg`=123, `max_dmg`=130 WHERE `id`=1014;');
        $this->addSql('UPDATE ItemInfo SET `min_dmg`=150, `max_dmg`=170 WHERE `id`=1008;');
        $this->addSql('UPDATE ItemInfo SET `min_dmg`=200, `max_dmg`=220 WHERE `id`=1023;');
        $this->addSql('UPDATE ItemInfo SET `min_dmg`=180, `max_dmg`=200 WHERE `id`=1018;');
        $this->addSql('UPDATE ItemInfo SET `wizardry_dmg`=150 WHERE `id`=1042;');

        // Maces
        $this->addSql('UPDATE ItemInfo SET `min_dmg`=145, `max_dmg`=185, `wizardry_dmg`=100 WHERE `id`=808;');
        $this->addSql('UPDATE ItemInfo SET `min_dmg`=103, `max_dmg`=120, `wizardry_dmg`=75, `wizardry_excellent_dmg`=75 WHERE `id`=790;');

        // Bows
        $this->addSql('UPDATE ItemInfo SET `min_dmg`=130, `max_dmg`=155 WHERE `id`=824;');
        $this->addSql('UPDATE ItemInfo SET `min_dmg`=197, `max_dmg`=219 WHERE `id`=819;');

        // Staffs
        $this->addSql('UPDATE ItemInfo SET `min_dmg`=75, `max_dmg`=82, `wizardry_dmg`=155 WHERE `id`=975;');
        $this->addSql('UPDATE ItemInfo SET `min_dmg`=75, `max_dmg`=83, `wizardry_dmg`=140 WHERE `id`=970;');
        $this->addSql('UPDATE ItemInfo SET `min_dmg`=66, `max_dmg`=75, `wizardry_dmg`=110, wizardry_excellent_dmg=NULL WHERE `id`=995;');

        // Helms
        $this->addSql('UPDATE ItemInfo SET `defense`=92 WHERE `id`=385;');
        $this->addSql('UPDATE ItemInfo SET `defense`=79 WHERE `id`=361;');
        $this->addSql('UPDATE ItemInfo SET `defense`=70 WHERE `id`=358;');
        $this->addSql('UPDATE ItemInfo SET `defense`=80 WHERE `id`=423;');
        $this->addSql('UPDATE ItemInfo SET `defense`=54 WHERE `id`=388;');
        $this->addSql('UPDATE ItemInfo SET `defense`=60 WHERE `id`=389;');

        // Armors
        $this->addSql('UPDATE ItemInfo SET `defense`=114 WHERE `id`=471;');
        $this->addSql('UPDATE ItemInfo SET `defense`=101 WHERE `id`=476;');
        $this->addSql('UPDATE ItemInfo SET `defense`=112 WHERE `id`=457;');
        $this->addSql('UPDATE ItemInfo SET `defense`=99 WHERE `id`=492;');
        $this->addSql('UPDATE ItemInfo SET `defense`=94 WHERE `id`=502;');
        $this->addSql('UPDATE ItemInfo SET `defense`=81 WHERE `id`=438;');
        $this->addSql('UPDATE ItemInfo SET `defense`=77 WHERE `id`=462;');
        $this->addSql('UPDATE ItemInfo SET `defense`=71 WHERE `id`=440;');

        // Pants
        $this->addSql('UPDATE ItemInfo SET `defense`=104 WHERE `id`=278;');
        $this->addSql('UPDATE ItemInfo SET `defense`=91 WHERE `id`=274;');
        $this->addSql('UPDATE ItemInfo SET `defense`=100 WHERE `id`=300;');
        $this->addSql('UPDATE ItemInfo SET `defense`=87 WHERE `id`=332;');
        $this->addSql('UPDATE ItemInfo SET `defense`=87 WHERE `id`=287;');
        $this->addSql('UPDATE ItemInfo SET `defense`=74 WHERE `id`=301;');
        $this->addSql('UPDATE ItemInfo SET `defense`=65 WHERE `id`=307;');
        $this->addSql('UPDATE ItemInfo SET `defense`=69 WHERE `id`=319;');

        // Gloves
        $this->addSql('UPDATE ItemInfo SET `defense`=84 WHERE `id`=75;');
        $this->addSql('UPDATE ItemInfo SET `defense`=72 WHERE `id`=58;');
        $this->addSql('UPDATE ItemInfo SET `defense`=75 WHERE `id`=44;');
        $this->addSql('UPDATE ItemInfo SET `defense`=65 WHERE `id`=66;');
        $this->addSql('UPDATE ItemInfo SET `defense`=75 WHERE `id`=24;');
        $this->addSql('UPDATE ItemInfo SET `defense`=62 WHERE `id`=22;');
        $this->addSql('UPDATE ItemInfo SET `defense`=51 WHERE `id`=61;');
        $this->addSql('UPDATE ItemInfo SET `defense`=58 WHERE `id`=49;');

        // Boots
        $this->addSql('UPDATE ItemInfo SET `defense`=89 WHERE `id`=577;');
        $this->addSql('UPDATE ItemInfo SET `defense`=76 WHERE `id`=574;');
        $this->addSql('UPDATE ItemInfo SET `defense`=83 WHERE `id`=582;');
        $this->addSql('UPDATE ItemInfo SET `defense`=72 WHERE `id`=552;');
        $this->addSql('UPDATE ItemInfo SET `defense`=79 WHERE `id`=575;');
        $this->addSql('UPDATE ItemInfo SET `defense`=66 WHERE `id`=583;');
        $this->addSql('UPDATE ItemInfo SET `defense`=55 WHERE `id`=594;');
        $this->addSql('UPDATE ItemInfo SET `defense`=63 WHERE `id`=631;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

    }
}
