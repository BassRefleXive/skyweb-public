<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180426120715 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $locations = json_decode(file_get_contents('app/data/locations.json'), true);

        $stmt = $this->connection->prepare('INSERT INTO `Country` (`code_iso3`, `code_iso2`, `name`, `name_official`, `latitude`, `longitude`, `zoom`, `state`) VALUES (:code_iso3, :code_iso2, :name, :name_official, :latitude, :longitude, :zoom, :state)');

        foreach ($locations as $location) {
            $stmt->bindParam('code_iso3', $location['code3l']);
            $stmt->bindParam('code_iso2', $location['code2l']);
            $stmt->bindParam('name', $location['name']);
            $stmt->bindParam('name_official', $location['name_official']);
            $stmt->bindParam('latitude', $location['latitude']);
            $stmt->bindParam('longitude', $location['longitude']);
            $stmt->bindParam('zoom', $location['zoom']);
            $stmt->bindParam('state', $location['enabled']);

            $stmt->execute();
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DELETE FROM Country');
      }
}
