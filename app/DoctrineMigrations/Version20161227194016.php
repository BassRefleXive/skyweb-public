<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161227194016 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_2DA17977AA08CB10');
        $this->addSql('DROP INDEX UNIQ_2DA17977E7927C74');
        $this->addSql('CREATE TEMPORARY TABLE __temp__User AS SELECT id, login, email, password, logged_at, created_at, updated_at, type_id FROM User');
        $this->addSql('DROP TABLE User');
        $this->addSql('CREATE TABLE User (id INTEGER NOT NULL, email VARCHAR(255) NOT NULL COLLATE BINARY, password VARCHAR(10) NOT NULL COLLATE BINARY, logged_at DATETIME NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, username VARCHAR(10) NOT NULL, type_id VARCHAR(255) NOT NULL, display_name VARCHAR(10) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO User (id, username, email, password, logged_at, created_at, updated_at, type_id, display_name) SELECT id, login, email, password, logged_at, created_at, updated_at, type_id, login FROM __temp__User');
        $this->addSql('DROP TABLE __temp__User');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977E7927C74 ON User (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977F85E0677 ON User (username)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977D5499347 ON User (display_name)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_2DA17977F85E0677');
        $this->addSql('DROP INDEX UNIQ_2DA17977D5499347');
        $this->addSql('DROP INDEX UNIQ_2DA17977E7927C74');
        $this->addSql('CREATE TEMPORARY TABLE __temp__User AS SELECT id, email, password, logged_at, created_at, updated_at, type_id FROM User');
        $this->addSql('DROP TABLE User');
        $this->addSql('CREATE TABLE User (id INTEGER NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(10) NOT NULL, logged_at DATETIME NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, type_id VARCHAR(255) NOT NULL COLLATE BINARY, login VARCHAR(10) NOT NULL COLLATE BINARY, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO User (id, email, password, logged_at, created_at, updated_at, type_id) SELECT id, email, password, logged_at, created_at, updated_at, type_id FROM __temp__User');
        $this->addSql('DROP TABLE __temp__User');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977E7927C74 ON User (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977AA08CB10 ON User (login)');
    }
}
