<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170119161521 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE ReferralConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, required_reset SMALLINT NOT NULL, required_grand_reset SMALLINT NOT NULL, required_online_time SMALLINT NOT NULL, required_votes SMALLINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FCB45F831844E6B7 ON ReferralConfig (server_id)');

        $this->addSql('DROP INDEX UNIQ_2DA17977E7927C74');
        $this->addSql('DROP INDEX UNIQ_2DA17977D5499347');
        $this->addSql('DROP INDEX UNIQ_2DA17977F85E0677');
        $this->addSql('CREATE TEMPORARY TABLE __temp__User AS SELECT id, username, display_name, email, password, logged_at, locale, created_at, updated_at, type_id FROM User');
        $this->addSql('DROP TABLE User');
        $this->addSql('CREATE TABLE User (id INTEGER NOT NULL, referred_by INTEGER DEFAULT NULL, username VARCHAR(10) NOT NULL COLLATE BINARY, display_name VARCHAR(10) NOT NULL COLLATE BINARY, email VARCHAR(255) NOT NULL COLLATE BINARY, password VARCHAR(10) NOT NULL COLLATE BINARY, logged_at DATETIME NOT NULL, locale VARCHAR(6) NOT NULL COLLATE BINARY, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, type_id VARCHAR(255) NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_2DA179778C0C9F8A FOREIGN KEY (referred_by) REFERENCES User (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO User (id, username, display_name, email, password, logged_at, locale, created_at, updated_at, type_id) SELECT id, username, display_name, email, password, logged_at, locale, created_at, updated_at, type_id FROM __temp__User');
        $this->addSql('DROP TABLE __temp__User');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977E7927C74 ON User (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977D5499347 ON User (display_name)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977F85E0677 ON User (username)');
        $this->addSql('CREATE INDEX IDX_2DA179778C0C9F8A ON User (referred_by)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE ReferralConfig');

        $this->addSql('DROP INDEX UNIQ_2DA17977F85E0677');
        $this->addSql('DROP INDEX UNIQ_2DA17977D5499347');
        $this->addSql('DROP INDEX UNIQ_2DA17977E7927C74');
        $this->addSql('DROP INDEX IDX_2DA179778C0C9F8A');
        $this->addSql('CREATE TEMPORARY TABLE __temp__User AS SELECT id, username, display_name, email, password, logged_at, locale, created_at, updated_at, type_id FROM User');
        $this->addSql('DROP TABLE User');
        $this->addSql('CREATE TABLE User (id INTEGER NOT NULL, username VARCHAR(10) NOT NULL, display_name VARCHAR(10) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(10) NOT NULL, logged_at DATETIME NOT NULL, locale VARCHAR(6) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, type_id VARCHAR(255) NOT NULL COLLATE BINARY, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO User (id, username, display_name, email, password, logged_at, locale, created_at, updated_at, type_id) SELECT id, username, display_name, email, password, logged_at, locale, created_at, updated_at, type_id FROM __temp__User');
        $this->addSql('DROP TABLE __temp__User');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977F85E0677 ON User (username)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977D5499347 ON User (display_name)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977E7927C74 ON User (email)');
    }
}
