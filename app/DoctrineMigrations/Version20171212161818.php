<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171212161818 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_6E1472A956D6E7AC');
        $this->addSql('DROP INDEX IDX_6E1472A9B1BB7436');
        $this->addSql('DROP INDEX IDX_6E1472A91844E6B7');
        $this->addSql('DROP INDEX MarketItem_index_item_specs');
        $this->addSql('CREATE TEMPORARY TABLE __temp__MarketItem AS SELECT id, item_info_id, user_server_account_id, server_id, hex, title, level, option, skill, luck, excellent, ancient, harmony, pvp, price, created_at, updated_at, price_type FROM MarketItem');
        $this->addSql('DROP TABLE MarketItem');
        $this->addSql('CREATE TABLE MarketItem (id INTEGER NOT NULL, bought_by INTEGER DEFAULT NULL, item_info_id INTEGER DEFAULT NULL, user_server_account_id INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, hex VARCHAR(100) NOT NULL COLLATE BINARY, level SMALLINT NOT NULL, option SMALLINT NOT NULL, skill BOOLEAN NOT NULL, luck BOOLEAN NOT NULL, excellent BOOLEAN NOT NULL, ancient BOOLEAN NOT NULL, harmony BOOLEAN NOT NULL, pvp BOOLEAN NOT NULL, price INTEGER NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, title VARCHAR(255) NOT NULL, price_type VARCHAR(255) NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_6E1472A9621B6D82 FOREIGN KEY (bought_by) REFERENCES UserServerAccount (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_6E1472A956D6E7AC FOREIGN KEY (item_info_id) REFERENCES ItemInfo (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_6E1472A9B1BB7436 FOREIGN KEY (user_server_account_id) REFERENCES UserServerAccount (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_6E1472A91844E6B7 FOREIGN KEY (server_id) REFERENCES Server (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO MarketItem (id, item_info_id, user_server_account_id, server_id, hex, title, level, option, skill, luck, excellent, ancient, harmony, pvp, price, created_at, updated_at, price_type) SELECT id, item_info_id, user_server_account_id, server_id, hex, title, level, option, skill, luck, excellent, ancient, harmony, pvp, price, created_at, updated_at, price_type FROM __temp__MarketItem');
        $this->addSql('DROP TABLE __temp__MarketItem');
        $this->addSql('CREATE INDEX IDX_6E1472A956D6E7AC ON MarketItem (item_info_id)');
        $this->addSql('CREATE INDEX IDX_6E1472A9B1BB7436 ON MarketItem (user_server_account_id)');
        $this->addSql('CREATE INDEX IDX_6E1472A91844E6B7 ON MarketItem (server_id)');
        $this->addSql('CREATE INDEX MarketItem_index_item_specs ON MarketItem (level, option, skill, luck, excellent, ancient, harmony, pvp, price, price_type)');
        $this->addSql('CREATE INDEX IDX_6E1472A9621B6D82 ON MarketItem (bought_by)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_6E1472A9621B6D82');
        $this->addSql('DROP INDEX IDX_6E1472A956D6E7AC');
        $this->addSql('DROP INDEX IDX_6E1472A9B1BB7436');
        $this->addSql('DROP INDEX IDX_6E1472A91844E6B7');
        $this->addSql('DROP INDEX MarketItem_index_item_specs');
        $this->addSql('CREATE TEMPORARY TABLE __temp__MarketItem AS SELECT id, item_info_id, user_server_account_id, server_id, hex, level, title, option, skill, luck, excellent, ancient, harmony, pvp, price, price_type, created_at, updated_at FROM MarketItem');
        $this->addSql('DROP TABLE MarketItem');
        $this->addSql('CREATE TABLE MarketItem (id INTEGER NOT NULL, item_info_id INTEGER DEFAULT NULL, user_server_account_id INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, hex VARCHAR(100) NOT NULL, level SMALLINT NOT NULL, option SMALLINT NOT NULL, skill BOOLEAN NOT NULL, luck BOOLEAN NOT NULL, excellent BOOLEAN NOT NULL, ancient BOOLEAN NOT NULL, harmony BOOLEAN NOT NULL, pvp BOOLEAN NOT NULL, price INTEGER NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, title VARCHAR(255) DEFAULT NULL COLLATE BINARY, price_type VARCHAR(255) NOT NULL COLLATE BINARY, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO MarketItem (id, item_info_id, user_server_account_id, server_id, hex, level, title, option, skill, luck, excellent, ancient, harmony, pvp, price, price_type, created_at, updated_at) SELECT id, item_info_id, user_server_account_id, server_id, hex, level, title, option, skill, luck, excellent, ancient, harmony, pvp, price, price_type, created_at, updated_at FROM __temp__MarketItem');
        $this->addSql('DROP TABLE __temp__MarketItem');
        $this->addSql('CREATE INDEX IDX_6E1472A956D6E7AC ON MarketItem (item_info_id)');
        $this->addSql('CREATE INDEX IDX_6E1472A9B1BB7436 ON MarketItem (user_server_account_id)');
        $this->addSql('CREATE INDEX IDX_6E1472A91844E6B7 ON MarketItem (server_id)');
        $this->addSql('CREATE INDEX MarketItem_index_item_specs ON MarketItem (level, option, skill, luck, excellent, ancient, harmony, pvp, price, price_type)');
    }
}
