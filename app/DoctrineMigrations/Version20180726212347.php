<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180726212347 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'razvan@yahoo.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'vasya.ivanov@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'arts@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'didzimednis007@inbox.lv\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'lolipop@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'huytq14111@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'little_skija97@yahoo.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'lisas@epastas.lt\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'huynhtiensi221kthd@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'arka69@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'gidraponika18@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'akopotil@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'1gegma1@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'asdhgsdgsdf@yahoo.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'vpontelev@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'bcbiz2@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'pula@yahoo.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'bcbiz1@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'vasya.pontelev@yandex.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'pelonchojua25@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'dfgh@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'lolil@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'misa@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'ajsjadsajdsa@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'rindan20@yahoo.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'itsmearka69@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'generwow@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'asldkjasaas@hotmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'alebergami@yahoo.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'pocem@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'ggfantasia1@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'nauka.home2016@ayndex.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'boldanrobi@yahoo.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'nauka.home2017@ayndex.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'nauka.home2061@ayndex.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'da232ffs@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'kalucaz.dani@freemail.hu\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'dscmmm4@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'drobytrewq@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'savrasvnr36@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'drobqwert@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'dfsfdf@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'43656079@163.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'234343@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'webou@zing.vn\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'kajshkaj@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'romagavriliuk@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'ajhskajh@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'Vladimir-kulkov@ya.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'misterimboybatista@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'jasd@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'hiisme@box.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'andrewmnsk1@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'dscmmm1@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'dscmmm2@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'dscmmm3@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'dviet@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'envik4@ya.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'nicolas_99_10@hotmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'tobyalpedo@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'bratka@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'4ek1st2011114@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'poddubny.nikita2017@yandex.by\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'sdfasdfa@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'azartt1@yandex.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'kram994@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'grozanebes@i.ua\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'kalandara1@yandex.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'fasdfam@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'bounty778@bk.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'barmen_21@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'4ek1st20114@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'4ek1st201114@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'bimainmg@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'bimainsum@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'bimaindw@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'bimainee@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'bimaindk@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'ajksduasyuidauyisd@yahoo.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'ajhsdhjakhjsdjkhasd@yahoo.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'asjkdahjkdhjkaskjhd@yahoo.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'laribax143143@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'adesei21@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'adese22i1@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'ades2ei21@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'anrior@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'dgichko@bk.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'efrencordova42@yahoo.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'sandroadriana@hotmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'bircav1@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'12345672@ru.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'kofdol@inbox.lv\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'lariba143@yahoo.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'asadhsadjkhkjhjkh@yahoo.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'AnnKharetonova@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'imbla@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'grudimania@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'kirill92@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'ustalov92@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'stoletov.ssswot@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'stoletov.swot@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'stoletov.sswot@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'Influencedmaks@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'test7@testr.io\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'vinh.huynh0691@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'a.cristahe89@yahoo.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'kok22@inbox.lv\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'mistris34@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'silentspz@gamil.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'geap9741@yahoo.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'khoas1@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'tavi_k@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'valdis8736@inbx.lv\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'MayprinceBoy_26@yahoo.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'prsto@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'ahjadhajdad@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'njanjanl@yahoo.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'jaylen099@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'jayred05@yahoo.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'ratata_99@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'jrs.steenbergen@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'dsad@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'janiro22.4@hotmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'serkanmaviserit@mail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'45685200@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'direkcimurat10@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'xolod-203@mai.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'iloveyouchin24@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'jadvcu@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'akdbvgjik@abv.bg\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'menhem56611@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'tminh62772@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'serafim.anafim@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'akirshado25@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'caobavong@gmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'xicalo@yahoo.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'oceana_why2000@yahoo.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'biadasda@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'maumenendez@gamil.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'bad_things_with_you@yahoo.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'habranustiu89@hotmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'numaiammailuri@yahoo.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'dumitrascu69@yahoo.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'Ckel90@mail.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'ice234@yahoo.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'gri_03@hotamil.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'difurgumus@htmail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'ghjbku@ghjbku.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'Robinho1902@yandx.ru\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'wieslaw1950@gemail.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'alebergami@yahoo.com\';');
        $this->addSql('UPDATE User SET email_state = \'I\' WHERE email=\'pocem@mail.ru\';');

    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

    }
}
