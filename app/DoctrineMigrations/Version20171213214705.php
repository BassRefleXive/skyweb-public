<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171213214705 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_D7D1FC571844E6B7');
        $this->addSql('DROP INDEX IDX_D7D1FC57B1BB7436');
        $this->addSql('CREATE TEMPORARY TABLE __temp__BonusCode AS SELECT code, activated_by, server_id, amount, status, expire_at, activated_at, created_at, updated_at FROM BonusCode');
        $this->addSql('DROP TABLE BonusCode');
        $this->addSql('CREATE TABLE BonusCode (code VARCHAR(10) NOT NULL COLLATE BINARY, activated_by INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, amount INTEGER NOT NULL, status SMALLINT NOT NULL, expire_at DATETIME NOT NULL, activated_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(code), CONSTRAINT FK_D7D1FC57255FA5E4 FOREIGN KEY (activated_by) REFERENCES UserServerAccount (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_D7D1FC571844E6B7 FOREIGN KEY (server_id) REFERENCES Server (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO BonusCode (code, activated_by, server_id, amount, status, expire_at, activated_at, created_at, updated_at) SELECT code, activated_by, server_id, amount, status, expire_at, activated_at, created_at, updated_at FROM __temp__BonusCode');
        $this->addSql('DROP TABLE __temp__BonusCode');
        $this->addSql('CREATE INDEX IDX_D7D1FC571844E6B7 ON BonusCode (server_id)');
        $this->addSql('CREATE INDEX IDX_D7D1FC57255FA5E4 ON BonusCode (activated_by)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_D7D1FC57255FA5E4');
        $this->addSql('DROP INDEX IDX_D7D1FC571844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__BonusCode AS SELECT code, activated_by, server_id, amount, status, expire_at, activated_at, created_at, updated_at FROM BonusCode');
        $this->addSql('DROP TABLE BonusCode');
        $this->addSql('CREATE TABLE BonusCode (code VARCHAR(10) NOT NULL, activated_by INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, amount INTEGER NOT NULL, status SMALLINT NOT NULL, expire_at DATETIME NOT NULL, activated_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, type VARCHAR(255) NOT NULL COLLATE BINARY, PRIMARY KEY(code))');
        $this->addSql('INSERT INTO BonusCode (code, activated_by, server_id, amount, status, expire_at, activated_at, created_at, updated_at) SELECT code, activated_by, server_id, amount, status, expire_at, activated_at, created_at, updated_at FROM __temp__BonusCode');
        $this->addSql('DROP TABLE __temp__BonusCode');
        $this->addSql('CREATE INDEX IDX_D7D1FC571844E6B7 ON BonusCode (server_id)');
        $this->addSql('CREATE INDEX IDX_D7D1FC57B1BB7436 ON BonusCode (activated_by)');
    }
}
