<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171219015510 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE VipLevelConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, type INTEGER NOT NULL, title VARCHAR(50) NOT NULL, reset_price_discount INTEGER NOT NULL, reset_points_bonus INTEGER NOT NULL, grand_reset_price_discount INTEGER NOT NULL, grand_reset_points_bonus INTEGER NOT NULL, night_start_hour INTEGER NOT NULL, night_start_minute INTEGER NOT NULL, night_add_exp INTEGER NOT NULL, night_add_master_exp INTEGER NOT NULL, night_add_drop INTEGER NOT NULL, night_add_exc_drop INTEGER NOT NULL, day_add_exp INTEGER NOT NULL, day_add_master_exp INTEGER NOT NULL, day_add_drop INTEGER NOT NULL, day_add_exc_drop INTEGER NOT NULL, off_afk_max_hours INTEGER NOT NULL, daily_coins_price INTEGER NOT NULL, daily_money_price INTEGER NOT NULL, renewal_discount INTEGER NOT NULL, enabled BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7D82554A1844E6B7 ON VipLevelConfig (server_id)');

        $this->addSql('DROP INDEX IDX_758ED6D91844E6B7');
        $this->addSql('DROP INDEX IDX_758ED6D9A76ED395');
        $this->addSql('CREATE TEMPORARY TABLE __temp__UserServerAccount AS SELECT id, user_id, server_id, account_id, selected_at, spent_money, spent_credits, purchased_stats, hidden_till, zen FROM UserServerAccount');
        $this->addSql('DROP TABLE UserServerAccount');
        $this->addSql('CREATE TABLE UserServerAccount (id INTEGER NOT NULL, user_id INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, account_id VARCHAR(10) NOT NULL COLLATE BINARY, selected_at DATETIME NOT NULL, spent_money DOUBLE PRECISION NOT NULL, spent_credits INTEGER NOT NULL, purchased_stats INTEGER NOT NULL, hidden_till DATETIME DEFAULT NULL, zen INTEGER NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_758ED6D9A76ED395 FOREIGN KEY (user_id) REFERENCES User (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_758ED6D91844E6B7 FOREIGN KEY (server_id) REFERENCES Server (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO UserServerAccount (id, user_id, server_id, account_id, selected_at, spent_money, spent_credits, purchased_stats, hidden_till, zen) SELECT id, user_id, server_id, account_id, selected_at, spent_money, spent_credits, purchased_stats, hidden_till, zen FROM __temp__UserServerAccount');
        $this->addSql('DROP TABLE __temp__UserServerAccount');
        $this->addSql('CREATE INDEX IDX_758ED6D91844E6B7 ON UserServerAccount (server_id)');
        $this->addSql('CREATE INDEX IDX_758ED6D9A76ED395 ON UserServerAccount (user_id)');
     }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE VipLevelConfig');

        $this->addSql('DROP INDEX IDX_758ED6D9A76ED395');
        $this->addSql('DROP INDEX IDX_758ED6D91844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__UserServerAccount AS SELECT id, user_id, server_id, account_id, selected_at, spent_money, spent_credits, purchased_stats, hidden_till, zen FROM UserServerAccount');
        $this->addSql('DROP TABLE UserServerAccount');
        $this->addSql('CREATE TABLE UserServerAccount (id INTEGER NOT NULL, user_id INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, account_id VARCHAR(10) NOT NULL, selected_at DATETIME NOT NULL, spent_money DOUBLE PRECISION NOT NULL, spent_credits INTEGER NOT NULL, purchased_stats INTEGER NOT NULL, hidden_till DATETIME DEFAULT NULL, zen INTEGER NOT NULL, web_coins INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO UserServerAccount (id, user_id, server_id, account_id, selected_at, spent_money, spent_credits, purchased_stats, hidden_till, zen, web_coin) SELECT id, user_id, server_id, account_id, selected_at, spent_money, spent_credits, purchased_stats, hidden_till, zen, 0 FROM __temp__UserServerAccount');
        $this->addSql('DROP TABLE __temp__UserServerAccount');
        $this->addSql('CREATE INDEX IDX_758ED6D9A76ED395 ON UserServerAccount (user_id)');
        $this->addSql('CREATE INDEX IDX_758ED6D91844E6B7 ON UserServerAccount (server_id)');
     }
}
