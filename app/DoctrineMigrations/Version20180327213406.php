<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180327213406 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_BDE1366EA76ED395');
        $this->addSql('DROP INDEX IDX_BDE1366E1844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__News AS SELECT id, user_id, server_id, status, created_at, updated_at, title_rus, short_text_eng, title_eng, short_text_rus, full_text_eng, full_text_rus FROM News');
        $this->addSql('DROP TABLE News');
        $this->addSql('CREATE TABLE News (id INTEGER NOT NULL, user_id INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, status SMALLINT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, title_rus VARCHAR(100) NOT NULL COLLATE BINARY, short_text_eng CLOB NOT NULL COLLATE BINARY, title_eng VARCHAR(100) NOT NULL COLLATE BINARY, short_text_rus CLOB NOT NULL COLLATE BINARY, full_text_eng CLOB DEFAULT NULL, full_text_rus CLOB DEFAULT NULL, PRIMARY KEY(id), CONSTRAINT FK_BDE1366EA76ED395 FOREIGN KEY (user_id) REFERENCES User (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_BDE1366E1844E6B7 FOREIGN KEY (server_id) REFERENCES Server (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO News (id, user_id, server_id, status, created_at, updated_at, title_rus, short_text_eng, title_eng, short_text_rus, full_text_eng, full_text_rus) SELECT id, user_id, server_id, status, created_at, updated_at, title_rus, short_text_eng, title_eng, short_text_rus, full_text_eng, full_text_rus FROM __temp__News');
        $this->addSql('DROP TABLE __temp__News');
        $this->addSql('CREATE INDEX IDX_BDE1366EA76ED395 ON News (user_id)');
        $this->addSql('CREATE INDEX IDX_BDE1366E1844E6B7 ON News (server_id)');
       }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_BDE1366EA76ED395');
        $this->addSql('DROP INDEX IDX_BDE1366E1844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__News AS SELECT id, user_id, server_id, title_rus, title_eng, short_text_eng, short_text_rus, full_text_eng, full_text_rus, status, created_at, updated_at FROM News');
        $this->addSql('DROP TABLE News');
        $this->addSql('CREATE TABLE News (id INTEGER NOT NULL, user_id INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, title_rus VARCHAR(100) NOT NULL, title_eng VARCHAR(100) NOT NULL, short_text_eng CLOB NOT NULL, short_text_rus CLOB NOT NULL, status SMALLINT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, full_text_eng CLOB NOT NULL COLLATE BINARY, full_text_rus CLOB NOT NULL COLLATE BINARY, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO News (id, user_id, server_id, title_rus, title_eng, short_text_eng, short_text_rus, full_text_eng, full_text_rus, status, created_at, updated_at) SELECT id, user_id, server_id, title_rus, title_eng, short_text_eng, short_text_rus, full_text_eng, full_text_rus, status, created_at, updated_at FROM __temp__News');
        $this->addSql('DROP TABLE __temp__News');
        $this->addSql('CREATE INDEX IDX_BDE1366EA76ED395 ON News (user_id)');
        $this->addSql('CREATE INDEX IDX_BDE1366E1844E6B7 ON News (server_id)');
      }
}
