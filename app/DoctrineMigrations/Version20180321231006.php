<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180321231006 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(15, 19, 203, \'Jewel of Dark Soul\', 0, 1, 1, NULL, 0, 0);');
        $this->addSql('UPDATE ItemInfo SET name=\'Jewel of Dark Life\' WHERE id=1216;');
        $this->addSql('UPDATE ItemInfo SET name=\'Jewel of Dark Bless\' WHERE id=1215;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DELETE FROM ItemInfo WHERE group_id=15 AND item_id=203 AND "level"=0');
        $this->addSql('UPDATE ItemInfo SET name=\'Soul Combination Note\' WHERE id=1216;');
        $this->addSql('UPDATE ItemInfo SET name=\'Thunder Combination Note\' WHERE id=1215;');
    }
}
