<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171215170606 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('UPDATE ItemInfo SET name=\'Jewel of Life Bundle (10)\' WHERE id=928;');
        $this->addSql('UPDATE ItemInfo SET name=\'Jewel of Creation Bundle (10)\' WHERE id=879;');
        $this->addSql('UPDATE ItemInfo SET name=\'Jewel of Guardian Bundle (10)\' WHERE id=868;');
        $this->addSql('UPDATE ItemInfo SET name=\'Gemstone Bundle (10)\' WHERE id=944;');
        $this->addSql('UPDATE ItemInfo SET name=\'Jewel of Harmony Bundle (10)\' WHERE id=938;');
        $this->addSql('UPDATE ItemInfo SET name=\'Jewel of Chaos Bundle (10)\' WHERE id=895;');
        $this->addSql('UPDATE ItemInfo SET name=\'Lower Refining Stone Bundle (10)\' WHERE id=848;');
        $this->addSql('UPDATE ItemInfo SET  name=\'Higher Refining Stone Bundle (10)\' WHERE id=878;');

        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(13, 19, 136, \'Jewel of Life Bundle (20)\', 1, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(13, 19, 137, \'Jewel of Creation Bundle (20)\', 1, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(13, 19, 138, \'Jewel of Guardian Bundle (20)\', 1, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(13, 19, 139, \'Gemstone Bundle (20)\', 1, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(13, 19, 140, \'Jewel of Harmony Bundle (20)\', 1, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(13, 19, 141, \'Jewel of Chaos Bundle (20)\', 1, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(13, 19, 142, \'Lower Refining Stone Bundle (20)\', 1, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(13, 19, 143, \'Higher Refining Stone Bundle (20)\', 1, 1, 1, 0, 0, 0);');

        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(13, 19, 136, \'Jewel of Life Bundle (30)\', 2, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(13, 19, 137, \'Jewel of Creation Bundle (30)\', 2, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(13, 19, 138, \'Jewel of Guardian Bundle (30)\', 2, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(13, 19, 139, \'Gemstone Bundle (30)\', 2, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(13, 19, 140, \'Jewel of Harmony Bundle (30)\', 2, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(13, 19, 141, \'Jewel of Chaos Bundle (30)\', 2, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(13, 19, 142, \'Lower Refining Stone Bundle (30)\', 2, 1, 1, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(13, 19, 143, \'Higher Refining Stone Bundle (30)\', 2, 1, 1, 0, 0, 0);');

        $this->addSql('DELETE FROM ItemInfo WHERE id=773;');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 19, \'Absolute Staff of Archangel\', 0, 1, 2, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 19, \'Absolute Sword of Archangel\', 1, 1, 2, 0, 0, 0);');
        $this->addSql('INSERT INTO ItemInfo (group_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, req_lvl) VALUES(14, 26, 19, \'Absolute Crossbow of Archangel\', 2, 1, 2, 0, 0, 0);');

        $this->addSql('INSERT INTO ItemInfo (group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y, drop_level, durability, attack_speed, defense, min_dmg, max_dmg, wizardry_dmg, wizardry_excellent_dmg, req_str, req_agi, req_ene, req_lvl, skill) VALUES(14, NULL, 26, 99, \'Character Slot Unlock Key\', 2, 1, 1, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);');

        $this->addSql('INSERT INTO ItemInfo (group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y) VALUES(15, NULL, 26, 23, \'Ring of Honor\', 1, 1, 1);');
        $this->addSql('INSERT INTO ItemInfo (group_id, pvp_option_id, market_category_id, item_id, name, "level", x, y) VALUES(15, NULL, 26, 24, \'Dark Stone\', 1, 1, 1);');


    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

    }
}
