<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180521154957 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('UPDATE `Skill` SET `item_info_id` = 519, `is_deletable` = 1 WHERE id = 1;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 535, `is_deletable` = 1 WHERE id = 2;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 544, `is_deletable` = 1 WHERE id = 3;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 514, `is_deletable` = 1 WHERE id = 4;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 525, `is_deletable` = 1 WHERE id = 5;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 516, `is_deletable` = 1 WHERE id = 6;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 534, `is_deletable` = 1 WHERE id = 7;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 549, `is_deletable` = 1 WHERE id = 8;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 521, `is_deletable` = 1 WHERE id = 9;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 517, `is_deletable` = 1 WHERE id = 10;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 532, `is_deletable` = 1 WHERE id = 11;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 548, `is_deletable` = 1 WHERE id = 12;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 536, `is_deletable` = 1 WHERE id = 13;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 522, `is_deletable` = 1 WHERE id = 14;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 537, `is_deletable` = 1 WHERE id = 15;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 523, `is_deletable` = 1 WHERE id = 16;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 936, `is_deletable` = 1 WHERE id = 27;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 932, `is_deletable` = 1 WHERE id = 28;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 902, `is_deletable` = 1 WHERE id = 30;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 1117, `is_deletable` = 1 WHERE id = 31;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 1118, `is_deletable` = 1 WHERE id = 32;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 1121, `is_deletable` = 1 WHERE id = 33;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 1122, `is_deletable` = 1 WHERE id = 34;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 1119, `is_deletable` = 1 WHERE id = 35;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 1120, `is_deletable` = 1 WHERE id = 36;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 547, `is_deletable` = 1 WHERE id = 38;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 543, `is_deletable` = 1 WHERE id = 39;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 540, `is_deletable` = 1 WHERE id = 40;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 939, `is_deletable` = 1 WHERE id = 41;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 864, `is_deletable` = 1 WHERE id = 42;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 911, `is_deletable` = 1 WHERE id = 43;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 869, `is_deletable` = 1 WHERE id = 47;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 937, `is_deletable` = 1 WHERE id = 51;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 959, `is_deletable` = 1 WHERE id = 52;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 856, `is_deletable` = 1 WHERE id = 55;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 945, `is_deletable` = 1 WHERE id = 61;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 894, `is_deletable` = 1 WHERE id = 63;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 861, `is_deletable` = 1 WHERE id = 64;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 901, `is_deletable` = 1 WHERE id = 65;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 880, `is_deletable` = 1 WHERE id = 78;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 524, `is_deletable` = 1 WHERE id = 214;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 533, `is_deletable` = 1 WHERE id = 215;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 546, `is_deletable` = 1 WHERE id = 217;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 531, `is_deletable` = 1 WHERE id = 218;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 515, `is_deletable` = 1 WHERE id = 219;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 518, `is_deletable` = 1 WHERE id = 221;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 545, `is_deletable` = 1 WHERE id = 222;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 541, `is_deletable` = 1 WHERE id = 230;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 870, `is_deletable` = 1 WHERE id = 232;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 529, `is_deletable` = 1 WHERE id = 233;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 889, `is_deletable` = 1 WHERE id = 234;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 924, `is_deletable` = 1 WHERE id = 235;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 842, `is_deletable` = 1 WHERE id = 236;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 538, `is_deletable` = 1 WHERE id = 237;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 843, `is_deletable` = 1 WHERE id = 238;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 530, `is_deletable` = 1 WHERE id = 262;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 539, `is_deletable` = 1 WHERE id = 263;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 528, `is_deletable` = 1 WHERE id = 264;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 520, `is_deletable` = 1 WHERE id = 265;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 526, `is_deletable` = 1 WHERE id = 266;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 527, `is_deletable` = 1 WHERE id = 267;');
        $this->addSql('UPDATE `Skill` SET `item_info_id` = 542, `is_deletable` = 1 WHERE id = 268;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

    }
}
