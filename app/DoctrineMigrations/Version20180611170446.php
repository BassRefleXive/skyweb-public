<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180611170446 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_DCFCB9A51844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__VoteRewardConfig AS SELECT id, server_id, reward_month_top1, reward_month_top2, reward_month_top3, reward_mmotop_regular_vote, reward_mmotop_sms_vote, votes_mmotop_file_url, reward_qtop_regular_vote, reward_qtop_sms_vote, votes_qtop_file_url FROM VoteRewardConfig');
        $this->addSql('DROP TABLE VoteRewardConfig');
        $this->addSql('CREATE TABLE VoteRewardConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, reward_month_top1 SMALLINT NOT NULL, reward_month_top2 SMALLINT NOT NULL, reward_month_top3 SMALLINT NOT NULL, reward_mmotop_regular_vote SMALLINT NOT NULL, reward_mmotop_sms_vote SMALLINT NOT NULL, votes_mmotop_file_url VARCHAR(255) NOT NULL COLLATE BINARY, reward_qtop_regular_vote SMALLINT NOT NULL, reward_qtop_sms_vote SMALLINT NOT NULL, votes_qtop_file_url VARCHAR(255) NOT NULL COLLATE BINARY, reward_xtreme_top100vote SMALLINT NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_DCFCB9A51844E6B7 FOREIGN KEY (server_id) REFERENCES Server (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO VoteRewardConfig (id, server_id, reward_month_top1, reward_month_top2, reward_month_top3, reward_mmotop_regular_vote, reward_mmotop_sms_vote, votes_mmotop_file_url, reward_qtop_regular_vote, reward_qtop_sms_vote, votes_qtop_file_url, reward_xtreme_top100vote) SELECT id, server_id, reward_month_top1, reward_month_top2, reward_month_top3, reward_mmotop_regular_vote, reward_mmotop_sms_vote, votes_mmotop_file_url, reward_qtop_regular_vote, reward_qtop_sms_vote, votes_qtop_file_url, 0 FROM __temp__VoteRewardConfig');
        $this->addSql('DROP TABLE __temp__VoteRewardConfig');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DCFCB9A51844E6B7 ON VoteRewardConfig (server_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_DCFCB9A51844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__VoteRewardConfig AS SELECT id, server_id, reward_month_top1, reward_month_top2, reward_month_top3, reward_mmotop_regular_vote, reward_mmotop_sms_vote, votes_mmotop_file_url, reward_qtop_regular_vote, reward_qtop_sms_vote, votes_qtop_file_url FROM VoteRewardConfig');
        $this->addSql('DROP TABLE VoteRewardConfig');
        $this->addSql('CREATE TABLE VoteRewardConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, reward_month_top1 SMALLINT NOT NULL, reward_month_top2 SMALLINT NOT NULL, reward_month_top3 SMALLINT NOT NULL, reward_mmotop_regular_vote SMALLINT NOT NULL, reward_mmotop_sms_vote SMALLINT NOT NULL, votes_mmotop_file_url VARCHAR(255) NOT NULL, reward_qtop_regular_vote SMALLINT NOT NULL, reward_qtop_sms_vote SMALLINT NOT NULL, votes_qtop_file_url VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO VoteRewardConfig (id, server_id, reward_month_top1, reward_month_top2, reward_month_top3, reward_mmotop_regular_vote, reward_mmotop_sms_vote, votes_mmotop_file_url, reward_qtop_regular_vote, reward_qtop_sms_vote, votes_qtop_file_url) SELECT id, server_id, reward_month_top1, reward_month_top2, reward_month_top3, reward_mmotop_regular_vote, reward_mmotop_sms_vote, votes_mmotop_file_url, reward_qtop_regular_vote, reward_qtop_sms_vote, votes_qtop_file_url FROM __temp__VoteRewardConfig');
        $this->addSql('DROP TABLE __temp__VoteRewardConfig');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DCFCB9A51844E6B7 ON VoteRewardConfig (server_id)');
    }
}
