<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171215001718 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('UPDATE CharacterClass SET type = 19 WHERE id = 3;');
        $this->addSql('UPDATE CharacterClass SET type = 3 WHERE id = 6;');
        $this->addSql('UPDATE CharacterClass SET type = 35 WHERE id = 9;');
        $this->addSql('UPDATE CharacterClass SET type = 50 WHERE id = 11;');
        $this->addSql('UPDATE CharacterClass SET type = 66 WHERE id = 13;');
        $this->addSql('UPDATE CharacterClass SET type = 83 WHERE id = 16;');
        $this->addSql('UPDATE CharacterClass SET type = 98 WHERE id = 18;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('UPDATE CharacterClass SET type = 18 WHERE id = 3;');
        $this->addSql('UPDATE CharacterClass SET type = 2 WHERE id = 6;');
        $this->addSql('UPDATE CharacterClass SET type = 34 WHERE id = 9;');
        $this->addSql('UPDATE CharacterClass SET type = 49 WHERE id = 11;');
        $this->addSql('UPDATE CharacterClass SET type = 65 WHERE id = 13;');
        $this->addSql('UPDATE CharacterClass SET type = 82 WHERE id = 16;');
        $this->addSql('UPDATE CharacterClass SET type = 97 WHERE id = 18;');
    }
}
