<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180426142502 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TEMPORARY TABLE __temp__Country AS SELECT code_iso3, code_iso2, name, name_official, latitude, longitude, zoom, state FROM Country');
        $this->addSql('DROP TABLE Country');
        $this->addSql('CREATE TABLE Country (code_iso3 VARCHAR(3) NOT NULL, code_iso2 VARCHAR(2) NOT NULL COLLATE BINARY, name VARCHAR(128) NOT NULL COLLATE BINARY, name_official VARCHAR(512) NOT NULL COLLATE BINARY, latitude NUMERIC(2, 8) NOT NULL, longitude NUMERIC(2, 8) NOT NULL, zoom SMALLINT NOT NULL, state integer NOT NULL, locale VARCHAR(2) NOT NULL, PRIMARY KEY(code_iso3))');
        $this->addSql('INSERT INTO Country (code_iso3, code_iso2, name, name_official, latitude, longitude, zoom, state, locale) SELECT code_iso3, code_iso2, name, name_official, latitude, longitude, zoom, state, "en" FROM __temp__Country');
        $this->addSql('DROP TABLE __temp__Country');

        $this->addSql('UPDATE Country SET locale = \'ru\' WHERE code_iso3 = \'RUS\';');
        $this->addSql('UPDATE Country SET locale = \'ru\' WHERE code_iso3 = \'BLR\';');
        $this->addSql('UPDATE Country SET locale = \'ru\' WHERE code_iso3 = \'KAZ\';');
        $this->addSql('UPDATE Country SET locale = \'ru\' WHERE code_iso3 = \'KGZ\';');
        $this->addSql('UPDATE Country SET locale = \'ru\' WHERE code_iso3 = \'UZB\';');
        $this->addSql('UPDATE Country SET locale = \'ru\' WHERE code_iso3 = \'TJK\';');
        $this->addSql('UPDATE Country SET locale = \'ru\' WHERE code_iso3 = \'TKM\';');
        $this->addSql('UPDATE Country SET locale = \'ru\' WHERE code_iso3 = \'ROU\';');
        $this->addSql('UPDATE Country SET locale = \'ru\' WHERE code_iso3 = \'MDA\';');
        $this->addSql('UPDATE Country SET locale = \'ru\' WHERE code_iso3 = \'UKR\';');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TEMPORARY TABLE __temp__Country AS SELECT code_iso3, code_iso2, name, name_official, latitude, longitude, zoom, state FROM Country');
        $this->addSql('DROP TABLE Country');
        $this->addSql('CREATE TABLE Country (code_iso3 VARCHAR(3) NOT NULL COLLATE BINARY, code_iso2 VARCHAR(2) NOT NULL, name VARCHAR(128) NOT NULL, name_official VARCHAR(512) NOT NULL, latitude NUMERIC(2, 8) NOT NULL, longitude NUMERIC(2, 8) NOT NULL, zoom SMALLINT NOT NULL, state INTEGER NOT NULL, PRIMARY KEY(code_iso3))');
        $this->addSql('INSERT INTO Country (code_iso3, code_iso2, name, name_official, latitude, longitude, zoom, state) SELECT code_iso3, code_iso2, name, name_official, latitude, longitude, zoom, state FROM __temp__Country');
        $this->addSql('DROP TABLE __temp__Country');
    }
}
