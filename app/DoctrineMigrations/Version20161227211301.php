<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161227211301 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE webshop_category_items (webshop_category_id INTEGER NOT NULL, item_info_id INTEGER NOT NULL, PRIMARY KEY(webshop_category_id, item_info_id))');
        $this->addSql('CREATE INDEX IDX_6E927315609AADCB ON webshop_category_items (webshop_category_id)');
        $this->addSql('CREATE INDEX IDX_6E92731556D6E7AC ON webshop_category_items (item_info_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE webshop_category_items');
    }
}
