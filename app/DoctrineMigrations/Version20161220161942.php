<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161220161942 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE AncientSetItem (id INTEGER NOT NULL, ancient_set_id INTEGER DEFAULT NULL, item_info_id INTEGER DEFAULT NULL, bonus INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_DE03DB73BFF8E8BE ON AncientSetItem (ancient_set_id)');
        $this->addSql('CREATE INDEX IDX_DE03DB7356D6E7AC ON AncientSetItem (item_info_id)');
        $this->addSql('CREATE TABLE ItemInfo (id INTEGER NOT NULL, group_id INTEGER DEFAULT NULL, pvp_option_id INTEGER DEFAULT NULL, item_id INTEGER NOT NULL, name VARCHAR(100) NOT NULL, level INTEGER NOT NULL, x INTEGER NOT NULL, y INTEGER NOT NULL, drop_level INTEGER DEFAULT NULL, durability INTEGER DEFAULT NULL, attack_speed INTEGER DEFAULT NULL, defense INTEGER DEFAULT NULL, min_dmg INTEGER DEFAULT NULL, max_dmg INTEGER DEFAULT NULL, wizardry_dmg INTEGER DEFAULT NULL, req_str INTEGER DEFAULT NULL, req_agi INTEGER DEFAULT NULL, req_ene INTEGER DEFAULT NULL, req_lvl INTEGER DEFAULT NULL, skill VARCHAR(100) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_38B39EF7FE54D947 ON ItemInfo (group_id)');
        $this->addSql('CREATE INDEX IDX_38B39EF7890798E5 ON ItemInfo (pvp_option_id)');
        $this->addSql('CREATE INDEX idx_item_id ON ItemInfo (item_id)');
        $this->addSql('CREATE TABLE character_class_items (item_info_id INTEGER NOT NULL, character_class_id INTEGER NOT NULL, PRIMARY KEY(item_info_id, character_class_id))');
        $this->addSql('CREATE INDEX IDX_3A362D756D6E7AC ON character_class_items (item_info_id)');
        $this->addSql('CREATE INDEX IDX_3A362D7B201E281 ON character_class_items (character_class_id)');
        $this->addSql('CREATE TABLE AncientSet (id INTEGER NOT NULL, name VARCHAR(100) NOT NULL, options CLOB NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE PvPOption (id INTEGER NOT NULL, options CLOB NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE CharacterClass (id INTEGER NOT NULL, name VARCHAR(50) NOT NULL, abbreviation VARCHAR(5) NOT NULL, type INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AB372AC0BCF3411D ON CharacterClass (abbreviation)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE AncientSetItem');
        $this->addSql('DROP TABLE ItemInfo');
        $this->addSql('DROP TABLE character_class_items');
        $this->addSql('DROP TABLE AncientSet');
        $this->addSql('DROP TABLE PvPOption');
        $this->addSql('DROP TABLE CharacterClass');
    }
}
