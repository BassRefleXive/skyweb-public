<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161228153002 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE WebShopConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, max_item_level SMALLINT NOT NULL, max_item_option SMALLINT NOT NULL, max_exc_count SMALLINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F11BB4681844E6B7 ON WebShopConfig (server_id)');

        $this->addSql('DROP INDEX IDX_4F3FA1C91844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__WebShopCategory AS SELECT id, server_id, name, status FROM WebShopCategory');
        $this->addSql('DROP TABLE WebShopCategory');
        $this->addSql('CREATE TABLE WebShopCategory (id INTEGER NOT NULL, web_shop_config_id INTEGER DEFAULT NULL, name VARCHAR(50) NOT NULL COLLATE BINARY, status SMALLINT NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_4F3FA1C940319467 FOREIGN KEY (web_shop_config_id) REFERENCES WebShopConfig (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO WebShopCategory (id, web_shop_config_id, name, status) SELECT id, server_id, name, status FROM __temp__WebShopCategory');
        $this->addSql('DROP TABLE __temp__WebShopCategory');
        $this->addSql('CREATE INDEX IDX_4F3FA1C940319467 ON WebShopCategory (web_shop_config_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE WebShopConfig');

        $this->addSql('DROP INDEX IDX_4F3FA1C940319467');
        $this->addSql('CREATE TEMPORARY TABLE __temp__WebShopCategory AS SELECT id, web_shop_config_id, name, status FROM WebShopCategory');
        $this->addSql('DROP TABLE WebShopCategory');
        $this->addSql('CREATE TABLE WebShopCategory (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, name VARCHAR(50) NOT NULL, status SMALLINT NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_4F3FA1C940319467 FOREIGN KEY (server_id) REFERENCES WebShopConfig (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO WebShopCategory (id, server_id, name, status) SELECT id, web_shop_config_id, name, status FROM __temp__WebShopCategory');
        $this->addSql('DROP TABLE __temp__WebShopCategory');
        $this->addSql('CREATE INDEX IDX_4F3FA1C91844E6B7 ON WebShopCategory (server_id)');
    }
}
