<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170309211541 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_DCFCB9A51844E6B8');
        $this->addSql('CREATE TEMPORARY TABLE __temp__FaqInfo AS SELECT id, server_id, arkania_max_reset, reset_zen_price, reset_credits_reward, gr_reset, gr_stats_reward, gr_credits_reward, gr_zen_required FROM FaqInfo');
        $this->addSql('DROP TABLE FaqInfo');
        $this->addSql('CREATE TABLE FaqInfo (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, arkania_max_reset SMALLINT NOT NULL, reset_zen_price SMALLINT NOT NULL, reset_credits_reward SMALLINT NOT NULL, gr_reset SMALLINT NOT NULL, gr_stats_reward SMALLINT NOT NULL, gr_credits_reward SMALLINT NOT NULL, gr_zen_required SMALLINT NOT NULL, `values` TEXT, PRIMARY KEY(id), CONSTRAINT FK_F752CBC81844E6B7 FOREIGN KEY (server_id) REFERENCES Server (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO FaqInfo (id, server_id, arkania_max_reset, reset_zen_price, reset_credits_reward, gr_reset, gr_stats_reward, gr_credits_reward, gr_zen_required) SELECT id, server_id, arkania_max_reset, reset_zen_price, reset_credits_reward, gr_reset, gr_stats_reward, gr_credits_reward, gr_zen_required FROM __temp__FaqInfo');
        $this->addSql('DROP TABLE __temp__FaqInfo');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F752CBC81844E6B7 ON FaqInfo (server_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_F752CBC81844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__FaqInfo AS SELECT id, server_id, arkania_max_reset, reset_zen_price, reset_credits_reward, gr_reset, gr_stats_reward, gr_credits_reward, gr_zen_required FROM FaqInfo');
        $this->addSql('DROP TABLE FaqInfo');
        $this->addSql('CREATE TABLE FaqInfo (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, arkania_max_reset SMALLINT NOT NULL, reset_zen_price SMALLINT DEFAULT 0 NOT NULL, reset_credits_reward SMALLINT DEFAULT 0 NOT NULL, gr_reset SMALLINT DEFAULT 0 NOT NULL, gr_stats_reward SMALLINT DEFAULT 0 NOT NULL, gr_credits_reward SMALLINT DEFAULT 0 NOT NULL, gr_zen_required SMALLINT DEFAULT 0 NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO FaqInfo (id, server_id, arkania_max_reset, reset_zen_price, reset_credits_reward, gr_reset, gr_stats_reward, gr_credits_reward, gr_zen_required) SELECT id, server_id, arkania_max_reset, reset_zen_price, reset_credits_reward, gr_reset, gr_stats_reward, gr_credits_reward, gr_zen_required FROM __temp__FaqInfo');
        $this->addSql('DROP TABLE __temp__FaqInfo');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DCFCB9A51844E6B8 ON FaqInfo (server_id)');
    }
}
