<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171227210849 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('UPDATE AncientSet SET `enabled` = 1 WHERE id = 11;');
        $this->addSql('UPDATE AncientSet SET `enabled` = 1 WHERE id = 27;');

        $this->addSql('DELETE FROM AncientSetItem WHERE ancient_set_id IN (48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64)');
        $this->addSql('DELETE FROM AncientSet WHERE id IN (48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64)');

        $this->addSql('UPDATE AncientSet SET name = "Heras" WHERE name = "Hera"');

        $stmt = $this->connection->prepare('INSERT INTO AncientSet (`name`, `options`, `enabled`) VALUES (:name, :options, 1)');
        $stmt->execute([
            'name'    => 'Hades',
            'options' => json_encode([
                'Increase Max HP +30',
                'Increase Skill Damage +25',
                'Increase Defence +10',
                'Double Damage Rate +10%',
                'Increase Critical Damage Rate +10%',
                'Increase Excellent Damage Rate +10%',
                'Increase Critical Damage +10',
                'Increase Excellent Damage +10',
            ]),
        ]);

        $insertAncientSetItemStmt = $this->connection->prepare('INSERT INTO AncientSetItem (bonus, ancient_set_id, item_info_id) VALUES (
            :bonus, 
            (SELECT id FROM AncientSet WHERE name = :ancientSetName),
            (SELECT id FROM ItemInfo WHERE group_id = (SELECT id FROM ItemGroup WHERE type_id = :groupId) AND item_id = :itemId)
        )');

        $insertAncientSetItemStmt->execute(['bonus' => 5, 'ancientSetName' => 'Hades', 'groupId' => 7, 'itemId' => 64]);
        $insertAncientSetItemStmt->execute(['bonus' => 5, 'ancientSetName' => 'Hades', 'groupId' => 8, 'itemId' => 64]);
        $insertAncientSetItemStmt->execute(['bonus' => 5, 'ancientSetName' => 'Hades', 'groupId' => 9, 'itemId' => 64]);
        $insertAncientSetItemStmt->execute(['bonus' => 5, 'ancientSetName' => 'Hades', 'groupId' => 10, 'itemId' => 64]);
        $insertAncientSetItemStmt->execute(['bonus' => 5, 'ancientSetName' => 'Hades', 'groupId' => 11, 'itemId' => 64]);

        $stmt = $this->connection->prepare('UPDATE AncientSet SET `options` = :options WHERE `name` = :name;');
        $stmt->execute([
            'name'    => 'Moros',
            'options' => json_encode([
                'Increase Max HP +30',
                'Increase Skill Damage +25',
                'Increase Defence +10',
                'Double Damage Rate +10%',
                'Increase Critical Damage Rate +10%',
                'Increase Excellent Damage Rate +10%',
                'Increase Critical Damage +10',
                'Increase Excellent Damage +10',
            ]),
        ]);
        $stmt->execute([
            'name'    => 'Dione',
            'options' => json_encode([
                'Increase Max HP +30',
                'Increase Skill Damage +25',
                'Increase Defence +10',
                'Double Damage Rate +10%',
                'Increase Critical Damage Rate +10%',
                'Increase Excellent Damage Rate +10%',
                'Increase Critical Damage +10',
                'Increase Excellent Damage +10',
            ]),
        ]);
        $stmt->execute([
            'name'    => 'Ophion',
            'options' => json_encode([
                'Increase Max HP +30',
                'Increase Skill Damage +25',
                'Increase Defence +10',
                'Double Damage Rate +10%',
                'Increase Critical Damage Rate +10%',
                'Increase Excellent Damage Rate +10%',
                'Increase Critical Damage +10',
                'Increase Excellent Damage +10',
            ]),
        ]);


        $stmt->execute([
            'name'    => 'Meter',
            'options' => json_encode([
                'Increase Max HP +50',
                'Increase Defence +40',
                'Double Damage Rate +15%',
                'Increase Critical Damage Rate +15%',
                'Increase Excellent Damage Rate +15%',
                'Increase Critical Damage +20',
                'Increase Excellent Damage +20',
                'Ignore Opponents Defensive Power +5%',
            ]),
        ]);


        $stmt->execute([
            'name'    => 'Hegaton',
            'options' => json_encode([
                'Increase Max HP +50',
                'Increase Defence +40',
                'Double Damage Rate +15%',
                'Increase Critical Damage Rate +15%',
                'Increase Excellent Damage Rate +15%',
                'Increase Critical Damage +20',
                'Increase Excellent Damage +20',
                'Ignore Opponents Defensive Power +5%',
            ]),
        ]);


        $stmt->execute([
            'name'    => 'Castol',
            'options' => json_encode([
                'Increase Max HP +50',
                'Increase Defence +40',
                'Double Damage Rate +15%',
                'Increase Critical Damage Rate +15%',
                'Increase Excellent Damage Rate +15%',
                'Increase Critical Damage +20',
                'Increase Excellent Damage +20',
                'Ignore Opponents Defensive Power +5%',
            ]),
        ]);


        $stmt->execute([
            'name'    => 'Taros',
            'options' => json_encode([
                'Increase Max HP +50',
                'Increase Defence +40',
                'Double Damage Rate +15%',
                'Increase Critical Damage Rate +15%',
                'Increase Excellent Damage Rate +15%',
                'Increase Critical Damage +20',
                'Increase Excellent Damage +20',
                'Ignore Opponents Defensive Power +5%',
            ]),
        ]);


        $stmt->execute([
            'name'    => 'Nemesis',
            'options' => json_encode([
                'Increase Max HP +50',
                'Increase Defence +40',
                'Double Damage Rate +15%',
                'Increase Critical Damage Rate +15%',
                'Increase Excellent Damage Rate +15%',
                'Increase Critical Damage +20',
                'Increase Excellent Damage +20',
                'Ignore Opponents Defensive Power +5%',
            ]),
        ]);


        $stmt->execute([
            'name'    => 'Amis',
            'options' => json_encode([
                'Increase Max HP +50',
                'Increase Defence +40',
                'Double Damage Rate +15%',
                'Increase Critical Damage Rate +15%',
                'Increase Excellent Damage Rate +15%',
                'Increase Critical Damage +20',
                'Increase Excellent Damage +20',
                'Ignore Opponents Defensive Power +5%',
            ]),
        ]);


        $stmt->execute([
            'name'    => 'Trite',
            'options' => json_encode([
                'Increase Max HP +50',
                'Increase Defence +40',
                'Double Damage Rate +15%',
                'Increase Critical Damage Rate +15%',
                'Increase Excellent Damage Rate +15%',
                'Increase Critical Damage +20',
                'Increase Excellent Damage +20',
                'Ignore Opponents Defensive Power +5%',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Chamer',
            'options' => json_encode([
                'Increase Defense +50',
                'Double Damage Rate +5%',
                'Damage +30',
                'Critical Damage +30',
                'Increase Excellent Damage Rate +15%',
                'Increase Skill Damage +30',
                'Excellent Damage +20',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Vega',
            'options' => json_encode([
                'Attack Rate +50',
                'Increase Stamina +50',
                'Increase Max Damage +30',
                'Increase Excellent Damage Rate +15%',
                'Double Damage Rate +5%',
                'Ignore Opponents Defensive Power +5%',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Semeden',
            'options' => json_encode([
                'Increase Wizardry Damage +15%',
                'Increase Skill Damage +25',
                'Increase Energy +30',
                'Increase Critical Damage Rate +15%',
                'Increase Excellent Damage Rate +15%',
                'Ignore Opponents Defensive Power +5%',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Chrono',
            'options' => json_encode([
                'Double Damage Rate +20%',
                'Increase Defence +60',
                'Increase Skill DMG +30',
                'Increase Critical Damage Rate +15%',
                'Increase Excellent Damage Rate +15%',
                'Increase Critical Damage +20',
                'Increase Excellent Damage +20',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Broy',
            'options' => json_encode([
                'Increase Damage +20',
                'Increase Skill Damage +20',
                'Increase Energy +30',
                'Increase Critical Damage Rate +15%',
                'Increase Excellent Damage Rate +15%',
                'Ignore Opponents Defensive Power +5%',
                'Increase Command +30',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Agnis',
            'options' => json_encode([
                'Double Damage Rate +10%',
                'Increase Defense +40',
                'Increase Skill Damage +20',
                'Increase Critical Damage Rate +15%',
                'Increase Excellent Damage Rate +15%',
                'Increase Critical Damage +20',
                'Increase Excellent Damage +20',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Muren',
            'options' => json_encode([
                'Increase Skill Damage +10',
                'Increase Wizardry Damage +10%',
                'Double Damage Rate +10%',
                'Increase Critical Damage Rate +15%',
                'Increase Excellent Damage Rate +15%',
                'Increase Defense +25',
                'DMG +20% with Two-handed weapons',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Muren',
            'options' => json_encode([
                'Ignore Opponents Defensive Power +5%',
                'Double Damage Rate +15%',
                'Increase Skill Damage +15',
                'Increase Excellent Damage Rate +15%',
                'Increase Excellent Damage +30',
                'Increase Wizardry Damage +10%',
                'Increase Strength +30',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Aruan',
            'options' => json_encode([
                'Increase Damage +20',
                'Double Damage Rate +10%',
                'Increase Skill Damage +20',
                'Increase Critical Damage Rate +15%',
                'Increase Excellent Damage Rate +15%',
                'Ignore Opponents Defensive Power +5%',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Gywen',
            'options' => json_encode([
                'Increase Agility +30',
                'Increase Min Damage +20',
                'Increase Defense +20',
                'Increase Max Damage +20',
                'Increase Critical Damage Rate +15%',
                'Increase Excellent Damage Rate +15%',
                'Increase Critical Damage +20',
                'Increase Excellent Damage +20',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Karis',
            'options' => json_encode([
                'Increase Skill Damage +15',
                'Double Damage Rate +10%',
                'Increase Critical Damage Rate +10%',
                'Increase Agility +40',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Argo',
            'options' => json_encode([
                'Increase Max Damage +20',
                'Increase Skill Damage +25',
                'Increase Max AG +50',
                'Double Damage Rate +5%',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Elvian',
            'options' => json_encode([
                'Increase Agility +30',
                'Ignore Opponents Defensive Power +5%',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Odin',
            'options' => json_encode([
                'Increase Energy +15',
                'Increase Max HP +50',
                'Increase Damage Success Rate +50',
                'Increase Agility +30',
                'Increase Max Mana +50',
                'Ignore Opponents Defensive Power +5%',
                'Increase Max AG +50',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Gaia',
            'options' => json_encode([
                'Increase Skill Damage +10',
                'Increase Max Mana +25',
                'Increase Strength +10',
                'Double Damage Rate +5%',
                'Increase Agility +30',
                'Increase Excellent Damage Rate +10%',
                'Increase Excellent Damage +10',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Drake',
            'options' => json_encode([
                'Increase Agility +20',
                'Increase Damage +25',
                'Double Damage Rate +20%',
                'Increase Defense +40',
                'Increase Critical Damage Rate +10%',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Ceto',
            'options' => json_encode([
                'Increase Agility +10',
                'Increase Max HP +50',
                'Increase Defense +20',
                'Increase Defense with Shield +5%',
                'Increase Energy +10',
                'Increase Strength +20',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Enis',
            'options' => json_encode([
                'Increase Skill Damage +10',
                'Double Damage Rate +10%',
                'Increase Energy +30',
                'Increase Wizardry Damage +10%',
                'Ignore Opponents Defensive Power +5%',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Anubis',
            'options' => json_encode([
                'Double Damage Rate +10%',
                'Increase Max Mana +50',
                'Increase Wizardry Damage +10%',
                'Increase Critical Damage Rate +15%',
                'Increase Excellent Damage Rate +15%',
                'Increase Critical Damage +20',
                'Increase Excellent Damage +20',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Minet',
            'options' => json_encode([
                'Increase Energy +30',
                'Increase Defense +30',
                'Increase Max Mana +100',
                'Increase Skill Damage +15',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Heras',
            'options' => json_encode([
                'Increase Strength +15',
                'Increase Wizardry Damage +10%',
                'Increase Defense with Shield +5%',
                'Increase Energy +15',
                'Increase Damage Success Rate +50',
                'Increase Critical Damage Rate +10%',
                'Increase Excellent Damage Rate +10%',
                'Increase Max HP +50',
                'Increase Max Mana +50',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Sylion',
            'options' => json_encode([
                'Double Damage Rate +5%',
                'Increase Critical Damage Rate +5%',
                'Increase Defense +20',
                'Increase Strength +50',
                'Increase Agility +50',
                'Increase Stamina +50',
                'Increase Energy +50',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Evis',
            'options' => json_encode([
                'Increase Skill Damage +15',
                'Increase Stamina +20',
                'Increase Wizardry Damage +10%',
                'Double Damage Rate +5%',
                'Increase Damage Success Rate +50',
                'Increase AG Rate +5',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Barnake',
            'options' => json_encode([
                'Increase Wizardry Damage +10%',
                'Increase Energy +20',
                'Increase Skill Damage +30',
                'Increase Max Mana +100',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Apollo',
            'options' => json_encode([
                'Increase Energy +10',
                'Increase Wizardry Damage +5%',
                'Increase Skill Damage +10',
                'Increase Max Mana +30',
                'Increase Max HP +30',
                'Increase Max AG +20',
                'Increase Critical Damage Rate +10%',
                'Increase Excellent Damage Rate +10%',
                'Increase Energy +30',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Vicious',
            'options' => json_encode([
                'Increase Skill Damage +15',
                'Increase Damage +15',
                'Double Damage Rate +10%',
                'Increase Min Damage +20',
                'Increase Max Damage +30',
                'Ignore Opponents Defensive Power +5%',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Hyon',
            'options' => json_encode([
                'Increase Defense +25',
                'Double Damage Rate +10%',
                'Increase Skill Damage +20',
                'Increase Critical Damage Rate +15%',
                'Increase Excellent Damage Rate +15%',
                'Increase Critical Damage +20',
                'Increase Excellent Damage +20',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Rave',
            'options' => json_encode([
                'Increase Skill Damage +20',
                'Double Damage Rate +10%',
                'DMG +30% with Two-handed weapons',
                'Ignore Opponents Defensive Power +5%',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Kantata',
            'options' => json_encode([
                'Increase Energy +15',
                'Increase Stamina +30',
                'Increase Wizardry Damage +10%',
                'Increase Strength +15',
                'Increase Skill Damage +25',
                'Increase Excellent Damage Rate +10%',
                'Increase Excellent Damage +20',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Cloud',
            'options' => json_encode([
                'Increase Critical Damage Rate +20%',
                'Critical Damage +50',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Garuda',
            'options' => json_encode([
                'Increase Max AG +30',
                'Double Damage Rate +5%',
                'Increase Energy +15',
                'Increase Max HP +50',
                'Increase Skill Damage +25',
                'Increase Wizardry Damage +15%',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Berserker',
            'options' => json_encode([
                'Increase Max Damage +10',
                'Increase Max Damage +20',
                'Increase Max Damage +30',
                'Increase Max Damage +40',
                'Increase Skill Damage +40',
                'Increase Strength +40',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Eplete',
            'options' => json_encode([
                'Increase Skill Damage +15',
                'Increase Damage Success Rate +50',
                'Increase Wizardry Damage +5%',
                'Increase Max HP +50',
                'Increase Max AG +30',
                'Increase Critical Damage Rate +10%',
                'Increase Excellent Damage Rate +10%',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Mist',
            'options' => json_encode([
                'Increase Stamina +20',
                'Increase Skill Damage +30',
                'Double Damage Rate +10%',
                'Increase Agility +20',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Hyperion',
            'options' => json_encode([
                'Increase Energy +15',
                'Increase Agility +15',
                'Increase Skill Damage +20',
                'Increase Max Mana +30',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Anonymous',
            'options' => json_encode([
                'Increase Max HP +50',
                'Increase Agility +50',
                'Increase Defense with Shield +25%',
                'Increase Damage +30',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Warrior',
            'options' => json_encode([
                'Increase Strength +10',
                'Increase Skill Damage +10',
                'Increase Max AG +20',
                'Increase AG Rate +5',
                'Increase Defense +20',
                'Increase Agility +10',
                'Increase Critical Damage Rate +5%',
                'Increase Excellent Damage Rate +5%',
                'Increase Strength +25',
            ]),
        ]);

        $stmt->execute([
            'name'    => 'Fase',
            'options' => json_encode([
                'Increase Max HP +100',
                'Increase Max Mana +100',
                'Increase Defense +100',
            ]),
        ]);

        $this->addSql('UPDATE AncientSet SET `name` = `name` || \'\'\'s\' WHERE `name` NOT LIKE "Enis" AND `name` NOT LIKE "%\'s";');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

    }
}
