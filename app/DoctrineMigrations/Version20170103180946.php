<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170103180946 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE CartDiscountCoupon (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, coupon VARCHAR(5) NOT NULL, discount INTEGER NOT NULL, type VARCHAR(255) NOT NULL, status INTEGER NOT NULL, expire_at DATETIME NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_82476BA1844E6B7 ON CartDiscountCoupon (server_id)');
        $this->addSql('CREATE UNIQUE INDEX WebShopDiscountCoupon_coupon_UNIQUE ON CartDiscountCoupon (coupon)');
        $this->addSql('CREATE TABLE CartPurchaseDiscount (id INTEGER NOT NULL, webshop_purchase_item_log_id INTEGER DEFAULT NULL, web_shop_discount_coupon_id INTEGER DEFAULT NULL, type VARCHAR(255) NOT NULL, discount_percent INTEGER NOT NULL, discount_amount DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6F2EA279963592CE ON CartPurchaseDiscount (webshop_purchase_item_log_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6F2EA27938D5CA98 ON CartPurchaseDiscount (web_shop_discount_coupon_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE CartDiscountCoupon');
        $this->addSql('DROP TABLE CartPurchaseDiscount');
    }
}
