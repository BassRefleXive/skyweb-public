<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171030225818 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');


        $this->addSql('CREATE TABLE DataServerCredentials (id INTEGER NOT NULL, ip VARCHAR(255) NOT NULL, port INTEGER NOT NULL, timeout INTEGER NOT NULL, PRIMARY KEY(id))');

        $this->addSql('INSERT INTO DataServerCredentials (id, ip, port, timeout) VALUES (1, "176.9.145.114", 55960, 3);');
        $this->addSql('INSERT INTO DataServerCredentials (id, ip, port, timeout) VALUES (2, "176.9.145.114", 55964, 3);');
        $this->addSql('INSERT INTO DataServerCredentials (id, ip, port, timeout) VALUES (3, "176.9.145.114", 55968, 3);');

        $this->addSql('DROP INDEX UNIQ_5DC1D0C0BD138F07');
        $this->addSql('CREATE TEMPORARY TABLE __temp__Server AS SELECT id, database_credential_id, exp, "drop", version, name, description, max_online FROM Server');
        $this->addSql('DROP TABLE Server');
        $this->addSql('CREATE TABLE Server (id INTEGER NOT NULL, database_credential_id INTEGER NOT NULL, data_server_credentials_id INTEGER NOT NULL DEFAULT 0, exp INTEGER NOT NULL, "drop" INTEGER NOT NULL, version VARCHAR(50) NOT NULL COLLATE BINARY, name VARCHAR(50) NOT NULL COLLATE BINARY, description VARCHAR(255) DEFAULT NULL COLLATE BINARY, max_online INTEGER NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_5DC1D0C0BD138F07 FOREIGN KEY (database_credential_id) REFERENCES DatabaseCredentials (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO Server (id, database_credential_id, exp, "drop", version, name, description, max_online) SELECT id, database_credential_id, exp, "drop", version, name, description, max_online FROM __temp__Server');
        $this->addSql('DROP TABLE __temp__Server');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5DC1D0C0BD138F07 ON Server (database_credential_id)');

        $this->addSql('UPDATE Server SET data_server_credentials_id = 1 WHERE id = 1;');
        $this->addSql('UPDATE Server SET data_server_credentials_id = 2 WHERE id = 2;');
        $this->addSql('UPDATE Server SET data_server_credentials_id = 3 WHERE id = 3;');

        $this->addSql('CREATE UNIQUE INDEX UNIQ_SERVER_DATASERVER_ID ON Server (database_credential_id);');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE DataServerCredentials');

        $this->addSql('DROP INDEX UNIQ_5DC1D0C0BD138F07');
        $this->addSql('DROP INDEX UNIQ_SERVER_DATASERVER_ID');
        $this->addSql('CREATE TEMPORARY TABLE __temp__Server AS SELECT id, database_credential_id, exp, "drop", version, name, description, max_online FROM Server');
        $this->addSql('DROP TABLE Server');
        $this->addSql('CREATE TABLE Server (id INTEGER NOT NULL, database_credential_id INTEGER NOT NULL, exp INTEGER NOT NULL, "drop" INTEGER NOT NULL, version VARCHAR(50) NOT NULL, name VARCHAR(50) NOT NULL, description VARCHAR(255) DEFAULT NULL, max_online INTEGER DEFAULT 0 NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO Server (id, database_credential_id, exp, "drop", version, name, description, max_online) SELECT id, database_credential_id, exp, "drop", version, name, description, max_online FROM __temp__Server');
        $this->addSql('DROP TABLE __temp__Server');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5DC1D0C0BD138F07 ON Server (database_credential_id)');
    }
}
