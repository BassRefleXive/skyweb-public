<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171219170157 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_7D82554A1844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__VipLevelConfig AS SELECT id, server_id, type, title, reset_price_discount, reset_points_bonus, grand_reset_price_discount, grand_reset_points_bonus, night_start_hour, night_start_minute, night_add_exp, night_add_master_exp, night_add_drop, night_add_exc_drop, day_add_exp, day_add_master_exp, day_add_drop, day_add_exc_drop, off_afk_max_hours, daily_coins_price, daily_money_price, renewal_discount, enabled FROM VipLevelConfig');
        $this->addSql('DROP TABLE VipLevelConfig');
        $this->addSql('CREATE TABLE VipLevelConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, type INTEGER NOT NULL, title VARCHAR(50) NOT NULL COLLATE BINARY, reset_price_discount INTEGER NOT NULL, reset_points_bonus INTEGER NOT NULL, grand_reset_price_discount INTEGER NOT NULL, grand_reset_points_bonus INTEGER NOT NULL, night_start_hour INTEGER NOT NULL, night_start_minute INTEGER NOT NULL, night_add_exp INTEGER NOT NULL, night_add_master_exp INTEGER NOT NULL, night_add_drop INTEGER NOT NULL, night_add_exc_drop INTEGER NOT NULL, day_add_exp INTEGER NOT NULL, day_add_master_exp INTEGER NOT NULL, day_add_drop INTEGER NOT NULL, day_add_exc_drop INTEGER NOT NULL, off_afk_max_hours INTEGER NOT NULL, daily_coins_price INTEGER NOT NULL, renewal_discount INTEGER NOT NULL, enabled BOOLEAN NOT NULL, day_start_hour INTEGER NOT NULL, day_start_minute INTEGER NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_7D82554A1844E6B7 FOREIGN KEY (server_id) REFERENCES Server (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO VipLevelConfig (id, server_id, type, title, reset_price_discount, reset_points_bonus, grand_reset_price_discount, grand_reset_points_bonus, night_start_hour, night_start_minute, night_add_exp, night_add_master_exp, night_add_drop, night_add_exc_drop, day_add_exp, day_add_master_exp, day_add_drop, day_add_exc_drop, off_afk_max_hours, daily_coins_price, day_start_hour, renewal_discount, enabled, day_start_hour, day_start_minute) SELECT id, server_id, type, title, reset_price_discount, reset_points_bonus, grand_reset_price_discount, grand_reset_points_bonus, night_start_hour, night_start_minute, night_add_exp, night_add_master_exp, night_add_drop, night_add_exc_drop, day_add_exp, day_add_master_exp, day_add_drop, day_add_exc_drop, off_afk_max_hours, daily_coins_price, daily_money_price, renewal_discount, enabled, 0, 0 FROM __temp__VipLevelConfig');
        $this->addSql('DROP TABLE __temp__VipLevelConfig');
        $this->addSql('CREATE INDEX IDX_7D82554A1844E6B7 ON VipLevelConfig (server_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_7D82554A1844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__VipLevelConfig AS SELECT id, server_id, type, title, reset_price_discount, reset_points_bonus, grand_reset_price_discount, grand_reset_points_bonus, night_start_hour, night_start_minute, night_add_exp, night_add_master_exp, night_add_drop, night_add_exc_drop, day_add_exp, day_add_master_exp, day_add_drop, day_add_exc_drop, off_afk_max_hours, daily_coins_price, renewal_discount, enabled FROM VipLevelConfig');
        $this->addSql('DROP TABLE VipLevelConfig');
        $this->addSql('CREATE TABLE VipLevelConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, type INTEGER NOT NULL, title VARCHAR(50) NOT NULL, reset_price_discount INTEGER NOT NULL, reset_points_bonus INTEGER NOT NULL, grand_reset_price_discount INTEGER NOT NULL, grand_reset_points_bonus INTEGER NOT NULL, night_start_hour INTEGER NOT NULL, night_start_minute INTEGER NOT NULL, night_add_exp INTEGER NOT NULL, night_add_master_exp INTEGER NOT NULL, night_add_drop INTEGER NOT NULL, night_add_exc_drop INTEGER NOT NULL, day_add_exp INTEGER NOT NULL, day_add_master_exp INTEGER NOT NULL, day_add_drop INTEGER NOT NULL, day_add_exc_drop INTEGER NOT NULL, off_afk_max_hours INTEGER NOT NULL, daily_coins_price INTEGER NOT NULL, renewal_discount INTEGER NOT NULL, enabled BOOLEAN NOT NULL, daily_money_price INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO VipLevelConfig (id, server_id, type, title, reset_price_discount, reset_points_bonus, grand_reset_price_discount, grand_reset_points_bonus, night_start_hour, night_start_minute, night_add_exp, night_add_master_exp, night_add_drop, night_add_exc_drop, day_add_exp, day_add_master_exp, day_add_drop, day_add_exc_drop, off_afk_max_hours, daily_coins_price, renewal_discount, enabled) SELECT id, server_id, type, title, reset_price_discount, reset_points_bonus, grand_reset_price_discount, grand_reset_points_bonus, night_start_hour, night_start_minute, night_add_exp, night_add_master_exp, night_add_drop, night_add_exc_drop, day_add_exp, day_add_master_exp, day_add_drop, day_add_exc_drop, off_afk_max_hours, daily_coins_price, renewal_discount, enabled FROM __temp__VipLevelConfig');
        $this->addSql('DROP TABLE __temp__VipLevelConfig');
        $this->addSql('CREATE INDEX IDX_7D82554A1844E6B7 ON VipLevelConfig (server_id)');
    }
}
