<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170831195923 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_5DC1D0C0BD138F07');
        $this->addSql('CREATE TEMPORARY TABLE __temp__Server AS SELECT id, database_credential_id, exp, "drop", version, name, description FROM Server');
        $this->addSql('DROP TABLE Server');
        $this->addSql('CREATE TABLE Server (id INTEGER NOT NULL, database_credential_id INTEGER NOT NULL, exp INTEGER NOT NULL, "drop" INTEGER NOT NULL, version VARCHAR(50) NOT NULL COLLATE BINARY, name VARCHAR(50) NOT NULL COLLATE BINARY, description VARCHAR(255) DEFAULT NULL COLLATE BINARY, max_online INTEGER NOT NULL DEFAULT 0, PRIMARY KEY(id), CONSTRAINT FK_5DC1D0C0BD138F07 FOREIGN KEY (database_credential_id) REFERENCES DatabaseCredentials (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO Server (id, database_credential_id, exp, "drop", version, name, description) SELECT id, database_credential_id, exp, "drop", version, name, description FROM __temp__Server');
        $this->addSql('DROP TABLE __temp__Server');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5DC1D0C0BD138F07 ON Server (database_credential_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_5DC1D0C0BD138F07');
        $this->addSql('CREATE TEMPORARY TABLE __temp__Server AS SELECT id, database_credential_id, exp, "drop", version, name, description FROM Server');
        $this->addSql('DROP TABLE Server');
        $this->addSql('CREATE TABLE Server (id INTEGER NOT NULL, database_credential_id INTEGER NOT NULL, exp INTEGER NOT NULL, "drop" INTEGER NOT NULL, version VARCHAR(50) NOT NULL, name VARCHAR(50) NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO Server (id, database_credential_id, exp, "drop", version, name, description) SELECT id, database_credential_id, exp, "drop", version, name, description FROM __temp__Server');
        $this->addSql('DROP TABLE __temp__Server');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5DC1D0C0BD138F07 ON Server (database_credential_id)');
    }
}
