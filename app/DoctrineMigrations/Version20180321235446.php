<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180321235446 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('UPDATE ItemInfo SET buff_effect=\'Critical Damage Increase at 25%\' WHERE id=225;');
        $this->addSql('UPDATE ItemInfo SET buff_effect=\'Excellent Damage Increase at 25%\' WHERE id=100;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('UPDATE ItemInfo SET buff_effect=\'Critical Damage Increase at 100%\' WHERE id=225;');
        $this->addSql('UPDATE ItemInfo SET buff_effect=\'Excellent Damage Increase at 100%\' WHERE id=100;');
    }
}
