<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171027023729 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');


        $this->addSql('CREATE TEMPORARY TABLE __temp__AncientSet AS SELECT id, name, options FROM AncientSet');
        $this->addSql('DROP TABLE AncientSet');
        $this->addSql('CREATE TABLE AncientSet (id INTEGER NOT NULL, name VARCHAR(100) NOT NULL COLLATE BINARY, options CLOB NOT NULL, enabled BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO AncientSet (id, name, options, enabled) SELECT id, name, options, 1 FROM __temp__AncientSet');
        $this->addSql('DROP TABLE __temp__AncientSet');

        $this->addSql('UPDATE AncientSet SET enabled = 0 WHERE id = 27;');
        $this->addSql('UPDATE AncientSet SET enabled = 0 WHERE id = 11;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TEMPORARY TABLE __temp__AncientSet AS SELECT id, name, options FROM AncientSet');
        $this->addSql('DROP TABLE AncientSet');
        $this->addSql('CREATE TABLE AncientSet (id INTEGER NOT NULL, name VARCHAR(100) NOT NULL, options CLOB NOT NULL COLLATE BINARY, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO AncientSet (id, name, options) SELECT id, name, options FROM __temp__AncientSet');
        $this->addSql('DROP TABLE __temp__AncientSet');
    }
}
