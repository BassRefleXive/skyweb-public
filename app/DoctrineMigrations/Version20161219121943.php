<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161219121943 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE ItemGroup (id INTEGER NOT NULL, name VARCHAR(20) NOT NULL, type_id INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BBE5DD825E237E06 ON ItemGroup (name)');
        $this->addSql('DROP INDEX UNIQ_2DA17977E7927C74');
        $this->addSql('DROP INDEX UNIQ_2DA17977AA08CB10');
        $this->addSql('CREATE TEMPORARY TABLE __temp__User AS SELECT id, login, email, password, logged_at, created_at, updated_at, type_id FROM User');
        $this->addSql('DROP TABLE User');
        $this->addSql('CREATE TABLE User (id INTEGER NOT NULL, login VARCHAR(10) NOT NULL COLLATE BINARY, email VARCHAR(255) NOT NULL COLLATE BINARY, password VARCHAR(10) NOT NULL COLLATE BINARY, logged_at DATETIME NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, type_id VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO User (id, login, email, password, logged_at, created_at, updated_at, type_id) SELECT id, login, email, password, logged_at, created_at, updated_at, type_id FROM __temp__User');
        $this->addSql('DROP TABLE __temp__User');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977E7927C74 ON User (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977AA08CB10 ON User (login)');
        $this->addSql('DROP INDEX IDX_758ED6D91844E6B7');
        $this->addSql('DROP INDEX IDX_758ED6D9A76ED395');
        $this->addSql('CREATE TEMPORARY TABLE __temp__UserServerAccount AS SELECT id, user_id, server_id, account_id, selected_at FROM UserServerAccount');
        $this->addSql('DROP TABLE UserServerAccount');
        $this->addSql('CREATE TABLE UserServerAccount (id INTEGER NOT NULL, user_id INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, account_id INTEGER NOT NULL, selected_at DATETIME NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_758ED6D9A76ED395 FOREIGN KEY (user_id) REFERENCES User (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_758ED6D91844E6B7 FOREIGN KEY (server_id) REFERENCES Server (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO UserServerAccount (id, user_id, server_id, account_id, selected_at) SELECT id, user_id, server_id, account_id, selected_at FROM __temp__UserServerAccount');
        $this->addSql('DROP TABLE __temp__UserServerAccount');
        $this->addSql('CREATE INDEX IDX_758ED6D91844E6B7 ON UserServerAccount (server_id)');
        $this->addSql('CREATE INDEX IDX_758ED6D9A76ED395 ON UserServerAccount (user_id)');
        $this->addSql('DROP INDEX UNIQ_5DC1D0C0BD138F07');
        $this->addSql('CREATE TEMPORARY TABLE __temp__Server AS SELECT id, database_credential_id, exp, "drop", version, name, description FROM Server');
        $this->addSql('DROP TABLE Server');
        $this->addSql('CREATE TABLE Server (id INTEGER NOT NULL, database_credential_id INTEGER NOT NULL, exp INTEGER NOT NULL, "drop" INTEGER NOT NULL, version VARCHAR(50) NOT NULL COLLATE BINARY, name VARCHAR(50) NOT NULL COLLATE BINARY, description VARCHAR(255) DEFAULT NULL COLLATE BINARY, PRIMARY KEY(id), CONSTRAINT FK_5DC1D0C0BD138F07 FOREIGN KEY (database_credential_id) REFERENCES DatabaseCredentials (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO Server (id, database_credential_id, exp, "drop", version, name, description) SELECT id, database_credential_id, exp, "drop", version, name, description FROM __temp__Server');
        $this->addSql('DROP TABLE __temp__Server');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5DC1D0C0BD138F07 ON Server (database_credential_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE ItemGroup');
        $this->addSql('DROP INDEX UNIQ_5DC1D0C0BD138F07');
        $this->addSql('CREATE TEMPORARY TABLE __temp__Server AS SELECT id, database_credential_id, exp, "drop", version, name, description FROM Server');
        $this->addSql('DROP TABLE Server');
        $this->addSql('CREATE TABLE Server (id INTEGER NOT NULL, database_credential_id INTEGER NOT NULL, exp INTEGER NOT NULL, "drop" INTEGER NOT NULL, version VARCHAR(50) NOT NULL, name VARCHAR(50) NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO Server (id, database_credential_id, exp, "drop", version, name, description) SELECT id, database_credential_id, exp, "drop", version, name, description FROM __temp__Server');
        $this->addSql('DROP TABLE __temp__Server');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5DC1D0C0BD138F07 ON Server (database_credential_id)');
        $this->addSql('DROP INDEX UNIQ_2DA17977AA08CB10');
        $this->addSql('DROP INDEX UNIQ_2DA17977E7927C74');
        $this->addSql('CREATE TEMPORARY TABLE __temp__User AS SELECT id, login, email, password, logged_at, created_at, updated_at, type_id FROM User');
        $this->addSql('DROP TABLE User');
        $this->addSql('CREATE TABLE User (id INTEGER NOT NULL, login VARCHAR(10) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(10) NOT NULL, logged_at DATETIME NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, type_id VARCHAR(255) NOT NULL COLLATE BINARY, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO User (id, login, email, password, logged_at, created_at, updated_at, type_id) SELECT id, login, email, password, logged_at, created_at, updated_at, type_id FROM __temp__User');
        $this->addSql('DROP TABLE __temp__User');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977AA08CB10 ON User (login)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977E7927C74 ON User (email)');
        $this->addSql('DROP INDEX IDX_758ED6D9A76ED395');
        $this->addSql('DROP INDEX IDX_758ED6D91844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__UserServerAccount AS SELECT id, user_id, server_id, account_id, selected_at FROM UserServerAccount');
        $this->addSql('DROP TABLE UserServerAccount');
        $this->addSql('CREATE TABLE UserServerAccount (id INTEGER NOT NULL, user_id INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, account_id INTEGER NOT NULL, selected_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO UserServerAccount (id, user_id, server_id, account_id, selected_at) SELECT id, user_id, server_id, account_id, selected_at FROM __temp__UserServerAccount');
        $this->addSql('DROP TABLE __temp__UserServerAccount');
        $this->addSql('CREATE INDEX IDX_758ED6D9A76ED395 ON UserServerAccount (user_id)');
        $this->addSql('CREATE INDEX IDX_758ED6D91844E6B7 ON UserServerAccount (server_id)');
    }
}
