<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170124000358 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_E3FD81D840C2021F');
        $this->addSql('CREATE TEMPORARY TABLE __temp__TopVoter AS SELECT id, voter_user_server_account_id, place, reward FROM TopVoter');
        $this->addSql('DROP TABLE TopVoter');
        $this->addSql('CREATE TABLE TopVoter (id INTEGER NOT NULL, voter_user_server_account_id INTEGER DEFAULT NULL, place SMALLINT NOT NULL, reward SMALLINT NOT NULL, regular_votes INTEGER NOT NULL, sms_votes INTEGER NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_E3FD81D840C2021F FOREIGN KEY (voter_user_server_account_id) REFERENCES UserServerAccount (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO TopVoter (id, voter_user_server_account_id, place, reward, regular_votes, sms_votes) SELECT id, voter_user_server_account_id, place, reward, 0, 0 FROM __temp__TopVoter');
        $this->addSql('DROP TABLE __temp__TopVoter');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E3FD81D840C2021F ON TopVoter (voter_user_server_account_id)');
        $this->addSql('DROP INDEX IDX_BDE1366E1844E6B7');
        $this->addSql('DROP INDEX IDX_BDE1366EA76ED395');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_E3FD81D840C2021F');
        $this->addSql('CREATE TEMPORARY TABLE __temp__TopVoter AS SELECT id, voter_user_server_account_id, place, reward FROM TopVoter');
        $this->addSql('DROP TABLE TopVoter');
        $this->addSql('CREATE TABLE TopVoter (id INTEGER NOT NULL, voter_user_server_account_id INTEGER DEFAULT NULL, place SMALLINT NOT NULL, reward SMALLINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO TopVoter (id, voter_user_server_account_id, place, reward) SELECT id, voter_user_server_account_id, place, reward FROM __temp__TopVoter');
        $this->addSql('DROP TABLE __temp__TopVoter');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E3FD81D840C2021F ON TopVoter (voter_user_server_account_id)');
    }
}
