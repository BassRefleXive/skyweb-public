<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180216154816 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE skill_character_classes (skill_id INTEGER NOT NULL, character_class_id INTEGER NOT NULL, PRIMARY KEY(skill_id, character_class_id))');
        $this->addSql('CREATE INDEX IDX_AE7296E05585C142 ON skill_character_classes (skill_id)');
        $this->addSql('CREATE INDEX IDX_AE7296E0B201E281 ON skill_character_classes (character_class_id)');


        $this->addSql('UPDATE CharacterClass SET id=1, name=\'Dark Knight\', "type"=16 WHERE abbreviation=\'DK\';');
        $this->addSql('UPDATE CharacterClass SET id=2, name=\'Blade Knight\', "type"=17 WHERE abbreviation=\'BK\';');
        $this->addSql('UPDATE CharacterClass SET id=3, name=\'Blade Master\', "type"=18 WHERE abbreviation=\'BM\';');
        $this->addSql('UPDATE CharacterClass SET id=4, name=\'Dark Wizard\', "type"=0 WHERE abbreviation=\'DW\';');
        $this->addSql('UPDATE CharacterClass SET id=5, name=\'Soul Master\', "type"=1 WHERE abbreviation=\'SM\';');
        $this->addSql('UPDATE CharacterClass SET id=6, name=\'Grand Master\', "type"=2 WHERE abbreviation=\'GM\';');
        $this->addSql('UPDATE CharacterClass SET id=7, name=\'Elf\', "type"=32 WHERE abbreviation=\'ELF\';');
        $this->addSql('UPDATE CharacterClass SET id=8, name=\'Muse Elf\', "type"=33 WHERE abbreviation=\'ME\';');
        $this->addSql('UPDATE CharacterClass SET id=9, name=\'High Elf\', "type"=34 WHERE abbreviation=\'HE\';');
        $this->addSql('UPDATE CharacterClass SET id=10, name=\'Magic Gladiator\', "type"=48 WHERE abbreviation=\'MG\';');
        $this->addSql('UPDATE CharacterClass SET id=11, name=\'Duel Master\', "type"=50 WHERE abbreviation=\'DM\';');
        $this->addSql('UPDATE CharacterClass SET id=12, name=\'Dark Lord\', "type"=64 WHERE abbreviation=\'DL\';');
        $this->addSql('UPDATE CharacterClass SET id=13, name=\'Lord Emperror\', "type"=66 WHERE abbreviation=\'LE\';');
        $this->addSql('UPDATE CharacterClass SET id=14, name=\'Summoner\', "type"=80 WHERE abbreviation=\'SUM\';');
        $this->addSql('UPDATE CharacterClass SET id=15, name=\'Bloody Summoner\', "type"=81 WHERE abbreviation=\'BS\';');
        $this->addSql('UPDATE CharacterClass SET id=16, name=\'Dimension Master\', "type"=82 WHERE abbreviation=\'DimM\';');
        $this->addSql('UPDATE CharacterClass SET id=17, name=\'Rage Fighter\', "type"=96 WHERE abbreviation=\'RF\'; ');
        $this->addSql('UPDATE CharacterClass SET id=18, name=\'Fist Master\', "type"=98 WHERE abbreviation=\'FM\';');




    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE skill_character_classes');
    }
}
