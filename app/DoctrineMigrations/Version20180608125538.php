<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180608125538 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX login_idx');
        $this->addSql('CREATE TEMPORARY TABLE __temp__UserAuthLog AS SELECT id, user_agent, ip_address, login, password, success, created_at, updated_at FROM UserAuthLog');
        $this->addSql('DROP TABLE UserAuthLog');
        $this->addSql('CREATE TABLE UserAuthLog (id INTEGER NOT NULL, user_agent VARCHAR(256) NOT NULL COLLATE BINARY, ip_address INTEGER NOT NULL, login VARCHAR(256) NOT NULL COLLATE BINARY, password VARCHAR(256) NOT NULL COLLATE BINARY, success BOOLEAN NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO UserAuthLog (id, user_agent, ip_address, login, password, success, created_at, updated_at) SELECT id, user_agent, ip_address, login, password, success, created_at, updated_at FROM __temp__UserAuthLog');
        $this->addSql('DROP TABLE __temp__UserAuthLog');
        $this->addSql('CREATE INDEX login_idx ON UserAuthLog (login)');
        $this->addSql('CREATE INDEX last_login_ip_INDEX ON UserAuthLog (login, ip_address, success, created_at)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX login_idx');
        $this->addSql('DROP INDEX last_login_ip_INDEX');
        $this->addSql('CREATE TEMPORARY TABLE __temp__UserAuthLog AS SELECT id, user_agent, ip_address, login, password, success, created_at, updated_at FROM UserAuthLog');
        $this->addSql('DROP TABLE UserAuthLog');
        $this->addSql('CREATE TABLE UserAuthLog (id INTEGER NOT NULL, user_agent VARCHAR(256) NOT NULL, ip_address INTEGER NOT NULL, login VARCHAR(256) NOT NULL, password VARCHAR(256) NOT NULL, success BOOLEAN NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO UserAuthLog (id, user_agent, ip_address, login, password, success, created_at, updated_at) SELECT id, user_agent, ip_address, login, password, success, created_at, updated_at FROM __temp__UserAuthLog');
        $this->addSql('DROP TABLE __temp__UserAuthLog');
        $this->addSql('CREATE INDEX login_idx ON UserAuthLog (login)');
    }
}
