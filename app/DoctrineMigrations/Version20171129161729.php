<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171129161729 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_C6AAADDD1844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__AccountMenuConfig AS SELECT id, server_id, nick_change_price, class_change_price, reset_stats_price, online_hours_rate FROM AccountMenuConfig');
        $this->addSql('DROP TABLE AccountMenuConfig');
        $this->addSql('CREATE TABLE AccountMenuConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, nick_change_price INTEGER NOT NULL, class_change_price INTEGER NOT NULL, reset_stats_price INTEGER NOT NULL, hide_info_daily_rate INTEGER NOT NULL, online_hours_rate INTEGER NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_C6AAADDD1844E6B7 FOREIGN KEY (server_id) REFERENCES Server (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO AccountMenuConfig (id, server_id, nick_change_price, class_change_price, reset_stats_price, online_hours_rate, hide_info_daily_rate) SELECT id, server_id, nick_change_price, class_change_price, reset_stats_price, online_hours_rate, 20 FROM __temp__AccountMenuConfig');
        $this->addSql('DROP TABLE __temp__AccountMenuConfig');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C6AAADDD1844E6B7 ON AccountMenuConfig (server_id)');

        $this->addSql('DROP INDEX IDX_758ED6D9A76ED395');
        $this->addSql('DROP INDEX IDX_758ED6D91844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__UserServerAccount AS SELECT id, user_id, server_id, account_id, selected_at, spent_money, spent_credits, purchased_stats FROM UserServerAccount');
        $this->addSql('DROP TABLE UserServerAccount');
        $this->addSql('CREATE TABLE UserServerAccount (id INTEGER NOT NULL, user_id INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, account_id VARCHAR(10) NOT NULL COLLATE BINARY, selected_at DATETIME NOT NULL, spent_money DOUBLE PRECISION NOT NULL, spent_credits INTEGER NOT NULL, purchased_stats INTEGER NOT NULL, hidden_till DATETIME DEFAULT NULL, PRIMARY KEY(id), CONSTRAINT FK_758ED6D9A76ED395 FOREIGN KEY (user_id) REFERENCES User (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_758ED6D91844E6B7 FOREIGN KEY (server_id) REFERENCES Server (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO UserServerAccount (id, user_id, server_id, account_id, selected_at, spent_money, spent_credits, purchased_stats) SELECT id, user_id, server_id, account_id, selected_at, spent_money, spent_credits, purchased_stats FROM __temp__UserServerAccount');
        $this->addSql('DROP TABLE __temp__UserServerAccount');
        $this->addSql('CREATE INDEX IDX_758ED6D9A76ED395 ON UserServerAccount (user_id)');
        $this->addSql('CREATE INDEX IDX_758ED6D91844E6B7 ON UserServerAccount (server_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_758ED6D9A76ED395');
        $this->addSql('DROP INDEX IDX_758ED6D91844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__UserServerAccount AS SELECT id, user_id, server_id, account_id, selected_at, spent_money, spent_credits, purchased_stats FROM UserServerAccount');
        $this->addSql('DROP TABLE UserServerAccount');
        $this->addSql('CREATE TABLE UserServerAccount (id INTEGER NOT NULL, user_id INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, account_id VARCHAR(10) NOT NULL, selected_at DATETIME NOT NULL, spent_money DOUBLE PRECISION NOT NULL, spent_credits INTEGER NOT NULL, purchased_stats INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO UserServerAccount (id, user_id, server_id, account_id, selected_at, spent_money, spent_credits, purchased_stats) SELECT id, user_id, server_id, account_id, selected_at, spent_money, spent_credits, purchased_stats FROM __temp__UserServerAccount');
        $this->addSql('DROP TABLE __temp__UserServerAccount');
        $this->addSql('CREATE INDEX IDX_758ED6D9A76ED395 ON UserServerAccount (user_id)');
        $this->addSql('CREATE INDEX IDX_758ED6D91844E6B7 ON UserServerAccount (server_id)');

        $this->addSql('DROP INDEX UNIQ_C6AAADDD1844E6B7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__AccountMenuConfig AS SELECT id, server_id, nick_change_price, class_change_price, reset_stats_price, online_hours_rate FROM AccountMenuConfig');
        $this->addSql('DROP TABLE AccountMenuConfig');
        $this->addSql('CREATE TABLE AccountMenuConfig (id INTEGER NOT NULL, server_id INTEGER DEFAULT NULL, nick_change_price INTEGER NOT NULL, class_change_price INTEGER NOT NULL, reset_stats_price INTEGER NOT NULL, online_hours_rate INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO AccountMenuConfig (id, server_id, nick_change_price, class_change_price, reset_stats_price, online_hours_rate) SELECT id, server_id, nick_change_price, class_change_price, reset_stats_price, online_hours_rate FROM __temp__AccountMenuConfig');
        $this->addSql('DROP TABLE __temp__AccountMenuConfig');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C6AAADDD1844E6B7 ON AccountMenuConfig (server_id)');
    }
}
