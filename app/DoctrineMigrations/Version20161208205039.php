<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161208205039 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE User (id INTEGER NOT NULL, login VARCHAR(10) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(10) NOT NULL, logged_at DATETIME NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, type_id VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977AA08CB10 ON User (login)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DA17977E7927C74 ON User (email)');
        $this->addSql('CREATE TABLE UserServerAccount (id INTEGER NOT NULL, user_id INTEGER DEFAULT NULL, server_id INTEGER DEFAULT NULL, account_id INTEGER NOT NULL, selected_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_758ED6D9A76ED395 ON UserServerAccount (user_id)');
        $this->addSql('CREATE INDEX IDX_758ED6D91844E6B7 ON UserServerAccount (server_id)');
        $this->addSql('CREATE TABLE Server (id INTEGER NOT NULL, database_credential_id INTEGER NOT NULL, exp INTEGER NOT NULL, "drop" INTEGER NOT NULL, version VARCHAR(50) NOT NULL, name VARCHAR(50) NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5DC1D0C0BD138F07 ON Server (database_credential_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE User');
        $this->addSql('DROP TABLE UserServerAccount');
        $this->addSql('DROP TABLE Server');
    }
}
