<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171212193922 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_E3FD81D840C2021F');
        $this->addSql('CREATE TEMPORARY TABLE __temp__TopVoter AS SELECT id, voter_user_server_account_id, place, reward, regular_votes, sms_votes FROM TopVoter');
        $this->addSql('DROP TABLE TopVoter');
        $this->addSql('CREATE TABLE TopVoter (id INTEGER NOT NULL, voter_user_server_account_id INTEGER DEFAULT NULL, place SMALLINT NOT NULL, reward SMALLINT NOT NULL, regular_votes INTEGER NOT NULL, sms_votes INTEGER NOT NULL, period_start DATETIME NOT NULL, period_end DATETIME NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_E3FD81D840C2021F FOREIGN KEY (voter_user_server_account_id) REFERENCES UserServerAccount (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO TopVoter (id, voter_user_server_account_id, place, reward, regular_votes, sms_votes, period_start, period_end) SELECT id, voter_user_server_account_id, place, reward, regular_votes, sms_votes, "2017-11-01 00:00:00", "2017-11-30 23:59:59" FROM __temp__TopVoter');
        $this->addSql('DROP TABLE __temp__TopVoter');
        $this->addSql('CREATE INDEX IDX_E3FD81D840C2021F ON TopVoter (voter_user_server_account_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TEMPORARY TABLE __temp__TopVoter AS SELECT id, voter_user_server_account_id, place, regular_votes, sms_votes, reward, period_start, period_end FROM TopVoter');
        $this->addSql('DROP TABLE TopVoter');
        $this->addSql('CREATE TABLE TopVoter (id INTEGER NOT NULL, voter_user_server_account_id INTEGER DEFAULT NULL, place SMALLINT NOT NULL, regular_votes INTEGER NOT NULL, sms_votes INTEGER NOT NULL, reward SMALLINT NOT NULL, period_start DATETIME NOT NULL, period_end DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO TopVoter (id, voter_user_server_account_id, place, regular_votes, sms_votes, reward, period_start, period_end) SELECT id, voter_user_server_account_id, place, regular_votes, sms_votes, reward, period_start, period_end FROM __temp__TopVoter');
        $this->addSql('DROP TABLE __temp__TopVoter');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E3FD81D840C2021F ON TopVoter (voter_user_server_account_id)');
    }
}
