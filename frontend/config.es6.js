const Encore = require('@symfony/webpack-encore');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const SpritesmithPlugin = require('webpack-spritesmith');

Encore
    .cleanupOutputBeforeBuild()
    .setOutputPath('web/build/')
    .setPublicPath('/build')
    .addEntry('app', './frontend/js/Application.js')
    .enableSassLoader()
    .autoProvidejQuery()
    .configureBabel(function (babelConfig) {
        babelConfig.presets = ["es2015", "stage-0"];
    })
    .addPlugin(
        new CopyWebpackPlugin([
            {from: './frontend/img/buff', to: '../img/buff'},
            {from: './frontend/img/error', to: '../img/error'},
            {from: './frontend/img/faq', to: '../img/faq'},
            {from: './frontend/img/gens', to: '../img/gens'},
            {from: './frontend/img/items', to: '../img/items'},
            {from: './frontend/img/magic_list', to: '../img/magic_list'},
            {from: './frontend/img/payments', to: '../img/payments'},
            {from: './frontend/img/banner', to: '../img/banner'},
            {from: './frontend/img/cash-shop', to: '../img/cash-shop'},
        ])
    )
    .autoProvideVariables({Popper: ['popper.js', 'default']})
    .enableSourceMaps(!Encore.isProduction())
    .enableBuildNotifications()
    .enableVersioning(Encore.isProduction())
;

if (Encore.isProduction()) {
    Encore
        .addPlugin(
            new UglifyJsPlugin({
                test: '/\.js($|\?)/i',
            })
        );
}

Encore.addPlugin(
    new SpritesmithPlugin({
        src: {
            cwd: './frontend/img/template',
            glob: 'sprite/**/*.png'
        },
        target: {
            image: './frontend/img/template/generated.png',
            css: './frontend/scss/sprite.scss',
        },
        apiOptions: {
            cssImageRef: "./frontend/img/template/generated.png"
        }
    })
);


module.exports = Encore.getWebpackConfig();