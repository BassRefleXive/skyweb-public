export default class ErrorException {

    msg = undefined;

    constructor(msg = 'Unknown error has been occurred') {
        this.msg = msg;
    }

    getMessage = () => {
        return this.msg;
    }

}