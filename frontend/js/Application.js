require('../scss/master.scss');


import SmoothScroll from 'smoothscroll-for-websites'
import 'bootstrap';

import ErrorException from './Exceptions/ErrorException'
import IndexController from './Controller/IndexController'
import UsersController from './Controller/UsersController'
import AccountsWarehouseController from './Controller/AccountsWarehouseController'
import AdminNewsController from './Controller/AdminNewsController'
import AdminWebShopController from './Controller/AdminWebShopController'
import AdminDownloadLinksController from './Controller/AdminDownloadLinksController'
import WebShopController from './Controller/WebShopController'
import CartController from './Controller/CartController'
import MarketController from './Controller/MarketController'
import WebStorageController from './Controller/WebStorageController'
import StatsShopController from './Controller/StatsShopController'
import AccountsController from './Controller/AccountsController'
import CharactersController from './Controller/CharactersController'
import WCoinShopController from './Controller/WCoinShopController'
import VipController from './Controller/VipController'
import GensController from './Controller/GensController'
import OrderController from './Controller/OrderController'
import ItemPopupService from './Service/ItemPopupService'
import EventTimingService from './Service/EventTimingService'
import ServersInfoService from './Service/ServersInfoService'
import ChatService from './Service/ChatService'
import ButtonEffectService from './Service/ButtonEffectService'
import TimerService from './Service/TimerService'
import LegacyBrowserWorkaroundService from './Service/LegacyBrowserWorkaroundService'
import FaqService from './Service/FaqService'

const SECTION_INDEX = 'IndexController';
const SECTION_USERS = 'UsersController';
const SECTION_WAREHOUSE = 'AccountsWarehouseController';
const SECTION_ADMIN_NEWS = 'AdminNewsController';
const SECTION_ADMIN_WEBSHOP = 'AdminWebshop.categoryController';
const SECTION_ADMIN_DOWNLOAD_LINKS = 'AdminDownload.linkController';
const SECTION_WEBSHOP = 'WebshopController';
const SECTION_CART = 'CartController';
const SECTION_MARKET = 'MarketController';
const SECTION_WEB_STORAGE = 'Web.storageController';
const SECTION_STATS_SHOP = 'Stats.shopController';
const SECTION_WCOIN_SHOP = 'Wcoin.shopController';
const SECTION_ACCOUNTS = 'AccountsController';
const SECTION_CHARACTERS = 'CharactersController';
const SECTION_GENS = 'GensController';
const SECTION_VIP = 'VipController';
const SECTION_ORDER = 'OrderController';

const KNOWN_CONTROLLERS = {
    [SECTION_ADMIN_NEWS]: AdminNewsController,
    [SECTION_ADMIN_WEBSHOP]: AdminWebShopController,
    [SECTION_ADMIN_DOWNLOAD_LINKS]: AdminDownloadLinksController,

    [SECTION_INDEX]: IndexController,
    [SECTION_USERS]: UsersController,
    [SECTION_WAREHOUSE]: AccountsWarehouseController,
    [SECTION_WEBSHOP]: WebShopController,
    [SECTION_CART]: CartController,
    [SECTION_MARKET]: MarketController,
    [SECTION_WEB_STORAGE]: WebStorageController,
    [SECTION_STATS_SHOP]: StatsShopController,
    [SECTION_ACCOUNTS]: AccountsController,
    [SECTION_CHARACTERS]: CharactersController,
    [SECTION_GENS]: GensController,
    [SECTION_VIP]: VipController,
    [SECTION_WCOIN_SHOP]: WCoinShopController,
    [SECTION_ORDER]: OrderController,
};

class Application {
    init() {
        const sectionData = $('body').data('section') || undefined;

        if (!sectionData) {
            throw new ErrorException('No data provided in section.');
        }

        const sectionParts = sectionData.split('_');

        if (sectionParts.length < 3) {
            throw new ErrorException('Section data is corrupted. Must be at least 3 segments.');
        }

        const action = sectionParts.pop();
        let controller = '';

        for (let i = 1; i < sectionParts.length; i++) {
            controller += sectionParts[i].charAt(0).toUpperCase() + sectionParts[i].slice(1);
        }

        controller += 'Controller';

        if (KNOWN_CONTROLLERS[controller] !== undefined) {
            (new KNOWN_CONTROLLERS[controller](action)).dispatch();
        } else {
            console.warn(`Controller "${controller}" is unknown. No JavaScript controller dispatched.`)
        }

        (new ItemPopupService()).init();
        (new EventTimingService()).init();
        (new ServersInfoService()).init();
        (new ChatService()).init();
        (new ButtonEffectService()).init();
        (new TimerService()).init();
        (new LegacyBrowserWorkaroundService()).init();
        (new FaqService()).init();

        this.initScroll();
    }

    initScroll() {
        SmoothScroll({
            stepSize: 50,
            animationTime: 350,
            pulseScale: 3,
            pulseNormalize: 1,
        });
    }
}

$(function () {
    try {
        (new Application()).init();
    } catch (e) {
        if (e instanceof ErrorException) {
            console.error(e.getMessage());
        } else {
            console.error(e);
        }
    }
});