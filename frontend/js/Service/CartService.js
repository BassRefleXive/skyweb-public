import RestService from './RestService'
import Routing from './Routing'

export default class CartService {
    restService = undefined;

    constructor() {
        this.restService = new RestService();
    }

    getCouponData = coupon => {
        return this.restService.getAction(
            Routing.generate(
                'sky_webshop.discount.coupon_index',
                {
                    coupon,
                }
            ),
            true
        )
            .then(this.restService.processResponse(`Obtain discount coupon data. `, []));
    }
}