export default class ServersInfoService {
    selectors = {
        modal: {
            selector: '#modal-servers-info',
        },
        infoSphere: {
            water: '.server-online-status .water',
            onlineLabel: '.server-online-status .online-label',
            onlinePercent: '.server-online-status [data-progress]',
        }
    };

    init() {
        $(this.selectors.modal.selector).on('show.bs.modal', function (e) {
            $(e.target).find(`.nav-pills a[href="${e.relatedTarget.hash}"]`).tab('show');
        });

        $(this.selectors.infoSphere.onlineLabel).addClass('ready');

        const $water = $(this.selectors.infoSphere.water);
        const onlinePercent = $(this.selectors.infoSphere.onlinePercent).data('progress');

        let percent = 0;

        const interval = setInterval(() => {
            percent++;
            $water.css('transform', `translate(0, ${100 - percent}%`);

            if (percent >= onlinePercent) {
                clearInterval(interval);
            }
        }, 20);
    }
}