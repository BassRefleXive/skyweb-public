export default class SkillPopupService {

    selectors = {
        toggleSkillPopupTrigger: '[data-toggle-skill-popup]',
        skillPopupContainer: '[data-skill-popup]',
    };

    init() {
        const that = this;

        $(this.selectors.toggleSkillPopupTrigger).on('mouseover', ev => {
            const popupContainer = $(ev.currentTarget).find(that.selectors.skillPopupContainer);

            popupContainer.addClass('-opened');
        });

        $(this.selectors.toggleSkillPopupTrigger).on('mouseout', ev => {
            const popupContainer = $(ev.currentTarget).find(that.selectors.skillPopupContainer);

            popupContainer.removeClass('-opened');
        });
    }

}