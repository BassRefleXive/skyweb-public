import 'select2';
import 'select2/dist/css/select2.css';

export default class Select2Service {
    initSelect2($el, placeholder = undefined) {
        const highlightCurrencies = item => {
            let result = item.text;

            if (item.text) {
                const currencies = [
                    {
                        trigger: 'wcoin',
                        class: '-wcoin',
                    },
                    {
                        trigger: 'kk zen',
                        class: '-zen',
                    },
                    {
                        trigger: 'credits',
                        class: '-credits',
                    },
                    {
                        trigger: 'gem',
                        class: '-credits',
                    },
                    {
                        trigger: '\\$',
                        class: '-money',
                    },
                ];

                currencies.map(currency => {
                    const currencyRegexp = new RegExp(currency.trigger, 'ig');
                    const match = result.match(currencyRegexp);
                    if (match && match.length) {
                        result = result.replace(currencyRegexp, `<span class="${currency.class}">${match[0]}</span>`);
                    }
                });

                result = $(`<span>${result}</span>`);
            }

            return result;
        };

        $el.select2({
            minimumResultsForSearch: -1,
            allowClear: true,
            placeholder,
            templateResult: highlightCurrencies,
            templateSelection: highlightCurrencies,
        });
    }
}