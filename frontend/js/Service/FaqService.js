import RestService from './RestService'
import Routing from './Routing'

export default class FaqService {
    restService = undefined;

    selectors = {
        modal: {
            container: '#modal-faq',
            frame: '#modal-faq .faq-content',
            loading: '#modal-faq .loading',
        },
        link: '[data-route]',
    };

    data = {
        initialHeight: undefined,
    };

    constructor() {
        this.restService = new RestService();
    }

    init() {
        $(this.selectors.modal.container).on('click', this.selectors.link, ev => {
            ev.preventDefault();
            ev.stopPropagation();

            loadContent($(ev.target).data('route'), $(ev.target).data('fragment'));
        });

        $(this.selectors.modal.container).on('show.bs.modal', ev => {
            const $relatedTarget = $(ev.relatedTarget);

            let route = $relatedTarget.data('route');

            if (undefined === route) {
                const $currentTarget = $(ev.currentTarget);

                route = $currentTarget.data('route');

                if (undefined === route) {
                    route = 'sky_faq_index';
                }
            }

            loadContent(route, $relatedTarget.data('fragment'));
        });

        $(this.selectors.modal.container).on('hidden.bs.modal', () => {
            $(this.selectors.modal.frame).empty();
        });

        const listenDirectFAQPageOpen = () => {
            const hash = window.location.hash;

            console.log(hash);

            if ('' === hash) {
                return;
            }

            if (-1 === hash.indexOf('faq')) {
                return;
            }

            const faqPage = hash.split('-');

            if (faqPage.length < 2 || undefined === faqPage[1]) {
                return;
            }

            $(this.selectors.modal.container).data('route', faqPage[1]);
            $(this.selectors.modal.container).modal({
                show: true,
            });
        };

        const loadContent = (route, fragment = undefined) => {
            const $loading = $(this.selectors.modal.loading);
            const $container = $(this.selectors.modal.frame);
            $container.fadeTo('fast', 0, () => {
                undefined !== this.data.initialHeight && $container.stop().animate({height: this.data.initialHeight}, 350);
                $loading.addClass('-active');
            });

            this.restService.getAction(Routing.generate(route), true)
                .then(this.restService.processResponse(`Fetch FAQ page ${route}`, []))
                .then(response => {

                    undefined === this.data.initialHeight && (this.data.initialHeight = $container.height());

                    $container.html(response);
                    const autoHeight = $container.css('height', 'auto').prop('scrollHeight');

                    $container.height(this.data.initialHeight);

                    $loading.removeClass('-active');

                    $container.stop().animate({height: autoHeight}, 350, () => {
                        $container.fadeTo('fast', 1, () => {
                            if (undefined !== fragment) {
                                $('.modal').animate({
                                    scrollTop: $container.find(`#${fragment}`).offset().top
                                }, 1000);
                            }
                        });
                    });
                });
        };

        listenDirectFAQPageOpen();
    }
}