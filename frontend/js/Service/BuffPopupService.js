export default class BuffPopupService {

    selectors = {
        toggleGensPopupTrigger: '[data-toggle-buff-popup]',
        gensPopupContainer: '[data-buff-popup]',
    };

    init() {
        const that = this;

        $(this.selectors.toggleGensPopupTrigger).on('mouseover', ev => {
            $(ev.currentTarget).find(that.selectors.gensPopupContainer).addClass('-opened');
        });

        $(this.selectors.toggleGensPopupTrigger).on('mouseout', ev => {
            $(ev.currentTarget).find(that.selectors.gensPopupContainer).removeClass('-opened');
        });
    }

}