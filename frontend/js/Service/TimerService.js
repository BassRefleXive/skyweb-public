import soon from './Soon'
import moment from 'moment'

export default class TimerService {
    selectors = {
        elements: {
            timer: {
                container: '.cabinet-container .timer',
                timer: '.cabinet-container .timer .container',
                title: '.cabinet-container .timer .timer-title',
            },
            serverTime: {
                timer: '.server-time-container .timer .container',
            },
        },
        data: {
            timer: '[data-timer]',
            serverTime: '[data-server-time]',
            locale: '[data-locale]',
        },
    };

    data = {
        timer: undefined,
        serverTime: undefined,
        locale: undefined,
    };

    init() {
        this.initData();
        this.initTimer();
    }

    initTimer() {
        const initTimer = () => {
            $(this.selectors.elements.timer.title).html(this.data.timer.title[this.data.locale]);

            const $timer = $(this.selectors.elements.timer.timer);

            const serverTime = moment(this.data.serverTime);
            const serverTimeZone = serverTime.parseZone().format('ZZ');
            const time = moment.unix((new Date(this.data.timer.time)).getTime() / 1000).utcOffset(serverTimeZone);

            $timer.soon().create({
                due: time.toISOString(true),
                now: this.data.serverTime,
                separateChars: false,
                format: "d,h,m,s",
                layout: 'group overlap',
                face: 'slot doctor glow',
                padding: 'false',
                scaleHide: 'none',
                scale: 'fill',
                visual: 'ring color-light width-thin progress-shadow-soft cap-round length-70 gap-0 offset-65',

                labelsDays: $timer.data('label-days'),
                labelsHours: $timer.data('label-hours'),
                labelsMinutes: $timer.data('label-minutes'),
                labelsSeconds: $timer.data('label-seconds'),
            });
        };

        const initServerTime = () => {
            const $timer = $(this.selectors.elements.serverTime.timer);


            const serverTime = moment(this.data.serverTime).parseZone();

            $timer.soon().create({
                since: serverTime
                    .clone()
                    .add(serverTime.hour(), 'hour')
                    .add(serverTime.minute(), 'minute')
                    .add(serverTime.second(), 'second')
                    .add(serverTime.millisecond(), 'millisecond')
                    .toISOString(true),
                now: serverTime
                    .toISOString(true),
                separateChars: false,
                format: "h,m,s",
                layout: 'group label-hidden',
                face: 'slot slide down',
                padding: true,
                paddingDays: '00',
                paddingHours: '00',
                paddingMinutes: '00',
                paddingSeconds: '00',
                scaleHide: 'none',
                scale: 'fill',
                separator: ':',
            });
        };

        if (Object.keys(this.data.timer).length) {
            initTimer();
        }

        initServerTime();
    }

    initData() {
        this.data.timer = $(this.selectors.data.timer).data('timer');
        this.data.serverTime = $(this.selectors.data.serverTime).data('server-time');
        this.data.locale = $(this.selectors.data.locale).data('locale');
    }
}