export default class ItemPopupService {

    selectors = {
        toggleItemPopupTrigger: '[data-toggle-item-popup]',
        itemPopupContainer: '[data-item-popup]',
        baseZIndex: '[data-base-z-index]',
    };

    init() {
        const that = this;

        $(this.selectors.toggleItemPopupTrigger).on('mouseover', ev => {
            $(ev.currentTarget).find(that.selectors.itemPopupContainer).addClass('-opened');

            $(ev.currentTarget).find(that.selectors.itemPopupContainer).closest('.items_item').css(
                'z-index',
                2000 - $(ev.currentTarget).find(that.selectors.itemPopupContainer).closest(this.selectors.baseZIndex).data('base-z-index')
            );
        });

        $(this.selectors.toggleItemPopupTrigger).on('mouseout', ev => {
            $(ev.currentTarget).find(that.selectors.itemPopupContainer).removeClass('-opened');

            setTimeout(() => {
                $(ev.currentTarget).find(that.selectors.itemPopupContainer).closest('.items_item').css(
                    'z-index',
                    $(ev.currentTarget).find(that.selectors.itemPopupContainer).closest(this.selectors.baseZIndex).data('base-z-index')
                );
            }, 350);
        });
    }

}