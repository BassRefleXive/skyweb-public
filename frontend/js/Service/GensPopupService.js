export default class GensPopupService {

    selectors = {
        toggleGensPopupTrigger: '[data-toggle-gens-popup]',
        gensPopupContainer: '[data-gens-popup]',
    };

    init() {
        const that = this;

        $(this.selectors.toggleGensPopupTrigger).on('mouseover', ev => {
            $(ev.currentTarget).find(that.selectors.gensPopupContainer).addClass('-opened');
        });

        $(this.selectors.toggleGensPopupTrigger).on('mouseout', ev => {
            $(ev.currentTarget).find(that.selectors.gensPopupContainer).removeClass('-opened');
        });
    }

}