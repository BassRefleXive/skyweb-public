export default class ButtonEffectService {
    selectors = {
        button: '.button',
        ripple: '.ripple',
    };

    init() {
        const that = this;

        $(this.selectors.button).click(ev => {
            $(that.selectors.ripple).remove();

            const posX = $(ev.target).offset().left;
            const posY = $(ev.target).offset().top;
            let buttonWidth = $(ev.target).width();
            let buttonHeight = $(ev.target).height();

            $(ev.target).prepend('<span class="ripple"></span>');

            if (buttonWidth >= buttonHeight) {
                buttonHeight = buttonWidth;
            } else {
                buttonWidth = buttonHeight;
            }
            const x = ev.pageX - posX - buttonWidth / 2;
            const y = ev.pageY - posY - buttonHeight / 2;


            // Add the ripples CSS and start the animation
            $(that.selectors.ripple).css({
                width: buttonWidth,
                height: buttonHeight,
                top: y + 'px',
                left: x + 'px'
            }).addClass('rippleEffect');
        });
    }

}