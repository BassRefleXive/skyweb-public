export default class ChatService {
    selectors = {
        data: '[data-chat]',
    };

    init() {
        // this.loader($(this.selectors.data).data('chat'));
    }

    loader(chat) {
        var params = {
            embedChatsParameters: [chat],
            needLoadCode: typeof Chatbro === 'undefined'
        };

        var xhr = new XMLHttpRequest();
        xhr.withCredentials = true;
        xhr.onload = function () {
            var script = document.createElement('script');
            script.textContent = xhr.responseText;
            document.head.appendChild(script);
        };
        xhr.onerror = function () {
            console.error('Chatbro loading error')
        };
        xhr.open('POST', '//www.chatbro.com/embed_chats/', true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send('parameters=' + encodeURIComponent(JSON.stringify(params)))
    }
}