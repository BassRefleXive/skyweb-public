export default class LegacyBrowserWorkaroundService {

    selectors = {
        form: {
            borderedInput: 'form .input-control.-bordered input, form .input-control.-bordered textarea',
        },
    };

    init() {
        this.formBorderedInputFocusHighlight();
    }

    formBorderedInputFocusHighlight() {
        $(this.selectors.form.borderedInput).on('focusin', el => {
            const $target = $(el.target);

            $target.parent().append(`<i class="-before"></i><i class="-after"></i>`);
            setTimeout(() => {
                $target.parent().addClass('-focus');
            }, 10);
        });

        $(this.selectors.form.borderedInput).on('focusout', el => {
            const $target = $(el.target);

            $target.parent().removeClass('-focus');
            setTimeout(() => {
                $target.parent().find('i').remove();
            }, 200);
        });
    }
}