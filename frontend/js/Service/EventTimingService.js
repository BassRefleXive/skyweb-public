import soon from './Soon'
import parser from 'cron-parser'
import moment from 'moment'

export default class EventTimingService {
    selectors = {
        container: {
            selector: '#event-timing-container',
            data: '[data-events]',
        },
        event: {
            ordinary: '#event-timing-ordinary-event',
            withLink: '#event-timing-link-event',
            title: '.title',
            container: '.event-container',
            currentEventContainer: '.current-event-container',
        },
        block: {
            selector: '.event-timing',
        },
        server_time: '[data-server-time]',
        server_time_zone: '[data-server-time-zone]',
        slider: {
            slide: '.events-container .events-slide',
            controlsContainer: '.events-container .slider-controls',
            prevSlideTrigger: '.events-container [data-trigger-prev-events]',
            nextSlideTrigger: '.events-container [data-trigger-next-events]',
        },
    };

    templates = {
        container: undefined,
        linkEvent: undefined,
        ordinaryEvent: undefined,
    };

    data = {
        events: undefined,
        serverTime: undefined,
        serverTimeZone: undefined,
        slider: {
            slides: {
                count: undefined,
                current: undefined,
            },
        }
    };

    init() {
        const $container = $(this.selectors.container.selector);

        this.templates.container = $container.html();
        this.templates.linkEvent = $(this.selectors.event.withLink).html();
        this.templates.ordinaryEvent = $(this.selectors.event.ordinary).html();

        this.data.events = $container.data('events');
        this.initServerTime();

        $(this.selectors.block.selector).html(this.render());
        $(this.selectors.block.selector).find('.timer').soon().redraw();

        this.initSlider();
    };

    initSlider() {
        const $slides = $(this.selectors.slider.slide);

        this.data.slider.slides.count = $slides.length;
        this.data.slider.slides.current = 0;


        $(this.selectors.slider.nextSlideTrigger).on('click', ev => {
            ev.preventDefault();

            switchSlide(nextSlide());
        });

        $(this.selectors.slider.prevSlideTrigger).on('click', ev => {
            ev.preventDefault();

            switchSlide(prevSlide());
        });

        const nextSlide = () => {

            let nextSlide = this.data.slider.slides.current + 1;

            if (nextSlide > this.data.slider.slides.count) {
                nextSlide = 1;
            }

            return nextSlide;
        };

        const prevSlide = () => {
            let nextSlide = this.data.slider.slides.current - 1;

            if (nextSlide < 1) {
                nextSlide = this.data.slider.slides.count;
            }

            return nextSlide;
        };

        const switchSlide = slide => {
            const $currentSlide = $slides.siblings(`[data-slide="${this.data.slider.slides.current}"]`);

            $currentSlide.removeClass('-active');
            $currentSlide.addClass('-fly-away');

            const $controlsContainer = $(this.selectors.slider.controlsContainer);
            $controlsContainer.addClass('-fly-away');

            setTimeout(() => {
                $currentSlide.removeClass('-fly-away');
                $controlsContainer.removeClass('-fly-away');
            }, 1200);

            setTimeout(() => {
                $slides.siblings(`[data-slide="${slide}"]`).addClass('-active');
            }, 350);

            this.data.slider.slides.current = slide;
        };

        switchSlide(nextSlide());
    }

    initServerTime() {
        this.data.serverTime = $(this.selectors.server_time).data('server-time');
        this.data.serverTimeZone = $(this.selectors.server_time_zone).data('server-time-zone');
    }

    prepareEventTimers() {
        return this.data.events.map(event => {
            const eventOccurrences = this.getClosestEventOccurrences(event);

            let markup = $(this.templates.ordinaryEvent);

            if (event.hasOwnProperty('url')) {
                markup = $(this.templates.linkEvent);

                markup.find(this.selectors.event.currentEventContainer).data('route', event.url);
            }

            markup.find(this.selectors.event.title).text(event.name);
            const $timer = markup.find('.timer');

            $timer.soon().create({
                due: eventOccurrences[0].toISOString(true),
                now: this.data.serverTime,
                separateChars: false,
                format: "d,h,m,s",
                layout: 'inline',
                face: 'slot fade',
                scaleMax: 'xs',
                scaleHide: 'none',

                labelsDays: $timer.data('label-days'),
                labelsHours: $timer.data('label-hours'),
                labelsMinutes: $timer.data('label-minutes'),
                labelsSeconds: $timer.data('label-seconds'),

                eventTick: (milliseconds, due) => {
                    // 5 minutes in milliseconds
                    const highlightTime = 5 * 60 * 1000;

                    if (milliseconds <= highlightTime) {
                        $timer.addClass('-soon');
                    }
                }
            });

            return markup;
        });
    };

    getClosestEventOccurrences(event) {
        let eventOccurrences = [];

        const serverTime = moment(this.data.serverTime);
        const serverTimeStamp = serverTime.format('X');
        const serverTimeZone = serverTime.parseZone().format('ZZ');

        event.timing.map(timing => {
            eventOccurrences = eventOccurrences.concat(
                parser
                    .parseExpression(
                        timing,
                        {
                            currentDate: serverTime.format('YYYY-MM-DD HH:mm:ss'),
                            tz: this.data.serverTimeZone
                        }
                    )
                    .next()
            );
        });

        return eventOccurrences
            .sort((a, b) => {
                return moment.unix(a._getUTC()).isAfter(moment.unix(b._getUTC())) ? 1 : -1;
            })
            .map(eventOccurrence => {
                return moment.unix(eventOccurrence.getTime() / 1000).utcOffset(serverTimeZone);
            })
            .filter(eventOccurrence => {
                return eventOccurrence.isAfter(moment.unix(serverTimeStamp).utcOffset(serverTimeZone));
            })
            .slice(0, 1);
    }

    render() {
        let $result = $(this.templates.container);

        let slide = 0;

        this.prepareEventTimers().map((timer, idx) => {
            if (idx % 7 === 0) {
                slide++;

                let $subContainer = $(`<div class="events-slide" data-slide="${slide}"></div>`);

                $result.append($subContainer);
            }

            $result.find('.events-slide').last().append(timer);
        });

        return $result;
    }
}