export default class RestService {

    postAction(endpointURI, data = {}, withCredentials = false, withHeaders = undefined) {
        const reqHeaders = withHeaders !== undefined
            ? withHeaders
            : this.getHeaders();

        return fetch(
            endpointURI,
            Object.assign(
                {
                    method: 'POST',
                    headers: reqHeaders,
                    mode: 'cors',
                    cache: 'default',
                    body: JSON.stringify(data)
                },
                withCredentials ? {credentials: 'include'} : {}
            )
        )
    }

    getAction(endpointURI, withCredentials = false) {
        const reqHeaders = this.getHeaders();

        return fetch(
            endpointURI,
            Object.assign({
                    method: 'GET',
                    headers: reqHeaders,
                    mode: 'cors',
                    cache: 'default'
                },
                withCredentials ? {credentials: 'include'} : {}
            )
        )
    }

    // update | crUd
    putAction(endpointURI, data = {}, withCredentials = false) {
        var reqHeaders = new Headers();
        reqHeaders.append('Content-Type', 'application/json; charset=UTF-8');
        reqHeaders.append('Accept', 'application/json');

        return fetch(
            endpointURI,
            Object.assign(
                {
                    method: 'PUT',
                    headers: reqHeaders,
                    mode: 'cors',
                    cache: 'default',
                    body: JSON.stringify(data)
                },
                withCredentials ? {credentials: 'include'} : {}
            )
        )
    }

    // delete | cruD
    deleteAction(endpointURI, withCredentials = false) {
        var reqHeaders = new Headers();
        reqHeaders.append('Content-Type', 'application/json; charset=UTF-8');
        reqHeaders.append('Accept', 'application/json');

        return fetch(
            endpointURI,
            Object.assign(
                {
                    method: 'DELETE',
                    headers: reqHeaders,
                    mode: 'cors'
                },
                withCredentials ? {credentials: 'include'} : {}
            )
        )
    }

    processResponse(name, defaultData = {}) {
        return (response) => {
            if (!response || !response.ok) {
                throw `${name} ${(response.statusText && response.statusText)}`;
            }

            if (false === response.headers.has('content-type')) {
                switch (response.status) {
                    case 204:
                        return response.text();
                    default:
                        return response.json();
                }
            } else {
                const contentType = response.headers.get('content-type');

                return contentType.indexOf('application/json') >= 0
                    ? response.json()
                    : response.text();

            }
        }
    }

    getHeaders() {
        var reqHeaders = new Headers();
        reqHeaders.append('Content-Type', 'application/json; charset=UTF-8');
        reqHeaders.append('Accept', 'application/json');

        return reqHeaders;
    }
}