import CartService from './../Service/CartService'

export default class CartController {

    action = undefined;

    cartService = undefined;

    selectors = {
        index: {
            totalCredits: '.cart [data-total-price-credits]',
            totalMoney: '.cart [data-total-price-money]',
            typeCredits: '.cart [data-type-credits]',
            typeMoney: '.cart [data-type-money]',
            couponTypeCredits: '.cart [data-coupon-type-credits]',
            couponTypeMoney: '.cart [data-coupon-type-money]',
            triggerBuyCredits: '.cart [data-trigger-buy-credits]',
            triggerBuyMoney: '.cart [data-trigger-buy-money]',
            buyTypeInput: '.cart [data-type-input]',
            couponInput: '.cart [data-coupon-input]',
            priceCreditsInput: '.cart [data-price-credits-input]',
            priceMoneyInput: '.cart [data-price-money-input]',
            persistentDiscountCredits: '.cart [data-persistent-discount-credits]',
            persistentDiscountMoney: '.cart [data-persistent-discount-money]',
            itemCredits: '.cart .item-price-credits',
            itemMoney: '.cart .item-price-money',
        },
    };

    data = {
        index: {
            typeCredits: undefined,
            typeMoney: undefined,
            couponTypeCredits: undefined,
            couponTypeMoney: undefined,
            totalPriceCredits: undefined,
            totalPriceMoney: undefined,
            buyType: undefined,
            coupon: undefined,
            totalPriceCreditsCalculated: undefined,
            totalPriceMoneyCalculated: undefined,
            persistentDiscountCredits: undefined,
            persistentDiscountMoney: undefined,
        },
    };

    constructor(action) {
        this.action = action;
        this.cartService = new CartService();
    }

    dispatch = () => {
        switch (this.action) {
            case 'index': {
                this.indexAction();
                break;
            }
        }
    };

    indexAction = () => {
        const that = this;

        $(this.selectors.index.triggerBuyCredits).on('click', () => {
            setBuyType(that.data.index.typeCredits);
        });

        $(this.selectors.index.triggerBuyMoney).on('click', () => {
            setBuyType(that.data.index.typeMoney);
        });

        $(this.selectors.index.couponInput).on('change', ev => {
            const $target = $(ev.target);
            const coupon = $(ev.target).val();
            if (coupon.length === 5) {
                processCoupon($target);
            } else {
                this.data.index.coupon = undefined;
                $target.val('');
                updateTotalPrices();
            }
        });

        const processCoupon = ($target) => {
            that.cartService.getCouponData($target.val())
                .then(response => {
                    this.data.index.coupon = response;
                    updateTotalPrices();
                })
                .catch(error => {
                    this.data.index.coupon = undefined;
                    $target.val('');
                    updateTotalPrices();
                });
        };


        const updateTotalPrices = () => {
            this.data.index.totalPriceCredits = 0;
            this.data.index.totalPriceMoney = 0;

            $(this.selectors.index.itemCredits).map((idx, item) => {
                const $item = $(item);
                const price = $item.data('item-price');

                this.data.index.totalPriceCredits += this.data.index.persistentDiscountCredits && $item.data('persistent-discount-can-be-applied')
                    ? Math.ceil(getDiscountedPrice(price, this.data.index.persistentDiscountCredits))
                    : price;
            });

            $(this.selectors.index.itemMoney).map((idx, item) => {
                const $item = $(item);
                const price = $item.data('item-price');

                this.data.index.totalPriceMoney += this.data.index.persistentDiscountMoney && $item.data('persistent-discount-can-be-applied')
                    ? Math.ceil(getDiscountedPrice(price, this.data.index.persistentDiscountMoney) * 100) / 100
                    : Math.round(price * 100) / 100;

                this.data.index.totalPriceMoney = Math.round(this.data.index.totalPriceMoney * 100) / 100;
            });


            this.data.index.totalPriceCreditsCalculated = this.data.index.totalPriceCredits;
            this.data.index.totalPriceMoneyCalculated = this.data.index.totalPriceMoney;

            const creditsDiscount = getDiscount(this.data.index.couponTypeCredits);

            this.data.index.totalPriceCreditsCalculated = Math.ceil(getDiscountedPrice(this.data.index.totalPriceCredits, creditsDiscount));

            const moneyDiscount = getDiscount(this.data.index.couponTypeMoney);

            this.data.index.totalPriceMoneyCalculated = Math.ceil(getDiscountedPrice(this.data.index.totalPriceMoney, moneyDiscount) * 100) / 100;

            updateTotalPricesHtml(this.data.index.couponTypeCredits, creditsDiscount);
            updateTotalPricesHtml(this.data.index.couponTypeMoney, moneyDiscount);
        };

        const getDiscount = (priceType) => {
            return this.data.index.coupon && this.data.index.coupon.type === priceType
                ? this.data.index.coupon.discount
                : 0;
        };

        const getDiscountedPrice = (price, discount) => {
            return Math.ceil(price * (100 - discount)) / 100;
        };

        const getPriceText = (priceBeforeDiscount, priceAfterDiscount, discount) => {

            if (Number.parseFloat(priceBeforeDiscount) === Number.parseFloat(priceAfterDiscount)) {
                return `<span class="regular-price">${priceBeforeDiscount}</span>`;
            } else {
                return `<span class="regular-price">${priceAfterDiscount}</span> <span class="discount-description">(${priceBeforeDiscount} - ${discount}%)</span>`;
            }
        };

        const updateTotalPricesHtml = (priceType, discount) => {
            switch (priceType) {
                case this.data.index.couponTypeCredits: {
                    $(this.selectors.index.totalCredits).html(
                        getPriceText(this.data.index.totalPriceCredits, this.data.index.totalPriceCreditsCalculated, discount)
                    );
                    $(this.selectors.index.priceCreditsInput).val(this.data.index.totalPriceCreditsCalculated);
                    break;
                }
                case this.data.index.couponTypeMoney: {
                    $(this.selectors.index.totalMoney).html(
                        getPriceText(this.data.index.totalPriceMoney, this.data.index.totalPriceMoneyCalculated, discount)
                    );
                    $(this.selectors.index.priceMoneyInput).val(this.data.index.totalPriceMoneyCalculated);
                    break;
                }
            }
        };

        const setBuyType = buyType => {
            $(this.selectors.index.buyTypeInput).val(buyType);
        };

        const initData = () => {
            this.data.index = Object.assign(
                this.data.index,

                {totalPriceCredits: $(this.selectors.index.priceCreditsInput).val()},
                {totalPriceMoney: $(this.selectors.index.priceMoneyInput).val()},

                $(this.selectors.index.typeCredits).data(),
                $(this.selectors.index.typeMoney).data(),

                $(this.selectors.index.couponTypeCredits).data(),
                $(this.selectors.index.couponTypeMoney).data(),

                $(this.selectors.index.persistentDiscountCredits).data(),
                $(this.selectors.index.persistentDiscountMoney).data()
            );

            this.data.index.totalPriceCreditsCalculated = this.data.index.totalPriceCredits;
            this.data.index.totalPriceMoneyCalculated = this.data.index.totalPriceMoney;
        };

        const processInputtedCoupon = () => {
            const $couponInput = $(this.selectors.index.couponInput);
            if ($couponInput.val().length === 5) {
                processCoupon($couponInput);
            }
        };

        const processPersistentDiscount = () => {
            if (this.data.index.persistentDiscountCredits) {
                const $items = $(this.selectors.index.itemCredits);

                $items.map((idx, item) => {
                    const $item = $(item);
                    const price = $item.data('item-price');

                    const discountedPrice = $item.data('persistent-discount-can-be-applied')
                        ? Math.ceil(getDiscountedPrice(price, this.data.index.persistentDiscountCredits))
                        : price;

                    $item.html(getPriceText(price, discountedPrice, this.data.index.persistentDiscountCredits));
                });

                updateTotalPrices();
            }

            if (this.data.index.persistentDiscountMoney) {
                const $items = $(this.selectors.index.itemMoney);

                $items.map((idx, item) => {
                    const $item = $(item);
                    const price = $item.data('item-price');

                    const discountedPrice = $item.data('persistent-discount-can-be-applied')
                        ? getDiscountedPrice(price, this.data.index.persistentDiscountMoney)
                        : price;

                    $item.html(getPriceText(price, discountedPrice, this.data.index.persistentDiscountMoney));
                });

                updateTotalPrices();
            }
        };

        initData();

        processInputtedCoupon();
        processPersistentDiscount();
    };

}