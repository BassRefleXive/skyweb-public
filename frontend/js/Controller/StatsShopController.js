import Select2Service from './../Service/Select2Service'

export default class StatsShopController {
    action = undefined;

    select2Service = undefined;

    selectors = {
        step: {
            select: '#stats_shop_step',
        },
    };

    constructor(action) {
        this.action = action;
        this.select2Service = new Select2Service();
    }

    dispatch = () => {
        switch (this.action) {
            case 'index': {
                this.indexAction();
                break;
            }
        }
    };

    indexAction = () => {
        this.select2Service.initSelect2($(this.selectors.step.select));
    };
}