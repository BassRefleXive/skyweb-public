export default class AdminNewsController {

    action = undefined;

    selectors = {
        index: {
            toggleTooltip: '[data-toggle="tooltip"]',
        },
    };

    constructor(action) {
        this.action = action;
    }

    dispatch = () => {
        switch (this.action) {
            case 'list':
            case 'index': {
                this.indexAction();
                break;
            }
        }
    };

    indexAction = () => {
        $(this.selectors.index.toggleTooltip).tooltip()
    };

}