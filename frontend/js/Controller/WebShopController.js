import Select2Service from './../Service/Select2Service'

export default class WebShopController {
    action = undefined;

    select2Service = undefined;

    selectors = {
        index: {
            categorySelector: '.webshop .selector.-category li',
            itemsSelector: '.webshop .selector.-item li',
        },
        buy: {
            form: '.-buy form',
            priceCredits: '.-buy form #web_shop_buy_item_priceCredits',
            priceMoney: '.-buy form #web_shop_buy_item_priceMoney',
            priceDisplayCredits: {
                regular: '.-buy form .price .credits .regular .value',
                description: '.-buy form .price .credits .description .value',
            },
            priceDisplayMoney: {
                regular: '.-buy form .price .money .regular .value',
                description: '.-buy form .price .money .description .value',
            },

            excellentOptions: '.-buy form .options-excellent input[type="checkbox"]',
            checkedExcellentOptions: '.-buy form .options-excellent input[type="checkbox"]:checked',
            levelSelector: '.-buy form #web_shop_buy_item_level',
            optionSelector: '.-buy form #web_shop_buy_item_option',
            skillSelector: '.-buy form #web_shop_buy_item_skill',
            luckSelector: '.-buy form #web_shop_buy_item_luck',
            ancientSelector: '.-buy form #web_shop_buy_item_ancient',
            harmonySelector: '.-buy form #web_shop_buy_item_harmony',
            pvpSelector: '.-buy form #web_shop_buy_item_pvp',

            select2: {
                level: {
                    select: '#web_shop_buy_item_level',
                    label: 'label[for=\'web_shop_buy_item_level\']',
                },
                option: {
                    select: '#web_shop_buy_item_option',
                    label: 'label[for=\'web_shop_buy_item_option\']',
                },
                harmony: {
                    select: '#web_shop_buy_item_harmony',
                    label: 'label[for=\'web_shop_buy_item_harmony\']',
                },
                ancient: {
                    select: '#web_shop_buy_item_ancient',
                    label: 'label[for=\'web_shop_buy_item_ancient\']',
                },
            }
        },
    };

    data = {
        index: {
            selections: {}
        },
        buy: {
            persistentDiscountCredits: undefined,
            persistentDiscountMoney: undefined,
            maxExcCount: undefined,
            priceCredits: undefined,
            priceMoney: undefined,
            globalCreditsDiscountPercent: undefined,
            globalMoneyDiscountPercent: undefined,
            priceMultiplier: {
                skill: undefined,
                luck: undefined,
                excellent: undefined,
                ancient: undefined,
                harmony: undefined,
                pvp: undefined,
            },
            priceAdd: {
                level: {
                    credits: undefined,
                    money: undefined,
                },
                option: {
                    credits: undefined,
                    money: undefined,
                },
            },
        },
    };

    constructor(action) {
        this.action = action;
        this.select2Service = new Select2Service();
    }

    dispatch = () => {
        switch (this.action) {
            case 'index': {
                this.indexAction();
                break;
            }
            case 'buy': {
                this.buyAction();
                break;
            }
        }
    };

    indexAction = () => {
        const that = this;

        $(that.selectors.index.categorySelector).on('click', ev => {
            ev.preventDefault();

            const $target = $(ev.currentTarget);

            if ($target.find('a').hasClass('-active')) {
                that.data.index.selections = {};
            } else {
                const currentLevel = $target.data('level');

                 for (let i = Object.keys(that.data.index.selections).length - 1; i >= currentLevel; i--) {
                     delete that.data.index.selections[i];
                 }

                that.data.index.selections[$target.data('level')] = $target.data('category-id');
            }

            updateVisibility();
        });

        const updateVisibility = () => {
            if (Object.keys(that.data.index.selections).length) {
                $(that.selectors.index.categorySelector).find('a').removeClass('-active');

                $(that.selectors.index.categorySelector).addClass('hidden');
                $(that.selectors.index.categorySelector).parent().removeClass('-visible');

                $(that.selectors.index.categorySelector).parent().find(`[data-parent-id="0"]`).removeClass('hidden');

                Object.keys(that.data.index.selections).map(level => {
                    const selectionOnLevel = that.data.index.selections[level];

                    $(that.selectors.index.categorySelector).parent().find(`[data-category-id="${selectionOnLevel}"]`).find('a').addClass('-active');
                    $(that.selectors.index.categorySelector).parent().find(`[data-parent-id="${selectionOnLevel}"]`).removeClass('hidden');
                    $(that.selectors.index.categorySelector).parent().find(`[data-category-id="${selectionOnLevel}"]`).parent('ul').addClass('-visible');
                });

               $(that.selectors.index.itemsSelector).addClass('hidden');
               $(that.selectors.index.itemsSelector).parent().find(`[data-category-id="${that.data.index.selections[Object.keys(that.data.index.selections).length - 1]}"]`).removeClass('hidden');
            }
        };

    };

    buyAction = () => {
        const that = this;
        this.select2Service.initSelect2($(this.selectors.buy.select2.level.select), $(this.selectors.buy.select2.level.label).text());
        this.select2Service.initSelect2($(this.selectors.buy.select2.option.select), $(this.selectors.buy.select2.option.label).text());
        this.select2Service.initSelect2($(this.selectors.buy.select2.harmony.select), $(this.selectors.buy.select2.harmony.label).text());
        this.select2Service.initSelect2($(this.selectors.buy.select2.ancient.select), $(this.selectors.buy.select2.ancient.label).text());

        $(that.selectors.buy.excellentOptions).on('click', ev => {
            if ($(that.selectors.buy.checkedExcellentOptions).length > this.data.buy.maxExcCount) {
                ev.preventDefault();
                ev.stopPropagation();
            }
        });

        $(that.selectors.buy.levelSelector).on('change', () => updatePrice());
        $(that.selectors.buy.optionSelector).on('change', () => updatePrice());
        $(that.selectors.buy.skillSelector).on('change', () => updatePrice());
        $(that.selectors.buy.luckSelector).on('change', () => updatePrice());
        $(that.selectors.buy.excellentOptions).on('change', () => updatePrice());
        $(that.selectors.buy.ancientSelector).on('change', () => updatePrice());
        $(that.selectors.buy.harmonySelector).on('change', () => updatePrice());
        $(that.selectors.buy.pvpSelector).on('change', () => updatePrice());

        const updatePrice = () => {
            const price = calculatePrice();
            $(that.selectors.buy.priceCredits).val(price.beforeDiscount.credits);
            $(that.selectors.buy.priceMoney).val(price.beforeDiscount.money);

            const priceDisplayCredits = getPriceText(price.beforeDiscount.credits, price.afterDiscount.credits, this.data.buy.persistentDiscountCredits);
            const priceDisplayMoney = getPriceText(price.beforeDiscount.money, price.afterDiscount.money, this.data.buy.persistentDiscountMoney);

            $(that.selectors.buy.priceDisplayCredits.regular).html(priceDisplayCredits.regular);
            $(that.selectors.buy.priceDisplayCredits.description).html(priceDisplayCredits.description);

            $(that.selectors.buy.priceDisplayMoney.regular).html(priceDisplayMoney.regular);
            $(that.selectors.buy.priceDisplayMoney.description).html(priceDisplayMoney.description);
        };

        const getPriceText = (priceBeforeDiscount, priceAfterDiscount, discount) => {

            if (Number.parseFloat(priceBeforeDiscount) === Number.parseFloat(priceAfterDiscount)) {
                return {
                    regular: priceAfterDiscount,
                    description: ``,
                };
            } else {
                return {
                    regular: priceAfterDiscount,
                    description: `(${priceBeforeDiscount} - ${discount}%)`,
                };
            }
        };

        const calculatePrice = () => {
            const currentValues = {
                level: Number.parseInt($(that.selectors.buy.levelSelector).val()) || 0,
                option: (Number.parseInt($(that.selectors.buy.optionSelector).val()) / 4) || 0,
                skill: ($(that.selectors.buy.skillSelector).prop('checked') && 1) || 0,
                luck: ($(that.selectors.buy.luckSelector).prop('checked') && 1) || 0,
                excellent: $(that.selectors.buy.checkedExcellentOptions).length || 0,
                ancient: ($(that.selectors.buy.ancientSelector).val() && 1) || 0,
                harmony: ($(that.selectors.buy.harmonySelector).val() && 1) || 0,
                pvp: ($(that.selectors.buy.pvpSelector).prop('checked') && 1) || 0,
            };

            let credits = this.data.buy.priceCredits;
            let money = this.data.buy.priceMoney;

            Object.keys(currentValues).map(key => {
                const currentValue = currentValues[key];
                const currentValuePriceMultiplier = this.data.buy.priceMultiplier[key];
                const currentValuePriceAdd = this.data.buy.priceAdd[key];
                if (currentValuePriceMultiplier && currentValue > 0) {
                    credits += this.data.buy.priceCredits * currentValuePriceMultiplier * currentValue;
                    money += Math.ceil((this.data.buy.priceMoney * currentValuePriceMultiplier * currentValue) * 100) / 100;
                }
                if (currentValuePriceAdd && currentValue > 0) {
                    credits += currentValue * currentValuePriceAdd.credits;
                    money += Math.ceil((currentValue * currentValuePriceAdd.money) * 100) / 100;
                }
            });

            credits = this.data.buy.priceMultiplier.globalCreditsDiscountPercent > 0
                ? Math.ceil(getDiscountedPrice(credits, this.data.buy.priceMultiplier.globalCreditsDiscountPercent))
                : credits;

            money = this.data.buy.priceMultiplier.globalMoneyDiscountPercent > 0
                ? getDiscountedPrice(money, this.data.buy.priceMultiplier.globalMoneyDiscountPercent)
                : money;

            return {
                beforeDiscount: {
                    credits: Math.ceil(credits),
                    money: Math.ceil(money * 100) / 100,
                },
                afterDiscount: {
                    credits: this.data.buy.persistentDiscountCredits
                        ? Math.ceil(getDiscountedPrice(credits, this.data.buy.persistentDiscountCredits))
                        : credits,
                    money: this.data.buy.persistentDiscountMoney
                        ? Math.ceil(getDiscountedPrice(money, this.data.buy.persistentDiscountMoney) * 100) / 100
                        : Math.ceil(money * 100) / 100,
                },
            }
        };

        const getDiscountedPrice = (price, discount) => {
            return Math.ceil(price * (100 - discount)) / 100;
        };

        const initData = () => {
            this.data.buy.maxExcCount = Number.parseInt($(that.selectors.buy.form).data('max-exc-count'));
            this.data.buy.persistentDiscountCredits = Number.parseInt($(that.selectors.buy.form).data('persistent-discount-credits'));
            this.data.buy.persistentDiscountMoney = Number.parseInt($(that.selectors.buy.form).data('persistent-discount-money'));

            this.data.buy.priceCredits = Number.parseInt($(that.selectors.buy.priceCredits).val());
            this.data.buy.priceMoney = Number.parseFloat($(that.selectors.buy.priceMoney).val());

            this.data.buy.priceAdd.level.money = Number.parseFloat($(that.selectors.buy.form).data('level-price-add-money'));
            this.data.buy.priceAdd.option.money = Number.parseFloat($(that.selectors.buy.form).data('option-price-add-money'));
            this.data.buy.priceAdd.level.credits = Number.parseFloat($(that.selectors.buy.form).data('level-price-add-credits'));
            this.data.buy.priceAdd.option.credits = Number.parseFloat($(that.selectors.buy.form).data('option-price-add-credits'));

            this.data.buy.priceMultiplier.skill = Number.parseFloat($(that.selectors.buy.form).data('skill-price-multiplier'));
            this.data.buy.priceMultiplier.luck = Number.parseFloat($(that.selectors.buy.form).data('luck-price-multiplier'));
            this.data.buy.priceMultiplier.excellent = Number.parseFloat($(that.selectors.buy.form).data('excellent-price-multiplier'));
            this.data.buy.priceMultiplier.ancient = Number.parseFloat($(that.selectors.buy.form).data('ancient-price-multiplier'));
            this.data.buy.priceMultiplier.harmony = Number.parseFloat($(that.selectors.buy.form).data('harmony-price-multiplier'));
            this.data.buy.priceMultiplier.pvp = Number.parseFloat($(that.selectors.buy.form).data('pvp-price-multiplier'));
            this.data.buy.priceMultiplier.globalCreditsDiscountPercent = Number.parseInt($(that.selectors.buy.form).data('global-credits-discount-percent'));
            this.data.buy.priceMultiplier.globalMoneyDiscountPercent = Number.parseInt($(that.selectors.buy.form).data('global-money-discount-percent'));
        };

        initData();
        updatePrice();
    };

}