import Select2Service from './../Service/Select2Service'
import Routing from './../Service/Routing'
import Masonry from 'masonry-layout'

export default class AccountsController {
    action = undefined;

    select2Service = undefined;

    selectors = {
        addShopStats: {
            character: {
                select: '#add_shop_stats_character',
            }
        },
        hideInfo: {
            days: {
                select: '#hide_info_days',
                options: '#hide_info_days option',
            },
            dailyRate: '#hide_info_dailyRate',
        },
        zenBank: {
            type: {
                select: '#zen_bank_operation_type',
            }
        },
        fundsTransfer: {
            currency: {
                select: '#funds_transfer_currency',
            }
        },
        webCoin: {
            type: {
                select: '#web_coin_operation_type',
            }
        },
        index: {
            toggleItemPopupTrigger: '.warehouse [data-toggle-item-popup] [data-item-serial]',
            warehouseContainer: '.warehouse',
            triggersContainer: '.warehouse .controls_container',
            moveToWebStorageTrigger: '.warehouse [data-move-to-storage-trigger]',
            marketSellTrigger: '.warehouse [data-market-sell-trigger]',
            removeTrigger: '.warehouse [data-remove-trigger]',
            webStorage: {
                modal: {
                    container: '#modal-web-storage',
                },
                grid: {
                    container: '.web-storage .items-container',
                    item: '.web-storage .items-container .item-container',
                },
                trigger: {
                    itemRemove: '.web-storage [data-remove-trigger]',
                },
            },
        },
    };

    data = {
        hideInfo: {
            dailyRate: undefined,
        },
        index: {
            selectedItemSerial: undefined,
        },
    };

    constructor(action) {
        this.action = action;
        this.select2Service = new Select2Service();
    }

    dispatch = () => {
        switch (this.action) {
            case 'add.shop.stats': {
                this.addShopStatsAction();
                break;
            }
            case 'hide.info': {
                this.hideInfoAction();
                break;
            }
            case 'zen.bank': {
                this.zenBankAction();
                break;
            }
            case 'funds.transfer': {
                this.fundsTransferAction();
                break;
            }
            case 'web.coin': {
                this.webCoinAction();
                break;
            }
            case 'index': {
                this.indexAction();
                break;
            }
        }
    };

    indexAction = () => {
        const that = this;

        const initWebStorage = () => {
            $(this.selectors.index.webStorage.modal.container).on('shown.bs.modal', () => {
                new Masonry(this.selectors.index.webStorage.grid.container, {
                    itemSelector: this.selectors.index.webStorage.grid.item,
                    columnWidth: 1
                });
            });

            $(this.selectors.index.webStorage.trigger.itemRemove).on('click', ev => {
                if (!confirm('Are you sure?')) {
                    ev.preventDefault();
                }
            });
        };

        const initWarehouse = () => {
            $(this.selectors.index.toggleItemPopupTrigger).on('click', ev => {
                const $target = $(ev.currentTarget);

                setSelectedSerial($target.data('item-serial'));
                highlightSelectedItem();
                updateLinks();
                openControlsContainer();
            });

            $(this.selectors.index.moveToWebStorageTrigger).on('click', ev => {
                if (that.data.index.selectedItemSerial === undefined) {
                    alert('Please select item first.');
                    ev.preventDefault();
                }
            });

            $(this.selectors.index.removeTrigger).on('click', ev => {
                if (that.data.index.selectedItemSerial === undefined) {
                    alert('Please select item first.');
                    ev.preventDefault();
                } else {
                    if (!confirm('Are you sure?')) {
                        ev.preventDefault();
                    }
                }
            });

            $(this.selectors.index.marketSellTrigger).on('click', ev => {
                if (that.data.index.selectedItemSerial === undefined) {
                    alert('Please select item first.');
                    ev.preventDefault();
                }
            });

            const setSelectedSerial = serial => {
                this.data.index.selectedItemSerial = serial;
            };

            const highlightSelectedItem = () => {
                $(this.selectors.index.toggleItemPopupTrigger).closest('li').removeClass('-selected');
                $(this.selectors.index.toggleItemPopupTrigger).removeClass('-selected');

                $(this.selectors.index.toggleItemPopupTrigger + `[data-item-serial="${this.data.index.selectedItemSerial}"]`).closest('li').addClass('-selected');
                $(this.selectors.index.toggleItemPopupTrigger + `[data-item-serial="${this.data.index.selectedItemSerial}"]`).addClass('-selected');
            };

            const updateLinks = () => {
                $(this.selectors.index.moveToWebStorageTrigger).attr(
                    'href',
                    Routing.generate(
                        'sky_accounts_warehouse_move',
                        {
                            serial: this.data.index.selectedItemSerial
                        }
                    )
                );

                $(this.selectors.index.marketSellTrigger).attr(
                    'href',
                    Routing.generate(
                        'sky_accounts_warehouse_sell',
                        {
                            serial: this.data.index.selectedItemSerial
                        }
                    )
                );

                $(this.selectors.index.removeTrigger).attr(
                    'href',
                    Routing.generate(
                        'sky_accounts_warehouse_remove',
                        {
                            serial: this.data.index.selectedItemSerial
                        }
                    )
                );
            };

            const openControlsContainer = () => {
                const $selectedItem = $(this.selectors.index.toggleItemPopupTrigger + `[data-item-serial="${this.data.index.selectedItemSerial}"]`);
                const $warehouseContainer = $(this.selectors.index.warehouseContainer);
                const $triggersContainer = $(this.selectors.index.triggersContainer);

                const selectedItemOffset = $selectedItem.offset();
                const warehouseContainerOffset = $warehouseContainer.offset();

                const triggersContainerPosition = {
                    left: selectedItemOffset.left - warehouseContainerOffset.left - ($(this.selectors.index.triggersContainer).width() / 2) + ($selectedItem.width() / 2) - 22,
                    top: warehouseContainerOffset.top - selectedItemOffset.top > 0
                        ? warehouseContainerOffset.top - selectedItemOffset.top + $selectedItem.height() + 20
                        : selectedItemOffset.top - warehouseContainerOffset.top + $selectedItem.height() + 20,
                };


                if (Number.parseInt($triggersContainer.css('opacity')) > 0) {
                    $(this.selectors.index.triggersContainer).removeClass('-opened');

                    setTimeout(() => {
                        $(this.selectors.index.triggersContainer).addClass('-opened');
                        $triggersContainer.css('left', triggersContainerPosition.left);
                        $triggersContainer.css('top', triggersContainerPosition.top);
                    }, 150);
                } else {
                    $(this.selectors.index.triggersContainer).addClass('-opened');
                    $triggersContainer.css('left', triggersContainerPosition.left);
                    $triggersContainer.css('top', triggersContainerPosition.top);
                }
            };

            const initTriggersContainerOutsideClick = () => {
                $(this.selectors.index.triggersContainer).on('click', ev => {
                    ev.stopPropagation();
                });

                $(window).on('click', ev => {
                    const $triggersContainer = $(this.selectors.index.triggersContainer);

                    if (Number.parseInt($triggersContainer.css('opacity')) > 0) {
                        $(this.selectors.index.triggersContainer).removeClass('-opened');
                    }

                    if ($(this.selectors.index.warehouseContainer).has($(ev.target)).length < 1) {
                        $(this.selectors.index.toggleItemPopupTrigger).closest('li').removeClass('-selected');
                        $(this.selectors.index.toggleItemPopupTrigger).removeClass('-selected');
                    }
                });
            };

            initTriggersContainerOutsideClick();
        };

        initWebStorage();
        initWarehouse();
    };

    addShopStatsAction = () => {
        this.select2Service.initSelect2($(this.selectors.addShopStats.character.select));
    };

    hideInfoAction = () => {
        const initData = new Promise((resolve, reject) => {
            this.data.hideInfo.dailyRate = Number.parseInt($(this.selectors.hideInfo.dailyRate).val());

            resolve();
        });

        const adjustSelect = new Promise((resolve, reject) => {
            $(this.selectors.hideInfo.days.options).map((idx, el) => {
                const $el = $(el);

                $el.text(`${$el.text()} - ${Number.parseInt((Number.parseInt($el.val()) + 1) * this.data.hideInfo.dailyRate)} WCoin`);
            });

            resolve();
        });

        const initSelect2 = () => {
            this.select2Service.initSelect2($(this.selectors.hideInfo.days.select));
        };

        initData
            .then(adjustSelect)
            .then(initSelect2)
        ;
    };

    zenBankAction = () => {
        this.select2Service.initSelect2($(this.selectors.zenBank.type.select));
    };

    fundsTransferAction = () => {
        this.select2Service.initSelect2($(this.selectors.fundsTransfer.currency.select));
    };

    webCoinAction = () => {
        this.select2Service.initSelect2($(this.selectors.webCoin.type.select));
    };
}