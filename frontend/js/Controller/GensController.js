import GensPopupService from './../Service/GensPopupService'

export default class CharactersController {
    action = undefined;

    gensPopupService = undefined;

    selectors = {
    };

    constructor(action) {
        this.action = action;
        this.gensPopupService = new GensPopupService();
    }

    dispatch = () => {
        switch (this.action) {
            case 'index': {
                this.indexAction();
                break;
            }
        }
    };

    indexAction = () => {
        this.gensPopupService.init();
    };
}