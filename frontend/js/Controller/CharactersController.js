import Select2Service from './../Service/Select2Service'
import GensPopupService from './../Service/GensPopupService'
import BuffPopupService from './../Service/BuffPopupService'
import SkillPopupService from './../Service/SkillPopupService'
import RestService from './../Service/RestService'
import Routing from './../Service/Routing'
import toastr from 'toastr'

export default class CharactersController {
    action = undefined;

    select2Service = undefined;
    gensPopupService = undefined;
    buffPopupService = undefined;
    skillPopupService = undefined;

    selectors = {
        class: {
            select: '#class_change_class',
            label: 'label[for=\'class_change_class\']',
        },
        skill: {
            index: {
                skillContainer: '.reset-skills .skills li',
                characterName: '.reset-skills',
                remove: '.reset-skills button[type="submit"]',
            },
        },
    };

    data = {
        skill: {
            index: {
                skills: [],
                messages: {},
            },
        },
    };

    constructor(action) {
        this.action = action;
        this.select2Service = new Select2Service();
        this.gensPopupService = new GensPopupService();
        this.buffPopupService = new BuffPopupService();
        this.skillPopupService = new SkillPopupService();
        this.restService = new RestService();
    }

    dispatch = () => {
        switch (this.action) {
            case 'change.class': {
                this.changeClassAction();
                break;
            }
            case 'view': {
                this.viewAction();
                break;
            }
            case 'index': {
                this.indexAction();
                break;
            }
            case 'skill.index': {
                this.skillIndexAction();
                break;
            }
        }
    };

    skillIndexAction = () => {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-top-center",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        $(this.selectors.skill.index.remove).on('click', ev => {
            if (this.data.skill.index.skills.length < 1) {
                toastr.error(this.data.skill.index.messages['empty']);
            } else {
                toastr.info(this.data.skill.index.messages['processing']);
                $(ev.target).attr('disabled', true);

                this.restService.putAction(
                    Routing.generate(
                        'sky_characters_skill.remove',
                        {
                            name: $(this.selectors.skill.index.characterName).data('character-name')
                        }
                    ),
                    {skills: this.data.skill.index.skills},
                    true
                )
                    .then(this.restService.processResponse(`Reset skills response.`, []))
                    .then(response => {
                        toastr.success(this.data.skill.index.messages['success']);

                        this.data.skill.index.skills.map(skill => {
                            $(this.selectors.skill.index.skillContainer).parent().children(`[data-skill="${skill}"]`).remove();
                        });

                        this.data.skill.index.skills = [];

                        $(ev.target).attr('disabled', false);
                    })
                    .catch(() => {
                        toastr.error(this.data.skill.index.messages['error']);

                        $(ev.target).attr('disabled', false);
                    });
            }
        });

        $(this.selectors.skill.index.skillContainer).on('click', ev => {
            const $target = $(ev.currentTarget);

            const index = this.data.skill.index.skills.indexOf($target.data('skill'));

            if (-1 === index) {
                this.data.skill.index.skills.push($target.data('skill'));
            } else {
                this.data.skill.index.skills.splice(index, 1);
            }

            updateSelectedSkills();
        });

        const updateSelectedSkills = () => {
            $(this.selectors.skill.index.skillContainer).removeClass('-checked');

            this.data.skill.index.skills.map(skill => {
                $(this.selectors.skill.index.skillContainer).parent().children(`[data-skill="${skill}"]`).addClass('-checked');
            });
        };

        const initMessages = () => {
            this.data.skill.index.messages = $(this.selectors.skill.index.characterName).data('messages');
        };

        initMessages();
        this.skillPopupService.init();
    };

    changeClassAction = () => {
        this.select2Service.initSelect2($(this.selectors.class.select), $(this.selectors.class.label).text());
    };

    viewAction = () => {
        this.gensPopupService.init();
        this.buffPopupService.init();
        this.skillPopupService.init();
    };

    indexAction = () => {
        this.gensPopupService.init();
    };
}