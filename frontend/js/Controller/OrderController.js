import Routing from './../Service/Routing'

export default class OrderController {

    action = undefined;

    selectors = {
        view: {
            button: '#paypal-button-container',
        },
    };

    data = {
        view: {
            environment: undefined,
            display: undefined,
            orderId: undefined,
        },
    };

    constructor(action) {
        this.action = action;
    }

    dispatch = () => {
        switch (this.action) {
            case 'view': {
                this.viewAction();
                break;
            }
        }
    };

    viewAction = () => {
        const initData = new Promise((resolve, reject) => {
            this.data.view.environment = $(this.selectors.view.button).data('environment');
            this.data.view.display = $(this.selectors.view.button).data('display');
            this.data.view.orderId = $(this.selectors.view.button).data('order-id');

            resolve();
        });

        const initPayPal = new Promise((resolve, reject) => {
            const that = this;

            undefined !== paypal && paypal.Button.render({
                env: this.data.view.environment,
                commit: this.data.view.display,
                style: {
                    label: 'pay',
                    size:  'responsive',
                    shape: 'rect',
                    color: 'blue',
                    tagline: false,
                },
                payment: () => {
                    return paypal.request.post(
                        Routing.generate(
                            'sky_integrations_payments_paypal_initialize',
                            {
                                id: that.data.view.orderId
                            }
                        )
                    )
                        .then(function (res) {
                            return res.id;
                        });
                },

                onAuthorize: (data, actions) => {
                    return paypal.request.post(
                        Routing.generate(
                            'sky_integrations_payments_paypal_finalize',
                            {
                                id: that.data.view.orderId
                            }
                        ),
                        {
                            paymentID: data.paymentID,
                            payerID: data.payerID
                        }
                    )
                        .then(function (res) {
                            window.location = Routing.generate('sky_integrations_payments_paypal_success');
                        });
                },

                onError: (a, b, c) => {
                    window.location = Routing.generate(
                        'sky_integrations_payments_paypal_fail',
                        {
                            id: that.data.view.orderId
                        }
                    );
                }

            }, this.selectors.view.button);

            resolve();
        });

        initData
            .then(initPayPal)
        ;
    };
}