import Select2Service from './../Service/Select2Service'

export default class MarketController {
    action = undefined;

    select2Service = undefined;

    selectors = {
        category: {
            select: '#market_search_category',
            label: 'label[for=\'market_search_category\']',
        },
        level: {
            select: '#market_search_level',
            label: 'label[for=\'market_search_level\']',
        },
        option: {
            select: '#market_search_option',
            label: 'label[for=\'market_search_option\']',
        },
        currency: {
            select: '#market_search_priceType',
            label: 'label[for=\'market_search_priceType\']',
        },
    };

    constructor(action) {
        this.action = action;
        this.select2Service = new Select2Service();
    }

    dispatch = () => {
        switch (this.action) {
            case 'index': {
                this.indexAction();
                break;
            }
        }
    };

    indexAction = () => {
        this.select2Service.initSelect2($(this.selectors.category.select), $(this.selectors.category.label).text());
        this.select2Service.initSelect2($(this.selectors.level.select), $(this.selectors.level.label).text());
        this.select2Service.initSelect2($(this.selectors.option.select), $(this.selectors.option.label).text());
        this.select2Service.initSelect2($(this.selectors.currency.select), $(this.selectors.currency.label).text());
    };
}