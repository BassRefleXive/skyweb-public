export default class WCoinShopController {
    action = undefined;

    selectors = {
        index: {
            coins: '#w_coin_shop_purchase_coins',
            money: '#w_coin_shop_purchase_money',
            rate: '#w_coin_shop_purchase_coinsPerUsd',
            minAmount: '#w_coin_shop_purchase_minAmount',
            formSubmit: '.wcoin-shop button[type="submit"]',
        },
    };

    data = {
        index: {
            coins: 0,
            money: 0,
            rate: 0,
            minAmount: 0,
        },
    };

    constructor(action) {
        this.action = action;
    }

    dispatch = () => {
        switch (this.action) {
            case 'index': {
                this.indexAction();
                break;
            }
        }
    };

    indexAction = () => {
        $(this.selectors.index.coins).on('change paste keyup', () => updateMoney());
        $(this.selectors.index.money).on('change paste keyup', () => updateCoins());


        const updateMoney = () => {
            readData();

            this.data.index.money = Math.ceil(this.data.index.coins / this.data.index.rate * 100) / 100;

            renderChanges();
        };

        const updateCoins = () => {
            readData();

            this.data.index.coins = this.data.index.money * this.data.index.rate;

            renderChanges();
        };

        const renderChanges = () => {
            $(this.selectors.index.coins).val(this.data.index.coins);
            $(this.selectors.index.money).val(this.data.index.money);
            $(this.selectors.index.formSubmit).prop('disabled', this.data.index.money < this.data.index.minAmount);
        };

        const readData = () => {
            this.data.index.coins = Number.parseInt($(this.selectors.index.coins).val()) || 0;
            this.data.index.money = Number.parseFloat($(this.selectors.index.money).val()) || 0;
            this.data.index.rate = Number.parseInt($(this.selectors.index.rate).val()) || 0;
            this.data.index.minAmount = Number.parseFloat($(this.selectors.index.minAmount).val()) || 0;
        };

        readData();
    };
}