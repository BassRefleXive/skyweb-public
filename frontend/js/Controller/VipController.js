import Select2Service from './../Service/Select2Service'

export default class VipController {
    action = undefined;

    select2Service = undefined;

    selectors = {
        purchase: {
            days: {
                select: '#purchase_vip_days',
                options: '#purchase_vip_days option',
            },
            dailyRate: '#purchase_vip_dailyRate',
        },
    };

    data = {
        purchase: {
            dailyRate: undefined,
        },
    };

    constructor(action) {
        this.action = action;
        this.select2Service = new Select2Service();
    }

    dispatch = () => {
        switch (this.action) {
            case 'buy': {
                this.purchaseAction();
                break;
            }
            case 'renewal': {
                this.purchaseAction();
                break;
            }
        }
    };

    purchaseAction = () => {
        const initData = new Promise((resolve, reject) => {
            this.data.purchase.dailyRate = Number.parseInt($(this.selectors.purchase.dailyRate).val());

            resolve();
        });

        const adjustSelect = new Promise((resolve, reject) => {
            $(this.selectors.purchase.days.options).map((idx, el) => {
                const $el = $(el);

                $el.text(`${$el.text()} - ${Number.parseInt((Number.parseInt($el.val()) + 1) * this.data.purchase.dailyRate)} WCoin`);
            });

            resolve();
        });

        const initSelect2 = () => {
            this.select2Service.initSelect2($(this.selectors.purchase.days.select));
        };

        initData
            .then(adjustSelect)
            .then(initSelect2)
        ;
    };
}