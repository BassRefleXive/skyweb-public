export default class AdminWebShopController {

    action = undefined;

    selectors = {
        edit: {
            addCategoryItem: '#add-another-webshop-category-item',
            removeCategoryItem: '.remove-webshop-category-item',
            categoryItemsList: '#webshop-category-items-list',
        },
    };

    constructor(action) {
        this.action = action;
    }

    dispatch = () => {
        switch (this.action) {
            case 'create':
            case 'edit': {
                this.editAction();
                break;
            }
        }
    };

    editAction = () => {
        const that = this;

        $(that.selectors.edit.addCategoryItem).on('click', ev => {
            ev.preventDefault();

            const itemsList = $(that.selectors.edit.categoryItemsList);

            let newWidget = itemsList.attr('data-prototype');

            const itemsCount = itemsList.find('li').length;
            newWidget = newWidget.replace(/__name__/g, itemsCount);

            const newItem = $('<li></li>').html(newWidget);
            newItem.appendTo(itemsList);

        });

        $(that.selectors.edit.removeCategoryItem).on('click', ev => {
            ev.preventDefault();

            const $target = $(ev.target);

            $target.closest('li').remove();

        });

    };

}