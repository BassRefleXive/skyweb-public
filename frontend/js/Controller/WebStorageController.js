import Select2Service from './../Service/Select2Service'

export default class WebStorageController {
    action = undefined;

    select2Service = undefined;

    selectors = {
        price: {
            select: '#market_sell_priceType',
            label: 'label[for=\'market_sell_priceType\']',
        },
    };

    constructor(action) {
        this.action = action;
        this.select2Service = new Select2Service();
    }

    dispatch = () => {
        switch (this.action) {
            case 'sell': {
                this.sellAction();
                break;
            }
        }
    };

    sellAction = () => {
        this.select2Service.initSelect2($(this.selectors.price.select), $(this.selectors.price.label).text());
    };
}