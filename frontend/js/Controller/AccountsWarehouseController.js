import Select2Service from './../Service/Select2Service'

export default class AccountsWarehouseController {
    action = undefined;

    select2Service = undefined;

    selectors = {
        index: {
            toggleItemPopupTrigger: '.warehouse [data-toggle-item-popup] [data-item-serial]',
            warehouseContainer: '.warehouse',
            triggersContainer: '.warehouse .controls_container',
            moveToWebStorageTrigger: '.warehouse [data-move-to-storage-trigger]',
            marketSellTrigger: '.warehouse [data-market-sell-trigger]',
        },
        sell: {
            price: {
                select: '#market_sell_priceType',
                label: 'label[for=\'market_sell_priceType\']',
            },
        },
    };

    data = {
        index: {
            selectedItemSerial: undefined,
        },
    };

    constructor(action) {
        this.action = action;
        this.select2Service = new Select2Service();
    }

    dispatch = () => {
        switch (this.action) {
            case 'sell': {
                this.sellAction();
                break;
            }
        }
    };

    sellAction = () => {
        this.select2Service.initSelect2($(this.selectors.sell.price.select), $(this.selectors.sell.price.label).text());
    };
}