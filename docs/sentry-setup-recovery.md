Guide How to Setup or Recover Sentry in Docker
==============================================

Setup:
------

1. Setup Docker (https://www.docker.com/)
2. Clone https://github.com/getsentry/onpremise
3. cd `clone directory` && make build

Next commands same both for Setup and Restore:
----------------------------------------------

1. Run Redis Docker Container:
```
docker run \
  --detach \
  --name sentry-redis \
  redis:3.2-alpine --appendonly yes
```
2. Run PostgreSQL Docker Container:
```
docker run \
  --detach \
  --name sentry-postgres \
  --env POSTGRES_PASSWORD=secret \
  --env POSTGRES_USER=sentry \
  postgres:9.5
```
3. Run Mailer Docker Container:
```
docker run \
  --detach \
  --name sentry-smtp \
  tianon/exim4
```

*Run only on setup:*
--------------------
```
docker run \
  --link sentry-redis:redis \
  --link sentry-postgres:postgres \
  --link sentry-smtp:smtp \
  --env SENTRY_SECRET_KEY=${SENTRY_SECRET_KEY} \
  --rm -it sentry-onpremise upgrade
```

4. Run WEB Container:
```
docker run \
  --detach \
  --link sentry-redis:redis \
  --link sentry-postgres:postgres \
  --link sentry-smtp:smtp \
  --env SENTRY_SECRET_KEY=${SENTRY_SECRET_KEY} \
  --env SENTRY_SERVER_EMAIL=no-reply@sentry.myenv.tk \
  --env SENTRY_EMAIL_HOST=smtp.mailgun.org \
  --env SENTRY_EMAIL_USER=postmaster@myenv.tk \
  --env SENTRY_EMAIL_PASSWORD=40f20592d5ee088c78277b5f67e62dfd \
  --name sentry-web-01 \
  --publish 9000:9000 \
  sentry-onpremise \
  run web
```
5. Starting Worker:
```
docker run \
  --detach \
  --name sentry-worker-01 \
  --link sentry-redis:redis \
  --link sentry-postgres:postgres \
  --link sentry-smtp:smtp \
  --env SENTRY_SECRET_KEY=${SENTRY_SECRET_KEY} \
  sentry-onpremise \
  run worker
```
5. Starting Cron Process:
```
docker run \
  --detach \
  --name sentry-cron \
  --link sentry-redis:redis \
  --link sentry-postgres:postgres \
  --link sentry-smtp:smtp \
  --env SENTRY_SECRET_KEY=${SENTRY_SECRET_KEY} \
  sentry-onpremise \
  run cron
```