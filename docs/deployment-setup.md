Deployment Setup Guide
==============================

Setup host user and working (deployment) directory
----------------------------------------------------------
#### Setup Working Directory
```sh
$ sudo su
$ adduser skyweb
$ sudo mkdir /var/www/skyweb
$ sudo mkdir /var/www/skyweb/releases
$ sudo chown -R skyweb:skyweb /var/www/skyweb
```
Need to move manually AppDatabase to:
```sh
/var/www/skyweb
```
#### Fixing File Permissions

```ssh
$ HTTPDUSER=$(ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1)

$ sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX /var/www/skyweb/releases
$ sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX /var/www/skyweb/releases
$ sudo adduser skyweb www-data
```
Add write permissions to directory for the user's group where AppDatabase.db is located.

#### Setup User Login Without Password
##### On machine where deploy task will be run:
When passphrase will be requested, just press ENTER.
```sh
$ ssh-keygen -t rsa
```
##### On host where we deploy app:
```sh
$ mkdir -p .ssh
```
##### On machine where deploy task will be run:
```sh
$ cat ~/.ssh/id_rsa.pub | ssh skyweb@HOST 'cat >> .ssh/authorized_keys'
```
where HOST is HOST of machine where we deploy task

##### On host where we deploy app:
```sh
$ chmod 700 ~/.ssh
$ chmod 600 ~/.ssh/authorized_keys
```


Setup GIT repository
----------------------------------------------------------
* There must be "production" branch

Setup nginx
-----------
```ssh
server {
    listen 80;
    server_name HOST;
    root /var/www/skyweb/current/web;

    location / {
        try_files $uri /app.php$is_args$args;
    }

    location ~ ^/(app|config)\.php(/|$) {
        fastcgi_pass unix:/run/php/php7.0-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
    }

    location ~ \.php$ {
      return 404;
    }

    error_log /var/log/nginx/skyweb.error.log;
    access_log /var/log/nginx/skyweb.access.log;
}
```
replace HOST with appropriate domain

###Command to use: 
bin/mage deploy digi-ocean
