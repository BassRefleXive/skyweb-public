Backup Database Guide
=============================

Make bach script executable
--------------------------
```
$ chmod a+x app/cron/database_backup.sh
```

Add following code to cron
--------------------------
```
$ crontab -e
  0 * * * * /var/www/skyweb/current/app/cron/database_backup.sh /var/www/skyweb/AppDatabase.db /var/www/skyweb/backup >/dev/null 2>&1
```
Script will backup AppDatabase.db to /var/www/skyweb/backup directory every hour.