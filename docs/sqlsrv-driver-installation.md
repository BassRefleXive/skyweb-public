Setup SQLSRV PHP Extension
==============================

```ssh
$ curl -s https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
$ sudo bash -c "curl -s https://packages.microsoft.com/config/ubuntu/16.04/prod.list > /etc/apt/sources.list.d/mssql-release.list"
$ sudo apt-get update
$ sudo ACCEPT_EULA=Y apt-get -y install msodbcsql mssql-tools
$ sudo apt-get install unixodbc-dev

$ sudo apt-get -y install gcc g++ make autoconf libc-dev pkg-config
$ sudo pecl install sqlsrv-4.1.6.1
$ sudo pecl install pdo_sqlsrv-4.1.6.1

$ sudo bash -c "echo extension=sqlsrv.so > /etc/php/7.2/cli/conf.d/sqlsrv.ini"
$ sudo bash -c "echo extension=pdo_sqlsrv.so > /etc/php/7.2/cli/conf.d/pdo_sqlsrv.ini"
$ sudo bash -c "echo extension=sqlsrv.so > /etc/php/7.2/fpm/conf.d/sqlsrv.ini"
$ sudo bash -c "echo extension=pdo_sqlsrv.so > /etc/php/7.2/fpm/conf.d/pdo_sqlsrv.ini"
$ sudo service php7.2-fpm restart
```