Auto Ban Scanners Setup Guide
=============================

Initialize iptables rule
------------------------
```
$ sudo iptables -N SCANNERS_AUTOBAN
$ sudo iptables -A SCANNERS_AUTOBAN -j RETURN
$ sudo iptables -A INPUT -p tcp --dport 80 -j SCANNERS_AUTOBAN
$ sudo iptables -A INPUT -p tcp --dport 443 -j SCANNERS_AUTOBAN
```

Grant execution rignhts to bash script
---------------------------------------------------------------------
Script receive as first argument iptables chain name (SCANNERS_AUTOBAN), second - file with IP addresses.
```
$ chmod a+x app/cron/scanner_auto_ban.sh
```

Install incron and configure it to execute bash script on IPs file update
------------------------------------------------------------------------
```
$ sudo apt-get install incron
$ echo root >> /etc/incron.allow

$ incrontab -e
  /var/www/skyweb/scanners_list.txt IN_MODIFY /var/www/skyweb/current/app/cron/scanner_auto_ban.sh SCANNERS_AUTOBAN /var/www/skyweb/scanners_list.txt >/dev/null 2>&1
```